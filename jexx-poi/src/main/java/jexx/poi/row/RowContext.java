package jexx.poi.row;

import jexx.poi.cell.FormulaCellValue;
import jexx.poi.cell.IMergedCell;
import jexx.poi.cell.MergedCell;
import jexx.poi.header.row.HeaderRowContext;
import jexx.poi.style.IWrapCellStyle;
import jexx.poi.util.CellOperateUtil;
import jexx.util.CollectionUtil;
import jexx.util.StringUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 行容器, 表示一行中的cell都在行容器中描述
 * @author jeff
 * @since 2019/5/22
 */
public class RowContext {

    private final DataRow rootRow;
    private final HeaderRowContext headerRowContext;

    private final Map<IMergedCell, DataRow> cellRowMap;
    private final Map<Integer, List<IMergedCell>> headerCellMap;

    /**
     * 是否有虚拟行
     */
    private boolean hasVirtualRow;

    public RowContext(DataRow rootRow, HeaderRowContext headerRowContext) {
        this.rootRow = rootRow;
        if(headerRowContext != null){
            this.rootRow.setHeaderRow(headerRowContext.getRootHeaderRow());
        }
        this.headerRowContext = headerRowContext;

        cellRowMap = new HashMap<>(64);
        headerCellMap = new HashMap<>(64);
    }

    public RowContext(DataRow rootRow) {
        this(rootRow, null);
    }

    public void addCell(DataRow row, IMergedCell cell){
        cellRowMap.put(cell, row);
        List<IMergedCell> headerCells = headerCellMap.computeIfAbsent(cell.getFirstColumnNum(), s->new ArrayList<>());
        headerCells.add(cell);

        row.addCell(cell);
    }

    public void addCell(IMergedCell cell){
        addCell(rootRow, cell);
    }

    /**
     * add row with some height at parent row
     *
     * @param parent parent row
     * @param rowHeight row height
     * @return the added row
     */
    public DataRow addDataRowAt(DataRow parent, int rowHeight){
        return parent.addDataRow(rowHeight);
    }

    /**
     * add row with 1 height at parent row
     *
     * @param parent parent row
     * @return the added row
     */
    public DataRow addDataRowAt(DataRow parent){
        return addDataRowAt(parent, 1);
    }

    /**
     * add row with some height at root row
     *
     * @param rowHeight row height
     * @return the added row
     */
    public DataRow addDataRowAt(int rowHeight){
        return addDataRowAt(this.rootRow, rowHeight);
    }

    /**
     * add row with 1 height at root row
     *
     * @return the added row
     */
    public DataRow addDataRowAt(){
        return addDataRowAt(1);
    }

    /**
     * 在指定数据行后添加虚拟行
     *
     * @param row 数据行
     * @return the append row
     */
    public VirtualRow appendVirtualRowAt(DataRow row){
        assert rootRow != row;
        this.hasVirtualRow = true;
        return row.parentRow.appendVirtualRowAt(row);
    }

    /**
     * 在指定行的子行中添加虚拟行
     *
     * @param row 指定行
     * @return 虚拟行
     */
    public VirtualRow addVirtualRowAt(DataRow row){
        this.hasVirtualRow = true;
        return row.addVirtualRow(1);
    }

    public IMergedCell getCell(IMergedCell cell, int columnNum){
        DataRow row = cellRowMap.get(cell);
        return row != null ? row.getCellByColumnNum(columnNum) : null;
    }

    /**
     * 修复数据行中的空单元格
     */
    public void repair(){
        rootRow.repair();
    }

    /**
     * 获取指定行的指定列的单元格
     *
     * @param row 指定行
     * @param columnNum 指定列
     */
    public List<IMergedCell> getChildrenCellByColumnNum(DataRow row, int columnNum){
        return row.getCellsByColumnNum(columnNum);
    }

    public FormulaCellValue sum(DataRow row, String columnNo){
        return sum(row, CellOperateUtil.toColumnNum(columnNo));
    }

    /**
     * 数据行下的指定列求和
     *
     * @param row 数据行
     * @param columnNum 指定列
     * @return 求和公式
     */
    public FormulaCellValue sum(DataRow row, int columnNum){
        return calculateFormula(row, columnNum, "SUM");
    }

    /**
     * 计算公式
     *
     * @param row 数据行
     * @param columnNum 指定列
     * @param expression 表达式, 如 SUM, AVG
     * @return 公式
     */
    public FormulaCellValue calculateFormula(DataRow row, int columnNum, String expression){
        List<IMergedCell> cells = getChildrenCellByColumnNum(row, columnNum);
        if(CollectionUtil.isEmpty(cells)){
            return null;
        }

        String columnLabel = CellOperateUtil.toColumnLabel(columnNum);
        List<String> formulas = new ArrayList<>();
        int f1 = 0;
        int f2 = 0;
        for(IMergedCell cell : cells){
            if(f1 == 0){
                f1 = cell.getFirstRowNum();
                f2 = cell.getLastRowNum();
            }
            else{
                assert cell.getFirstRowNum() > f2;
                if(cell.getFirstRowNum() == f2 + 1){
                    f2 = cell.getLastRowNum();
                }
                else{
                    formulas.add(StringUtil.format("{}{}:{}{}", columnLabel, f1, columnLabel, f2));
                    f1 = cell.getFirstRowNum();
                    f2 = cell.getLastRowNum();
                }
            }
        }
        formulas.add(StringUtil.format("{}{}:{}{}", columnLabel, f1, columnLabel, f2));
        StringBuilder stringBuilder = new StringBuilder(expression.concat("("));
        for (int i = 0; i < formulas.size(); i++) {
            stringBuilder.append(i == 0 ? "" : ",").append(formulas.get(i));
        }
        stringBuilder.append(")");
        return new FormulaCellValue(stringBuilder.toString());
    }

    public int getStartRowNum(){
        return rootRow.getStartRowNum();
    }

    public int getEndRowNum(){
        return rootRow.getEndRowNum();
    }

    public DataRow getRootRow(){
        return this.rootRow;
    }

    public List<IMergedCell> getHeaderCells(int columnNum){
        return headerCellMap.get(columnNum);
    }

    public HeaderRowContext getHeaderRowContext() {
        return headerRowContext;
    }

    /**
     * create row with some data
     * @param rowNum row num
     * @param columnNum column num
     * @param rowData some data
     * @param cellStyle cell style
     * @return {@link DataRow}
     */
    public static RowContext createRow(final int rowNum, final int columnNum, final List<?> rowData, IWrapCellStyle cellStyle){
        DataRow row = new DataRow(rowNum);
        RowContext rowContext = new RowContext(row);

        if(CollectionUtil.isNotEmpty(rowData)){
            for(int i = 0; i < rowData.size(); i++){
                MergedCell cell = new MergedCell(row, columnNum+i, columnNum+i, rowData.get(i), cellStyle);
                rowContext.addCell(row, cell);
            }
        }
        return rowContext;
    }

    public boolean isHasVirtualRow() {
        return hasVirtualRow;
    }
}

package jexx.poi.row;

/**
 * 行类型
 *
 * @author jeff
 * @since 2019/6/17
 */
public enum RowTypeEnum {

    /**
     * 数据
     */
    DATA,
    /**
     * 虚拟
     */
    VIRTUAL

}

package jexx.poi.row;

/**
 * 行扫描行为
 *
 * @author jeff
 * @since 2019/7/2
 */
public enum RowScanAction {

    /**
     * 继续处理行
     */
    CONTINUE,
    /**
     * 停止处理当前行
     */
    STOP,
    /**
     * 保留当前行，并停止扫描
     */
    KEEP_AND_STOP

}

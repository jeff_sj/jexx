package jexx.poi.row;

/**
 * predicate whether operates this row
 *
 * @author jeff
 */
@FunctionalInterface
public interface RowActionPredicate<T> {

    /**
     * 行处理行为; 若返回为null, 则默认为 {@link RowScanAction#CONTINUE}
     * @param obj 当前行对象
     * @param rowNum 当前行数,从1开始
     * @return 扫描行的行为
     */
    RowScanAction action(T obj, int rowNum);

}

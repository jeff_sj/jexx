package jexx.poi.row;

import jexx.poi.cell.IMergedCell;

/**
 * @author jeff
 * @since 2019/6/17
 */
public class VirtualRow extends Row {

    @Override
    public RowTypeEnum getRowType() {
        return RowTypeEnum.VIRTUAL;
    }

    @Override
    public void addCell(IMergedCell cell){
        assert cell.getRow() == this;
        this.cells.add(cell);
    }

}

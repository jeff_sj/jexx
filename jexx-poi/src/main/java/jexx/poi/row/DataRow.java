package jexx.poi.row;

import jexx.poi.cell.ICellSupportMeta;
import jexx.poi.cell.IMergedCell;
import jexx.poi.cell.MergedCell;
import jexx.poi.header.IDataHeader;
import jexx.poi.header.row.HeaderRow;
import jexx.util.Assert;
import jexx.util.CollectionUtil;

import javax.xml.crypto.Data;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * a row of data
 *
 * @author jeff
 * @since 2019/5/21
 */
public class DataRow extends Row {

    private String path = "";

    /**
     * row所代表的HeaderRow, 可为空
     */
    protected HeaderRow headerRow;

    /**
     * 列对应的单元格
     */
    private Map<Integer, IMergedCell> headerCellMap = new HashMap<>();

    @Override
    public RowTypeEnum getRowType() {
        return RowTypeEnum.DATA;
    }

    public DataRow() {
        this(-1);
    }

    public DataRow(int startRowNum) {
        this(startRowNum, startRowNum);
    }

    public DataRow(int startRowNum, int endRowNum) {
        Assert.isTrue(endRowNum >= startRowNum);
        this.startRowNum = startRowNum;
        this.endRowNum = endRowNum;
    }

    @Override
    protected void addCell(IMergedCell cell){
        this.headerCellMap.put(cell.getFirstColumnNum(), cell);
        super.addCell(cell);
    }

    /**
     * 添加指定行数的一行
     *
     * @param rowCount 指定行数
     */
    protected DataRow addDataRow(int rowCount){
        Assert.isTrue(rowCount > 0);

        DataRow row = new DataRow(endRowNum+1, endRowNum+rowCount);
        return addDataRow(row, rowCount);
    }

    /**
     * 添加一行
     */
    protected DataRow addDataRow(){
        return addDataRow(1);
    }

    protected DataRow addDataRow(DataRow row, int rowCount){
        assert row != null;

        Row last;
        if(CollectionUtil.isNotEmpty(this.children)){
            last = this.children.get(this.children.size()-1);
            last.setNextRow(row);
        }

        int startNum = findNextRowNumInChildren();
        int endNum = startNum + rowCount - 1;
        row.setStartRowNum(startNum);
        row.setEndRowNum(endNum);
        this.children.add(row);
        expandRow(this, endNum);
        return row;
    }

    protected VirtualRow appendVirtualRowAt(DataRow row){
        return appendVirtualRowAt(row, 1);
    }

    /**
     * 在指定数据行后追加虚拟行
     *
     * @param row 数据行
     */
    protected VirtualRow appendVirtualRowAt(DataRow row, int rowCount){
        assert rowCount > 0;
        assert hasRow(row);
        VirtualRow appendRow = new VirtualRow();
        appendRow.setStartRowNum(row.endRowNum+1);
        int appendEndRow = row.endRowNum+rowCount;
        appendRow.setEndRowNum(appendEndRow);
        children.add(appendRow);
        expandRow(this, appendEndRow);

        Row nextRow = row.nextRow;
        row.nextRow = appendRow;
        if(nextRow != null){
            appendRow.nextRow = nextRow;
            while(nextRow != null){
                nextRow.raiseRowNum(rowCount);
                nextRow = nextRow.nextRow;
            }
        }
        return appendRow;
    }

    protected VirtualRow addVirtualRow(int rowCount){
        assert rowCount > 0;

        VirtualRow appendRow = new VirtualRow();

        Row last;
        if(CollectionUtil.isNotEmpty(this.children)){
            last = this.children.get(this.children.size()-1);
            last.setNextRow(appendRow);
        }

        int startNum = findNextRowNumInChildren();
        int endNum = startNum + rowCount - 1;
        appendRow.setStartRowNum(startNum);
        appendRow.setEndRowNum(endNum);
        this.children.add(appendRow);
        expandRow(this, endNum);
        return appendRow;
    }


    /**
     * 修复空单元格
     */
    protected void repair(){
        if(headerRow != null){
            Map<Integer, IDataHeader> headerMap = headerRow.getHeaderMap();
            if(headerMap != null){
                headerMap.forEach((k,v)->{
                    IMergedCell cell = getCellByColumnNum(k);
                    if(cell == null){
                        MergedCell mergedCell = new MergedCell(this, v.getStartColumnNum(), v.getEndColumnNum(), null, v.getDataCellStyle());
                        mergedCell.setMeta(v.getMeta());
                        mergedCell.valid(v.isValid());

                        IDataHeader referHeader = v.getReferHeader();
                        //TODO parentRow 未空
                        if(referHeader != null && parentRow != null){
                            IMergedCell referCell = parentRow.getCellByColumnNum(referHeader.getStartColumnNum());
                            Assert.notNull(referCell, "Please check header[{}]", referHeader.getKey());
                            mergedCell.setCellReference((ICellSupportMeta)referCell);
                        }
                        addCell(mergedCell);
                    }
                });
            }

            HeaderRow child = headerRow.getChild();
            if(child != null){
                if(CollectionUtil.isEmpty(children)){
                    DataRow row = this.addDataRow(1);
                    row.setPath(child.getPath());
                    row.setHeaderRow(child);
                }
            }
        }

        if(children != null){
            for(Row row : children){
                if(row instanceof DataRow){
                    ((DataRow) row).repair();
                }
            }
        }
    }

    /**
     * 获取指定列的单元格, 指定列的单一单元格，默认向上查找
     *
     * @param columnNum 列号
     * @return 单元格
     */
    public IMergedCell getCellByColumnNum(int columnNum){
        IMergedCell cell = this.headerCellMap.get(columnNum);
        if(cell != null){
            return cell;
        }
        if(this.parentRow != null){
            return this.parentRow.getCellByColumnNum(columnNum);
        }
        return null;
    }

    /**
     * 获取指定列的多个单元格，向下查找
     *
     * @param columnNum 列号
     */
    protected List<IMergedCell> getCellsByColumnNum(int columnNum){
        List<IMergedCell> cells = new ArrayList<>();
        doRecursionRow(this, cells, columnNum);
        return cells;
    }

    private void doRecursionRow(DataRow row, List<IMergedCell> cells, int columnNum){
        if(row != null){
            if(row.headerCellMap.get(columnNum) != null){
                cells.add(row.headerCellMap.get(columnNum));
            }
            else{
                if(CollectionUtil.isNotEmpty(row.children)){
                    for(Row r : row.children){
                        if(r instanceof DataRow){
                            doRecursionRow((DataRow) r, cells, columnNum);
                        }
                    }
                }
            }

        }
    }

    public boolean hasHeaderData(int columnNum){
        return headerRow != null && headerRow.getHeaderMap().get(columnNum) != null;
    }

    public boolean hasHeaderDataInChild(int columnNum){
        return headerRow != null && headerRow.getChild() != null && headerRow.getChild().getHeaderMap().get(columnNum) != null;
    }

    public Map<Integer, IMergedCell> getHeaderCellMap() {
        return headerCellMap;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public void setHeaderRow(HeaderRow headerRow) {
        this.headerRow = headerRow;
    }
}

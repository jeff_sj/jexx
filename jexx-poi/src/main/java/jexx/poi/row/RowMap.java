package jexx.poi.row;

import java.util.HashMap;
import java.util.Map;

public class RowMap extends HashMap<String,Object> {

    private int rowNum;

    /** RowMap指示的结束行号 */
    private int endRowNum;

    /**
     * 构建RowMap
     * @param map map数据
     * @param rowNum 开始行号
     * @param endRowNum 结束行号
     * @return {@link RowMap}
     */
    public static RowMap of(Map<String,Object> map, int rowNum, int endRowNum){
        RowMap rowMap = new RowMap();
        rowMap.rowNum = rowNum;
        rowMap.endRowNum = endRowNum;
        rowMap.putAll(map);
        return rowMap;
    }

    /**
     * @see RowMap#of(Map, int, int)
     */
    public static RowMap of(Map<String,Object> map, int rowNum){
        return of(map, rowNum, rowNum);
    }


    public int getRowNum() {
        return rowNum;
    }

    public void setRowNum(int rowNum) {
        this.rowNum = rowNum;
    }

    public int getEndRowNum() {
        return endRowNum;
    }

    public void setEndRowNum(int endRowNum) {
        this.endRowNum = endRowNum;
    }

    /**
     * 浅克隆RowMap
     */
    public RowMap clone(){
        RowMap map = new RowMap();
        map.putAll(this);
        return map;
    }
}

package jexx.poi.row;

import jexx.poi.cell.IMergedCell;
import jexx.util.Assert;
import jexx.util.CollectionUtil;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * @author jeff
 * @since 2019/6/17
 */
public abstract class Row {

    /**
     * 开始行号
     */
    protected int startRowNum;

    /**
     * 结束行号
     */
    protected int endRowNum;

    /**
     * 行中第一列
     */
    protected int firstColumnNum = -1;

    /**
     * 行中最后一列
     */
    protected int lastColumnNum = -1;

    /**
     * 单元格
     */
    protected List<IMergedCell> cells = new CopyOnWriteArrayList<>();

    /**
     * 父行必定是数据行
     */
    protected DataRow parentRow;

    protected List<Row> children = new CopyOnWriteArrayList<>();

    protected Row nextRow;

    /**
     * 获取行类型
     */
    public abstract RowTypeEnum getRowType();

    protected void addCell(IMergedCell cell){
        assert cell.getRow() == this;
        this.cells.add(cell);

        if(firstColumnNum == -1){
            firstColumnNum = cell.getFirstColumnNum();
            lastColumnNum = cell.getLastColumnNum();
        }
        else{
            firstColumnNum = Math.min(firstColumnNum, cell.getFirstColumnNum());
            lastColumnNum =  Math.max(lastColumnNum, cell.getLastColumnNum());
        }
    }

    /**
     * 扩张行到指定行号
     *
     * @param row 行
     * @param endNum 结束行号
     */
    protected void expandRow(Row row, int endNum){
        assert row.getStartRowNum() <= endNum;
        if(row.getEndRowNum() >= endNum){
            return;
        }

        int rowCount = endNum - row.getEndRowNum();
        row.setEndRowNum(endNum);

        Row nextRow = row.getNextRow();
        while(nextRow != null){
            nextRow.raiseRowNum(rowCount);
            nextRow = nextRow.getNextRow();
        }

        if(row.getParentRow() != null){
            expandRow(row.getParentRow(), endNum);
        }
    }

    /**
     * 找到子行中最大行的下一行号
     */
    protected int findNextRowNumInChildren(){
        int maxRowNum = endRowNum;
        if(CollectionUtil.isNotEmpty(this.children)){
            Row child = this.children.get(this.children.size()-1);
            Assert.isTrue(child.getEndRowNum() >= maxRowNum);
            maxRowNum = child.getEndRowNum()+1;
        }
        return maxRowNum;
    }

    /**
     * 增加本行的起始行号
     *
     * @param rowCount 行数
     */
    protected void raiseRowNum(int rowCount){
        this.startRowNum = this.startRowNum + rowCount;
        this.endRowNum = this.endRowNum + rowCount;

        if(this.parentRow != null){
            expandRow(this.parentRow, this.endRowNum);
        }

        if(CollectionUtil.isNotEmpty(this.children)){
            for(Row r : this.children){
                r.raiseRowNum(rowCount);
            }
        }
    }

    public boolean hasRow(Row row){
        if(CollectionUtil.isNotEmpty(children)){
            return children.stream().anyMatch(s -> s == row);
        }
        return false;
    }

    public List<IMergedCell> getCells() {
        return cells;
    }



    public int getStartRowNum() {
        return startRowNum;
    }

    protected void setStartRowNum(int startRowNum) {
        this.startRowNum = startRowNum;
    }

    public int getEndRowNum() {
        return endRowNum;
    }

    protected void setEndRowNum(int endRowNum) {
        this.endRowNum = endRowNum;
    }

    public Row getParentRow() {
        return parentRow;
    }

    public void setParentRow(DataRow parentRow) {
        this.parentRow = parentRow;
    }

    public List<Row> getChildren() {
        return children;
    }

    public Row getNextRow() {
        return nextRow;
    }

    protected void setNextRow(Row row) {
        this.nextRow = row;
    }

    public int getFirstColumnNum() {
        return firstColumnNum;
    }

    public int getLastColumnNum() {
        return lastColumnNum;
    }
}

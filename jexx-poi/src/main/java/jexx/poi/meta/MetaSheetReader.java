package jexx.poi.meta;

import jexx.base.Tuple2;
import jexx.convert.Convert;
import jexx.poi.exception.POIException;
import jexx.poi.header.*;
import jexx.poi.meta.node.INode;
import jexx.poi.meta.node.Node;
import jexx.poi.meta.node.TreeNode;
import jexx.poi.util.CellOperateUtil;
import jexx.poi.util.RowOperationUtil;
import jexx.time.DateUtil;
import jexx.util.Assert;
import jexx.util.ClassLoaderUtil;
import jexx.util.CollectionUtil;
import jexx.util.StringUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * meta信息读取
 * @author jeff
 */
public class MetaSheetReader extends AbstractMetaSheet implements IMetaReader {

    private static final Logger LOG = LoggerFactory.getLogger(MetaSheetReader.class);

    protected Metas excelMeta;

    public MetaSheetReader(Sheet sheet) {
        this.sheet = sheet;
    }

    public Metas readMeta() {
        excelMeta = new Metas();
        String cellValue;;
        Row row;
        Iterator<Row> rowIterator = this.sheet.iterator();
        while(rowIterator.hasNext()){
            row = rowIterator.next();
            cellValue = CellOperateUtil.getStringCellValue(row.getCell(0));
            if (StringUtil.isEmpty(cellValue)) {
                break;
            }
            if (INFO_START.equals(cellValue)) {
                readInfo(excelMeta, rowIterator);
            }
            else if (METADATA_START.equals(cellValue)) {
                String metaType = CellOperateUtil.getStringCellValue(row.getCell(1));
                DVConstraintType constraintType = DVConstraintType.valueOf(metaType);
                if (DVConstraintType.ARRAY == constraintType) {
                    ArrayMeta arrayMeta = readArrayMeta(rowIterator);
                    excelMeta.addMeta(arrayMeta);
                } else if (DVConstraintType.TREE == constraintType) {
                    TreeMeta treeMeta = readTreeMeta(rowIterator);
                    excelMeta.addMeta(treeMeta);
                } else {
                    throw new IllegalArgumentException(StringUtil.format("constraintType[{}] can't handle", metaType));
                }
            }
            else if (HEADER_START.equals(cellValue)) {
                String headerName = CellOperateUtil.getStringCellValue(row.getCell(1));
                Headers headers = readerHeaders(headerName, rowIterator);
                excelMeta.addHeaders(headers);
            }
        }
        return excelMeta;
    }

    /**
     * 读取excel信息
     */
    private void readInfo(Metas metas, Iterator<Row> rowIterator){
        Map<String, String> customProperties = new HashMap<>(16);
        Row row;
        while(rowIterator.hasNext()){
            row = rowIterator.next();
            if(row == null){
                throw new POIException("meta 格式异常,读取不到下一行数据");
            }
            String value1 = CellOperateUtil.getStringCellValue(row.getCell(0));
            if(END.equals(value1)){
                break;
            }
            if(StringUtil.isEmpty(value1) || value1.startsWith(PREFIX)){
                throw new POIException("meta 格式异常, value1="+value1);
            }
            String value2 = CellOperateUtil.getStringCellValue(row.getCell(1));
            if(Metas.META_VERSION.equals(value1)){
                metas.setVersion(value2);
            }
            else if(Metas.META_CREATE_TIME.equals(value1)){
                if(StringUtil.isNotEmpty(value2)){
                    try{
                        Date createTime = DateUtil.parseDateTime(value2);
                        metas.setCreateTime(createTime);
                    }
                    catch (Exception e){
                        LOG.warn("", e);
                    }
                }
            }
            else{
                customProperties.put(value1, value2);
            }
        }
        metas.addProperty(customProperties);
    }



    private ArrayMeta readArrayMeta(Iterator<Row> rowIterator) {
        Assert.isTrue(rowIterator.hasNext(), "no value row");
        Row valueRow = rowIterator.next();
        Assert.isTrue(rowIterator.hasNext(), "no label row");
        Row labelRow = rowIterator.next();
        Tuple2<String, List<INode>> tuple2 = readValue(valueRow, labelRow);

        Assert.isTrue(rowIterator.hasNext(), "array no end row");
        Row endRow = rowIterator.next();
        String endTag = CellOperateUtil.getStringCellValue(endRow.getCell(0));
        Assert.isTrue(END.equals(endTag), "ArrayMeta must has end tag!");
        return new ArrayMeta(tuple2.getFirst(), tuple2.getSecond());
    }

    private TreeMeta readTreeMeta(Iterator<Row> rowIterator) {
        Map<String, TreeNode> treeMap = new HashMap<>(16);

        Row row;
        String endTag;
        String name;
        TreeNode tree;
        TreeNode rootTree = null;
        while (rowIterator.hasNext()) {
            row = rowIterator.next();
            if (row == null) {
                break;
            }
            endTag = CellOperateUtil.getStringCellValue(row.getCell(0));
            if (END.equals(endTag)) {
                break;
            }
            Assert.isTrue(rowIterator.hasNext(), "tree has no label row");
            Row labelRow = rowIterator.next();
            Tuple2<String, List<INode>> tuple2 = readValue(row, labelRow);

            String treeName = tuple2.getFirst();

            int lastIndex = treeName.lastIndexOf("_");
            name = treeName.substring(lastIndex + 1);

            tree = treeMap.get(treeName);
            if(tree == null){
                tree = new TreeNode(name);
                if(rootTree == null){
                    tree.setRoot(true);
                    rootTree = tree;
                }
                treeMap.put(treeName, tree);
            }

            if(CollectionUtil.isNotEmpty(tuple2.getSecond())){
                List<TreeNode> children = tree.getChildren();
                if(children == null){
                    children = new ArrayList<>();
                    tree.setChildren(children);
                }
                for(INode node : tuple2.getSecond()){
                    TreeNode tNode = new TreeNode(node.getValue(), node.getLabel());
                    tNode.setParent(tree);
                    String fullLabel = treeName.concat(AbstractMeta.SEPARATOR).concat(node.getLabel() != null ? node.getLabel().toString() : "");
                    treeMap.putIfAbsent(fullLabel, tNode);
                    children.add(tNode);
                }
            }
        }
        if(rootTree == null){
            throw new NullPointerException("Root tree not find!");
        }

        repairTree(rootTree);
        return new TreeMeta(rootTree.getFullLabel(), rootTree);
    }

    private Headers readerHeaders(String headerName, Iterator<Row> rowIterator) {
        Row row;
        String endTag;
        Headers headers = new Headers(headerName);
        while (rowIterator.hasNext()) {
            row = rowIterator.next();
            if (row == null) {
                break;
            }
            endTag = CellOperateUtil.getStringCellValue(row.getCell(0));
            if (END.equals(endTag)) {
                break;
            }
            List<Object> list = RowOperationUtil.readRow(row);
            Assert.notNull(list);
            Assert.isTrue(list.size() >= 4);
            String key = list.get(0).toString();
            String value = list.get(1).toString();

            DVConstraintType dvConstraintType = DVConstraintType.valueOf(list.get(3).toString());
            if(DVConstraintType.DEFAULT == dvConstraintType){
                DefaultDataHeader header = new DefaultDataHeader(key, value);
                loadHeader(header, list);
                headers.addHeader(header);
            }
            else if(DVConstraintType.ARRAY == dvConstraintType){
                Assert.isTrue(list.size() >= 5, "headerName={},key={}", headerName, key);
                String metaName = list.get(4).toString();
                IMeta meta = excelMeta.getMeta(metaName);
                Assert.notNull(meta,"not find meta[{}]", metaName);
                Assert.isInstanceOf(ArrayMeta.class, meta);
                ArrayMeta arrayMeta = (ArrayMeta) meta;

                ArrayDataHeader arrayHeader = new ArrayDataHeader(key, value, arrayMeta);
                loadHeader(arrayHeader, list);
                headers.addHeader(arrayHeader);
            }
            else if(DVConstraintType.TREE == dvConstraintType){
                Assert.isTrue(list.size() >= 6, "headerName={},key={}", headerName, key);
                String metaName = list.get(4).toString();
                IMeta meta = excelMeta.getMeta(metaName);
                Assert.notNull(meta,"not find meta[{}]", metaName);
                Assert.isInstanceOf(TreeMeta.class, meta);
                TreeMeta treeMeta = (TreeMeta) meta;

                short referHeaderColumnNum = Convert.toShort(list.get(5), (short)-1);
                IDataHeader referHeader = headers.getDataHeaderByColumnNum(referHeaderColumnNum);
                TreeDataHeader treeHeader = new TreeDataHeader(key, value, treeMeta, referHeader);
                loadHeader(treeHeader, list);

                headers.addHeader(treeHeader);
            }
            else{
                throw new POIException("not right for header");
            }
        }
        return headers;
    }

    /**
     * 读取两行数据
     */
    private Tuple2<String, List<INode>> readValue(Row valueRow, Row labelRow) {
        String name = CellOperateUtil.getStringCellValue(valueRow.getCell(0));

        short lastCellNum = valueRow.getLastCellNum();
        List<INode> values = new ArrayList<>();
        for (int i = 1; i <= lastCellNum; i++) {
            Object value = CellOperateUtil.getCellValue(valueRow.getCell(i));
            if (value == null) {
                break;
            }
            Object label = CellOperateUtil.getCellValue(labelRow.getCell(i));

            Node node = new Node(value, label);
            values.add(node);
        }
        return new Tuple2<>(name, values);
    }


    /**
     * 修复树中叶子节点
     */
    protected void repairTree(TreeNode treeNode){
        if(CollectionUtil.isEmpty(treeNode.getChildren())){
            treeNode.setLeaf(true);
            return;
        }
        for(TreeNode c : treeNode.getChildren()){
            repairTree(c);
        }
    }

    /**
     * 完善 header 公共部分数据
     * @param header header
     * @param list 解析所需要的数据
     */
    protected void loadHeader(AbstractDataHeader<?> header, List<Object> list){
        Assert.notNull(list);
        Assert.isTrue(list.size() >= 4);

        short columnIndex = Convert.toShort(list.get(2));
        header.setStartColumnNum(columnIndex);
        header.setEndColumnNum(columnIndex);

        if(list.size() >= 7){
            Object multiValueObj = list.get(6);
            if(multiValueObj != null){
                String multiValueStr = multiValueObj.toString();
                if(StringUtil.isNotEmpty(multiValueStr)){
                    String[] arr = StringUtil.split(multiValueStr, ",");
                    boolean isMultiValue = arr.length >= 1 && "1".equals(arr[0]);
                    if(isMultiValue){
                        Class<?> multiValueCollectionType = arr.length >= 2 ? ClassLoaderUtil.loadClass(arr[1]) : null;
                        Class<?> multiValueElementType = arr.length >= 3 ? ClassLoaderUtil.loadClass(arr[2]) : null;
                        header.withMultiValue(multiValueCollectionType, multiValueElementType);
                    }
                }
            }
        }
    }

}

package jexx.poi.meta;

import jexx.exception.UtilException;
import jexx.poi.exception.POIException;
import jexx.poi.header.Headers;
import jexx.poi.meta.node.INode;
import jexx.poi.meta.node.TreeNode;
import jexx.poi.util.CellOperateUtil;
import jexx.poi.util.NameUtil;
import jexx.poi.util.RowOperationUtil;
import jexx.time.DateUtil;
import jexx.util.CollectionUtil;
import jexx.util.MapUtil;
import jexx.util.StringPool;
import jexx.util.StringUtil;
import org.apache.poi.ss.usermodel.Name;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * meta writer
 *
 * @author jeff
 */
public class MetaSheetWriter extends AbstractMetaSheet implements IMetaWriter {

    private int rowNum = 1;

    public MetaSheetWriter(Sheet sheet) {
        this.sheet = sheet;
    }

    @Override
    public void save(Metas excelMeta){
        rowNum = 1;

        saveInfo(excelMeta);

        List<IMeta> metas = excelMeta.getMetas();
        if(CollectionUtil.isNotEmpty(metas)){
            for(IMeta meta : metas){
                save(meta);
            }
        }

        List<Headers> headersList = excelMeta.getHeaders();
        if(CollectionUtil.isNotEmpty(headersList)){
            for(Headers header : headersList){
                saveHeaders(header);
            }
        }
    }

    /**
     * 写入信息
     */
    private void saveInfo(Metas metas){
        RowOperationUtil.writeData(createRow(sheet, rowNum++), CollectionUtil.list(INFO_START));
        if(metas != null){
            RowOperationUtil.writeData(createRow(sheet, rowNum++), CollectionUtil.list(Metas.META_VERSION, metas.getVersion()));
            RowOperationUtil.writeData(createRow(sheet, rowNum++), CollectionUtil.list(Metas.META_CREATE_TIME, DateUtil.formatDateTime(metas.getCreateTime())));
            Map<String, String> properties = metas.getCustomProperties();
            if(MapUtil.isNotEmpty(properties)){
                properties.forEach((k,v)->{
                    if(k.startsWith(PREFIX)){
                        throw new POIException("excel中meta自定义信息格式不对,key="+k);
                    }
                    RowOperationUtil.writeData(createRow(sheet, rowNum++), CollectionUtil.list(k, v));
                });
            }
        }
        RowOperationUtil.writeData(createRow(sheet, rowNum++), CollectionUtil.list(END));
    }

    private void save(IMeta meta){
        if(meta instanceof ArrayMeta){
            RowOperationUtil.writeData(createRow(sheet, rowNum++), CollectionUtil.list(METADATA_START, "ARRAY"));

            ArrayMeta listMeta = (ArrayMeta)meta;
            List<Object> values = listMeta.getValueMetas().stream().map(INode::getValue).collect(Collectors.toList());
            RowOperationUtil.writeMeta(createRow(sheet, rowNum++), meta.getName(), values);

            List<Object> labels = listMeta.getValueMetas().stream().map(INode::getLabel).collect(Collectors.toList());
            RowOperationUtil.writeMeta(createRow(sheet, rowNum++), "", labels);

            Name name = sheet.getWorkbook().createName();
            String nameName = NameUtil.encodeNameName(meta.getName());
            name.setNameName(nameName);
            String formula = StringUtil.format("{}!${}${}:${}${}", sheet.getSheetName(), "B", rowNum-1,
                    CellOperateUtil.toColumnLabel(listMeta.getValueMetas().size()+1), rowNum-1);
            name.setRefersToFormula(formula);
        }
        else if(meta instanceof TreeMeta){
            RowOperationUtil.writeData(createRow(sheet, rowNum++), CollectionUtil.list(METADATA_START, "TREE"));

            TreeMeta treeMeta = (TreeMeta)meta;
            writeTreeMeta(treeMeta.getTree());
        }
        else{
            throw new UtilException("no right meta for this meta[{}]", meta);
        }
        RowOperationUtil.writeData(createRow(sheet, rowNum++), CollectionUtil.list(END));
    }

    private void writeTreeMeta(TreeNode treeNode){
        if(CollectionUtil.isNotEmpty(treeNode.getChildren())){
            String label = treeNode.getFullLabel();

            List<Object> values = treeNode.getChildren().stream().map(TreeNode::getValue).collect(Collectors.toList());
            RowOperationUtil.writeMeta(createRow(sheet, rowNum++), label, values);

            List<Object> labels = treeNode.getChildren().stream().map(TreeNode::getLabel).collect(Collectors.toList());
            RowOperationUtil.writeMeta(createRow(sheet, rowNum++), "", labels);

            Name name = sheet.getWorkbook().createName();
            String nameName = NameUtil.encodeNameName(label);
            name.setNameName(nameName);
            String formula = StringUtil.format("{}!${}${}:${}${}", sheet.getSheetName(), "B", rowNum-1,
                    CellOperateUtil.toColumnLabel(treeNode.getChildren().size()+1), rowNum-1);
            name.setRefersToFormula(formula);

            for (TreeNode child : treeNode.getChildren()){
                writeTreeMeta(child);
            }
        }
    }

    private void saveHeaders(Headers headers){
        RowOperationUtil.writeData(createRow(sheet, rowNum++), CollectionUtil.list(HEADER_START, headers.getName()));
        if(CollectionUtil.isNotEmpty(headers.getDataHeaders())){
            headers.getDataHeaders().forEach(s -> {
                DVConstraintType dvConstraintType = s.getDVConstraintType();
                String metaName = StringPool.EMPTY;
                if(s.getMeta() != null){
                    metaName = s.getMeta().getName();
                }
                String referHeaderIndex = s.getReferHeader() != null ? Integer.toString(s.getReferHeader().getStartColumnNum()) : StringPool.EMPTY;
                //G: 多值属性
                String multiValue = "0";
                if(s.isMultiValue()){
                    Class<?> multiValueCollectionType = s.getValueCollectionType();
                    Class<?> multiValueElementType = s.getValueElementType();
                    multiValue = "1,".concat(multiValueCollectionType.getTypeName()).concat(",")
                            .concat(multiValueElementType != null ? multiValueElementType.getTypeName() : "");
                }
                RowOperationUtil.writeData(createRow(sheet, rowNum++), CollectionUtil.list(s.getKey(), s.getValue(), s.getStartColumnNum(), dvConstraintType, metaName, referHeaderIndex, multiValue));
            });
        }
        RowOperationUtil.writeData(createRow(sheet, rowNum++), CollectionUtil.list(END));
    }

    protected Row createRow(Sheet sheet, int rowNum){
        return sheet.createRow(rowNum-1);
    }

}

package jexx.poi.meta.node;

/**
 * 单例对象
 */
public class SingletonNode implements INode {

    private static final SingletonNode node = new SingletonNode();

    protected Object label;
    protected Object value;

    private SingletonNode(){}

    public static SingletonNode of(Object value, Object label){
        node.value = value;
        node.label = label;
        return node;
    }

    public static SingletonNode of(Object value){
        node.value = value;
        node.label = value;
        return node;
    }

    @Override
    public Object getValue() {
        return value;
    }

    @Override
    public Object getLabel() {
        return label;
    }
}

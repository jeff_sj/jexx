package jexx.poi.meta.node;

public class NullNode extends Node {

    private static final NullNode node = new NullNode();

    public NullNode() {
        super(null);
    }

    public static final NullNode instance(){
        return node;
    }

}

package jexx.poi.meta.node;

public class Node implements INode {

    protected Object label;
    protected Object value;

    public Node(Object value) {
        this.value = value;
        this.label = value;
    }

    public Node(Object value, Object label) {
        this.value = value;
        this.label = label;
    }

    @Override
    public Object getLabel() {
        return label;
    }

    @Override
    public Object getValue() {
        return value;
    }
}

package jexx.poi.meta.node;

/**
 *
 * node builder
 *
 * @author jeff
 * @since 2019/5/7
 */
public class NodeBuilder {

    public static INode buildNode(Object value){
        return new Node(value);
    }

}

package jexx.poi.meta.node;

/**
 * node interface
 *
 * @author jeff
 */
public interface INode {

    /**
     * excel中实际值
     *
     * @return value
     */
    Object getValue();

    /**
     * excel中显示名称,非包装显示名称;
     * @see jexx.poi.header.label.LabelWrapFunction 包装显示名称
     *
     * @return label
     */
    Object getLabel();

}

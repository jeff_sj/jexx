package jexx.poi.meta.node;

import jexx.poi.meta.AbstractMeta;
import jexx.poi.util.NameUtil;
import jexx.util.Assert;
import jexx.util.ObjectUtil;

import java.util.List;
import java.util.Stack;

/**
 * tree node
 *
 * @author jeff
 */
public class TreeNode implements INode {

    public static final String ROOT_NODE_NAME = "RooT";

    private final Object value;
    private Object label;
    private TreeNode parent;
    private List<TreeNode> children;
    private boolean isRoot;
    private boolean isLeaf;

    public TreeNode(Object value) {
        this(value, value);
    }

    public TreeNode(Object value, Object label) {
        this(value, label, null, null);
    }

    public TreeNode(Object value, Object label, TreeNode parent, List<TreeNode> children) {
        Assert.notNull(value);
        Assert.notNull(label);
        this.value = value;
        this.label = label;
        this.parent = parent;
        this.children = children;
    }

    /**
     * 创建根节点
     */
    public static TreeNode createRootTreeNode(){
        return createRootTreeNode(ROOT_NODE_NAME);
    }

    public static TreeNode createRootTreeNode(String name){
        TreeNode root = new TreeNode(name);
        root.isRoot = true;
        return root;
    }

    /**
     * 由树节点中各个值组成的全路径, 如 部门_1_11_112
     */
    public String getFullValue(){
        Stack<String> stack = new Stack<>();
        TreeNode p = this;
        while((p = p.parent) != null){
            Object l = p.getValue();
            stack.add(l == null ? "" : l.toString());
        }

        StringBuilder valueBuilder = new StringBuilder();
        String s;
        while(!stack.isEmpty()){
            s = stack.pop();
            valueBuilder.append(s).append(AbstractMeta.SEPARATOR);
        }
        valueBuilder.append(this.value);
        return valueBuilder.toString();
    }

    /**
     * 树节点代表的全路径名称, 如 部门_部门1_职位11_姓名112
     */
    public String getFullLabel(){
        Stack<String> stack = new Stack<>();
        TreeNode p = this;
        while((p = p.parent) != null){
            Object l = p.getLabel();
            stack.add(l == null ? "" : NameUtil.replaceIllegalCharForName(l.toString()));
        }

        StringBuilder valueBuilder = new StringBuilder();
        String s;
        while(!stack.isEmpty()){
            s = stack.pop();
            valueBuilder.append(s).append(AbstractMeta.SEPARATOR);
        }

        Object l = ObjectUtil.notNullOrDefault(getLabel(), "");
        valueBuilder.append(l.toString());
        return valueBuilder.toString();
    }

    @Override
    public Object getValue() {
        return value;
    }

    @Override
    public Object getLabel() {
        if(this.label == null){
            return null;
        }
        return isLeaf ? this.label : NameUtil.replaceIllegalCharForName(this.label.toString());
    }

    public void setLabel(Object label) {
        this.label = label;
    }

    public TreeNode getParent() {
        return parent;
    }

    public void setParent(TreeNode parent) {
        this.parent = parent;
    }

    public List<TreeNode> getChildren() {
        return children;
    }

    public void setChildren(List<TreeNode> children) {
        this.children = children;
    }

    public boolean isRoot() {
        return isRoot;
    }

    public void setRoot(boolean root) {
        isRoot = root;
    }

    public boolean isLeaf() {
        return isLeaf;
    }

    public void setLeaf(boolean leaf) {
        isLeaf = leaf;
    }

    @Override
    public int hashCode() {
        int code = getFullValue().hashCode();
        code = 31 * code;
        return code;
    }

    @Override
    public boolean equals(Object o) {
        if(o instanceof TreeNode){
            TreeNode another = (TreeNode)o;
            return this.getFullLabel().equals(another.getFullValue());
        }
        return false;
    }
}

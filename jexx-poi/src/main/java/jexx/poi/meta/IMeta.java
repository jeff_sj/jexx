package jexx.poi.meta;

import jexx.poi.meta.node.INode;

/**
 * meta interface
 *
 * @author jeff
 */
public interface IMeta {

    /**
     * meta's name
     *
     * @return name
     */
    String getName();

    /**
     * 当containValue为true时,可以调用该方法获取到ValueMeta信息
     * @param fullValue 数据中的唯一值
     * @return Node
     */
    INode getNodeByFullValue(Object fullValue);

    INode getNodeByFullLabel(Object fullLabel);

}

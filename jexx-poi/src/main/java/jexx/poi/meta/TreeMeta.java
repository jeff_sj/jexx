package jexx.poi.meta;

import jexx.poi.meta.aspect.ExcelMeta;
import jexx.poi.meta.aspect.ExcelMetaChildren;
import jexx.poi.meta.aspect.ExcelMetaLabel;
import jexx.poi.meta.aspect.ExcelMetaValue;
import jexx.poi.meta.node.INode;
import jexx.poi.meta.node.TreeNode;
import jexx.util.Assert;
import jexx.util.CollectionUtil;
import jexx.util.ReflectUtil;
import jexx.util.StringUtil;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 注意：在构建树形结构数据时，请确保ValueMeta中的value非空且具有唯一性
 *
 * @author jeff
 */
public class TreeMeta extends AbstractMeta{

    private final String name;
    private final TreeNode tree;

    /**
     * 根据key来查找Tree; key为tree的value和type的组合, 必须保证在同一个type下value唯一,
     * 否则值会被覆盖
     */
    private Map<Object, TreeNode> fullValueMap;
    /**
     * 根据名称来查找Tree; key为tree的name,非label
     */
    private Map<Object, TreeNode> fullLabelMap;

    public TreeMeta(String name, TreeNode tree) {
        this.name = name;
        this.tree = tree;
        Assert.notNull(name);
        Assert.notNull(tree);
        fullValueMap = new HashMap<>();
        fullLabelMap = new HashMap<>();
        fillFullMap(tree, fullValueMap, fullLabelMap);
    }

    private void fillFullMap(TreeNode tree, Map<Object, TreeNode> fullValueMap, Map<Object, TreeNode> fullLabelMap){
        fullValueMap.putIfAbsent(tree.getFullValue(), tree);
        fullLabelMap.putIfAbsent(tree.getFullLabel(), tree);
        if(CollectionUtil.isNotEmpty(tree.getChildren())){
            for(TreeNode child : tree.getChildren()){
                fillFullMap(child, fullValueMap, fullLabelMap);
            }
        }
    }


    /**
     * 通过注解来构建TreeMeta
     *
     * @see TreeMeta#buildMetaWithAnnotation(String, Object, int)
     */
    public static <T> TreeMeta buildMetaWithAnnotation(String name, T obj){
        return buildMetaWithAnnotation(name, obj, 0);
    }

    /**
     * 根据对象构建meta信息
     *
     * @param obj 数据对象
     * @param maxLevel 遍历父子结构的最大层级; 0 为无限制
     * @return 树结构meta
     */
    public static <T> TreeMeta buildMetaWithAnnotation(String name, T obj, int maxLevel){
        return buildMetaWithAnnotation(name, CollectionUtil.list(obj), maxLevel);
    }

    /**
     * 通过注解来构建TreeMeta
     *
     * @see TreeMeta#buildMetaWithAnnotation(String, List, int)
     * @param name TreeMeta名称
     * @param list 构建对象
     * @return {@link TreeMeta}
     */
    public static <T> TreeMeta buildMetaWithAnnotation(String name, List<T> list){
        return buildMetaWithAnnotation(name, list, 0);
    }

    /**
     * 通过注解来构建TreeMeta
     *
     * @param list 集合数据
     * @param maxLevel 遍历父子结构的最大层级; 0 为无限制
     * @return 树形meta
     */
    public static <T> TreeMeta buildMetaWithAnnotation(String name, List<T> list, int maxLevel){
        Assert.isTrue(maxLevel >= 0);
        Assert.notEmpty(list);

        Map<Object, TreeNode> valueMap = new HashMap<>(16);
        Map<Object, TreeNode> labelMap = new HashMap<>(16);

        TreeNode root = TreeNode.createRootTreeNode(name);
        root.setChildren(new ArrayList<>());
        for(T obj : list){
            TreeNode child = buildTreeWithAnnotation(obj, root, labelMap, valueMap, 1, maxLevel);
            root.getChildren().add(child);
        }

        TreeMeta meta = new TreeMeta(StringUtil.str(root.getLabel()), root);
        meta.fullLabelMap = labelMap;
        meta.fullValueMap = valueMap;

        meta.checkNode();
        return meta;
    }

    private static TreeNode buildTreeWithAnnotation(Object obj, TreeNode parent, Map<Object, TreeNode> fullLabelMap, Map<Object, TreeNode> fullValueMap, int level, int maxLevel) {
        ExcelMeta excelMeta = ReflectUtil.getAnnotationOfClass(obj.getClass(), ExcelMeta.class);
        Assert.notNull(excelMeta, "Object don't contain the annotation ExcelMeta");

        List<Field> valueFields = ReflectUtil.getFieldsByAnnotation(obj.getClass(), ExcelMetaValue.class);
        Assert.isTrue(valueFields.size() == 1 , "Object has zero or more than one value field!");
        Field valueField = valueFields.get(0);
        Object value = ReflectUtil.getFieldValue(obj, valueField);

        Object label;
        List<Field> labelFields = ReflectUtil.getFieldsByAnnotation(obj.getClass(), ExcelMetaLabel.class);
        if(CollectionUtil.isNotEmpty(labelFields)){
            Field labelField = labelFields.get(0);
            label = ReflectUtil.getFieldValue(obj, labelField);
        }
        else{
            label = value;
        }

        TreeNode tree = new TreeNode(value, label);
        tree.setLabel(label);
        tree.setParent(parent);

        if(maxLevel == 0 || level < maxLevel){
            List<Field> childrenFields = ReflectUtil.getFieldsByAnnotation(obj.getClass(), ExcelMetaChildren.class);
            Assert.isTrue(childrenFields.size() <= 1, "Object has more than one children field!");
            if(CollectionUtil.isNotEmpty(childrenFields)){
                Assert.isTrue(childrenFields.size() == 1, "Object has more than one children field!");
                Field childrenField = childrenFields.get(0);
                Assert.isTrue(childrenField.getType().isAssignableFrom(List.class), "Children only support list!");

                Object children = ReflectUtil.getFieldValue(obj, childrenField);
                if(children == null){
                    tree.setLeaf(true);
                }
                else{
                    List<?> cds = (List<?>)children;
                    List<TreeNode> trees = new ArrayList<>();
                    for(Object item : cds){
                        TreeNode child = buildTreeWithAnnotation(item, tree, fullLabelMap, fullValueMap, level+1, maxLevel);
                        trees.add(child);
                    }
                    tree.setChildren(trees);
                    tree.setLeaf(false);
                }
            }
            else{
                tree.setLeaf(true);
            }
        }
        else{
            tree.setLeaf(true);
        }

        fullValueMap.putIfAbsent(tree.getFullValue(), tree);
        fullLabelMap.putIfAbsent(tree.getFullLabel(), tree);
        return tree;
    }

    /**
     * check whether the tree node is valid
     */
    public void checkNode(){
        if(tree != null){
            doCheckName(tree);
        }
    }

    /**
     * check repeated label at one node.
     *
     * @param node parent node
     */
    private void doCheckName(TreeNode node){
        List<TreeNode> children = node.getChildren();
        if(CollectionUtil.isNotEmpty(children)){
            Map<Object, TreeNode> map = new HashMap<>(children.size());
            for(TreeNode treeNode : children){
                Assert.isNull(map.get(treeNode.getLabel()), "There are same label[{}] at TreeNode[{}]", treeNode.getLabel(), node.getFullLabel());
                map.put(treeNode.getLabel(), treeNode);
                doCheckName(treeNode);
            }
        }
    }



    @Override
    public String getName() {
        return name;
    }

    @Override
    public INode getNodeByFullValue(Object fullValue){
        return fullValueMap.get(fullValue);
    }

    @Override
    public INode getNodeByFullLabel(Object fullLabel) {
        return fullLabelMap.get(fullLabel);
    }

    public TreeNode getTree() {
        return tree;
    }

}

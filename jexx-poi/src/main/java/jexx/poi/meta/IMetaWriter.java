package jexx.poi.meta;

/**
 * meta信息写入
 *
 * @author jeff
 */
public interface IMetaWriter {

    /**
     * 保存meta信息
     * @param excelMeta excel的meta信息
     */
    void save(Metas excelMeta);

}

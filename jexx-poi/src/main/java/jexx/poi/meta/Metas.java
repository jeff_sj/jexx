package jexx.poi.meta;

import jexx.poi.header.Headers;
import jexx.util.Assert;
import jexx.util.CollectionUtil;
import jexx.util.MapUtil;

import java.util.*;

/**
 * excel中的元信息描述
 *
 * @author jeff
 */
public class Metas {

    public static final String META_VERSION = "META_VERSION";
    public static final String META_CREATE_TIME = "META_CREATE_TIME";

    /** 版本号 */
    private String version;
    /** 创建时间 */
    private Date createTime;
    /** 自定义属性 */
    private final Map<String, String> customProperties;

    private final Map<String, IMeta> metaMap;
    private final Map<String, Headers> headersMap;

    public Metas() {
        this("");
    }

    public Metas(String version) {
        this.metaMap = new HashMap<>(16);
        this.headersMap = new HashMap<>(16);

        this.version = version;
        this.createTime = new Date();
        this.customProperties = new HashMap<>(4);
    }

    public String getVersion(){
        return version;
    }

    public void setVersion(String version){
        this.version = version;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Map<String, String> getCustomProperties() {
        return Collections.unmodifiableMap(customProperties);
    }

    public void addProperty(String key, String value){
        if(META_VERSION.equals(key) || META_CREATE_TIME.equals(key)){
            throw new IllegalArgumentException("cannot use property " + key);
        }
        customProperties.put(key, value);
    }

    public void addProperty(Map<String, String> properties){
        if(CollectionUtil.isNotEmpty(properties)){
            properties.forEach(this::addProperty);
        }
    }

    public void removeProperty(String key){
        if(META_VERSION.equals(key) || META_CREATE_TIME.equals(key)){
            throw new IllegalArgumentException("cannot remove property " + key);
        }
        customProperties.remove(key);
    }

    public String getProperty(String key){
        if(META_VERSION.equals(key) || META_CREATE_TIME.equals(key)){
            throw new IllegalArgumentException("cannot remove property " + key);
        }
        return customProperties.get(key);
    }

    public Metas addMeta(IMeta meta){
        Assert.hasText(meta.getName(), "Meta's label is not empty!");
        Assert.isNull(metaMap.get(meta.getName()), "Meta[{}] exist!", meta.getName());
        metaMap.put(meta.getName(), meta);
        return this;
    }

    public Metas addHeaders(Headers headers){
        Assert.isNull(headersMap.get(headers.getName()), "headers[{}] 名字重复", headers.getName());
        headersMap.putIfAbsent(headers.getName(), headers);

        if(!headers.isFlushed()){
            headers.flush();
        }
        return this;
    }

    public IMeta getMeta(String label){
        return metaMap.get(label);
    }

    public List<IMeta> getMetas(){
        return  MapUtil.convertValueToList(metaMap);
    }

    public Headers getHeadersByName(String name){
        return headersMap.get(name);
    }

    public List<Headers> getHeaders(){
        return  MapUtil.convertValueToList(headersMap);
    }

}

package jexx.poi.meta;

import jexx.poi.meta.aspect.ExcelMeta;
import jexx.poi.meta.aspect.ExcelMetaLabel;
import jexx.poi.meta.aspect.ExcelMetaValue;
import jexx.poi.meta.node.INode;
import jexx.poi.meta.node.Node;
import jexx.util.Assert;
import jexx.util.CollectionUtil;
import jexx.util.ReflectUtil;

import java.lang.reflect.Field;
import java.util.*;
import java.util.function.Function;

public class ArrayMeta implements IMeta {

    private final String name;
    private final List<INode> valueMetas;
    private final Map<Object, INode> valueMap;
    private final Map<Object, INode> labelMap;

    public ArrayMeta(String name, List<INode> nodes) {
        this.name = name;
        this.valueMetas = nodes;
        valueMap = new HashMap<>();
        labelMap = new HashMap<>();

        for (INode node : valueMetas){
            valueMap.put(node.getValue(), node);
            labelMap.put(node.getLabel(), node);
        }
    }

    /**
     * 通过注解来构建ArrayMeta
     * @param name meta名称
     * @param list 待构建的对象集合
     * @return {@link ArrayMeta}
     */
    public static <T> ArrayMeta buildMetaWithAnnotation(String name, List<T> list){
        List<INode> valueMetas = new ArrayList<>();
        for(T t : list){
            ExcelMeta excelMeta = ReflectUtil.getAnnotationOfClass(t.getClass(), ExcelMeta.class);
            Assert.notNull(excelMeta, "Object don't contain the annotation ExcelMeta");

            List<Field> valueFields = ReflectUtil.getFieldsByAnnotation(t.getClass(), ExcelMetaValue.class);
            Assert.isTrue(valueFields.size() == 1 , "Object has zero or more than one value field!");
            Field valueField = valueFields.get(0);
            Object value = ReflectUtil.getFieldValue(t, valueField);

            Object label;
            List<Field> labelFields = ReflectUtil.getFieldsByAnnotation(t.getClass(), ExcelMetaLabel.class);
            if(CollectionUtil.isNotEmpty(labelFields)){
                Field labelField = labelFields.get(0);
                label = ReflectUtil.getFieldValue(t, labelField);
            }
            else{
                label = value;
            }

            valueMetas.add(new Node(value, label));
        }
        return new ArrayMeta(name, valueMetas);
    }

    /**
     * 构建ArrayMeta
     * @param name meta名称
     * @param list 待构建的对象集合
     * @param valueMapper 获取value的方法
     * @param labelMapper 获取label的方法
     * @return {@link ArrayMeta}
     */
    public static <T,K,U> ArrayMeta buildMeta(final String name, final List<T> list, Function<? super T, ? extends K> valueMapper, Function<? super T, ? extends U> labelMapper){
        Objects.requireNonNull(valueMapper);
        List<INode> nodes = new ArrayList<>();
        for(T t : list){
            if(labelMapper != null){
                nodes.add(new Node(valueMapper.apply(t), labelMapper.apply(t)));
            }
            else{
                nodes.add(new Node(valueMapper.apply(t)));
            }
        }
        return new ArrayMeta(name, nodes);
    }

    /**
     * 构建ArrayMeta
     * @param name meta名称
     * @param list 待构建的对象集合
     * @param valueMapper 获取value的方法
     * @return {@link ArrayMeta}
     */
    public static <T,K> ArrayMeta buildMeta(final String name, final List<T> list, Function<? super T, ? extends K> valueMapper){
        return buildMeta(name, list, valueMapper, null);
    }

    public static <K,V> ArrayMeta buildMeta(final String name, final Map<K, V> map){
        List<INode> nodes = new ArrayList<>();
        map.forEach((k,v)->nodes.add(new Node(k, v)));
        return new ArrayMeta(name, nodes);
    }

    public static <T> ArrayMeta buildMeta(String name, List<T> list){
        List<INode> nodes = new ArrayList<>();
        list.forEach((k)->nodes.add(new Node(k)));
        return new ArrayMeta(name, nodes);
    }

    public static <T> ArrayMeta buildMeta(String name, Set<T> list){
        List<INode> nodes = new ArrayList<>();
        list.forEach((k)->nodes.add(new Node(k)));
        return new ArrayMeta(name, nodes);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public INode getNodeByFullValue(Object fullValue) {
        return valueMap.get(fullValue);
    }

    @Override
    public INode getNodeByFullLabel(Object fullLabel){
        return labelMap.get(fullLabel);
    }

    public List<INode> getValueMetas() {
        return valueMetas;
    }
}

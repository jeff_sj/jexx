package jexx.poi.meta;

import org.apache.poi.ss.usermodel.Sheet;

/**
 * meta对应sheet的抽象类
 * @author jeff
 */
public abstract class AbstractMetaSheet {

    public static final String PREFIX = "__";
    public static final String INFO_START = "__INFO__START";
    public static final String METADATA_START = "__METADATA__START";
    public static final String HEADER_START = "__HEADER__START";
    public static final String END = "__END__";

    /** meta对应的sheet */
    protected Sheet sheet;

}

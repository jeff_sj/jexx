package jexx.poi.meta.aspect;

import java.lang.annotation.*;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface ExcelMeta {

    /**
     * 是否由显示名称
     */
    boolean hasLabel() default false;

}

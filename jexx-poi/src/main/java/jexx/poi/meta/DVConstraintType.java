package jexx.poi.meta;

/**
 * cell constraint type
 *
 * @author jeff
 */
public enum DVConstraintType {

    /**
     * default, no constraint
     */
    DEFAULT,
    /**
     * array constraint
     */
    ARRAY,
    /**
     * tree constraint
     */
    TREE,
    /**
     * seq constraint
     */
    SEQ,
    /**
     * formula
     */
    FORMULA

}

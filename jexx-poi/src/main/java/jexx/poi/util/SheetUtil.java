package jexx.poi.util;

import jexx.util.FontUtil;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.font.FontRenderContext;
import java.awt.font.TextAttribute;
import java.awt.font.TextLayout;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.text.AttributedString;
import java.util.Locale;

/**
 * copy from poi
 *
 * @see org.apache.poi.ss.util.SheetUtil
 * @author jeff
 * @since 2019/6/13
 */
public class SheetUtil {

    private static final Logger LOG = LoggerFactory.getLogger(SheetUtil.class);

    private static final char defaultChar = '0';

    private static final double fontHeightMultiple = 2.0;

    private static final FontRenderContext fontRenderContext = new FontRenderContext(null, true, true);

    public static final String SONG_TI = "宋体";
    private static boolean hasSongTi;
    static {
        try{
            hasSongTi = FontUtil.hasFontFamilyName(SONG_TI);
        }
        catch (Error e){
            LOG.warn("忽视系统不支持字体异常, msg={}", e.getMessage());
        }
        if(!hasSongTi){
            LOG.warn("this environment has no font with \"宋体\", so jexx-poi may be failed to auto column");
        }
    }

    public static double getColumnWidth(Sheet sheet, int columnNum, boolean useMergedCells) {
        return getColumnWidth(sheet, columnNum, useMergedCells, sheet.getFirstRowNum()+1, sheet.getLastRowNum()+1);
    }


    public static double getColumnWidth(Sheet sheet, int columnNum, boolean useMergedCells, int firstRowNum, int lastRowNum){
        DataFormatter formatter = new DataFormatter();
        FormulaEvaluator formulaEvaluator = sheet.getWorkbook().getCreationHelper().createFormulaEvaluator();

        double defaultCharWidth = getDefaultCharWidth(sheet.getWorkbook());

        double width = -1;
        for (int rowIdx = firstRowNum; rowIdx <= lastRowNum; rowIdx++) {
            Row row = sheet.getRow(rowIdx-1);
            if( row != null ) {
                double cellWidth = getColumnWidthForRow(row, columnNum, defaultCharWidth, formatter, formulaEvaluator, useMergedCells);
                width = Math.max(width, cellWidth);
            }
        }
        return width;
    }

    private static double getColumnWidthForRow(Row row, int columnNum, double defaultCharWidth, DataFormatter formatter,
                                               FormulaEvaluator formulaEvaluator, boolean useMergedCells) {
        if( row == null ) {
            return -1;
        }

        Cell cell = row.getCell(columnNum-1);

        if (cell == null) {
            return -1;
        }

        return getCellWidth(cell, defaultCharWidth, formatter, formulaEvaluator, useMergedCells);
    }

    public static double getDefaultCharWidth(final Workbook wb) {
        Font defaultFont = wb.getFontAt((short) 0);

        String x = String.valueOf(defaultChar);
        AttributedString str = new AttributedString(x);
        copyAttributes(defaultFont, str, 0, 1);
        TextLayout layout = new TextLayout(str.getIterator(), fontRenderContext);
        return layout.getAdvance();
    }

    private static void copyAttributes(Font font, AttributedString str, int startIdx, int endIdx) {
        //excel的XSSFFont默认字体Calibri 会导致中文字符长度计算有误
        str.addAttribute(TextAttribute.FAMILY, hasSongTi ? SONG_TI : font.getFontName(), startIdx, endIdx);
        str.addAttribute(TextAttribute.SIZE, font.getFontHeightInPoints());
        if(font.getBold()){
            str.addAttribute(TextAttribute.WEIGHT, TextAttribute.WEIGHT_BOLD, startIdx, endIdx);
        }
        if(font.getItalic() ){
            str.addAttribute(TextAttribute.POSTURE, TextAttribute.POSTURE_OBLIQUE, startIdx, endIdx);
        }
        if(font.getUnderline() == Font.U_SINGLE ){
            str.addAttribute(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON, startIdx, endIdx);
        }
    }

    public static double getCellWidth(Cell cell, double defaultCharWidth, DataFormatter formatter, FormulaEvaluator formulaEvaluator, boolean useMergedCells) {
        Sheet sheet = cell.getSheet();
        Workbook wb = sheet.getWorkbook();
        Row row = cell.getRow();
        int column = cell.getColumnIndex();

        // FIXME: this looks very similar to getCellWithMerges below. Consider consolidating.
        // We should only be checking merged regions if useMergedCells is true. Why are we doing this for-loop?
        int colspan = 1;
        for (CellRangeAddress region : sheet.getMergedRegions()) {
            if (region.isInRange(row.getRowNum(), column)) {
                if (!useMergedCells) {
                    // If we're not using merged cells, skip this one and move on to the next.
                    return -1;
                }
                cell = row.getCell(region.getFirstColumn());
                colspan = 1 + region.getLastColumn() - region.getFirstColumn();
            }
        }

        CellStyle style = cell.getCellStyle();
        CellType cellType = cell.getCellTypeEnum();

        // for formula cells we compute the cell columnCount for the cached formula result
        if(cellType == CellType.FORMULA){
            cellType = cell.getCachedFormulaResultTypeEnum();
        }

        Font font = wb.getFontAt(style.getFontIndex());

        double width = -1;
        if (cellType == CellType.STRING) {
            RichTextString rt = cell.getRichStringCellValue();
            String[] lines = rt.getString().split("\\n");
            for (String line : lines) {
                String txt = line + defaultChar;

                AttributedString str = new AttributedString(txt);
                copyAttributes(font, str, 0, txt.length());

                /*if (rt.numFormattingRuns() > 0) {
                    // TODO: support rich text fragments
                }*/

                width = getCellWidth(defaultCharWidth, colspan, style, width, str);
            }
        } else {
            String sval = null;
            if (cellType == CellType.NUMERIC) {
                // Try to get it formatted to look the same as excel
                try {
                    sval = formatter.formatCellValue(cell, formulaEvaluator);
                } catch (Exception e) {
                    sval = String.valueOf(cell.getNumericCellValue());
                }
            } else if (cellType == CellType.BOOLEAN) {
                sval = String.valueOf(cell.getBooleanCellValue()).toUpperCase(Locale.ROOT);
            }
            if(sval != null) {
                String txt = sval + defaultChar;
                AttributedString str = new AttributedString(txt);
                copyAttributes(font, str, 0, txt.length());

                width = getCellWidth(defaultCharWidth, colspan, style, width, str);
            }
        }
        return width;
    }

    private static double getCellWidth(double defaultCharWidth, int colspan,
                                       CellStyle style, double minWidth, AttributedString str) {
        TextLayout layout = new TextLayout(str.getIterator(), fontRenderContext);
        final Rectangle2D bounds;
        if(style.getRotation() != 0){
            /*
             * Transform the text using a scale so that it's height is increased by a multiple of the leading,
             * and then rotate the text before computing the bounds. The scale results in some whitespace around
             * the unrotated top and bottom of the text that normally wouldn't be present if unscaled, but
             * is added by the standard Excel autosize.
             */
            AffineTransform trans = new AffineTransform();
            trans.concatenate(AffineTransform.getRotateInstance(style.getRotation()*2.0*Math.PI/360.0));
            trans.concatenate(
                    AffineTransform.getScaleInstance(1, fontHeightMultiple)
            );
            bounds = layout.getOutline(trans).getBounds();
        } else {
            bounds = layout.getBounds();
        }
        // frameWidth accounts for leading spaces which is excluded from bounds.getColumnCount()
        final double frameWidth = bounds.getX() + bounds.getWidth();
        return Math.max(minWidth, ((frameWidth / colspan) / defaultCharWidth) + style.getIndention());
    }

}

package jexx.poi.util;

import org.apache.poi.ss.usermodel.DataValidation;
import org.apache.poi.ss.usermodel.DataValidationConstraint;
import org.apache.poi.ss.usermodel.DataValidationHelper;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.util.CellRangeAddressList;

public class DataValidationUtil {

    /**
     * 创建校验器
     * @param sheet sheet
     * @param formula 公式
     * @param firstRowNum 开始行号,从1开始
     * @param lastRowNum 结束行号,从1开始
     * @param firstColNum 开始列号,从1开始
     * @param lastColNum 结束列号,从1开始
     */
    public static DataValidation createValidation(Sheet sheet, String formula, int firstRowNum, int lastRowNum, int firstColNum, int lastColNum){
        int firstRowIndex = firstRowNum - 1;
        int lastRowIndex = lastRowNum - 1;
        int firstColIndex = firstColNum - 1;
        int lastColIndex = lastColNum - 1;

        DataValidation dataValidation;

        DataValidationHelper dvHelper = sheet.getDataValidationHelper();
        DataValidationConstraint dvConstraint = dvHelper.createFormulaListConstraint(formula);
        CellRangeAddressList regions = new CellRangeAddressList(firstRowIndex, lastRowIndex, firstColIndex, lastColIndex);
        dataValidation = dvHelper.createValidation(dvConstraint, regions);

        return dataValidation;
    }

}

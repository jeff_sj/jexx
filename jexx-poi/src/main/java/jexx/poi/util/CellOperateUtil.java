package jexx.poi.util;

import jexx.poi.cell.FormulaCellValue;
import jexx.time.DateUtil;
import jexx.util.Assert;
import jexx.util.CharUtil;
import jexx.util.StringPool;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.CellUtil;
import org.apache.poi.xssf.usermodel.XSSFCell;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;

/**
 * cell utilities
 *
 * @author jeff
 */
public class CellOperateUtil {

    /**
     * Get a specific cell from a row. If the cell doesn't exist, then create it.
     *
     * @param row The row that the cell is part of
     * @param columnNum The column num that the cell is in. base 1;
     * @return  The cell indicated by the column.
     */
    public static Cell getCell(Row row, int columnNum) {
        return CellUtil.getCell(row, columnNum - 1);
    }

    public static Object getCellValue(Cell cell) {
        if (null == cell) {
            return null;
        }
        return getCellValue(cell, cell.getCellTypeEnum());
    }

    public static Object getCellValue(Cell cell, CellType cellType) {
        if (null == cell) {
            return null;
        }
        if (null == cellType) {
            cellType = cell.getCellTypeEnum();
        }

        Object value;
        switch (cellType) {
            case NUMERIC:
                value = getNumericValue(cell);
                break;
            case BOOLEAN:
                value = cell.getBooleanCellValue();
                break;
            case FORMULA:
                // 遇到公式时查找公式结果类型
                value = getCellValue(cell, cell.getCachedFormulaResultTypeEnum());
                break;
            case BLANK:
                value = StringPool.EMPTY;
                break;
            case ERROR:
                final FormulaError error = FormulaError.forInt(cell.getErrorCellValue());
                value = (null == error) ? StringPool.EMPTY : error.getString();
                break;
            default:
                value = cell.getStringCellValue();
        }
        return value;
    }

    /**
     * 获取数字类型的单元格值
     *
     * @param cell 单元格
     * @return 单元格值，可能为Long、Double、Date
     */
    private static Object getNumericValue(Cell cell) {
        final double value = cell.getNumericCellValue();

        final CellStyle style = cell.getCellStyle();
        if (null == style) {
            return value;
        }

        final short formatIndex = style.getDataFormat();
        // 判断是否为日期
        if (isDateType(cell, formatIndex)) {
            return DateUtil.date(cell.getDateCellValue());
        }

        final String format = style.getDataFormatString();
        // 普通数字
        if (null != format && !format.contains(StringPool.DOT)) {
            final long longPart = (long) value;
            if (longPart == value) {
                // 对于无小数部分的数字类型，转为Long
                return longPart;
            }
        }
        return value;
    }

    /**
     * get the last column num of row, base 1;
     * 获取最后列号,从1开始
     *
     * @param row row
     * @return the last column num; return -1 if no cell
     */
    public static int getLastCellNum(Row row){
        int lastCellNum = row.getLastCellNum();
        if(lastCellNum == -1){
            return -1;
        }
        return lastCellNum+1;
    }

    /**
     * 获取单元格的字符串值
     * @param cell 单元格
     * @return 字符串
     */
    public static String getStringCellValue(Cell cell) {
        if (null == cell) {
            return null;
        }
        return cell.getStringCellValue();
    }

    /**
     * 是否为日期格式<br>
     * 判断方式：
     *
     * <pre>
     * 1、指定序号
     * 2、org.apache.poi.ss.usermodel.DateUtil.isADateFormat方法判定
     * </pre>
     *
     * @param cell excel单元格
     * @param formatIndex 格式序号
     * @return 是否为日期格式
     */
    private static boolean isDateType(Cell cell, int formatIndex) {
        // yyyy-MM-dd----- 14
        // yyyy年m月d日---- 31
        // yyyy年m月------- 57
        // m月d日 ---------- 58
        // HH:mm----------- 20
        // h时mm分 -------- 32
        if (formatIndex == 14 || formatIndex == 31 || formatIndex == 57 || formatIndex == 58 || formatIndex == 20 || formatIndex == 32) {
            return true;
        }

        if (org.apache.poi.ss.usermodel.DateUtil.isCellDateFormatted(cell)) {
            return true;
        }

        return false;
    }

    /**
     * 单元格中列序号转换成列字母编号,默认列序号从1开始
     * <p>
     *     <pre>
     *         1="A"
     *         26="Z"
     *         27="AA
     *     </pre>
     * </p>
     * @param columnNum 列号,从1开始
     * @return 列字母编号
     */
    public static String toColumnLabel(int columnNum) {
        int columnIndex = columnNum - 1;
        StringBuilder label = new StringBuilder();
        while(columnIndex >= 0) {
            label.append((char)(columnIndex % 26 + 'A'));
            columnIndex = (columnIndex - columnIndex % 26) / 26;
            columnIndex--;
        }
        return label.reverse().toString();
    }

    /**
     * 单元格中列的字母序号转换成从1开始列号
     * <p>
     *     <pre>
     *         "A"=1
     *         "Z"=26
     *         "AA"=27
     *     </pre>
     * </p>
     * @param columnNo 字母序号
     * @return 从1开始的列号
     */
    public static int toColumnNum(String columnNo) {
        Assert.hasText(columnNo, "label is not empty!");
        int num;
        int result = 0;
        int length = columnNo.toUpperCase().length();
        for(int i = 0; i < length; i++) {
            char ch = columnNo.charAt(length - i - 1);
            Assert.isTrue(CharUtil.isAlphaUpper(ch));
            num = ch - 'A' + 1;
            num *= Math.pow(26, i);
            result += num;
        }
        return result;
    }

    public static void setCellValue(Cell cell, Object value) {
        if (null == value) {
            cell.setCellType(CellType.BLANK);
        }
        //需设置setCellStyle才能显示时间格式
        else if (value instanceof Date) {
            setCellType(cell, CellType.NUMERIC);
            cell.setCellValue((Date) value);
        } else if (value instanceof Calendar) {
            setCellType(cell, CellType.NUMERIC);
            cell.setCellValue((Calendar) value);
        } else if (value instanceof Boolean) {
            setCellType(cell, CellType.BOOLEAN);
            cell.setCellValue((Boolean) value);
        } else if (value instanceof RichTextString) {
            setCellType(cell, CellType.STRING);
            cell.setCellValue((RichTextString) value);
        }
        //此处影响公式; 公式统计的单元格必须是Number,否则无法自动计算值
        else if (value instanceof Number) {
            setCellType(cell, CellType.NUMERIC);
            cell.setCellValue(((Number)value).doubleValue());
        }
        else if(value instanceof FormulaCellValue){
            setCellType(cell, CellType.FORMULA);
            cell.setCellFormula(((FormulaCellValue)value).getFormula());
        }
        else {
            setCellType(cell, CellType.STRING);
            cell.setCellValue(value.toString());
        }
    }

    private static void setCellType(Cell cell, CellType cellType){
        if(cell instanceof XSSFCell){
            cell.setCellType(cellType);
        }
    }

    /**
     * merge cells unsafely, so need to invoke <code>sheet.validateMergedRegions()</code> ;
     * @param sheet sheet
     * @param firstRowNum the row num of first cell
     * @param firstColumnNum the column's num of first cell
     * @param lastRowNum the row num of last cell
     * @param lastColumnNum he column's num of last cell
     */
    public static void mergeCellsByUnsafe(Sheet sheet, int firstRowNum, int firstColumnNum, int lastRowNum, int lastColumnNum){
        CellRangeAddress cellRangeAddress = new CellRangeAddress(firstRowNum-1, lastRowNum-1,
                firstColumnNum-1, lastColumnNum-1);
        sheet.addMergedRegionUnsafe(cellRangeAddress);
    }

    /**
     * merge cells safely
     * @param sheet sheet
     * @param firstRowNum the row num of first cell
     * @param firstColumnNum the column's num of first cell
     * @param lastRowNum the row num of last cell
     * @param lastColumnNum he column's num of last cell
     */
    public static void mergeCells(Sheet sheet, int firstRowNum, int firstColumnNum, int lastRowNum, int lastColumnNum){
        CellRangeAddress cellRangeAddress = new CellRangeAddress(firstRowNum-1, lastRowNum-1,
                firstColumnNum-1, lastColumnNum-1);
        sheet.addMergedRegion(cellRangeAddress);
    }

}

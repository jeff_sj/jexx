package jexx.poi.util;

import jexx.util.Assert;

import java.util.ArrayList;
import java.util.List;

/**
 * @author jeff
 * @since 2019/6/20
 */
public class HeaderPathUtil {


    /**
     * header的key，分割 对应的数据路径，获取到集合段的路径
     *
     * headerKey=a[],dataPath=a[0]  ==> a[0]
     * headerKey=a[].b[],dataPath=a[0].b[0]  ==>a[0],a[0].b[0]
     * headerKey=a[].b[].c,dataPath=a[0].b[0].c ==>a[0],a[0].b[0]
     * headerKey=a[1],dataPath=a[1]  ==>
     * headerKey=a[1].b,dataPath=a[1].b  ==>
     * headerKey=a[1].b[].c,dataPath=a[1].b[0].c  ==>
     * headerKey=a[].b[1].c,dataPath=a[0].b[1].c ==> a[0]
     *
     *
     * @param headerKey header的key
     * @param dataPath 对象数据的路径
     * @return 分割
     */
    public static List<String> splitDataPaths(String headerKey, String dataPath){
        List<String> list = new ArrayList<>();

        char[] headerKeys = headerKey.toCharArray();
        char[] dataPaths = dataPath.toCharArray();
        int i = 0;
        int dataStartIndex = 0;
        int dataEndIndex = 0;
        while(i < headerKeys.length){
            if(headerKeys[i] == '[' && i + 1 < headerKeys.length && headerKeys[i+1]==']'){
                Assert.isTrue(dataPath.charAt(dataStartIndex) == '[');
                Assert.isTrue(dataPath.charAt(dataStartIndex+1) != ']');
                dataEndIndex = dataEndIndex + 2;
                while(dataEndIndex < dataPaths.length){
                    if(dataPaths[dataEndIndex] == ']'){
                        break;
                    }
                    dataEndIndex++;
                }
                list.add(dataPath.substring(0, dataEndIndex+1));
                i = i+2;
                dataEndIndex++;
                dataStartIndex = dataEndIndex;
            }
            else{
                Assert.isTrue(headerKeys[i] == dataPaths[dataStartIndex]);
                i++;
                dataStartIndex++;
                dataEndIndex++;
            }
        }

        return list;
    }

}

package jexx.poi.util;

import jexx.io.IOUtil;
import jexx.poi.exception.POIException;
import org.apache.poi.openxml4j.util.ZipSecureFile;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import java.io.File;
import java.io.InputStream;

/**
 * poi的Workbook工具类
 * @author jeff
 */
public class WorkbookUtil {

    /**
     * 根据文件路径创建新的工作簿，文件路径
     * @param isXlsx 是否为xlsx格式的Excel
     * @return {@link Workbook}
     */
    public static Workbook createBook(boolean isXlsx) {
        Workbook workbook;
        if (isXlsx) {
            workbook = new org.apache.poi.xssf.usermodel.XSSFWorkbook();
        } else {
            workbook = new org.apache.poi.hssf.usermodel.HSSFWorkbook();
        }
        return workbook;
    }

    /**
     * 创建excel的工作簿
     * @param excelFile excel文件
     * @return 工作簿
     */
    public static Workbook createBook(File excelFile) {
        return createBook(excelFile, null);
    }

    public static Workbook createBook(File excelFile, String password) {
        return createBook(excelFile, password, false);
    }

    /**
     * 根据密码创建excel的工作簿
     * @param excelFile excel文件
     * @param password 密码
     * @param readOnly 是否只读
     * @return 工作簿
     */
    public static Workbook createBook(File excelFile, String password, boolean readOnly) {
        try {
            setMinInflateRatio();
            return WorkbookFactory.create(excelFile, password, readOnly);
        } catch (Exception e) {
            throw new POIException(e);
        }
    }

    /**
     * 创建或加载工作簿
     * @param in Excel输入流
     */
    public static Workbook createBook(InputStream in) {
        return createBook(in, null , true);
    }

    /**
     * 创建或加载工作簿
     * @param in Excel输入流
     * @param closeAfterRead 读取结束是否关闭流
     */
    public static Workbook createBook(InputStream in, boolean closeAfterRead) {
        return createBook(in, null ,closeAfterRead);
    }

    /**
     * 创建或加载工作簿
     * @param in Excel输入流
     * @param password 密码
     * @param closeAfterRead 读取结束是否关闭流
     */
    public static Workbook createBook(InputStream in, String password, boolean closeAfterRead) {
        try {
            setMinInflateRatio();
            return WorkbookFactory.create(IOUtil.toMarkSupportStream(in), password);
        } catch (Exception e) {
            throw new POIException(e);
        } finally {
            if (closeAfterRead) {
                IOUtil.closeQuietly(in);
            }
        }
    }


    /**
     * 是否已设置MIN_INFLATE_RATIO
     */
    private static boolean isSetMinInflateRatio = false;

    /**
     * 解决Zip bomb
     */
    public static void setMinInflateRatio(double ratio){
        isSetMinInflateRatio = true;
        ZipSecureFile.setMinInflateRatio(ratio);
    }

    public static void setMinInflateRatio(){
        if(!isSetMinInflateRatio){
            setMinInflateRatio(0.009);
        }
    }

}

package jexx.poi.util;

import jexx.util.Assert;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.util.CellUtil;

import java.util.ArrayList;
import java.util.List;

public class RowOperationUtil {

    public static void writeData(Row row, Iterable<?> data){
        int i = 0;
        Cell cell;
        for(Object obj : data){
            cell = row.createCell(i++);
            CellOperateUtil.setCellValue(cell, obj);
        }
    }

    public static void writeMeta(Row row, String name, List<?> valueMetas) {
        int i = 0;
        Cell cell;
        cell = row.createCell(i++);
        CellOperateUtil.setCellValue(cell, name);
        for (Object value : valueMetas) {
            cell = row.createCell(i++);
            CellOperateUtil.setCellValue(cell, value);
        }
    }

    /**
     * 读取当前行的数据
     * @param row 一行
     * @return 数据
     */
    public static List<Object> readRow(Row row){
        return readRow(row, 1);
    }

    /**
     * 读取当前行指定列后开始的数据
     * @param row 一行
     * @param startColumnNum 读取数据的开始列号,从1开始
     * @return 数据
     */
    public static List<Object> readRow(Row row, int startColumnNum){
        Assert.isTrue(startColumnNum > 0);
        if(row == null){
            return new ArrayList<>();
        }
        return readRow(row, startColumnNum, row.getLastCellNum()+1);
    }

    /**
     * 
     * @param row 行
     * @param startColumnNum 开始列号,从1开始
     * @param endColumnNum 结束列号,从1开始
     * @return 数据
     */
    public static List<Object> readRow(Row row, int startColumnNum, int endColumnNum){
        if(row == null){
            return new ArrayList<>();
        }
        Assert.isTrue(startColumnNum > 0 && endColumnNum >= startColumnNum, "rowNum={},startColumnNum={},endColumnNum={}",
                row.getRowNum()+1, startColumnNum, endColumnNum);

        int lastCellNum = row.getLastCellNum()+1;
        lastCellNum = lastCellNum <= endColumnNum ? lastCellNum : endColumnNum;
        Cell cell;
        List<Object> data = new ArrayList<>(lastCellNum);
        for(int i = startColumnNum; i <= lastCellNum; i++){
            cell = row.getCell(i-1);
            data.add(CellOperateUtil.getCellValue(cell));
        }
        return data;
    }

    /**
     * 获取行
     * @param sheet sheet
     * @param rowNum 行号,从1开始
     * @return 新行
     */
    public static Row getRow(Sheet sheet, int rowNum){
        return getRow(rowNum, sheet);
    }

    /**
     * Get a row from the spreadsheet, and create it if it doesn't exist.
     * @param rowNum The 1 based row number
     * @param sheet The sheet that the row is part of.
     * @return The row indicated by the rowCounter
     */
    public static Row getRow(int rowNum, Sheet sheet) {
        return CellUtil.getRow(rowNum - 1, sheet);
    }

    /**
     * 最后行号, 从1开始计算
     * @param sheet sheet
     * @return 最后行号
     */
    public static int getLastRowNum(Sheet sheet){
        return sheet.getLastRowNum()+1;
    }

    /**
     * 创建行
     * @param sheet sheet
     * @param rowNum 行号,从1开始
     * @return 新行
     */
    public static Row createRow(Sheet sheet, int rowNum){
        return sheet.createRow(rowNum-1);
    }

}

package jexx.poi.util;

/**
 * 公式工具类
 *
 * @author jeff
 * @since 2019/6/19
 */
public class FormulaUtil {

    /**
     * 区域公式
     *
     * @param formula 公式类型,如SUM
     * @param startRowNum 开始行, base 1
     * @param endRowNum 结束行, base 1
     * @param startColumnNum 开始列, base 1
     * @param endColumnNum 结束列, base 1
     * @return 公式
     */
    public static String calculateAreaFormula(String formula, int startRowNum, int endRowNum, int startColumnNum, int endColumnNum){
        StringBuilder sb = new StringBuilder(formula);
        sb.append("(").append(CellOperateUtil.toColumnLabel(startColumnNum)).append(startRowNum);
        sb.append(":");
        sb.append(CellOperateUtil.toColumnLabel(endColumnNum)).append(endRowNum);
        sb.append(")");
        return sb.toString();
    }

    /**
     * 求行中求和公式
     *
     * @param rowNum 行号
     * @param startColumnNum 开始列号
     * @param endColumnNum 结束列号
     * @return 公式
     */
    public static String calculateRowSumFormula(int rowNum, int startColumnNum, int endColumnNum){
        return calculateAreaFormula("SUM", rowNum, rowNum, startColumnNum, endColumnNum);
    }

    /**
     * 求行中求和公式
     *
     * @param rowNum 行号
     * @param startColumnNo 开始列号
     * @param endColumnNo 结束列号
     * @return 公式
     */
    public static String calculateRowSumFormula(int rowNum, String startColumnNo, String endColumnNo){
        return calculateRowSumFormula(rowNum, CellOperateUtil.toColumnNum(startColumnNo), CellOperateUtil.toColumnNum(endColumnNo));
    }

    /**
     * 求列中求和公式
     *
     * @param startRowNum 开始行号
     * @param endRowNum 结束行号
     * @param columnNum 列号
     * @return 公式
     */
    public static String calculateColumnSumFormula(int startRowNum, int endRowNum, int columnNum){
        return calculateAreaFormula("SUM", startRowNum, endRowNum, columnNum, columnNum);
    }

    /**
     * 求列中求和公式
     *
     * @param startRowNum 开始行号
     * @param endRowNum 结束行号
     * @param columnNo 列号
     * @return 公式
     */
    public static String calculateColumnSumFormula(int startRowNum, int endRowNum, String columnNo){
        return calculateColumnSumFormula(startRowNum, endRowNum, CellOperateUtil.toColumnNum(columnNo));
    }

}

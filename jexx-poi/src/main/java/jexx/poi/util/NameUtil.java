package jexx.poi.util;

import jexx.poi.cell.ICellSupportMeta;
import jexx.util.StringUtil;

public class NameUtil {

    //名称管理器中不合法字符替换成这个
    public static final char REPLACE_CHAR = ' ';

    /**
     * 名称管理器名称合法规则, 以下划线或者字母或者数字为开头, 其余字符可以为 字母，数字，或者  _.?\
     * @see org.apache.poi.xssf.usermodel.XSSFName#validateName(String)
     * @param name 字符
     * @return 是否合法
     */
    public static boolean validateName(String name){
        char c = name.charAt(0);
        String allowedSymbols = "_\\";
        boolean characterIsValid = (Character.isLetter(c) || allowedSymbols.indexOf(c) != -1);
        if(!characterIsValid){
            return false;
        }

        allowedSymbols = "_.?\\";
        for (final char ch : name.toCharArray()) {
            characterIsValid = (Character.isLetterOrDigit(ch) || allowedSymbols.indexOf(ch) != -1);
            if (!characterIsValid) {
                return false;
            }
        }

        return true;
    }

    /**
     * 用 {@code REPLACE_CHAR} 替换名称管理器名字中的不合法字符
     * @param name 字符
     * @param skipWhitespace 是否跳过空格; 虽然名称管理器不支持空格, 但是此工具作了优化
     * @return 替换后的字符
     */
    public static String replaceIllegalCharForName(String name, boolean skipWhitespace){
        String allowedSymbols = "_.?\\";
        char[] chars = name.toCharArray();
        boolean replace = false;
        for(int i = 0; i < chars.length; i++){
            char ch = chars[i];
            if(skipWhitespace && Character.isWhitespace(ch)){
                continue;
            }

            boolean characterIsValid = (Character.isLetterOrDigit(ch) || allowedSymbols.indexOf(ch) != -1);
            if(!characterIsValid){
                chars[i] = REPLACE_CHAR;
                replace = true;
            }
        }
        return replace ? new String(chars) : name;
    }

    public static String replaceIllegalCharForName(String name){
        return replaceIllegalCharForName(name, true);
    }

    /**
     * 转换名字为符合名称管理器的名称;加前缀,去除空格
     * @param str 名称管理器名称
     */
    public static String encodeNameName(String str){
        if(StringUtil.isEmpty(str)){
            throw new IllegalArgumentException("excel name's name is not empty");
        }

        String name = String.valueOf(ICellSupportMeta.NAME_PREFIX).concat(str);
        return StringUtil.replace(name, " ", "");
    }

}

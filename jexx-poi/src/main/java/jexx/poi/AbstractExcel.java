package jexx.poi;

import jexx.io.IOUtil;
import jexx.poi.constant.SheetVisibilityEnum;
import jexx.poi.meta.Metas;
import jexx.util.Assert;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import java.io.Closeable;

/**
 * excel处理相关类
 * @author jeff
 */
public abstract class AbstractExcel implements Closeable {

    public static final String METADATA_SHEET_NAME = "__METADATA__";

    public static final String DEFAULT_SHEET_NAME = "Sheet1";

    /** excel的工作簿 */
    protected Workbook workbook;
    /** meta信息集合 */
    protected Metas meta;

    protected Workbook getWorkbook(){
        return this.workbook;
    }

    /**
     * excel中是否含有meta信息的sheet
     * @return bool
     */
    public boolean hasMetaSheet(){
        return workbook.getSheet(METADATA_SHEET_NAME) != null;
    }

    /**
     * 获取meta信息
     * @return {@link Metas}
     */
    public Metas getMetas(){
        return this.meta;
    }

    /**
     * 判断是否有某个sheet
     * @param sheetName sheet名字
     */
    public boolean hasSheet(String sheetName){
        return workbook.getSheet(sheetName) != null;
    }

    /**
     * 获取指定名称的sheet,如果不存在则创建
     * @param sheetName sheet名称
     * @return {@link Sheet}
     */
    protected Sheet getOrCreateSheet(String sheetName){
        Sheet sheet = workbook.getSheet(sheetName);
        if(sheet == null){
            sheet = workbook.createSheet(sheetName);
        }
        return sheet;
    }

    protected Sheet getSheet(String sheetName){
        return workbook.getSheet(sheetName);
    }

    /**
     * 根据索引获取sheet, base 1
     * @param sheetNum sheet索引
     */
    protected Sheet getSheetAt(int sheetNum){
        return workbook.getSheetAt(sheetNum - 1);
    }

    /**
     * 设置sheet可见类型
     * @param sheetName sheet名称
     */
    public void setSheetVisibility(String sheetName, SheetVisibilityEnum visibility){
        int sheetIndex = workbook.getSheetIndex(sheetName);
        if(sheetIndex > -1){
            workbook.setSheetVisibility(sheetIndex, visibility.getVisibility());
        }
    }

    /**
     * 使得Metadata Sheet可见
     * <p>bug: Workbook.write之后, 原先的meta那个sheet可以隐藏后再取消隐藏从而可见了</p>
     */
    public void makeMetaSheetVisilble(){
        setSheetVisibility(METADATA_SHEET_NAME, SheetVisibilityEnum.VISIBLE);
    }

    /**
     * 设置sheet位置
     * @param sheetName sheet名称
     * @param pos 位置, 从0开始
     */
    public void setSheetOrder(String sheetName, int pos){
        Sheet sheet = this.workbook.getSheet(sheetName);
        Assert.notNull(sheet, "Sheet[{}]不存在", sheetName);
        this.workbook.setSheetOrder(sheetName, pos);
    }

    @Override
    public void close() {
        IOUtil.closeQuietly(workbook);
    }
}

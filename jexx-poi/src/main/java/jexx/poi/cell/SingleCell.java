package jexx.poi.cell;

import jexx.poi.style.CellStyleSets;
import jexx.poi.style.IWrapCellStyle;
import jexx.poi.util.CellOperateUtil;

/**
 * Single cell only contains one cell!
 *
 * @author jeff
 * @since 2019/5/7
 */
public class SingleCell extends MergedCell {

    public SingleCell(int firstRowNum, int firstColumnNum, Object value, IWrapCellStyle cellStyle) {
        super(firstRowNum, firstColumnNum, firstRowNum, firstColumnNum, value, cellStyle);
    }

    public SingleCell(int firstRowNum, int firstColumnNum, Object value) {
        this(firstRowNum, firstColumnNum, value, null);
    }

    public SingleCell(int firstRowNum, int firstColumnNum) {
        this(firstRowNum, firstColumnNum, null);
    }

    public SingleCell(int firstRowNum, String firstColumnNo, Object value, IWrapCellStyle cellStyle) {
        this(firstRowNum,  CellOperateUtil.toColumnNum(firstColumnNo), value, cellStyle);
    }

    public SingleCell(int firstRowNum, String firstColumnNo, Object value) {
        this(firstRowNum, firstColumnNo, value, null);
    }

    public SingleCell(int firstRowNum, String firstColumnNo) {
        this(firstRowNum, firstColumnNo, null);
    }

}

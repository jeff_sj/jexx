package jexx.poi.cell;

import jexx.poi.style.IWrapCellStyle;

import java.lang.reflect.Array;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * 基类单元格
 *
 * @author jeff
 * @since 2019/5/7
 */
public interface ICell {

    String getId();

    /**
     * the first cell's row num, base 1
     *
     * @return row num
     */
    int getFirstRowNum();

    /**
     * the first cell's column num, base 1
     *
     * @return column num
     */
    int getFirstColumnNum();

    /**
     * the first cell's column no, base A;
     *
     * @return column no
     */
    String getFirstColumnNo();

    /**
     * 实际值是否为多值; 若为true,单元格实际值应为 数组或者集合
     */
    boolean isMultiValue();

    /**
     * 单元格实际值
     */
    Object getValue();

    /**
     * 获取单元格多个实际值; 非多值,则默认返回一个
     */
    default Object[] getValues(){
        Object[] values;
        if(!isMultiValue()){
            values = new Object[1];
            values[0] = getValue();
            return values;
        }

        Object value = getValue();
        if(value == null){
            return new Object[0];
        }

        if(value.getClass().isArray()){
            int length = Array.getLength(value);
            values = new Object[length];
            for (int i = 0; i < length; i++){
                values[i] = Array.get(value, i);
            }
        }
        else if(value instanceof List){
            int length = ((List<?>) value).size();
            values = new Object[length];
            for (int i = 0; i < length; i++){
                values[i] = ((List<?>) value).get(i);
            }
        }
        else if(value instanceof Set){
            Set<?> set = (Set<?>) value;
            int length = set.size();
            values = new Object[length];
            Iterator<?> iterator = set.iterator();
            int i = 0;
            while (iterator.hasNext()){
                values[i++] = iterator.next();
            }
        }
        else{
            throw new UnsupportedOperationException("Multi value cannot support class "+value.getClass());
        }
        return values;
    }

    /**
     * 设置cell值
     */
    void setValue(Object value);

    /**
     * 单元格显示值
     */
    Object getLabel();

    /**
     * 设置cell显示
     */
    void setLabel(Object label);

    /**
     * cell style
     *
     * @return {@link IWrapCellStyle}
     */
    IWrapCellStyle getCellStyle();

    /**
     * 设置单元格样式
     * @param cellStyle 样式
     */
    void setCellStyle(IWrapCellStyle cellStyle);


}

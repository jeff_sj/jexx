package jexx.poi.cell;

/**
 * 公式单元格的值
 *
 * @author Jeff
 */
public class FormulaCellValue {

    private String formula;

    public FormulaCellValue(String formula) {
        this.formula = formula;
    }

    public String getFormula() {
        return formula;
    }
}

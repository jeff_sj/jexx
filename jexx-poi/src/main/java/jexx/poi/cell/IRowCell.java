package jexx.poi.cell;

import jexx.poi.row.Row;

/**
 * cell row interface
 *
 * @author jeff
 * @since 2019/5/23
 */
public interface IRowCell {

    /**
     * the row that contains cell
     *
     * @return row
     */
    Row getRow();

}

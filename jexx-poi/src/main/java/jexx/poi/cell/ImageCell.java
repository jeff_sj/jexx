package jexx.poi.cell;

import jexx.poi.constant.PictureType;
import jexx.poi.util.CellOperateUtil;

import java.util.Objects;

/**
 * a cell that contains a image!
 *
 * @author jeff
 * @since 2019/5/8
 */
public class ImageCell extends AbstractCell implements IMergedCell {

    protected PictureType pictureType;

    public ImageCell(int firstRowNum, int firstColumnNum, int lastRowNum, int lastColumnNum, PictureType pictureType, byte[] pictureData) {
        super(firstRowNum, firstColumnNum, lastRowNum, lastColumnNum, pictureData, null);
        Objects.requireNonNull(pictureData);
        this.lastRowNum = lastRowNum;
        this.lastColumnNum = lastColumnNum;
        this.pictureType = pictureType;
    }

    public ImageCell(int firstRowNum, int firstColumnNum, PictureType pictureType, byte[] pictureData) {
        this(firstRowNum, firstColumnNum, firstRowNum, firstColumnNum, pictureType,  pictureData);
    }

    public ImageCell(int firstRowNum, String firstColumnNo, int lastRowNum, String lastColumnNo, PictureType pictureType, byte[] pictureData) {
        this(firstRowNum, CellOperateUtil.toColumnNum(firstColumnNo), lastRowNum, CellOperateUtil.toColumnNum(lastColumnNo), pictureType, pictureData);
    }

    public ImageCell(int firstRowNum, String firstColumnNo, PictureType pictureType, byte[] pictureData) {
        this(firstRowNum, CellOperateUtil.toColumnNum(firstColumnNo), pictureType,  pictureData);
    }

    @Override
    public byte[] getValue() {
        return (byte[])this.value;
    }

    @Override
    public void setValue(Object value){
        throw new UnsupportedOperationException("ImageCell donn't support setValue");
    }

    @Override
    public Object getLabel() {
        throw new UnsupportedOperationException("ImageCell donn't support getLabel");
    }

    public PictureType getPictureType() {
        return pictureType;
    }
}

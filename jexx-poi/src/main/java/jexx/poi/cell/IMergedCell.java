package jexx.poi.cell;

/**
 * cell's interface support to custom a range cell!
 *
 * jexx-poi 最使用的单元格类
 *
 * @author jeff
 * @since 2019/5/7
 */
public interface IMergedCell extends ICell, IRowCell {

    /**
     * the last cell's row num, base 1; the last cell is at bottom right corner
     *
     * @return the row's num of last row
     */
    int getLastRowNum();

    /**
     * the last cell's column num, base 1; the last cell is at bottom right corner
     *
     * @return the column's num of last column
     */
    int getLastColumnNum();

    /**
     * the last cell's column no, base 1; the last cell is at bottom right corner
     *
     * @return the column's no of last column
     */
    String getLastColumnNo();

    /**
     * indicate whether the cell is merged
     * @return bool
     */
    default boolean isMerged(){
        return getFirstRowNum() != getLastRowNum() || getFirstColumnNum() != getLastColumnNum();
    }

}

package jexx.poi.cell;

import jexx.poi.row.Row;
import jexx.poi.style.IWrapCellStyle;
import jexx.poi.util.CellOperateUtil;
import jexx.random.RandomUtil;
import jexx.util.Assert;

/**
 * abstract cell
 *
 * @author jeff
 * @since 2019/5/7
 */
public abstract class AbstractCell implements IMergedCell, IRowCell {

    protected String id;
    protected int firstRowNum;
    protected int firstColumnNum;
    protected int lastRowNum;
    protected int lastColumnNum;
    /**
     * value 是否为多值
     */
    protected boolean multiValue;
    protected Object value;
    protected Object label;
    /**
     * 标记 label 属性 是否被设置
     */
    protected boolean labelSet;
    protected IWrapCellStyle cellStyle;

    protected Row row;

    public AbstractCell(int firstRowNum, int firstColumnNum, int lastRowNum, int lastColumnNum, Object value, IWrapCellStyle cellStyle) {
        Assert.isTrue(firstRowNum <= lastRowNum && firstColumnNum <= lastColumnNum,
                "firstRowNum={}, firstColumnNum={},lastRowNum={},lastColumnNum={}", firstRowNum, firstColumnNum, lastRowNum, lastColumnNum);

        this.id = RandomUtil.simpleUUID();
        this.firstRowNum = firstRowNum;
        this.firstColumnNum = firstColumnNum;
        this.lastRowNum = lastRowNum;
        this.lastColumnNum = lastColumnNum;
        this.value = value;
        this.label = value;
        this.cellStyle = cellStyle;
    }

    public AbstractCell(int firstRowNum, int firstColumnNum, Object value, IWrapCellStyle cellStyle) {
        this(firstRowNum, firstColumnNum, firstRowNum, firstColumnNum, value, cellStyle);
    }

    public AbstractCell(Row row, int firstColumnNum, int lastColumnNum, Object value, IWrapCellStyle cellStyle) {
        this(row.getStartRowNum(), firstColumnNum, row.getEndRowNum(), lastColumnNum, value, cellStyle);
        this.row = row;
    }


    @Override
    public String getId() {
        return id;
    }

    @Override
    public int getFirstRowNum() {
        return this.row != null ? row.getStartRowNum() : firstRowNum;
    }

    @Override
    public int getLastRowNum() {
        return this.row != null ? row.getEndRowNum() : lastRowNum;
    }

    @Override
    public int getFirstColumnNum() {
        return firstColumnNum;
    }

    @Override
    public int getLastColumnNum() {
        return lastColumnNum;
    }

    @Override
    public String getFirstColumnNo() {
        return CellOperateUtil.toColumnLabel(getFirstColumnNum());
    }

    @Override
    public String getLastColumnNo() {
        return CellOperateUtil.toColumnLabel(getLastColumnNum());
    }

    @Override
    public boolean isMultiValue() {
        return multiValue;
    }

    public void setMultiValue(boolean multiValue) {
        this.multiValue = multiValue;
    }

    @Override
    public Object getValue() {
        return value;
    }

    @Override
    public void setValue(Object value) {
        this.value = value;
    }

    @Override
    public Object getLabel() {
        if(!labelSet){
            setValue(this.value);
        }
        return this.label;
    }

    @Override
    public void setLabel(Object label) {
        this.label = label;
        this.labelSet = true;
    }

    @Override
    public IWrapCellStyle getCellStyle() {
        return cellStyle;
    }

    @Override
    public void setCellStyle(IWrapCellStyle cellStyle) {
        this.cellStyle = cellStyle;
    }

    @Override
    public Row getRow() {
        return this.row;
    }
}

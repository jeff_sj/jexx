package jexx.poi.cell;

import jexx.poi.meta.IMeta;
import jexx.poi.meta.node.INode;
import jexx.poi.row.Row;
import jexx.poi.style.CellStyleSets;
import jexx.poi.style.IWrapCellStyle;
import jexx.poi.util.CellOperateUtil;
import jexx.util.StringUtil;

/**
 * MultiCell contain multiple cells
 *
 * @author jeff
 * @since 2019/5/7
 */
public class MergedCell extends AbstractCell implements IMergedCell, ICellSupportMeta {

    protected IMeta meta;
    protected boolean valid = true;
    protected ICellSupportMeta cellReference;

    public MergedCell(int firstRowNum, int firstColumnNum, int lastRowNum, int lastColumnNum, Object value, IWrapCellStyle cellStyle) {
        super(firstRowNum, firstColumnNum, lastRowNum, lastColumnNum, value, cellStyle);
    }

    public MergedCell(Row row, int firstColumnNum, int lastColumnNum, Object value, IWrapCellStyle cellStyle) {
        super(row, firstColumnNum, lastColumnNum, value, cellStyle);
    }

    public MergedCell(int firstRowNum, int firstColumnNum, int lastRowNum, int lastColumnNum, Object value) {
        this(firstRowNum, firstColumnNum, lastRowNum, lastColumnNum, value, null);
    }

    public MergedCell(Row row, int firstColumnNum, int lastColumnNum, Object value) {
        this(row, firstColumnNum, lastColumnNum, value, null);
    }

    public MergedCell(int firstRowNum, int firstColumnNum, int lastRowNum, int lastColumnNum) {
        this(firstRowNum, firstColumnNum, lastRowNum, lastColumnNum, null);
    }

    public MergedCell(Row row, int firstColumnNum, int lastColumnNum) {
        this(row, firstColumnNum, lastColumnNum, null);
    }

    public MergedCell(int firstRowNum, String firstColumnNo, int lastRowNum, String lastColumnNo, Object value, IWrapCellStyle cellStyle) {
        this(firstRowNum, CellOperateUtil.toColumnNum(firstColumnNo), lastRowNum, CellOperateUtil.toColumnNum(lastColumnNo), value, cellStyle);
    }

    public MergedCell(Row row, String firstColumnNo, String lastColumnNo, Object value, IWrapCellStyle cellStyle) {
        this(row, CellOperateUtil.toColumnNum(firstColumnNo), CellOperateUtil.toColumnNum(lastColumnNo), value, cellStyle);
    }

    public MergedCell(int firstRowNum, String firstColumnNo, int lastRowNum, String lastColumnNo, Object value) {
        this(firstRowNum, firstColumnNo, lastRowNum, lastColumnNo, value, null);
    }

    public MergedCell(Row row, String firstColumnNo, String lastColumnNo, Object value) {
        this(row, firstColumnNo, lastColumnNo, value, null);
    }

    public MergedCell(int firstRowNum, String firstColumnNo, int lastRowNum, String lastColumnNo) {
        this(firstRowNum, firstColumnNo, lastRowNum, lastColumnNo,  null);
    }

    public MergedCell(Row row, String firstColumnNo, String lastColumnNo) {
        this(row, firstColumnNo, lastColumnNo,  null);
    }

    @Override
    public Object getLabel() {
        if(!labelSet){
            Object label;
            Object[] fullValues = getFullValues();
            Object[] labels = new Object[fullValues.length];
            for (int i = 0; i < fullValues.length; i++) {
                if(supportMeta()){
                    INode node = this.meta.getNodeByFullValue(fullValues[i]);
                    if(node != null){
                        labels[i] = StringUtil.str(node.getLabel());
                    }
                }
                else{
                    labels[i] = fullValues[i];
                }
            }
            if(labels.length == 0){
                label = null;
            }
            else if(labels.length == 1){
                label = labels[0];
            }
            else{
                label = StringUtil.join(labels, ",");
            }
            super.setLabel(label);
        }
        return super.getLabel();
    }

    @Override
    public void setLabel(Object label) {
        if(supportMeta()){
            throw new UnsupportedOperationException("not support to set label");
        }
        super.setLabel(label);
    }

    @Override
    public boolean supportMeta() {
        return this.meta != null;
    }

    @Override
    public IMeta getMeta() {
        return this.meta;
    }

    public void setMeta(IMeta meta){
        this.meta = meta;
    }

    @Override
    public boolean isValid() {
        return valid;
    }

    public void valid(boolean valid){
        this.valid = valid;
    }

    @Override
    public ICellSupportMeta getCellReference() {
        return cellReference;
    }

    @Override
    public void setCellReference(ICellSupportMeta cellReference){
        this.cellReference = cellReference;
    }

}

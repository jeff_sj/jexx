package jexx.poi.cell;

import jexx.poi.meta.AbstractMeta;
import jexx.poi.meta.ArrayMeta;
import jexx.poi.meta.IMeta;
import jexx.poi.meta.TreeMeta;
import jexx.util.Assert;
import jexx.util.StringUtil;

import java.util.Stack;

/**
 * support cell's meta
 *
 * @author jeff
 * @since 2019/5/7
 */
public interface ICellSupportMeta extends ICell {

    char NAME_PREFIX = '_';

    /**
     *
     * Do support meta?
     *
     * @return support?
     */
    boolean supportMeta();

    /**
     * get the meta
     *
     * @return {@link IMeta}
     */
    IMeta getMeta();

    /**
     * valid the data
     *
     * @return valid
     */
    boolean isValid();

    /**
     * get the cell's reference; It can build the relation of any cell!
     * @return {@link ICellSupportMeta}
     */
    ICellSupportMeta getCellReference();

    void setCellReference(ICellSupportMeta cellReference);

    /**
     * 获取全路径值集合; 不支持meta时,默认返回实际值
     */
    default Object[] getFullValues(){
        Object[] values = getValues();
        Object[] fullValues = new Object[values.length];
        for (int i = 0; i < values.length; i++) {
            fullValues[i] = toFullValue(values[i]);
        }
        return fullValues;
    }

    /**
     * 值 转换为 全路径值;
     * 全路径值, 逐级递归引用cell的值组合后的值
     * @param value 实际单元格值
     * @return 全路径值
     */
    default Object toFullValue(Object value){
        if(supportMeta()){
            IMeta meta = getMeta();

            if(meta instanceof ArrayMeta){
                return value;
            }

            Stack<ICellSupportMeta> stack = new Stack<>();
            ICellSupportMeta refer = getCellReference();
            while(refer != null){
                stack.push(refer);
                refer = refer.getCellReference();
            }

            StringBuilder nameBuilder = new StringBuilder(meta.getName());
            if(!stack.isEmpty()){
                ICell fetch = stack.pop();
                while (fetch != null){
                    nameBuilder.append(AbstractMeta.SEPARATOR).append(fetch.getValue());
                    if(stack.isEmpty()){
                        break;
                    }
                    fetch = stack.pop();
                }
            }
            nameBuilder.append(AbstractMeta.SEPARATOR).append(value);
            return nameBuilder.toString();
        }
        return value;
    }

    /**
     * create name's name
     *
     * @return name's name
     */
    default String createNameName(){
        IMeta meta = getMeta();
        Assert.notNull(meta, "Cell's meta cannot be null!");

        StringBuilder nameBuilder = new StringBuilder("INDIRECT(SUBSTITUTE(CONCATENATE(");
        nameBuilder.append("\"").append(NAME_PREFIX).append("\",");
        nameBuilder.append(",\"").append(meta.getName()).append("\"");
        if(meta instanceof ArrayMeta){
            //skip
        }
        else if(meta instanceof TreeMeta){
            Stack<ICellSupportMeta> stack = new Stack<>();
            ICellSupportMeta refer = getCellReference();
            while(refer != null){
                stack.push(refer);
                refer = refer.getCellReference();
            }

            if(!stack.isEmpty()){
                ICell fetch = stack.pop();
                while (fetch != null){
                    nameBuilder.append(",\"").append(AbstractMeta.SEPARATOR).append("\",");
                    nameBuilder.append("$").append(fetch.getFirstColumnNo()).append("$").append(fetch.getFirstRowNum());
                    if(stack.isEmpty()){
                        break;
                    }
                    fetch = stack.pop();
                }
            }

        }
        else{
            throw new IllegalArgumentException(StringUtil.format("cannot handle this meta, meta[{}]!", meta.getClass()));
        }
        nameBuilder.append("),\" \",\"\"))");
        return  nameBuilder.toString();
    }

}

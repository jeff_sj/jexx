package jexx.poi;

import jexx.poi.header.Headers;
import jexx.poi.row.RowActionPredicate;

import java.util.List;
import java.util.Map;

/**
 * sheet读取器
 * @author jeff
 * @since 2019/10/8
 */
public interface SheetReader {

    /**
     * 设置跳过空行; 这会验证每条数据是否为空
     * @param skipEmptyRow 是否跳过空行
     * @return {@link SheetReader}
     */
    SheetReader setSkipEmptyRow(boolean skipEmptyRow);

    /**
     * 设置支持读取数据时去除两边空格，默认支持
     * @param supportTrimData 是否支持读取数据时去除两边空格
     */
    SheetReader setSupportTrimData(boolean supportTrimData);

    /**
     * 设置是否简单bean
     * @param isSimpleBean 是否简单
     * @return {@link SheetReader}
     */
    SheetReader setIsSimpleBean(boolean isSimpleBean);

    /**
     * 跳过当前行
     * @return {@link SheetReader}
     */
    SheetReader passCurrentRow();

    /**
     * 跳过指定几行
     * @param skipRowSize 几行
     * @return {@link SheetReader}
     */
    SheetReader skipRows(int skipRowSize);

    /**
     * 直接跳至某一行
     * @param rowNum 行号,从1开始
     * @return {@link SheetReader}
     */
    SheetReader jumpToRowNum(final int rowNum);


    /**
     * 读取当前行的数据,并跳到下一行
     * @param startColumnNum 开始列号
     * @param endColumnNum 结束列号
     * @param rowMapper {@link RowMapper}
     * @return 转换对象
     */
    <T> T readRowAndNext(final int startColumnNum, final int endColumnNum, final RowMapper<T> rowMapper);

    /**
     * 读取当前行数据,并跳到下一行
     * @param rowMapper {@link RowMapper}
     * @param <T> 转换泛型
     * @return 转换对象
     */
    <T> T readRowAndNext(final RowMapper<T> rowMapper);

    /**
     * 通过行转换类,把当前行到指定一行转换成想要的集合数据,并跳到下一行。
     * @param lastRowNum 读取结束行, 从1开始
     * @param rowMapper {@link RowMapper}
     * @param includeNull 行转换器得到的结果为空，是否包含
     * @param <T> 泛型
     * @return 对象集合
     */
    <T>  List<T> readRowsAndNext(final int lastRowNum, final RowMapper<T> rowMapper, boolean includeNull);

    /**
     * 通过行转换类,把当前行到最后一行转换成想要的集合数据,并跳到下一行。
     * @param rowMapper {@link RowMapper}
     * @param includeNull 行转换器得到的结果为空，是否包含
     * @param <T> 泛型
     * @return 对象集合
     */
    <T> List<T> readRowsAndNext(final RowMapper<T> rowMapper, boolean includeNull);

    /**
     * 通过行转换类,把当前行到最后一行转换成想要的集合数据,不包含空数据,并跳到下一行。
     * @param rowMapper {@link RowMapper}
     * @param <T> 泛型
     * @return 对象集合
     */
    <T> List<T> readRowsAndNext(final RowMapper<T> rowMapper);

    /**
     * 读取当前行,指定范围列的数据,并跳到下一行。
     * @param startColumnNum 开始列号,从1开始
     * @param endColumnNum 结束列号,从1开始
     * @return 一行的范围数据
     */
    List<Object> readListRowAndNext(final int startColumnNum, final int endColumnNum);

    /**
     * 读取当前行,指定范围列的数据,并跳到下一行。
     * @param startColumnNum 开始列号,从1开始
     * @return 一行的范围数据
     */
    List<Object> readListRowAndNext(int startColumnNum);

    /**
     * 读取当前行,指定范围列的数据,并跳到下一行。
     * @return 一行的范围数据
     */
    List<Object> readListRowAndNext();

    /**
     * 读取当前行到指定行,和指定范围列的数据,并跳到下一行。
     * @param lastRowNum 读取最后行
     * @param startColumnNum 开始列号
     * @param endColumnNum 结束列号
     * @return 行数据集合
     */
    List<List<Object>> readListRowsAndNext(final int lastRowNum, final int startColumnNum, final int endColumnNum);

    List<List<Object>> readListRowsAndNext(final int startColumnNum, final int endColumnNum);

    List<List<Object>> readListRowsAndNext(final int startColumnNum);

    List<List<Object>> readListRowsAndNext();

    /**
     * 读取当前行,指定范围列的数据,并跳到下一行。
     * @param headers headers
     * @return 一行数据
     */
    Map<String, Object> readMapRowAndNext(final Headers headers);

    List<Map<String, Object>> readMapRowsAndNext(final int lastRowNum, final Headers headers);

    List<Map<String, Object>> readMapRowsAndNext(final Headers headers);

    <T> T readBeanRowAndNext(final Class<T> clazz, final Headers headers);

    <T> List<T> readBeanRowsAndNext(final int lastRowNum, final Class<T> clazz, final Headers headers, RowActionPredicate<T> skipRow);

    /**
     * 读取行数据转换为对象,并跳到下一行。
     * @param clazz 转换对象
     * @param headers {@link Headers}
     * @param skipRow 跳行判断
     * @param <T> 泛型
     * @return 对象集合
     */
    <T> List<T> readBeanRowsAndNext(final Class<T> clazz, final Headers headers, RowActionPredicate<T> skipRow);


    /**
     * 读取行数据转换为bean对象,并跳到下一行。
     * @param clazz 转换对象
     * @param headers {@link Headers}
     * @param <T> 泛型
     * @return 对象集合
     */
    <T> List<T> readBeanRowsAndNext(final Class<T> clazz, final Headers headers);

    /**
     * 验证当前行是否匹配header
     * @param headers {@link Headers}
     * @return 是否匹配
     */
    boolean validateHeaders(Headers headers);

}

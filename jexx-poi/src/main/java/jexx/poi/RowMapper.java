package jexx.poi;

import jexx.poi.cell.IMergedCell;

import java.util.List;

/**
 * excel行数据转换
 *
 * @author jeff
 */
@FunctionalInterface
public interface RowMapper<T> {

    /**
     * 行转想要的新对象
     * @param cells 行中符合条件的单元格
     * @param rowNum 行号，从1开始
     * @return 新对象
     */
    T mapRow(List<IMergedCell> cells, int rowNum);

}

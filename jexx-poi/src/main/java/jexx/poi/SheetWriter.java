package jexx.poi;

import jexx.poi.cell.IMergedCell;
import jexx.poi.header.Headers;
import jexx.poi.row.RowContext;
import jexx.poi.style.CellStyleMapper;
import jexx.poi.writer.SheetCellWriter;
import jexx.poi.writer.SheetRowWriter;

import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;

/**
 * sheet写入器抽象接口
 * @author jeff
 * @since 2019/10/2
 */
public interface SheetWriter extends SheetRowWriter, SheetCellWriter {

    /**
     * 获取当前行号,从1开始
     * @return 当前行号
     */
    int getCurrentRowNum();

    /**
     * 跳过当前行
     */
    void passCurrentRow();

    /**
     * 跳过指定几行
     * @param skipRowNum 指定几行
     */
    void skipRows(final int skipRowNum);

    /**
     * 直接跳至某一行
     * @param rowNum 指定行号
     */
    void jumpToRowNum(final int rowNum);

    //------------------------------------------------------------ header

    /**
     * 指定行写入headers
     * @param rowNum 指定行
     * @param headers {@link Headers}
     * @param cellStyleMapper {@link CellStyleMapper}
     */
    void writeHeader(final int rowNum, final Headers headers, final CellStyleMapper cellStyleMapper);

    /**
     * 指定行写入headers
     * @param rowNum 指定行
     * @param headers {@link Headers}
     */
    void writeHeader(final int rowNum, final Headers headers);

    /**
     * 下一行写入header
     * @param headers {@link Headers}
     * @param cellStyleMapper {@link CellStyleMapper}
     */
    void writeHeader(final Headers headers, final CellStyleMapper cellStyleMapper);

    /**
     * 下一行写入header
     * @param headers {@link Headers}
     */
    void writeHeader(final Headers headers);

    //------------------------------------------------------------ write list rows

    /**
     * 指定行指定列开始写入多行数据
     * @param startRowNum 开始行
     * @param columnNum 指定列
     * @param rows 多行数据
     * @param cellStyleMapper {@link CellStyleMapper}
     * @return 结束行, 未写入数据返回-1
     */
    int writeRows(int startRowNum, int columnNum, List<?> rows, final CellStyleMapper cellStyleMapper);

    /**
     * 指定行指定列开始写入多行数据
     * @param startRowNum 开始行
     * @param columnNum 指定列
     * @param rows 多行数据
     * @return 结束行, 未写入数据返回-1
     */
    int writeRows(int startRowNum, int columnNum, List<?> rows);

    /**
     * 下一行指定列开始写入多行数据
     * @param columnNum 指定列
     * @param rows 多行数据
     * @param cellStyleMapper {@link CellStyleMapper}
     * @return 结束行, 未写入数据返回-1
     */
    int writeRows(int columnNum, List<?> rows, final CellStyleMapper cellStyleMapper);

    /**
     * 下一行指定列开始写入多行数据
     * @param columnNum 指定列
     * @param rows 多行数据
     * @return 结束行, 未写入数据返回-1
     */
    int writeRows(int columnNum, List<?> rows);

    /**
     * 下一行第一列开始写入多行数据
     * @param rows 多行数据
     * @param cellStyleMapper {@link CellStyleMapper}
     * @return 结束行, 未写入数据返回-1
     */
    int writeRows(List<?> rows, final CellStyleMapper cellStyleMapper);

    /**
     * 下一行第一列开始写入多行数据
     * @param rows 多行数据
     * @return 结束行, 未写入数据返回-1
     */
    int writeRows(List<?> rows);

    //------------------------------------------------------------ writeRows

    /**
     * 指定行指定header写入多行数据
     * @param startRowNum 开始行
     * @param rows 多行数据
     * @param headers {@link Headers}
     * @param consumer 加工cell
     * @return 结束行, 未写入数据返回-1
     */
    <T> int writeRows(int startRowNum, List<T> rows, Headers headers, BiConsumer<IMergedCell, T> consumer);

    /**
     * 指定行指定header写入多行数据
     * @param startRowNum 开始行
     * @param rows 多行数据
     * @param headers {@link Headers}
     * @param cellStyleMapper {@link CellStyleMapper}
     * @return 结束行, 未写入数据返回-1
     */
    default <T> int writeRows(int startRowNum, List<T> rows, Headers headers, final CellStyleMapper cellStyleMapper){
        return writeRows(startRowNum, rows, headers, (cell,bean)->{
            if(cellStyleMapper != null){
                cell.setCellStyle(cellStyleMapper.warpCellStyle(cell));
            }
        });
    }

    /**
     * 指定行指定header写入多行数据
     * @param startRowNum 开始行
     * @param rows 多行数据
     * @param headers {@link Headers}
     * @return 结束行, 未写入数据返回-1
     */
    default <T> int writeRows(int startRowNum, List<T> rows, Headers headers){
        return writeRows(startRowNum, rows, headers, (cell,bean)->{});
    }

    /**
     * 下一行指定header写入多行数据
     * @param rows 多行数据
     * @param headers {@link Headers}
     * @param consumer 加工cell
     * @return 结束行, 未写入数据返回-1
     */
    <T> int writeRows(List<T> rows, Headers headers, BiConsumer<IMergedCell, T> consumer);

    /**
     * 下一行指定header写入多行数据
     * @param rows 多行数据
     * @param headers {@link Headers}
     * @param cellStyleMapper {@link CellStyleMapper}
     * @return 结束行, 未写入数据返回-1
     */
    default <T> int writeRows(List<T> rows, Headers headers, final CellStyleMapper cellStyleMapper){
        return writeRows(rows, headers, (cell,bean)->{
            if(cellStyleMapper != null){
                cell.setCellStyle(cellStyleMapper.warpCellStyle(cell));
            }
        });
    }

    /**
     * 下一行行指定header写入多行数据
     * @param rows 多行数据
     * @param headers {@link Headers}
     * @return 结束行, 未写入数据返回-1
     */
    default <T> int writeRows(List<T> rows, Headers headers){
        return writeRows(rows, headers, (cell,bean)->{});
    }

    //------------------------------------------------------------ writeMapRows

    /**
     * 指定行指定header写入多行数据
     * @param startRowNum 开始行
     * @param rows 多行数据
     * @param headers {@link Headers}
     * @param cellStyleMapper {@link CellStyleMapper}
     * @return 结束行, 未写入数据返回-1
     */
    int writeMapRows(int startRowNum, List<Map<String, Object>> rows, Headers headers, final CellStyleMapper cellStyleMapper);

    /**
     * 指定行指定header写入多行数据
     * @param startRowNum 开始行
     * @param rows 多行数据
     * @param headers {@link Headers}
     * @return 结束行, 未写入数据返回-1
     */
    int writeMapRows(int startRowNum, List<Map<String, Object>> rows, Headers headers);

    /**
     * 下一行行指定header写入多行数据
     * @param rows 多行数据
     * @param headers {@link Headers}
     * @param cellStyleMapper {@link CellStyleMapper}
     * @return 结束行, 未写入数据返回-1
     */
    int writeMapRows(List<Map<String, Object>> rows, Headers headers, final CellStyleMapper cellStyleMapper);

    /**
     * 下一行行指定header写入多行数据
     * @param rows 多行数据
     * @param headers {@link Headers}
     * @return 结束行, 未写入数据返回-1
     */
    int writeMapRows(List<Map<String, Object>> rows, Headers headers);

    //----------------------------------------------------------------填充

    /**
     * 指定起始行到结束行填充多行数据
     * @param fillStartRowNum 起始行
     * @param fillEndRowNum 结束行
     * @param rows 多行数据
     * @param headers {@link Headers}
     */
    void fill(int fillStartRowNum, int fillEndRowNum, List<?> rows, Headers headers);

    //----------------------------------------------------------------write row

    /**
     * 指定行指定header写入一个对象
     * @param startRowNum 开始行
     * @param rowData 对象
     * @param headers {@link Headers}
     * @param cellStyleMapper {@link CellStyleMapper}
     * @return {@link RowContext}
     */
    RowContext writeRow(int startRowNum, Object rowData, Headers headers, final CellStyleMapper cellStyleMapper);

    /**
     * 指定行指定header写入一个对象
     * @param startRowNum 开始行
     * @param rowData 对象
     * @param headers {@link Headers}
     * @return {@link RowContext}
     */
    RowContext writeRow(int startRowNum, Object rowData, Headers headers);

    /**
     * 下一行指定header写入一个对象
     * @param rowData 对象
     * @param headers {@link Headers}
     * @param cellStyleMapper {@link CellStyleMapper}
     * @return {@link RowContext}
     */
    RowContext writeRow(Object rowData, Headers headers, final CellStyleMapper cellStyleMapper);

    /**
     * 下一行指定header写入一个对象
     * @param rowData 对象
     * @param headers {@link Headers}
     * @return {@link RowContext}
     */
    RowContext writeRow(Object rowData, Headers headers);

    //----------------------------------------------------------------写入list类型数据

    /**
     * 指定行指定列 开始写入一行数据
     * @param startRowNum 开始写入行
     * @param columnNum 列号
     * @param rowData 一行数据
     * @param cellStyleMapper {@link CellStyleMapper}
     * @return {@link RowContext}
     */
    RowContext writeListRow(int startRowNum, int columnNum, List<?> rowData, final CellStyleMapper cellStyleMapper);

    /**
     * 下一行 指定列 开始写入一行数据
     * @param columnNum 列号
     * @param rowData 一行数据
     * @param cellStyleMapper {@link CellStyleMapper}
     * @return {@link RowContext}
     */
    RowContext writeListRow(int columnNum, List<?> rowData, final CellStyleMapper cellStyleMapper);

    /**
     * 下一行 第一列 开始写入一行数据
     * @param rowData 一行数据
     * @param cellStyleMapper {@link  CellStyleMapper}
     * @return {@link RowContext}
     */
    RowContext writeListRow(List<?> rowData, final CellStyleMapper cellStyleMapper);

    /**
     * 下一行 第一列 开始写入一行数据
     * @param rowData 一行数据
     * @return {@link RowContext}
     */
    RowContext writeListRow(List<?> rowData);

    //----------------------------------------------------------------write map data

    /**
     * 指定行写入map
     * @param rowNum 指定行
     * @param map map
     * @param headers {@link Headers}
     * @param cellStyleMapper {@link CellStyleMapper}
     * @return {@link RowContext}
     */
    RowContext writeMapRow(int rowNum, final Map<String, Object> map, final Headers headers, final CellStyleMapper cellStyleMapper);

    /**
     * 指定行写入map
     * @param rowNum 指定行
     * @param map map数据
     * @param headers {@link Headers}
     * @return {@link RowContext}
     */
    RowContext writeMapRow(int rowNum, final Map<String, Object> map, final Headers headers);

    /**
     * 下一行写入map
     * @param rowData map数据
     * @param headers {@link Headers}
     * @param cellStyleMapper {@link CellStyleMapper}
     * @return {@link RowContext}
     */
    RowContext writeMapRow(Map<String, Object> rowData,final Headers headers, final CellStyleMapper cellStyleMapper);

    /**
     * 下一行写入map
     * @param rowData map数据
     * @param headers {@link Headers}
     * @return {@link RowContext}
     */
    RowContext writeMapRow(Map<String, Object> rowData, final Headers headers);

    //--------------------------------------------------------- constraint

    /**
     * 指定行开始构建指定行数的单元格约束
     * @param startRowNum 指定开始行
     * @param rowCount 约束行数
     * @param headers {@link Headers}
     */
    void validData(int startRowNum, int rowCount, Headers headers);

    /**
     * 下一行开始构建指定行数的单元格约束
     * @param rowCount 约束行数
     * @param headers {@link Headers}
     */
    void validData(int rowCount, Headers headers);

    //------------------------------------------------------------ spread column's width

    /**
     * 指定某几列自动扩展列宽
     * @param columnNums 指定列号
     */
    void autoSizeColumn(int... columnNums);

    /**
     * 指定 header 对应的列 自动扩展 列宽
     * @param headers {@link Headers}
     */
    void autoSizeColumn(Headers headers);

    /**
     * 小于等于指定列号的列 自动扩展列宽
     * @param maxColumnNum 指定列号
     */
    void autoSizeColumnWithMaxColumnNum(int maxColumnNum);

    /**
     * 设置指定列指定宽度
     * @param columnNum 指定列
     * @param width 列宽
     */
    void setColumnWidth(int columnNum, int width);

}

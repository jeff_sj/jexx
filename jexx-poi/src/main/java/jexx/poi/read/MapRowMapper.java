package jexx.poi.read;

import jexx.poi.RowMapper;
import jexx.poi.cell.IMergedCell;
import jexx.poi.header.Headers;
import jexx.poi.header.IDataHeader;
import jexx.poi.row.RowMap;
import jexx.util.MapUtil;
import jexx.util.StringUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MapRowMapper implements RowMapper<Map<String, Object>> {

    private final Headers headers;
    private final boolean supportTrimData;
    private final boolean skipEmptyRow;

    public MapRowMapper(Headers headers, boolean supportTrimData, boolean skipEmptyRow) {
        this.headers = headers;
        this.supportTrimData = supportTrimData;
        this.skipEmptyRow = skipEmptyRow;
    }

    @Override
    public Map<String, Object> mapRow(List<IMergedCell> cells, int rowNum) {
        RowMap labelMap = new RowMap();
        for(IMergedCell cell : cells){
            IDataHeader header = headers.getDataHeaderByColumnNum(cell.getFirstColumnNum());
            if(header == null){
                continue;
            }
            String key = header.getKey();
            Object value = cell.getValue();
            if(supportTrimData){
                if(value instanceof String){
                    value = StringUtil.trim(value.toString());
                }
            }
            if(header.getUnwrapLabelFunction() != null){
                value = header.getUnwrapLabelFunction().unwrap(value);
            }
            labelMap.putIfAbsent(key, value);
        }

        Map<String, Object> dataMap = new HashMap<>(16);
        for(IDataHeader dataHeader : headers.getDataHeaders()){
            Object value = dataHeader.getValueByLabel(labelMap);
            if(value != null){
                dataMap.put(dataHeader.getKey(), value);
            }
        }

        if(skipEmptyRow && checkMapEmpty(dataMap)){
            return null;
        }

        return dataMap;
    }

    private boolean checkMapEmpty(Map<String, Object> map){
        if(map == null || MapUtil.isEmpty(map)){
            return true;
        }
        Object value;
        for (Map.Entry<String, Object> m : map.entrySet()){
            value = m.getValue();
            if(value != null){
                if(value instanceof String && StringUtil.isEmpty((String)value)){
                    continue;
                }
                return false;
            }
        }
        return true;
    }

}

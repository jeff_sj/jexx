package jexx.poi.read;

import org.apache.poi.ss.usermodel.*;

import java.util.Iterator;

public class DelegateRow implements Row {

    private static final Iterator<Cell> EMPTY = new Iterator<Cell>() {
        @Override
        public boolean hasNext() {
            return false;
        }

        @Override
        public Cell next() {
            return null;
        }
    };

    private final Row row;
    /**
     * 行号， 从0开始，保持和row统一
     */
    private final int rowNum;

    public DelegateRow(Row row) {
        this.row = row;
        this.rowNum = row.getRowNum();
    }

    public DelegateRow(int rowNum) {
        this.row = null;
        this.rowNum = rowNum;
    }

    public Row getDelegate() {
        return row;
    }

    @Override
    public Cell createCell(int column) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Cell createCell(int column, int type) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Cell createCell(int column, CellType type) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void removeCell(Cell cell) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void setRowNum(int rowNum) {
        throw new UnsupportedOperationException();
    }

    @Override
    public int getRowNum() {
        return rowNum;
    }

    @Override
    public Cell getCell(int cellnum) {
        return row != null ? row.getCell(cellnum) : null;
    }

    @Override
    public Cell getCell(int cellnum, MissingCellPolicy policy) {
        return row != null ? row.getCell(cellnum, policy) : null;
    }

    @Override
    public short getFirstCellNum() {
        return row != null ? row.getFirstCellNum() : -1;
    }

    @Override
    public short getLastCellNum() {
        return row != null ? row.getLastCellNum() : -1;
    }

    @Override
    public int getPhysicalNumberOfCells() {
        return row != null ? row.getPhysicalNumberOfCells() : 0;
    }

    @Override
    public void setHeight(short height) {
        if(row != null){
            row.setHeight(height);
        }
    }

    @Override
    public void setZeroHeight(boolean zHeight) {
        if(row != null){
            row.setZeroHeight(zHeight);
        }
    }

    @Override
    public boolean getZeroHeight() {
        return row != null && row.getZeroHeight();
    }

    @Override
    public void setHeightInPoints(float height) {
        if(row != null){
            row.setHeightInPoints(height);
        }
    }

    @Override
    public short getHeight() {
        return row != null ? row.getHeight() : 0;
    }

    @Override
    public float getHeightInPoints() {
        return row != null ? row.getHeightInPoints() : 0;
    }

    @Override
    public boolean isFormatted() {
        return row != null && row.isFormatted();
    }

    @Override
    public CellStyle getRowStyle() {
        return row != null ? row.getRowStyle() : null;
    }

    @Override
    public void setRowStyle(CellStyle style) {
        if(row != null){
            row.setRowStyle(style);
        }
    }

    @Override
    public Iterator<Cell> cellIterator() {
        return row != null ? row.cellIterator() : null;
    }

    @Override
    public Sheet getSheet() {
        throw new UnsupportedOperationException();
    }

    @Override
    public int getOutlineLevel() {
        return row != null ? row.getOutlineLevel() : 0;
    }

    @Override
    public Iterator<Cell> iterator() {
        return row != null ? row.iterator() : EMPTY;
    }

}

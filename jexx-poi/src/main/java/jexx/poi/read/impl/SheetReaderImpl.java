package jexx.poi.read.impl;

import jexx.bean.BeanUtil;
import jexx.bean.DyBean;
import jexx.bean.DyBeanFactory;
import jexx.poi.RowMapper;
import jexx.poi.SheetReader;
import jexx.poi.cell.ICell;
import jexx.poi.cell.IMergedCell;
import jexx.poi.header.Headers;
import jexx.poi.header.IDataHeader;
import jexx.poi.read.MapRowMapper;
import jexx.poi.read.SheetCellReader;
import jexx.poi.row.RowActionPredicate;
import jexx.poi.row.RowScanAction;
import jexx.util.Assert;
import jexx.util.CollectionUtil;
import jexx.util.ReflectUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * sheet reader
 *
 * @author jeff
 */
public class SheetReaderImpl extends SheetCellReader implements SheetReader {

    private static final Logger LOG = LoggerFactory.getLogger(SheetReaderImpl.class);

    /**
     * 跳过空行, 空行为当前行的单元格为空, 只对批量读取数据时生效
     */
    private boolean skipEmptyRow = false;
    /**
     * 是否支持在读取单元格数据时去除两端空格;
     * <b>读取时候去除两边空格会导致meta读取不匹配</b>
     */
    @Deprecated
    protected boolean supportTrimData = false;

    /**
     * 读取对象是否为简单实体，默认是
     */
    protected boolean simpleBean = true;

    public SheetReaderImpl(Sheet sheet) {
        super(sheet);
    }

    @Override
    public SheetReaderImpl setSkipEmptyRow(boolean skipEmptyRow){
        this.skipEmptyRow = skipEmptyRow;
        return this;
    }

    @Override
    public SheetReaderImpl setSupportTrimData(boolean supportTrimData) {
        this.supportTrimData = supportTrimData;
        return this;
    }

    @Override
    public SheetReader setIsSimpleBean(boolean isSimpleBean) {
        this.simpleBean = isSimpleBean;
        return this;
    }

    @Override
    public SheetReaderImpl passCurrentRow(){
        skipRows(1);
        return this;
    }


    @Override
    public SheetReaderImpl skipRows(int skipRowSize){
        doSkipRow(skipRowSize);
        return this;
    }

    @Override
    public SheetReaderImpl jumpToRowNum(final int rowNum){
        Assert.isTrue(rowNum > 0, "rowNum is not illegal");
        doJumpToRow(rowNum);
        return this;
    }

    //------------------------------------------------------------readRow

    protected <T> T readRow(Row row, final int startColumnNum, final int endColumnNum, final RowMapper<T> rowMapper){
        int rowNum = row.getRowNum();
        int minColumnNum = Math.max(1, startColumnNum);
        int lastCellNum = getLastCellNumAtOneRow(row);
        if(lastCellNum == -1){
            return null;
        }
        int maxColumnNum = Math.min(lastCellNum, endColumnNum);

        List<IMergedCell> cells = readCellsAtOneRow(row, minColumnNum, maxColumnNum);
        return rowMapper.mapRow(cells, rowNum);
    }

    protected <T> T readRowAndNext(Row row, final int startColumnNum,  final int endColumnNum, final RowMapper<T> rowMapper){
        T t = readRow(row, startColumnNum, endColumnNum, rowMapper);
        doNextRowIfHasNext();
        return t;
    }

    protected <T> T readRow(Row row, final RowMapper<T> rowMapper){
        int rowNum = row.getRowNum();
        int minColumnNum = 1;
        int lastCellNum = getLastCellNumAtOneRow(row);
        if(lastCellNum == -1){
            return null;
        }

        List<IMergedCell> cells = readCellsAtOneRow(row, minColumnNum, lastCellNum);
        return rowMapper.mapRow(cells, rowNum);
    }

    protected <T> T readRowAndNext(Row row, final RowMapper<T> rowMapper){
        T t = readRow(row, rowMapper);
        doNextRowIfHasNext();
        return t;
    }

    protected <T> List<T> readRows(final int lastRowNum, final int startColumnNum, final int endColumnNum, final RowMapper<T> rowMapper, RowActionPredicate<T> skipRow){
        List<T> list = new ArrayList<>();
        if(getCurrentRowNum() > lastRowNum){
            return list;
        }
        int currentRowNum = getCurrentRowNum();
        Row row;
        int minColumnNum = Math.max(1, startColumnNum);
        while(currentRowNum <= lastRowNum){
            row = getCurrentRow();
            int lastCellNum = getLastCellNumAtOneRow(row);
            List<IMergedCell> cells = readCellsAtOneRow(row, minColumnNum, Math.min(lastCellNum, endColumnNum));
            T t = rowMapper.mapRow(cells, currentRowNum);
            if(t != null){
                if(skipRow == null){
                    list.add(t);
                }
                else{
                    RowScanAction action = skipRow.action(t, currentRowNum);
                    if(RowScanAction.STOP == action){
                        break;
                    }
                    else if(RowScanAction.KEEP_AND_STOP == action){
                        list.add(t);
                        break;
                    }
                    else{
                        list.add(t);
                    }
                }
            }

            if(!hasNextRow()){
                break;
            }
            doNextRow();
            currentRowNum = getCurrentRowNum();
        }
        return list;
    }

    protected <T> List<T> readRowsAndNext(final int lastRowNum, final int startColumnNum,  final int endColumnNum, final RowMapper<T> rowMapper, RowActionPredicate<T> skipRow){
        List<T> list = readRows(lastRowNum, startColumnNum, endColumnNum, rowMapper, skipRow);
        doNextRowIfHasNext();
        return list;
    }

    @Override
    public <T> T readRowAndNext(int startColumnNum, int endColumnNum, RowMapper<T> rowMapper) {
        Assert.isTrue(endColumnNum >= startColumnNum, "endColumnNum[{}] must be equal or greater than startColumnNum[{}]", endColumnNum, startColumnNum);
        Row row = getCurrentRow();
        return readRowAndNext(row, startColumnNum, endColumnNum, rowMapper);
    }

    @Override
    public <T> T readRowAndNext(final RowMapper<T> rowMapper){
        Row row = getCurrentRow();
        return readRowAndNext(row, rowMapper);
    }

    @Override
    public <T> List<T> readRowsAndNext(final int lastRowNum, final RowMapper<T> rowMapper, boolean includeNull){
        List<T> list = new ArrayList<>();

        int currentRowNum = getCurrentRowNum();
        if(currentRowNum > lastRowNum){
            return list;
        }

        while(currentRowNum <= lastRowNum){
            T t = readRowAndNext(getCurrentRow(), rowMapper);
            if(t != null || includeNull){
                list.add(t);
            }
            if(!hasNextRow()){
                break;
            }
            currentRowNum = getCurrentRowNum();
        }
        return list;
    }

    @Override
    public <T> List<T> readRowsAndNext(final RowMapper<T> rowMapper, boolean includeNull){
        return readRowsAndNext(Integer.MAX_VALUE, rowMapper, includeNull);
    }

    @Override
    public <T> List<T> readRowsAndNext(final RowMapper<T> rowMapper){
       return readRowsAndNext(rowMapper, false);
    }

    //------------------------------------------------------------readListRow

    @Override
    public List<Object> readListRowAndNext(final int startColumnNum, final int endColumnNum){
        Assert.isTrue(startColumnNum > 0 && endColumnNum >= startColumnNum, "startColumnNum={},endColumnNum={}", startColumnNum, endColumnNum);
        return readRowAndNext(startColumnNum, endColumnNum, (rowData, rowNum)-> rowData.stream().map(ICell::getValue).collect(Collectors.toList()));
    }

    @Override
    public List<Object> readListRowAndNext(int startColumnNum){
        return readListRowAndNext(startColumnNum, getLastCellNumOfCurrentRow());
    }

    @Override
    public List<Object> readListRowAndNext(){
        return readListRowAndNext(1);
    }

    @Override
    public List<List<Object>> readListRowsAndNext(final int lastRowNum, final int startColumnNum, final int endColumnNum){
        List<List<Object>> list = new ArrayList<>();

        int currentRowNum = getCurrentRowNum();
        if(currentRowNum > lastRowNum){
            return list;
        }
        while(currentRowNum <= lastRowNum){
            List<Object> t = readRowAndNext(getCurrentRow(), (rowData, rowNum)-> rowData.stream().map(ICell::getValue).collect(Collectors.toList()));
            if(t != null){
                list.add(t);
            }
            if(!hasNextRow()){
                break;
            }
            currentRowNum = getCurrentRowNum();
        }
        return list;
    }

    @Override
    public List<List<Object>> readListRowsAndNext(final int startColumnNum, final int endColumnNum){
        return readListRowsAndNext(Integer.MAX_VALUE, startColumnNum, endColumnNum);
    }

    @Override
    public List<List<Object>> readListRowsAndNext(final int startColumnNum){
        int lastCellNum = getLastCellNumOfCurrentRow();
        return readListRowsAndNext(startColumnNum, lastCellNum);
    }

    @Override
    public List<List<Object>> readListRowsAndNext(){
        return readListRowsAndNext(1);
    }


    //------------------------------------------------------------readMapRow

    @Override
    public Map<String, Object> readMapRowAndNext(final Headers headers){
        Assert.isTrue(headers != null && CollectionUtil.isNotEmpty(headers.getHeaders()), "headers not empty");

        int minHeaderNum = 0;
        int maxHeaderNum = 0;
        for(IDataHeader dataHeader : headers.getDataHeaders()){
            if(minHeaderNum == 0){
                minHeaderNum = dataHeader.getStartColumnNum();
                maxHeaderNum = dataHeader.getStartColumnNum();
            }
            else{
                minHeaderNum = Math.min(minHeaderNum, dataHeader.getStartColumnNum());
                maxHeaderNum = Math.max(maxHeaderNum, dataHeader.getStartColumnNum());
            }
        }
        Row row = getCurrentRow();
        return readRowAndNext(row, minHeaderNum, maxHeaderNum, new MapRowMapper(headers, this.supportTrimData, this.skipEmptyRow));
    }

    @Override
    public List<Map<String, Object>> readMapRowsAndNext(final int lastRowNum, final Headers headers){
        Assert.isTrue(headers != null && CollectionUtil.isNotEmpty(headers.getHeaders()), "headers not empty");

        int minHeaderNum = 0;
        int maxHeaderNum = 0;
        for(IDataHeader dataHeader : headers.getDataHeaders()){
            if(minHeaderNum == 0){
                minHeaderNum = dataHeader.getStartColumnNum();
                maxHeaderNum = dataHeader.getStartColumnNum();
            }
            else{
                minHeaderNum = Math.min(minHeaderNum, dataHeader.getStartColumnNum());
                maxHeaderNum = Math.max(maxHeaderNum, dataHeader.getStartColumnNum());
            }
        }
        return readRowsAndNext(lastRowNum, minHeaderNum, maxHeaderNum, new MapRowMapper(headers, this.supportTrimData, this.skipEmptyRow), null);
    }

    @Override
    public List<Map<String, Object>> readMapRowsAndNext(final Headers headers){
        return readMapRowsAndNext(Integer.MAX_VALUE, headers);
    }

    //-----------------------------------------bean

    @Override
    public <T> T readBeanRowAndNext(final Class<T> clazz, final Headers headers){
        Map<String, Object> map = readMapRowAndNext(headers);
        return BeanUtil.toBean(clazz, map);
    }

    @Override
    public <T> List<T> readBeanRowsAndNext(final int lastRowNum, final Class<T> clazz, final Headers headers, RowActionPredicate<T> skipRow){
        int minHeaderNum = 0;
        int maxHeaderNum = 0;
        for(IDataHeader dataHeader : headers.getDataHeaders()){
            if(minHeaderNum == 0){
                minHeaderNum = dataHeader.getStartColumnNum();
                maxHeaderNum = dataHeader.getStartColumnNum();
            }
            else{
                minHeaderNum = Math.min(minHeaderNum, dataHeader.getStartColumnNum());
                maxHeaderNum = Math.max(maxHeaderNum, dataHeader.getStartColumnNum());
            }
        }

        MapRowMapper rowMapper = new MapRowMapper(headers, this.supportTrimData, this.skipEmptyRow);
        return readRowsAndNext(lastRowNum, minHeaderNum, maxHeaderNum, (cells, rowNum)-> {
            Map<String, Object> map = rowMapper.mapRow(cells, rowNum);
            if(map == null){
                return null;
            }

            T t;
            if(simpleBean){
                t = BeanUtil.toBean(clazz, map);
            }
            else{
                t = ReflectUtil.newInstance(clazz);
                DyBean dyBean = DyBeanFactory.createDyBean(t,true, true);
                map.forEach(dyBean::setPropertyValue);
            }
            return t;
        }, skipRow);
    }

    @Override
    public <T> List<T> readBeanRowsAndNext(final Class<T> clazz, final Headers headers, RowActionPredicate<T> skipRow){
        return readBeanRowsAndNext(Integer.MAX_VALUE, clazz, headers, skipRow);
    }

    @Override
    public <T> List<T> readBeanRowsAndNext(final Class<T> clazz, final Headers headers){
        return readBeanRowsAndNext(clazz, headers, null);
    }

    @Override
    public boolean validateHeaders(Headers headers){
        if(CollectionUtil.isEmpty(headers.getDataHeaders())){
            return false;
        }

        List<IMergedCell> cells = readCellsAtOneRow(getCurrentRow());
        if(CollectionUtil.isEmpty(cells)){
            return false;
        }

        Map<Integer, Object> map = cells.stream().filter(s -> s.getValue() != null).collect(Collectors.toMap(ICell::getFirstColumnNum, ICell::getValue));

        for(IDataHeader header : headers.getDataHeaders()){
            String mapValue = (String)map.get(header.getStartColumnNum());
            if(!header.getValue().equals(mapValue)){
                LOG.warn("validate header[{}] failed, now is \"{}\"!", header.getValue(), mapValue);
                return false;
            }
        }
        return true;
    }


    /**
     * 获取当前行的最后列号
     */
    protected int getLastCellNumOfCurrentRow(){
        return getLastCellNumAtOneRow(getCurrentRow());
    }

}

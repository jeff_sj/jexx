package jexx.poi.read;

import jexx.poi.exception.POIException;
import jexx.poi.util.CellOperateUtil;
import jexx.poi.util.RowOperationUtil;
import jexx.util.Assert;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

import java.util.Iterator;

/**
 * sheet抽象读取器；
 * 注意: 为了兼容 poi 对大量数据的读取，sheet本身方法 getLastRowNum 是不对的。主要原因还是分批读取数据，导致数据不准确。
 * @author jeff
 * @since 2019/7/2
 */
public abstract class AbstractSheetReader {

    private final String sheetName;
    protected Iterator<Row> rowIterator;
    private DelegateRow nextRow;
    private DelegateRow currentRow;

    public AbstractSheetReader(Sheet sheet) {
        this.sheetName = sheet.getSheetName();
        this.rowIterator = sheet.iterator();
        doNextRow();
    }

    public String getName(){
        return this.sheetName;
    }

    protected Row getCurrentRow(){
        if(this.currentRow == null){
            throw new POIException("no current row");
        }
        return this.currentRow;
    }

    protected int getCurrentRowNum(){
        return getCurrentRow().getRowNum() + 1;
    }

    /**
     * 是否是最后一行
     */
//    protected boolean isRowEnd(){
//        return !hasNextRow();
//    }

    protected boolean hasNextRow(){
        return this.rowIterator.hasNext();
    }

    protected Row doNextRow(){
        if(this.nextRow != null){
            Assert.notNull(this.currentRow, "currentRow is null ?");
            Assert.isTrue(this.currentRow.getRowNum() < this.nextRow.getRowNum(), "currentRow={},nextRow={} ", this.currentRow.getRowNum(), this.nextRow.getRowNum());

            DelegateRow r;

            if(this.currentRow.getRowNum() < this.nextRow.getRowNum() - 1){
                this.currentRow = new DelegateRow(this.currentRow.getRowNum()+1);
            }
            else{
                this.currentRow = this.nextRow;
                this.nextRow = null;
            }
            return this.currentRow;
        }

        if(!this.rowIterator.hasNext()){
            throw new POIException("no next row");
        }
        DelegateRow delegateRow = new DelegateRow(this.rowIterator.next());

        int cr = this.currentRow == null ? -1 : this.currentRow.getRowNum();
        if(cr < delegateRow.getRowNum() - 1){
            this.currentRow = new DelegateRow(cr+1);
            this.nextRow = delegateRow;
        }
        else{
            this.currentRow = delegateRow;
        }
        return this.currentRow;
    }

    protected void doNextRowIfHasNext(){
        if(hasNextRow()){
            doNextRow();
        }
        else{
            this.currentRow = null;
        }
    }

    protected Row doSkipRow(int skipRowNum){
        Assert.isTrue(skipRowNum > 0, "skipRowNum is not illegal");
        int rn = skipRowNum;

        Row thisRow = null;
        while(rn > 0){
            thisRow = doNextRow();
            rn--;
        }
        return thisRow;
    }

    protected Row doJumpToRow(int jumpRowNum){
        Assert.isTrue(jumpRowNum > 0, "jumpRowNum is not illegal");
        Assert.isTrue(getCurrentRowNum() <= jumpRowNum, "jumpRowNum is too small");

        if(jumpRowNum == getCurrentRowNum()){
            return getCurrentRow();
        }

        while(getCurrentRowNum() >= jumpRowNum){
            doNextRow();
        }
        return getCurrentRow();
    }

    protected Cell getCell(Row row, int columnNum){
        return row.getCell(columnNum-1);
    }

    protected Object getCellValue(Cell cell){
        return CellOperateUtil.getCellValue(cell);
    }

    protected int getLastCellNumAtOneRow(Row row){
        return row.getLastCellNum();
    }


}

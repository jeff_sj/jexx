package jexx.poi.read;

import jexx.poi.cell.IMergedCell;
import jexx.poi.cell.MergedCell;
import jexx.poi.util.CellOperateUtil;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

import java.util.ArrayList;
import java.util.List;

/**
 * @author jeff
 * @since 2019/7/2
 */
public class SheetCellReader extends AbstractSheetReader {

    public SheetCellReader(Sheet sheet) {
        super(sheet);
    }

    /**
     * 指定行 从开始列到结束列读取 单元格
     * @param row row
     * @param startColumnNum 开始列号
     * @param endColumnNum 结束列号
     */
    protected List<IMergedCell> readCellsAtOneRow(final Row row, final int startColumnNum, final int endColumnNum){
        if(row == null){
            return null;
        }

        int rowNum = row.getRowNum() + 1;
        int lastCellNum = row.getLastCellNum()+1;
        int minColumnNum = Math.max(1, startColumnNum);
        int maxColumnNum = Math.min(lastCellNum, endColumnNum);

        Cell cell;
        Object value;
        List<IMergedCell> cells = new ArrayList<>();
        for(int i = minColumnNum; i <= maxColumnNum; i++){
            cell = getCell(row, i);
            value = getCellValue(cell);
            cells.add(new MergedCell(rowNum, i, rowNum, i, value));
        }
        return cells;
    }

    /**
     * 读取指定行的单元格
     * @param row row
     */
    protected List<IMergedCell> readCellsAtOneRow(final Row row){
        return readCellsAtOneRow(row, 1, getLastCellNumAtOneRow(row));
    }

}

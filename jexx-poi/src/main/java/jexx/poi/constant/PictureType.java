package jexx.poi.constant;

import jexx.util.StringPool;
import jexx.util.StringUtil;
import org.apache.poi.ss.usermodel.Workbook;

/**
 * excel中图片类型
 *
 * @author jeff
 */
public enum PictureType {

    /**
     * EMF
     */
    EMF(Workbook.PICTURE_TYPE_EMF, "emf"),
    /**
     * WMF
     */
    WMF(Workbook.PICTURE_TYPE_WMF, "wmf"),
    /**
     * PICT
     */
    PICT(Workbook.PICTURE_TYPE_PICT, "pict"),
    /**
     * JPEG
     */
    JPEG(Workbook.PICTURE_TYPE_JPEG, "jpeg"),
    /**
     * PNG
     */
    PNG(Workbook.PICTURE_TYPE_PNG, "png"),
    /**
     * DIB
     */
    DIB(Workbook.PICTURE_TYPE_DIB, "dib")
    ;

    private int type;
    private String extend;

    PictureType(int type, String extend) {
        this.type = type;
        this.extend = extend;
    }

    public static PictureType checkType(final String imageName){
        PictureType type = null;
        for(PictureType pictureType : PictureType.values()){
            if(StringUtil.endWithIgnoreCase(imageName, StringPool.DOT.concat(pictureType.getExtend()))){
                type = pictureType;
                break;
            }
        }
        return type;
    }

    public int getType() {
        return type;
    }

    public String getExtend() {
        return extend;
    }
}

package jexx.poi.constant;

/**
 * 写入excel的模式
 *
 * @author jeff
 * @since 2019/5/17
 */
public enum ExcelWriteMode {

    /**
     * 默认写入模式; 写入
     */
    DEFAULT_MODE,

    /**
     * 视图写入模式;
     */
    VIEW_MODE


}

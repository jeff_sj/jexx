package jexx.poi.constant;

import jexx.poi.style.CellStyleSets;
import jexx.poi.style.IWrapCellStyle;

import java.util.HashMap;
import java.util.Map;

/**
 * 内置系统单元格样式名称
 * @author jeff
 * @since 2020/1/7
 */
public enum EmbeddedDataStyleName {

    /** header样式 */
    HEADER("_header"),
    /** data样式 */
    DATA("_data"),
    /** 无样式 */
    NONE("_none"),
    /** 日期样式,yyyy */
    DATE_YEAR("_year"),
    /** 日期样式,yyyy-MM */
    DATE_MONTH("_month"),
    /** 日期样式,yyyy-MM-dd */
    DATE("_date"),
    /** 日期样式,yyyy-MM-dd HH:mm:ss */
    DATE_TIME("_datetime"),
    /** 公式样式 */
    FORMULA("_formula"),
    /** 保留两位小数 */
    DECIMAL_TWO_PRECISION("_twoDecimal"),
    ;

    private String name;

    EmbeddedDataStyleName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    /**
     * 包装内置的系统单元格样式
     */
    public static void wrapEmbeddedDataStyle(Map<String, IWrapCellStyle> cellStyleMap){
        cellStyleMap.put(HEADER.name, CellStyleSets.HEADER_CELL_STYLE);
        cellStyleMap.put(DATA.name, CellStyleSets.DATA_CELL_STYLE);
        cellStyleMap.put(NONE.name, CellStyleSets.NONE_CELL_STYLE);
        cellStyleMap.put(DATE_YEAR.name, CellStyleSets.YEAR_CELL_STYLE);
        cellStyleMap.put(DATE_MONTH.name, CellStyleSets.MONTH_CELL_STYLE);
        cellStyleMap.put(DATE.name, CellStyleSets.DATE_CELL_STYLE);
        cellStyleMap.put(DATE_TIME.name, CellStyleSets.DATE_TIME_CELL_STYLE);
        cellStyleMap.put(FORMULA.name, CellStyleSets.FORMULA_CELL_STYLE);
        cellStyleMap.put(DECIMAL_TWO_PRECISION.name, CellStyleSets.DECIMAL_TWO_CELL_STYLE);
    }

    /**
     * 获取内置的系统单元格样式
     */
    public static Map<String, IWrapCellStyle> getEmbeddedDataStyle(){
        Map<String, IWrapCellStyle> cellStyleMap = new HashMap<>(16);
        wrapEmbeddedDataStyle(cellStyleMap);
        return cellStyleMap;
    }

}

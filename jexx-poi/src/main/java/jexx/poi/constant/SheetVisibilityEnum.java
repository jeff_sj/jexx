package jexx.poi.constant;

/**
 * copy from poi
 *
 * @see org.apache.poi.ss.usermodel.SheetVisibility
 * @author jeff
 * @since 2019/6/14
 */
public enum SheetVisibilityEnum {

    /**
     * sheet可见
     */
    VISIBLE(org.apache.poi.ss.usermodel.SheetVisibility.VISIBLE),
    /**
     * sheet隐藏
     */
    HIDDEN(org.apache.poi.ss.usermodel.SheetVisibility.HIDDEN),
    /**
     * sheet隐藏且无法打开
     */
    VERY_HIDDEN(org.apache.poi.ss.usermodel.SheetVisibility.VERY_HIDDEN);

    private org.apache.poi.ss.usermodel.SheetVisibility visibility;

    SheetVisibilityEnum(org.apache.poi.ss.usermodel.SheetVisibility visibility) {
        this.visibility = visibility;
    }

    public org.apache.poi.ss.usermodel.SheetVisibility getVisibility() {
        return visibility;
    }
}

package jexx.poi.constant;

/**
 * data format
 *
 * @author jeff
 * @since 2019/5/19
 */
public enum DataFormatEnum {

    /**
     * 文本
     */
    TEXT("@"),

    /**
     * 小数,两位
     */
    DECIMAL_TWO("0.00"),

    /**
     * 百分比
     */
    PERCENT("#0.00%"),
    /**
     * 百分比
     */
    PERCENT_1("#0%"),

    /**
     * 千分位
     */
    THOUSAND("#,##0"),
    /**
     * 千分位,保留两位小数
     */
    THOUSAND_1("#,##0.00"),
    /**
     * 万元,保留四位小数, 表示excel中 0!.0000
     */
    TEN_THOUSAND_YUAN("0\\.0000"),
    /**
     * 年, yyyy
     */
    YEAR("yyyy"),
    /**
     * 年月, yyyy-MM
     */
    MONTH("yyyy-MM"),
    /**
     * 日期, yyyy-MM-dd
     */
    DATE("yyyy-MM-dd"),
    /**
     * 时间  yyyy-MM-dd HH:mm:ss
     */
    DATETIME("yyyy-MM-dd HH:mm:ss")
    ;

    private String format;

    DataFormatEnum(String format) {
        this.format = format;
    }

    public String getFormat() {
        return format;
    }
}

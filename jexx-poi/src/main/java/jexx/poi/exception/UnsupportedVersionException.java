package jexx.poi.exception;

/**
 * 版本不支持异常
 * @author jeff
 * @since 2019/12/3
 */
public class UnsupportedVersionException extends POIException {

    public UnsupportedVersionException(Throwable e) {
        super(e);
    }

    public UnsupportedVersionException(String message) {
        super(message);
    }

    public UnsupportedVersionException(String messageTemplate, Object... params) {
        super(messageTemplate, params);
    }

    public UnsupportedVersionException(String message, Throwable throwable) {
        super(message, throwable);
    }

    public UnsupportedVersionException(Throwable throwable, String messageTemplate, Object... params) {
        super(throwable, messageTemplate, params);
    }
}

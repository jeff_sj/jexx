package jexx.poi.exception;

import jexx.util.StringUtil;

/**
 * POI异常
 */
public class POIException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public POIException(Throwable e) {
		super(e);
	}

	public POIException(String message) {
		super(message);
	}

	public POIException(String messageTemplate, Object... params) {
		super(StringUtil.format(messageTemplate, params));
	}

	public POIException(String message, Throwable throwable) {
		super(message, throwable);
	}

	public POIException(Throwable throwable, String messageTemplate, Object... params) {
		super(StringUtil.format(messageTemplate, params), throwable);
	}

	/**
	 * 导致这个异常的异常是否是指定类型的异常
	 * 
	 * @param clazz 异常类
	 * @return 是否为指定类型异常
	 */
	public boolean causeInstanceOf(Class<? extends Throwable> clazz) {
		return clazz.isInstance(this.getCause());
	}
}

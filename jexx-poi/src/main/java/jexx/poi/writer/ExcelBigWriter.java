package jexx.poi.writer;

import jexx.exception.IORuntimeException;
import jexx.io.FileUtil;
import jexx.io.IOUtil;
import jexx.poi.exception.POIException;
import jexx.poi.util.WorkbookUtil;
import jexx.poi.writer.impl.SheetBigWriterImpl;
import jexx.util.Assert;
import jexx.util.StringUtil;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * 使用SXSSFWorkbook的写入器; 只能写不能读
 * @author jeff
 * @since 2019/9/28
 */
public class ExcelBigWriter extends AbstractExcelWriter {

    protected List<SheetBigWriterImpl> sheetWriters = new ArrayList<>();

    public ExcelBigWriter() {
        this(new org.apache.poi.xssf.usermodel.XSSFWorkbook());
    }

    public ExcelBigWriter(XSSFWorkbook workbook) {
        super(new SXSSFWorkbook(workbook), true, 0);
    }

    //----------------------------------------------- create

    public static ExcelBigWriter createBigWriter() {
        return new ExcelBigWriter();
    }

    public static ExcelBigWriter createBigWriter(File file) {
        Workbook workbook = WorkbookUtil.createBook(file);
        if (!(workbook instanceof XSSFWorkbook)) {
            IOUtil.closeQuietly(workbook);
            throw new POIException("don't support this file");
        }
        return new ExcelBigWriter((XSSFWorkbook) workbook);
    }

    public static ExcelBigWriter createBigWriter(InputStream inputStream) {
        Workbook workbook = WorkbookUtil.createBook(inputStream);
        if (!(workbook instanceof XSSFWorkbook)) {
            IOUtil.closeQuietly(workbook);
            throw new POIException("don't support this stream");
        }
        return new ExcelBigWriter((XSSFWorkbook) workbook);
    }

    public SheetBigWriter sheet(String sheetName) {
        Assert.isTrue(!METADATA_SHEET_NAME.equalsIgnoreCase(sheetName), "Sheet name is not equal to {}", sheetName);
        if (StringUtil.isEmpty(sheetName)) {
            sheetName = "Sheet1";
        }
        SXSSFSheet sheet = getOrCreateSheet(sheetName);
        SheetBigWriterImpl sheetWriter = new SheetBigWriterImpl(sheet);
        sheetWriters.add(sheetWriter);
        return sheetWriter;
    }

    @Override
    protected SXSSFWorkbook getWorkbook() {
        return (SXSSFWorkbook) this.workbook;
    }

    @Override
    protected SXSSFSheet getOrCreateSheet(String sheetName) {
        SXSSFSheet sheet = getWorkbook().getSheet(sheetName);
        if (sheet == null) {
            sheet = getWorkbook().createSheet(sheetName);
        }
        return sheet;
    }

    public ExcelBigWriter flush(OutputStream out) {
        try {
            flushSheets();

            this.workbook.write(out);
            out.flush();
        } catch (IOException e) {
            throw new IORuntimeException(e);
        }
        return this;
    }

    public ExcelBigWriter flush(File destFile){
        OutputStream out = null;
        try {
            out = FileUtil.newBufferedOutputStream(destFile);
            flush(out);
        } finally {
            IOUtil.closeQuietly(out);
        }
        return this;
    }

    private void flushSheets(){
        for(SheetBigWriterImpl sheetBigWriter : sheetWriters){
            sheetBigWriter.flush();
        }
    }

}

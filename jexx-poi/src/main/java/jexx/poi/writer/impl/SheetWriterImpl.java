package jexx.poi.writer.impl;

import jexx.bean.BeanUtil;
import jexx.poi.SheetWriter;
import jexx.poi.cell.IMergedCell;
import jexx.poi.header.Headers;
import jexx.poi.header.IDataHeader;
import jexx.poi.meta.DVConstraintType;
import jexx.poi.row.RowContext;
import jexx.poi.style.CellStyleMapper;
import jexx.poi.util.DataValidationUtil;
import jexx.poi.writer.IndexRowWriter;
import jexx.poi.writer.MapRowWriter;
import jexx.util.Assert;
import jexx.util.CollectionUtil;
import jexx.util.MapUtil;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.ss.usermodel.DataValidation;
import org.apache.poi.ss.usermodel.Sheet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;

/**
 * SheetWriter
 *
 * @author Jeff
 */
public class SheetWriterImpl extends SheetRowWriterImpl implements SheetWriter {

    private static final Logger log = LoggerFactory.getLogger(SheetWriterImpl.class);

    /** 当前操作行号, 0表示未开始操作 */
    protected int currentRowNum = 0;

    private final MapRowWriter mapRowWriter = new MapRowWriter();
    private final IndexRowWriter indexRowWriter = new IndexRowWriter();

    public SheetWriterImpl(Sheet sheet) {
        super(sheet);
    }

    @Override
    public int getCurrentRowNum(){
        return this.currentRowNum;
    }

    @Override
    public void passCurrentRow(){
        skipRows(1);
    }

    @Override
    public void skipRows(final int skipRowNum){
        Assert.isTrue(skipRowNum > 0, "skipRowNum must be greater than zero!");
        this.currentRowNum += skipRowNum;
    }

    @Override
    public void jumpToRowNum(final int rowNum){
        Assert.isTrue(rowNum > 0, "rowNum must be greater than zero!");
        this.currentRowNum = rowNum;
    }

    //------------------------------------------------------------ header

    @Override
    public void writeHeader(final int rowNum, final Headers headers, final CellStyleMapper cellStyleMapper){
        this.currentRowNum = doWriteRow(rowNum, headers, cellStyleMapper);
    }

    @Override
    public void writeHeader(final int rowNum, final Headers headers){
        writeHeader(rowNum, headers, null);
    }

    @Override
    public void writeHeader(final Headers headers, final CellStyleMapper cellStyleMapper){
        writeHeader(currentRowNum+1, headers, cellStyleMapper);
    }

    @Override
    public void writeHeader(final Headers headers){
        writeHeader(headers, null);
    }

    //------------------------------------------------------------ write list rows

    @Override
    public int writeRows(int startRowNum, int columnNum, List<?> rows, final CellStyleMapper cellStyleMapper){
        if(CollectionUtil.isEmpty(rows)){
            return -1;
        }

        RowContext rowContext = null;
        for (Object row : rows) {
            if(row instanceof List){
                rowContext = doWriteRow(startRowNum, columnNum, (List<?>)row, cellStyleMapper);
            }
            else if(row instanceof Map){
                @SuppressWarnings("unchecked")
                Map<String, Object> map = (Map<String, Object>)row;
                rowContext = doWriteRow(startRowNum, columnNum, MapUtil.convertValueToList(map), cellStyleMapper);
            }
            else{
                rowContext = doWriteRow(startRowNum, columnNum, CollectionUtil.list(row.toString()), cellStyleMapper);
            }
        }

        return rowContext == null ? -1 : rowContext.getEndRowNum();
    }

    @Override
    public int writeRows(int startRowNum, int columnNum, List<?> rows){
        return writeRows(startRowNum, columnNum, rows, null);
    }

    @Override
    public int writeRows(int columnNum, List<?> rows, final CellStyleMapper cellStyleMapper){
        return writeRows(currentRowNum+1, columnNum, rows, cellStyleMapper);
    }

    @Override
    public int writeRows(int columnNum, List<?> rows){
        return writeRows(columnNum, rows, (CellStyleMapper)null);
    }

    @Override
    public int writeRows(List<?> rows, final CellStyleMapper cellStyleMapper){
        return writeRows(1, rows, cellStyleMapper);
    }

    @Override
    public int writeRows(List<?> rows){
        return writeRows(rows, (CellStyleMapper)null);
    }


    //------------------------------------------------------------ writeRows

    @Override
    public <T> int writeRows(int startRowNum, List<T> rows, Headers headers, BiConsumer<IMergedCell, T> consumer) {
        if(CollectionUtil.isEmpty(rows)){
            return -1;
        }
        RowContext rowContext = null;
        T row;
        for (int i = 0; i < rows.size(); i++) {
            row = rows.get(i);
            int num = rowContext == null ? startRowNum : rowContext.getEndRowNum() + 1;
            rowContext = doWriteRow(num, row, headers, consumer, i);
        }

        int endRowNum = rowContext == null ? -1 : rowContext.getEndRowNum();
        if(supportValid && !headers.isIndex()){
            doValidData(headers.getDataHeaders(), startRowNum, endRowNum);
        }
        return endRowNum;
    }

    @Override
    public <T> int writeRows(List<T> rows, Headers headers, BiConsumer<IMergedCell, T> consumer) {
        return writeRows(currentRowNum+1, rows, headers, consumer);
    }


//------------------------------------------------------------ writeMapRows

    @Override
    public int writeMapRows(int startRowNum, List<Map<String, Object>> rows, Headers headers, final CellStyleMapper cellStyleMapper){
        if(CollectionUtil.isEmpty(rows)){
            return -1;
        }

        RowContext rowContext = null;
        Map<String, Object> row;
        for (int i = 0; i < rows.size(); i++) {
            row = rows.get(i);
            int num = rowContext == null ? startRowNum : rowContext.getEndRowNum() + 1;
            rowContext = doWriteMapRow(num, row, headers, cellStyleMapper, i);
        }

        if(supportValid){
            doValidData(headers.getDataHeaders(), startRowNum, startRowNum + rows.size() - 1);
        }

        return rowContext == null ? -1 : rowContext.getEndRowNum();
    }

    @Override
    public int writeMapRows(int startRowNum, List<Map<String, Object>> rows, Headers headers){
        return writeMapRows(startRowNum, rows, headers, null);
    }

    @Override
    public int writeMapRows(List<Map<String, Object>> rows, Headers headers, final CellStyleMapper cellStyleMapper){
        return writeMapRows(currentRowNum+1, rows, headers, cellStyleMapper);
    }

    @Override
    public int writeMapRows(List<Map<String, Object>> rows, Headers headers){
        return writeMapRows(rows, headers, null);
    }

    //----------------------------------------------------------------填充

    /**
     * 从开始行到指定行填充数据; 如果数据量大于可填充行数,则会新增新的row;
     * <pre>
     *     注意只对非索引列支持
     * </pre>
     * @param fillStartRowNum 开始行, 从1开始
     * @param fillEndRowNum 结束行, 从1开始
     * @param rows 数据
     */
    @Override
    public void fill(int fillStartRowNum, int fillEndRowNum, List<?> rows, Headers headers){
        int fillLength = fillEndRowNum - fillStartRowNum + 1;
        Assert.isTrue(fillLength >= 0);
        int rowsLength = rows.size();
        if(rowsLength > fillLength){
            int specifyStyleRowNum = fillLength > 0 ? fillStartRowNum + 2 : fillStartRowNum;
            insertRows(specifyStyleRowNum, rowsLength - fillLength);
        }
        writeRows(fillStartRowNum, rows, headers);
    }

    //----------------------------------------------------------------write row

    @Override
    public RowContext writeRow(int startRowNum, Object rowData, Headers headers, final CellStyleMapper cellStyleMapper){
        Assert.notNull(headers);
        RowContext rowContext = doWriteRow(startRowNum, rowData, headers, cellStyleMapper, 0);
        this.currentRowNum = rowContext.getEndRowNum();
        return rowContext;
    }

    @Override
    public RowContext writeRow(int startRowNum, Object rowData, Headers headers){
        return writeRow(startRowNum, rowData, headers, null);
    }

    @Override
    public RowContext writeRow(Object rowData, Headers headers, final CellStyleMapper cellStyleMapper){
        return writeRow(currentRowNum+1, rowData, headers, cellStyleMapper);
    }

    @Override
    public RowContext writeRow(Object rowData, Headers headers){
        return writeRow(currentRowNum+1, rowData, headers);
    }

    //----------------------------------------------------------------写入list类型数据

    @Override
    public RowContext writeListRow(int startRowNum, int columnNum, List<?> rowData, final CellStyleMapper cellStyleMapper) {
        RowContext rowContext =  doWriteRow(startRowNum, columnNum, rowData, cellStyleMapper);
        this.currentRowNum = rowContext.getEndRowNum();
        return rowContext;
    }

    @Override
    public RowContext writeListRow(int columnNum, List<?> rowData, final CellStyleMapper cellStyleMapper) {
        return writeListRow(this.currentRowNum+1, columnNum, rowData, cellStyleMapper);
    }

    @Override
    public RowContext writeListRow(List<?> rowData, final CellStyleMapper cellStyleMapper) {
        return writeListRow(1, rowData, cellStyleMapper);
    }

    @Override
    public RowContext writeListRow(List<?> rowData) {
        return writeListRow(1, rowData, null);
    }

    //----------------------------------------------------------------write map data

    @Override
    public RowContext writeMapRow(int rowNum, final Map<String, Object> map, final Headers headers, final CellStyleMapper cellStyleMapper){
        Assert.notNull(headers);
        return doWriteMapRow(rowNum, map, headers, cellStyleMapper, 0);
    }

    @Override
    public RowContext writeMapRow(int rowNum, final Map<String, Object> map, final Headers headers){
        return writeMapRow(rowNum, map, headers, null);
    }

    @Override
    public RowContext writeMapRow(Map<String, Object> rowData,final Headers headers, final CellStyleMapper cellStyleMapper){
        return writeMapRow(currentRowNum+1, rowData, headers, cellStyleMapper);
    }

    @Override
    public RowContext writeMapRow(Map<String, Object> rowData, final Headers headers){
        return writeMapRow(rowData, headers, null);
    }

    //----------------------------------------------------------------do row's data

    /**
     * 指定行指定列开始写一行数据
     * @param rowNum 指定行
     * @param columnNum 指定列
     * @param rowData 一行数据
     * @param cellStyleMapper {@link CellStyleMapper}
     * @return {@link RowContext}
     */
    protected RowContext doWriteRow(final int rowNum, final int columnNum, final List<?> rowData, final CellStyleMapper cellStyleMapper) {
        RowContext rowContext = RowContext.createRow(rowNum, columnNum, rowData, null);
        writeRow(rowContext, cellStyleMapper);
        return rowContext;
    }

    /**
     * 指定行开始写 header
     * @param rowNum 指定行
     * @param headers {@link Headers}
     * @param cellStyleMapper {@link CellStyleMapper}
     * @return 结束行
     */
    protected int doWriteRow(final int rowNum, Headers headers, final CellStyleMapper cellStyleMapper) {
        List<IMergedCell> cells = headers.createHeaderCells(rowNum);
        int maxEndRowNum = rowNum;
        for(IMergedCell cell : cells){
            writeCell(cell, cellStyleMapper);
            maxEndRowNum = Math.max(cell.getLastRowNum(), maxEndRowNum);
        }
        return maxEndRowNum;
    }

    /**
     * 指定行指定header写入map数据
     * @param startRowNum 指定行
     * @param rowData 一行map类型数据
     * @param headers {@link Headers}
     * @param cellStyleMapper {@link CellStyleMapper}
     * @param itemIndex 该行数据在整个集合数据中的索引
     * @return {@link RowContext}
     */
    protected RowContext doWriteMapRow(final int startRowNum, final Map<String, Object> rowData, Headers headers, final CellStyleMapper cellStyleMapper, int itemIndex) {
        return doWriteRow(startRowNum, rowData, headers, (cell,bean)->{
            if(cellStyleMapper != null){
                cell.setCellStyle(cellStyleMapper.warpCellStyle(cell));
            }
        }, itemIndex);
    }

    /**
     * 指定行指定header写入bean或者map
     * @param startRowNum 指定开始行
     * @param rowData 一行map类型数据
     * @param headers {@link Headers}
     * @param cellStyleMapper {@link CellStyleMapper}
     * @param itemIndex 该行数据在整个集合数据中的索引
     * @return {@link RowContext}
     */
    protected RowContext doWriteRow(final int startRowNum, final Object rowData, final Headers headers, final CellStyleMapper cellStyleMapper, int itemIndex) {
        return doWriteRow(startRowNum, rowData, headers, (cell,bean)->{
            if(cellStyleMapper != null){
                cell.setCellStyle(cellStyleMapper.warpCellStyle(cell));
            }
        }, itemIndex);
    }

    /**
     * 指定行指定header写入bean或者map
     * @param startRowNum 指定开始行
     * @param rowData 一行map类型数据
     * @param headers {@link Headers}
     * @param consumer 加工cell
     * @param itemIndex 该行数据在整个集合数据中的索引
     * @return {@link RowContext}
     */
    protected <T> RowContext doWriteRow(final int startRowNum, final T rowData, final Headers headers, final BiConsumer<IMergedCell, T> consumer, int itemIndex) {
        Assert.isTrue(rowData instanceof Map || BeanUtil.isBean(rowData.getClass()), "no support to write row[{}]", rowData);
        RowContext rowContext;

        if(headers.isIndex() && !(rowData instanceof Map) ){
            rowContext = indexRowWriter.writeRow(startRowNum, rowData, headers, itemIndex, !headers.isIndex());
        }
        else{
            rowContext = mapRowWriter.writeRow(startRowNum, BeanUtil.toMap(rowData), headers, itemIndex, !headers.isIndex());
        }
        writeRow(rowContext, rowData, consumer);
        this.currentRowNum = rowContext.getEndRowNum();
        return rowContext;
    }

    //--------------------------------------------------------- constraint

    @Override
    public void validData(int startRowNum, int rowCount, Headers headers){
        Assert.isTrue(rowCount > 0);
        List<IDataHeader> headerList = headers.getDataHeaders();
        doValidData(headerList, startRowNum, startRowNum+rowCount-1);
    }

    @Override
    public void validData(int rowCount, Headers headers){
        validData(currentRowNum+1, rowCount, headers);
    }

    /**
     * 校验数据有效性;
     * <p>数据有效性不应该设置过多。设置过多, 生成速度变慢, excel文件打开异常, 会失去有效性校验</p>
     * @param headers headers
     * @param startRowNum 开始行,从1开始
     * @param endRowNum 结束行,从1开始, 结束行不能小于开始行
     */
    protected void doValidData(List<IDataHeader> headers, int startRowNum, int endRowNum){
        if(endRowNum < startRowNum){
            endRowNum = startRowNum;
        }
        if(CollectionUtil.isNotEmpty(headers)) {
            for (IDataHeader header : headers) {
                DVConstraintType dvConstraintType = header.getDVConstraintType();
                if(DVConstraintType.ARRAY == dvConstraintType || DVConstraintType.TREE == dvConstraintType){
                    int sr = startRowNum;
                    if(sheet instanceof HSSFSheet){
                        sr = 1;
                    }
                    String formula = header.createDataValidateFormula(sr);
                    DataValidation dataValidation = DataValidationUtil.createValidation(sheet, formula, startRowNum, endRowNum,
                            header.getStartColumnNum(), header.getEndColumnNum());
                    sheet.addValidationData(dataValidation);
                    log.debug("create data validation for header[{}], formula={}", header.getKey(), formula);
                }
            }
        }
    }

}

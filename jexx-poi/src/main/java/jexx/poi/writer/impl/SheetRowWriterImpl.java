package jexx.poi.writer.impl;

import jexx.poi.cell.IMergedCell;
import jexx.poi.row.DataRow;
import jexx.poi.row.Row;
import jexx.poi.row.RowContext;
import jexx.poi.style.CellStyleMapper;
import jexx.poi.writer.SheetRowWriter;
import jexx.util.CollectionUtil;
import org.apache.poi.ss.usermodel.Sheet;

import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

/**
 * @author jeff
 * @since 2019/5/22
 */
public class SheetRowWriterImpl extends SheetCellWriterImpl implements SheetRowWriter {

    /**
     * 是否修复header对应的空数据;
     * <pre>
     *     由于空数据会造成单元格没有样式, 因此使用该字段控制是否对空单元格填充。
     *     修复会遍历所有数据
     * </pre>
     */
    protected boolean repair = true;

    public SheetRowWriterImpl(Sheet sheet) {
        super(sheet);
    }

    @Override
    public SheetRowWriterImpl writeRow(final RowContext row, final CellStyleMapper cellStyleMapper){
       return writeRow(row, null, (cell,bean)->{
           if(cellStyleMapper != null){
               cell.setCellStyle(cellStyleMapper.warpCellStyle(cell));
           }
       });
    }

    @Override
    public SheetRowWriterImpl writeRow(RowContext row, final Consumer<IMergedCell> consumer) {
        DataRow rootRow = row.getRootRow();
        if(repair){
            row.repair();
        }
        doRecursionRow(rootRow, consumer);
        return writeRow(row, null, (cell,bean)->{
            if(consumer != null){
                consumer.accept(cell);
            }
        });
    }

    @Override
    public <T> SheetRowWriterImpl writeRow(RowContext row, T bean, BiConsumer<IMergedCell, T> consumer) {
        DataRow rootRow = row.getRootRow();
        if(repair){
            row.repair();
        }
        doRecursionRow(rootRow, bean, consumer);
        return this;
    }

    private void doRecursionRow(Row row, final Consumer<IMergedCell> consumer){
        List<IMergedCell> cells = row.getCells();
        for(IMergedCell cell : cells){
            writeCell(cell, consumer);
        }

        List<Row> children = row.getChildren();
        if(CollectionUtil.isNotEmpty(children)){
            for(Row child : children){
                doRecursionRow(child, consumer);
            }
        }
    }

    private <T> void doRecursionRow(Row row, T bean, final BiConsumer<IMergedCell, T> consumer){
        List<IMergedCell> cells = row.getCells();
        for(IMergedCell cell : cells){
            writeCell(cell, bean, consumer);
        }

        List<Row> children = row.getChildren();
        if(CollectionUtil.isNotEmpty(children)){
            for(Row child : children){
                doRecursionRow(child, bean, consumer);
            }
        }
    }

    public void setRepair(boolean repair) {
        this.repair = repair;
    }
}

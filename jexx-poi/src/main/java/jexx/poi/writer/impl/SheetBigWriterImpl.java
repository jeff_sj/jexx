package jexx.poi.writer.impl;

import jexx.poi.header.Headers;
import jexx.poi.writer.SheetBigWriter;
import jexx.util.Assert;
import org.apache.poi.xssf.streaming.SXSSFSheet;

import java.util.List;

/**
 * SheetWriter的包装类，主要处理大数据的写入
 * @author jeff
 * @since 2019/9/30
 */
public class SheetBigWriterImpl extends SheetWriterImpl implements SheetBigWriter {

    public SheetBigWriterImpl(SXSSFSheet sheet) {
        super(sheet);
    }

    @Override
    public void jumpToRowNum(final int rowNum){
        if(rowNum <= getCurrentRowNum()){
            throw new UnsupportedOperationException();
        }
        super.jumpToRowNum(rowNum);
    }

    @Override
    public void fill(int fillStartRowNum, int fillEndRowNum, List<?> rows, Headers headers){
        throw new UnsupportedOperationException();
    }

    @Override
    public void validData(int startRowNum, int rowCount, Headers headers){
        throw new UnsupportedOperationException();
    }

    @Override
    public void validData(int rowCount, Headers headers){
        throw new UnsupportedOperationException();
    }

}

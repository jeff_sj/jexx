package jexx.poi.writer.impl;

import jexx.poi.cell.*;
import jexx.poi.style.CellStyleMapper;
import jexx.poi.style.IWrapCellStyle;
import jexx.poi.util.CellOperateUtil;
import jexx.poi.util.DataValidationUtil;
import jexx.poi.writer.AbstractSheetWriter;
import jexx.poi.writer.SheetCellWriter;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataValidation;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

/**
 * SheetWriter support to write a range cell!
 *
 * @author jeff
 * @since 2019/5/7
 */
public class SheetCellWriterImpl extends AbstractSheetWriter implements SheetCellWriter {

    /**
     * 定义存储在内存中的cell默认数量
     */
    public static final int DEFAULT_WINDOW_SIZE = 1000;

    private List<ICell> cellList;
    /**
     * 是否支持valid, 默认支持
     */
    protected boolean supportValid = true;
    /**
     * 存储在内存中的cell数量; 0 则不存储
     */
    protected int rowAccessWindowSize;

    public SheetCellWriterImpl(Sheet sheet) {
        super(sheet);
        this.cellList = new ArrayList<>();
        this.rowAccessWindowSize = DEFAULT_WINDOW_SIZE;
    }

    /**
     * 设置是否支持valid, 默认支持
     */
    @Override
    public void setSupportValid(boolean supportValid) {
        this.supportValid = supportValid;
    }

    @Override
    public SheetCellWriterImpl writeCell(IMergedCell cell){
        return writeCell(cell, (Consumer<IMergedCell>)null);
    }

    @Override
    public SheetCellWriterImpl writeCell(IMergedCell cell, final CellStyleMapper cellStyleMapper){
        if(cellStyleMapper != null){
            //CellStyleMapper has highest authority to custom cell style
            IWrapCellStyle cellStyle = cellStyleMapper.warpCellStyle(cell);
            cell.setCellStyle(cellStyle);
        }
        writeCell(cell, (Consumer<IMergedCell>)null);
        return this;
    }

    @Override
    public SheetCellWriterImpl writeCell(IMergedCell cell, Consumer<IMergedCell> consumer) {
        if(consumer != null){
            consumer.accept(cell);
        }
        doWriteCell(cell);
        return this;
    }

    @Override
    public <T> SheetCellWriter writeCell(IMergedCell cell, T bean, BiConsumer<IMergedCell, T> consumer) {
        if(consumer != null){
            consumer.accept(cell, bean);
        }
        doWriteCell(cell);
        return this;
    }

    private void doWriteCell(IMergedCell cell){
        this.cellList.add(cell);
        flushCells();
    }

    private void flushCells(){
        if(rowAccessWindowSize == 0 || cellList.size() >= rowAccessWindowSize){
            flush();
        }
    }

    /**
     * flush cell to sheet
     *
     * @return {@link SheetCellWriterImpl}
     */
    @Override
    public SheetCellWriterImpl flush(){
        boolean isMerge = false;
        for(ICell cell : cellList){
            if(cell instanceof SingleCell){
                handleCell((SingleCell)cell);
            }
            else if(cell instanceof MergedCell){
                MergedCell mergedCell = (MergedCell)cell;
                if(mergedCell.isMerged()){
                    isMerge = true;
                    CellOperateUtil.mergeCellsByUnsafe(sheet, mergedCell.getFirstRowNum(), mergedCell.getFirstColumnNum(), mergedCell.getLastRowNum(), mergedCell.getLastColumnNum());
                }
                handleCell(mergedCell);
            }
            else if(cell instanceof ImageCell){
                ImageCell imageCell = (ImageCell)cell;
                if(imageCell.isMerged()){
                    isMerge = true;
                    CellOperateUtil.mergeCellsByUnsafe(sheet, imageCell.getFirstRowNum(), imageCell.getFirstColumnNum(), imageCell.getLastRowNum(), imageCell.getLastColumnNum());
                }
                handleCell(imageCell);
            }
        }
        if(isMerge){
            sheet.validateMergedRegions();
        }
        this.cellList.clear();
        return this;
    }

    protected void handleCell(SingleCell cell) {
        Row sRow = getOrCreateRow(cell.getFirstRowNum());
        Cell sCell = getOrCreateCell(sRow, cell.getFirstColumnNum());

        if(cell.getCellStyle() != null){
            setCellStyle(sCell, cell.getCellStyle());
        }
        setCellValue(sCell, cell.getLabel());
        addValidationData(cell);
    }

    protected void handleCell(MergedCell cell) {
        Row sRow = getOrCreateRow(cell.getFirstRowNum());
        Cell sCell = getOrCreateCell(sRow, cell.getFirstColumnNum());
        setCellStyle(cell.getFirstRowNum(), cell.getFirstColumnNum(), cell.getLastRowNum(), cell.getLastColumnNum(), cell.getCellStyle());
        setCellValue(sCell, cell.getLabel());
        addValidationData(cell);
    }

    protected void handleCell(ImageCell cell) {
        addPicture(cell.getFirstRowNum(), cell.getFirstColumnNum(), cell.getLastRowNum(), cell.getLastColumnNum(), cell.getValue(), cell.getPictureType());
    }

    protected void addValidationData(ICellSupportMeta cell){
        if(supportValid && cell != null && cell.supportMeta() && cell.isValid()){
            int startRowNum = cell.getFirstRowNum();
            int startColumnNum = cell.getFirstColumnNum();

            String formula = cell.createNameName();
            DataValidation dataValidation = DataValidationUtil.createValidation(sheet, formula, startRowNum, startRowNum, startColumnNum, startColumnNum);
            sheet.addValidationData(dataValidation);
        }
    }

    /**
     * 扩展列之前,刷新数据,防止遗漏
     */
    @Override
    public void autoSizeColumn(int... columnNums){
        flush();
        super.autoSizeColumn(columnNums);
    }



}

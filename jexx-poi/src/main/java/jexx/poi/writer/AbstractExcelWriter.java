package jexx.poi.writer;

import jexx.poi.AbstractExcel;
import jexx.poi.meta.IMetaWriter;
import jexx.poi.meta.MetaSheetWriter;
import jexx.poi.meta.Metas;
import jexx.util.Assert;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.SheetVisibility;
import org.apache.poi.ss.usermodel.Workbook;

/**
 * ExcelWriter抽象类
 * @author jeff
 * @since 2019/10/4
 */
public abstract class AbstractExcelWriter extends AbstractExcel {

    /**
     * define the version of excel
     */
    protected int version;
    /**
     * hide the meta sheet, default true
     */
    protected boolean hiddenMetaSheet;
    protected IMetaWriter metaWriter;



    /**
     * 创建excel的写入类
     * @param workbook 工作簿
     * @param version the version of excel
     */
    public AbstractExcelWriter(Workbook workbook, boolean hiddenMetaSheet, int version) {
        Assert.isTrue(version >= 0, "version must be equal or greater than 0");
        this.workbook = workbook;
        this.hiddenMetaSheet = hiddenMetaSheet;
        this.version = version;
    }

    public void setHiddenMetaSheet(boolean hiddenMetaSheet) {
        this.hiddenMetaSheet = hiddenMetaSheet;
    }

    /**
     * 创建含有meta信息的sheet
     * @param metas meta信息集合
     */
    public void createMetaSheet(Metas metas){
        Assert.isNull(metaWriter, "Can't repeatedly create meta sheet!");
        Sheet sheet = getOrCreateSheet(METADATA_SHEET_NAME);
        if(hiddenMetaSheet){
            workbook.setSheetVisibility(workbook.getSheetIndex(sheet), SheetVisibility.VERY_HIDDEN);
        }
        metaWriter = new MetaSheetWriter(sheet);
        metaWriter.save(metas);
        this.meta = metas;
    }

}

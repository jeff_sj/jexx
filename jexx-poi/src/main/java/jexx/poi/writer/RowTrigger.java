package jexx.poi.writer;

import jexx.poi.row.DataRow;
import jexx.poi.row.RowContext;

/**
 * 行触发器, 用于处理一行结束后的逻辑
 * <pre>
 *     一般用于添加单元格
 * </pre>
 *
 * @author jeff
 * @since 2019/5/25
 * @since 1.0.6.1
 */
public interface RowTrigger {

    void append(RowContext rowContext, DataRow row);

}

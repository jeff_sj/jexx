package jexx.poi.writer;

import jexx.poi.cell.MergedCell;
import jexx.poi.header.Headers;
import jexx.poi.header.ICustomHeader;
import jexx.poi.header.IDataHeader;
import jexx.poi.row.DataRow;
import jexx.poi.row.Row;
import jexx.poi.row.RowContext;
import jexx.util.CollectionUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * @author jeff
 * @since 2019/6/21
 */
public abstract class AbstractRowWriter {

    private static final Logger LOG = LoggerFactory.getLogger(AbstractRowWriter.class);

    /**
     * 写自定义header的数据
     *
     * @param context 行容器
     * @param headers headers
     * @param itemIndex 写入集合数据时，该行对应的索引
     */
    protected void writeCustom(RowContext context, Headers headers, int itemIndex){
        List<ICustomHeader> customHeaders = headers.getCustomHeaders();
        if(CollectionUtil.isEmpty(customHeaders)){
            return;
        }
        doWriteCustom(context, context.getRootRow(), customHeaders, itemIndex);
    }

    protected void doWriteCustom(RowContext context, DataRow row, List<ICustomHeader> customHeaders, int itemIndex){
        for(ICustomHeader customHeader : customHeaders){
            IDataHeader sameLevelHeader = customHeader.getSameLevelHeader();
            if(sameLevelHeader == null){
                if(row == context.getRootRow()){
                    Object value = customHeader.getCustomValue(context.getRootRow(), itemIndex);

                    MergedCell cell = new MergedCell(row, customHeader.getStartColumnNum(), customHeader.getEndColumnNum(), value, customHeader.getDataCellStyle());
                    context.addCell(row, cell);
                }
            }
            else{
                if(row.hasHeaderDataInChild(sameLevelHeader.getStartColumnNum())){
                    if(CollectionUtil.isNotEmpty(row.getChildren())){
                        for(int i = 0; i < row.getChildren().size(); i++){
                            Row r = row.getChildren().get(i);
                            if(r instanceof DataRow){
                                DataRow dataRow = (DataRow)r;
                                Object value = customHeader.getCustomValue(dataRow, i);
                                MergedCell cell = new MergedCell(dataRow, customHeader.getStartColumnNum(), customHeader.getEndColumnNum(), value, customHeader.getDataCellStyle());
                                context.addCell(dataRow, cell);
                            }
                        }
                    }
                }
            }
        }

        List<Row> children = row.getChildren();
        if(CollectionUtil.isNotEmpty(children)){
            for(int i = 0; i < row.getChildren().size(); i++){
                Row r = row.getChildren().get(i);
                if(r instanceof DataRow){
                    doWriteCustom(context, (DataRow)r, customHeaders, itemIndex);
                }
            }
        }
    }

}

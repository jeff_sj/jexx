package jexx.poi.writer;

import jexx.bean.DyBeanImpl;
import jexx.bean.IndexProperty;
import jexx.poi.cell.ICellSupportMeta;
import jexx.poi.cell.IMergedCell;
import jexx.poi.cell.MergedCell;
import jexx.poi.header.Headers;
import jexx.poi.header.IDataHeader;
import jexx.poi.header.row.HeaderRow;
import jexx.poi.header.row.HeaderRowContext;
import jexx.poi.row.DataRow;
import jexx.poi.row.Row;
import jexx.poi.row.RowContext;
import jexx.poi.util.HeaderPathUtil;
import jexx.util.CollectionUtil;
import jexx.util.StringPool;
import jexx.util.StringUtil;

import java.util.*;
import java.util.stream.Collectors;

/**
 * write index row
 *
 * @author jeff
 * @since 2019/5/21
 */
public class IndexRowWriter extends AbstractRowWriter implements RowWriter<Object> {

    @Override
    public RowContext writeRow(int startRowNum, Object data, Headers headers, int itemIndex, boolean ignoreDataValid) {
        RowContext context = toRow(startRowNum, data, headers, itemIndex, ignoreDataValid);
        //行向下移,会导致行号变化,因此需要执行行触发器,再执行写自定义header
        doRowTrigger(context, context.getRootRow(), headers);
        writeCustom(context, headers, itemIndex);
        return context;
    }

    protected RowContext toRow(int startRowNum, Object bean, Headers headers, int itemIndex, boolean ignoreDataValid){
        DyBeanImpl dyBean = new DyBeanImpl();
        dyBean.setWrappedInstance(bean);

        //对header的key进行层次排序
        List<IDataHeader> headerList = new ArrayList<>();
        HeaderRowContext headerRowContext = headers.getHeaderRowContext();
        List<HeaderRow> headerRows = headerRowContext.getHeaderRows().stream()
                .sorted(Comparator.comparingInt(HeaderRow::getLevel)).collect(Collectors.toList());
        for(HeaderRow headerRow : headerRows){
            headerRow.getHeaderMap().forEach((k,v)->headerList.add(v));
        }

        //row
        Map<String, DataRow> rows = new HashMap<>(16);
        DataRow rootRow = new DataRow(startRowNum);
        RowContext rowContext = new RowContext(rootRow, headers.getHeaderRowContext());
        rows.put(StringPool.EMPTY, rootRow);

        doRecursionHeader(new ArrayList<>(headerList), new HashMap<>(), rowContext, dyBean, rows, itemIndex, ignoreDataValid);
        return rowContext;
    }

    protected void doRecursionHeader(List<IDataHeader> headerList, Map<IDataHeader, Object> checkMap, RowContext rowContext, DyBeanImpl dyBean, Map<String, DataRow> rows, int itemIndex,
                                     boolean ignoreDataValid){
        HeaderRowContext headerRowContext = rowContext.getHeaderRowContext();

        int preSize = headerList.size();
        Iterator<IDataHeader> iterator = headerList.iterator();
        while(iterator.hasNext()){
            IDataHeader header = iterator.next();

            IDataHeader referHeader = header.getReferHeader();
            if(referHeader != null && checkMap.get(referHeader) == null){
                continue;
            }

//            if(header instanceof ICustomHeader){
//                Object value = ((ICustomHeader) header).getCustomValue(rowContext.getRootRow(), itemIndex);
//                rowContext.addCell(new MergedCell(rowContext.getRootRow(), header.getStartColumnNum(), header.getStartColumnNum(), value));
//
//                //remove header
//                checkMap.put(header, new Object());
//                iterator.remove();
//                continue;
//            }

            List<IndexProperty> indexProperties = dyBean.getIndexPropertyValues(header.getKey());

            String key;
            Object value;
            for(IndexProperty indexProperty : indexProperties) {
                key = indexProperty.getPath();
                value = indexProperty.getValue();

                List<String> paths = HeaderPathUtil.splitDataPaths(header.getKey(), key);
                paths.add(0, StringPool.EMPTY);
                int pathSize = paths.size();

                String lastPath = paths.get(pathSize-1);
                DataRow row = rows.get(lastPath);
                if(row == null){
                    String path = paths.get(pathSize-2);
                    DataRow parentRow = rows.get(path);
                    if(parentRow == null){
                        throw new IllegalArgumentException("row[path={}] is null ");
                    }
                    row = rowContext.addDataRowAt(parentRow);
                    row.setParentRow(parentRow);
                    row.setPath(lastPath);

                    if(headerRowContext != null){
                        row.setHeaderRow(headerRowContext.getHeaderRowByHeader(header));
                    }
                    rows.put(lastPath, row);
                }

                MergedCell cell = new MergedCell(row, header.getStartColumnNum(), header.getStartColumnNum(), value, header.getDataCellStyle());
                cell.setMeta(header.getMeta());
                cell.valid(!ignoreDataValid && header.isValid());
                cell.setMultiValue(header.isMultiValue());
                rowContext.addCell(row, cell);
                if(referHeader != null){
                    IMergedCell referCell = row.getCellByColumnNum(referHeader.getStartColumnNum());
                    assert referCell != null;
                    cell.setCellReference((ICellSupportMeta)referCell);
                }
            }

            //remove header
            checkMap.put(header, new Object());
            iterator.remove();
        }

        int postSize = headerList.size();
        if(postSize > 0 && postSize == preSize){
            String referKeys = StringUtil.join(headerList, ",", IDataHeader::getReferHeader);
            throw new IllegalArgumentException(StringUtil.format("Please check the header column's referKey, referKeys=[{}] ", referKeys));
        }

        if(postSize > 0){
            doRecursionHeader(headerList, checkMap, rowContext, dyBean, rows, itemIndex, ignoreDataValid);
        }

    }

    /**
     * 循环遍历行数据，并触发处理指定列的事件
     *
     * @param context 行容器
     * @param row 定位到的行
     * @param headers headers
     */
    private void doRowTrigger(RowContext context, DataRow row, Headers headers){
        List<Row> children = row.getChildren();
        if(CollectionUtil.isNotEmpty(children)){
            for(Row row1 : children){
                if(row1 instanceof DataRow){
                    doRowTrigger(context, (DataRow)row1, headers);
                }
            }
        }

        List<IMergedCell> cells = row.getCells();
        if(CollectionUtil.isNotEmpty(cells)){
            for(IMergedCell cell : cells){
                List<RowTrigger> rowTriggers = headers.getRowTriggers(cell.getFirstColumnNum());
                if(CollectionUtil.isNotEmpty(rowTriggers)){
                    for(RowTrigger trigger : rowTriggers){
                        trigger.append(context, row);
                    }
                }
            }
        }
    }

}

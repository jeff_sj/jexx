package jexx.poi.writer;

import jexx.poi.header.Headers;
import jexx.poi.row.DataRow;
import jexx.poi.row.RowContext;

/**
 *
 * the interface that write one data into excel
 *
 * @author jeff
 * @since 2019/5/21
 */
public interface RowWriter<T> {

    /**
     * write a bean into excel, and return a data row
     *
     * @param startRowNum start row num
     * @param data one data
     * @param headers headers
     * @param itemIndex the index of collection, default 0
     * @param ignoreDataValid 是否忽视单元格的数据有效性; 会影响cell的数据有效性属性
     * @return {@link DataRow}
     */
    RowContext writeRow(int startRowNum, T data, Headers headers, int itemIndex, boolean ignoreDataValid);

    default RowContext writeRow(int startRowNum, T data, Headers headers, int itemIndex){
        return writeRow(startRowNum, data, headers, itemIndex, false);
    }

    default RowContext writeRow(int startRowNum, T data, Headers headers){
        return writeRow(startRowNum, data, headers, 0);
    }

}

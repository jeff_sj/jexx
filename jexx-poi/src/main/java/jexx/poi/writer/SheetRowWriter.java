package jexx.poi.writer;

import jexx.poi.cell.IMergedCell;
import jexx.poi.row.RowContext;
import jexx.poi.style.CellStyleMapper;

import java.util.function.BiConsumer;
import java.util.function.Consumer;

/**
 * @author jeff
 * @since 2019/10/2
 */
public interface SheetRowWriter {

    /**
     * 写入行数据
     * @param row {@link RowContext}
     * @param cellStyleMapper {@link CellStyleMapper}
     * @return SheetRowWriter的子类
     */
    SheetRowWriter writeRow(final RowContext row, final CellStyleMapper cellStyleMapper);

    /**
     * 写入行数据
     * @param row {@link RowContext}
     * @param consumer 加工cell
     * @return SheetRowWriter的子类
     */
    SheetRowWriter writeRow(final RowContext row, final Consumer<IMergedCell> consumer);

    <T> SheetRowWriter writeRow(final RowContext row, T bean, final BiConsumer<IMergedCell, T> consumer);

}

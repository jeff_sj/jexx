package jexx.poi.writer;

import jexx.io.FileUtil;
import jexx.poi.ExcelWriter;
import jexx.poi.constant.PictureType;
import jexx.poi.header.Headers;
import jexx.poi.header.IHeader;
import jexx.poi.style.IWrapCellStyle;
import jexx.poi.style.StyleUtil;
import jexx.poi.style.WrapCellStyle;
import jexx.poi.util.CellOperateUtil;
import jexx.poi.util.RowOperationUtil;
import jexx.poi.util.SheetUtil;
import jexx.util.Assert;
import jexx.util.CollectionUtil;
import jexx.util.StringUtil;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.CellUtil;
import org.apache.poi.ss.util.RegionUtil;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * SheetWriter抽象类
 *
 * @author Jeff
 */
public abstract class AbstractSheetWriter {

    protected Sheet sheet;
    protected DataFormat dataFormat;
    protected FormulaEvaluator formulaEvaluator;

    /**
     * 是否支持在写入单元格数据时去除两端空格;
     * <b>去除空格会导致读取meta信息对应的列失效</b>
     */
    @Deprecated
    protected boolean supportTrimData = false;

    public AbstractSheetWriter(Sheet sheet) {
        this.sheet = sheet;
    }


    /**
     * 名称
     */
    public String getName(){
        return this.sheet.getSheetName();
    }


    //--------------------------------------------------------- complex operation

    /**
     * 在指定行插入指定数量的行数; 复制startRowNum的上一行样式, 不复制约束.
     * 若要使用约束，请使用
     * @param startRowNum 指定行, 从1开始算起
     * @param rowNum 行数量
     */
    public void insertRows(int startRowNum, int rowNum){
        Assert.isTrue(startRowNum > 1, "startRowNum must be greater than 1");
        int lastRowIndexOfSheet = sheet.getLastRowNum();
        Assert.isTrue(lastRowIndexOfSheet > 0, "Sheet[{}] has no row", sheet.getSheetName());
        sheet.shiftRows(startRowNum - 1, lastRowIndexOfSheet, rowNum, true, false);

        //merge
        List<Integer> indices = new ArrayList<>();
        List<CellRangeAddress> cellRangeAddresses = new ArrayList<>();
        List<CellRangeAddress> cellRangeAddressList = sheet.getMergedRegions();

        int startRowIndex = startRowNum - 1;
        int lastRowIndex = startRowIndex - 1;
        for (int i = 0; i < cellRangeAddressList.size(); i++) {
            CellRangeAddress cellRangeAddress = sheet.getMergedRegion(i);
            //行合并单元格
            if(startRowIndex >= cellRangeAddress.getFirstRow() && startRowIndex <= cellRangeAddress.getLastRow()){
                cellRangeAddress.setLastRow(cellRangeAddress.getLastRow()+rowNum);
                indices.add(i);
                cellRangeAddresses.add(cellRangeAddress);
            }
            //列合并单元格
            if(lastRowIndex == cellRangeAddress.getFirstRow() && lastRowIndex == cellRangeAddress.getLastRow()){
                for(int j = 0; j < rowNum; j++){
                    CellRangeAddress newCellRangeAddress = new CellRangeAddress(startRowNum+j-1, startRowNum+j-1, cellRangeAddress.getFirstColumn(), cellRangeAddress.getLastColumn());
                    cellRangeAddresses.add(newCellRangeAddress);
                }
            }
        }
        if(CollectionUtil.isNotEmpty(indices)){
            sheet.removeMergedRegions(indices);
        }
        if(CollectionUtil.isNotEmpty(cellRangeAddresses)){
            for(CellRangeAddress cellRangeAddress : cellRangeAddresses){
                //addMergedRegionUnsafe 速度优于 addMergedRegion
                sheet.addMergedRegionUnsafe(cellRangeAddress);
            }
            sheet.validateMergedRegions();
        }

        //style
        org.apache.poi.ss.usermodel.Row lastRow = sheet.getRow(lastRowIndex);
        if(lastRow == null){
            return;
        }

        for(int i = startRowNum; i < startRowNum+rowNum; i++){
            org.apache.poi.ss.usermodel.Row row = getOrCreateRow(i);
            //fixed 行高未复制,重新设置
            row.setHeight(lastRow.getHeight());
            for(short j = 0; j <= lastRow.getLastCellNum(); j++){
                org.apache.poi.ss.usermodel.Cell lastCell = lastRow.getCell(j);
                if(lastCell != null){
                    org.apache.poi.ss.usermodel.Cell cell = getOrCreateCell(row, j+1);
                    cell.setCellStyle(lastCell.getCellStyle());
                }
            }
        }
    }


    public void setSupportTrimData(boolean supportTrimData) {
        this.supportTrimData = supportTrimData;
    }

    //--------------------------------------------------------- 图片

    /**
     * 添加图片
     * @param row1Num 第一个单元格的行号,从1开始
     * @param col1Num 第一个单元格的列号,从1开始
     * @param row2Num 第二个单元格的行号,从1开始
     * @param col2Num 第二个单元格的列号,从1开始
     * @param imgBuffer 二进制图片
     * @param pictureType 图片类型
     * @return 添加后的图片索引
     */
    protected int addPicture(int row1Num, int col1Num, int row2Num, int col2Num, byte[] imgBuffer, PictureType pictureType){
        Assert.isTrue(row1Num > 0, "row1Num must be greater than 0");
        Assert.isTrue(col1Num > 0, "col1Num must be greater than 0");
        Assert.isTrue(row2Num > 0, "row2Num must be greater than 0");
        Assert.isTrue(col2Num > 0, "col2Num must be greater than 0");
        Drawing drawing = sheet.getDrawingPatriarch();
        if(drawing == null){
            drawing = sheet.createDrawingPatriarch();
        }

        CreationHelper helper = sheet.getWorkbook().getCreationHelper();
        ClientAnchor anchor = helper.createClientAnchor();
        anchor.setRow1(row1Num-1);
        anchor.setCol1(col1Num-1);
        anchor.setRow2(row2Num);
        anchor.setCol2(col2Num);
        anchor.setAnchorType(ClientAnchor.AnchorType.MOVE_AND_RESIZE);

        int pictureIdx = sheet.getWorkbook().addPicture(imgBuffer, pictureType.getType());
        Picture pict = drawing.createPicture(anchor, pictureIdx);
        pict.resize(1,1);
        return pictureIdx;
    }

    /**
     * 添加图片
     * @param row1Num 第一个单元格的行号,从1开始
     * @param col1No 第一个单元格的行字母编号,从A开始
     * @param row2Num 第二个单元格的行号,从1开始
     * @param col2No 第二个单元格的行字母编号,从A开始
     * @param imgBuffer 二进制图片
     * @param pictureType 图片类型
     * @return 添加后的图片索引
     */
    protected int addPicture(int row1Num, String col1No, int row2Num, String col2No, byte[] imgBuffer, PictureType pictureType){
        return addPicture(row1Num, CellOperateUtil.toColumnNum(col1No), row2Num, CellOperateUtil.toColumnNum(col2No), imgBuffer, pictureType);
    }

    /**
     * 某个单元格插入图片
     * @param rowNum 单元格的行,从1开始
     * @param colNum 单元格的列,从1开始
     * @param imgBuffer 二进制图片
     * @param pictureType 图片类型
     * @return 添加后的图片索引
     */
    protected int addPicture(int rowNum, int colNum, byte[] imgBuffer, PictureType pictureType){
        return addPicture(rowNum, colNum, rowNum, colNum, imgBuffer, pictureType);
    }

    /**
     * 某个单元格插入图片
     * @param rowNum 单元格的行,从1开始
     * @param colNo 单元格的列,从A开始
     * @param imgBuffer 二进制图片
     * @param pictureType 图片类型
     * @return 添加后的图片索引
     */
    protected int addPicture(int rowNum, String colNo, byte[] imgBuffer, PictureType pictureType){
        return addPicture(rowNum, CellOperateUtil.toColumnNum(colNo), imgBuffer, pictureType);
    }

    /**
     * 添加图片
     * @param row1Num 第一个单元格的行号,从1开始
     * @param col1Num 第一个单元格的列号,从1开始
     * @param row2Num 第二个单元格的行号,从1开始
     * @param col2Num 第二个单元格的列号,从1开始
     * @param imageFile 文件
     * @param pictureType 图片类型
     */
    protected int addPicture(int row1Num, int col1Num, int row2Num, int col2Num, File imageFile, PictureType pictureType){
        byte[] imgBuffer = FileUtil.read(imageFile);
        return addPicture(row1Num, col1Num, row2Num, col2Num, imgBuffer, pictureType);
    }

    /**
     * 添加图片
     * @param row1Num 第一个单元格的行号,从1开始
     * @param col1No 第一个单元格的行字母编号,从A开始
     * @param row2Num 第二个单元格的行号,从1开始
     * @param col2No 第二个单元格的行字母编号,从A开始
     * @param imageFile 文件
     * @param pictureType 图片类型
     */
    protected int addPicture(int row1Num, String col1No, int row2Num, String col2No, File imageFile, PictureType pictureType){
        byte[] imgBuffer = FileUtil.read(imageFile);
        return addPicture(row1Num, CellOperateUtil.toColumnNum(col1No), row2Num, CellOperateUtil.toColumnNum(col2No), imgBuffer, pictureType);
    }

    /**
     * 某个单元格插入图片
     * @param rowNum 单元格的行,从1开始
     * @param colNum 单元格的列,从1开始
     * @param imageFile 图片文件
     * @param pictureType 图片类型
     * @return 添加后的图片索引
     */
    protected int addPicture(int rowNum, int colNum, File imageFile, PictureType pictureType){
        byte[] imgBuffer = FileUtil.read(imageFile);
        return addPicture(rowNum, colNum, imgBuffer, pictureType);
    }

    /**
     * 某个单元格插入图片
     * @param rowNum 单元格的行,从1开始
     * @param colNo 单元格的列,从A开始
     * @param imageFile 图片文件
     * @param pictureType 图片类型
     * @return 添加后的图片索引
     */
    protected int addPicture(int rowNum, String colNo, File imageFile, PictureType pictureType){
        return addPicture(rowNum, CellOperateUtil.toColumnNum(colNo), imageFile, pictureType);
    }

    /**
     * 某个单元格插入图片
     * @param rowNum 单元格的行,从1开始
     * @param colNum 单元格的列,从1开始
     * @param imageFile 图片文件
     * @return 添加后的图片索引
     */
    protected int addPicture(int rowNum, int colNum, File imageFile){
        PictureType pictureType = PictureType.checkType(imageFile.getName());
        if(pictureType == null){
            pictureType = PictureType.PNG;
        }
        return addPicture(rowNum, colNum, imageFile, pictureType);
    }

    /**
     * 某个单元格插入图片
     * @param rowNum 单元格的行,从1开始
     * @param colNo 单元格的列,从A开始
     * @param imageFile 图片文件
     * @return 添加后的图片索引
     */
    protected int addPicture(int rowNum, String colNo, File imageFile){
        return addPicture(rowNum, CellOperateUtil.toColumnNum(colNo), imageFile);
    }


    //--------------------------------------------------------- auto

    public void autoSizeColumn(int... columnNums){
        for (int columnNum : columnNums){
            double width = SheetUtil.getColumnWidth(sheet, columnNum, true);
            if (width != -1) {
                width *= 256;
                // The maximum column columnCount for an individual cell is 255 characters
                int maxColumnWidth = 255*256;
                if (width > maxColumnWidth) {
                    width = maxColumnWidth;
                }
                setColumnWidth(columnNum, (int)(width));
            }
        }
    }

    /**
     * 按照header自动扩展列
     *
     * @param headers headers
     */
    public void autoSizeColumn(Headers headers){
        List<IHeader> headerList = headers.getHeaders();
        for(IHeader header : headerList){
            autoSizeColumn(header.getStartColumnNum());
        }
    }

    /**
     * 扩展当列之前的所有单元格
     * @param maxColumnNum 从1开始
     */
    public void autoSizeColumnWithMaxColumnNum(int maxColumnNum){
        for(int i = 1; i <= maxColumnNum; i++){
            autoSizeColumn(i);
        }
    }

    /**
     * set column columnCount
     * @param columnNum column num, base 1
     * @param width column's columnCount
     */
    public void setColumnWidth(int columnNum, int width){
        sheet.setColumnWidth(columnNum - 1, width);
    }

    /**
     * 更新计算所有含有公式的单元格
     * @see ExcelWriter#evaluateFormulaCell()
     */
    @Deprecated
    public void evaluateFormulaCell(){
        sheet.getWorkbook().getCreationHelper().createFormulaEvaluator().evaluateAll();
    }


    //--------------------------------------------------------- cell style

    /**
     * get cell style's format from cell
     * @param cellStyle cell style
     * @return cell style's properties
     */
    protected Map<String, Object> getFormatProperties(IWrapCellStyle cellStyle){
        Map<String, Object> properties = new HashMap<>(25);
        properties.put(CellUtil.ALIGNMENT, cellStyle.getAlignment());
        properties.put(CellUtil.VERTICAL_ALIGNMENT, cellStyle.getVerticalAlignment());

        properties.put(CellUtil.BORDER_BOTTOM, cellStyle.getBottomBorderStyle());
        properties.put(CellUtil.BORDER_LEFT, cellStyle.getLeftBorderStyle());
        properties.put(CellUtil.BORDER_RIGHT, cellStyle.getRightBorderStyle());
        properties.put(CellUtil.BORDER_TOP, cellStyle.getTopBorderStyle());

        IndexedColors bottomBorderColor = cellStyle.getBottomBorderColor();
        properties.put(CellUtil.BOTTOM_BORDER_COLOR, bottomBorderColor != null ? bottomBorderColor.getIndex() : null);
        IndexedColors leftBorderColor = cellStyle.getLeftBorderColor();
        properties.put(CellUtil.LEFT_BORDER_COLOR, leftBorderColor != null ? leftBorderColor.getIndex() : null);
        IndexedColors rightBorderColor = cellStyle.getRightBorderColor();
        properties.put(CellUtil.RIGHT_BORDER_COLOR, rightBorderColor != null ? rightBorderColor.getIndex() : null);
        IndexedColors topBorderColor = cellStyle.getTopBorderColor();
        properties.put(CellUtil.TOP_BORDER_COLOR, topBorderColor != null ? topBorderColor.getIndex() : null);

        properties.put(CellUtil.FILL_PATTERN, cellStyle.getFillPattern());
        IndexedColors fillForegroundColor = cellStyle.getFillForegroundColor();
        properties.put(CellUtil.FILL_FOREGROUND_COLOR, fillForegroundColor != null ? fillForegroundColor.getIndex() : 0);
        IndexedColors fillBackgroundColor = cellStyle.getFillBackgroundColor();
        properties.put(CellUtil.FILL_BACKGROUND_COLOR, fillBackgroundColor != null ? fillBackgroundColor.getIndex() : 0);

        properties.put(CellUtil.HIDDEN, cellStyle.isHidden());
        properties.put(CellUtil.LOCKED, cellStyle.isLocked());
        properties.put(CellUtil.INDENTION, cellStyle.getIndent());
        properties.put(CellUtil.ROTATION, cellStyle.getRotation());
        properties.put(CellUtil.WRAP_TEXT, cellStyle.isWrapped());

        //skip font
        properties.put(CellUtil.FONT, 0);

        //do data format
        if(StringUtil.isNotEmpty(cellStyle.getDataFormat())){
            properties.put(CellUtil.DATA_FORMAT, getDataFormat().getFormat(cellStyle.getDataFormat()));
        }
        return properties;
    }

    /**
     * set cell style
     * @param cell cell
     * @param cellStyle cell style
     */
    protected void setCellStyle(Cell cell, IWrapCellStyle cellStyle){
        CellUtil.setCellStyleProperties(cell, getFormatProperties(cellStyle));
    }

    /**
     * set range cell's cell style
     * @param firstRowNum first row num
     * @param firstColumnNum first column num
     * @param lastRowNum last row num
     * @param lastColumnNum last column num
     * @param cellStyle cell style
     */
    protected void setCellStyle(int firstRowNum, int firstColumnNum, int lastRowNum, int lastColumnNum, IWrapCellStyle cellStyle) {
        if(cellStyle == null){
            return;
        }

        CellRangeAddress cellRangeAddress = new CellRangeAddress(firstRowNum-1, lastRowNum-1, firstColumnNum-1, lastColumnNum-1);
        setCellStyle(cellRangeAddress, cellStyle);
    }

    /**
     * set range cell's style
     * @param cellRangeAddress cell range address
     * @param cellStyle  cell style
     */
    protected void setCellStyle(CellRangeAddress cellRangeAddress, IWrapCellStyle cellStyle) {
        int firstRowNum = cellRangeAddress.getFirstRow() + 1;
        int firstColumnNum = cellRangeAddress.getFirstColumn() + 1;
        int lastRowNum = cellRangeAddress.getLastRow() + 1;
        int lastColumnNum = cellRangeAddress.getLastColumn() + 1;

        Row row = RowOperationUtil.getRow(sheet, firstRowNum);
        Cell cell = CellOperateUtil.getCell(row, firstColumnNum);
        setCellStyle(cell, cellStyle);

        if(lastRowNum > firstRowNum || lastColumnNum > firstColumnNum){
            //left
            if(cellStyle.getLeftBorderStyle() != null){
                RegionUtil.setBorderLeft(cellStyle.getLeftBorderStyle(), cellRangeAddress, sheet);
            }
            if(cellStyle.getLeftBorderColor() != null){
                RegionUtil.setLeftBorderColor(cellStyle.getLeftBorderColor().index, cellRangeAddress, sheet);
            }
            //top
            if(cellStyle.getTopBorderStyle() != null){
                RegionUtil.setBorderTop(cellStyle.getTopBorderStyle(), cellRangeAddress, sheet);
            }
            if(cellStyle.getTopBorderColor() != null){
                RegionUtil.setTopBorderColor(cellStyle.getTopBorderColor().index, cellRangeAddress, sheet);
            }
            //right
            if(cellStyle.getRightBorderStyle() != null){
                RegionUtil.setBorderRight(cellStyle.getRightBorderStyle(), cellRangeAddress, sheet);
            }
            if(cellStyle.getRightBorderColor() != null){
                RegionUtil.setRightBorderColor(cellStyle.getRightBorderColor().index, cellRangeAddress, sheet);
            }
            //bottom
            if(cellStyle.getBottomBorderStyle() != null){
                RegionUtil.setBorderBottom(cellStyle.getBottomBorderStyle(), cellRangeAddress, sheet);
            }
            if(cellStyle.getBottomBorderColor() != null){
                RegionUtil.setBottomBorderColor(cellStyle.getBottomBorderColor().index, cellRangeAddress, sheet);
            }
        }
    }

    public IWrapCellStyle getCellStyle(int rowNum, int columnNum){
        Row row = RowOperationUtil.getRow(sheet, rowNum);
        Cell cell = CellOperateUtil.getCell(row, columnNum);

        CellStyle cellStyle = cell.getCellStyle();
        if(cellStyle == null){
            return null;
        }

        WrapCellStyle wrapCellStyle = new WrapCellStyle();
        wrapCellStyle.setAlignment(cellStyle.getAlignmentEnum());
        wrapCellStyle.setVerticalAlignment(cellStyle.getVerticalAlignmentEnum());

        wrapCellStyle.setBottomBorderStyle(cellStyle.getBorderBottomEnum());
        wrapCellStyle.setLeftBorderStyle(cellStyle.getBorderLeftEnum());
        wrapCellStyle.setRightBorderStyle(cellStyle.getBorderRightEnum());
        wrapCellStyle.setTopBorderStyle(cellStyle.getBorderTopEnum());

        wrapCellStyle.setBottomBorderColor(IndexedColors.fromInt(cellStyle.getBottomBorderColor()));
        wrapCellStyle.setLeftBorderColor(IndexedColors.fromInt(cellStyle.getLeftBorderColor()));
        wrapCellStyle.setRightBorderColor(IndexedColors.fromInt(cellStyle.getRightBorderColor()));
        wrapCellStyle.setTopBorderColor(IndexedColors.fromInt(cellStyle.getTopBorderColor()));

        wrapCellStyle.setFillPattern(cellStyle.getFillPatternEnum());
        wrapCellStyle.setFillBackgroundColor(IndexedColors.fromInt(cellStyle.getFillBackgroundColor()));
        wrapCellStyle.setFillBackgroundColor(IndexedColors.fromInt(cellStyle.getFillForegroundColor()));

        wrapCellStyle.setHidden(cellStyle.getHidden());
        wrapCellStyle.setLocked(cellStyle.getLocked());
        wrapCellStyle.setIndent(cellStyle.getIndention());
        wrapCellStyle.setRotation(cellStyle.getRotation());
        wrapCellStyle.setWrapped(cellStyle.getWrapText());

        wrapCellStyle.setDataFormat(getDataFormat().getFormat(cellStyle.getDataFormat()));
        return wrapCellStyle;
    }


    //--------------------------------------------------------- create row or column

    /**
     * 获取行,若无则创建
     * @param rowNum 行号,从1开始
     */
    protected Row getOrCreateRow(int rowNum){
        Assert.isTrue(rowNum > 0);
        return RowOperationUtil.getRow(sheet, rowNum);
    }

    /**
     * 获取单元格,若无则创建
     * @param row 行
     * @param columnNum 列号,从1开始
     */
    protected Cell getOrCreateCell(Row row, int columnNum){
        Assert.isTrue(columnNum > 0, "columnNum={}", columnNum);
        return CellOperateUtil.getCell(row, columnNum);
    }

    protected void setCellValue(Cell cell, Object value) {
        Object newValue = value;
        if(supportTrimData){
            if(newValue instanceof String){
                newValue = StringUtil.trim(newValue.toString());
            }
        }
        CellOperateUtil.setCellValue(cell, newValue);
    }

    /**
     * 获取单元格样式, 该样式时克隆后的样式。直接修改CellStyle,不能对单元格起效果。
     * 只有设置时有效
     */
    protected CellStyle getCellStyle(Cell cell){
        CellStyle cellStyle = StyleUtil.createCellStyle(sheet.getWorkbook());
        if(cell.getCellStyle() != null){
            cellStyle.cloneStyleFrom(cell.getCellStyle());
        }
        return cellStyle;
    }

    /**
     * 创建单元格样式, 如果
     */
    protected CellStyle createCellStyle(Cell cell){
        CellStyle cellStyle = StyleUtil.createCellStyle(sheet.getWorkbook());
        if(cell.getCellStyle() != null){
            cellStyle.cloneStyleFrom(cell.getCellStyle());
        }
        return cellStyle;
    }

    protected DataFormat getDataFormat(){
        if(this.dataFormat == null){
            this.dataFormat = this.sheet.getWorkbook().createDataFormat();
        }
        assert this.dataFormat != null;
        return this.dataFormat;
    }

    //--------------------------------------------------------- formula


    /**
     * 计算单元格公式值
     */
    protected CellValue evaluate(Cell cell){
        if(formulaEvaluator == null){
            formulaEvaluator = this.sheet.getWorkbook().getCreationHelper().createFormulaEvaluator();
        }
        return formulaEvaluator.evaluate(cell);
    }

}

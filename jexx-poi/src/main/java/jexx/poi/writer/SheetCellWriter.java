package jexx.poi.writer;

import jexx.poi.cell.IMergedCell;
import jexx.poi.style.CellStyleMapper;

import java.util.function.BiConsumer;
import java.util.function.Consumer;

/**
 * @author jeff
 * @since 2019/10/2
 */
public interface SheetCellWriter {

    /**
     * 设置是否支持valid, 默认支持
     * @param supportValid 是否支持valid
     */
    void setSupportValid(boolean supportValid);

    /**
     * 单元格写入
     * @param cell {@link IMergedCell}
     * @return {@link SheetCellWriter}
     */
    SheetCellWriter writeCell(IMergedCell cell);

    /**
     * 单元格写入
     * @param cell {@link IMergedCell}
     * @param cellStyleMapper {@link CellStyleMapper}
     * @return {@link SheetCellWriter}
     */
    SheetCellWriter writeCell(IMergedCell cell, final CellStyleMapper cellStyleMapper);

    /**
     * 指定单元格写入
     * @param cell {@link IMergedCell}
     * @param consumer 加工cell
     * @return {@link SheetCellWriter}
     */
    SheetCellWriter writeCell(IMergedCell cell, final Consumer<IMergedCell> consumer);

    /**
     * 指定单元格写入
     * @param cell {@link IMergedCell}
     * @param bean 指定对象数据
     * @param consumer 通过cell和指定对象处理cell
     * @param <T> 指定对象
     * @return {@link SheetCellWriter}
     */
    <T> SheetCellWriter writeCell(IMergedCell cell, T bean, final BiConsumer<IMergedCell, T> consumer);

    /**
     * 刷新所有单元格到sheet
     * @return {@link SheetCellWriter}
     */
    SheetCellWriter flush();

}

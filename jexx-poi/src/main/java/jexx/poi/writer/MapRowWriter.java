package jexx.poi.writer;

import jexx.poi.cell.ICellSupportMeta;
import jexx.poi.cell.IMergedCell;
import jexx.poi.cell.MergedCell;
import jexx.poi.header.Headers;
import jexx.poi.header.ICustomHeader;
import jexx.poi.header.IDataHeader;
import jexx.poi.row.DataRow;
import jexx.poi.row.RowContext;
import jexx.util.StringUtil;

import java.util.*;

/**
 * @author jeff
 * @since 2019/5/23
 */
public class MapRowWriter extends AbstractRowWriter implements RowWriter<Map<String, Object>> {

    @Override
    public RowContext writeRow(int startRowNum, Map<String, Object> data, Headers headers, int itemIndex, boolean ignoreDataValid) {
        DataRow row = new DataRow(startRowNum);
        RowContext rowContext = new RowContext(row, headers.getHeaderRowContext());
        List<IDataHeader> cloneHeader = new ArrayList<>(headers.getDataHeaders());
        doRecursionHeader(cloneHeader, new HashMap<>(), data, rowContext, itemIndex, ignoreDataValid);
        writeCustom(rowContext, headers, itemIndex);
        return rowContext;
    }

    protected void doRecursionHeader(List<IDataHeader> headerList, Map<IDataHeader, Object> checkMap, Map<String, Object> data, RowContext rowContext, int itemIndex,
                                     boolean ignoreDataValid){
        int preSize = headerList.size();

        Iterator<IDataHeader> iterator = headerList.iterator();
        while(iterator.hasNext()){
            IDataHeader header = iterator.next();
            IDataHeader referHeader = header.getReferHeader();
            if(referHeader != null && checkMap.get(referHeader) == null){
                continue;
            }

            Object value = data.get(header.getKey());
            if(header instanceof ICustomHeader){
                value = ((ICustomHeader) header).getCustomValue(rowContext.getRootRow(), itemIndex);
            }
            MergedCell cell = new MergedCell(rowContext.getRootRow(), header.getStartColumnNum(), header.getStartColumnNum(), value, header.getDataCellStyle());
            cell.valid(!ignoreDataValid && header.isValid());
            cell.setMeta(header.getMeta());
            cell.setMultiValue(header.isMultiValue());
            rowContext.addCell(cell);
            if(referHeader != null){
                IMergedCell referCell = rowContext.getCell(cell, referHeader.getStartColumnNum());
                assert referCell != null;
                cell.setCellReference((ICellSupportMeta)referCell);
            }

            //remove header
            checkMap.put(header, new Object());
            iterator.remove();
        }

        int postSize = headerList.size();
        if(postSize > 0 && postSize == preSize){
            String referKeys = StringUtil.join(headerList, ",", IDataHeader::getReferHeader);
            throw new IllegalArgumentException(StringUtil.format("Please check the header column's referKey, referKeys=[{}] ", referKeys));
        }

        if(postSize > 0){
            doRecursionHeader(headerList, checkMap, data, rowContext, itemIndex, ignoreDataValid);
        }
    }

}

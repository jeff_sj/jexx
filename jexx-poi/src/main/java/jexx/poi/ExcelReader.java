package jexx.poi;

import jexx.poi.exception.POIException;
import jexx.poi.meta.MetaSheetReader;
import jexx.poi.meta.Metas;
import jexx.poi.read.impl.SheetReaderImpl;
import jexx.poi.util.WorkbookUtil;
import jexx.util.StringUtil;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import java.io.File;
import java.io.InputStream;

/**
 * excel读取类
 *
 * @author jeff
 */
public class ExcelReader extends AbstractExcel {

    public ExcelReader(Workbook workbook){
        this.workbook = workbook;
        loadMeta();
    }

    public ExcelReader(File excelFile){
        this(WorkbookUtil.createBook(excelFile));
    }

    public ExcelReader(InputStream excelInputStream){
        this(WorkbookUtil.createBook(excelInputStream));
    }

    private void loadMeta(){
        Sheet sheet = getSheet(METADATA_SHEET_NAME);
        Metas meta = null;
        if(sheet != null){
            MetaSheetReader reader = new MetaSheetReader(sheet);
            meta = reader.readMeta();
        }
        this.meta = meta;
    }

    /**
     * 读取 默认名称的sheet
     */
    public SheetReader sheet(){
        return sheet(DEFAULT_SHEET_NAME);
    }

    /**
     * 当excel只有一个sheet时，直接获取此sheet
     */
    public SheetReader oneSheet(){
        int sheetNum = workbook.getNumberOfSheets();

        Sheet sheet;
        Sheet metaSheet = getSheet(METADATA_SHEET_NAME);
        if(metaSheet != null){
            sheetNum = sheetNum - 1;
            if(sheetNum != 1){
                throw new POIException("there is not one sheet!");
            }
            int sheetIndex = workbook.getSheetIndex(metaSheet) == 0 ? 1 : 0;
            sheet = getSheetAt(sheetIndex+1);
        }
        else{
            if(sheetNum != 1){
                throw new POIException("there is not one sheet!");
            }
            sheet = getSheetAt(1);
        }
        return new SheetReaderImpl(sheet);
    }

    public SheetReader sheet(String sheetName){
        if(METADATA_SHEET_NAME.equals(sheetName)){
            return null;
        }

        Sheet sheet = workbook.getSheet(sheetName);
        if(sheet == null){
            throw new POIException("Sheet {} not exist!",sheetName);
        }
        return new SheetReaderImpl(sheet);
    }

    /**
     * 指定sheet索引获取 {@link SheetReader};
     * 建议使用 具体sheet名称来构建SheetReader
     * @param sheetNum sheet索引, 从1开始
     * @return {@link SheetReader}
     */
    @Deprecated
    public SheetReader getSheet(int sheetNum){
        String sheetName = workbook.getSheetName(sheetNum-1);
        if(METADATA_SHEET_NAME.equals(sheetName)){
            return null;
        }
        return sheet(sheetName);
    }

    /**
     * 版本是否一致
     * @param version 待检测版本号
     * @return bool
     */
    public boolean matchVersion(String version){
        if(StringUtil.isEmpty(version)){
            throw new IllegalArgumentException("version is not illegal");
        }
        if(meta == null){
            return false;
        }
        return version.equals(meta.getVersion());
    }

}

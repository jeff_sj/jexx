package jexx.poi.header;

import jexx.poi.meta.ArrayMeta;
import jexx.poi.meta.DVConstraintType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

public class ArrayDataHeader extends AbstractDataHeader<ArrayDataHeader> {

    private static final Logger LOG = LoggerFactory.getLogger(ArrayDataHeader.class);

    private final ArrayMeta meta;

    public ArrayDataHeader(String key, String value, int columnCount, ArrayMeta meta) {
        super(key, value, columnCount);
        this.meta = meta;
    }

    public ArrayDataHeader(String key, String value, ArrayMeta meta) {
        this(key, value, 1, meta);
    }

    @Override
    public String getKey() {
        return key;
    }

    @Override
    public String getValue() {
        return value;
    }

    @Override
    public DVConstraintType getDVConstraintType() {
        return DVConstraintType.ARRAY;
    }

    @Override
    public ArrayMeta getMeta() {
        return meta;
    }

    @Override
    public IDataHeader getReferHeader() {
        return null;
    }

    @Override
    protected String getFullLabel(String label, Map<?, ?> data){
        return label;
    }

}

package jexx.poi.header;

import jexx.convert.Convert;
import jexx.poi.header.label.LabelFunction;
import jexx.poi.header.label.LabelUnwrapFunction;
import jexx.poi.header.label.LabelWrapFunction;
import jexx.poi.meta.AbstractMeta;
import jexx.poi.meta.IMeta;
import jexx.poi.meta.node.INode;
import jexx.poi.meta.node.Node;
import jexx.poi.style.CellStyleSets;
import jexx.poi.style.IWrapCellStyle;
import jexx.util.Assert;
import jexx.util.CollectionUtil;
import jexx.util.StringUtil;

import java.lang.reflect.Array;
import java.util.*;

/**
 * abstract data header
 *
 * @author jeff
 */
public abstract class AbstractDataHeader<T extends AbstractDataHeader<T>> extends AbstractHeader implements IDataHeader {

    /**
     * valid data if include meta
     */
    protected boolean valid = true;

    protected boolean multiValue = false;

    protected Class<?> valueCollectionType;

    protected Class<?> valueElementType;
    protected boolean multiValueIsSet = false;

    /**
     * label加工方法
     */
    protected LabelWrapFunction labelWrapFunction;
    /**
     * label解加工方法
     */
    protected LabelUnwrapFunction labelUnwrapFunction;

    /**
     * 数据样式
     */
    private IWrapCellStyle dataCellStyle;

    public AbstractDataHeader(String key, String value) {
        this(key, value, 1);
    }

    public AbstractDataHeader(String key, String value, int columnCount) {
        this(key, value, columnCount, CellStyleSets.HEADER_CELL_STYLE, CellStyleSets.DATA_CELL_STYLE);
    }

    public AbstractDataHeader(String key, String value, int columnCount, IWrapCellStyle headerCellStyle, IWrapCellStyle dataCellStyle) {
        Objects.requireNonNull(key);
        this.key = key;
        this.value = value;
        this.columnCount = columnCount;
        this.headerCellStyle = headerCellStyle;
        this.dataCellStyle = dataCellStyle;
    }

    @SuppressWarnings("unchecked")
    private T _this(){
        return (T)this;
    }

    @Override
    public boolean isValid() {
        return this.valid;
    }

    public T setValid(boolean valid) {
        this.valid = valid;
        return _this();
    }

    @Override
    public boolean isMultiValue() {
        return multiValue;
    }

    public T setMultiValue(boolean multiValue) {
        this.multiValue = multiValue;
        return _this();
    }

    @Override
    public Class<?> getValueCollectionType() {
        return valueCollectionType;
    }

    /**
     * 多值类型必须是 数组, list 或者 set
     * @param valueCollectionType 多值的集合类型
     */
    protected void setValueCollectionType(Class<?> valueCollectionType) {
        if(valueCollectionType == null ||
                (!valueCollectionType.isArray() && !List.class.isAssignableFrom(valueCollectionType) && !Set.class.isAssignableFrom(valueCollectionType) )){
            throw new IllegalArgumentException("multiValueType must be an array, a list , or a set");
        }
        this.valueCollectionType = valueCollectionType;
    }

    @Override
    public Class<?> getValueElementType() {
        return valueElementType;
    }

    /**
     * 构建 多值, 数组类多值时 valueElementType 无效
     * @param valueCollectionType 多值集合类型
     * @param valueElementType 多值元素类型
     * @return self
     */
    public T withMultiValue(Class<?> valueCollectionType, Class<?> valueElementType){
        if(multiValueIsSet){
            throw new UnsupportedOperationException("Not allow to invoke this method repeatedly");
        }
        if(valueCollectionType == null ||
                (!valueCollectionType.isArray() && !List.class.isAssignableFrom(valueCollectionType) && !Set.class.isAssignableFrom(valueCollectionType) )){
            throw new IllegalArgumentException("valueCollectionType must be an array, a list , or a set");
        }
        if(!valueCollectionType.isArray() && valueElementType == null){
            throw new IllegalArgumentException("'valueElementType' cannot be null if collection");
        }

        IDataHeader referHeader = this;
        while((referHeader = referHeader.getReferHeader()) != null){
            if(referHeader.isMultiValue()){
                throw new UnsupportedOperationException("header("+referHeader.getKey()+") is multi-value's header, so failed");
            }
        }

        this.valueCollectionType = valueCollectionType;
        this.valueElementType = valueElementType;
        this.multiValue = true;
        this.multiValueIsSet = true;
        return _this();
    }

    /**
     * 构建 数组类多值
     * @param valueCollectionType 数组剋行
     * @return self
     */
    public T withMultiValue(Class<?> valueCollectionType){
        if(valueCollectionType == null || !valueCollectionType.isArray() ){
            throw new IllegalArgumentException("valueCollectionType must be an array");
        }
        return withMultiValue(valueCollectionType, null);
    }

    /**
     * 提供对包装label和实际label显示
     */
    public T label(LabelFunction labelFunction){
        wrapLabel(labelFunction);
        unwrapLabel(labelFunction);
        return _this();
    }

    /**
     * 包装label
     */
    public T wrapLabel(LabelWrapFunction labelWrapFunction){
        this.labelWrapFunction = labelWrapFunction;
        return _this();
    }

    /**
     * 转换实际label
     */
    public T unwrapLabel(LabelUnwrapFunction labelUnwrapFunction){
        this.labelUnwrapFunction = labelUnwrapFunction;
        return _this();
    }

    /**
     * 设置数据样式
     * @param dataCellStyle 数据样式
     */
    public T setDataCellStyle(IWrapCellStyle dataCellStyle) {
        this.dataCellStyle = dataCellStyle;
        return _this();
    }

    @Override
    public LabelWrapFunction getWrapLabelFunction() {
        return labelWrapFunction;
    }

    @Override
    public LabelUnwrapFunction getUnwrapLabelFunction() {
        return labelUnwrapFunction;
    }

    @Override
    public IWrapCellStyle getDataCellStyle() {
        return dataCellStyle;
    }

    @Override
    public Object getValueByLabel(Map<String, Object> labelMap){
        Object labelObj = labelMap.get(key);
        if(labelObj == null){
            return null;
        }
        String label = StringUtil.str(labelObj);

        Object value;
        Object v;
        if(isMultiValue()){
            Class<?> collectionType = getValueCollectionType();
            Assert.notNull(collectionType, "valueCollectionType not null");

            String[] labels = StringUtil.split(label, getLabelDelimiter());
            Object[] values = new Object[labels.length];
            for (int i = 0; i < labels.length; i++) {
                String fullLabel = getFullLabel(labels[i], labelMap);
                if(getMeta() != null){
                    INode node = getMeta().getNodeByFullLabel(fullLabel);
                    v = node != null ? node.getValue() : null;
                }
                else{
                    v = fullLabel;
                }
                if(collectionType.isArray()){
                    v = Convert.convert(collectionType.getComponentType(), v);
                }
                else {
                    Class<?> elementType = getValueElementType();
                    Assert.notNull(elementType, "valueElementType not null");
                    v = Convert.convert(elementType, v);
                }
                values[i] = v;
            }

            if(collectionType.isArray()){
                value = Array.newInstance(collectionType.getComponentType(), values.length);
                for (int i = 0; i < values.length; i++) {
                    Array.set(value, i, values[i]);
                }
            }
            else{
                Class<?> elementType = getValueElementType();
                Assert.notNull(elementType, "valueElementType not null");

                Collection<Object> list = CollectionUtil.createCollection(collectionType, elementType, 0);
                Collections.addAll(list, values);
                value = list;
            }

        }
        else{
            String fullLabel = getFullLabel(label, labelMap);
            if(getMeta() != null){
                INode node = getMeta().getNodeByFullLabel(fullLabel);
                value = node != null ? node.getValue() : null;
            }
            else{
                value = fullLabel;
            }
        }
        return value;
    }

    protected String getFullLabel(String label, Map<?, ?> data){
        if(label == null){
            return null;
        }

        List<Object> lvs = new ArrayList<>();
        lvs.add(label);

        IDataHeader next = getReferHeader();
        while(next != null){
            lvs.add(data.get(next.getKey()));
            next = next.getReferHeader();
        }

        int length = lvs.size();
        IMeta meta = getMeta();
        String metaName = meta != null ? meta.getName() : "";
        StringBuilder sb = new StringBuilder(metaName);
        for(int i = length - 1; i >= 0; i--){
            sb.append(AbstractMeta.SEPARATOR).append(lvs.get(i));
        }
        return sb.toString();
    }

}

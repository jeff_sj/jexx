package jexx.poi.header;

import jexx.poi.row.DataRow;
import jexx.poi.style.IWrapCellStyle;

/**
 * indicate that the header can custom value
 *
 * @author jeff
 * @since 2019/5/23
 */
public interface ICustomHeader extends IHeader {


    /**
     * 同一层的数据header名称
     */
    String getSameLevelHeaderName();

    /**
     * 获取同一级的数据header, 用于确定当前自定义header在哪一级;
     * 为空时默认最顶层
     */
    IDataHeader getSameLevelHeader();

    /**
     * 设置同一层的数据header
     */
    void setSameLevelHeader(IDataHeader sameLevelHeader);

    /**
     * the custom value of a header
     * @param row row info
     * @param itemIndex index; the index of row when batching to write data; default 0
     * @return the custom value
     */
    Object getCustomValue(DataRow row, int itemIndex);

    /**
     * 获取数据样式
     */
    IWrapCellStyle getDataCellStyle();

}

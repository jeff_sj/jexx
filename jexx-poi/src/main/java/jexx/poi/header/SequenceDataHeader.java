package jexx.poi.header;

import jexx.poi.row.DataRow;

/**
 * sequence header that create sequence number
 *
 * @author jeff
 */
public class SequenceDataHeader extends AbstractCustomHeader implements ICustomHeader {

    public SequenceDataHeader(String value, int columnCount, IDataHeader sameLevelHeader) {
        super(value, columnCount, sameLevelHeader);
    }

    public SequenceDataHeader(String value, int columnCount, String sameLevelHeaderName) {
        super(value, columnCount, sameLevelHeaderName);
    }

    public SequenceDataHeader(String value, IDataHeader sameLevelHeader) {
        this(value, 1, sameLevelHeader);
    }

    public SequenceDataHeader(String value, String sameLevelHeaderName) {
        this(value, 1, sameLevelHeaderName);
    }

    public SequenceDataHeader(String value) {
        this(value, 1, (IDataHeader)null);
    }

    @Override
    public Object getCustomValue(DataRow row, int itemIndex) {
        return itemIndex + 1;
    }

}

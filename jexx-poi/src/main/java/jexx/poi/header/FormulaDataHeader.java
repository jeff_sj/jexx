package jexx.poi.header;

import jexx.poi.cell.FormulaCellValue;
import jexx.poi.row.DataRow;
import jexx.poi.style.CellStyleSets;
import jexx.poi.style.IWrapCellStyle;

import java.util.Objects;

/**
 * the header that handle formula
 *
 * @author jeff
 */
public class FormulaDataHeader extends AbstractCustomHeader implements ICustomHeader {

    private FormulaFunction function;

    public FormulaDataHeader(String value, int columnCount, IDataHeader sameLevelHeader, FormulaFunction function,
                             IWrapCellStyle headerCellStyle, IWrapCellStyle dataCellStyle) {
        super(value, columnCount, sameLevelHeader);
        Objects.requireNonNull(function);
        this.function = function;
        setHeaderCellStyle(headerCellStyle);
        setDataCellStyle(dataCellStyle);
    }

    public FormulaDataHeader(String value, int columnCount, IDataHeader sameLevelHeader, FormulaFunction function) {
        this(value, columnCount, sameLevelHeader, function, CellStyleSets.HEADER_CELL_STYLE, CellStyleSets.DATA_CELL_STYLE);
    }

    public FormulaDataHeader(String value, int columnCount, String sameLevelHeaderName, FormulaFunction function,
                             IWrapCellStyle headerCellStyle, IWrapCellStyle dataCellStyle) {
        super(value, columnCount, sameLevelHeaderName, headerCellStyle, dataCellStyle);
        Objects.requireNonNull(function);
        this.function = function;
    }

    public FormulaDataHeader(String value, int columnCount, String sameLevelHeaderName, FormulaFunction function) {
        this(value, columnCount, sameLevelHeaderName, function, CellStyleSets.HEADER_CELL_STYLE, CellStyleSets.DATA_CELL_STYLE);
    }

    public FormulaDataHeader(String value, String sameLevelHeaderName, FormulaFunction function,
                             IWrapCellStyle headerCellStyle, IWrapCellStyle dataCellStyle) {
        this(value, 1, sameLevelHeaderName, function, headerCellStyle, dataCellStyle);
    }

    public FormulaDataHeader(String value, String sameLevelHeaderName, FormulaFunction function) {
        this(value, 1, sameLevelHeaderName, function, CellStyleSets.HEADER_CELL_STYLE, CellStyleSets.DATA_CELL_STYLE);
    }

    public FormulaDataHeader(String value, int columnCount, FormulaFunction function, IWrapCellStyle headerCellStyle, IWrapCellStyle dataCellStyle) {
        this(value, columnCount, (String)null, function, headerCellStyle, dataCellStyle);
    }

    public FormulaDataHeader(String value, FormulaFunction function, IWrapCellStyle headerCellStyle, IWrapCellStyle dataCellStyle) {
        this(value, 1, function, headerCellStyle, dataCellStyle);
    }

    public FormulaDataHeader(String value, FormulaFunction function) {
        this(value, 1, function, CellStyleSets.HEADER_CELL_STYLE, CellStyleSets.DATA_CELL_STYLE);
    }

    @Override
    public Object getCustomValue(DataRow row, int itemIndex) {
        String formula = function.formula(row, itemIndex, getStartColumnNum());
        return new FormulaCellValue(formula);
    }

    public interface FormulaFunction{
        /**
         * 计算公式
         * @param row 行数据
         * @param itemIndex 行数据在父对象中索引
         * @param columnNum 列号,从1开始
         * @return 公式
         */
        String formula(DataRow row, int itemIndex, int columnNum);
    }

}

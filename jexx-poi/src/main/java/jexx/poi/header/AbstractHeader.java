package jexx.poi.header;

import jexx.poi.style.IWrapCellStyle;

/**
 * @author jeff
 * @since 2019/6/19
 */
public abstract class AbstractHeader implements IHeader {

    /**
     * 列名称
     */
    protected String key;
    /**
     * 列显示名称
     */
    protected String value;

    /**
     * start column num, base 1
     */
    protected int startColumnNum;

    /**
     * end column num, base 1
     */
    protected int endColumnNum;

    /**
     * header columnCount
     */
    protected int columnCount;

    /**
     * 头样式
     */
    protected IWrapCellStyle headerCellStyle;

    @Override
    public int getStartColumnNum() {
        return startColumnNum;
    }

    @Override
    public void setStartColumnNum(int startColumnNum) {
        this.startColumnNum = startColumnNum;
    }

    @Override
    public int getEndColumnNum() {
        return endColumnNum;
    }

    @Override
    public void setEndColumnNum(int endColumnNum) {
        this.endColumnNum = endColumnNum;
    }

    @Override
    public int getColumnCount() {
        return columnCount;
    }

    @Override
    public String getKey() {
        return key;
    }

    @Override
    public String getValue() {
        return value;
    }

    @Override
    public IWrapCellStyle getHeaderCellStyle() {
        return headerCellStyle;
    }

    public void setHeaderCellStyle(IWrapCellStyle headerCellStyle) {
        this.headerCellStyle = headerCellStyle;
    }
}

package jexx.poi.header;

import jexx.poi.cell.IMergedCell;
import jexx.poi.cell.MergedCell;
import jexx.poi.header.factory.HeadersFactoryImpl;
import jexx.poi.header.row.HeaderRowContext;
import jexx.poi.meta.ArrayMeta;
import jexx.poi.meta.TreeMeta;
import jexx.poi.style.CellStyleSets;
import jexx.poi.style.IWrapCellStyle;
import jexx.poi.util.CellOperateUtil;
import jexx.poi.writer.RowTrigger;
import jexx.util.Assert;
import jexx.util.CollectionUtil;
import jexx.util.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * header set, it's useful to manager header
 *
 * @author jeff
 */
public class Headers {

    private static final Logger LOG = LoggerFactory.getLogger(Headers.class);

    public static final String INDEX_TAG = "[]";

    public static final String DEFAULT_HEADERS = "default";

    private final String name;

    /** 头开始索引,从1开始 */
    private final int startHeaderNum;

    /**
     * all headers at one level
     */
    private final List<IHeader> headers = new ArrayList<>();
    /**
     * all data headers
     */
    private final List<IDataHeader> dataHeaders = new ArrayList<>();
    /**
     * every data map has a key
     */
    private final Map<String, IDataHeader> dataHeaderMap = new HashMap<>(16);
    /**
     * data-header map with index key
     */
    private final Map<Integer, IDataHeader> dataHeaderIndexMap = new HashMap<>(16);

    /**
     * 自定义header集合
     */
    private final List<ICustomHeader> customHeaders = new ArrayList<>();

    protected Map<IHeader, List<RowTrigger>> rowTriggerMap = new LinkedHashMap<>(16);

    protected HeaderRowContext headerRowContext = new HeaderRowContext();

    /**
     * 默认header样式
     */
    private IWrapCellStyle defaultHeaderCellStyle = CellStyleSets.HEADER_CELL_STYLE;
    /**
     * 默认数据样式
     */
    private IWrapCellStyle defaultDataCellStyle = CellStyleSets.DATA_CELL_STYLE;

    private int currentHeaderNum = 1;
    private int maxHeaderNum = 1;
    /**
     * 添加header后是否调用过flush
     */
    protected boolean flushed;

    /**
     * 标识 header的key中引用了 bean的索引属性表示, 如 a[].b
     */
    private boolean index;

    public Headers(){
        this(DEFAULT_HEADERS);
    }

    public Headers(String name) {
        this(1, name);
    }

    public Headers(int startHeaderNum) {
        this(startHeaderNum, DEFAULT_HEADERS);
    }

    /**
     * 构造
     * @param startHeaderNum 头信息的开始索引,从1开始
     */
    public Headers(int startHeaderNum, String name) {
        Assert.isTrue(startHeaderNum > 0, "startHeaderNum={}", startHeaderNum);
        this.startHeaderNum = startHeaderNum;
        this.currentHeaderNum = startHeaderNum;
        this.maxHeaderNum = startHeaderNum;
        this.name = name;
    }

    /**
     * Headers工厂构建类
     */
    public static HeadersFactory newBuild(){
       return new HeadersFactoryImpl();
    }

    public String getName() {
        return name;
    }

    /**
     * skip some columns
     *
     * @param columnCount column's num
     * @return {@link Headers}
     */
    public Headers skipColumns(int columnCount){
        Assert.isTrue(columnCount > 0);
        this.currentHeaderNum += columnCount;
        return this;
    }

    /**
     * skip to one column, only go the bigger column
     *
     * @param columnNum column num, base 1
     * @return {@link Headers}
     */
    public Headers skipToOneColumn(int columnNum){
        Assert.isTrue(columnNum > currentHeaderNum);
        this.currentHeaderNum = columnNum;
        return this;
    }

    /**
     * @see Headers#skipToOneColumn(int)
     */
    public Headers skipToOneColumn(String columnNo){
        int columnNum = CellOperateUtil.toColumnNum(columnNo);
        return skipToOneColumn(columnNum);
    }

    public void addHeaders(Map<String, String> headerAlias){
        for(Map.Entry<String, String> entry : headerAlias.entrySet()){
            addHeader(new DefaultDataHeader(entry.getKey(),entry.getValue()));
        }
    }

    /**
     * add headers;
     * It may update currentHeaderNum if the header's startColumnNum is equal to 0.
     *
     * @param header header
     */
    private void doAddHeader(IHeader header){
        if(CollectionUtil.isNotEmpty(headers)){
            currentHeaderNum++;
        }

        Assert.isTrue(header.getColumnCount() > 0);
        if(header.getStartColumnNum() <= 0){
            handleColumn(header, currentHeaderNum);
        }
        currentHeaderNum = header.getEndColumnNum();
        Assert.isTrue(header.getStartColumnNum() > 0 && header.getEndColumnNum() >= header.getStartColumnNum());

        headers.add(header);
        if(maxHeaderNum < currentHeaderNum){
            maxHeaderNum = currentHeaderNum;
        }

        flushed = false;
    }

    /**
     * 添加数据header
     */
    public IDataHeader addHeader(IDataHeader header){
        doAddHeader(header);
        doAddDataHeader(header);
        return header;
    }

    private void doAddDataHeader(IDataHeader header){
        if (!index) {
            this.index = header.getKey().contains("[") || header.getKey().contains(".");
        }

        this.dataHeaders.add(header);
        this.dataHeaderMap.put(header.getKey(), header);
        this.dataHeaderIndexMap.put(header.getStartColumnNum(), header);

//        int index = header.getKey().lastIndexOf("[]");
//        String parentPath = index == -1 ? "" : header.getKey().substring(0, index);
//        List<IDataHeader> list = pathDataHeadersMap.computeIfAbsent(parentPath, s->new ArrayList<>());
//        list.add(header);

        //header row context
        headerRowContext.addHeader(header);
    }

    /**
     * 添加自定义header
     */
    public ICustomHeader addHeader(ICustomHeader header){
        doAddHeader(header);
        doAddCustomHeader(header);
        return header;
    }

    private void doAddCustomHeader(ICustomHeader header){
        customHeaders.add(header);
    }

    /**
     * 添加分组header
     */
    public IGroupHeader addHeader(IGroupHeader header){
        doAddHeader(header);
        doAddGroupHeader(header);
        return header;
    }

    private void doAddGroupHeader(IGroupHeader header){
        List<IHeader> children = header.getChildren();
        if(CollectionUtil.isNotEmpty(children)){
            for (IHeader h : children){
                if(h instanceof IDataHeader){
                    doAddDataHeader((IDataHeader) h);
                }
                else if(h instanceof IGroupHeader){
                    doAddGroupHeader((IGroupHeader) h);
                }
                else if(h instanceof ICustomHeader){
                    doAddCustomHeader((ICustomHeader) h);
                }
                else{
                    throw new UnsupportedOperationException(StringUtil.format("no support header {}", h));
                }
            }
        }
    }

    /**
     * 处理header的开始列和结束列
     *
     * @param header header
     * @param startColumnNum 开始列
     */
    private void handleColumn(IHeader header, int startColumnNum){
        header.setStartColumnNum(startColumnNum);
        header.setEndColumnNum(startColumnNum + header.getColumnCount() - 1);
        if(header instanceof IGroupHeader){
            List<IHeader> children = ((IGroupHeader) header).getChildren();
            int start = startColumnNum;
            for(IHeader child : children){
                handleColumn(child, start);
                start = child.getEndColumnNum() + 1;
            }
        }
    }

    /**
     * 添加默认头
     * @param label label
     * @param value value
     * @param headerCellStyle 头样式
     * @param dataCellStyle 数据样式
     */
    public IDataHeader addHeader(String label, String value, IWrapCellStyle headerCellStyle, IWrapCellStyle dataCellStyle){
        DefaultDataHeader header = new DefaultDataHeader(label, value);
        header.setHeaderCellStyle(headerCellStyle);
        header.setDataCellStyle(dataCellStyle);
        return addHeader(header);
    }

    /**
     * @see Headers#addHeader(String, String, IWrapCellStyle, IWrapCellStyle)
     */
    public IDataHeader addHeader(String label, String value){
        return addHeader(label, value, defaultHeaderCellStyle, defaultDataCellStyle);
    }

    /**
     * 添加array头信息
     * @param label label
     * @param value value
     * @param meta meta
     * @param headerCellStyle 头样式
     * @param dataCellStyle 数据样式
     */
    public IDataHeader addArrayHeader(String label, String value, ArrayMeta meta, IWrapCellStyle headerCellStyle, IWrapCellStyle dataCellStyle){
        ArrayDataHeader header = new ArrayDataHeader(label, value, meta);
        header.setHeaderCellStyle(headerCellStyle);
        header.setDataCellStyle(dataCellStyle);
        return addHeader(header);
    }

    /**
     * @see Headers#addArrayHeader(String, String, ArrayMeta, IWrapCellStyle, IWrapCellStyle)
     */
    public IDataHeader addArrayHeader(String label, String value, ArrayMeta meta){
        return addArrayHeader(label, value, meta, defaultHeaderCellStyle, defaultDataCellStyle);
    }

    /**
     * 添加tree头信息
     * @param label label
     * @param value value
     * @param meta meta
     * @param referHeader 引用头信息
     * @param headerCellStyle 头样式
     * @param dataCellStyle 数据样式
     */
    public IDataHeader addTreeHeader(String label, String value, TreeMeta meta, IDataHeader referHeader,
                                 IWrapCellStyle headerCellStyle, IWrapCellStyle dataCellStyle){
        TreeDataHeader header = new TreeDataHeader(label, value, meta, referHeader);
        header.setHeaderCellStyle(headerCellStyle);
        header.setDataCellStyle(dataCellStyle);
        return addHeader(header);
    }

    /**
     * 添加tree头信息
     * @param label label
     * @param value value
     * @param meta meta
     * @param referHeaderName 引用头信息名称
     * @param headerCellStyle 头样式
     * @param dataCellStyle 数据样式
     * @return 数据header
     */
    public IDataHeader addTreeHeader(String label, String value, TreeMeta meta, String referHeaderName,
                                     IWrapCellStyle headerCellStyle, IWrapCellStyle dataCellStyle){
        IDataHeader referHeader = dataHeaderMap.get(referHeaderName);
        Assert.notNull(referHeader, "refer header[{}] is null", referHeaderName);
        return addTreeHeader(label, value, meta, referHeader, headerCellStyle, dataCellStyle);
    }

    /**
     * 添加第一层header信息
     * @param label label
     * @param value value
     * @param meta meta
     * @param headerCellStyle 头样式
     * @param dataCellStyle 数据样式
     */
    public IDataHeader addTreeHeader(String label, String value, TreeMeta meta,
                                     IWrapCellStyle headerCellStyle, IWrapCellStyle dataCellStyle){
        return addTreeHeader(label, value, meta, (IDataHeader)null, headerCellStyle, dataCellStyle);
    }

    /**
     * @see Headers#addTreeHeader(String, String, TreeMeta, IDataHeader, IWrapCellStyle, IWrapCellStyle)
     */
    public IDataHeader addTreeHeader(String label, String value, TreeMeta meta, IDataHeader referHeader){
        return addTreeHeader(label, value, meta, referHeader, defaultHeaderCellStyle, defaultDataCellStyle);
    }

    /**
     * @see Headers#addTreeHeader(String, String, TreeMeta, String)
     */
    public IDataHeader addTreeHeader(String label, String value, TreeMeta meta, String referHeaderName){
        IDataHeader referHeader = dataHeaderMap.get(referHeaderName);
        Assert.notNull(referHeader, "refer header[{}] is null", referHeaderName);
        return addTreeHeader(label, value, meta, referHeader, defaultHeaderCellStyle, defaultDataCellStyle);
    }

    public IDataHeader addTreeHeader(String label, String value, TreeMeta meta){
        return addTreeHeader(label, value, meta, (IDataHeader)null, defaultHeaderCellStyle, defaultDataCellStyle);
    }

    /**
     * 添加序列header
     *
     * @param value value
     */
    public ICustomHeader addSequenceHeader(String value){
        return addSequenceHeader(value, null);
    }

    public ICustomHeader addSequenceHeader(String value, IDataHeader sameLevelHeader){
        return addHeader(new SequenceDataHeader(value, sameLevelHeader));
    }


    //------------------------------------- 刷新，验证

    /**
     *
     * 刷新headers
     * <pre>
     *     刷新header引用: 一些header在引用其他header时,使用的是名字引用，而非对象; 因此需要帮助重新设置对象,确保对象存在
     * </pre>
     */
    public void flush(){
        //验证自定义header
        for(ICustomHeader customHeader : customHeaders){
            if(StringUtil.isNotEmpty(customHeader.getSameLevelHeaderName()) && customHeader.getSameLevelHeader() == null){
                IDataHeader sameLevelHeader = dataHeaderMap.get(customHeader.getSameLevelHeaderName());
                Assert.notNull(sameLevelHeader, "can't find header[{}]", customHeader.getSameLevelHeaderName());
                customHeader.setSameLevelHeader(sameLevelHeader);
            }
        }

        flushed = true;
    }

    /**
     * get one-level header
     *
     * @return one-level headers
     */
    public List<IHeader> getHeaders(){
        return headers;
    }

    /**
     * 获取自定义header
     */
    public List<ICustomHeader> getCustomHeaders(){
        return customHeaders;
    }

    /**
     * get data headers
     *
     * @return all data headers
     */
    public List<IDataHeader> getDataHeaders(){
        return this.dataHeaders;
    }

    public int getStartHeaderNum() {
        return startHeaderNum;
    }

    /**
     * 获取最大header列号,从1开始
     */
    public int getMaxHeaderNum(){
        return maxHeaderNum;
    }

    /**
     * get data-header by column num
     *
     * @param columnNum column num, base 1
     * @return 头信息
     */
    public IDataHeader getDataHeaderByColumnNum(int columnNum){
        return dataHeaderIndexMap.get(columnNum);
    }

    /**
     * get data header by label
     * @param label header label
     */
    public IDataHeader getDataHeaderByLabel(String label){
        return dataHeaderMap.get(label);
    }

    /**
     * 列号是否匹配于当前列名称
     * @param columnNum 列号
     * @param labels 列名称集合
     * @return 是否匹配
     */
    public boolean matchColumnNum(int columnNum, String ...labels){
        for(String label : labels){
            IHeader header = getDataHeaderByLabel(label);
            if(header == null){
                continue;
            }
            if(header.getStartColumnNum() == columnNum){
                return true;
            }
        }
        return false;
    }

    /**
     * 有索引属性
     */
    public boolean isIndex() {
        return index;
    }

    /**
     * create cells of the header
     *
     * @param startRowNum start row num
     * @return header cells
     */
    public List<IMergedCell> createHeaderCells(int startRowNum){
        List<IMergedCell> cells = new ArrayList<>();
        List<HeaderMergeCell> dataCells = new ArrayList<>();
        int maxEndRowNum = doCreateHeaderCells(headers, startRowNum, cells, dataCells);
        for(HeaderMergeCell cell : dataCells){
            if(cell.getLastRowNum() < maxEndRowNum){
                cell.setLastRowNum(maxEndRowNum);
            }
        }
        return cells;
    }

    private int doCreateHeaderCells(List<IHeader> headers, int startRowNum, List<IMergedCell> cells, List<HeaderMergeCell> dataCells){
        int maxEndRowNum = startRowNum;
        if(CollectionUtil.isNotEmpty(headers)) {
            for (IHeader header : headers) {
                HeaderMergeCell cell = new HeaderMergeCell(startRowNum, header.getStartColumnNum(), startRowNum, header.getEndColumnNum(),
                        header.getValue(), header.getHeaderCellStyle());
                cells.add(cell);

                if (header instanceof GroupHeader) {
                    GroupHeader groupHeader = (GroupHeader) header;
                    int lastRowNum = doCreateHeaderCells(groupHeader.getChildren(), startRowNum+1, cells, dataCells);
                    maxEndRowNum = Math.max(maxEndRowNum, lastRowNum);
                } else if (header instanceof IDataHeader) {
                    dataCells.add(cell);
                }
                else if (header instanceof ICustomHeader) {
                    dataCells.add(cell);
                }
            }
        }
        return maxEndRowNum;
    }

    public void setDefaultHeaderCellStyle(IWrapCellStyle defaultHeaderCellStyle) {
        this.defaultHeaderCellStyle = defaultHeaderCellStyle;
    }

    public void setDefaultDataCellStyle(IWrapCellStyle defaultDataCellStyle) {
        this.defaultDataCellStyle = defaultDataCellStyle;
    }

    public void addRowTrigger(IDataHeader header, RowTrigger trigger){
        List<RowTrigger> rowTriggers = rowTriggerMap.computeIfAbsent(header, s->new ArrayList<>());
        rowTriggers.add(trigger);
    }

    public void addRowTrigger(String headerName, RowTrigger trigger){
        IDataHeader header = dataHeaderMap.get(headerName);
        Assert.notNull(header, "header[{}] not exist", headerName);
        addRowTrigger(header, trigger);
    }

    public List<RowTrigger> getRowTriggers(IHeader header){
        return rowTriggerMap.get(header);
    }

    public List<RowTrigger> getRowTriggers(int headerColumnNum){
        IDataHeader header = dataHeaderIndexMap.get(headerColumnNum);
        if(header != null){
            return getRowTriggers(header);
        }
        return null;
    }

    public boolean hasRowTrigger(){
        return CollectionUtil.isNotEmpty(rowTriggerMap);
    }

    public HeaderRowContext getHeaderRowContext() {
        return headerRowContext;
    }

    public boolean isFlushed() {
        return flushed;
    }

    private static class HeaderMergeCell extends MergedCell{

        public HeaderMergeCell(int firstRowNum, int firstColumnNum, int lastRowNum, int lastColumnNum, Object value, IWrapCellStyle cellStyle) {
            super(firstRowNum, firstColumnNum, lastRowNum, lastColumnNum, value, cellStyle);
        }

        public void setLastRowNum(int lastRowNum){
            this.lastRowNum = lastRowNum;
        }

    }

}

package jexx.poi.header;

import jexx.poi.style.CellStyleSets;
import jexx.poi.style.IWrapCellStyle;

import java.util.ArrayList;
import java.util.List;

/**
 * group header
 *
 * @author jeff
 * @since 2019/5/27
 * @since 1.0.6.1
 */
public class GroupHeader implements IGroupHeader {

    protected String value;

    /**
     * start column num, base 1
     */
    protected int startColumnNum;

    /**
     * end column num, base 1
     */
    protected int endColumnNum;

    protected IWrapCellStyle headerCellStyle;

    private List<IHeader> children = new ArrayList<>();

    public GroupHeader(String value, IWrapCellStyle headerCellStyle) {
        this.value = value;
        this.headerCellStyle = headerCellStyle;
    }

    public GroupHeader(String value) {
        this(value, CellStyleSets.HEADER_CELL_STYLE);
    }

    public void addHeader(IHeader header){
        this.children.add(header);
    }

    @Override
    public List<IHeader> getChildren() {
        return children;
    }

    @Override
    public String getKey() {
        return null;
    }

    @Override
    public String getValue() {
        return value;
    }

    @Override
    public int getStartColumnNum() {
        return startColumnNum;
    }

    @Override
    public void setStartColumnNum(int startColumnNum) {
        this.startColumnNum = startColumnNum;
    }

    @Override
    public int getEndColumnNum() {
        return endColumnNum;
    }

    @Override
    public void setEndColumnNum(int endColumnNum) {
        this.endColumnNum = endColumnNum;
    }

    @Override
    public int getColumnCount() {
        return children.stream().mapToInt(IHeader::getColumnCount).sum();
    }

    @Override
    public IWrapCellStyle getHeaderCellStyle() {
        return headerCellStyle;
    }
}

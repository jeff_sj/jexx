package jexx.poi.header.impl;

import jexx.poi.header.HeaderModifyField;

/**
 * 可修改字段默认实现类
 * @author jeff
 * @since 2019/12/3
 */
public class HeaderModifyFieldImpl implements HeaderModifyField {

    private String value;

    private int groupOrder;

    private int order;

    public HeaderModifyFieldImpl(String value, int groupOrder, int order) {
        this.value = value;
        this.groupOrder = groupOrder;
        this.order = order;
    }

    @Override
    public String getValue() {
        return value;
    }

    @Override
    public int getGroupOrder() {
        return groupOrder;
    }

    @Override
    public int getOrder() {
        return order;
    }
}

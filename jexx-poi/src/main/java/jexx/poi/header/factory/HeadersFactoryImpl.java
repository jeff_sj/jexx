package jexx.poi.header.factory;

import jexx.convert.Convert;
import jexx.poi.constant.EmbeddedDataStyleName;
import jexx.poi.header.*;
import jexx.poi.header.annotation.*;
import jexx.poi.meta.ArrayMeta;
import jexx.poi.meta.IMeta;
import jexx.poi.meta.Metas;
import jexx.poi.meta.TreeMeta;
import jexx.poi.meta.node.INode;
import jexx.poi.meta.node.Node;
import jexx.poi.style.CellStyleSets;
import jexx.poi.style.IWrapCellStyle;
import jexx.util.*;

import java.lang.reflect.Field;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author jeff
 * @since 2019/6/14
 */
public class HeadersFactoryImpl implements HeadersFactory {

    /**
     * 内置样式前缀
     */
    private static final String INTERNAL_CELL_STYLE_PREFIX = "_";

    /**
     * headers名称
     */
    private String name;
    /**
     * 列选择模式
     */
    private HeaderColumnSelectMode selectMode = HeaderColumnSelectMode.ALL;
    /**
     * 分组属性，用于区分不同的字段分组
     */
    private Class<?>[] groups;
    /**
     * 样式集合
     */
    private Map<String, IWrapCellStyle> cellStyleMap;
    /**
     * 默认头样式
     */
    private IWrapCellStyle defaultHeaderCellStyle = CellStyleSets.HEADER_CELL_STYLE;
    /**
     * 默认数据样式
     */
    private IWrapCellStyle defaultDataCellStyle = CellStyleSets.DATA_CELL_STYLE;
    /**
     * meta信息
     */
    private Metas metas;
    /**
     * 忽视的列集合
     */
    private final Set<String> ignoreFields = new HashSet<>();
    /**
     * 可修改字段属性集合
     */
    private final Map<String, HeaderModifyField> canModifyFields = new HashMap<>(16);
    /**
     * 国际化
     */
    private  Map<String, String> i18nMap;
    /**
     * 国际化
     */
    private ResourceBundle resourceBundle;

    private final List<IDataHeader> replaceHeaders = new ArrayList<>();
    /**
     * 操作
     */
    private final List<Operation> operations = new ArrayList<>();

    public HeadersFactoryImpl() {
        initCellStyle();
    }

    @Override
    public HeadersFactory withCellStyle(String name, IWrapCellStyle cellStyle) {
        Assert.isTrue(StringUtil.isNotEmpty(name), "name is not empty!");
        Assert.isTrue(!name.startsWith(INTERNAL_CELL_STYLE_PREFIX), "name cannot start with '_' ");
        cellStyleMap.put(name, cellStyle);
        return this;
    }

    /**
     * 初始化内部默认样式
     */
    private void initCellStyle(){
        if(this.cellStyleMap == null){
            Map<String, IWrapCellStyle> map = new HashMap<>(16);
            EmbeddedDataStyleName.wrapEmbeddedDataStyle(map);
            this.cellStyleMap = map;
        }
    }

    @Override
    public HeadersFactory withDefaultHeaderCellStyle(IWrapCellStyle cellStyle) {
        this.defaultHeaderCellStyle = cellStyle;
        return this;
    }

    @Override
    public HeadersFactory withDefaultDataCellStyle(IWrapCellStyle cellStyle) {
        this.defaultDataCellStyle = cellStyle;
        return this;
    }

    @Override
    public HeadersFactory withMetas(Metas metas) {
        this.metas = metas;
        return this;
    }

    @Override
    public HeadersFactory select(HeaderColumnSelectMode mode) {
        this.selectMode = mode;
        return this;
    }

    @Override
    public HeadersFactory group(Class<?>... groups) {
        this.groups = groups;
        return this;
    }

    @Override
    public HeadersFactory ignoreField(String... fields) {
        Collections.addAll(ignoreFields, fields);
        return this;
    }

    @Override
    public HeadersFactory ignoreField(Collection<String> fields) {
        ignoreFields.addAll(fields);
        return this;
    }

    @Override
    public HeadersFactory modify(String name, HeaderModifyField field) {
        canModifyFields.put(name, field);
        return this;
    }

    @Override
    public HeadersFactory i18n(Map<String, String> i18nMap) {
        this.i18nMap = i18nMap;
        return this;
    }

    @Override
    public HeadersFactory i18n(ResourceBundle resourceBundle) {
        this.resourceBundle = resourceBundle;
        return this;
    }

    /**
     * 获取value对应的值,有则替换,无则默认
     *
     * @param value 默认显示值
     * @return 显示值
     */
    protected String convertValue(final String value){
        if(value == null){
            return null;
        }
        String newValue = value;
        if(i18nMap != null){
            newValue = i18nMap.get(newValue);
        }

        if(newValue == null && resourceBundle != null){
            newValue = resourceBundle.getString(value);
        }
        return newValue == null ? value : newValue;
    }

    @Override
    public HeadersFactory replace(IDataHeader header) {
        replaceHeaders.add(header);
        return this;
    }

    @Override
    public HeadersFactory addHeaderAfter(String headerName, ICustomHeader header) {
        Operation operation = new Operation();
        operation.headerName = headerName;
        operation.type = Operation.AFTER;
        operation.header = header;
        operations.add(operation);
        return this;
    }

    @Override
    public HeadersFactory addHeaderBefore(String headerName, ICustomHeader header) {
        Operation operation = new Operation();
        operation.headerName = headerName;
        operation.type = Operation.BEFORE;
        operation.header = header;
        operations.add(operation);
        return this;
    }

    @Override
    public Headers build(Class<?> clazz) {
        String name;
        if(HeaderColumnSelectMode.ALL == selectMode){
            name = Headers.DEFAULT_HEADERS;
        }
        else{
            HeaderTable table = ReflectUtil.getAnnotationOfClass(clazz, HeaderTable.class);
            Assert.notNull(table, "Class[{}] no support to build header", clazz);
            name = table.name();
        }

        List<HeaderFieldImpl> headerFields = new ArrayList<>();
        doScanCollection("", clazz, headerFields);

        Map<String, IDataHeader> headerMap = new HashMap<>(16);
        doRecursionHeader(new ArrayList<>(headerFields), headerMap);
        //replace
        if(CollectionUtil.isNotEmpty(replaceHeaders)){
            replaceHeaders.forEach(s->{
                if(headerMap.get(s.getKey()) != null){
                    headerMap.put(s.getKey(), s);
                }
            });
        }

        headerFields = headerFields.stream().sorted(Comparator.comparingInt(HeaderFieldImpl::getGroupOrder).thenComparing(HeaderFieldImpl::getOrder))
                .collect(Collectors.toList());
        Map<String, List<Operation>> operationGroupMap =  operations.stream().collect(Collectors.groupingBy(s -> s.headerName));

        Headers headers = new Headers(name);
        headers.setDefaultHeaderCellStyle(defaultHeaderCellStyle);
        headers.setDefaultDataCellStyle(defaultDataCellStyle);

        for(HeaderFieldImpl field : headerFields){
            IDataHeader dataHeader = headerMap.get(field.getFieldName());
            Assert.notNull(dataHeader);

            List<Operation> operations = operationGroupMap.remove(field.getFieldName());
            if(CollectionUtil.isEmpty(operations)){
                headers.addHeader(dataHeader);
            }
            else{
                for(Operation operation : operations){
                    if(Operation.BEFORE == operation.type){
                        headers.addHeader(operation.header);
                    }
                }
                headers.addHeader(dataHeader);
                for(Operation operation : operations){
                    if(Operation.AFTER == operation.type){
                        headers.addHeader(operation.header);
                    }
                }
            }
        }
        Assert.isEmpty(operationGroupMap, "can't handle this custom header, {}", operationGroupMap.keySet());

        headers.flush();

        return headers;
    }

    private void doScanCollection(String prefix, Class<?> clazz, List<HeaderFieldImpl> headerFields){
        Field[] fields = ReflectUtil.getFields(clazz);
        Assert.notEmpty(fields, "Class {} has no field to build headers!", clazz);

        for(Field field : fields) {
            String fieldName = prefix.concat(field.getName());
            if (ignoreFields.contains(fieldName)) {
                continue;
            }

            HeaderIgnore headerIgnore = field.getAnnotation(HeaderIgnore.class);

            HeaderColumn headerColumn = field.getAnnotation(HeaderColumn.class);
            HeaderCollection headerCollection = field.getAnnotation(HeaderCollection.class);
            Assert.isTrue(headerColumn == null || headerCollection == null, "A field can't  support HeaderColumn and HeaderCollection at same time");

            if (HeaderColumnSelectMode.ALL == selectMode) {
                HeaderFieldImpl headerField = new HeaderFieldImpl(fieldName, fieldName);
                modifyField(headerField);
                headerFields.add(headerField);
            } else if (HeaderColumnSelectMode.ALL_WITH_TAG == selectMode || HeaderColumnSelectMode.ONLY_TAG == selectMode) {
                if (headerIgnore != null) {
                    continue;
                }
                if (headerCollection != null) {
                    Assert.isTrue(field.getType().isArray() || List.class == field.getType() || Set.class == field.getType(),
                            "field [{}] is not array or collection", fieldName);
                    Class<?> componentType;
                    if(field.getType().isArray()){
                        componentType = field.getType().getComponentType();
                    }
                    else{
                        componentType = ClassUtil.getComponentType(field.getGenericType());
                    }
                    Assert.notNull(componentType, "check field [{}] because HeaderCollection", fieldName);

                    doScanCollection(fieldName.concat("[]."), componentType, headerFields);
                } else if (headerColumn != null) {
                    if (!checkGroup(headerColumn)) {
                        continue;
                    }

                    ArrayMeta meta = parseHeaderMeta(field);
                    boolean metaEnabled = meta != null && this.metas != null;
                    if(metaEnabled){
                        this.metas.addMeta(meta);
                    }

                    HeaderFieldImpl headerField = new HeaderFieldImpl();
                    headerField.setFieldName(headerColumn.list() ? fieldName.concat("[]") : fieldName);
                    headerField.setValue(headerColumn.value());
                    headerField.setGroupOrder(headerColumn.groupOrder());
                    headerField.setOrder(headerColumn.order());
                    headerField.setHeaderCellStyle(headerColumn.headerCellStyle());
                    headerField.setDataCellStyle(headerColumn.dataCellStyle());
                    headerField.setReferKey(headerColumn.referKey());
                    headerField.setMeta(metaEnabled ? meta.getName() : headerColumn.meta());
                    headerField.setValid(headerColumn.valid());
                    modifyField(headerField);

                    boolean multiValue = headerColumn.multiValue();
                    headerField.setMultiValue(multiValue);
                    if(multiValue){
                        Class<?> fieldType = field.getType();
                        Assert.isTrue(fieldType.isArray() || List.class.isAssignableFrom(fieldType) || Set.class.isAssignableFrom(fieldType),
                                "field [{}] is not array or collection", fieldName);
                        headerField.setMultiValueCollectionType(fieldType);
                        Class<?> componentType = fieldType.isArray() ? fieldType.getComponentType() : ClassUtil.getComponentType(field.getGenericType());
                        headerField.setMultiValueElementType(componentType);
                    }
                    headerFields.add(headerField);
                } else if (HeaderColumnSelectMode.ALL_WITH_TAG == selectMode) {
                    HeaderFieldImpl headerField = new HeaderFieldImpl(fieldName, fieldName);
                    modifyField(headerField);
                    headerFields.add(headerField);
                }
            }
        }
        Assert.notEmpty(headerFields, "No field to build headers!");
    }

    /**
     * 解析 HeaderMeta
     */
    private ArrayMeta parseHeaderMeta(Field field){
        HeaderMeta headerMeta = field.getAnnotation(HeaderMeta.class);
        if(headerMeta == null){
            return null;
        }
        String[] strValues = headerMeta.values();
        String[] labels = headerMeta.labels();
        if(ArrayUtil.isNotEmpty(labels) && labels.length !=  strValues.length){
            throw new IllegalArgumentException("labels' length is not equal to strValues");
        }

        List<INode> nodes = new ArrayList<>();
        for (int i = 0; i < strValues.length; i++) {
            Object value = String.class == headerMeta.valueType() ? strValues[i] : Convert.convert(headerMeta.valueType(), strValues[i]);
            Object label = value;
            if(ArrayUtil.isNotEmpty(labels)){
                label = labels[i];
            }
            nodes.add(new Node(value, label));
        }
        return new ArrayMeta(headerMeta.name(), nodes);
    }


    private void doRecursionHeader(List<HeaderFieldImpl> headerFieldImpls, Map<String, IDataHeader> dataHeaderMap){
        int size = headerFieldImpls.size();

        Iterator<HeaderFieldImpl> headerFieldIterator = headerFieldImpls.iterator();
        while(headerFieldIterator.hasNext()){
            HeaderFieldImpl headerFieldImpl = headerFieldIterator.next();

            String referKey = headerFieldImpl.getReferKey();
            IDataHeader referHeader = null;
            if(StringUtil.isNotEmpty(referKey)){
                referHeader = dataHeaderMap.get(referKey);
                if(referHeader == null){
                    continue;
                }
            }

            IWrapCellStyle headerCellStyle = this.defaultHeaderCellStyle;
            IWrapCellStyle dataCellStyle = this.defaultDataCellStyle;
            if(cellStyleMap != null){
                //header
                if(!StringPool.EMPTY.equals(headerFieldImpl.getHeaderCellStyle())){
                    headerCellStyle = cellStyleMap.get(headerFieldImpl.getHeaderCellStyle());
                }
                //data
                if(!StringPool.EMPTY.equals(headerFieldImpl.getDataCellStyle())){
                    dataCellStyle = cellStyleMap.get(headerFieldImpl.getDataCellStyle());
                }
            }

            IMeta meta = null;
            if(metas != null && StringUtil.isNotEmpty(headerFieldImpl.getMeta())){
                meta = metas.getMeta(headerFieldImpl.getMeta());
            }

            AbstractDataHeader<?> header;
            String value = convertValue(headerFieldImpl.getValue());
            if(meta == null){
                header = new DefaultDataHeader(headerFieldImpl.getFieldName(), value);
            }
            else if(meta instanceof ArrayMeta){
                header = new ArrayDataHeader(headerFieldImpl.getFieldName(),value, (ArrayMeta)meta);
            }
            else if(meta instanceof TreeMeta){
                header = new TreeDataHeader(headerFieldImpl.getFieldName(), value, (TreeMeta)meta, referHeader);
            }
            else{
                throw new UnsupportedOperationException(StringUtil.format("cannot handle this meta \"{}\"", meta));
            }
            header.setValid(headerFieldImpl.isValid());
            header.setHeaderCellStyle(headerCellStyle);
            header.setDataCellStyle(dataCellStyle);
            if(headerFieldImpl.isMultiValue()){
                header.withMultiValue(headerFieldImpl.getMultiValueCollectionType(), headerFieldImpl.getMultiValueElementType());
            }

            dataHeaderMap.put(headerFieldImpl.getFieldName(), header);
            headerFieldIterator.remove();
        }

        if(headerFieldImpls.size() == size){
            String referKeys = StringUtil.join(headerFieldImpls, ",", HeaderFieldImpl::getReferKey);
            throw new IllegalArgumentException(StringUtil.format("Please check the header column's referKey, referKeys=[{}] ", referKeys));
        }

        if(headerFieldImpls.size() > 0){
            doRecursionHeader(headerFieldImpls, dataHeaderMap);
        }
    }

    private boolean checkGroup(HeaderColumn headerColumn){
        if(headerColumn == null || ArrayUtil.isEmpty(headerColumn.group()) || ArrayUtil.isEmpty(groups)){
            return true;
        }
        for(Class<?> c : headerColumn.group()){
            for(Class<?> b : groups){
                if(c == b){
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * 修改可修改的字段属性
     * @param field 待修改的字段
     */
    private void modifyField(HeaderFieldImpl field){
        HeaderModifyField modifyField = canModifyFields.get(field.getFieldName());
        if(modifyField != null){
            field.setGroupOrder(modifyField.getGroupOrder());
            field.setOrder(modifyField.getOrder());
        }
    }


    private static class Operation{

        public static final int BEFORE = 1;

        public static final int AFTER = 2;

        public String headerName;

        public int type;

        public ICustomHeader header;

    }

}

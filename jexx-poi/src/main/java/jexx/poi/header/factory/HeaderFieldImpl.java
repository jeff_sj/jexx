package jexx.poi.header.factory;

import jexx.poi.header.HeaderField;

import java.util.List;

/**
 * @author jeff
 * @since 2019/6/14
 */
class HeaderFieldImpl implements HeaderField {

    /**
     * 字段名称
     */
    private String fieldName;

    /**
     * header名称
     */
    private String value;

    /**
     * 组排序，越小越靠前，默认为0
     */
    private int groupOrder;

    /**
     * 组排序下的排序，越小越靠前, 默认为0
     */
    private int order;

    /**
     * 头样式名称
     */
    private String headerCellStyle;

    /**
     * 数据样式名称
     */
    private String dataCellStyle;

    /**
     * 引用列
     */
    private String referKey;

    /**
     * meta名称
     */
    private String meta;

    /**
     * if valid data
     */
    private boolean valid;

    private boolean multiValue;

    private Class<?> multiValueCollectionType;

    private Class<?> multiValueElementType;

    private List<HeaderFieldImpl> children;

    public HeaderFieldImpl() {
    }

    public HeaderFieldImpl(String fieldName, String value) {
        this.fieldName = fieldName;
        this.value = value;
    }

    @Override
    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    @Override
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public int getGroupOrder() {
        return groupOrder;
    }

    public void setGroupOrder(int groupOrder) {
        this.groupOrder = groupOrder;
    }

    @Override
    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    @Override
    public String getHeaderCellStyle() {
        return headerCellStyle;
    }

    public void setHeaderCellStyle(String headerCellStyle) {
        this.headerCellStyle = headerCellStyle;
    }

    @Override
    public String getDataCellStyle() {
        return dataCellStyle;
    }

    public void setDataCellStyle(String dataCellStyle) {
        this.dataCellStyle = dataCellStyle;
    }

    @Override
    public String getReferKey() {
        return referKey;
    }

    public void setReferKey(String referKey) {
        this.referKey = referKey;
    }

    @Override
    public String getMeta() {
        return meta;
    }

    public void setMeta(String meta) {
        this.meta = meta;
    }

    @Override
    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    public List<HeaderFieldImpl> getChildren() {
        return children;
    }

    public void setChildren(List<HeaderFieldImpl> children) {
        this.children = children;
    }

    @Override
    public boolean isMultiValue() {
        return multiValue;
    }

    public void setMultiValue(boolean multiValue) {
        this.multiValue = multiValue;
    }

    @Override
    public Class<?> getMultiValueCollectionType() {
        return multiValueCollectionType;
    }

    public void setMultiValueCollectionType(Class<?> multiValueCollectionType) {
        this.multiValueCollectionType = multiValueCollectionType;
    }

    @Override
    public Class<?> getMultiValueElementType() {
        return multiValueElementType;
    }

    public void setMultiValueElementType(Class<?> multiValueElementType) {
        this.multiValueElementType = multiValueElementType;
    }
}

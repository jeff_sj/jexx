package jexx.poi.header.factory;

/**
 * @author jeff
 * @since 2019/6/14
 */
public enum HeaderColumnSelectMode {

    /**
     * 所有字段, 忽视注解, 只构建当前对象的字段，不会遍历子对象
     */
    ALL,
    /**
     * 所有字段, 有注解则使用注解; 会遍历子对象
     */
    ALL_WITH_TAG,
    /**
     * 仅注解的字段; 会遍历子对象
     */
    ONLY_TAG

}

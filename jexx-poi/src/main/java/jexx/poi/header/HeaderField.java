package jexx.poi.header;

/**
 * 头字段
 * @author jeff
 * @since 2019/12/3
 */
public interface HeaderField extends HeaderModifyField {

    /**
     * 字段名称
     * @return 字段
     */
    String getFieldName();

    /**
     * header头样式名称
     * @return 头样式名称
     */
    String getHeaderCellStyle();

    /**
     * 数据样式名称
     * @return 数据样式名称
     */
    String getDataCellStyle();

    /**
     * 引用的列名称
     * @return 引用的列名称
     */
    String getReferKey();

    /**
     * meta信息名称
     * @return meta名称
     */
    String getMeta();

    /**
     * 是否待约束
     * @return 待约束
     */
    boolean isValid();

    /**
     * 是否为多值
     */
    boolean isMultiValue();

    /**
     * 多值集合类型
     */
    Class<?> getMultiValueCollectionType();

    /**
     * 多值集合元素类型
     */
    Class<?> getMultiValueElementType();

}

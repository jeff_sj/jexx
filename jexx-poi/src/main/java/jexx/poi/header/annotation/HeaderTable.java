package jexx.poi.header.annotation;

import jexx.poi.header.Headers;

import java.lang.annotation.*;

/**
 * @author jeff
 * @since 2019/6/14
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface HeaderTable {

    /**
     * headers的名称
     */
    String name() default Headers.DEFAULT_HEADERS;

}

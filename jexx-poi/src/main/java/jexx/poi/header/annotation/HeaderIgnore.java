package jexx.poi.header.annotation;

import java.lang.annotation.*;

/**
 * @author jeff
 * @since 2019/6/19
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface HeaderIgnore {
}

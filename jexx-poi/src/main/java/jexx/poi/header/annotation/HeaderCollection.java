package jexx.poi.header.annotation;

import java.lang.annotation.*;

/**
 * 标记字段属于集合
 *
 * @author jeff
 * @since 2019/6/14
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface HeaderCollection {

}

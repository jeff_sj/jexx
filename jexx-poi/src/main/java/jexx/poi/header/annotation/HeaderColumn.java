package jexx.poi.header.annotation;

import jexx.util.StringPool;

import java.lang.annotation.*;

/**
 * @author jeff
 * @since 2019/6/14
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface HeaderColumn {

    /**
     * 列字段分组
     */
    Class[] group() default {};

    /**
     * header名称
     */
    String value() default "";

    /**
     * 是否为数组或者集合,默认不是
     */
    boolean list() default false;

    /**
     * 组排序，越小越靠前，默认为0
     * @return 组排序
     */
    int groupOrder() default 0;

    /**
     * 组排序下的排序，越小越靠前, 默认为0
     * @return 组排序下的排序
     */
    int order() default 0;

    /**
     * 头样式名称,可直接使用内部样式, EmbeddedDataStyleName 已定义
     * <pre>
     *     _header : header样式
     *     _data : data样式
     *     _none : 无样式
     *     _year : 日期样式,yyyy
     *     _month : 日期样式,yyyy-MM
     *     _date : 日期样式,yyyy-MM-dd
     *     _datetime : 日期样式,yyyy-MM-dd HH:mm:ss
     *     _formula : 公式样式
     *     _twoDecimal : 保留两位小数
     * </pre>
     */
    String headerCellStyle() default "_header";

    /**
     * 数据样式名称,可直接使用内部样式
     * <pre>
     *     _header : header样式
     *     _data : data样式
     *     _none : 无样式
     *     _year : 日期样式,yyyy
     *     _month : 日期样式,yyyy-MM
     *     _date : 日期样式,yyyy-MM-dd
     *     _datetime : 日期样式,yyyy-MM-dd HH:mm:ss
     *     _formula : 公式样式
     * </pre>
     */
    String dataCellStyle() default  "_data";

    /**
     * 引用列
     */
    String referKey() default StringPool.EMPTY;

    /**
     * meta名称; 当 @HeaderMeta 注解起效时, 此属性忽视
     */
    String meta() default StringPool.EMPTY;

    /**
     * 含有meta的时候，是否包含约束信息，即下拉框, 默认true
     */
    boolean valid() default true;

    /**
     * 是否为多值; 多值情况下，注解对应变量必须是 数组,List,或者 Set中一个
     */
    boolean multiValue() default false;

}

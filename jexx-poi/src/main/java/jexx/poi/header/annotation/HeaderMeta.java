package jexx.poi.header.annotation;

import java.lang.annotation.*;

/**
 * meta信息注解, 仅适用与 ArrayMeta
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface HeaderMeta {

    String name();

    String[] values();

    Class<?> valueType() default String.class;

    String[] labels() default {};

}

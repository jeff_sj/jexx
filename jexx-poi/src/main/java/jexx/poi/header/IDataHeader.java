package jexx.poi.header;

import jexx.poi.cell.ICellSupportMeta;
import jexx.poi.header.label.LabelUnwrapFunction;
import jexx.poi.header.label.LabelWrapFunction;
import jexx.poi.meta.*;
import jexx.poi.style.IWrapCellStyle;
import jexx.poi.util.CellOperateUtil;
import jexx.util.Assert;
import jexx.util.StringUtil;

import java.util.Map;
import java.util.Stack;

/**
 * a header that indicate data
 *
 * @author jeff
 * @since 2019/5/27
 * @since 1.0.6.1
 */
public interface IDataHeader extends IHeader {

    /**
     * 是否为多值
     */
    boolean isMultiValue();

    /**
     * 获取多值集合类型, array, List 或者 set
     */
    Class<?> getValueCollectionType();

    /**
     * 获取多值情况下集合的元素类型
     */
    Class<?> getValueElementType();

    /**
     * label分隔符
     */
    default String getLabelDelimiter(){
        return ",";
    }

    /**
     * refer header
     *
     * @return header
     */
    IDataHeader getReferHeader();

    /**
     * meta type
     *
     * @return {@link DVConstraintType}
     */
    DVConstraintType getDVConstraintType();

    /**
     * get the header's meta
     *
     * @return meta
     */
    IMeta getMeta();

    /**
     * valid data if include meta
     */
    boolean isValid();

    /**
     * a function that wrap label
     *
     * @return label function
     */
    LabelWrapFunction getWrapLabelFunction();

    /**
     * 显示的label转换为实际label
     */
    LabelUnwrapFunction getUnwrapLabelFunction();

    /**
     * 获取数据样式
     */
    IWrapCellStyle getDataCellStyle();

    Object getValueByLabel(Map<String, Object> labelMap);

    /**
     * 创建数据有效性公式
     * @param startRowNum 开始行
     * @return 数据有效性公式
     */
    default String createDataValidateFormula(int startRowNum){
        IMeta meta = getMeta();
        Assert.notNull(meta, "header's meta cannot be null!");

        StringBuilder nameBuilder = new StringBuilder("INDIRECT(SUBSTITUTE(CONCATENATE(");
        nameBuilder.append("\"").append(ICellSupportMeta.NAME_PREFIX).append("\",");
        nameBuilder.append(",\"").append(meta.getName()).append("\"");
        if(meta instanceof ArrayMeta){
            //skip
        }
        else if(meta instanceof TreeMeta){
            Stack<IDataHeader> stack = new Stack<>();
            IDataHeader refer = getReferHeader();
            while(refer != null){
                stack.push(refer);
                refer = refer.getReferHeader();
            }

            if(!stack.isEmpty()){
                IDataHeader fetch = stack.pop();
                while (fetch != null){
                    nameBuilder.append(",\"").append(AbstractMeta.SEPARATOR).append("\",");
                    String columnNo = CellOperateUtil.toColumnLabel(fetch.getStartColumnNum());
                    nameBuilder.append("$").append(columnNo).append("").append(startRowNum);
                    if(stack.isEmpty()){
                        break;
                    }
                    fetch = stack.pop();
                }
            }

        }
        else{
            throw new IllegalArgumentException(StringUtil.format("cannot handle this meta, meta[{}]!", meta.getClass()));
        }
        nameBuilder.append("),\" \",\"\"))");
        return  nameBuilder.toString();
    }

}

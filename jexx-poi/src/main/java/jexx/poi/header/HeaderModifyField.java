package jexx.poi.header;

/**
 * header字段可修改部分属性
 * @author jeff
 * @since 2019/12/3
 */
public interface HeaderModifyField {

    /**
     * 字段解释
     * @return 字段解释
     */
    String getValue();

    /**
     * 组排序
     * @return 组排序
     */
    int getGroupOrder();

    /**
     * 排序
     * @return 排序
     */
    int getOrder();

}

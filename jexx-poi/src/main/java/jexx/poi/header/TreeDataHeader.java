package jexx.poi.header;

import jexx.poi.meta.DVConstraintType;
import jexx.poi.meta.TreeMeta;

/**
 * tree header
 *
 * @author jeff
 */
public class TreeDataHeader extends AbstractDataHeader<TreeDataHeader> {

    private final TreeMeta meta;
    private final IDataHeader referHeader;

    public TreeDataHeader(String key, String value, int columnCount, TreeMeta meta, IDataHeader referHeader) {
        super(key, value, columnCount);
        this.meta = meta;
        this.referHeader = referHeader;
    }

    /**
     * 构造级联头部信息
     * @param key 变量名称
     * @param value 变量对应解释名称
     * @param meta 级联所需元数据
     * @param referHeader 引用另一个头部信息来构建名称,请确保该头部信息已存在
     */
    public TreeDataHeader(String key, String value, TreeMeta meta, IDataHeader referHeader) {
        this(key, value, 1, meta, referHeader);
    }

    @Override
    public DVConstraintType getDVConstraintType() {
        return DVConstraintType.TREE;
    }

    @Override
    public IDataHeader getReferHeader(){
        return referHeader;
    }

    @Override
    public TreeMeta getMeta() {
        return meta;
    }

}

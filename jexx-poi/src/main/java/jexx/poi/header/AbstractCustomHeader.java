package jexx.poi.header;

import jexx.poi.style.CellStyleSets;
import jexx.poi.style.IWrapCellStyle;

/**
 * @author jeff
 * @since 2019/6/19
 */
public abstract class AbstractCustomHeader extends AbstractHeader implements ICustomHeader {

    protected String sameLevelHeaderName;

    protected IDataHeader sameLevelHeader;

    /**
     * 数据样式
     */
    private IWrapCellStyle dataCellStyle;

    public AbstractCustomHeader(String value, int columnCount, IDataHeader sameLevelHeader, IWrapCellStyle headerCellStyle, IWrapCellStyle dataCellStyle){
        this.key = null;
        this.value = value;
        this.columnCount = columnCount;
        this.sameLevelHeader = sameLevelHeader;
        if(sameLevelHeader != null){
            this.sameLevelHeaderName = sameLevelHeader.getKey();
        }
        this.headerCellStyle = headerCellStyle;
        this.dataCellStyle = dataCellStyle;
    }

    public AbstractCustomHeader(String value, int columnCount, IDataHeader sameLevelHeader){
        this(value, columnCount, sameLevelHeader, CellStyleSets.HEADER_CELL_STYLE, CellStyleSets.DATA_CELL_STYLE);
    }

    public AbstractCustomHeader(String value, int columnCount, String sameLevelHeaderName, IWrapCellStyle headerCellStyle, IWrapCellStyle dataCellStyle){
        this.key = null;
        this.value = value;
        this.columnCount = columnCount;
        this.sameLevelHeaderName = sameLevelHeaderName;
        this.headerCellStyle = headerCellStyle;
        this.dataCellStyle = dataCellStyle;
    }

    public AbstractCustomHeader(String value, int columnCount, String sameLevelHeaderName){
        this(value, columnCount, sameLevelHeaderName, CellStyleSets.HEADER_CELL_STYLE, CellStyleSets.DATA_CELL_STYLE);
    }

    @Override
    public String getSameLevelHeaderName() {
        return sameLevelHeaderName;
    }

    @Override
    public IDataHeader getSameLevelHeader() {
        return sameLevelHeader;
    }

    @Override
    public void setSameLevelHeader(IDataHeader sameLevelHeader) {
        this.sameLevelHeader = sameLevelHeader;
        this.sameLevelHeaderName = sameLevelHeader.getKey();
    }

    @Override
    public IWrapCellStyle getDataCellStyle() {
        return dataCellStyle;
    }

    public void setDataCellStyle(IWrapCellStyle dataCellStyle) {
        this.dataCellStyle = dataCellStyle;
    }
}

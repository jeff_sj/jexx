package jexx.poi.header.row;

import jexx.poi.header.IDataHeader;
import jexx.util.Assert;
import jexx.util.CollectionUtil;
import jexx.util.StringPool;

import java.util.*;

/**
 * 数据header层级管理
 *
 * @author jeff
 * @since 2019/6/15
 */
public class HeaderRowContext {

    public static final String PROPERTY_KEY_SUFFIX = "[].";

    private final HeaderRow rootHeaderRow;

    /**
     * 所有的HeaderRow
     */
    private final List<HeaderRow> headerRows = new ArrayList<>();
    /**
     * path对应的HeaderRow, 如header的key为a[].b[].c，那么对应的path为a[].b[]
     */
    private final Map<String, HeaderRow> pathHeaderRowMap = new HashMap<>(16);
    private final Map<IDataHeader, HeaderRow> datHeaderHeaderRowMap = new HashMap<>(16);


    public HeaderRowContext() {
        rootHeaderRow = new HeaderRow();
        rootHeaderRow.path = StringPool.EMPTY;
        rootHeaderRow.paths = CollectionUtil.list(StringPool.EMPTY);

        headerRows.add(rootHeaderRow);
        pathHeaderRowMap.put(StringPool.EMPTY, rootHeaderRow);
    }

    public HeaderRow addHeader(IDataHeader header){
        String key = header.getKey();
        if(key.endsWith("[]")){
            key = key.concat(".");
        }

        List<String> paths = new ArrayList<>();
        paths.add(StringPool.EMPTY);
        int dotIndex = -1;
        String lastPath = StringPool.EMPTY;
        while(true){
            dotIndex = key.indexOf(PROPERTY_KEY_SUFFIX, dotIndex + PROPERTY_KEY_SUFFIX.length());
            if(dotIndex == -1){
                break;
            }
            lastPath = key.substring(0, dotIndex + PROPERTY_KEY_SUFFIX.length()-1);
            paths.add(lastPath);
        }

        HeaderRow headerRow = pathHeaderRowMap.get(lastPath);
        if(headerRow == null){
            headerRow = new HeaderRow();
            headerRow.path = lastPath;
            headerRow.paths = paths;
            findRightPositionForHeaderRow(headerRow);
            headerRows.add(headerRow);
            pathHeaderRowMap.put(lastPath, headerRow);
            datHeaderHeaderRowMap.put(header, headerRow);
        }
        headerRow.addHeader(header);
        return headerRow;
    }

    /**
     * 为headerRow找到合适的位置
     */
    private void findRightPositionForHeaderRow(HeaderRow headerRow){
        HeaderRow row = rootHeaderRow;
        int rowPathLength;
        int headerRowPathLength = headerRow.paths.size();
        while(row != null){
            rowPathLength = row.paths.size();
            if(rowPathLength < headerRowPathLength){
                Assert.isTrue(CollectionUtil.equals(row.paths, headerRow.paths, rowPathLength));
                if(row.child == null){
                    row.child = headerRow;
                    headerRow.parent = row;
                    break;
                }
                row = row.child;
            }
            else if(rowPathLength > headerRowPathLength){
                Assert.isTrue(CollectionUtil.equals(row.paths, headerRow.paths, headerRowPathLength));
                HeaderRow parent = row.parent;
                parent.child = headerRow;
                headerRow.parent = parent;
                headerRow.child = row;
                row.parent = headerRow;
            }
            else{
                throw new IllegalArgumentException("check header's key ");
            }
        }
    }

    public HeaderRow getRootHeaderRow() {
        return rootHeaderRow;
    }

    public List<HeaderRow> getHeaderRows() {
        return headerRows;
    }

    public HeaderRow getHeaderRowByPath(String path){
        return pathHeaderRowMap.get(path);
    }

    public HeaderRow getHeaderRowByHeader(IDataHeader header){
        return datHeaderHeaderRowMap.get(header);
    }
}

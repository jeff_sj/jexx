package jexx.poi.header.row;

import jexx.poi.header.IDataHeader;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author jeff
 * @since 2019/6/15
 */
public class HeaderRow {

    protected String path = "";
    protected List<String> paths;

    protected Map<Integer, IDataHeader> headerMap = new HashMap<>(16);

    protected HeaderRow parent;
    protected HeaderRow child;

    protected void addHeader(IDataHeader header){
        headerMap.put(header.getStartColumnNum(), header);
    }

    public String getPath() {
        return path;
    }

    public List<String> getPaths() {
        return paths;
    }

    public int getLevel(){
        if(parent == null){
            return 0;
        }
        return parent.getLevel()+1;
    }

    public Map<Integer, IDataHeader> getHeaderMap() {
        return headerMap;
    }

    public HeaderRow getChild() {
        return child;
    }

}

package jexx.poi.header;

import jexx.poi.header.factory.HeaderColumnSelectMode;
import jexx.poi.meta.Metas;
import jexx.poi.style.IWrapCellStyle;

import java.util.Collection;
import java.util.Map;
import java.util.ResourceBundle;

/**
 * @author jeff
 * @since 2019/6/14
 */
public interface HeadersFactory {

    /**
     * 附加样式
     * @param name 样式名称, 不能以 _ 开头
     * @param cellStyle 样式
     */
    HeadersFactory withCellStyle(String name, IWrapCellStyle cellStyle);

    /**
     * 附加默认头样式
     *
     * @param cellStyle 样式
     */
    HeadersFactory withDefaultHeaderCellStyle(IWrapCellStyle cellStyle);

    /**
     * 附加默认数据样式
     *
     * @param cellStyle 样式
     */
    HeadersFactory withDefaultDataCellStyle(IWrapCellStyle cellStyle);

    /**
     * 附加Metas
     * @param metas Metas
     */
    HeadersFactory withMetas(Metas metas);

    /**
     * 字段处理模式
     *
     * @param mode 模式
     */
    HeadersFactory select(HeaderColumnSelectMode mode);

    /**
     * 分组，不选择默认全部;
     * <pre>
     *     ALL: 分组不起效
     *     ALL_WITH_TAG: 有HeaderColumn标记,则验证分组
     *     ONLY_TAG: 验证分组
     * </pre>
     */
    HeadersFactory group(Class<?>... groups);

    /**
     * 忽视字段
     *
     * @param fields 字段名称
     */
    HeadersFactory ignoreField(String ... fields);

    /**
     * 忽视字段
     * @param fields 字段名称集合
     * @return {@link HeadersFactory}
     */
    HeadersFactory ignoreField(Collection<String> fields);

    /**
     * 修改字段部分属性
     * @param name 字段名称
     * @param field 字段可修改属性
     * @return {@link HeadersFactory}
     */
    HeadersFactory modify(String name, HeaderModifyField field);

    /**
     * 国际化,替换HeaderColumn的value
     *
     * @param i18nMap map
     */
    HeadersFactory i18n(Map<String, String> i18nMap);

    /**
     * 国际化,替换HeaderColumn的value, i18nMap的优先级高于resourceBundle
     *
     * @param resourceBundle ResourceBundle
     */
    HeadersFactory i18n(ResourceBundle resourceBundle);

    /**
     * 有则替换header
     *
     * @param header header
     */
    HeadersFactory replace(IDataHeader header);

    /**
     * 在header之后添加新的自定义header
     *
     * @param headerName header名称
     * @param header 待添加的header
     */
    HeadersFactory addHeaderAfter(String headerName, ICustomHeader header);

    /**
     * 在header之前添加新的自定义header
     *
     * @param headerName header名称
     * @param header 待添加的header
     */
    HeadersFactory addHeaderBefore(String headerName, ICustomHeader header);

    /**
     * 构建
     *
     * @param clazz 类
     */
    Headers build(Class<?> clazz);

}

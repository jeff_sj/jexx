package jexx.poi.header.label;

/**
 * label转换为正常显示的label
 */
public interface LabelUnwrapFunction {

    Object unwrap(Object lable);

}

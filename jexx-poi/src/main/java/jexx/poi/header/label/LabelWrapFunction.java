package jexx.poi.header.label;

/**
 * 显示名称转换为 包装显示名称; 再定义header对象时使用
 */
public interface LabelWrapFunction {

    Object wrap(Object label);

}

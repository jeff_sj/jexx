package jexx.poi.header;

import java.util.List;

/**
 * header group
 *
 * @author jeff
 * @since 2019/5/27
 * @since 1.0.6.1
 */
public interface IGroupHeader extends IHeader {

    /**
     * header children
     *
     * @return header sets
     */
    List<IHeader> getChildren();

}

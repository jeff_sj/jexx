package jexx.poi.header;

import jexx.poi.style.IWrapCellStyle;
import jexx.poi.style.WrapCellStyle;

/**
 * header interface
 *
 * @author jeff
 */
public interface IHeader {

    /**
     * start column num, base 1
     *
     * @return start column num
     */
    int getStartColumnNum();

    /**
     * set start column num
     *
     * @param startColumnNum start column num
     */
    void setStartColumnNum(int startColumnNum);

    /**
     * end column num, base 1
     *
     * @return end column num
     */
    int getEndColumnNum();

    /**
     * set end column num
     *
     * @param endColumnNum enc column num
     */
    void setEndColumnNum(int endColumnNum);

    /**
     * get header's columnCount
     *
     * @return header columnCount
     */
    int getColumnCount();

    /**
     * header key
     *
     * @return key
     */
    String getKey();

    /**
     * header value
     *
     * @return value
     */
    String getValue();

    /**
     * get the header cell style
     *
     * @return header cell style
     */
    IWrapCellStyle getHeaderCellStyle();

}

package jexx.poi.header;

import jexx.poi.meta.DVConstraintType;
import jexx.poi.meta.IMeta;
import jexx.poi.style.IWrapCellStyle;

import java.util.Map;

/**
 * default header
 *
 * @author jeff
 */
public class DefaultDataHeader extends AbstractDataHeader<DefaultDataHeader> {

    public DefaultDataHeader(String key, String value) {
        this(key, value, 1);
    }

    public DefaultDataHeader(String key, String value, int columnCount) {
        super(key, value, columnCount);
    }

    public DefaultDataHeader(String key, String value, IWrapCellStyle headerCellStyle, IWrapCellStyle dataCellStyle) {
        this(key, value, 1, headerCellStyle, dataCellStyle);
    }

    public DefaultDataHeader(String key, String value, int columnCount, IWrapCellStyle headerCellStyle, IWrapCellStyle dataCellStyle) {
        super(key, value, columnCount, headerCellStyle, dataCellStyle);
    }

    @Override
    public DVConstraintType getDVConstraintType() {
        return DVConstraintType.DEFAULT;
    }

    @Override
    public IDataHeader getReferHeader() {
        return null;
    }

    @Override
    public IMeta getMeta() {
        return null;
    }

    @Override
    protected String getFullLabel(String label, Map<?, ?> data){
        return label;
    }

}

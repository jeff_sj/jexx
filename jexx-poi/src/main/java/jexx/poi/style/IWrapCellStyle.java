package jexx.poi.style;

import org.apache.poi.ss.usermodel.*;

import java.util.Map;

/**
 * 单元格样式
 *
 * @author jeff
 */
public interface IWrapCellStyle {

    HorizontalAlignment getAlignment();

    VerticalAlignment getVerticalAlignment();

    BorderStyle getBottomBorderStyle();

    BorderStyle getLeftBorderStyle();

    BorderStyle getRightBorderStyle();

    BorderStyle getTopBorderStyle();

    IndexedColors getBottomBorderColor();

    IndexedColors getLeftBorderColor();

    IndexedColors getRightBorderColor();

    IndexedColors getTopBorderColor();

    String getDataFormat();

    FillPatternType getFillPattern();

    IndexedColors getFillForegroundColor();

    IndexedColors getFillBackgroundColor();

    boolean isHidden();

    boolean isLocked();

    short getIndent();

    short getRotation();

    boolean isWrapped();

}

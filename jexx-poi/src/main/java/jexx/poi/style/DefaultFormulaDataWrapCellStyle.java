package jexx.poi.style;

/**
 * @author jeff
 * @since 2019/9/25
 */
public class DefaultFormulaDataWrapCellStyle extends BaseWrapCellStyle {

    public DefaultFormulaDataWrapCellStyle() {
        super();
        this.locked = true;
    }
}

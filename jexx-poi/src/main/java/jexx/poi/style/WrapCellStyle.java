package jexx.poi.style;

import jexx.poi.constant.DataFormatEnum;
import org.apache.poi.ss.usermodel.*;

/**
 * the default implement of WrapCellStyle
 *
 * @author jeff
 */
public class WrapCellStyle extends AbstractWrapCellStyle implements IWrapCellStyle {

    /**
     * fill style
     */
    public void fill(IWrapCellStyle style) {
        if(style == null){
            return;
        }
        this.alignment = style.getAlignment();
        this.verticalAlignment = style.getVerticalAlignment();

        this.bottomBorderStyle = style.getBottomBorderStyle();
        this.leftBorderStyle = style.getLeftBorderStyle();
        this.rightBorderStyle = style.getRightBorderStyle();
        this.topBorderStyle = style.getTopBorderStyle();

        this.bottomBorderColor = style.getBottomBorderColor();
        this.leftBorderColor = style.getLeftBorderColor();
        this.rightBorderColor = style.getRightBorderColor();
        this.topBorderColor = style.getTopBorderColor();

        this.dataFormat = style.getDataFormat();

        this.fillPattern = style.getFillPattern();
        this.fillBackgroundColor = style.getFillBackgroundColor();
        this.fillForegroundColor = style.getFillForegroundColor();

        this.hidden = style.isHidden();
        this.locked = style.isLocked();
        this.indent = style.getIndent();
        this.rotation = style.getRotation();
        this.wrapped = style.isWrapped();
    }

    /**
     * set border style
     */
    public void setBorderStyle(BorderStyle borderStyle) {
        this.setTopBorderStyle(borderStyle);
        this.setBottomBorderStyle(borderStyle);
        this.setLeftBorderStyle(borderStyle);
        this.setRightBorderStyle(borderStyle);
    }

    /**
     * set border color
     */
    public void setBorderColor(IndexedColors borderColor){
        this.setTopBorderColor(borderColor);
        this.setBottomBorderColor(borderColor);
        this.setLeftBorderColor(borderColor);
        this.setRightBorderColor(borderColor);
    }

    public void setAlignment(HorizontalAlignment alignment) {
        this.alignment = alignment;
    }

    public void setVerticalAlignment(VerticalAlignment verticalAlignment) {
        this.verticalAlignment = verticalAlignment;
    }

    public void setBottomBorderStyle(BorderStyle bottomBorderStyle) {
        this.bottomBorderStyle = bottomBorderStyle;
    }

    public void setLeftBorderStyle(BorderStyle leftBorderStyle) {
        this.leftBorderStyle = leftBorderStyle;
    }

    public void setRightBorderStyle(BorderStyle rightBorderStyle) {
        this.rightBorderStyle = rightBorderStyle;
    }

    public void setTopBorderStyle(BorderStyle topBorderStyle) {
        this.topBorderStyle = topBorderStyle;
    }

    public void setBottomBorderColor(IndexedColors bottomBorderColor) {
        this.bottomBorderColor = bottomBorderColor;
    }

    public void setLeftBorderColor(IndexedColors leftBorderColor) {
        this.leftBorderColor = leftBorderColor;
    }

    public void setRightBorderColor(IndexedColors rightBorderColor) {
        this.rightBorderColor = rightBorderColor;
    }

    public void setTopBorderColor(IndexedColors topBorderColor) {
        this.topBorderColor = topBorderColor;
    }

    public void setDataFormat(String dataFormat) {
        this.dataFormat = dataFormat;
    }

    public void setDataFormat(DataFormatEnum format) {
        this.dataFormat = format.getFormat();
    }

    public void setFillPattern(FillPatternType fillPattern) {
        this.fillPattern = fillPattern;
    }

    public void setFillForegroundColor(IndexedColors fillForegroundColor) {
        this.fillForegroundColor = fillForegroundColor;
    }

    public void setFillBackgroundColor(IndexedColors fillBackgroundColor) {
        this.fillBackgroundColor = fillBackgroundColor;
    }

    public void setHidden(boolean hidden) {
        this.hidden = hidden;
    }

    public void setLocked(boolean locked) {
        this.locked = locked;
    }

    public void setIndent(short indent) {
        this.indent = indent;
    }

    public void setRotation(short rotation) {
        this.rotation = rotation;
    }

    public void setWrapped(boolean wrapped) {
        this.wrapped = wrapped;
    }

}

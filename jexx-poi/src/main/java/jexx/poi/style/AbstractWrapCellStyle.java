package jexx.poi.style;

import org.apache.poi.ss.usermodel.*;

/**
 * @author jeff
 * @since 2019/5/17
 */
public abstract class AbstractWrapCellStyle implements IWrapCellStyle {

    protected HorizontalAlignment alignment;
    protected VerticalAlignment verticalAlignment;

    protected BorderStyle bottomBorderStyle;
    protected BorderStyle leftBorderStyle;
    protected BorderStyle rightBorderStyle;
    protected BorderStyle topBorderStyle;

    protected IndexedColors bottomBorderColor;
    protected IndexedColors leftBorderColor;
    protected IndexedColors rightBorderColor;
    protected IndexedColors topBorderColor;

    protected String dataFormat;

    protected FillPatternType fillPattern;
    protected IndexedColors fillForegroundColor;
    protected IndexedColors fillBackgroundColor = IndexedColors.WHITE;

    protected boolean hidden;
    protected boolean locked;
    protected short indent;
    protected short rotation;
    protected boolean wrapped;

    @Override
    public HorizontalAlignment getAlignment() {
        return alignment;
    }

    @Override
    public VerticalAlignment getVerticalAlignment() {
        return verticalAlignment;
    }

    @Override
    public BorderStyle getBottomBorderStyle() {
        return bottomBorderStyle;
    }

    @Override
    public BorderStyle getLeftBorderStyle() {
        return leftBorderStyle;
    }

    @Override
    public BorderStyle getRightBorderStyle() {
        return rightBorderStyle;
    }

    @Override
    public BorderStyle getTopBorderStyle() {
        return topBorderStyle;
    }

    @Override
    public IndexedColors getBottomBorderColor() {
        return bottomBorderColor;
    }

    @Override
    public IndexedColors getLeftBorderColor() {
        return leftBorderColor;
    }

    @Override
    public IndexedColors getRightBorderColor() {
        return rightBorderColor;
    }

    @Override
    public IndexedColors getTopBorderColor() {
        return topBorderColor;
    }

    @Override
    public String getDataFormat() {
        return dataFormat;
    }

    @Override
    public FillPatternType getFillPattern() {
        return fillPattern;
    }

    @Override
    public IndexedColors getFillForegroundColor() {
        return fillForegroundColor;
    }

    @Override
    public IndexedColors getFillBackgroundColor() {
        return fillBackgroundColor;
    }

    @Override
    public boolean isHidden() {
        return hidden;
    }

    @Override
    public boolean isLocked() {
        return locked;
    }

    @Override
    public short getIndent() {
        return indent;
    }

    @Override
    public short getRotation() {
        return rotation;
    }

    @Override
    public boolean isWrapped() {
        return wrapped;
    }

}

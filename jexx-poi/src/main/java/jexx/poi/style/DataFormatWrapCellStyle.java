package jexx.poi.style;

import jexx.poi.constant.DataFormatEnum;

/**
 * 在基本样式基础上定义数据格式的样式
 *
 * @author jeff
 * @since 2019/6/15
 */
public class DataFormatWrapCellStyle extends BaseWrapCellStyle {

    public DataFormatWrapCellStyle(DataFormatEnum dataFormatEnum) {
        this(dataFormatEnum.getFormat());
    }

    public DataFormatWrapCellStyle(String dataFormat) {
        super();
        this.dataFormat = dataFormat;
    }

    public static DataFormatWrapCellStyle build(DataFormatEnum dataFormatEnum){
        return new DataFormatWrapCellStyle(dataFormatEnum.getFormat());
    }

    public static DataFormatWrapCellStyle build(String dataFormat){
        return new DataFormatWrapCellStyle(dataFormat);
    }

}

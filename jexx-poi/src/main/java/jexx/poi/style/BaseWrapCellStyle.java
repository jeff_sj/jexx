package jexx.poi.style;

import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.VerticalAlignment;

/**
 * base cell style
 *
 * @author jeff
 * @since 2019/6/15
 */
public abstract class BaseWrapCellStyle extends AbstractWrapCellStyle {

    public BaseWrapCellStyle() {
        this.alignment = HorizontalAlignment.CENTER;
        this.verticalAlignment = VerticalAlignment.CENTER;

        this.bottomBorderStyle = BorderStyle.THIN;
        this.leftBorderStyle = BorderStyle.THIN;
        this.topBorderStyle = BorderStyle.THIN;
        this.rightBorderStyle = BorderStyle.THIN;

        this.bottomBorderColor = IndexedColors.BLACK;
        this.leftBorderColor = IndexedColors.BLACK;
        this.topBorderColor = IndexedColors.BLACK;
        this.rightBorderColor = IndexedColors.BLACK;
    }

}

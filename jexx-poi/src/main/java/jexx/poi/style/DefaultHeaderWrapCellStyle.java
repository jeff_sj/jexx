package jexx.poi.style;

import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.IndexedColors;

/**
 * @author jeff
 * @since 2019/6/15
 */
public class DefaultHeaderWrapCellStyle extends BaseWrapCellStyle {

    public DefaultHeaderWrapCellStyle() {
        super();
        this.fillForegroundColor = IndexedColors.GREY_25_PERCENT;
        this.fillPattern = FillPatternType.SOLID_FOREGROUND;
        this.wrapped = true;
    }

}

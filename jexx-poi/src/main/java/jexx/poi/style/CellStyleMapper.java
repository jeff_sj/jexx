package jexx.poi.style;

import jexx.poi.cell.IMergedCell;

/**
 * 单元格样式包装
 *
 * @author jeff
 */
@FunctionalInterface
public interface CellStyleMapper {

    /**
     * 修饰单元格样式; <br/>
     * <b>单元格样式尽量复用,不宜过多<b/>
     * @param cell cell
     * @return {@link WrapCellStyle}
     */
    IWrapCellStyle warpCellStyle(IMergedCell cell);

}

package jexx.poi.style;

import jexx.poi.constant.DataFormatEnum;

/**
 * CellStyle样式集合
 * @author jeff
 */
public class CellStyleSets {

    /**
     * 空样式
     */
    public static final IWrapCellStyle NONE_CELL_STYLE = new NoneWrapCellStyle();
    /**
     * 默认标题头样式
     */
    public static final IWrapCellStyle HEADER_CELL_STYLE = new DefaultHeaderWrapCellStyle();
    /**
     * 默认单元格样式
     */
    public static final IWrapCellStyle DATA_CELL_STYLE = new DefaultDataWrapCellStyle();
    /**
     * 日期样式, yyyy-MM
     */
    public static final IWrapCellStyle YEAR_CELL_STYLE = new DataFormatWrapCellStyle(DataFormatEnum.MONTH);
    /**
     * 日期样式, yyyy-MM
     */
    public static final IWrapCellStyle MONTH_CELL_STYLE = new DataFormatWrapCellStyle(DataFormatEnum.MONTH);
    /**
     * 日期样式, yyyy-MM-dd
     */
    public static final IWrapCellStyle DATE_CELL_STYLE = new DataFormatWrapCellStyle(DataFormatEnum.DATE);
    /**
     * 日期样式, yyyy-MM-dd HH:mm:ss
     */
    public static final IWrapCellStyle DATE_TIME_CELL_STYLE = new DataFormatWrapCellStyle(DataFormatEnum.DATETIME);

    /**
     * 保留两位小数
     */
    public static final IWrapCellStyle DECIMAL_TWO_CELL_STYLE = new DataFormatWrapCellStyle(DataFormatEnum.DECIMAL_TWO);

    /**
     * 公式单元格样式
     */
    public static final IWrapCellStyle FORMULA_CELL_STYLE = new DefaultFormulaDataWrapCellStyle();

    /**
     * 使用默认样式来创建 {@link WrapCellStyle}
     * @return {@link WrapCellStyle}
     */
    public static WrapCellStyle createCellStyle(){
        WrapCellStyle cellStyle = new WrapCellStyle();
        cellStyle.fill(DATA_CELL_STYLE);
        return cellStyle;
    }

    /**
     * 利用已有样式来创建 {@link WrapCellStyle}
     * @param cellStyle 已有样式
     */
    public static WrapCellStyle createCellStyle(IWrapCellStyle cellStyle){
        WrapCellStyle wrapCellStyle = new WrapCellStyle();
        wrapCellStyle.fill(cellStyle);
        return wrapCellStyle;
    }

    private static class NoneWrapCellStyle extends AbstractWrapCellStyle {
        public NoneWrapCellStyle() {
        }
    }

}

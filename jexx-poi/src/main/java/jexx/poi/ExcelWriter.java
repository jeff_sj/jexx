package jexx.poi;

import jexx.exception.IORuntimeException;
import jexx.io.FileUtil;
import jexx.io.IOUtil;
import jexx.poi.constant.ExcelWriteMode;
import jexx.poi.constant.SheetVisibilityEnum;
import jexx.poi.meta.IMetaWriter;
import jexx.poi.meta.MetaSheetWriter;
import jexx.poi.meta.Metas;
import jexx.poi.util.CellOperateUtil;
import jexx.poi.util.WorkbookUtil;
import jexx.poi.writer.AbstractExcelWriter;
import jexx.poi.writer.impl.SheetWriterImpl;
import jexx.util.Assert;
import jexx.util.StringUtil;
import org.apache.poi.ss.formula.eval.ErrorEval;
import org.apache.poi.ss.usermodel.*;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * excel写入类
 *
 * @author jeff
 */
public class ExcelWriter extends AbstractExcelWriter {

    protected List<SheetWriter> sheetWriters = new ArrayList<>();

    /**
     * 创建excel的写入类
     */
    public ExcelWriter() {
        this(true);
    }

    /**
     * 创建excel写入类
     *
     * @param isXlsx true: xlsx false: xls
     */
    public ExcelWriter(boolean isXlsx) {
        this(WorkbookUtil.createBook(isXlsx));
    }

    public ExcelWriter(Workbook workbook) {
        super(workbook, true, 0);
    }

    /**
     * 创建excel的写入类
     * @param file excel文件
     */
    public ExcelWriter(File file) {
        this(WorkbookUtil.createBook(file));
    }

    /**
     * 创建excel的写入类
     * @param inputStream excel流
     * @param hiddenMetaSheet 隐藏MetaSheet
     */
    public ExcelWriter(InputStream inputStream, boolean hiddenMetaSheet) {
        super(WorkbookUtil.createBook(inputStream), hiddenMetaSheet, 0);
    }

    /**
     * 创建excel的写入类,默认隐藏meta
     * @param inputStream excel流
     */
    public ExcelWriter(InputStream inputStream) {
        this(inputStream, true);
    }

    /**
     * 创建默认sheet
     */
    public SheetWriter sheet(){
        return sheet("Sheet1");
    }

    public SheetWriter sheet(String sheetName){
        Assert.isTrue(!METADATA_SHEET_NAME.equalsIgnoreCase(sheetName), "Sheet name is not equal to {}", sheetName);
        if(StringUtil.isEmpty(sheetName)){
            sheetName = "Sheet1";
        }
        Sheet sheet = getOrCreateSheet(sheetName);
        SheetWriterImpl sheetWriter = new SheetWriterImpl(sheet);
        sheetWriters.add(sheetWriter);
        return sheetWriter;
    }

    /**
     * 刷新公式
     */
    public ExcelWriter evaluateFormulaCell(){
        flushSheetWriter();
        workbook.getCreationHelper().createFormulaEvaluator().evaluateAll();
        return this;
    }

    public ExcelWriter flush(File destFile){
        return flush(destFile, ExcelWriteMode.DEFAULT_MODE);
    }

    public ExcelWriter flush(File destFile, ExcelWriteMode writeMode){
        OutputStream out = null;
        try {
            out = FileUtil.newBufferedOutputStream(destFile);
            flush(out, writeMode);
        } finally {
            IOUtil.closeQuietly(out);
        }
        return this;
    }

    public ExcelWriter flush(OutputStream out){
        return flush(out, ExcelWriteMode.DEFAULT_MODE);
    }

    public ExcelWriter flush(OutputStream out, ExcelWriteMode writeMode){
        try {
            flushSheetWriter();

            if(ExcelWriteMode.VIEW_MODE == writeMode){
                //公式转换成值
                FormulaEvaluator evaluator = null;
                for(Sheet sheet : this.workbook){
                    if(METADATA_SHEET_NAME.equals(sheet.getSheetName())){
                        continue;
                    }
                    for(Row row : sheet){
                        for(Cell cell : row){
                            if(CellType.FORMULA == cell.getCellTypeEnum()){
                                if(evaluator == null){
                                    evaluator = workbook.getCreationHelper().createFormulaEvaluator();
                                }
                                CellValue cellValue = evaluator.evaluate(cell);
                                switch (cellValue.getCellTypeEnum()) {
                                    case NUMERIC:
                                        CellOperateUtil.setCellValue(cell, cellValue.getNumberValue());
                                        break;
                                    case STRING:
                                        CellOperateUtil.setCellValue(cell, cellValue.getStringValue());
                                        break;
                                    case BOOLEAN:
                                        CellOperateUtil.setCellValue(cell, cellValue.getBooleanValue());
                                        break;
                                    case ERROR:
                                        CellOperateUtil.setCellValue(cell, ErrorEval.getText(cellValue.getErrorValue()));
                                        break;
                                    default:
                                        CellOperateUtil.setCellValue(cell, "<error unexpected cell type " + cellValue.getCellTypeEnum() + ">");
                                        break;
                                }
                            }
                        }
                    }
                }
            }
            this.workbook.write(out);
            out.flush();
        } catch (IOException e) {
            throw new IORuntimeException(e);
        }
        return this;
    }

    /**
     * 刷新所有 {@link SheetWriterImpl}
     */
    private void flushSheetWriter(){
        for(SheetWriter sheetWriter : sheetWriters){
            sheetWriter.flush();
        }
    }






}

package jexx.poi;

import jexx.io.FileUtil;
import jexx.poi.bean.*;
import jexx.poi.cell.*;
import jexx.poi.constant.DataFormatEnum;
import jexx.poi.constant.ExcelWriteMode;
import jexx.poi.constant.PictureType;
import jexx.poi.header.*;
import jexx.poi.header.factory.HeaderColumnSelectMode;
import jexx.poi.meta.ArrayMeta;
import jexx.poi.meta.Metas;
import jexx.poi.meta.TreeMeta;
import jexx.poi.meta.node.INode;
import jexx.poi.meta.node.Node;
import jexx.poi.row.DataRow;
import jexx.poi.row.RowContext;
import jexx.poi.row.VirtualRow;
import jexx.poi.style.CellStyleSets;
import jexx.poi.style.DataFormatWrapCellStyle;
import jexx.poi.style.IWrapCellStyle;
import jexx.poi.style.WrapCellStyle;
import jexx.poi.util.CellOperateUtil;
import jexx.poi.util.FormulaUtil;
import jexx.util.*;
import org.apache.poi.ss.usermodel.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.math.BigDecimal;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

public class ExcelWriterTest {

    private static final Logger log = LoggerFactory.getLogger(ExcelWriterTest.class);

    protected String baseDir;

    @Before
    public void setUp(){
        URL data = ResourceUtil.getResource("poi");
        baseDir = data.getFile().concat("/").concat(ExcelWriterTest.class.getSimpleName());
        FileUtil.mkdirs(baseDir);
        Console.log("baseDir={}", baseDir);
    }

    /**
     * 写入无header的集合数据
     */
    @Test
    public void testWriteListRow(){
        ExcelWriter writer = new ExcelWriter();
        SheetWriter sheetWriter = writer.sheet();
        //无样式写入
        sheetWriter.writeListRow(CollectionUtil.list("1", "2", "3", "4"));
        //默认样式写入
        sheetWriter.writeListRow(CollectionUtil.list("1", "2", "3", "4"), cell->CellStyleSets.DATA_CELL_STYLE);
        //自定义样式写入
        sheetWriter.writeListRow(CollectionUtil.list("1", "2", "3", "4"), cell ->{
            if("3".equals(cell.getValue())){
                return CellStyleSets.DATE_CELL_STYLE;
            }
            return null;
        });
        writer.flush(FileUtil.file(baseDir, "testWriteListRow.xlsx"));
        writer.close();
    }

    /**
     * 基本数据写入
     */
    @Test
    public void testBase(){
        //随机10条数据
        List<Employee> employees = Employee.randomEmployees(10);

        ExcelWriter writer = new ExcelWriter();

        //自定义header
        Headers headers = new Headers();
        headers.addHeader("employeeNo", "工号\n(格式: x001)");
        headers.addHeader("employeeName", "姓名");
        headers.addHeader("age", "年龄");

        SheetWriter sheetWriter = writer.sheet("test1");
        sheetWriter.writeHeader(headers);
        sheetWriter.writeRows(employees, headers);
        writer.flush(FileUtil.file(baseDir, "testBase.xlsx"));
        writer.close();
    }

    /**
     * 样式控制, 数值转化
     */
    @Test
    public void testBaseStyle(){
        List<Employee> employees = Employee.randomEmployees(10);

        ExcelWriter writer = new ExcelWriter();

        Headers headers = new Headers("test");
        headers.addHeader("employeeNo", "工号");
        headers.addHeader("employeeName", "姓名");
        //显示值转换, 年龄为null转化为 "-"
        headers.addHeader(new DefaultDataHeader("age", "年龄"));
        headers.addHeader("birthdate", "生日", null, DataFormatWrapCellStyle.build(DataFormatEnum.DATE));
        headers.addHeader("percent", "百分比", null, DataFormatWrapCellStyle.build(DataFormatEnum.PERCENT));

        SheetWriter sheetWriter = writer.sheet("test1");
        sheetWriter.writeHeader(headers);
        sheetWriter.writeRows(employees, headers, (cell,bean) -> {
            //指定列设置样式
            if(headers.matchColumnNum(cell.getFirstColumnNum(), "employeeName")){
                WrapCellStyle cellStyle = CellStyleSets.createCellStyle();
                cellStyle.fill(cell.getCellStyle());
                cellStyle.setAlignment(HorizontalAlignment.CENTER);
                cellStyle.setFillForegroundColor(IndexedColors.RED);
                cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
                cellStyle.setBottomBorderStyle(BorderStyle.THIN);
                cell.setCellStyle(cellStyle);
            }
            else if(headers.matchColumnNum(cell.getFirstColumnNum(), "age")){
                if(bean.getAge() == null){
                    cell.setLabel("-");
                }
            }
        });
        sheetWriter.autoSizeColumn(headers);
        writer.flush(FileUtil.file(baseDir, "testBaseStyle.xlsx"));
        writer.close();
    }

    /**
     * 下拉框, 级联
     */
    @Test
    public void testCascade(){
        List<Department> departments = Department.randomDepartments(5);
        List<Employee> employees = Employee.randomEmployees(10, departments.get(0), departments.get(0).getPositions().get(0));

        //构建元信息类
        Metas metas = new Metas();

        //构建性别选择元信息
        List<INode> sexNodes = new ArrayList<>();
        sexNodes.add(new Node(true, "男"));
        sexNodes.add(new Node(false, "女"));
        ArrayMeta sexMeta = new ArrayMeta("性别", sexNodes);
        metas.addMeta(sexMeta);

        //部门,职位,员工 三级级联元信息构建; 此方式快速构建级联元信息, 请查看 Department中的@ExcelMeta
        TreeMeta treeMeta = TreeMeta.buildMetaWithAnnotation("部门", departments);
        metas.addMeta(treeMeta);

        ExcelWriter writer = new ExcelWriter();
        writer.setHiddenMetaSheet(false);
        //写入元信息
        writer.createMetaSheet(metas);

        Headers headers = new Headers("test");
        headers.addHeader("employeeNo", "工号", CellStyleSets.HEADER_CELL_STYLE, CellStyleSets.DATA_CELL_STYLE);
        headers.addHeader("employeeName", "姓名", CellStyleSets.HEADER_CELL_STYLE, CellStyleSets.DATA_CELL_STYLE);
        headers.addArrayHeader("sex", "性别", sexMeta, CellStyleSets.HEADER_CELL_STYLE, CellStyleSets.DATA_CELL_STYLE);
        TreeDataHeader departmentHeader = new TreeDataHeader("departmentId", "部门", treeMeta, null);
        departmentHeader.setHeaderCellStyle(CellStyleSets.HEADER_CELL_STYLE);
        departmentHeader.setDataCellStyle(CellStyleSets.DATA_CELL_STYLE);
        headers.addHeader(departmentHeader);
        TreeDataHeader positionHeader = new TreeDataHeader("positionId", "职位", treeMeta, departmentHeader);
        positionHeader.setHeaderCellStyle(CellStyleSets.HEADER_CELL_STYLE);
        positionHeader.setDataCellStyle(CellStyleSets.DATA_CELL_STYLE);
        headers.addHeader(positionHeader);
        TreeDataHeader employeeHeader = new TreeDataHeader("lastEmployeeId", "上级", treeMeta, positionHeader);
        employeeHeader.setHeaderCellStyle(CellStyleSets.HEADER_CELL_STYLE);
        employeeHeader.setDataCellStyle(CellStyleSets.DATA_CELL_STYLE);
        headers.addHeader(employeeHeader);

        SheetWriter sheetWriter = writer.sheet("test1");
        sheetWriter.setSupportValid(true);

        sheetWriter.writeHeader(headers);
//        sheetWriter.validData(20, headers);
        sheetWriter.writeRows(employees, headers, cell -> {
                    if(cell.getFirstColumnNum() == 1 || cell.getValue() == null){
                        WrapCellStyle cellStyle = CellStyleSets.createCellStyle();
                        cellStyle.fill(cell.getCellStyle());
                        cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
                        cellStyle.setAlignment(HorizontalAlignment.CENTER);
                        cellStyle.setFillForegroundColor(IndexedColors.RED);
                        //设置前景色的填充方式
                        cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
                        cellStyle.setBottomBorderStyle(BorderStyle.THIN);
                        return cellStyle;
                    }
                    return CellStyleSets.DATA_CELL_STYLE;
                })
        ;
        sheetWriter.autoSizeColumn(headers);
        writer.flush(FileUtil.file(baseDir, "testCascade.xlsx"));
    }

    @Test
    public void testWriteIndexBean(){
        List<Department> departments = Department.randomDepartments(10);

        Metas metas = new Metas();

        List<INode> sexNodes = new ArrayList<>();
        sexNodes.add(new Node(true, "男"));
        sexNodes.add(new Node(false, "女"));
        ArrayMeta sexMeta = new ArrayMeta("性别", sexNodes);
        metas.addMeta(sexMeta);

        TreeMeta treeMeta = TreeMeta.buildMetaWithAnnotation("部门", departments);
        metas.addMeta(treeMeta);

        ExcelWriter writer = new ExcelWriter(ResourceUtil.getStream("indexbean.xlsx"));
        writer.setHiddenMetaSheet(false);

        Headers headers = new Headers("test");
        headers.addSequenceHeader("序号");
        TreeDataHeader departmentHeader = new TreeDataHeader("id", "部门", treeMeta, null);
        headers.addHeader(departmentHeader);
        TreeDataHeader positionHeader = new TreeDataHeader("positions[].id", "职位", treeMeta, departmentHeader);
        headers.addHeader(positionHeader);

        GroupHeader groupHeader = new GroupHeader("员工");
        groupHeader.addHeader(new DefaultDataHeader("positions[].employees[].employeeNo", "工号"));
        groupHeader.addHeader(new DefaultDataHeader("positions[].employees[].employeeName", "姓名"));

        GroupHeader infoGroupHeader = new GroupHeader("信息");
        infoGroupHeader.addHeader(new DefaultDataHeader("positions[].employees[].age", "年龄"));
        infoGroupHeader.addHeader(new ArrayDataHeader("positions[].employees[].sex", "性别", sexMeta));
        groupHeader.addHeader(infoGroupHeader);

        headers.addHeader(groupHeader);

        IDataHeader hobbyHeader = new DefaultDataHeader("positions[].employees[].hobbies[]", "兴趣");
        headers.addHeader(hobbyHeader);
        metas.addHeaders(headers);

        writer.createMetaSheet(metas);

        //row trigger
        headers.addRowTrigger(positionHeader, (RowContext rowContext, DataRow row)-> {
            VirtualRow row1 = rowContext.appendVirtualRowAt(row);
            row1.addCell(new MergedCell(row1, 3, 4, "合计"));

            IMergedCell cell = new MergedCell(row1, 6, 6, rowContext.sum(row, 6), CellStyleSets.DATA_CELL_STYLE);
            row1.addCell(cell);
        });

        SheetWriter sheetWriter = writer.sheet("test1");
        sheetWriter.writeHeader(headers);
        sheetWriter.fill(4,7, departments, headers);
        writer.flush(FileUtil.file(baseDir, "testWriteIndexBean.xlsx"));
    }

    @Test
    public void testWriteIndexBean1(){
        List<FinanceReport> list = FinanceReport.createFinanceReport();
        try(ExcelWriter writer = new ExcelWriter()){
            WrapCellStyle percentCellStyle = CellStyleSets.createCellStyle();
            percentCellStyle.setDataFormat(DataFormatEnum.PERCENT);
            percentCellStyle.setLocked(true);

            WrapCellStyle thousandCellStyle = CellStyleSets.createCellStyle();
            thousandCellStyle.setDataFormat("#,##0.00");

            Headers headers = new Headers();
            headers.addHeader("schoolName", "学校");
            headers.addHeader("children[].memberType", "学生性质");
            headers.addHeader("children[].amount", "应收总额", CellStyleSets.HEADER_CELL_STYLE, thousandCellStyle);
            headers.addHeader("children[].shouldPayNum", "应收人数", CellStyleSets.HEADER_CELL_STYLE, CellStyleSets.DATA_CELL_STYLE);
            headers.addHeader("children[].payedAmount", "已收总额", CellStyleSets.HEADER_CELL_STYLE, thousandCellStyle);
            headers.addHeader("children[].payedNum", "已付人数", CellStyleSets.HEADER_CELL_STYLE, CellStyleSets.DATA_CELL_STYLE);
            headers.addHeader("children[].payedPercent", "已收占比", CellStyleSets.HEADER_CELL_STYLE , percentCellStyle);
            headers.addHeader("children[].noPayAmount", "未付总额", CellStyleSets.HEADER_CELL_STYLE, thousandCellStyle);
            headers.addHeader("children[].noPayPercent", "未付占比", CellStyleSets.HEADER_CELL_STYLE, percentCellStyle);
            headers.addRowTrigger("schoolName", (rowContext, row)->{
                VirtualRow virtualRow = rowContext.addVirtualRowAt(row);
                virtualRow.addCell(new MergedCell(virtualRow, 2, 2, "所有学生", CellStyleSets.DATA_CELL_STYLE));
                virtualRow.addCell(new MergedCell(virtualRow, 3, 3, rowContext.sum(row, 3), CellStyleSets.FORMULA_CELL_STYLE));
                virtualRow.addCell(new MergedCell(virtualRow, 4, 4, rowContext.sum(row, 4), CellStyleSets.FORMULA_CELL_STYLE));
                virtualRow.addCell(new MergedCell(virtualRow, 5, 5, rowContext.sum(row, 5), CellStyleSets.FORMULA_CELL_STYLE));
                virtualRow.addCell(new MergedCell(virtualRow, 6, 6, rowContext.sum(row, 6), CellStyleSets.FORMULA_CELL_STYLE));
                FormulaCellValue formula1 = new FormulaCellValue(StringUtil.format("E{}/C{}", virtualRow.getStartRowNum(), virtualRow.getStartRowNum()));
                virtualRow.addCell(new MergedCell(virtualRow, 7, 7, formula1, percentCellStyle));
                virtualRow.addCell(new MergedCell(virtualRow, 8, 8, rowContext.sum(row, 8), CellStyleSets.FORMULA_CELL_STYLE));
                FormulaCellValue formula2 = new FormulaCellValue(StringUtil.format("G{}/C{}", virtualRow.getStartRowNum(), virtualRow.getStartRowNum()));
                virtualRow.addCell(new MergedCell(virtualRow, 9, 9, formula2, percentCellStyle));
            });

            Headers groupHeaders = new Headers();
            groupHeaders.addHeader("schoolName", "学校");
            groupHeaders.addHeader("children[].memberType", "学生性质");
            groupHeaders.addHeader("children[].children[].gradeGroupName", "学部");
            groupHeaders.addHeader("children[].children[].children[].gradeName", "年级");
            groupHeaders.addHeader("children[].children[].children[].amount", "应收总额", CellStyleSets.HEADER_CELL_STYLE, thousandCellStyle);
            groupHeaders.addHeader("children[].children[].children[].shouldPayNum", "应收人数", CellStyleSets.HEADER_CELL_STYLE, CellStyleSets.DATA_CELL_STYLE);
            groupHeaders.addHeader("children[].children[].children[].payedAmount", "已收总额", CellStyleSets.HEADER_CELL_STYLE, thousandCellStyle);
            groupHeaders.addHeader("children[].children[].children[].payedNum", "已付人数", CellStyleSets.HEADER_CELL_STYLE, CellStyleSets.DATA_CELL_STYLE);
            groupHeaders.addHeader("children[].children[].children[].payedPercent", "已收占比", CellStyleSets.HEADER_CELL_STYLE, percentCellStyle);
            groupHeaders.addHeader("children[].children[].children[].noPayAmount", "未付总额", CellStyleSets.HEADER_CELL_STYLE, thousandCellStyle);
            groupHeaders.addHeader("children[].children[].children[].noPayPercent", "未付占比", CellStyleSets.HEADER_CELL_STYLE, percentCellStyle);

            SheetWriter sheetWriter = writer.sheet("财务报表");
            sheetWriter.writeHeader(headers);
            sheetWriter.writeRows(list, headers);
            sheetWriter.skipRows(2);
            sheetWriter.writeHeader(groupHeaders);
            sheetWriter.writeRows(list, groupHeaders);
            sheetWriter.flush();
            sheetWriter.autoSizeColumn(groupHeaders);

            writer.flush(FileUtil.file(baseDir, "testWriteIndexBean1.xlsx"));
        }
    }

    @Test
    public void testFill(){
        List<Student> students = Student.randomStudents(10);

        Headers headers = Headers.newBuild()
                .withDefaultDataCellStyle(null)
                .withDefaultDataCellStyle(null)
                .addHeaderAfter("e", new FormulaDataHeader("总和",
                        (DataRow row, int itemIndex, int columnNum)->FormulaUtil.calculateRowSumFormula(row.getStartRowNum(), "C", "G"),
                                null, null)
                        )
                .build(Student.class);
        headers.flush();

        ExcelWriter excelWriter = new ExcelWriter(ResourceUtil.getStream("testFill.xlsx"));
        SheetWriter sheetWriter = excelWriter.sheet();
        sheetWriter.writeHeader(headers);
        sheetWriter.fill(2,4, students, headers);
        excelWriter.flush(FileUtil.file(baseDir, "testFill.xlsx"));
        excelWriter.close();
    }

    @Test
    public void testWriteCell(){
        ExcelWriter writer = new ExcelWriter();
        writer.setHiddenMetaSheet(false);

        SheetWriter sheetWriter = writer.sheet("test1");

        IWrapCellStyle cellStyle = CellStyleSets.DATA_CELL_STYLE;

        Metas metas = new Metas();

        ArrayMeta sexMeta = new ArrayMeta("性别", CollectionUtil.list(new Node(true, "男"), new Node(false, "女")));
        metas.addMeta(sexMeta);

        List<Department> departments = Department.randomDepartments(5);
        TreeMeta departmentMeta = TreeMeta.buildMetaWithAnnotation("部门", departments);
        metas.addMeta(departmentMeta);

        writer.createMetaSheet(metas);

        byte[] image = ResourceUtil.getStreamAsBytes("test.png");
        PictureType pictureType = PictureType.PNG;

        int startRowNum;
        for(int i = 0; i < 3; i++){
            WrapCellStyle style = new WrapCellStyle();
            style.fill(cellStyle);
            style.setBorderStyle(BorderStyle.THIN);
            style.setBorderColor(IndexedColors.RED);

            //单个单元格写入
            startRowNum = 4*i + 1;
            SingleCell indexCell = new SingleCell(startRowNum, 1, i+1);
            indexCell.setCellStyle(style);
            sheetWriter.writeCell(indexCell);

            //合并单元格写入
            startRowNum++;
            MergedCell sexCell = new MergedCell(startRowNum, 2, startRowNum+1, 3 ,i%2==0);
            sexCell.setCellStyle(style);
            sexCell.setMeta(sexMeta);
            sheetWriter.writeCell(sexCell);

            startRowNum = startRowNum + 2;
            SingleCell departmentCell = new SingleCell(startRowNum, 4, 1);
            departmentCell.setCellStyle(style);
            departmentCell.setMeta(departmentMeta);
            departmentCell.valid(false);
            sheetWriter.writeCell(departmentCell);

            startRowNum = startRowNum + 1;
            MergedCell positionCell = new MergedCell(startRowNum, 5, startRowNum+1, 6, 1);
            positionCell.setCellStyle(style);
            positionCell.setCellReference(departmentCell);
            positionCell.setMeta(departmentMeta);
            positionCell.valid(false);
            sheetWriter.writeCell(positionCell);

            startRowNum = startRowNum + 2;
            SingleCell employeeCell = new SingleCell(startRowNum, 7, 1);
            employeeCell.setCellStyle(style);
            employeeCell.setMeta(departmentMeta);
            employeeCell.setCellReference(positionCell);
            sheetWriter.writeCell(employeeCell);

            //图片单元格写入
            startRowNum = startRowNum + 1;
            ImageCell imageCell = new ImageCell(startRowNum, 8, pictureType, image);
            sheetWriter.writeCell(imageCell);
        }
        sheetWriter.flush();

        writer.flush(FileUtil.file(baseDir, "testWriteCell.xlsx"));
        writer.close();
    }

    @Test
    public void testWriteView(){
        ExcelWriter writer = new ExcelWriter();
        SheetWriter sheetWriter = writer.sheet("Sheet1");
        sheetWriter.writeCell(new SingleCell(1, "E", new FormulaCellValue("SUM(A1:D1)")));
        sheetWriter.writeCell(new SingleCell(1, "F", new Date()));
        sheetWriter.writeCell(new SingleCell(1, "G", new FormulaCellValue("SUM(A1:C1)")));

        WrapCellStyle dataCellStyle = new WrapCellStyle();
        dataCellStyle.setDataFormat("yyyy-MM-dd");

        for(int i = 2; i < 10; i++){
            sheetWriter.writeCell(new SingleCell(i, "A", i));
            sheetWriter.writeCell(new SingleCell(i, "B", new FormulaCellValue("SUM(A1:D1)")));
            sheetWriter.writeCell(new SingleCell(i, "C", new Date(), dataCellStyle));
            sheetWriter.writeCell(new SingleCell(i, "D", i%2==0));
        }

        sheetWriter.flush();
        writer.flush(FileUtil.file(baseDir, "testWriteView.xlsx"), ExcelWriteMode.VIEW_MODE);
        writer.close();
    }

    @Test
    public void testWriteDataFormat(){
        ExcelWriter writer = new ExcelWriter();
        SheetWriter sheetWriter = writer.sheet("Sheet1");
        int firstRowNum = 1;

        //百分比
        sheetWriter.writeCell(new SingleCell(firstRowNum, "A", "百分比"));
        WrapCellStyle cellStyle = new WrapCellStyle();
        cellStyle.setDataFormat(DataFormatEnum.PERCENT);
        sheetWriter.writeCell(new SingleCell(firstRowNum, "B", 0.123, cellStyle));
        cellStyle = new WrapCellStyle();
        cellStyle.setDataFormat(DataFormatEnum.PERCENT_1);
        sheetWriter.writeCell(new SingleCell(firstRowNum, "C", 0.123, cellStyle));

        //日期
        firstRowNum++;
        sheetWriter.writeCell(new SingleCell(firstRowNum, "A", "时间"));
        cellStyle = new WrapCellStyle();
        cellStyle.setDataFormat(DataFormatEnum.DATE);
        sheetWriter.writeCell(new SingleCell(firstRowNum, "B", new Date(), cellStyle));
        cellStyle = new WrapCellStyle();
        cellStyle.setDataFormat(DataFormatEnum.DATETIME);
        sheetWriter.writeCell(new SingleCell(firstRowNum, "C", new Date(), cellStyle));

        //千分位
        firstRowNum++;
        sheetWriter.writeCell(new SingleCell(firstRowNum, "A", "千分位"));
        cellStyle = new WrapCellStyle();
        cellStyle.setDataFormat(DataFormatEnum.THOUSAND);
        sheetWriter.writeCell(new SingleCell(firstRowNum, "B", 123456789, cellStyle));

        //保留两位小数
        firstRowNum++;
        sheetWriter.writeCell(new SingleCell(firstRowNum, "A", "保留两位小数"));
        cellStyle = new WrapCellStyle();
        cellStyle.setDataFormat(DataFormatEnum.DECIMAL_TWO);
        sheetWriter.writeCell(new SingleCell(firstRowNum, "B", 123456.123, cellStyle));

        //保留两位小数
        firstRowNum++;
        sheetWriter.writeCell(new SingleCell(firstRowNum, "A", "身份证"));
        cellStyle = new WrapCellStyle();
//        cellStyle.setDataFormat("0.00");
        BigDecimal bbb = new BigDecimal("320481198712034614");
        sheetWriter.writeCell(new SingleCell(firstRowNum, "B", bbb.toString(), cellStyle));

        firstRowNum++;
        sheetWriter.writeCell(new SingleCell(firstRowNum, "A", "万元"));
        cellStyle = new WrapCellStyle();
        cellStyle.setDataFormat("0\\.0000");
        sheetWriter.writeCell(new SingleCell(firstRowNum, "B",  new BigDecimal(10000), cellStyle));

        sheetWriter.autoSizeColumnWithMaxColumnNum(3);
        sheetWriter.flush();
        writer.flush(FileUtil.file(baseDir, "testWriteDataFormat.xlsx"));
        writer.close();
    }

    /**
     * 对象中有map,map数据作为header
     */
    @Test
    public void testWriteMap(){
        List<Employee> employees = Employee.randomEmployees(100);
        for(Employee employee : employees){
            Map<String, Integer> results = employee.getResults();
            results.put("c1", ThreadLocalRandom.current().nextInt(100));
            results.put("c2", 1);
            results.put("c3", ThreadLocalRandom.current().nextInt(1000));
        }

        ExcelWriter writer = new ExcelWriter();

        Headers headers = new Headers("test");
        headers.addHeader("employeeNo", "工号");
        headers.addHeader("employeeName", "姓名");
        headers.addHeader("results[c1]", "c1");
        headers.addHeader("results[c2]", "c2");
        headers.addHeader("results[c3]", "c3");

        SheetWriter sheetWriter = writer.sheet("Sheet1");
        sheetWriter.writeHeader(headers);
        sheetWriter.writeRows(employees, headers);
        sheetWriter.flush();
        writer.flush(FileUtil.file(baseDir, "testWriteMap.xlsx"));
        writer.close();
    }

    @Test
    public void testAutoColumn(){
        ExcelWriter writer = new ExcelWriter();

        SheetWriter sheetWriter = writer.sheet("Sheet1");

        sheetWriter.writeCell(new SingleCell( 1, 1, "你好", CellStyleSets.DATA_CELL_STYLE));
        sheetWriter.writeCell(new SingleCell(1, 2 , "yOikqexAPXRNkvfXFLbk", CellStyleSets.DATA_CELL_STYLE));
        sheetWriter.writeCell(new SingleCell(1, 3 , new FormulaCellValue("A1&B1"), CellStyleSets.DATA_CELL_STYLE));
        WrapCellStyle dataCellStyle = CellStyleSets.createCellStyle();
        dataCellStyle.setDataFormat(DataFormatEnum.DATETIME);
        sheetWriter.writeCell(new SingleCell(1, 4 , new Date(), dataCellStyle));

        sheetWriter.writeCell(new MergedCell(2, 1 , 3,1, "我是中国人民解放军的坚强后盾，放心交给我", CellStyleSets.DATA_CELL_STYLE));

        sheetWriter.flush();

        writer.evaluateFormulaCell();
        sheetWriter.autoSizeColumnWithMaxColumnNum(4);

        writer.flush(FileUtil.file(baseDir, "testAutoColumn.xlsx"));
        writer.close();
    }

    @Test
    public void testEvaluateFormulaCell(){
        ExcelWriter writer = new ExcelWriter(ResourceUtil.getStream("testEvaluateFormulaCell.xlsx"));
        SheetWriter sheetWriter = writer.sheet("Sheet1");
        sheetWriter.writeCell(new SingleCell(1,1, 1));
        sheetWriter.writeCell(new SingleCell(1,2, 2));
        sheetWriter.writeCell(new SingleCell(1,3, 3));
        sheetWriter.flush();
        writer.evaluateFormulaCell();
        writer.flush(FileUtil.file(baseDir, "testEvaluateFormulaCell.xlsx"));
        writer.close();
    }

    @Test
    public void testWriteWithBuildHeader(){
        List<Employee> employees = Employee.randomEmployees(100);

        //meta
        Metas metas = new Metas();
        metas.addMeta(new ArrayMeta("sex", CollectionUtil.list(new Node(true, "男"), new Node(false, "女"))));
        metas.addMeta(TreeMeta.buildMetaWithAnnotation("department", Department.randomDepartments(5)));
        metas.addMeta(ArrayMeta.buildMeta("employees", employees, Employee::getEmployeeNo, Employee::getEmployeeName));

        //i18n
        Map<String, String> i18nMap = MapUtil.toSingletonMap("i18.employeeNo", "工号");

        Headers headers = Headers.newBuild().select(HeaderColumnSelectMode.ONLY_TAG)
                .withMetas(metas)
                .withCellStyle("date", CellStyleSets.DATE_CELL_STYLE)
                .withCellStyle("percent", new DataFormatWrapCellStyle(DataFormatEnum.PERCENT))
                .i18n(i18nMap)
                .build(Employee.class);

        ExcelWriter writer = new ExcelWriter();
        writer.createMetaSheet(metas);
        SheetWriter sheetWriter = writer.sheet("Sheet1");
        sheetWriter.setSupportValid(true);

        sheetWriter.writeHeader(headers);
        sheetWriter.writeRows(employees, headers);
        sheetWriter.autoSizeColumn(headers);
        writer.flush(FileUtil.file(baseDir, "testWriteWithBuildHeader.xlsx"));
        writer.close();
    }

    @Test
    public void testWriteStudents(){
        List<Grade> grades = Grade.randomClass(2);

        Headers headers = Headers.newBuild().select(HeaderColumnSelectMode.ONLY_TAG)
                .addHeaderAfter("classList[].students[].e", new FormulaDataHeader("成绩总和", 1, "classList[].students[].e", (DataRow row, int itemIndex, int columnNum)
                        ->FormulaUtil.calculateRowSumFormula(row.getStartRowNum(), "E", "I")))
                .build(Grade.class);
        headers.flush();

        WrapCellStyle hjCellStyle = CellStyleSets.createCellStyle();
        hjCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        hjCellStyle.setFillForegroundColor(IndexedColors.GREEN);

        headers.addRowTrigger("classList[].className", (RowContext rowContext, DataRow row)->{
            VirtualRow virtualRow = rowContext.addVirtualRowAt(row);
            virtualRow.addCell(new MergedCell(virtualRow, "C", "D", "合计", hjCellStyle));
            int startColumnNum = CellOperateUtil.toColumnNum("E");
            for(int i = 0; i < 5; i++){
                String columnNo = CellOperateUtil.toColumnLabel(startColumnNum+i);
                virtualRow.addCell(new MergedCell(virtualRow, columnNo, columnNo, rowContext.sum(row, columnNo), hjCellStyle));
            }
            FormulaCellValue formulaCellValue = new FormulaCellValue(FormulaUtil.calculateRowSumFormula(virtualRow.getStartRowNum(), startColumnNum, startColumnNum+4));
            virtualRow.addCell(new MergedCell(virtualRow, startColumnNum+5, startColumnNum+5, formulaCellValue, hjCellStyle));
        });

        WrapCellStyle zhjCellStyle = CellStyleSets.createCellStyle();
        zhjCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        zhjCellStyle.setFillForegroundColor(IndexedColors.YELLOW);
        zhjCellStyle.setLocked(true);
        headers.addRowTrigger("gradeName", (RowContext rowContext, DataRow row)->{
            VirtualRow virtualRow = rowContext.addVirtualRowAt(row);
            virtualRow.addCell(new MergedCell(virtualRow, "B", "D", "总合计", zhjCellStyle));
            int startColumnNum = CellOperateUtil.toColumnNum("E");
            for(int i = 0; i < 5; i++){
                String columnNo = CellOperateUtil.toColumnLabel(startColumnNum+i);
                virtualRow.addCell(new MergedCell(virtualRow, columnNo, columnNo, rowContext.sum(row, columnNo), zhjCellStyle));
            }
            FormulaCellValue formulaCellValue = new FormulaCellValue(FormulaUtil.calculateRowSumFormula(virtualRow.getStartRowNum(), startColumnNum, startColumnNum+4));
            virtualRow.addCell(new MergedCell(virtualRow, startColumnNum+5, startColumnNum+5, formulaCellValue, zhjCellStyle));
        });

        ExcelWriter writer = new ExcelWriter(false);
        SheetWriter sheetWriter = writer.sheet("Sheet1");
        sheetWriter.writeHeader(headers);
        sheetWriter.writeRows(grades, headers);
        sheetWriter.autoSizeColumn(headers);
        writer.flush(FileUtil.file(baseDir, "testWriteStudents.xls"));
        writer.close();
    }

    /**
     * 模拟合并多个合并过的单元格
     */
    @Test
    public void testMulWriteMergeCell(){
        ExcelWriter writer = new ExcelWriter(false);
        SheetWriter sheetWriter = writer.sheet("Sheet1");
        Assert.assertNotNull(sheetWriter);

        try {
            sheetWriter.writeCell(new MergedCell(1, 1, 1, 2));
            sheetWriter.writeCell(new MergedCell(2, 1, 2, 2));
            sheetWriter.writeCell(new MergedCell(1, 1, 2, 2));
            writer.flush(FileUtil.file(baseDir, "testMulWriteMergeCell.xls"));
            Assert.fail();
        }
        catch (IllegalStateException e){
           //skip
            log.info(e.getMessage());
        }
        writer.close();
    }

    /**
     * 测试 long 类型的数据，超过一定长度后，写入excel中，数据会有偏差。 此处记录下，如何处理？
     * 不使用long类型，公式求和会有问题； 使用 long类型，数据又会出现偏差。选哪一种都会有问题？
     */
    @Test
    public void testWriteLong(){
        TestLongBean bean1 = new TestLongBean();
        bean1.setA(1679038802959540226L);
        bean1.setB("1679038802959540226");
        List<TestLongBean> beanList = CollectionUtil.list(bean1);

        Headers headers = new Headers();
        headers.addHeader("a", "a");
        headers.addHeader("b", "b");

        ExcelWriter writer = new ExcelWriter(false);
        SheetWriter sheetWriter = writer.sheet("Sheet1");

        sheetWriter.writeRows(beanList, headers);

        File file = FileUtil.file(baseDir, "testWriteLong.xlsx");
        writer.flush(file);
        writer.close();

        ExcelReader reader = new ExcelReader(file);
        SheetReader sheetReader = reader.sheet("Sheet1");
        TestLongBean readBean1 = sheetReader.readBeanRowAndNext(TestLongBean.class, headers);
        reader.close();

        Assert.assertNotEquals(bean1.getA(), readBean1.getA());
        Assert.assertEquals(bean1.getB(), readBean1.getB());

    }

}

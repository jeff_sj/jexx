package jexx.poi.bean;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * @author jeff
 * @since 2019/9/24
 */
public class FinanceReport {

    private static final BigDecimal ZERO = BigDecimal.ZERO;

    /**
     * 学校ID
     */
    private Integer schoolId;
    /**
     * 学校名称
     */
    private String schoolName;

    /**
     * 成员类型
     */
    private String memberType;

    /**
     * 学部ID
     */
    private Integer gradeGroupId;

    /**
     * 学部名称
     */
    private String gradeGroupName;

    /**
     * 年级ID
     */
    private Integer gradeId;

    /**
     * 年级名称
     */
    private String gradeName;

    /**
     * 应收总额
     */
    private BigDecimal amount = ZERO;
    /**
     * 已收总额
     */
    private BigDecimal payedAmount = ZERO;
    /**
     * 已收占比
     */
    private BigDecimal payedPercent ;
    /**
     * 未付总额
     */
    private BigDecimal noPayAmount = ZERO;
    /**
     * 未付占比
     */
    private BigDecimal noPayPercent;
    /**
     * 应付人数
     */
    private Integer shouldPayNum;
    /**
     * 已付人数
     */
    private Integer payedNum;
    /**
     * 部分付款人数
     */
    private Integer partPayedNum;
    /**
     * 应退款金额
     */
    private BigDecimal needRefundMoney = ZERO;
    /**
     * 应退款人数
     */
    private Integer needRefundNum;
    /**
     * 实际退款金额
     */
    private BigDecimal actualRefundMoney = ZERO;
    /**
     * 实际退款人数
     */
    private Integer actualRefundNum;

    /**
     * 学部报表统计
     */
    private List<FinanceReport> children = new ArrayList<>();

    public static List<FinanceReport> createFinanceReport(){
        List<FinanceReport> list = new ArrayList<>();
        for(int i = 0; i < 3;i ++){
            list.add(createSchool("学校"+i));
        }
        return list;
    }

    private static FinanceReport createSchool(String schoolName){
        FinanceReport yunMail = new FinanceReport();
        yunMail.setSchoolName(schoolName);
        for(int i = 0; i < 3; i++){
            String name;
            if(i == 0){
                name = "老生";
            }
            else if(i == 1){
                name = "新生";
            }
            else{
                name = "合计";
            }
            yunMail.getChildren().add(createMember(name));
        }
        return yunMail;
    }

    private static FinanceReport createMember(String name){
        FinanceReport yunMail = new FinanceReport();
        yunMail.setMemberType(name);
        if(!"合计".equals(name)){
            for(int i = 0; i < 3; i++){
                yunMail.getChildren().add(createGradeGroup("学部"+i));
            }
        }
        return yunMail;
    }
    private static FinanceReport createGradeGroup(String name){
        FinanceReport yunMail = new FinanceReport();
        yunMail.setGradeGroupName(name);
        for(int i = 0; i < 5; i++){
            yunMail.getChildren().add(createGrade("年级"+i));
        }
        return yunMail;
    }
    private static FinanceReport createGrade(String name){
        FinanceReport yunMail = new FinanceReport();
        yunMail.setGradeName(name);
        return yunMail;
    }


    public Integer getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(Integer schoolId) {
        this.schoolId = schoolId;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getMemberType() {
        return memberType;
    }

    public void setMemberType(String memberType) {
        this.memberType = memberType;
    }

    public Integer getGradeGroupId() {
        return gradeGroupId;
    }

    public void setGradeGroupId(Integer gradeGroupId) {
        this.gradeGroupId = gradeGroupId;
    }

    public String getGradeGroupName() {
        return gradeGroupName;
    }

    public void setGradeGroupName(String gradeGroupName) {
        this.gradeGroupName = gradeGroupName;
    }

    public Integer getGradeId() {
        return gradeId;
    }

    public void setGradeId(Integer gradeId) {
        this.gradeId = gradeId;
    }

    public String getGradeName() {
        return gradeName;
    }

    public void setGradeName(String gradeName) {
        this.gradeName = gradeName;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getPayedAmount() {
        return payedAmount;
    }

    public void setPayedAmount(BigDecimal payedAmount) {
        this.payedAmount = payedAmount;
    }

    public BigDecimal getPayedPercent() {
        return payedPercent;
    }

    public void setPayedPercent(BigDecimal payedPercent) {
        this.payedPercent = payedPercent;
    }

    public BigDecimal getNoPayAmount() {
        return noPayAmount;
    }

    public void setNoPayAmount(BigDecimal noPayAmount) {
        this.noPayAmount = noPayAmount;
    }

    public BigDecimal getNoPayPercent() {
        return noPayPercent;
    }

    public void setNoPayPercent(BigDecimal noPayPercent) {
        this.noPayPercent = noPayPercent;
    }

    public Integer getShouldPayNum() {
        return shouldPayNum;
    }

    public void setShouldPayNum(Integer shouldPayNum) {
        this.shouldPayNum = shouldPayNum;
    }

    public Integer getPayedNum() {
        return payedNum;
    }

    public void setPayedNum(Integer payedNum) {
        this.payedNum = payedNum;
    }

    public Integer getPartPayedNum() {
        return partPayedNum;
    }

    public void setPartPayedNum(Integer partPayedNum) {
        this.partPayedNum = partPayedNum;
    }

    public BigDecimal getNeedRefundMoney() {
        return needRefundMoney;
    }

    public void setNeedRefundMoney(BigDecimal needRefundMoney) {
        this.needRefundMoney = needRefundMoney;
    }

    public Integer getNeedRefundNum() {
        return needRefundNum;
    }

    public void setNeedRefundNum(Integer needRefundNum) {
        this.needRefundNum = needRefundNum;
    }

    public BigDecimal getActualRefundMoney() {
        return actualRefundMoney;
    }

    public void setActualRefundMoney(BigDecimal actualRefundMoney) {
        this.actualRefundMoney = actualRefundMoney;
    }

    public Integer getActualRefundNum() {
        return actualRefundNum;
    }

    public void setActualRefundNum(Integer actualRefundNum) {
        this.actualRefundNum = actualRefundNum;
    }

    public List<FinanceReport> getChildren() {
        return children;
    }

    public void setChildren(List<FinanceReport> children) {
        this.children = children;
    }
}

package jexx.poi.bean;

import jexx.poi.header.annotation.HeaderCollection;
import jexx.poi.header.annotation.HeaderColumn;

import java.util.ArrayList;
import java.util.List;

/**
 * @author jeff
 * @since 2019/6/17
 */
public class Class {

    @HeaderColumn(value = "班级")
    private String className;

    @HeaderCollection
    private List<Student> students;

    public static List<Class> randomClass(int count){
        List<Class> classList = new ArrayList<>();
        for(int i = 0; i < count; i++){
            Class c = new Class();
            c.setClassName("班级".concat(Integer.toString(i)));
            c.setStudents(Student.randomStudents(2));
            classList.add(c);
        }
        return classList;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public List<Student> getStudents() {
        return students;
    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }
}

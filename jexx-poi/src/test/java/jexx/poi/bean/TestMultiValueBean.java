package jexx.poi.bean;

import jexx.random.RandomUtil;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class TestMultiValueBean {

    private String[] as;
    private List<String> bs;
    private Set<String> cs;
    private List<Integer> ds;
    private Integer[] es;
    private Integer departmentId;
    private Integer[] positionIds;

    public static TestMultiValueBean randomOne(){
        TestMultiValueBean bean = new TestMultiValueBean();

        String[] as = new String[2];
        for (int i = 0; i < as.length; i++){
            as[i]= RandomUtil.randomAlphaNumeric(2);
        }

        List<String> bs = new ArrayList<>();
        for (int i = 0; i < 3; i++){
            bs.add(RandomUtil.randomAlphaNumeric(2));
        }

        Set<String> cs = new HashSet<>();
        for (int i = 0; i < 4; i++){
            cs.add(RandomUtil.randomAlphaNumeric(2));
        }

        bean.setAs(as);
        bean.setBs(bs);
        bean.setCs(cs);
        return bean;
    }

    public static List<TestMultiValueBean> random(){
        List<TestMultiValueBean> list = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            list.add(randomOne());
        }
        return list;
    }

    public String[] getAs() {
        return as;
    }

    public void setAs(String[] as) {
        this.as = as;
    }

    public List<String> getBs() {
        return bs;
    }

    public void setBs(List<String> bs) {
        this.bs = bs;
    }

    public Set<String> getCs() {
        return cs;
    }

    public void setCs(Set<String> cs) {
        this.cs = cs;
    }

    public List<Integer> getDs() {
        return ds;
    }

    public void setDs(List<Integer> ds) {
        this.ds = ds;
    }

    public Integer[] getEs() {
        return es;
    }

    public void setEs(Integer[] es) {
        this.es = es;
    }

    public Integer getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(Integer departmentId) {
        this.departmentId = departmentId;
    }

    public Integer[] getPositionIds() {
        return positionIds;
    }

    public void setPositionIds(Integer[] positionIds) {
        this.positionIds = positionIds;
    }

}

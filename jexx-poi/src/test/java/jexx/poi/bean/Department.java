package jexx.poi.bean;

import jexx.poi.meta.aspect.ExcelMeta;
import jexx.poi.meta.aspect.ExcelMetaChildren;
import jexx.poi.meta.aspect.ExcelMetaLabel;
import jexx.poi.meta.aspect.ExcelMetaValue;
import jexx.random.RandomUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

@ExcelMeta
public class Department {

    @ExcelMetaValue
    private Integer id;
    @ExcelMetaLabel
    private String name;
    @ExcelMetaChildren
    private List<Position> positions;

    public static Department randomDepartment(int id){
        Department department = new Department();
        department.id = id;
        department.name = "部门&".concat(id%3==0?" ":"").concat(Integer.toString(id));
        department.positions = id == 3 ? new ArrayList<>() : Position.randomPositions(5, department);
        department.positions = Position.randomPositions(5, department);
        return department;
    }

    public static List<Department> randomDepartments(int count){
        List<Department> departments = new ArrayList<>();
        for(int i = 0; i < count; i++){
            departments.add(randomDepartment(i+1));
        }
        return departments;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Position> getPositions() {
        return positions;
    }

    public void setPositions(List<Position> positions) {
        this.positions = positions;
    }
}

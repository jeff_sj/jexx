package jexx.poi.bean;

import jexx.poi.meta.aspect.ExcelMeta;
import jexx.poi.meta.aspect.ExcelMetaChildren;
import jexx.poi.meta.aspect.ExcelMetaLabel;
import jexx.poi.meta.aspect.ExcelMetaValue;
import jexx.random.RandomUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

@ExcelMeta
public class Position {
    @ExcelMetaValue
    private Integer id;
    @ExcelMetaLabel
    private String name;

    private Integer departmentId;
    private Department department;

    @ExcelMetaChildren
    private List<Employee> employees;

    public static Position randomPosition(int id, Department department){
        Position position = new Position();
        position.id = id;
        position.name = "职位".concat(id%3==0?"*":"").concat(Integer.toString(department.getId()*10+id));
        position.departmentId =  department.getId();
        position.department = department;
        position.employees = Employee.randomEmployees(5, department, position);
        if(id % 2 == 0){
            position.employees = new ArrayList<>();
        }
        return position;
    }

    public static List<Position> randomPositions(int count, Department department){
        List<Position> positions = new ArrayList<>();
        for(int i = 0; i < count; i++){
            positions.add(randomPosition(i+1, department));
        }
        return positions;
    }

    public static List<Position> randomPositions(int count){
        Department department = new Department();
        department.setId(0);
        return randomPositions(count, department);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(Integer departmentId) {
        this.departmentId = departmentId;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public List<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }
}

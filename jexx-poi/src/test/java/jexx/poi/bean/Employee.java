package jexx.poi.bean;

import jexx.poi.header.annotation.HeaderColumn;
import jexx.poi.header.annotation.HeaderTable;
import jexx.poi.meta.aspect.ExcelMeta;
import jexx.poi.meta.aspect.ExcelMetaLabel;
import jexx.poi.meta.aspect.ExcelMetaValue;
import jexx.util.CollectionUtil;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

@ExcelMeta
@HeaderTable(name = "Employee")
public class Employee {
    @ExcelMetaValue
    @HeaderColumn(value = "i18.employeeNo")
    private Integer employeeNo;
    @ExcelMetaLabel
    @HeaderColumn(value = "姓名")
    private String employeeName;
    @HeaderColumn(value = "年龄")
    private Integer age;
    @HeaderColumn(value = "性别", meta = "sex", valid = false)
    private Boolean sex;
    @HeaderColumn(value = "出生日期", dataCellStyle = "date")
    private Date birthdate;
    @HeaderColumn(value = "百分比", dataCellStyle = "percent")
    private Float percent;
    @HeaderColumn(value = "部门", meta = "department")
    private Integer departmentId;
    private Department department;
    @HeaderColumn(value = "职位", meta = "department", referKey = "departmentId")
    private Integer positionId;
    private Position position;
    private List<String> hobbies;
    @HeaderColumn(value = "上司", meta = "employees")
    private Integer lastEmployeeId;

    private Map<String, Integer> results = new HashMap<>();


    public static Employee randomEmployee(int id, Department department, Position position){
        Employee employee = new Employee();
        employee.employeeNo = id;
        employee.employeeName = "名字".concat(id%2==0?"$":"").concat(Integer.toString(department.getId()*100 + position.getId()*10 + id));
        employee.age = ThreadLocalRandom.current().nextBoolean() ? id : null;
        employee.sex = ThreadLocalRandom.current().nextBoolean();
        employee.birthdate = ThreadLocalRandom.current().nextInt(10) == 5 ? null : new Date();
        employee.percent = ThreadLocalRandom.current().nextFloat();
        employee.departmentId = department.getId();
        employee.department = department;
        employee.positionId = position.getId();
        employee.position = position;
        employee.hobbies = CollectionUtil.list("钓鱼", "爬山");
        employee.lastEmployeeId = 1;

        employee.getResults().put("c1", id);
        employee.getResults().put("c2", id+1);

        return employee;
    }

    public static List<Employee> randomEmployees(int count, Department department, Position position){
        List<Employee> employees = new ArrayList<>();
        for(int i = 0; i < count; i++){
            employees.add(randomEmployee(i+1, department, position));
        }
        return employees;
    }

    public static List<Employee> randomEmployees(int count){
        Department department = new Department();
        department.setId(1);
        Position position = new Position();
        position.setDepartmentId(1);
        position.setDepartment(department);
        position.setId(1);
        return randomEmployees(count, department, position);
    }

    public Integer getEmployeeNo() {
        return employeeNo;
    }

    public void setEmployeeNo(Integer employeeNo) {
        this.employeeNo = employeeNo;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Boolean getSex() {
        return sex;
    }

    public void setSex(Boolean sex) {
        this.sex = sex;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    public Integer getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(Integer departmentId) {
        this.departmentId = departmentId;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public Integer getPositionId() {
        return positionId;
    }

    public void setPositionId(Integer positionId) {
        this.positionId = positionId;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public List<String> getHobbies() {
        return hobbies;
    }

    public void setHobbies(List<String> hobbies) {
        this.hobbies = hobbies;
    }

    public Integer getLastEmployeeId() {
        return lastEmployeeId;
    }

    public void setLastEmployeeId(Integer lastEmployeeId) {
        this.lastEmployeeId = lastEmployeeId;
    }

    public Map<String, Integer> getResults() {
        return results;
    }

    public Float getPercent() {
        return percent;
    }

    public void setPercent(Float percent) {
        this.percent = percent;
    }
}



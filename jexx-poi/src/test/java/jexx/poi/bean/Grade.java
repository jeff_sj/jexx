package jexx.poi.bean;

import jexx.poi.header.annotation.HeaderCollection;
import jexx.poi.header.annotation.HeaderColumn;
import jexx.poi.header.annotation.HeaderTable;

import java.util.ArrayList;
import java.util.List;

/**
 * @author jeff
 * @since 2019/6/17
 */
@HeaderTable(name = "grade")
public class Grade {

    @HeaderColumn(value = "年级")
    private String gradeName;

    @HeaderCollection
    private List<Class> classList;

    public static List<Grade> randomClass(int count){
        List<Grade> gradeList = new ArrayList<>();
        for(int i = 0; i < count; i++){
            Grade c = new Grade();
            c.setGradeName("年级".concat(Integer.toString(i)));
            c.setClassList(Class.randomClass(2));
            gradeList.add(c);
        }
        return gradeList;
    }

    public String getGradeName() {
        return gradeName;
    }

    public void setGradeName(String gradeName) {
        this.gradeName = gradeName;
    }

    public List<Class> getClassList() {
        return classList;
    }

    public void setClassList(List<Class> classList) {
        this.classList = classList;
    }
}

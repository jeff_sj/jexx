package jexx.poi.bean;

import jexx.poi.header.annotation.HeaderColumn;
import jexx.poi.header.annotation.HeaderTable;
import jexx.random.RandomUtil;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

/**
 * @author jeff
 * @since 2019/6/17
 */
@HeaderTable
public class Student {

    @HeaderColumn(value = "学生")
    private String name;

    @HeaderColumn(value = "学号")
    private String studentNo;

    @HeaderColumn(value = "a")
    private Integer a;
    @HeaderColumn(value = "b")
    private Integer b;
    @HeaderColumn(value = "c")
    private Integer c;
    @HeaderColumn(value = "d")
    private Integer d;
    @HeaderColumn(value = "e")
    private Integer e;

    public static List<Student> randomStudents(int count){
        List<Student> list = new ArrayList<>();
        for(int i = 0; i < count; i++){
            Student student = new Student();
            student.setName(RandomUtil.randomAscii(4));
            student.setStudentNo(RandomUtil.randomNumeric(16));
            student.setA(ThreadLocalRandom.current().nextInt(100));
            student.setB(ThreadLocalRandom.current().nextInt(100));
            student.setC(ThreadLocalRandom.current().nextInt(100));
            student.setD(ThreadLocalRandom.current().nextInt(100));
            student.setE(ThreadLocalRandom.current().nextInt(100));
            list.add(student);
        }
        return list;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getA() {
        return a;
    }

    public void setA(Integer a) {
        this.a = a;
    }

    public Integer getB() {
        return b;
    }

    public void setB(Integer b) {
        this.b = b;
    }

    public Integer getC() {
        return c;
    }

    public void setC(Integer c) {
        this.c = c;
    }

    public Integer getD() {
        return d;
    }

    public void setD(Integer d) {
        this.d = d;
    }

    public Integer getE() {
        return e;
    }

    public void setE(Integer e) {
        this.e = e;
    }

    public String getStudentNo() {
        return studentNo;
    }

    public void setStudentNo(String studentNo) {
        this.studentNo = studentNo;
    }
}

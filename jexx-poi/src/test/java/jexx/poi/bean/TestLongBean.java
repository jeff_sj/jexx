package jexx.poi.bean;

public class TestLongBean {

    private long a;
    private String b;

    public long getA() {
        return a;
    }

    public void setA(long a) {
        this.a = a;
    }

    public String getB() {
        return b;
    }

    public void setB(String b) {
        this.b = b;
    }
}

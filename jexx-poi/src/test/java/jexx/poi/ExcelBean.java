package jexx.poi;

public class ExcelBean {
    private String name;
    private Integer age;
    private Boolean sex;
    private int country;
    private int province;
    private int area;
    private String gradeName;

    public ExcelBean() {
    }

    public ExcelBean(String name, Integer age, Boolean sex, int country, int province, int area, String gradeName) {
        this.name = name;
        this.age = age;
        this.sex = sex;
        this.country = country;
        this.province = province;
        this.area = area;
        this.gradeName = gradeName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Boolean getSex() {
        return sex;
    }

    public void setSex(Boolean sex) {
        this.sex = sex;
    }

    public int getCountry() {
        return country;
    }

    public void setCountry(int country) {
        this.country = country;
    }

    public int getProvince() {
        return province;
    }

    public void setProvince(int province) {
        this.province = province;
    }

    public int getArea() {
        return area;
    }

    public void setArea(int area) {
        this.area = area;
    }

    public String getGradeName() {
        return gradeName;
    }

    public void setGradeName(String gradeName) {
        this.gradeName = gradeName;
    }
}

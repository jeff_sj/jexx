package jexx.poi;

import jexx.io.FileUtil;
import jexx.poi.util.*;
import jexx.random.RandomUtil;
import jexx.util.Console;
import jexx.util.ResourceUtil;
import jexx.util.StringUtil;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

/**
 * @author jeff
 * @since 2019/6/20
 */
//@Ignore
public class PoiTest {

    protected String baseDir;

    @Before
    public void setUp(){
        URL data = ResourceUtil.getResource("poi");
        baseDir = data.getFile().concat("/").concat(PoiTest.class.getSimpleName());
        FileUtil.mkdirs(baseDir);
        Console.log("baseDir={}", baseDir);
    }

    @Test
    public void test() throws Exception {
        Workbook workbook  = new org.apache.poi.hssf.usermodel.HSSFWorkbook();
        Sheet sheet = workbook.createSheet();

        CellStyle cellStyle = workbook.createCellStyle();
        cellStyle.setDataFormat(workbook.createDataFormat().getFormat("0\\.00"));

        for(int i = 0; i < 89; i++){
            Row row = sheet.createRow(i);
            Cell cell = row.createCell(0);
            cell.setCellStyle(cellStyle);
            cell.setCellType(CellType.NUMERIC);
            cell.setCellValue(10000);
        }

        sheet.autoSizeColumn(0);
        OutputStream outputStream = FileUtil.newBufferedOutputStream(FileUtil.file(baseDir, "a.xls"));
        workbook.write(outputStream);
        outputStream.flush();
        workbook.close();
    }

    /**
     * HSSFCell 设置单元格类型会报错
     */
    @Test
    public void testSetCellType() throws IOException {
        InputStream inputStream = ResourceUtil.getStream("testSetCellType.xls");
        Assert.assertNotNull(inputStream);
        Workbook workbook  = new org.apache.poi.hssf.usermodel.HSSFWorkbook(inputStream);
        Sheet sheet = workbook.getSheet("Sheet1");
        Row row = sheet.getRow(0);
        Cell cell = row.getCell(0);

        try {
            cell.setCellType(CellType.NUMERIC);
        }
        catch (IllegalStateException e){
            //skip
        }

        Assert.assertEquals(CellType.STRING, cell.getCellTypeEnum());

        //塞入数字,单元格类型自动变化
        cell.setCellValue(111);
        Assert.assertEquals(CellType.NUMERIC, cell.getCellTypeEnum());

        OutputStream outputStream = FileUtil.newBufferedOutputStream(FileUtil.file(baseDir, "testSetCellType.xls"));
        workbook.write(outputStream);
        outputStream.flush();
        workbook.close();
    }

    @Test
    public void testSXSSFWorkbook(){
        SXSSFWorkbook wb = new SXSSFWorkbook(100);
    }

    /**
     * 数据有效性约束过多，会导致打开excel后约束失效
     */
    @Test
    public void testWriteValidateCell() throws Exception {
        Workbook workbook  = new org.apache.poi.hssf.usermodel.HSSFWorkbook();

        Sheet metaSheet = workbook.createSheet("meta");
        Row row1 = metaSheet.createRow(0);
        List<String> dlist = new ArrayList<>();
        for (int j = 0; j < 255; j++) {
            dlist.add(RandomUtil.randomChinese(4));
        }
        String metaName = "m1";
        RowOperationUtil.writeMeta(row1, metaName, dlist);

        Name name = workbook.createName();
        String nameName = NameUtil.encodeNameName(metaName);
        name.setNameName(nameName);
        String formula = StringUtil.format("{}!${}${}:${}${}", metaSheet.getSheetName(), "B", 1,
                CellOperateUtil.toColumnLabel(100), 1);
        name.setRefersToFormula(formula);

        Sheet sheet = workbook.createSheet();

        for(int i = 0; i < 100; i++){
            Row row = sheet.createRow(i);
            for (int j = 0; j < 100; j++) {
                Cell cell = row.createCell(j);
                cell.setCellValue(dlist.get(ThreadLocalRandom.current().nextInt(255)));

                String fla = StringUtil.format("INDIRECT(\"{}\")", nameName);
                DataValidation dataValidation = DataValidationUtil.createValidation(sheet, fla, i+1, i+1, j+1, j+1);
                sheet.addValidationData(dataValidation);
            }
        }

//        String fla = StringUtil.format("INDIRECT(\"{}\")", nameName);
//        DataValidation dataValidation = DataValidationUtil.createValidation(sheet, fla, 1, 1000, 1, 100);
//        sheet.addValidationData(dataValidation);

        OutputStream outputStream = FileUtil.newBufferedOutputStream(FileUtil.file(baseDir, "testWriteValidateCell.xls"));
        workbook.write(outputStream);
        outputStream.flush();
        workbook.close();
    }

    @Test
    public void testFormulaIncrease() throws Exception {
        boolean xlsx = false;
        Workbook workbook  = WorkbookUtil.createBook(xlsx);;

        Sheet sheet = workbook.createSheet();

        // HSSF格式：以 C3 单元格为例, 如果公式为 $F1 , 那么实际公示为 $F3; 如果公式为 $F2, 那么实际为 $F4;
        // hssf 会递增
        // XSSF格式：以 C3 单元格为例, 如果公式为 $F1 , 那么实际公示为 $F1; 如果公式为 $F2, 那么实际为 $F2
        DataValidation dataValidation = DataValidationUtil.createValidation(sheet, "$F3", 3, 4,
                3, 3);
        sheet.addValidationData(dataValidation);

        for (int i = 0; i < 4; i++){
            Row row = sheet.createRow(i);
            Cell cell = row.createCell(2);

            CellStyle cellStyle = workbook.createCellStyle();
            cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            cellStyle.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.index);
            cell.setCellStyle(cellStyle);
        }

        OutputStream outputStream = FileUtil.newBufferedOutputStream(FileUtil.file(baseDir, "testFormulaIncrease.".concat(xlsx ? "xlsx" : "xls")));
        workbook.write(outputStream);
        outputStream.flush();
        workbook.close();
    }

}

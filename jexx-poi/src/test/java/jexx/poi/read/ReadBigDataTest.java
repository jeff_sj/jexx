//package jexx.poi.read;
//
//import com.monitorjbl.xlsx.StreamingReader;
//import jexx.io.FileUtil;
//import jexx.poi.ExcelReader;
//import jexx.poi.SheetReader;
//import jexx.poi.header.Headers;
//import jexx.poi.meta.Metas;
//import jexx.time.TimeInterval;
//import jexx.util.Assert;
//import jexx.util.Console;
//import org.apache.poi.ooxml.util.SAXHelper;
//import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
//import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
//import org.apache.poi.openxml4j.opc.OPCPackage;
//import org.apache.poi.ss.usermodel.Workbook;
//import org.apache.poi.xssf.eventusermodel.XSSFReader;
//import org.apache.poi.xssf.model.SharedStringsTable;
//import org.junit.Test;
//import org.xml.sax.*;
//
//import javax.xml.parsers.ParserConfigurationException;
//import java.io.File;
//import java.io.FileInputStream;
//import java.io.IOException;
//import java.io.InputStream;
//import java.util.List;
//
//public class ReadBigDataTest {
//
//    @Test
//    public void readData1() throws IOException, InvalidFormatException {
//        TimeInterval timeInterval = new TimeInterval();
//        timeInterval.start("读取文件");
//
//        File file = FileUtil.file("C:\\Users\\kxys4\\Desktop\\skuList_big.xlsx");
//        InputStream is = new FileInputStream(file);
//        Workbook workbook = StreamingReader.builder()
//                .rowCacheSize(100)    // number of rows to keep in memory (defaults to 10)
//                .bufferSize(4096)     // buffer size to use when reading InputStream to file (defaults to 1024)
//                .open(is);            // InputStream or File for XLSX file (required)
//
//        ExcelReader excelReader = new ExcelReader(workbook);
//        timeInterval.stop();
//
//        timeInterval.start("读取meta");
//        Metas metas = excelReader.getMetas();
//        timeInterval.stop();
//        Headers headers = metas.getHeadersByName(Headers.DEFAULT_HEADERS);
//        Assert.notNull(headers, "文件格式不匹配,请导出SKU模板再导入");
//
//        timeInterval.start("读取数据");
//        SheetReader sheetReader = excelReader.sheet();
//        sheetReader.skipRows(1);
//        sheetReader.setIsSimpleBean(false);
//        List<ProductSkuForExcel> excelList = sheetReader.readBeanRows(ProductSkuForExcel.class, headers);
//        timeInterval.stop();
//        Assert.notEmpty(excelList);
//        Console.log("导入sku,共{}条数据", excelList.size());
//        Console.log(timeInterval.prettyPrint());
//    }
//
//    @Test
//    public void readData2() throws IOException, OpenXML4JException, SAXException, ParserConfigurationException {
//        OPCPackage pkg = OPCPackage.open("C:\\Users\\kxys4\\Desktop\\skuList_big.xlsx");
//        XSSFReader r = new XSSFReader(pkg);
//        SharedStringsTable sst = r.getSharedStringsTable();
//
//        XMLReader parser = SAXHelper.newXMLReader();
//        parser.setContentHandler(new DefaultContentHandler());
//
//        XSSFReader.SheetIterator sheets = (XSSFReader.SheetIterator) r.getSheetsData();
//        while (sheets.hasNext()) { //遍历sheet
//            InputStream sheet = sheets.next(); //sheets.next()和sheets.getSheetName()不能换位置，否则sheetName报错
//            String sheetName = sheets.getSheetName();
//            Console.log(sheetName);
//            InputSource sheetSource = new InputSource(sheet);
//            parser.parse(sheetSource); //解析excel的每条记录，在这个过程中 startElement()、characters()、endElement()这三个函数会依次执行
//            sheet.close();
//        }
//
//    }
//
//    private static class DefaultContentHandler implements ContentHandler{
//        @Override
//        public void setDocumentLocator(Locator locator) {
//
//        }
//
//        @Override
//        public void startDocument() throws SAXException {
//
//        }
//
//        @Override
//        public void endDocument() throws SAXException {
//
//        }
//
//        @Override
//        public void startPrefixMapping(String prefix, String uri) throws SAXException {
////            Console.log("prefix={},uri={}", prefix, uri);
//        }
//
//        @Override
//        public void endPrefixMapping(String prefix) throws SAXException {
////            Console.log("prefix={}", prefix);
//        }
//
//        @Override
//        public void startElement(String uri, String localName, String qName, Attributes atts) throws SAXException {
////            Console.log("uri={}, localName={}, qName={}, atts={}", localName, qName, atts);
//        }
//
//        @Override
//        public void endElement(String uri, String localName, String qName) throws SAXException {
////            Console.log("uri={},localName={},qName={}", uri, localName, qName);
//        }
//
//        @Override
//        public void characters(char[] ch, int start, int length) throws SAXException {
//
//        }
//
//        @Override
//        public void ignorableWhitespace(char[] ch, int start, int length) throws SAXException {
//
//        }
//
//        @Override
//        public void processingInstruction(String target, String data) throws SAXException {
////            Console.log("target={}, data={}", target, data);
//        }
//
//        @Override
//        public void skippedEntity(String name) throws SAXException {
////            Console.log("name={}", name);
//        }
//    }
//
//}

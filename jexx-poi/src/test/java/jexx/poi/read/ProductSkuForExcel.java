package jexx.poi.read;

import jexx.poi.header.annotation.HeaderTable;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

@HeaderTable
public class ProductSkuForExcel {

    private String model;

    private String code;

    private Map<String, String> attributeValueMap;

    private BigDecimal basePrice;

    private Date startTime;

    private String countryOrigin;

    private String tariffNo;

    private BigDecimal weight;

    private Date endTime;

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Map<String, String> getAttributeValueMap() {
        return attributeValueMap;
    }

    public void setAttributeValueMap(Map<String, String> attributeValueMap) {
        this.attributeValueMap = attributeValueMap;
    }

    public BigDecimal getBasePrice() {
        return basePrice;
    }

    public void setBasePrice(BigDecimal basePrice) {
        this.basePrice = basePrice;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public String getCountryOrigin() {
        return countryOrigin;
    }

    public void setCountryOrigin(String countryOrigin) {
        this.countryOrigin = countryOrigin;
    }

    public String getTariffNo() {
        return tariffNo;
    }

    public void setTariffNo(String tariffNo) {
        this.tariffNo = tariffNo;
    }

    public BigDecimal getWeight() {
        return weight;
    }

    public void setWeight(BigDecimal weight) {
        this.weight = weight;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }
}

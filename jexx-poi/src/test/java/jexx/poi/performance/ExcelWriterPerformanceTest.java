package jexx.poi.performance;

import jexx.io.FileUtil;
import jexx.poi.ExcelWriter;
import jexx.poi.SheetWriter;
import jexx.poi.bean.Department;
import jexx.poi.header.Headers;
import jexx.poi.meta.ArrayMeta;
import jexx.poi.meta.Metas;
import jexx.poi.meta.TreeMeta;
import jexx.poi.meta.node.INode;
import jexx.poi.meta.node.Node;
import jexx.poi.style.CellStyleSets;
import jexx.poi.style.WrapCellStyle;
import jexx.poi.writer.ExcelBigWriter;
import jexx.random.RandomUtil;
import jexx.time.TimeInterval;
import jexx.util.Console;
import jexx.util.ResourceUtil;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.net.URL;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

/**
 * 模拟大量数据写入excel测试
 */
public class ExcelWriterPerformanceTest {

    protected String baseDir;

    @Before
    public void setUp(){
        URL data = ResourceUtil.getResource("poi");
        baseDir = data.getFile().concat("/").concat(ExcelWriterPerformanceTest.class.getSimpleName());
        FileUtil.mkdirs(baseDir);
        Console.log("baseDir={}", baseDir);
    }

    @Test
    @Ignore
    public void testPerformance(){
        TimeInterval timeInterval = new TimeInterval();

        List<INode> nodes1 = new ArrayList<>();
        for (int i = 0; i < 1000; i++) {
            nodes1.add(new Node(i, RandomUtil.randomAlphaNumeric(6)));
        }

        List<INode> nodes2 = new ArrayList<>();
        for (int i = 0; i < 1000; i++) {
            nodes2.add(new Node(i, RandomUtil.randomAlphaNumeric(6)));
        }

        List<Department> departments = Department.randomDepartments(1000);


        Metas metas = new Metas();
        ArrayMeta meta1 = new ArrayMeta("meta1", nodes1);
        metas.addMeta(meta1);
        ArrayMeta meta2 = new ArrayMeta("meta2", nodes2);
        metas.addMeta(meta2);
        TreeMeta meta3 = TreeMeta.buildMetaWithAnnotation("tree", departments);
        metas.addMeta(meta3);

        WrapCellStyle redCellStyle = CellStyleSets.createCellStyle();
        redCellStyle.setFillForegroundColor(IndexedColors.RED);
        redCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

        int columnNum = 100;
        Headers headers = new Headers();
        for(int j = 0; j < columnNum; j++){
            if(j % 10 == 0){
                headers.addArrayHeader(String.valueOf(j), String.valueOf(j), meta1);
            }
            else if(j % 9 == 0){
                headers.addArrayHeader(String.valueOf(j), String.valueOf(j), meta2);
            }
            else if(j % 8 == 0){
                headers.addTreeHeader(String.valueOf(j), String.valueOf(j), meta3, String.valueOf(j-1));
            }
            else if(j % 7 == 0){
                headers.addTreeHeader(String.valueOf(j), String.valueOf(j), meta3, String.valueOf(j-1));
            }
            else if(j % 6 == 0){
                headers.addTreeHeader(String.valueOf(j), String.valueOf(j), meta3);
            }
            else if(j % 5 == 0){
                headers.addHeader(String.valueOf(j), String.valueOf(j), redCellStyle, CellStyleSets.DATE_CELL_STYLE);
            }
            else{
                headers.addArrayHeader(String.valueOf(j), String.valueOf(j), meta1);
            }
        }

        int rowNum = 1500;
        List<Map<String, Object>> dataList = new ArrayList<>();
        for(int i = 1; i < rowNum; i++){
            Map<String, Object> data = new HashMap<>();
            for(int j = 0; j < columnNum; j++){
                if(j % 10 == 0){
                    data.put(String.valueOf(j), ThreadLocalRandom.current().nextInt(100));
                }
                else if(j % 9 == 0){
                    data.put(String.valueOf(j), ThreadLocalRandom.current().nextInt(100));
                }
                else if(j % 8 == 0){
                    data.put(String.valueOf(j), ThreadLocalRandom.current().nextInt(1,5));
                }
                else if(j % 7 == 0){
                    data.put(String.valueOf(j), ThreadLocalRandom.current().nextInt(1,5));
                }
                else if(j % 6 == 0){
                    data.put(String.valueOf(j), ThreadLocalRandom.current().nextInt(1, 5));
                }
                else if(j % 5 == 0){
                    data.put(String.valueOf(j), new Date());
                }
                else{
                    data.put(String.valueOf(j), ThreadLocalRandom.current().nextInt(100));
                }
            }
            dataList.add(data);
        }

        Console.log("数据构建已完成,耗时 {} s", timeInterval.getTaskTimeSeconds());

        ExcelBigWriter writer = new ExcelBigWriter();
        writer.setHiddenMetaSheet(false);
        writer.createMetaSheet(metas);
        Console.log("meta写入已完成,耗时 {} s", timeInterval.getTaskTimeSeconds());

        SheetWriter sheetWriter = writer.sheet("test1");
        sheetWriter.writeHeader(headers);
        Console.log("header写入已完成,耗时 {} s", timeInterval.getTaskTimeSeconds());

        sheetWriter.writeMapRows(dataList, headers);
        Console.log("data写入已完成,耗时 {} s", timeInterval.getTaskTimeSeconds());

        sheetWriter.flush();
        Console.log("flush写入已完成,耗时 {} s", timeInterval.getTaskTimeSeconds());

        sheetWriter.autoSizeColumn(2);
        Console.log("自动扩展列写入已完成,耗时 {} s", timeInterval.getTaskTimeSeconds());

        writer.flush(FileUtil.file(baseDir, "testPerformance.xlsx"));
        Console.log("文件写入已完成,耗时 {} s", timeInterval.getTaskTimeSeconds());
        writer.close();
    }

    @Test
    @Ignore
    public void testPerformance1(){
        TimeInterval timeInterval = new TimeInterval();

        timeInterval.start("数据构建");
        List<INode> nodes1 = new ArrayList<>();
        for (int i = 0; i < 1000; i++) {
            nodes1.add(new Node(i, RandomUtil.randomAlphaNumeric(6)));
        }

        Metas metas = new Metas();
        ArrayMeta meta1 = new ArrayMeta("meta1", nodes1);
        metas.addMeta(meta1);

        int columnNum = 100;
        Headers headers = new Headers();
        for(int j = 0; j < columnNum; j++){
            headers.addArrayHeader(String.valueOf(j), String.valueOf(j), meta1);
        }

        int rowNum = 100000;
        List<Map<String, Object>> dataList = new ArrayList<>();
        for(int i = 1; i < rowNum; i++){
            Map<String, Object> data = new HashMap<>(columnNum);
            for(int j = 0; j < columnNum; j++){
                data.put(String.valueOf(j), ThreadLocalRandom.current().nextInt(100));
            }
            dataList.add(data);
        }

        timeInterval.stop();
        Console.log("数据构建已完成,耗时 {} s", timeInterval.getLastTaskTimeSeconds());

        timeInterval.start("meta写入");
//        ExcelWriter writer = new ExcelWriter();
        ExcelBigWriter writer = new ExcelBigWriter();
        writer.setHiddenMetaSheet(false);
        writer.createMetaSheet(metas);
        timeInterval.stop();
        Console.log("meta写入已完成,耗时 {} s", timeInterval.getLastTaskTimeSeconds());

        timeInterval.start("header写入");
        SheetWriter sheetWriter = writer.sheet("test1");
        sheetWriter.writeHeader(headers);
        timeInterval.stop();
        Console.log("header写入已完成,耗时 {} s", timeInterval.getLastTaskTimeSeconds());

        timeInterval.start("data写入");
        sheetWriter.writeMapRows(dataList, headers);
        timeInterval.stop();
        Console.log("data写入已完成,耗时 {} s", timeInterval.getLastTaskTimeSeconds());

        timeInterval.start("flush");
        sheetWriter.flush();
        timeInterval.stop();
        Console.log("flush写入已完成,耗时 {} s", timeInterval.getLastTaskTimeSeconds());

        timeInterval.start("文件写入");
        writer.flush(FileUtil.file(baseDir, "testPerformance1.xlsx"));
        writer.close();
        timeInterval.stop();
        Console.log("文件写入已完成,耗时 {} s", timeInterval.getLastTaskTimeSeconds());
    }

}

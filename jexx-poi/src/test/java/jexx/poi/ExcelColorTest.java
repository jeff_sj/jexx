package jexx.poi;

import jexx.io.FileUtil;
import jexx.poi.cell.SingleCell;
import jexx.poi.style.WrapCellStyle;
import jexx.poi.writer.impl.SheetWriterImpl;
import jexx.util.ResourceUtil;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.net.URL;

/**
 * @author jeff
 * @since 2019/5/13
 */
public class ExcelColorTest {

    protected String baseDir;

    @Before
    public void setUp(){
        URL data = ResourceUtil.getResource("poi");
        baseDir = data.getFile();
    }

    @Ignore
    @Test
    public void writeIndexColors(){
        ExcelWriter writer = new ExcelWriter();
        SheetWriter sheetWriter = writer.sheet("test");

        IndexedColors.values();

        for (int k = 0; k < IndexedColors.values().length; k++) {
            IndexedColors colors = IndexedColors.values()[k];
            int rowNum = k/4;
            int colNum = k%4;

            sheetWriter.writeCell(new SingleCell(rowNum+1, 2*colNum+1, colors));

            WrapCellStyle cellStyle = new WrapCellStyle();
            cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            cellStyle.setFillForegroundColor(colors);
            sheetWriter.writeCell(new SingleCell(rowNum+1, 2*colNum+2, null, cellStyle));
        }
        sheetWriter.flush();
        writer.flush(FileUtil.file(baseDir, "color.xlsx"));
        writer.close();

    }

}

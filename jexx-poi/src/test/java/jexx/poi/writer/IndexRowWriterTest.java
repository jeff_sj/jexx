package jexx.poi.writer;

import jexx.poi.cell.IMergedCell;
import jexx.poi.header.Headers;
import jexx.poi.row.DataRow;
import jexx.poi.row.Row;
import jexx.poi.row.RowContext;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * @author jeff
 * @since 2019/5/22
 */
public class IndexRowWriterTest {

    @Test
    public void testToRow(){
        IndexRowWriter indexRowWriter = new IndexRowWriter();

        TestBean bean = new TestBean();
        bean.setA("1_a");
        bean.setB(1);

        TestBean bean1 = new TestBean();
        bean1.setA("2_a1");
        bean1.setB(21);
        bean.getC().add(bean1);

        TestBean bean2 = new TestBean();
        bean2.setA("2_a2");
        bean2.setB(22);
        bean.getC().add(bean2);

        Headers headers = new Headers("test");
        headers.addHeader("a", "a标签");
        headers.addHeader("b", "b标签");
        headers.addHeader("c[].a", "ca标签");
        headers.addHeader("c[].b", "cb标签");

        int startRowNum = 10;
        RowContext rowContext = indexRowWriter.toRow(startRowNum, bean, headers, 9, false);
        DataRow row = rowContext.getRootRow();
        Assert.assertNotNull(row);
        Assert.assertEquals("", row.getPath());
        Assert.assertEquals(10, row.getStartRowNum());
        Assert.assertEquals(11, row.getEndRowNum());
        List<IMergedCell> cells = row.getCells();
        Assert.assertEquals(2, cells.size());
        IMergedCell cell = cells.get(0);
        Assert.assertEquals("1_a", cell.getValue());
        cell = cells.get(1);
        Assert.assertEquals(1, cell.getValue());

        List<Row> children = row.getChildren();
        Assert.assertEquals(2, children.size());
        DataRow dataRow = (DataRow)children.get(0);
        Assert.assertNotNull(dataRow);
        Assert.assertEquals("c[0]", dataRow.getPath());
        Assert.assertEquals(startRowNum, dataRow.getStartRowNum());
        Assert.assertEquals(startRowNum, dataRow.getEndRowNum());

        dataRow = (DataRow)children.get(1);
        Assert.assertEquals("c[1]", dataRow.getPath());
        Assert.assertEquals(startRowNum+1, dataRow.getStartRowNum());
        Assert.assertEquals(startRowNum+1, dataRow.getEndRowNum());
    }

    private static class TestBean{

        private String a;
        private Integer b;
        private List<TestBean> c = new ArrayList<>();

        public String getA() {
            return a;
        }

        public void setA(String a) {
            this.a = a;
        }

        public Integer getB() {
            return b;
        }

        public void setB(Integer b) {
            this.b = b;
        }

        public List<TestBean> getC() {
            return c;
        }

        public void setC(List<TestBean> c) {
            this.c = c;
        }
    }

}

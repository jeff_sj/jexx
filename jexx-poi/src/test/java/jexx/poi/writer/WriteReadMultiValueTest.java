package jexx.poi.writer;

import jexx.io.FileUtil;
import jexx.poi.ExcelReader;
import jexx.poi.ExcelWriter;
import jexx.poi.SheetReader;
import jexx.poi.SheetWriter;
import jexx.poi.bean.Department;
import jexx.poi.bean.TestMultiValueBean;
import jexx.poi.header.ArrayDataHeader;
import jexx.poi.header.DefaultDataHeader;
import jexx.poi.header.Headers;
import jexx.poi.header.TreeDataHeader;
import jexx.poi.header.annotation.HeaderColumn;
import jexx.poi.header.annotation.HeaderMeta;
import jexx.poi.header.annotation.HeaderTable;
import jexx.poi.header.factory.HeaderColumnSelectMode;
import jexx.poi.meta.ArrayMeta;
import jexx.poi.meta.Metas;
import jexx.poi.meta.TreeMeta;
import jexx.poi.meta.node.Node;
import jexx.util.ClassUtil;
import jexx.util.CollectionUtil;
import jexx.util.Console;
import jexx.util.ResourceUtil;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class WriteReadMultiValueTest {

    protected String baseDir;

    @Before
    public void setUp(){
        URL data = ResourceUtil.getResource("poi");
        baseDir = data.getFile().concat("/").concat(WriteReadMultiValueTest.class.getSimpleName());
        FileUtil.mkdirs(baseDir);
        Console.log("baseDir={}", baseDir);
    }

    @Test
    public void testWriteMultiValues(){
        ExcelWriter writer = new ExcelWriter(false);
        writer.setHiddenMetaSheet(false);

        Metas metas = new Metas();

        ArrayMeta meta1 = new ArrayMeta("sex", CollectionUtil.list(
                new Node(0, "女"),
                new Node(1, "男"),
                new Node(2, "未知")
        ));
        metas.addMeta(meta1);

        List<Department> departments = Department.randomDepartments(5);
        TreeMeta meta2 = TreeMeta.buildMetaWithAnnotation("部门", departments);
        metas.addMeta(meta2);

        List<TestMultiValueBean> list = new ArrayList<>();

        TestMultiValueBean bean1 = new TestMultiValueBean();
        bean1.setAs(new String[]{"a1", "a2"});
        bean1.setBs(CollectionUtil.list("b1", "b2"));
        Set<String> set1 = new HashSet<>();
        set1.add("c1");
        set1.add("c2");
        bean1.setCs(set1);
        bean1.setDs(CollectionUtil.list(1, 2));
        bean1.setEs(new Integer[]{1,2});
        bean1.setDepartmentId(1);
        bean1.setPositionIds(new Integer[]{1,2});
        list.add(bean1);

        writer.createMetaSheet(metas);

        Headers headers = new Headers("test");
        headers.addHeader(new DefaultDataHeader("as", "as").withMultiValue(String[].class));
        headers.addHeader(new DefaultDataHeader("bs", "bs").withMultiValue(List.class, String.class));
        headers.addHeader(new DefaultDataHeader("cs", "cs").withMultiValue(Set.class, String.class));
        headers.addHeader(new DefaultDataHeader("ds", "ds").withMultiValue(List.class, Integer.class));
        headers.addHeader(new ArrayDataHeader("es", "es", meta1).withMultiValue(int[].class));
        TreeDataHeader departmentHeader = new TreeDataHeader("departmentId", "departmentId", meta2, null);
        headers.addHeader(departmentHeader);
        headers.addHeader(new TreeDataHeader("positionIds", "positionIds", meta2, departmentHeader).withMultiValue(Integer[].class));
        metas.addHeaders(headers);

        SheetWriter sheetWriter = writer.sheet("Sheet1");
        sheetWriter.setSupportValid(true);

        sheetWriter.writeHeader(headers);
        sheetWriter.writeRows(list, headers);
        sheetWriter.autoSizeColumn(headers);
        File file = FileUtil.file(baseDir, "testWriteMultiValues.xls");
        writer.flush(file);

        ExcelReader excelReader = new ExcelReader(file);
        SheetReader sheetReader = excelReader.sheet();

        sheetReader.skipRows(1);
        TestMultiValueBean readBean1 = sheetReader.readBeanRowAndNext(TestMultiValueBean.class, headers);
        Assert.assertNotNull(readBean1);
        Assert.assertArrayEquals(new String[]{"a1", "a2"}, readBean1.getAs());
        Assert.assertEquals("b1", readBean1.getBs().get(0));
        Assert.assertEquals("b2", readBean1.getBs().get(1));
        Assert.assertEquals(2, readBean1.getCs().size());
        Assert.assertEquals(1, readBean1.getDs().get(0).intValue());
        Assert.assertEquals(2, readBean1.getDs().get(1).intValue());
        Integer[] es = readBean1.getEs();
        Assert.assertEquals(2, es.length);
        Assert.assertEquals(1, es[0].intValue());
        Assert.assertEquals(2, es[1].intValue());
        Assert.assertEquals(1, readBean1.getDepartmentId().intValue());
        Integer[] positionIds = readBean1.getPositionIds();
        Assert.assertEquals(2, positionIds.length);
        Assert.assertEquals(1, positionIds[0].intValue());
        Assert.assertEquals(2, positionIds[1].intValue());
    }

    @Test
    public void testWriteMultiValuesWithHeader() throws ClassNotFoundException {
        Metas metas = new Metas();
        ArrayMeta meta1 = new ArrayMeta("兴趣", CollectionUtil.list(
                new Node(1, "游戏"),
                new Node(2, "爬山"),
                new Node(3, "旅游")
        ));
        metas.addMeta(meta1);

        Headers headers = Headers.newBuild().select(HeaderColumnSelectMode.ONLY_TAG).withMetas(metas).build(Test1Bean.class);
        metas.addHeaders(headers);

        List<Test1Bean> list1 = new ArrayList<>();
        Test1Bean bean11 = new Test1Bean();
        bean11.setId(1);
        Integer[] hobby1 = new Integer[]{1, 2};
        bean11.setHobbies(hobby1);
        bean11.setSex(0);
        bean11.setRepeatedSex(1);
        list1.add(bean11);
        Test1Bean bean12 = new Test1Bean();
        bean12.setId(2);
        Integer[] hobby2 = new Integer[]{3};
        bean12.setHobbies(hobby2);
        bean12.setSex(0);
        bean12.setRepeatedSex(1);
        list1.add(bean12);

        ExcelWriter writer = new ExcelWriter();
        writer.setHiddenMetaSheet(false);
        writer.createMetaSheet(metas);
        SheetWriter sheetWriter = writer.sheet();
        sheetWriter.writeHeader(headers);
        sheetWriter.writeRows(list1, headers);
        File file = FileUtil.file(baseDir, "testWriteMultiValuesWithHeader.xlsx");
        writer.flush(file);

        ExcelReader reader = new ExcelReader(file);
        SheetReader sheetReader = reader.sheet();
        Metas metas2 = reader.getMetas();
        Headers headers2 = metas2.getHeadersByName("test");
        List<Test1Bean> list2 = sheetReader.skipRows(1).readBeanRowsAndNext(Test1Bean.class, headers2);
        Assert.assertEquals(2, list2.size());
        Test1Bean bean21 = list2.get(0);
        Assert.assertNotNull(bean21);
        Assert.assertEquals(1, bean21.getId().intValue());
        Assert.assertArrayEquals(hobby1, bean21.getHobbies());
        Assert.assertEquals(0, bean21.getSex().intValue());
        Assert.assertEquals(1, bean21.getRepeatedSex().intValue());

        Test1Bean bean22 = list2.get(1);
        Assert.assertNotNull(bean22);
        Assert.assertEquals(2, bean22.getId().intValue());
        Assert.assertArrayEquals(hobby2, bean22.getHobbies());
        Assert.assertEquals(0, bean22.getSex().intValue());
        Assert.assertEquals(1, bean22.getRepeatedSex().intValue());
    }

    @HeaderTable(name = "test")
    private static class Test1Bean{
        @HeaderColumn(value = "id")
        private Integer id;

        @HeaderColumn(value = "兴趣", meta = "兴趣", multiValue = true)
        private Integer[] hobbies;

        @HeaderColumn(value = "性别")
        @HeaderMeta(name = "性别", values = {"0", "1"}, valueType = Integer.class, labels = {"女", "男"})
        private Integer sex;

        @HeaderColumn(value = "测试性别", meta = "性别")
        private Integer repeatedSex;

        public Integer getId() {
            return id;
        }
        public void setId(Integer id) {
            this.id = id;
        }

        public Integer[] getHobbies() {
            return hobbies;
        }

        public void setHobbies(Integer[] hobbies) {
            this.hobbies = hobbies;
        }

        public Integer getSex() {
            return sex;
        }

        public void setSex(Integer sex) {
            this.sex = sex;
        }

        public Integer getRepeatedSex() {
            return repeatedSex;
        }

        public void setRepeatedSex(Integer repeatedSex) {
            this.repeatedSex = repeatedSex;
        }
    }

}

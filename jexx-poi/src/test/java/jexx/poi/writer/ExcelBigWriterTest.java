package jexx.poi.writer;

import jexx.io.FileUtil;
import jexx.poi.bean.Department;
import jexx.poi.cell.IMergedCell;
import jexx.poi.cell.MergedCell;
import jexx.poi.header.*;
import jexx.poi.meta.ArrayMeta;
import jexx.poi.meta.Metas;
import jexx.poi.meta.TreeMeta;
import jexx.poi.meta.node.INode;
import jexx.poi.meta.node.Node;
import jexx.poi.row.DataRow;
import jexx.poi.row.RowContext;
import jexx.poi.row.VirtualRow;
import jexx.poi.style.CellStyleSets;
import jexx.util.ResourceUtil;
import org.junit.Before;
import org.junit.Test;

import java.net.URL;
import java.util.*;

/**
 * @author jeff
 * @since 2019/10/6
 */
public class ExcelBigWriterTest {

    protected String baseDir;

    @Before
    public void setUp(){
        URL data = ResourceUtil.getResource("poi");
        baseDir = data.getFile().concat("/").concat(ExcelBigWriterTest.class.getSimpleName());
        FileUtil.mkdirs(baseDir);
    }

    @Test
    public void testWriteIndexBean(){
        List<Department> departments = Department.randomDepartments(10);

        Metas metas = new Metas();

        List<INode> sexNodes = new ArrayList<>();
        sexNodes.add(new Node(true, "男"));
        sexNodes.add(new Node(false, "女"));
        ArrayMeta sexMeta = new ArrayMeta("性别", sexNodes);
        metas.addMeta(sexMeta);

        TreeMeta treeMeta = TreeMeta.buildMetaWithAnnotation("部门", departments);
        metas.addMeta(treeMeta);

        ExcelBigWriter writer = ExcelBigWriter.createBigWriter();

        Headers headers = new Headers("test");
        headers.addSequenceHeader("序号");
        TreeDataHeader departmentHeader = new TreeDataHeader("id", "部门", treeMeta, null);
        headers.addHeader(departmentHeader);
        TreeDataHeader positionHeader = new TreeDataHeader("positions[].id", "职位", treeMeta, departmentHeader);
        headers.addHeader(positionHeader);

        GroupHeader groupHeader = new GroupHeader("员工");
        groupHeader.addHeader(new DefaultDataHeader("positions[].employees[].employeeNo", "工号"));
        groupHeader.addHeader(new DefaultDataHeader("positions[].employees[].employeeName", "姓名"));

        GroupHeader infoGroupHeader = new GroupHeader("信息");
        infoGroupHeader.addHeader(new DefaultDataHeader("positions[].employees[].age", "年龄"));
        infoGroupHeader.addHeader(new ArrayDataHeader("positions[].employees[].sex", "性别", sexMeta));
        groupHeader.addHeader(infoGroupHeader);

        headers.addHeader(groupHeader);

        IDataHeader hobbyHeader = new DefaultDataHeader("positions[].employees[].hobbies[]", "兴趣");
        headers.addHeader(hobbyHeader);
        metas.addHeaders(headers);

        writer.createMetaSheet(metas);

        //row trigger
        headers.addRowTrigger(positionHeader, (RowContext rowContext, DataRow row)-> {
            VirtualRow row1 = rowContext.appendVirtualRowAt(row);
            row1.addCell(new MergedCell(row1, 3, 4, "合计1"));

            IMergedCell cell = new MergedCell(row1, 6, 6, rowContext.sum(row, 6), CellStyleSets.DATA_CELL_STYLE);
            row1.addCell(cell);
        });

        SheetBigWriter sheetWriter = writer.sheet("test1");
        sheetWriter.writeHeader(headers);
        sheetWriter.writeRows(departments, headers);
        writer.flush(FileUtil.file(baseDir, "testWriteIndexBean.xlsx"));
    }

    /**
     * 性能测试
     * 1000*50=2.5s
     * 10000*5=2.5s
     * 10000*10=3.5s
     * 10000*50=8s
     * 10000*100=17s
     */
    @Test
    public void testPerformance(){
        Map<String, String> header = new LinkedHashMap<>();
        for(int j = 0; j < 50; j++){
            header.put(String.valueOf(j), String.valueOf(j));
        }
        List<Map<String, Object>> datas = new ArrayList<>();
        for(int i = 1; i < 10001; i++){
            Map<String, Object> data = new HashMap<>();
            for(int j = 0; j < 50; j++){
                if(j == 1){
                    data.put(String.valueOf(j), new Date());
                }
                else {
                    data.put(String.valueOf(j), "这是测试");
                }
            }
            datas.add(data);
        }

        Headers headers = new Headers("test");
        headers.addHeaders(header);

        ExcelBigWriter writer = new ExcelBigWriter();
        SheetBigWriter sheetWriter = writer.sheet("test1");
        sheetWriter.writeHeader(headers);
        sheetWriter.writeMapRows(datas, headers);
        writer.flush(FileUtil.file(baseDir, "testPerformance.xlsx"));
        writer.close();
    }

}

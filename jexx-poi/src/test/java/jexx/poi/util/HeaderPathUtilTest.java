package jexx.poi.util;

import jexx.util.CollectionUtil;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

/**
 * @author jeff
 * @since 2019/6/20
 */
public class HeaderPathUtilTest {


    @Test
    public void testSplitDataPaths(){
        List<String> list = HeaderPathUtil.splitDataPaths("a[]", "a[0]");
        Assert.assertTrue(checkEqual(list, CollectionUtil.list("a[0]")));

        list = HeaderPathUtil.splitDataPaths("a[].b[]", "a[0].b[0]");
        Assert.assertTrue(checkEqual(list, CollectionUtil.list("a[0]", "a[0].b[0]")));

        list = HeaderPathUtil.splitDataPaths("a[].b[].c", "a[0].b[0].c");
        Assert.assertTrue(checkEqual(list, CollectionUtil.list("a[0]", "a[0].b[0]")));

        list = HeaderPathUtil.splitDataPaths("a[1]", "a[1]");
        Assert.assertTrue(checkEqual(list, CollectionUtil.list()));

        list = HeaderPathUtil.splitDataPaths("a[1].b", "a[1].b");
        Assert.assertTrue(checkEqual(list, CollectionUtil.list()));

        list = HeaderPathUtil.splitDataPaths("a[1].b[].c", "a[1].b[0].c");
        Assert.assertTrue(checkEqual(list, CollectionUtil.list("a[1].b[0]")));

        list = HeaderPathUtil.splitDataPaths("a[].b[1].c", "a[0].b[1].c");
        Assert.assertTrue(checkEqual(list, CollectionUtil.list("a[0]")));
    }

    private boolean checkEqual( List<String> a, List<String> b){
        if(a == null || b == null || a.size() != b.size()){
            return false;
        }
        for(int i = 0; i < a.size(); i++){
            if(a.get(i) == null && b.get(i) == null){
                continue;
            }
            else if(a.get(i) != null && a.get(i).equals(b.get(i))){
                continue;
            }
            return false;
        }
        return true;
    }

}

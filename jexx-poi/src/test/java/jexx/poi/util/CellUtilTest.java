package jexx.poi.util;

import org.junit.Assert;
import org.junit.Test;

public class CellUtilTest {

    @Test
    public void testToColumnIndex(){
        Assert.assertEquals(1, CellOperateUtil.toColumnNum("A"));
        Assert.assertEquals(26, CellOperateUtil.toColumnNum("Z"));
        Assert.assertEquals(27, CellOperateUtil.toColumnNum("AA"));

        Assert.assertEquals("A", CellOperateUtil.toColumnLabel(1));
        Assert.assertEquals("Z", CellOperateUtil.toColumnLabel(26));
        Assert.assertEquals("AA", CellOperateUtil.toColumnLabel(27));
    }

}

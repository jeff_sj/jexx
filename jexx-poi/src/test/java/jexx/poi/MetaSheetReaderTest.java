package jexx.poi;

import jexx.io.FileUtil;
import jexx.poi.header.Headers;
import jexx.poi.meta.ArrayMeta;
import jexx.poi.meta.Metas;
import jexx.poi.meta.node.INode;
import jexx.poi.meta.node.Node;
import jexx.time.DateUtil;
import jexx.util.ResourceUtil;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MetaSheetReaderTest {

    protected String baseDir;

    @Before
    public void setUp(){
        URL data = ResourceUtil.getResource("poi");
        baseDir = data.getFile().concat("/").concat(MetaSheetReaderTest.class.getSimpleName());
        FileUtil.mkdirs(baseDir);
    }

    @Test
    public void testReadMeta(){
        Metas metas = new Metas("1.3.1");
        Date date = DateUtil.parseDateTime("2020-05-26 12:00:01");
        metas.setCreateTime(date);
        metas.addProperty("name", "小明");

        ExcelWriter writer = new ExcelWriter();
        writer.setHiddenMetaSheet(false);
        writer.createMetaSheet(metas);
        File file = FileUtil.file(baseDir, "testReadMeta.xlsx");
        writer.flush(file);
        writer.close();

        ExcelReader reader = new ExcelReader(file);
        Metas metas1 = reader.getMetas();
        Assert.assertNotNull(metas1);
        Assert.assertTrue("版本不一致", reader.matchVersion("1.3.1"));
        Assert.assertEquals("2020-05-26 12:00:01", DateUtil.formatDateTime(metas1.getCreateTime()));
        Assert.assertEquals("小明", metas.getProperty("name"));
    }

    /**
     * 测试meta的名称含有特殊字符的情况下是否异常
     * <p>
     *     1. 含有空格   =====> meta sheet中会去除空格
     * </p>
     */
    @Test
    public void testCheckMetaNameHasCharacter(){
        Metas metas = new Metas();

        List<INode> nodeList = new ArrayList<>();
        nodeList.add(new Node(1, "是"));
        nodeList.add(new Node(0, "否"));

        ArrayMeta arrayMeta1 = new ArrayMeta("住宿 伙食费", nodeList);
        metas.addMeta(arrayMeta1);

        Headers headers = new Headers();
        headers.addArrayHeader("test", "测试", arrayMeta1);
        headers.addHeader("test1", "测试1");
        metas.addHeaders(headers);

        ExcelWriter writer = new ExcelWriter();
        writer.setHiddenMetaSheet(false);
        writer.createMetaSheet(metas);

        SheetWriter sheetWriter = writer.sheet();
        sheetWriter.writeHeader(headers);

        File file = FileUtil.file(baseDir, "testCheckNameContainSpace.xlsx");
        writer.flush(file);
        writer.close();

        ExcelReader reader = new ExcelReader(file);
        Metas metas1 = reader.getMetas();
        Assert.assertNotNull(metas1);
    }

}

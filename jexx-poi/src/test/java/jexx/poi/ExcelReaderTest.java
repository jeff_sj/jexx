package jexx.poi;

import jexx.io.FileUtil;
import jexx.poi.bean.Department;
import jexx.poi.bean.Employee;
import jexx.poi.bean.Position;
import jexx.poi.exception.POIException;
import jexx.poi.header.ArrayDataHeader;
import jexx.poi.header.DefaultDataHeader;
import jexx.poi.header.Headers;
import jexx.poi.meta.ArrayMeta;
import jexx.poi.meta.IMeta;
import jexx.poi.meta.Metas;
import jexx.poi.meta.TreeMeta;
import jexx.poi.meta.node.INode;
import jexx.poi.meta.node.Node;
import jexx.poi.row.RowScanAction;
import jexx.util.Console;
import jexx.util.ResourceUtil;
import jexx.util.StringUtil;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ExcelReaderTest {

    protected String baseDir;

    @Before
    public void setUp(){
        URL data = ResourceUtil.getResource("poi");
        baseDir = data.getFile().concat("/").concat(ExcelReaderTest.class.getSimpleName());
        FileUtil.mkdirs(baseDir);
    }


    @Test
    public void testOneSheet(){
        ExcelReader reader = new ExcelReader(ResourceUtil.getStream("read/test.xlsx"));
        SheetReader sheetReader = reader.oneSheet();
        Assert.assertNotNull(sheetReader);

        try {
            reader = new ExcelReader(ResourceUtil.getStream("read/testOneSheet.xlsx"));
            sheetReader = reader.oneSheet();
            Assert.assertNotNull(sheetReader);
        }
        catch (Exception e){
            Assert.assertTrue(e instanceof POIException);
            Assert.assertEquals("there is not one sheet!", e.getMessage());
        }
    }

    @Test
    public void testReadMetaSheet(){
        ExcelReader reader = new ExcelReader(ResourceUtil.getStream("read/test.xlsx"));
        Metas metas = reader.getMetas();
        Assert.assertNotNull(metas);

        Headers headers = metas.getHeadersByName("test1");
        Assert.assertNotNull(headers);

        //validate sex
        IMeta meta = metas.getMeta("性别");
        Assert.assertTrue(meta instanceof ArrayMeta);
        ArrayMeta arrayMeta = (ArrayMeta)meta;
        INode value = arrayMeta.getNodeByFullLabel("男");
        Assert.assertNotNull(value);
        Assert.assertEquals(true, value.getValue());

        //validate zone
        meta = metas.getMeta("RooT");
        Assert.assertTrue(meta instanceof TreeMeta);
        TreeMeta treeMeta = (TreeMeta)meta;
        value = treeMeta.getNodeByFullLabel("RooT_中国_江苏_苏州");
        Assert.assertNotNull(value);
        Assert.assertEquals(111L, value.getValue());

        //validate header
        SheetReader sheetReader = reader.sheet("test1");
        sheetReader.skipRows(1);
        boolean validate = sheetReader.validateHeaders(headers);
        Assert.assertTrue(validate);

        reader.close();
    }

    @Test
    public void testReaderNullArrayMeta(){
        ExcelReader reader = new ExcelReader(ResourceUtil.getStream("read/testnull.xlsx"));

        Headers headers = new Headers();

        List<INode> sexNodes = new ArrayList<>();
        sexNodes.add(new Node(true, "男"));
        sexNodes.add(new Node(false, "女"));
        ArrayMeta sexMeta = new ArrayMeta("性别", sexNodes);

        headers.addArrayHeader("sex", "性别", sexMeta);
        headers.addHeader("age", "年龄");

        SheetReader sheetReader = reader.sheet();
        List<ExcelBean> beans = sheetReader.jumpToRowNum(2).readBeanRowsAndNext(ExcelBean.class, headers);
        Assert.assertNotNull(beans);

        reader.close();
    }

    @Test
    public void testReadBeanRows(){
        ExcelReader reader = new ExcelReader(ResourceUtil.getStream("read/test.xlsx"));
        Metas metas = reader.getMetas();
        Assert.assertNotNull(metas);

        Headers headers = metas.getHeadersByName("test1");
        Assert.assertNotNull(headers);

        SheetReader sheetReader = reader.sheet("test1");
        sheetReader.skipRows(1);
        sheetReader.passCurrentRow();
        Map<String, Object> data = sheetReader.readMapRowAndNext(headers);
        Console.log(data);
        Assert.assertEquals(111L, data.get("area"));

        sheetReader = reader.sheet("test1");
        sheetReader.skipRows(1);
        sheetReader.passCurrentRow();
        List<ExcelBean> beans = sheetReader.readBeanRowsAndNext(ExcelBean.class, headers);
        Assert.assertNotNull(beans);
        Assert.assertEquals(11, beans.size());
        Assert.assertEquals(111L, beans.get(9).getArea());

        reader.close();
    }

    @Test
    public void testReadBeanRows1(){
        ExcelReader reader = new ExcelReader(ResourceUtil.getStream("read/test.xlsx"));

        Headers headers = new Headers("test");

        headers.addHeader("age", "年龄");
        headers.skipColumns(1);
        headers.addHeader("name", "名称");

        List<INode> valueMetas = new ArrayList<>();
        valueMetas.add(new Node(true, "男"));
        valueMetas.add(new Node(false, "女"));
        ArrayMeta sexMeta = new ArrayMeta("性别", valueMetas);
        headers.addArrayHeader("sex", "性别", sexMeta);

        SheetReader sheetReader = reader.sheet("test1");
        List<ExcelBean> beans = sheetReader.skipRows(1).passCurrentRow().readBeanRowsAndNext(ExcelBean.class, headers);
        Assert.assertNotNull(beans);
        Assert.assertEquals(11, beans.size());
        Assert.assertTrue(beans.get(9).getSex());

        sheetReader = reader.sheet("test1");
        beans = sheetReader.skipRows(1).passCurrentRow().readBeanRowsAndNext(ExcelBean.class, headers, (obj, rowNum) -> {
            if("孙6".equals(obj.getName())){
                return RowScanAction.STOP;
            }
            return RowScanAction.CONTINUE;
        });
        Assert.assertNotNull(beans);
        Assert.assertEquals(5, beans.size());

        reader.close();
    }

    @Test
    public void testEmptyRow(){
        ExcelReader reader = new ExcelReader(ResourceUtil.getStream("read/testEmptyRow.xlsx"));

        Headers headers = new Headers("test");

        headers.addHeader(new DefaultDataHeader("age", "年龄").unwrapLabel(s -> {
            if(s != null && "-".equals(s.toString())){
                return 99;
            }
            return s;
        }));
        headers.skipColumns(1);
        headers.addHeader("name", "名称");

        List<INode> valueMetas = new ArrayList<>();
        valueMetas.add(new Node(true, "男"));
        valueMetas.add(new Node(false, "女"));
        ArrayMeta sexMeta = new ArrayMeta("性别", valueMetas);
        headers.addHeader(new ArrayDataHeader("sex", "性别", sexMeta));

        SheetReader sheetReader = reader.sheet("test1");
        List<ExcelBean> beans = sheetReader.setSkipEmptyRow(true).passCurrentRow().readBeanRowsAndNext(ExcelBean.class, headers);
        Assert.assertNotNull(beans);
        Assert.assertEquals(Integer.valueOf(99), beans.get(0).getAge());
        Assert.assertEquals(11, beans.size());
        Assert.assertTrue(beans.get(9).getSex());

        reader.close();
    }

    @Test
    public void testReadEmployee(){
        ExcelReader reader = new ExcelReader(ResourceUtil.getStream("read/employee.xlsx"));

        Headers headers = reader.getMetas().getHeadersByName("test1");

        SheetReader sheetReader = reader.sheet("test1");
        List<Employee> employees = sheetReader.skipRows(1).readBeanRowsAndNext(Employee.class, headers);
        String format = "employeeNo={},employeeName={},sex={},departmentId={},positionId={},lastEmployeeId={}";

        Employee employee = employees.get(0);
        Assert.assertEquals("employeeNo=1,employeeName=名字111,sex=true,departmentId=1,positionId=1,lastEmployeeId=2",
                StringUtil.format(format, employee.getEmployeeNo(), employee.getEmployeeName(), employee.getSex(), employee.getDepartmentId(), employee.getPositionId(), employee.getLastEmployeeId()));
        employee = employees.get(1);
        Assert.assertEquals("employeeNo=2,employeeName=名字$112,sex=true,departmentId=2,positionId=2,lastEmployeeId=null",
                StringUtil.format(format, employee.getEmployeeNo(), employee.getEmployeeName(), employee.getSex(), employee.getDepartmentId(), employee.getPositionId(), employee.getLastEmployeeId()));
        employee = employees.get(2);
        Assert.assertEquals("employeeNo=3,employeeName=名字113,sex=false,departmentId=3,positionId=3,lastEmployeeId=3",
                StringUtil.format(format, employee.getEmployeeNo(), employee.getEmployeeName(), employee.getSex(), employee.getDepartmentId(), employee.getPositionId(), employee.getLastEmployeeId()));

        reader.close();
    }

    /**
     * 职位带有结尾空格,导致读取的时候无法读取数据
     */
    @Test
    public void testNameIsEmpty(){
        Department department1 = new Department();
        department1.setId(1);
        //注意 " 财务部,X Finance " meta信息中转化为 "财务部 X Finance "
        department1.setName(" 财务部,X Finance ");
        List<Position> positions1 = new ArrayList<>();
        department1.setPositions(positions1);
        Position position11 = new Position();
        position11.setId(1);
        position11.setName(" 财务经理 Finance Manager ");
        positions1.add(position11);
        Position position12 = new Position();
        position12.setId(2);
        position12.setName(" 财务助理,Finance Manager ");
        positions1.add(position12);

        List<Employee> employees = new ArrayList<>();
        Employee employee = new Employee();
        employee.setEmployeeName("王一");
        employee.setDepartmentId(1);
        employee.setPositionId(2);
        employees.add(employee);

        Metas metas = new Metas();
        TreeMeta departmentMeta = TreeMeta.buildMetaWithAnnotation("employees", department1);
        metas.addMeta(departmentMeta);

        Headers headers = new Headers();
        headers.addHeader("employeeName", "员工");
        headers.addTreeHeader("departmentId", "部门", departmentMeta);
        headers.addTreeHeader("positionId", "职位", departmentMeta, "departmentId");
        metas.addHeaders(headers);

        ExcelWriter writer = new ExcelWriter();
        writer.setHiddenMetaSheet(false);
        writer.createMetaSheet(metas);
        SheetWriter sheetWriter = writer.sheet("Sheet1");
        sheetWriter.writeHeader(headers);
        sheetWriter.writeRows(employees, headers);
        sheetWriter.autoSizeColumn(headers);

        File file = FileUtil.file(baseDir, "testNameIsEmpty.xlsx");
        writer.flush(file);
        writer.close();

        //读取验证
        ExcelReader reader = new ExcelReader(file);
        Metas newMeta = reader.getMetas();
        Headers newHeader = newMeta.getHeadersByName(Headers.DEFAULT_HEADERS);

        SheetReader sheetReader = reader.sheet("Sheet1");
        List<Employee> readEmployees = sheetReader.passCurrentRow().readBeanRowsAndNext(Employee.class, newHeader);
        reader.close();

        Assert.assertEquals(1, readEmployees.size());
        Employee employee1 = readEmployees.get(0);
        Assert.assertEquals(Integer.valueOf(1), employee1.getDepartmentId());
        Assert.assertEquals(Integer.valueOf(2), employee1.getPositionId());
    }

    @Test
    public void testReadMapOfObject(){
        ExcelReader reader = new ExcelReader(ResourceUtil.getStream("read/testReadMapOfObject.xlsx"));
        SheetReader sheetReader = reader.sheet("sheet1");
        Assert.assertNotNull(sheetReader);
        sheetReader.setIsSimpleBean(false);

        Headers headers = new Headers();
        headers.addHeader("name", "姓名");
        headers.addHeader("map[h1]", "爱好1");
        headers.addHeader("map[h2]", "爱好2");
        headers.addHeader("map[h3]", "爱好3");
        headers.addHeader("age", "年龄");

        List<Bean1> list = sheetReader.skipRows(1).readBeanRowsAndNext(Bean1.class, headers);
        Assert.assertNotNull(list);

    }

    public static class Bean1 {
        private String name;
        private Map<String, String> map;
        private Integer age;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Map<String, String> getMap() {
            return map;
        }

        public void setMap(Map<String, String> map) {
            this.map = map;
        }

        public Integer getAge() {
            return age;
        }

        public void setAge(Integer age) {
            this.age = age;
        }
    }




}

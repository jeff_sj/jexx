package jexx.poi.meta;

import jexx.io.FileUtil;
import jexx.poi.ExcelReader;
import jexx.poi.ExcelWriter;
import jexx.poi.bean.Employee;
import jexx.poi.bean.Position;
import jexx.poi.meta.node.INode;
import jexx.poi.meta.node.Node;
import jexx.poi.meta.node.TreeNode;
import jexx.util.ResourceUtil;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author jeff
 * @since 2019/6/14
 */
public class MetaBuilderTest {

    protected String baseDir;

    @Before
    public void setUp(){
        URL data = ResourceUtil.getResource("poi");
        baseDir = data.getFile().concat("/").concat(MetaBuilderTest.class.getSimpleName());
        FileUtil.mkdirs(baseDir);
    }


    @Test
    public void testBuildArrayMeta(){
        List<Employee> employees = Employee.randomEmployees(10);

        ArrayMeta meta  = ArrayMeta.buildMetaWithAnnotation("employee", employees);
        Assert.assertEquals(10, meta.getValueMetas().size());

        meta  = ArrayMeta.buildMeta("employee", employees, Employee::getEmployeeNo, Employee::getEmployeeName);
        Assert.assertEquals(10, meta.getValueMetas().size());

        meta  = ArrayMeta.buildMeta("employee", employees, Employee::getEmployeeName);
        Assert.assertEquals(10, meta.getValueMetas().size());

        Map<Integer,String> map = employees.stream().collect(Collectors.toMap(Employee::getEmployeeNo, Employee::getEmployeeName));
        meta  = ArrayMeta.buildMeta("employee", map);
        Assert.assertEquals(10, meta.getValueMetas().size());

        List<String> employeeNameList = employees.stream().map(Employee::getEmployeeName).collect(Collectors.toList());
        meta  = ArrayMeta.buildMeta("employee", employeeNameList);
        Assert.assertEquals(10, meta.getValueMetas().size());

        Set<String> employeeNameSet = employees.stream().map(Employee::getEmployeeName).collect(Collectors.toSet());
        meta  = ArrayMeta.buildMeta("employee", employeeNameSet);
        Assert.assertEquals(10, meta.getValueMetas().size());
    }

    @Test
    public void testBuildTreeMeta(){
        List<Position> positions = Position.randomPositions(10);

        TreeMeta treeMeta = TreeMeta.buildMetaWithAnnotation("position", positions);
        Assert.assertNotNull(treeMeta);
        TreeNode treeNode = treeMeta.getTree();
        Assert.assertNotNull(treeNode);
        Assert.assertEquals(10, treeNode.getChildren().size());
    }

    @Test
    public void testWriteMetadata(){
        Metas metas = new Metas();

        List<INode> sexNodes = new ArrayList<>();
        sexNodes.add(new Node(true, "男"));
        sexNodes.add(new Node(false, "女"));
        String metaName = "0809住宿";
        ArrayMeta sexMeta = new ArrayMeta(metaName, sexNodes);
        metas.addMeta(sexMeta);

        ExcelWriter writer = new ExcelWriter();
        writer.setHiddenMetaSheet(false);
        writer.createMetaSheet(metas);

        File file = FileUtil.file(baseDir, "testWriteMetadata.xlsx");
        writer.flush(file);
        writer.close();

        ExcelReader excelReader = new ExcelReader(file);
        Metas readerMetas = excelReader.getMetas();
        Assert.assertNotNull(readerMetas);

        IMeta meta = readerMetas.getMeta(metaName);
        Assert.assertNotNull(meta);
        Assert.assertTrue(meta instanceof ArrayMeta);
        ArrayMeta arrayMeta = (ArrayMeta)meta;
        Assert.assertEquals(metaName, arrayMeta.getName());
        List<INode> valueMetas = arrayMeta.getValueMetas();
        Assert.assertEquals(2, valueMetas.size());
        INode node2 = valueMetas.get(1);
        Assert.assertEquals(false, node2.getValue());
        Assert.assertEquals("女", node2.getLabel());
    }

}

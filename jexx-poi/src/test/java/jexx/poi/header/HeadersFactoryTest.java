package jexx.poi.header;

import jexx.poi.header.annotation.HeaderColumn;
import jexx.poi.header.annotation.HeaderIgnore;
import jexx.poi.header.annotation.HeaderTable;
import jexx.poi.header.factory.HeaderColumnSelectMode;
import jexx.poi.header.impl.HeaderModifyFieldImpl;
import jexx.poi.style.CellStyleSets;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

/**
 * @author jeff
 * @since 2019/6/14
 */
public class HeadersFactoryTest {

    @Test
    public void testBuildHeaders1(){
        Headers headers = Headers.newBuild().build(TestHeadersBean1.class);
        List<IHeader> headerList = headers.getHeaders();
        Assert.assertNotNull(headerList);
        Assert.assertEquals(2, headerList.size());
        Assert.assertEquals("id", headerList.get(0).getValue());
        Assert.assertEquals("name", headerList.get(1).getValue());

        headers = Headers.newBuild().ignoreField("id").build(TestHeadersBean1.class);
        headerList = headers.getHeaders();
        Assert.assertNotNull(headerList);
        Assert.assertEquals(1, headerList.size());
        Assert.assertEquals("name", headerList.get(0).getValue());
    }

    private static class TestHeadersBean1{
        public Integer id;
        public String name;
    }

    @Test
    public void testBuildHeaders2(){
        Headers headers = Headers.newBuild()
                .select(HeaderColumnSelectMode.ALL_WITH_TAG)
                .withCellStyle("headerCellStyle", CellStyleSets.HEADER_CELL_STYLE)
                .withCellStyle("dataCellStyle", CellStyleSets.DATA_CELL_STYLE)
                .build(TestHeadersBean2.class);
        List<IHeader> headerList = headers.getHeaders();
        Assert.assertNotNull(headerList);
        Assert.assertEquals(3, headerList.size());
        Assert.assertEquals("id", headerList.get(0).getValue());
        IDataHeader dataHeader = headers.getDataHeaderByLabel("name");
        Assert.assertEquals("名称", dataHeader.getValue());

        headers = Headers.newBuild()
                .select(HeaderColumnSelectMode.ONLY_TAG)
                .withCellStyle("headerCellStyle", CellStyleSets.HEADER_CELL_STYLE)
                .withCellStyle("dataCellStyle", CellStyleSets.DATA_CELL_STYLE)
                .build(TestHeadersBean2.class);
        headerList = headers.getHeaders();
        Assert.assertNotNull(headerList);
        Assert.assertEquals(2, headerList.size());
        dataHeader = headers.getDataHeaderByLabel("name");
        Assert.assertEquals("名称", dataHeader.getValue());
    }

    @Test
    public void testBuildHeaders3(){
        Headers headers = Headers.newBuild()
                .select(HeaderColumnSelectMode.ALL)
                .build(TestHeadersBean2.class);
        Assert.assertEquals(4, headers.getDataHeaders().size());

        headers = Headers.newBuild()
                .select(HeaderColumnSelectMode.ALL_WITH_TAG)
                .build(TestHeadersBean2.class);
        Assert.assertEquals(3, headers.getDataHeaders().size());

        headers = Headers.newBuild()
                .select(HeaderColumnSelectMode.ALL_WITH_TAG)
                .group(TestHeadersBean2.A.class)
                .build(TestHeadersBean2.class);
        Assert.assertEquals(2, headers.getDataHeaders().size());

        headers = Headers.newBuild()
                .select(HeaderColumnSelectMode.ALL_WITH_TAG)
                .group(TestHeadersBean2.B.class)
                .build(TestHeadersBean2.class);
        Assert.assertEquals(2, headers.getDataHeaders().size());

        headers = Headers.newBuild()
                .select(HeaderColumnSelectMode.ONLY_TAG)
                .group(TestHeadersBean2.A.class)
                .build(TestHeadersBean2.class);
        Assert.assertEquals(1, headers.getDataHeaders().size());

        headers = Headers.newBuild()
                .select(HeaderColumnSelectMode.ONLY_TAG)
                .group(TestHeadersBean2.B.class)
                .build(TestHeadersBean2.class);
        Assert.assertEquals(1, headers.getDataHeaders().size());

        headers = Headers.newBuild()
                .select(HeaderColumnSelectMode.ONLY_TAG)
                .group(TestHeadersBean2.B.class)
                .replace(new DefaultDataHeader("age", "年龄"))
                .build(TestHeadersBean2.class);
        Assert.assertEquals(1, headers.getDataHeaders().size());

        headers = Headers.newBuild()
                .select(HeaderColumnSelectMode.ONLY_TAG)
                .group(TestHeadersBean2.B.class)
                .replace(new DefaultDataHeader("name", "名称"))
                .build(TestHeadersBean2.class);
        Assert.assertEquals(1, headers.getDataHeaders().size());

        //排序测试
        headers = Headers.newBuild()
                .select(HeaderColumnSelectMode.ONLY_TAG)
                .build(TestHeadersBean2.class);
        Assert.assertEquals("name", headers.getDataHeaders().get(0).getKey());
        headers = Headers.newBuild()
                .select(HeaderColumnSelectMode.ONLY_TAG)
                .modify("age", new HeaderModifyFieldImpl("名称123", 0, 1))
                .build(TestHeadersBean2.class);
        Assert.assertEquals("age", headers.getDataHeaders().get(0).getKey());
    }

    @HeaderTable(name = "TestHeadersBean2")
    private static class TestHeadersBean2{

        public static class A {}
        public static class B {}

        public Integer id;
        @HeaderColumn(group = A.class, value = "名称", order = 2, headerCellStyle = "headerCellStyle", dataCellStyle = "dataCellStyle")
        public String name;
        @HeaderColumn(group = B.class, value = "年龄", order = 3, headerCellStyle = "headerCellStyle", dataCellStyle = "dataCellStyle")
        public Integer age;
        @HeaderIgnore
        @HeaderColumn
        public String desc;
    }

}

package jexx.poi.header;

import jexx.poi.header.row.HeaderRow;
import jexx.poi.header.row.HeaderRowContext;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author jeff
 * @since 2019/6/16
 */
public class HeaderRowContextTest {


    @Test
    public void test(){
        Headers headers = new Headers();
        headers.addHeader(new SequenceDataHeader("序号"));
        headers.addHeader(new DefaultDataHeader("id", "部门"));
        headers.addHeader(new DefaultDataHeader("positions[].id", "职位"));
        headers.addHeader(new DefaultDataHeader("positions[].employees[].employeeNo", "工号"));
        headers.addHeader(new DefaultDataHeader("positions[].employees[].employeeName", "年龄"));
        headers.addHeader(new DefaultDataHeader("positions[].employees[].age", "姓名"));
        headers.addHeader(new DefaultDataHeader("positions[].employees[].sex", "性别"));
        headers.addHeader(new DefaultDataHeader("positions[].employees[].hobbies[]", "兴趣"));

        HeaderRowContext headerRowContext = headers.getHeaderRowContext();
        HeaderRow headerRow = headerRowContext.getRootHeaderRow();
        Assert.assertNotNull(headerRow);
        Assert.assertEquals(1, headerRow.getHeaderMap().size());
        headerRow = headerRow.getChild();
        Assert.assertEquals(1, headerRow.getHeaderMap().size());
        headerRow = headerRow.getChild();
        Assert.assertEquals(4, headerRow.getHeaderMap().size());
        headerRow = headerRow.getChild();
        Assert.assertEquals(1, headerRow.getHeaderMap().size());
    }

}

poi 4.0 版本使用 shiftRows 时有bug！

## SheetWriter
### 基本数据写入
@see ExcelWriterTest#test1
```
        List<Employee> employees = Employee.randomEmployees(10, null, null);

        //构建Excel写入类
        ExcelWriter writer = new ExcelWriter();

        Headers headers = new Headers();
        headers.addHeader("employeeNo", "工号");
        headers.addHeader("employeeName", "姓名");
        headers.addHeader("age", "年龄");


        writer.sheet("test1", headers).writeHeader().writeRows(employees);
        writer.flush(new File("C:/Users/kxys4/Desktop/a.xlsx"));
        writer.close();
```

### 样式,以及显示值控制
@see ExcelWriterTest#test2
```
        List<Employee> employees = Employee.randomEmployees(10, null, null);

        ExcelWriter writer = new ExcelWriter();

        //样式集成控制类
        CellStyleSets cellStyleSets = writer.getCellStyleSets();

        //定义日期样式
        WrapCellStyle dateCellStyle = new WrapCellStyle();
        dateCellStyle.setDataFormat(cellStyleSets.createDataFormat("yyyy-MM-dd"));

        Headers headers = new Headers();
        headers.addHeader("employeeNo", "工号");
        headers.addHeader("employeeName", "姓名");
        //显示值转换, 年龄为null转化为 "-"
        headers.addHeader(new DefaultHeader("age", "年龄").wrapLabel(s->s==null?"-":s));
        headers.addHeader("birthdate", "生日", null, dateCellStyle);

        writer.sheet("test1", headers).writeHeader().writeRows(employees, new CellStyleMapper() {
            @Override
            public IWrapCellStyle warpCellStyle(Object value, int rowNum, int startColumnNum) {
                //指定列设置样式
                if(headers.matchColumnNum(startColumnNum, "employeeName")){
                    WrapCellStyle cellStyle = getCellStyle();
                    cellStyle.setAlignment(HorizontalAlignment.CENTER);
                    cellStyle.setFillForegroundColor(IndexedColors.RED);
                    cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
                    cellStyle.setBottomBorderStyle(BorderStyle.THIN);
                    return cellStyle;
                }
                return null;
            }
        });
        writer.flush(new File("C:/Users/kxys4/Desktop/a.xlsx"));
        writer.close();
```

### 下拉框, 级联
@see ExcelWriterTest#test3
```
        List<Department> departments = Department.randomDepartments(5);
        List<Employee> employees = Employee.randomEmployees(10, departments.get(0), departments.get(0).getPositions().get(0));

        //构建元信息类
        Metas metas = new Metas();

        //构建性别选择元信息
        List<INode> sexNodes = new ArrayList<>();
        sexNodes.add(new Node(true, "男"));
        sexNodes.add(new Node(false, "女"));
        ArrayMeta sexMeta = new ArrayMeta("性别", sexNodes);
        metas.addMeta(sexMeta);

        //部门,职位,员工 三级级联元信息构建
        TreeMeta treeMeta = TreeMeta.builderMeta(departments);
        metas.addMeta(treeMeta);

        ExcelWriter writer = new ExcelWriter();
        //写入元信息
        writer.createMetaSheet(metas);

        Headers headers = new Headers();
        headers.addHeader("employeeNo", "工号");
        headers.addHeader("employeeName", "姓名");
        headers.addArrayHeader("sex", "性别", sexMeta);
        TreeHeader departmentHeader = new TreeHeader("departmentId", "部门", treeMeta, null);
        headers.addHeader(departmentHeader);
        TreeHeader positionHeader = new TreeHeader("positionId", "职位", treeMeta, departmentHeader);
        headers.addHeader(positionHeader);

        SheetWriter sheetWriter = writer.sheet("test1", headers);

        sheetWriter.writeRowWithDefaultHeaderStyle()
                .validData(20)
                .writeRows(employees, new CellStyleMapper() {
                    @Override
                    public IWrapCellStyle warpCellStyle(INode node, int rowNum, int startColumnNum) {
                        if(startColumnNum == 1 || node.getValue() == null){
                            WrapCellStyle cellStyle = getCellStyle();
                            cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
                            cellStyle.setAlignment(HorizontalAlignment.CENTER);
                            cellStyle.setFillForegroundColor(IndexedColors.RED);
                            //设置前景色的填充方式
                            cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
                            cellStyle.setBottomBorderStyle(BorderStyle.THIN);
                            return cellStyle;
                        }
                        return null;
                    }
                });
        writer.flush(new File("C:/Users/kxys4/Desktop/a.xlsx"));
```

### 父子行
@see ExcelWriterTest#testWriteIndexBean
```
        List<Department> departments = Department.randomDepartments(5);

        Metas metas = new Metas();

        List<INode> valueMetas = new ArrayList<>();
        valueMetas.add(new Node(true, "男"));
        valueMetas.add(new Node(false, "女"));
        ArrayMeta sexMeta = new ArrayMeta("性别", valueMetas);
        metas.addMeta(sexMeta);

        TreeMeta treeMeta = TreeMeta.builderMeta(departments);
        metas.addMeta(treeMeta);

        ExcelWriter writer = new ExcelWriter(ResourceUtil.getStream("indexbean.xlsx"));
        writer.createMetaSheet(metas);

        Headers headers = new Headers();
        TreeHeader departmentHeader = new TreeHeader("id", "部门", treeMeta, null);
        headers.addHeader(departmentHeader);
        TreeHeader positionHeader = new TreeHeader("positions[].id", "职位", treeMeta, departmentHeader);
        headers.addHeader(positionHeader);
        headers.addHeader("positions[].employees[].employeeNo", "工号");
        headers.addHeader("positions[].employees[].employeeName", "姓名");
        headers.addHeader("positions[].employees[].age", "年龄");
        headers.addArrayHeader("positions[].employees[].sex", "年龄", sexMeta);
        headers.addHeader("positions[].employees[].hobbies[]", "兴趣");

        SheetWriter sheetWriter = writer.sheet("test1", headers);
        //指定范围内填充数据, 会自动插入多余的数据
        sheetWriter.fill(2, 4, departments);
        writer.flush(new File("C:/Users/kxys4/Desktop/a.xlsx"));
```

### 任意Cell写入
@see ExcelWriterTest#testWriteCell
```     
        //单个单元格写入
        SingleCell indexCell = new SingleCell(startRowNum, 1, i+1);
        indexCell.setCellStyle(style);
        cellWriter.writeCell(indexCell);
        
        //合并单元格写入
        MergedCell sexCell = new MergedCell(startRowNum, 2, startRowNum+1, 3 ,i%2==0);
        sexCell.setCellStyle(style);
        sexCell.setMeta(sexMeta);
        cellWriter.writeCell(sexCell);
        
        //图片单元格写入
        ImageCell imageCell = new ImageCell(startRowNum, 8, pictureType, image);
        cellWriter.writeCell(imageCell);
        
        //调用flush才会写入excel
        cellWriter.flush();

```

### 通过注解构建header
@see ExcelWriterTest#testWriteWithBuildHeader
```
//meta
Metas metas = new Metas();
ArrayMeta sexMeta = new ArrayMeta("sex", CollectionUtil.list(new Node(true, "男"), new Node(false, "女")));
metas.addMeta(sexMeta);

List<Department> departments = Department.randomDepartments(5);
TreeMeta departmentMeta = TreeMeta.buildMetaWithAnnotation("department", departments);
metas.addMeta(departmentMeta);

metas.addMeta(ArrayMeta.buildMeta("employees", employees, Employee::getEmployeeNo, Employee::getEmployeeName));

//i18n
Map<String, String> i18nMap = MapUtil.toMap("i18.employeeNo", "工号");

Headers headers = Headers.newBuild().select(HeaderColumnSelectMode.ONLY_TAG)
        .withMetas(metas)
        .withCellStyle("date", new DataFormatWrapCellStyle(DataFormatEnum.DATE))
        .i18n(i18nMap)
        .build(Employee.class);
```
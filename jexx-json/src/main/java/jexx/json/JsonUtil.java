package jexx.json;

import jexx.collect.StringKeyMap;
import jexx.util.ClassUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;
import java.util.List;

/**
 * json工具类
 * @author jeff
 * @since 2021-01-21
 */
public class JsonUtil {

    private static final Logger log = LoggerFactory.getLogger(JsonUtil.class);

    private static final Json json;

    static {
        ClassLoader classLoader = JsonUtil.class.getClassLoader();
        boolean jackson2Present = ClassUtil.exist("com.fasterxml.jackson.databind.ObjectMapper", classLoader);
        boolean fastjson2Present = ClassUtil.exist("com.alibaba.fastjson.JSON", classLoader);

        if(jackson2Present){
            json = new JacksonJson();
            log.info("JsonUtil select jackson!");
        }
        else if(fastjson2Present){
            json = new FastJsonJson();
            log.info("JsonUtil select fastjson!");
        }
        else{
            throw new Error("Please provide a json jar");
        }
    }

    @SuppressWarnings("unchecked")
    public static  <T> T parseObject(String input, Class<T> clazz) {
        if(StringKeyMap.class.equals(clazz)){
            return (T)parseMap(input);
        }
        return json.parseObject(input, clazz);
    }

    @SuppressWarnings("unchecked")
    public static  <T> T parseObject(InputStream inputStream, Class<T> clazz) {
        if(StringKeyMap.class.equals(clazz)){
            return (T)parseMap(inputStream);
        }
        return json.parseObject(inputStream, clazz);
    }


    public static <T> List<T> parseList(String input, Class<T> clazz){
        return json.parseList(input, clazz);
    }

    public static <T> List<T> parseList(InputStream input, Class<T> clazz){
        return json.parseList(input, clazz);
    }

    public static StringKeyMap parseMap(String input){
        return json.parseMap(input);
    }

    public static StringKeyMap parseMap(InputStream input){
        return json.parseMap(input);
    }

    public static List<?> parseMapList(String input){
        return json.parseMapList(input);
    }

    public static String toJSONString(Object object) {
        return json.toJSONString(object);
    }

}

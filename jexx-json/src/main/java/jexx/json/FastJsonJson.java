package jexx.json;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.JSONReader;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.alibaba.fastjson.support.config.FastJsonConfig;
import jexx.collect.LinkedStringKeyMap;
import jexx.collect.StringKeyMap;
import jexx.exception.IORuntimeException;
import jexx.util.CollectionUtil;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * fastjson实现
 * @author jeff
 * @since 2021-01-21
 */
class FastJsonJson implements Json {

    private final FastJsonConfig config;
    public FastJsonJson() {
        this.config = new FastJsonConfig();
        this.config.setSerializerFeatures(
                SerializerFeature.WriteDateUseDateFormat
        );
    }

    @Override
    public <T> T parseObject(String input, Class<T> clazz) {
        return JSON.parseObject(input, clazz);
    }

    @Override
    public <T> T parseObject(InputStream input, Class<T> clazz) {
        try {
            return JSON.parseObject(input, clazz);
        } catch (IOException e) {
            throw new IORuntimeException(e);
        }
    }

    @Override
    public <T> List<T> parseList(String input, Class<T> clazz) {
        return JSON.parseArray(input, clazz);
    }

    @Override
    public <T> List<T> parseList(InputStream input, Class<T> clazz) {
        JSONReader jsonReader = new JSONReader(new InputStreamReader(input));
        jsonReader.startArray();
        List<T> list = new ArrayList<>();
        while(jsonReader.hasNext()){
            T t = jsonReader.readObject(clazz);
            list.add(t);
        }
        return list;
    }

    @Override
    public LinkedStringKeyMap parseMap(String input) {
        JSONObject jsonObject = JSON.parseObject(input);
        if(jsonObject == null){
            return null;
        }
        LinkedStringKeyMap map = new LinkedStringKeyMap(jsonObject.size());
        scanJSONObject(map, jsonObject);
        return map;
    }

    @Override
    public StringKeyMap parseMap(InputStream input) {
        JSONObject jsonObject = parseObject(input, JSONObject.class);
        if(jsonObject == null){
            return null;
        }
        LinkedStringKeyMap map = new LinkedStringKeyMap(jsonObject.size());
        scanJSONObject(map, jsonObject);
        return map;
    }

    @Override
    public List<?> parseMapList(String input) {
        JSONArray jsonArray = JSON.parseArray(input);
        List<Object> result = new ArrayList<>();
        scanJSONArray(result, jsonArray);
        return result;
    }

    private void scanJSONObject(LinkedStringKeyMap map, JSONObject jsonObject){
        if(CollectionUtil.isEmpty(jsonObject)){
            return;
        }
        for(Map.Entry<String, Object> entry : jsonObject.entrySet()){
            String key = entry.getKey();
            Object value = entry.getValue();
            if(value instanceof JSONArray){
                List<Object> list = new ArrayList<>(((JSONArray) value).size());
                scanJSONArray(list, (JSONArray)value);
                map.put(key, list);
            }
            else if(value instanceof JSONObject){
                LinkedStringKeyMap map1 = new LinkedStringKeyMap(((JSONObject) value).size());
                scanJSONObject(map1, (JSONObject)value);
                map.put(key, map1);
            }
            else{
                map.put(key, value);
            }
        }
    }

    private void scanJSONArray(List<Object> list, JSONArray array){
        if(CollectionUtil.isEmpty(array)){
            return;
        }
        for(Object obj : array){
            if(obj instanceof JSONArray){
                List<Object> list1 = new ArrayList<>(((JSONArray) obj).size());
                scanJSONArray(list1, (JSONArray)obj);
                list.add(list1);
            }
            else if(obj instanceof JSONObject){
                LinkedStringKeyMap map = new LinkedStringKeyMap(((JSONObject) obj).size());
                scanJSONObject(map, (JSONObject)obj);
                list.add(map);
            }
            else{
                list.add(obj);
            }
        }
    }

    @Override
    public String toJSONString(Object object) {
        return JSON.toJSONString(object);
    }

}

package jexx.json;

import jexx.collect.StringKeyMap;

import java.io.InputStream;
import java.util.List;

interface Json {

    <T> T parseObject(String input, Class<T> clazz);

    <T> T parseObject(InputStream input, Class<T> clazz);

    <T> List<T> parseList(String input, Class<T> clazz);

    <T> List<T> parseList(InputStream input, Class<T> clazz);

    /**
     * json转map, 内部对象仅 StringKeyMap,List,以及基本类型
     * @param input json
     */
    StringKeyMap parseMap(String input);

    StringKeyMap parseMap(InputStream input);

    /**
     * json转集合, 内部对象仅 StringKeyMap,List,以及基本类型
     * @param input json
     */
    List<?> parseMapList(String input);

    String toJSONString(Object object);

}

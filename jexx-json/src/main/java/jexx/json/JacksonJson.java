package jexx.json;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import jexx.collect.LinkedStringKeyMap;
import jexx.collect.StringKeyMap;
import jexx.exception.IORuntimeException;
import jexx.util.ClassUtil;
import jexx.util.CollectionUtil;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * jackson实现Json
 * @author jeff
 * @since 2021-01-21
 */
class JacksonJson implements Json {

    private final ObjectMapper mapper;

    public JacksonJson() {
        this.mapper = new ObjectMapper();
        this.mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        this.mapper.configure(DeserializationFeature.ADJUST_DATES_TO_CONTEXT_TIME_ZONE, false);
        this.mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS , false);
        //允许变量无双引号
        this.mapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
        if(ClassUtil.exist("com.fasterxml.jackson.datatype.jsr310.JavaTimeModule")){
            this.mapper.registerModule(new JavaTimeModule());
        }
        //忽视 transient
        mapper.configure(MapperFeature.PROPAGATE_TRANSIENT_MARKER, true);
    }

    @Override
    public <T> T parseObject(String input, Class<T> clazz) {
        try {
            return mapper.readValue(input, clazz);
        } catch (IOException e) {
            throw new IORuntimeException(e);
        }
    }

    @Override
    public <T> T parseObject(InputStream input, Class<T> clazz) {
        try {
            return mapper.readValue(input, clazz);
        } catch (IOException e) {
            throw new IORuntimeException(e);
        }
    }

    @Override
    public <T> List<T> parseList(String input, Class<T> clazz) {
        try {
            return mapper.readValue(input, mapper.getTypeFactory().constructCollectionType(List.class, clazz));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public <T> List<T> parseList(InputStream input, Class<T> clazz) {
        try {
            return mapper.readValue(input, mapper.getTypeFactory().constructCollectionType(List.class, clazz));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public StringKeyMap parseMap(String input) {
        LinkedStringKeyMap map = parseObject(input, LinkedStringKeyMap.class);
        LinkedStringKeyMap result = new LinkedStringKeyMap(map.size());
        scanMap(result, map);
        return result;
    }

    @Override
    public StringKeyMap parseMap(InputStream input) {
        LinkedStringKeyMap map = parseObject(input, LinkedStringKeyMap.class);
        LinkedStringKeyMap result = new LinkedStringKeyMap(map.size());
        scanMap(result, map);
        return result;
    }

    @Override
    public List<?> parseMapList(String input) {
        List<Object> list = parseList(input, Object.class);
        List<Object> result = new ArrayList<>();
        scanList(result, list);
        return result;
    }

    private void scanMap(Map<String, Object> map, Map<?,?> value){
        if(CollectionUtil.isEmpty(value)){
            return;
        }
        for(Map.Entry<?, ?> entry : value.entrySet()){
            String k = entry.getKey().toString();
            Object v = entry.getValue();
            if(v instanceof List){
                List<Object> list = new ArrayList<>(((List<?>) v).size());
                scanList(list, (List<?>)v);
                map.put(k, list);
            }
            else if(v instanceof Map){
                LinkedStringKeyMap map1 = new LinkedStringKeyMap(((Map<?,?>) v).size());
                scanMap(map1, (Map<?,?>)v);
                map.put(k, map1);
            }
            else{
                map.put(k, v);
            }

        }
    }

    private void scanList(List<Object> list, List<?> values){
        if(CollectionUtil.isEmpty(values)){
            return;
        }
        for (Object value : values){
            if(value instanceof List){
                List<Object> list1 = new ArrayList<>(((List<?>) value).size());
                scanList(list1, (List<?>) value);
                list.add(list1);
            }
            else if(value instanceof Map){
                LinkedStringKeyMap map = new LinkedStringKeyMap(((Map<?,?>) value).size());
                scanMap(map, (Map<?,?>)value);
                list.add(map);
            }
            else{
                list.add(value);
            }
        }
    }

    @Override
    public String toJSONString(Object object) {
        try {
            return mapper.writeValueAsString(object);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}

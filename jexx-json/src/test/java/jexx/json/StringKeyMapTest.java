package jexx.json;

import jexx.collect.LinkedStringKeyMap;
import jexx.collect.StringKeyMap;
import jexx.util.Console;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class StringKeyMapTest {

    @Test
    public void testGetArray(){
        StringKeyMap map = new LinkedStringKeyMap();
        List<Bean1> list1 = new ArrayList<>();

        Bean1 bean11 = new Bean1();
        bean11.setName("bean11");
        list1.add(bean11);
        Bean1 bean12 = new Bean1();
        bean12.setName("bean12");
        list1.add(bean12);
        map.put("list1", list1);

        String json = JsonUtil.toJSONString(map);
        Console.log(json);

        StringKeyMap stringKeyMap1 = JsonUtil.parseMap(json);

        StringKeyMap[] array1 = stringKeyMap1.getArray("list1", StringKeyMap.class);
        Assert.assertEquals(2, array1.length);
        StringKeyMap b2 = array1[1];
        Assert.assertEquals("bean12", b2.getString("name"));

    }

    private static class Bean1 {

        private String name;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

}

package jexx.json;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

public class TestTimeBean {

    private LocalDateTime a;
    private LocalDate b;
    private LocalTime c;

    public LocalDateTime getA() {
        return a;
    }

    public void setA(LocalDateTime a) {
        this.a = a;
    }

    public LocalDate getB() {
        return b;
    }

    public void setB(LocalDate b) {
        this.b = b;
    }

    public LocalTime getC() {
        return c;
    }

    public void setC(LocalTime c) {
        this.c = c;
    }
}

package jexx.json;

import jexx.collect.LinkedStringKeyMap;
import org.junit.Assert;
import org.junit.Test;

public class JsonUtilTest {

    @Test
    public void testParseObject(){
        TestJsonBean bean1 = new TestJsonBean();
        bean1.setId(1);
        bean1.setName("小王");

        String json = JsonUtil.toJSONString(bean1);
        Assert.assertEquals("{\"id\":1,\"name\":\"小王\",\"bean1\":null}", json);

        TestJsonBean bean2 = JsonUtil.parseObject(json, TestJsonBean.class);
        Assert.assertEquals(bean1.getId(), bean2.getId());
        Assert.assertEquals(bean1.getName(), bean2.getName());
    }

    @Test
    public void testParseStringKeyMap(){
        TestJsonBean bean1 = new TestJsonBean();
        bean1.setId(1);
        bean1.setName("小王");

        TestJson1Bean bean11 = new TestJson1Bean();
        bean11.setAge(99);
        bean1.setBean1(bean11);

        String json = JsonUtil.toJSONString(bean1);
        Assert.assertEquals("{\"id\":1,\"name\":\"小王\",\"bean1\":{\"age\":99}}", json);

        LinkedStringKeyMap map = JsonUtil.parseObject(json, LinkedStringKeyMap.class);
        Assert.assertEquals(bean1.getId(), map.getInteger("id"));
        Assert.assertEquals(bean1.getName(), map.getString("name"));
        LinkedStringKeyMap map11 = map.getObject("bean1", LinkedStringKeyMap.class);
        Assert.assertEquals(bean11.getAge(), map11.getInteger("age"));
    }

}

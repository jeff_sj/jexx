package jexx.json;

public class TestJsonBean {

    private Integer id;

    private String name;

    private TestJson1Bean bean1;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public TestJson1Bean getBean1() {
        return bean1;
    }

    public void setBean1(TestJson1Bean bean1) {
        this.bean1 = bean1;
    }
}

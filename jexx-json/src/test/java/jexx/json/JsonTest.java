package jexx.json;

import jexx.collect.LinkedStringKeyMap;
import jexx.collect.StringKeyMap;
import jexx.time.LocalDateUtil;
import jexx.util.StringUtil;
import org.junit.Assert;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;

public class JsonTest {

    @Test
    public void testJson(){
        checkJson(new FastJsonJson());
        checkJson(new JacksonJson());
    }

    private void checkJson(Json json){
        //空对象
        String str = "{}";
        StringKeyMap map = json.parseMap(str);
        Assert.assertNotNull(map);
        Assert.assertEquals(0, map.size());

        //对象
        str = "{\"code\":\"000\",\"msg\":\"success\",\"body\":{\"list\":[{\"code\":\"001\",\"name\":\"大学1\"},{\"code\":\"002\",\"name\":\"大学2\"}]}}";
        map = json.parseMap(str);
        checkObject(map);

        //对象流
        InputStream inputStream = new ByteArrayInputStream(StringUtil.getBytes(str));
        map = json.parseMap(inputStream);
        checkObject(map);

        //简单集合
        str = "[{\"a\":1, \"b\":2},{\"a\":3, \"b\":4}]";
        List<LinkedStringKeyMap> list1 = json.parseList(str, LinkedStringKeyMap.class);
        checkList1(list1);
        inputStream = new ByteArrayInputStream(StringUtil.getBytes(str));
        list1 = json.parseList(inputStream, LinkedStringKeyMap.class);
        checkList1(list1);

        //集合
        str = "[[{\"a\":1},{\"a\":2}],[{\"a\":3},{\"a\":4}]]";
        List<?> list2 = json.parseMapList(str);
        checkList(list2);

        // support JSR-310 (Java 8 Date & Time API) data types
        TestTimeBean testTimeBean1 = new TestTimeBean();
        testTimeBean1.setA(LocalDateUtil.parse("2021-08-26T13:33:33.425", "yyyy-MM-dd'T'HH:mm:ss.SSS"));
        testTimeBean1.setB(LocalDateUtil.parseDate("2021-08-26", "yyyy-MM-dd"));
        testTimeBean1.setC(LocalTime.parse("13:33:33.425",  DateTimeFormatter.ofPattern("HH:mm:ss.SSS")));
        String timeStr = "{\"a\":\"2021-08-26T13:33:33.425\",\"b\":\"2021-08-26\",\"c\":\"13:33:33.425\"}";
        Assert.assertEquals(timeStr, json.toJSONString(testTimeBean1));
        TestTimeBean testTimeBean2 = json.parseObject(timeStr, TestTimeBean.class);
        Assert.assertEquals(testTimeBean1.getA(), testTimeBean2.getA());
        Assert.assertEquals(testTimeBean1.getB(), testTimeBean2.getB());
        Assert.assertEquals(testTimeBean1.getC(), testTimeBean2.getC());

        //测试变量无双引号
        Map<Integer, List<Integer>> productSkuMap = JsonUtil.parseObject("{1:[1,2,3],5:[10,11],6:[12,13,14]}", Map.class);
        Assert.assertNotNull(productSkuMap);

        StringKeyMap stringKeyMap = JsonUtil.parseMap("{1:[1,2,3],5:[10,11],6:[12,13,14]}");
        Assert.assertNotNull(stringKeyMap);
        Integer[] stringKeyMap1Value = stringKeyMap.getArray("6", Integer.class);
        Assert.assertArrayEquals(new Integer[]{12,13,14}, stringKeyMap1Value);

        //忽视transient
        checkTransient(json);
    }

    private void checkObject(StringKeyMap map){
        Assert.assertEquals("000", map.getString("code"));
        Assert.assertEquals("success", map.getString("msg"));
        Assert.assertNotNull(map.getStringKeyMap("body"));
        List<?> list = map.getStringKeyMap("body").getObject("list", List.class);
        Assert.assertNotNull(list);
        Assert.assertEquals(2, list.size());
        Assert.assertTrue(list.get(1) instanceof LinkedStringKeyMap);
        LinkedStringKeyMap map1 = (LinkedStringKeyMap)list.get(1);
        Assert.assertEquals("大学2", map1.getString("name"));
    }

    private void checkList1(List<LinkedStringKeyMap> list1){
        Assert.assertNotNull(list1);
        Assert.assertNotNull(list1.get(1));
        Assert.assertEquals(4, list1.get(1).getInteger("b").intValue());
    }

    private void checkList(List<?> list2){
        Assert.assertEquals(2, list2.size());
        Assert.assertTrue(list2.get(1) instanceof List);
        List<?> list22 = (List<?>)list2.get(1);
        Assert.assertEquals(2, list22.size());
        Assert.assertTrue(list22.get(1) instanceof StringKeyMap);
        StringKeyMap map2 = (StringKeyMap) list22.get(1);
        Assert.assertNotNull(map2.getInteger("a"));
        Assert.assertEquals(4, map2.getInteger("a").intValue());
    }

    private void checkTransient(Json json){
        class A {
            private String a;
            private transient String b;
            private Integer c;
            private String d;
            public String getA() {
                return a;
            }
            public String getB() {
                return b;
            }
            public Integer getC() {
                return c;
            }
        }

        A a = new A();
        a.a = "aaa";
        a.b = "bbb";
        a.c = 111;
        a.d = "ddd";

        String expect = "{\"a\":\"aaa\",\"c\":111}";

        String str1 = json.toJSONString(a);
        Assert.assertEquals(expect, str1);
    }

}

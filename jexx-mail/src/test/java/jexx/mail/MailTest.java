package jexx.mail;

import jexx.time.TimeInterval;
import jexx.util.Console;
import jexx.util.ResourceUtil;
import org.junit.Ignore;
import org.junit.Test;

public class MailTest {

    private String host = "smtp.163.com";
    private int port = 25;
    private String username = "jexxtest@163.com";
    private String password = "jexxtest123456";

    private String to = "271833997@qq.com";

    @Ignore
    @Test
    public void testSendSimpleMail() {
        Email email = Email.create()
                .from("jeff", username)
                .to("sunj", to)
                .subject("测试一下")
                .textMessage("这是文本")
        ;

        SmtpServer  smtpServer = MailServer.create()
                .host(host)
                .port(port)
                .auth(username, password)
//                .ssl(true)
                .starttls(true)
                .buildSmtpMailServer();

        SendMailSession sendMailSession = smtpServer.createSession();
        sendMailSession.open();
        sendMailSession.sendMail(email);
        sendMailSession.close();
    }

    @Ignore
    @Test
    public void testSendSimpleMailWithAttachment() {
        Email email = Email.create()
                .from("jeff", username)
                .to("sunj", to)
                .subject("测试一下")
                .textMessage("发送附件")
                .htmlMessage(
                        "<html><META http-equiv=Content-Type content=\"text/html; " +
                                "charset=utf-8\"><body><h1>Hey!</h1><img src='cid:a.png'>" +
                                "<h2>Hay!</h2></body></html>")
                .embeddedAttachment(
                        EmailAttachment.with()
                                .content(ResourceUtil.getStreamAsBytes("a.png"))
                                .name("a.png")
                )
                .attachment(
                        EmailAttachment.with()
                                .content(ResourceUtil.getStreamAsBytes("a.txt"))
                                .name("为了测试创建了一个一个很长的文件名.txt")
                );

        SmtpServer  smtpServer = MailServer.create()
                .host(host)
                .port(465)
                .ssl(true)
                .auth(username, password)
                .buildSmtpMailServer()
                ;

        SendMailSession sendMailSession = smtpServer.createSession();
        TimeInterval timeInterval = new TimeInterval();
        timeInterval.start();
        sendMailSession.open();
        Console.log("open session waste {}ms", timeInterval.getTaskTimeMillis());

        timeInterval.restart();
        sendMailSession.sendMail(email);
        Console.log("send email waste {}ms", timeInterval.getTaskTimeMillis());
        sendMailSession.close();
    }

}

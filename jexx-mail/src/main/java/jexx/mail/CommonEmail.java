package jexx.mail;

import jexx.util.ArrayUtil;
import jexx.util.StringPool;

import javax.activation.DataSource;
import javax.mail.Address;
import javax.mail.Header;
import java.util.*;

public abstract class CommonEmail<T extends CommonEmail<T>> {

    public static final String X_PRIORITY = "X-Priority";

    public static final int PRIORITY_HIGHEST = 1;
    public static final int PRIORITY_HIGH = 2;
    public static final int PRIORITY_NORMAL = 3;
    public static final int PRIORITY_LOW = 4;
    public static final int PRIORITY_LOWEST = 5;

    private EmailAddress from;

    @SuppressWarnings("unchecked")
    protected T _this() {
        return (T) this;
    }

    @Override
    public abstract T clone();

    public T from(final EmailAddress from) {
        this.from = from;
        return _this();
    }

    public T from(final Address from) {
        return from(EmailAddress.of(from));
    }

    public T from(final String from) {
        return from(EmailAddress.of(from));
    }

    public T from(final String personalName, final String from) {
        return from(new EmailAddress(personalName, from));
    }

    public EmailAddress from() {
        return from;
    }

    // ---------------------------------------------------------------- to

    private EmailAddress[] to = EmailAddress.EMPTY_ARRAY;

    public T to(final EmailAddress to) {
        this.to = ArrayUtil.append(this.to, to);
        return _this();
    }

    public T to(final String to) {
        return to(EmailAddress.of(to));
    }

    public T to(final String personalName, final String to) {
        return to(new EmailAddress(personalName, to));
    }

    public T to(final Address to) {
        return to(EmailAddress.of(to));
    }

    public T to(final EmailAddress... tos) {
        this.to = tos == null ? EmailAddress.EMPTY_ARRAY : tos;
        return _this();
    }

    public T to(final Address... tos) {
        return to(EmailAddress.of(tos));
    }

    public T to(final String... tos) {
        return to(EmailAddress.of(tos));
    }

    public EmailAddress[] to() {
        return to;
    }

    public T resetTo() {
        this.to = EmailAddress.EMPTY_ARRAY;
        return _this();
    }

    // ---------------------------------------------------------------- reply-to

    private EmailAddress[] replyTo = EmailAddress.EMPTY_ARRAY;

    public T replyTo(final EmailAddress replyTo) {
        this.replyTo = ArrayUtil.append(this.replyTo, replyTo);
        return _this();
    }

    public T replyTo(final String replyTo) {
        return replyTo(EmailAddress.of(replyTo));
    }

    public T replyTo(final String personalName, final String replyTo) {
        return replyTo(new EmailAddress(personalName, replyTo));
    }

    public T replyTo(final Address replyTo) {
        return replyTo(EmailAddress.of(replyTo));
    }

    public T replyTo(final String... replyTos) {
        return replyTo(EmailAddress.of(replyTos));
    }

    public T replyTo(final Address... replyTos) {
        return replyTo(EmailAddress.of(replyTos));
    }

    public T replyTo(final EmailAddress... replyTo) {
        this.replyTo = ArrayUtil.join(this.replyTo, replyTo);
        return _this();
    }

    public EmailAddress[] replyTo() {
        return replyTo;
    }

    public T resetReplyTo() {
        this.replyTo = EmailAddress.EMPTY_ARRAY;
        return _this();
    }

    // ---------------------------------------------------------------- cc

    /**
     * CC address.
     */
    private EmailAddress[] cc = EmailAddress.EMPTY_ARRAY;

    public T cc(final EmailAddress to) {
        this.cc = ArrayUtil.append(this.cc, to);
        return _this();
    }

    public T cc(final String cc) {
        return cc(EmailAddress.of(cc));
    }


    public T cc(final String personalName, final String cc) {
        return cc(new EmailAddress(personalName, cc));
    }

    public T cc(final Address cc) {
        return cc(EmailAddress.of(cc));
    }

    public T cc(final String... ccs) {
        return cc(EmailAddress.of(ccs));
    }

    public T cc(final Address... ccs) {
        return cc(EmailAddress.of(ccs));
    }

    public T cc(final EmailAddress... ccs) {
        this.cc = ArrayUtil.join(this.cc, ccs);
        return _this();
    }

    public EmailAddress[] cc() {
        return cc;
    }

    public T resetCc() {
        this.cc = EmailAddress.EMPTY_ARRAY;
        return _this();
    }

    // ---------------------------------------------------------------- subject

    /**
     * 邮件标题.
     */
    private String subject;

    /**
     * 邮件标题编码
     */
    private String subjectEncoding;

    public T subject(final String subject) {
        this.subject = subject;
        return _this();
    }

    public T subject(final String subject, final String encoding) {
        subject(subject);
        this.subjectEncoding = encoding;
        return _this();
    }

    public String subject() {
        return this.subject;
    }

    public String subjectEncoding() {
        return this.subjectEncoding;
    }

    // ---------------------------------------------------------------- message

    private final List<EmailMessage> messages = new ArrayList<>();

    public List<EmailMessage> messages() {
        return messages;
    }

    public T message(final List<EmailMessage> msgsToAdd) {
        messages.addAll(msgsToAdd);
        return _this();
    }

    public T message(final EmailMessage msgToAdd) {
        messages.add(msgToAdd);
        return _this();
    }

    public T message(final String text, final String mimeType, final String encoding) {
        return message(new EmailMessage(text, mimeType, encoding));
    }

    public T message(final String text, final String mimeType) {
        return message(new EmailMessage(text, mimeType));
    }

    public T textMessage(final String text) {
        return message(text, "text/plain");
    }

    public T textMessage(final String text, final String encoding) {
        return message(new EmailMessage(text, "text/plain", encoding));
    }

    public T htmlMessage(final String html) {
        return message(new EmailMessage(html, "text/html"));
    }

    public T htmlMessage(final String html, final String encoding) {
        return message(new EmailMessage(html, "text/html", encoding));
    }

    // ---------------------------------------------------------------- headers
    private final Map<String, String> headers = new HashMap<>();

    protected Map<String, String> headers() {
        return headers;
    }

    public T header(final String name, final String value) {
        headers.put(name, value);
        return _this();
    }

    public T headers(final Map<String, String> headersToSet) {
        headers.putAll(headersToSet);
        return _this();
    }


    public T headers(final Enumeration<Header> headersToSet) {
        while (headersToSet.hasMoreElements()) {
            final Header header = headersToSet.nextElement();
            header(header.getName(), header.getValue());
        }
        return _this();
    }

    public String header(final String name) {
        return headers.get(name);
    }

    public T priority(final int priority) {
        header(X_PRIORITY, String.valueOf(priority));
        return _this();
    }

    public int priority() {
        try {
            return Integer.parseInt(headers.get(X_PRIORITY));
        } catch (final NumberFormatException ignore) {
            return -1;
        }
    }

    // ---------------------------------------------------------------- attachments
    private final List<EmailAttachment<? extends DataSource>> attachments = new ArrayList<>();


    public List<EmailAttachment<? extends DataSource>> attachments() {
        return attachments;
    }


    protected T storeAttachments(final List<EmailAttachment<? extends DataSource>> attachments) {
        this.attachments.addAll(attachments);
        return _this();
    }

    /**
     * 添加附件
     * @param attachment 附件
     */
    protected T storeAttachment(final EmailAttachment<? extends DataSource> attachment) {
        this.attachments.add(attachment);
        return _this();
    }

    public T attachments(final List<EmailAttachment<? extends DataSource>> attachments) {
        for (final EmailAttachment<?> attachment : attachments) {
            attachment(attachment);
        }
        return _this();
    }

    public T attachment(final EmailAttachment<? extends DataSource> attachment) {
        attachment.setContentId(null);
        return storeAttachment(attachment);
    }

    public T attachment(final EmailAttachmentBuilder builder) {
        return attachment(builder.buildByteArrayDataSource());
    }

    /**
     * 添加内置附件
     * @param builder 附件构造器
     * @return this
     */
    public T embeddedAttachment(final EmailAttachmentBuilder builder) {
        builder.setContentIdFromNameIfMissing();
        builder.inline(true);
        return embeddedAttachment(builder.buildByteArrayDataSource());
    }

    /**
     * 添加内置附件,并附加在最后一个{@link EmailMessage}上
     * @param attachment {@link EmailAttachment}
     * @return this
     */
    public T embeddedAttachment(final EmailAttachment<? extends DataSource> attachment) {
        storeAttachment(attachment);

        final List<EmailMessage> messages = messages();
        final int size = messages.size();
        if (size > 1) {
            final int lastMessagePos = size - 1;
            final EmailMessage lastMessage = messages.get(lastMessagePos);
            attachment.setEmbeddedMessage(lastMessage);
        }
        return _this();
    }

    // ---------------------------------------------------------------- date

    private Date sentDate;

    public T sentDate(final Date date) {
        sentDate = date;
        return _this();
    }

    public Date sentDate() {
        return sentDate;
    }

    // ---------------------------------------------------------------- toString

    @Override
    public String toString() {
        return "Email{'" + from() + "\', subject='" + subject() + "\'}";
    }

}

package jexx.mail;

import jexx.util.ArrayUtil;
import jexx.util.Console;

import javax.mail.Address;
import java.util.Date;

public class Email extends CommonEmail<Email> {

    public static Email create() {
        return new Email();
    }

    @Override
    public Email clone() {
        return create()

                // from / reply-to
                .from(from())
                .replyTo(replyTo())

                // recipients
                .to(to())
                .cc(cc())
                .bcc(bcc())

                // subject
                .subject(subject(), subjectEncoding())

                // dates
                .sentDate(sentDate())

                // headers - includes priority
                .headers(headers())

                // content / attachments
                .storeAttachments(attachments())
                .message(messages());
    }

    public Email currentSentDate() {
        return sentDate(new Date());
    }

    // ---------------------------------------------------------------- bcc

    /** 密送;收件人不知道这封邮件还发给了其他人 */
    private EmailAddress[] bcc = EmailAddress.EMPTY_ARRAY;

    public Email bcc(final EmailAddress to) {
        this.bcc = ArrayUtil.append(this.bcc, to);
        return _this();
    }

    public Email bcc(final String bcc) {
        return bcc(EmailAddress.of(bcc));
    }

    public Email bcc(final String personalName, final String bcc) {
        return bcc(new EmailAddress(personalName, bcc));
    }

    public Email bcc(final Address bcc) {
        return bcc(EmailAddress.of(bcc));
    }

    public Email bcc(final String... bccs) {
        return bcc(EmailAddress.of(bccs));
    }

    public Email bcc(final Address... bccs) {
        return bcc(EmailAddress.of(bccs));
    }

    public Email bcc(final EmailAddress... bccs) {
        this.bcc = ArrayUtil.join(this.bcc, bccs);
        return _this();
    }

    public EmailAddress[] bcc() {
        return bcc;
    }

    public Email resetBcc() {
        this.bcc = EmailAddress.EMPTY_ARRAY;
        return _this();
    }


}

package jexx.mail;

import jexx.util.StringPool;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.util.*;

public class SendMailSession extends MailSession<Transport> {

    private static final String ALTERNATIVE = "alternative";
    private static final String RELATED = "related";
    private static final String CHARSET = ";charset=";
    private static final String INLINE = "inline";

    public SendMailSession(Session session, Transport transport) {
        super(session, transport);
    }

    @Override
    public Transport getService() {
        return service;
    }

    public String sendMail(final Email email) {
        try {
            final MimeMessage msg = createMessage(email);
            getService().sendMessage(msg, msg.getAllRecipients());
            return msg.getMessageID();
        } catch (final MessagingException e) {
            e.printStackTrace();
            throw new MailException(e, "Failed to send email: {}", email);
        }
    }

    protected MimeMessage createMessage(final Email email) throws MessagingException {
        final Email clone = email.clone();

        final MimeMessage newMsg = new MimeMessage(getSession());

        setPeople(clone, newMsg);
        setSubject(clone, newMsg);
        setSentDate(clone, newMsg);
        setHeaders(clone, newMsg);
        addBodyData(clone, newMsg);
        return newMsg;
    }

    private void setPeople(final Email emailWithData, final MimeMessage msgToSet) throws MessagingException {
        EmailAddress from = emailWithData.from();
        if(from != null){
            msgToSet.setFrom(from.toInternetAddress());
        }

        msgToSet.setReplyTo(EmailAddress.convert(emailWithData.replyTo()));
        setRecipients(emailWithData, msgToSet);
    }

    private void setRecipients(final Email emailWithData, final MimeMessage msgToSet) throws MessagingException {
        // TO
        final InternetAddress[] to = EmailAddress.convert(emailWithData.to());
        if (to.length > 0) {
            msgToSet.setRecipients(Message.RecipientType.TO, to);
        }

        // CC
        final InternetAddress[] cc = EmailAddress.convert(emailWithData.cc());
        if (cc.length > 0) {
            msgToSet.setRecipients(Message.RecipientType.CC, cc);
        }

        // BCC
        final InternetAddress[] bcc = EmailAddress.convert(emailWithData.bcc());
        if (bcc.length > 0) {
            msgToSet.setRecipients(Message.RecipientType.BCC, bcc);
        }
    }

    private void setSubject(final Email emailWithData, final MimeMessage msgToSet) throws MessagingException {
        if (emailWithData.subjectEncoding() != null) {
            msgToSet.setSubject(emailWithData.subject(), emailWithData.subjectEncoding());
        } else {
            msgToSet.setSubject(emailWithData.subject());
        }
    }

    private void setSentDate(final Email emailWithData, final MimeMessage msgToSet) throws MessagingException {
        Date date = emailWithData.sentDate();
        if (date == null) {
            date = new Date();
        }
        msgToSet.setSentDate(date);
    }

    private void setHeaders(final Email emailWithData, final MimeMessage msgToSet) throws MessagingException {
        final Map<String, String> headers = emailWithData.headers();
        if (headers != null) {
            for (final Map.Entry<String, String> entry : headers.entrySet()) {
                msgToSet.setHeader(entry.getKey(), entry.getValue());
            }
        }
    }

    private void addBodyData(final Email emailWithData, final MimeMessage msgToSet) throws MessagingException {
        final List<EmailMessage> messages = emailWithData.messages();

        final int totalMessages = messages.size();

        final List<EmailAttachment<? extends DataSource>> attachments = new ArrayList<>(emailWithData.attachments());

        if (attachments.isEmpty() && totalMessages == 1) {
            // 特殊情况: 无附件,以及只有一个消息
            setContent(messages.get(0), msgToSet);

        } else {
            //mixed
            final MimeMultipart multipart = new MimeMultipart();
            msgToSet.setContent(multipart);

            /**
             * MINE消息体; multipart类型主要有三种子类型：mixed、alternative、related
             */
            final MimeMultipart msgMultipart = new MimeMultipart(ALTERNATIVE);
            multipart.addBodyPart(getBaseBodyPart(msgMultipart));
            for (final EmailMessage emailMessage : messages) {
                msgMultipart.addBodyPart(getBodyPart(emailMessage, attachments));
            }

            addAnyAttachments(attachments, multipart);

            msgToSet.setContent(multipart);
        }
    }

    private void setContent(final EmailMessage emailWithData, final Part partToSet) throws MessagingException {
        partToSet.setContent(emailWithData.getContent(), emailWithData.getMimeType() + CHARSET + emailWithData.getEncoding());
    }
    private MimeBodyPart getBaseBodyPart(final MimeMultipart msgMultipart) throws MessagingException {
        final MimeBodyPart bodyPart = new MimeBodyPart();
        bodyPart.setContent(msgMultipart);
        return bodyPart;
    }

    private MimeBodyPart getBodyPart(final EmailMessage emailMessage, final List<EmailAttachment<? extends DataSource>> attachments) throws MessagingException {

        final MimeBodyPart bodyPart = new MimeBodyPart();

        // 查找内嵌附件
        final List<EmailAttachment<? extends DataSource>> embeddedAttachments = filterEmbeddedAttachments(attachments, emailMessage);

        if (embeddedAttachments.isEmpty()) {
            // 仅添加消息
            setContent(emailMessage, bodyPart);
        } else {
            attachments.removeAll(embeddedAttachments);

            // embedded attachments detected, join them as related
            final MimeMultipart relatedMultipart = new MimeMultipart(RELATED);

            final MimeBodyPart messageData = new MimeBodyPart();

            setContent(emailMessage, messageData);

            relatedMultipart.addBodyPart(messageData);

            addAnyAttachments(embeddedAttachments, relatedMultipart);

            bodyPart.setContent(relatedMultipart);
        }

        return bodyPart;
    }

    protected MimeBodyPart createAttachmentBodyPart(final EmailAttachment<? extends DataSource> attachment) throws MessagingException {
        final MimeBodyPart part = new MimeBodyPart();

        final String attachmentName = attachment.getEncodedName();
        if (attachmentName != null) {
            part.setFileName(attachmentName);
        }

        part.setDataHandler(new DataHandler(attachment.getDataSource()));

        if (attachment.getContentId() != null) {
            part.setContentID(StringPool.LEFT_CHEV + attachment.getContentId() + StringPool.RIGHT_CHEV);
        }
        if (attachment.isInline()) {
            part.setDisposition(INLINE);
        }

        return part;
    }

    protected List<EmailAttachment<? extends DataSource>> filterEmbeddedAttachments(final List<EmailAttachment<? extends DataSource>> attachments, final EmailMessage emailMessage) {
        final List<EmailAttachment<? extends DataSource>> embeddedAttachments = new ArrayList<>();

        if (attachments == null || attachments.isEmpty() || emailMessage == null) {
            return embeddedAttachments;
        }

        final Iterator<EmailAttachment<? extends DataSource>> iterator = attachments.iterator();

        while (iterator.hasNext()) {
            final EmailAttachment<? extends DataSource> emailAttachment = iterator.next();

            if (emailAttachment.isEmbeddedInto(emailMessage)) {
                embeddedAttachments.add(emailAttachment);
                iterator.remove();
            }
        }

        return embeddedAttachments;
    }

    private void addAnyAttachments(final List<EmailAttachment<? extends DataSource>> attachments, final MimeMultipart multipart) throws MessagingException {
        for (final EmailAttachment<? extends DataSource> attachment : attachments) {
            final MimeBodyPart bodyPart = createAttachmentBodyPart(attachment);
            multipart.addBodyPart(bodyPart);
        }
    }





}

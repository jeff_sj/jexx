package jexx.mail;

import jexx.util.StringPool;

import java.util.Properties;

/**
 * JavaMail: SSL vs TLS vs STARTTLS =====> https://www.cnblogs.com/qingwen/p/5513090.html
 *              http://wemedia.ifeng.com/59490954/wemedia.shtml
 * JavaMail属性 =====> https://blog.csdn.net/u011479200/article/details/79232736
 */
public class SmtpSslServer extends SmtpServer {

    protected static final int DEFAULT_SSL_PORT = 465;

    public SmtpSslServer(Builder builder) {
        super(builder, DEFAULT_SSL_PORT);
    }

    /** 是否开启tls; tls是ssl的继承者,是同一协议的不同版本; starttls指把纯文本转换为tls的加密 */
    protected boolean startTlsRequired = false;

    protected boolean startSsl = false;

    /**
     * Sets <code>mail.smtp.starttls.required</code>.
     * <p>
     * If the server doesn't support the STARTTLS command, or the command fails,
     * the connect method will fail. Defaults to {@code false}.
     *
     * @param startTlsRequired If {@code true}, requires the use of the STARTTLS command.
     * @return this
     */
    public SmtpSslServer startTlsRequired(final boolean startTlsRequired) {
        this.startTlsRequired = startTlsRequired;
        return this;
    }

    public SmtpSslServer startSsl(final boolean startSsl) {
        this.startSsl = startSsl;
        return this;
    }

    @Override
    protected Properties createSessionProperties() {
        final Properties props = super.createSessionProperties();

        if(startTlsRequired){
            props.setProperty(MAIL_SMTP_STARTTLS_REQUIRED, StringPool.TRUE);
            props.setProperty(MAIL_SMTP_STARTTLS_ENABLE, StringPool.TRUE);
        }

        if (startSsl) {
            props.setProperty(MAIL_SMTP_SOCKET_FACTORY_CLASS, "javax.net.ssl.SSLSocketFactory");
            props.setProperty(MAIL_SMTP_SOCKET_FACTORY_FALLBACK, StringPool.FALSE);
            props.setProperty(MAIL_SMTP_SOCKET_FACTORY_PORT, String.valueOf(port));
        }
        return props;
    }

}

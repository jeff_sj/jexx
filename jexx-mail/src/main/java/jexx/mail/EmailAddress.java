package jexx.mail;

import jexx.lang.Charsets;
import jexx.util.ArrayUtil;
import jexx.util.StringUtil;

import javax.mail.Address;
import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import java.io.UnsupportedEncodingException;

public class EmailAddress {

    public static final EmailAddress[] EMPTY_ARRAY = new EmailAddress[0];

    private final String email;

    private final String personalName;

    public EmailAddress(final String personalName, final String email) {
        this.email = email;
        this.personalName = personalName;
    }

    public static EmailAddress of(final String personalName, final String email) {
        return new EmailAddress(personalName, email);
    }

    /**
     * 根据指定地址创建EmailAddress
     * <ul>
     * <li>{@code "foo@bar.com" - 仅邮件地址.}</li>
     * <li>{@code "Jenny Doe <foo@bar.com>" - 第一部分个人名字,另一部分为邮件}</li>
     * </ul>
     * @param address 地址
     */
    public static EmailAddress of(String address) {
        address = address.trim();

        if (!StringUtil.endWithChar(address, '>')) {
            return new EmailAddress(null, address);
        }

        final int ndx = address.lastIndexOf('<');
        if (ndx == -1) {
            return new EmailAddress(null, address);
        }

        String email = address.substring(ndx + 1, address.length() - 1);
        String personalName = address.substring(0, ndx).trim();
        return new EmailAddress(personalName, email);
    }

    /**
     * 根据 {@link InternetAddress} 创建地址
     *
     * @param internetAddress {@link InternetAddress} to convert
     */
    public static EmailAddress of(final InternetAddress internetAddress) {
        return new EmailAddress(internetAddress.getPersonal(), internetAddress.getAddress());
    }

    /**
     * 根据 {@link InternetAddress} 创建地址
     *
     * @param address {@link Address} to convert.
     */
    public static EmailAddress of(final Address address) {
        return of(address.toString());
    }

    public String getEmail() {
        return email;
    }

    public String getPersonalName() {
        return personalName;
    }

    @Override
    public String toString() {
        if (this.personalName == null) {
            return this.email;
        }
        return this.personalName + " <" + this.email + '>';
    }

    public InternetAddress toInternetAddress() throws AddressException {
        try {
            return new InternetAddress(email, personalName, Charsets.UTF_8.name());
        } catch (final UnsupportedEncodingException ueex) {
            throw new AddressException(ueex.toString());
        }
    }

    public static EmailAddress[] of(final Address... addresses) {
        if (addresses == null) {
            return EmailAddress.EMPTY_ARRAY;
        }
        if (addresses.length == 0) {
            return EmailAddress.EMPTY_ARRAY;
        }

        final EmailAddress[] res = new EmailAddress[addresses.length];

        for (int i = 0; i < addresses.length; i++) {
            res[i] = EmailAddress.of(addresses[i]);
        }

        return res;
    }

    public static EmailAddress[] of(final String... addresses) {
        if (ArrayUtil.isEmpty(addresses)) {
            return EmailAddress.EMPTY_ARRAY;
        }

        final EmailAddress[] res = new EmailAddress[addresses.length];

        for (int i = 0; i < addresses.length; i++) {
            res[i] = EmailAddress.of(addresses[i]);
        }
        return res;
    }

    public static InternetAddress[] convert(final EmailAddress[] addresses) throws MessagingException {
        if (addresses == null) {
            return new InternetAddress[0];
        }

        final int numRecipients = addresses.length;
        final InternetAddress[] address = new InternetAddress[numRecipients];

        for (int i = 0; i < numRecipients; i++) {
            address[i] = addresses[i].toInternetAddress();
        }
        return address;
    }


}

package jexx.mail;

/**
 * 邮件内容
 */
public class EmailMessage {

    private final String content;

    private final String mimeType;

    private final String encoding;

    public EmailMessage(String content, String mimeType, String encoding) {
        this.content = content;
        this.mimeType = mimeType;
        this.encoding = encoding;
    }

    public EmailMessage(final String content, final String mimeType) {
        this(content, mimeType, "UTF-8");
        if(content == null){
            throw new MailException("EmailMessage's content is not empty");
        }
    }

    public String getContent() {
        return content;
    }

    public String getMimeType() {
        return mimeType;
    }

    public String getEncoding() {
        return encoding;
    }
}

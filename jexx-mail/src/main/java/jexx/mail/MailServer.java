package jexx.mail;

import javax.mail.Authenticator;
import java.util.Objects;
import java.util.Properties;

public abstract class MailServer<MailSessionImpl extends MailSession> {

    public static final String MAIL_HOST = "mail.host";

    /**
     * 分割长参数, 默认true, 会导致附件长度太长从而导致被截
     */
    public static final String MAIL_MIME_SPLITLONGPARAMETERS = "mail.mime.splitlongparameters";

    public static final String MAIL_SMTP_HOST = "mail.smtp.host";
    public static final String MAIL_SMTP_PORT = "mail.smtp.port";
    public static final String MAIL_SMTP_AUTH = "mail.smtp.auth";
    public static final String MAIL_TRANSPORT_PROTOCOL = "mail.transport.protocol";

    public static final String MAIL_SMTP_CONNECTIONTIMEOUT = "mail.smtp.connectiontimeout";
    public static final String MAIL_SMTP_TIMEOUT = "mail.smtp.timeout";
    public static final String MAIL_SMTP_WRITETIMEOUT = "mail.smtp.writetimeout";

    public static final String MAIL_SMTP_STARTTLS_REQUIRED = "mail.smtp.starttls.required";
    public static final String MAIL_SMTP_STARTTLS_ENABLE = "mail.smtp.starttls.enable";
    public static final String MAIL_SMTP_SOCKET_FACTORY_PORT = "mail.smtp.socketFactory.port";
    public static final String MAIL_SMTP_SOCKET_FACTORY_CLASS = "mail.smtp.socketFactory.class";
    /**
     * 如果设置为true, 则使用指定的socketFactory创建socket失败的话,则使用java.net.Socket. 默认true
     */
    public static final String MAIL_SMTP_SOCKET_FACTORY_FALLBACK = "mail.smtp.socketFactory.fallback";

    public static final String MAIL_DEBUG = "mail.debug";

    protected final String host;
    protected final int port;
    protected final Authenticator authenticator;
    /**
     * 邮件连接,接收,发送 超时时间
     */
    protected final int timeout;
    protected final Properties customProperties;
    protected final boolean debugMode;

    protected MailServer(final Builder builder, final int defaultPort) {
        Objects.requireNonNull(builder.host, "Host cannot be null");

        this.host = builder.host;
        this.port = builder.port == -1 ? defaultPort : builder.port;
        this.authenticator = builder.authenticator;
        this.timeout = builder.timeout;
        this.debugMode = builder.debug;
        this.customProperties = builder.customProperties;

        initSystemProperties();
    }

    protected void initSystemProperties(){
        //solve that the attachment's name in the email is wrong if the attachment's name is too long
        System.setProperty(MAIL_MIME_SPLITLONGPARAMETERS, "false");
    }

    protected Properties createSessionProperties() {
        final Properties props = new Properties();

        props.putAll(customProperties);

        if (debugMode) {
            props.put(MAIL_DEBUG, "true");
        }

        return props;
    }


    /** 创建session */
    public abstract MailSessionImpl createSession();

    /** 创建新的构建器 */
    public static Builder create() {
        return new Builder();
    }

    @Override
    public String toString() {
        return "MailServer{" +
                "host='" + host + '\'' +
                ", port=" + port +
                ", timeout=" + timeout +
                ", customProperties=" + customProperties +
                ", debugMode=" + debugMode +
                '}';
    }

    public static class Builder{
        private String host = null;
        private int port = -1;
        private boolean ssl = false;
        private boolean starttls = false;
        private Authenticator authenticator;
        //默认5分钟超时
        private int timeout = 5*60*1000;
        private Properties customProperties = new Properties();

        private boolean debug;

        public Builder host(final String host) {
            this.host = host;
            return this;
        }

        public Builder port(final int port) {
            this.port = port;
            return this;
        }

        public Builder ssl(final boolean ssl) {
            this.ssl = ssl;
            return this;
        }

        public Builder starttls(final boolean starttls) {
            this.starttls = starttls;
            return this;
        }

        public Builder debugMode(final boolean debug) {
            this.debug = debug;
            return this;
        }

        public Builder auth(final String username, final String password) {
            Objects.requireNonNull(username, "Username cannot be null");
            Objects.requireNonNull(password, "Password cannot be null");
            return auth(new SimpleAuthenticator(username, password));
        }

        /**
         * Sets the authenticator.
         *
         * @param authenticator {@link Authenticator} to set.
         * @return this
         */
        public Builder auth(final Authenticator authenticator) {
            this.authenticator = authenticator;
            return this;
        }

        public Builder timeout(final int timeout) {
            this.timeout = timeout;
            return this;
        }

        /**
         * 添加属性
         */
        public Builder property(final String name, final String value) {
            this.customProperties.put(name, value);
            return this;
        }


        public SmtpServer buildSmtpMailServer() {
            if (ssl || starttls) {
                SmtpSslServer smtpSslServer = new SmtpSslServer(this);
                smtpSslServer.startTlsRequired(starttls);
                smtpSslServer.startSsl(ssl);
                return smtpSslServer;
            }
            return new SmtpServer(this);
        }

    }

}

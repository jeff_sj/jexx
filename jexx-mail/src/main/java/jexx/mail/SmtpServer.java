package jexx.mail;

import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import javax.mail.Transport;
import java.util.Properties;

public class SmtpServer extends MailServer<SendMailSession> {

    protected static final String PROTOCOL_SMTP = "smtp";
    protected static final int DEFAULT_SMTP_PORT = 25;

    public SmtpServer(Builder builder) {
        super(builder, DEFAULT_SMTP_PORT);
    }

    public SmtpServer(Builder builder, int defaultPort) {
        super(builder, defaultPort);
    }

    @Override
    protected Properties createSessionProperties() {
        final Properties props = super.createSessionProperties();

        props.setProperty(MAIL_TRANSPORT_PROTOCOL, PROTOCOL_SMTP);
        props.setProperty(MAIL_HOST, host);
        props.setProperty(MAIL_SMTP_HOST, host);
        props.setProperty(MAIL_SMTP_PORT, String.valueOf(port));

        if (authenticator != null) {
            props.setProperty(MAIL_SMTP_AUTH, "TRUE");
        }

        if (timeout > 0) {
            final String timeoutValue = String.valueOf(timeout);
            props.put(MAIL_SMTP_CONNECTIONTIMEOUT, timeoutValue);
            props.put(MAIL_SMTP_TIMEOUT, timeoutValue);
            props.put(MAIL_SMTP_WRITETIMEOUT, timeoutValue);
        }

        props.put(MAIL_MIME_SPLITLONGPARAMETERS, "false");

        return props;
    }

    @Override
    public SendMailSession createSession() {
        final Session session = Session.getInstance(createSessionProperties(), authenticator);
        final Transport mailTransport;
        try {
            mailTransport = getTransport(session);
        } catch (final NoSuchProviderException e) {
            throw new MailException(e);
        }
        return new SendMailSession(session, mailTransport);
    }

    protected Transport getTransport(final Session session) throws NoSuchProviderException {
        return session.getTransport(PROTOCOL_SMTP);
    }

}

package jexx.mail;

import jexx.util.StringUtil;

public class MailException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public MailException() {
        super();
    }

    public MailException(String message) {
        super(message);
    }

    public MailException(String message, Object ...params) {
        super(StringUtil.format(message, params));
    }

    public MailException(Throwable cause) {
        super(cause);
    }

    public MailException(Throwable throwable, String message, Object... params) {
        super(StringUtil.format(message, params), throwable);
    }

}

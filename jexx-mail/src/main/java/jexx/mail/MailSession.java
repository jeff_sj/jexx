package jexx.mail;

import javax.mail.MessagingException;
import javax.mail.Service;
import javax.mail.Session;

public abstract class MailSession<T extends Service> implements AutoCloseable {

    private final Session session;
    protected final T service;

    protected MailSession(final Session session, final T service) {
        this.session = session;
        this.service = service;
    }

    /**
     * 打开 session.
     */
    public void open() {
        try {
            service.connect();
        } catch (final MessagingException e) {
            throw new MailException(e, "Open session error");
        }
    }

    @Override
    public void close() {
        try {
            service.close();
        } catch (final MessagingException e) {
            throw new MailException(e, "Failed to close session");
        }
    }

    /** 是否连接 */
    public boolean isConnected() {
        return service.isConnected();
    }

    public Session getSession() {
        return session;
    }

    public abstract T getService();

}

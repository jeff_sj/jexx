# 介绍
## maven 使用

```
<jexx.version>1.1.6-RELEASE</jexx.version>

<dependency>
    <groupId>cn.jexxs</groupId>
	<artifactId>jexx-all</artifactId>
	<version>${jexx.version}</version>
</dependency>
<dependency>
    <groupId>cn.jexxs</groupId>
	<artifactId>jexx-core</artifactId>
	<version>${jexx.version}</version>
</dependency>
<dependency>
    <groupId>cn.jexxs</groupId>
	<artifactId>jexx-poi</artifactId>
	<version>${jexx.version}</version>
</dependency>
<dependency>
    <groupId>cn.jexxs</groupId>
	<artifactId>jexx-template</artifactId>
	<version>${jexx.version}</version>
</dependency>
<dependency>
    <groupId>cn.jexxs</groupId>
	<artifactId>jexx-mail</artifactId>
	<version>${jexx.version}</version>
</dependency>
<dependency>
    <groupId>cn.jexxs</groupId>
	<artifactId>jexx-mail</artifactId>
	<version>${jexx.version}</version>
</dependency>
<dependency>
    <groupId>cn.jexxs</groupId>
	<artifactId>jexx-json</artifactId>
	<version>${jexx.version}</version>
</dependency>
```

## 部署
    gradlew clean test install uploadArchives

## 参考学习 
hutool
jodd

## git小技巧
    git remote add origin-chiway http://192.168.37.168:3000/jeff/jexx.git
    git remote -v
    git push origin
    git push origin-chiway
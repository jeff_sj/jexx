package jexx.http;

import org.junit.Assert;
import org.junit.Test;

public class ContentDispositionTest {

    @Test
    public void testParse(){
        String ct = "form-data; name=\"testFile\"; filename=\"文件XXX.zip\"";
        ContentDisposition contentDisposition = ContentDisposition.parse(ct);
        Assert.assertNotNull(contentDisposition);
        Assert.assertEquals("form-data", contentDisposition.getType());
        Assert.assertEquals("testFile", contentDisposition.getName());
        Assert.assertEquals("文件XXX.zip", contentDisposition.getFilename());
        Assert.assertEquals(ct, contentDisposition.toString());
    }

}

package jexx.http;

import jexx.util.Console;
import org.junit.Test;

import java.net.URI;

public class URITest {

    @Test
    public void testUri(){
        print(URI.create("www.baidu.com"));
        print(URI.create("http://www.baidu.com"));
        print(URI.create("http://192.168.1.1:8080/user/info?a=1&b=2"));
        print(URI.create("mailto:a@b.com"));
        print(URI.create("scheme:example.com/path"));
        print(URI.create("path"));
        print(URI.create("/path"));
    }

    private void print(URI uri){
        boolean absolute = uri.isAbsolute();
        boolean opaque = uri.isOpaque();
        String scheme = uri.getScheme();
        String fragment = uri.getFragment();
        String userInfo = uri.getUserInfo();
        String host = uri.getHost();
        int port = uri.getPort();
        String path = uri.getPath();
        String query = uri.getQuery();
        Console.log("absolute={}, qpaque={}, scheme={}, fragment={}, userInfo={}, host={}, port={}, path={}, query={}", absolute, opaque,
                scheme, fragment, userInfo, host, port, path, query);
    }

}

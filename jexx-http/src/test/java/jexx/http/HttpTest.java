package jexx.http;

import jexx.http.client.HttpComponentsClientHttpRequestFactory;
import jexx.http.client.SimpleClientHttpRequestFactory;
import jexx.http.exception.HttpException;
import jexx.util.Console;
import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

public class HttpTest {

    @Test
    public void testSimpleClient(){
        testHttp(HttpBuilder.create().requestFactory(new SimpleClientHttpRequestFactory()).build());
        testHttps(HttpBuilder.create().requestFactory(new SimpleClientHttpRequestFactory()).sslIgnore().build());
    }

    @Test
    public void testHttpComponents(){
        testHttp(HttpBuilder.create().requestFactory(new HttpComponentsClientHttpRequestFactory()).build());
        testHttps(HttpBuilder.create().requestFactory(new HttpComponentsClientHttpRequestFactory()).sslIgnore().build());
    }

    private void testHttp(Http http){
        try {
            ResponseEntity<String> response = http.postForEntity("http://www.qq.com", null, String.class);
            Console.log(response);
        } catch (HttpException e) {
            Assert.fail("接口异常");
        }

        HttpHeaders headers1 = new HttpHeaders();
        headers1.setContentType(ContentType.APPLICATION_JSON);

        Map<String, String> map1 = new HashMap<>(2);
        map1.put("userAccount", "admin");
        map1.put("userPassword", "admin@321");

//        RequestEntity<Map<String, String>> requestEntity = new RequestEntity<>(map1, headers1);
//        ResponseEntity<Object> responseEntity = http.exchangeForEntity("http://10.5.96.62:9090/rschool/api/auth/login", HttpMethod.POST, requestEntity, Object.class);
//        Assert.assertEquals(200, responseEntity.getStatus());
    }

    private void testHttps(Http http){
        try {
            ResponseEntity<String> response = http.postForEntity("https://chiwayschool.com/rschool/login", null, String.class);
            Console.log(response);
        } catch (HttpException e) {
            Console.log(e);
            Assert.fail("接口异常");
        }
    }

}

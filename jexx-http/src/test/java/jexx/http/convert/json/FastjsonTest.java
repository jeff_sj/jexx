package jexx.http.convert.json;

import com.alibaba.fastjson.JSON;
import jexx.util.Console;
import org.junit.Assert;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class FastjsonTest {

    @Test
    public void testJson() throws IOException {
        TestBean bean = new TestBean();
        bean.setId(1);
        bean.setName("ddd");

        String json = JSON.toJSONString(bean);
        Console.log("json={}", json);

        ByteArrayInputStream inputStream = new ByteArrayInputStream(json.getBytes());
        TestBean bean1 = JSON.parseObject(inputStream, TestBean.class);
        Assert.assertNotNull(bean1);
        Assert.assertEquals(bean.getId(), bean1.getId());
        Assert.assertEquals(bean.getName(), bean1.getName());
    }

    private static class TestBean{

        private int id;
        private String name;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

}

package jexx.http.client;

import jexx.http.HttpHeaders;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

public class TestClientHttpResponse implements ClientHttpResponse{

    private final byte[] response;
    private final HttpHeaders httpHeaders;

    public TestClientHttpResponse(HttpHeaders httpHeaders, byte[] response) {
        this.response = response;
        this.httpHeaders = httpHeaders;
    }

    @Override
    public HttpHeaders getHeaders() {
        return this.httpHeaders;
    }

    @Override
    public InputStream getBody() throws IOException {
        return new ByteArrayInputStream(response);
    }

    @Override
    public int getStatus() throws IOException {
        return 0;
    }

    @Override
    public void close() {

    }
}

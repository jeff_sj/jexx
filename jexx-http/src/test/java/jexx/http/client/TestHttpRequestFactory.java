package jexx.http.client;

import jexx.http.DefaultHttp;
import jexx.http.Http;
import jexx.http.HttpHeaders;
import jexx.http.HttpMethod;

import java.io.IOException;
import java.net.URI;

public class TestHttpRequestFactory implements ClientHttpRequestFactory{

    private final TestClientHttpRequest request;

    public TestHttpRequestFactory(TestClientHttpRequest request) {
        this.request = request;
    }

    @Override
    public ClientHttpRequest createRequest(URI uri, HttpMethod httpMethod) throws IOException {
        return request;
    }

    public static Http createHttp(HttpHeaders headers, String response){
        TestClientHttpResponse testClientHttpResponse = new TestClientHttpResponse(headers, response.getBytes());
        TestClientHttpRequest testClientHttpRequest = new TestClientHttpRequest(testClientHttpResponse);
        return new DefaultHttp(new TestHttpRequestFactory(testClientHttpRequest));
    }

}

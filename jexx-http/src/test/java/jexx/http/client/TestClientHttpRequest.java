package jexx.http.client;

import jexx.http.HttpHeaders;
import jexx.http.RequestBody;
import jexx.util.Console;
import jexx.util.StringUtil;

import java.io.IOException;
import java.io.OutputStream;
import java.net.URI;
import java.nio.ByteBuffer;

public class TestClientHttpRequest extends AbstractClientHttpRequest {

    private final TestClientHttpResponse response;
    private final TestOutputStream outputStream;

    public TestClientHttpRequest(TestClientHttpResponse response) {
        this.response = response;
        this.outputStream = new TestOutputStream();
    }

    @Override
    public RequestBody getBody() {
        return null;
    }

    @Override
    protected ClientHttpResponse executeInternal(HttpHeaders headers) throws IOException {
        return response;
    }

    @Override
    public URI getURI() {
        return null;
    }

    @Override
    public String getMethodValue() {
        return null;
    }



    private static class TestOutputStream extends OutputStream{

        ByteBuffer byteBuffer = ByteBuffer.allocate(1024);

        @Override
        public void write(int b) throws IOException {
            byteBuffer.put((byte) b);
            if(byteBuffer.remaining() <= 0){
                byteBuffer.compact();
                byte[] cc = new byte[1024];
                byteBuffer.get(cc, 0, cc.length);
                Console.log("=======================start===============================");
                Console.log(StringUtil.str(cc));
                Console.log("========================end==============================");
                byteBuffer.flip();
            }
        }

        @Override
        public void flush() throws IOException {

        }
    }

}

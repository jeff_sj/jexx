package jexx.http.client;

import jexx.http.HttpMethod;
import jexx.http.HttpResponse;
import jexx.http.client.ssl.NoopSSLContextFactory;
import jexx.io.IOUtil;
import jexx.util.Console;
import jexx.util.StringUtil;
import org.junit.Ignore;
import org.junit.Test;

import java.io.IOException;
import java.net.URI;

public class ClientHttpRequestFactoryTest {

    @Test
    @Ignore
    public void testSimpleClientHttpRequestFactory() throws IOException {
        SimpleClientHttpRequestFactory clientHttpRequestFactory = new SimpleClientHttpRequestFactory();
        clientHttpRequestFactory.setSslContextFactory(NoopSSLContextFactory.INSTANCE);
        ClientHttpRequest clientHttpRequest = clientHttpRequestFactory.createRequest(URI.create("https://www.baidu.com"), HttpMethod.GET);

        HttpResponse httpResponse = clientHttpRequest.execute();
        httpResponse.getBody();
        byte[] buffer = IOUtil.readBytes(httpResponse.getBody());
        Console.log("{}", StringUtil.str(buffer));
    }

    @Test
    @Ignore
    public void testHttpComponentsClientHttpRequestFactory() throws IOException {
        HttpComponentsClientHttpRequestFactory clientHttpRequestFactory = new HttpComponentsClientHttpRequestFactory();
        clientHttpRequestFactory.setSslContextFactory(NoopSSLContextFactory.INSTANCE);
        ClientHttpRequest clientHttpRequest = clientHttpRequestFactory.createRequest(URI.create("https://baidu.com"), HttpMethod.GET);

        HttpResponse httpResponse = clientHttpRequest.execute();
        httpResponse.getBody();
        byte[] buffer = IOUtil.readBytes(httpResponse.getBody());
        Console.log("{}", StringUtil.str(buffer));
    }

}

package jexx.http;

import org.junit.Assert;
import org.junit.Test;

public class ContentTypeTest {

    @Test
    public void testParseContentType(){
        Assert.assertEquals(ContentType.APPLICATION_JSON, ContentType.parseContentType("application/json"));
    }

}

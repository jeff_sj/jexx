package jexx.http;

import jexx.collect.LinkedMultiValueMap;
import jexx.collect.MultiValueMap;
import jexx.io.FileResource;
import jexx.io.FileUtil;
import jexx.io.Resource;
import jexx.util.Console;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Ignore
public class DefaultHttpTest {

    @Test
    public void testExchange(){
        DefaultHttp http = new DefaultHttp();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(ContentType.ALL);
        ResponseEntity<String> responseEntity = http.exchangeForEntity("http://www.baidu.com", HttpMethod.GET, new RequestEntity<>("", headers), String.class);
        Console.log(responseEntity);
    }

    @Test
    public void testJson(){
        DefaultHttp http = new DefaultHttp();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(ContentType.APPLICATION_JSON);

        Map<String, String> map = new HashMap<>(2);
        map.put("userAccount", "admin");
        map.put("userPassword", "admin@321");

        RequestEntity<Map<String, String>> requestEntity = new RequestEntity<>(map, headers);

        ResponseEntity<Object> responseEntity = http.exchangeForEntity("http://10.5.96.62:9090/rschool/api/auth/login", HttpMethod.POST, requestEntity, Object.class);
        Console.log(responseEntity);
        Assert.assertEquals(200, responseEntity.getStatus());
    }

    @Test
    public void testPostFormData(){
//        HttpHeaders responseHeaders = new HttpHeaders();
//        Http http = TestHttpRequestFactory.createHttp(responseHeaders, "");
        Http http = new DefaultHttp();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(ContentType.MULTIPART_FORM_DATA);

        MultiValueMap<String, Object> formMap = new LinkedMultiValueMap<>();
        formMap.set("key1", "ddd");

        FileResource resource1 = new FileResource(FileUtil.file("C:\\Users\\kxys4\\Desktop\\blue_whale.sql"));
        formMap.add("files", resource1);
        FileResource resource2 = new FileResource(FileUtil.file("C:\\Users\\kxys4\\Desktop\\备注.txt"));
        formMap.add("files", resource2);

        RequestEntity<MultiValueMap<String, Object>> requestEntity = new RequestEntity<>(formMap, headers);

        ResponseEntity<String> responseEntity = http.postForEntity("http://localhost:8080/test/uploadFile", requestEntity, String.class);
        Assert.assertEquals(200, responseEntity.getStatus());
        Assert.assertEquals("", responseEntity.getBody());
    }

    @Test
    public void testDownloadFile() throws IOException {
        Http http = new DefaultHttp();
        ResponseEntity<Resource> responseEntity = http.postForEntity("http://localhost:8080/test/downloadFile", null, Resource.class);
        Assert.assertEquals(200, responseEntity.getStatus());

        Resource resource = responseEntity.getBody();
        FileUtil.write(FileUtil.file("testDownloadFile"), resource.getInputStream());
    }

}

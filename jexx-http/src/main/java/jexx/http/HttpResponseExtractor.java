package jexx.http;

import java.io.IOException;

@FunctionalInterface
public interface HttpResponseExtractor<T> {

    T extractData(HttpResponse response) throws IOException;

}

package jexx.http;

public interface HttpOutputMessage {

    HttpHeaders getHeaders();

    RequestBody getBody();

}

package jexx.http;

import java.io.IOException;
import java.io.InputStream;

public interface HttpResponse extends HttpInputMessage {

    int getStatus() throws IOException;

}

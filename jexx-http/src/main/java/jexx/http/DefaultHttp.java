package jexx.http;

import jexx.http.client.ClientHttpRequest;
import jexx.http.client.ClientHttpRequestFactory;
import jexx.http.client.ClientHttpResponse;
import jexx.http.client.SimpleClientHttpRequestFactory;
import jexx.http.convert.*;
import jexx.http.convert.json.FastjsonHttpMessageConverter;
import jexx.http.convert.json.JacksonHttpMessageConverter;
import jexx.http.convert.json.JexxJsonHttpMessageConverter;
import jexx.http.convert.xml.JaxbHttpMessageConverter;
import jexx.http.exception.HttpException;
import jexx.io.IOUtil;
import jexx.util.Assert;
import jexx.util.ClassUtil;
import jexx.util.CollectionUtil;
import jexx.util.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class DefaultHttp implements Http {

    private static final Logger log = LoggerFactory.getLogger(DefaultHttp.class);

    private static final boolean jackson2Present;
    private static final boolean fastjson2Present;
    private static final boolean jexxjson2Present;

    static {
        ClassLoader classLoader = DefaultHttp.class.getClassLoader();
        jackson2Present = ClassUtil.exist("com.fasterxml.jackson.databind.ObjectMapper", classLoader);
        fastjson2Present = ClassUtil.exist("com.alibaba.fastjson.JSON", classLoader);
        jexxjson2Present = ClassUtil.exist("jexx.json.Json", classLoader);
    }

    private final List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();

    private ClientHttpRequestFactory clientHttpRequestFactory;

    public DefaultHttp() {
        this(new SimpleClientHttpRequestFactory());
    }

    public DefaultHttp(ClientHttpRequestFactory requestFactory) {
        messageConverters.add(new ByteArrayHttpMessageConverter());
        messageConverters.add(new StringHttpMessageConverter());
        messageConverters.add(new FormHttpMessageConverter());
        //json
        if(jexxjson2Present){
            messageConverters.add(new JexxJsonHttpMessageConverter());
        }
        else if(jackson2Present){
            messageConverters.add(new JacksonHttpMessageConverter());
        }
        else if(fastjson2Present){
            messageConverters.add(new FastjsonHttpMessageConverter());
        }
        //xml
        messageConverters.add(new JaxbHttpMessageConverter());
        //resource
        messageConverters.add(new ResourceHttpMessageConverter());

        this.clientHttpRequestFactory = requestFactory == null ? new SimpleClientHttpRequestFactory() : requestFactory;
    }

    public void setClientHttpRequestFactory(ClientHttpRequestFactory clientHttpRequestFactory) {
        if(clientHttpRequestFactory != null){
            this.clientHttpRequestFactory = clientHttpRequestFactory;
        }
    }

    public List<HttpMessageConverter<?>> getMessageConverters() {
        return this.messageConverters;
    }

    @Override
    public <T> T exchangeForObject(String url, HttpMethod method, RequestEntity<?> httpEntity, Class<T> responseType) {
        HttpRequestCallback callback = createHttpEntityRequestCallback(httpEntity);
        HttpMessageConverterExtractor<T> responseExtractor = createObjectExtractor(responseType);
        return execute(url, method, callback, responseExtractor);
    }

    @Override
    public <T> ResponseEntity<T> exchangeForEntity(String url, HttpMethod method, RequestEntity<?> httpEntity, Class<T> responseType){
        HttpRequestCallback callback = createHttpEntityRequestCallback(httpEntity);
        ResponseEntityResponseExtractor<T> responseExtractor = createResponseEntityExtractor(responseType);
        return execute(url, method, callback, responseExtractor);
    }


    @Override
    public <T> T exchangeForObject(URI url, HttpMethod method, RequestEntity<?> httpEntity, Class<T> responseType) {
        HttpRequestCallback callback = createHttpEntityRequestCallback(httpEntity);
        HttpMessageConverterExtractor<T> responseExtractor = createObjectExtractor(responseType);
        return execute(url, method, callback, responseExtractor);
    }

    @Override
    public <T> ResponseEntity<T> exchangeForEntity(URI url, HttpMethod method, RequestEntity<?> httpEntity, Class<T> responseType){
        HttpRequestCallback callback = new HttpEntityRequestCallback(httpEntity);
        ResponseEntityResponseExtractor<T> responseExtractor = new ResponseEntityResponseExtractor<>(responseType);
        return execute(url, method, callback, responseExtractor);
    }

    @Override
    public <T> T execute(String url, HttpMethod method, HttpRequestCallback requestCallback, HttpResponseExtractor<T> responseExtractor){
        URI expanded = URI.create(url);
        return execute(expanded, method, requestCallback, responseExtractor);
    }

    @Override
    public <T> T execute(URI url, HttpMethod method, HttpRequestCallback requestCallback, HttpResponseExtractor<T> responseExtractor){
        return doExecute(url, method, requestCallback, responseExtractor);
    }

    protected <T> T doExecute(URI url, HttpMethod method, HttpRequestCallback requestCallback, HttpResponseExtractor<T> responseExtractor){
        Assert.notNull(url, "URI is required");
        Assert.notNull(method, "HttpMethod is required");

        ClientHttpResponse response = null;
        try {
            ClientHttpRequest request = this.clientHttpRequestFactory.createRequest(url, method);
            if(requestCallback != null){
                requestCallback.wrap(request);
            }
            response = request.execute();
            return responseExtractor != null ? responseExtractor.extractData(response) : null;
        }
        catch (IOException e){
            throw new HttpException(e);
        }
        finally {
            if (response != null) {
                response.close();
            }
        }
    }

    protected HttpRequestCallback createAcceptedRequestCallback(Class<?> responseType){
        return new AcceptedRequestCallback(responseType);
    }

    protected HttpRequestCallback createHttpEntityRequestCallback(Object entity){
        return new HttpEntityRequestCallback(entity);
    }

    protected <T> HttpMessageConverterExtractor<T> createObjectExtractor(Class<T> responseType){
        return new HttpMessageConverterExtractor<>(responseType);
    }

    protected <T> ResponseEntityResponseExtractor<T> createResponseEntityExtractor(Class<T> responseType){
        return new ResponseEntityResponseExtractor<>(responseType);
    }

    private class AcceptedRequestCallback implements HttpRequestCallback {

        private final Class<?> responseType;

        public AcceptedRequestCallback(Class<?> responseType) {
            this.responseType = responseType;
        }

        @Override
        public void wrap(HttpRequest request) {
            if(responseType != null){
                List<ContentType> contentTypes = fetchSupportReadContentTypeByResponseType(responseType);
                if(CollectionUtil.isNotEmpty(contentTypes)){
                    request.getHeaders().setAccept(contentTypes);
                }
            }
        }

        private List<ContentType> fetchSupportReadContentTypeByResponseType(Class<?> responseType){
            return getMessageConverters().stream().filter(s -> s.supportRead(responseType, null))
                    .flatMap(s -> s.getSupportContentTypes().stream())
                    .distinct()
                    .collect(Collectors.toList());
        }

    }

    private class HttpEntityRequestCallback extends AcceptedRequestCallback {

        private final RequestEntity<?> entity;

        public HttpEntityRequestCallback(Object entity) {
            this(null, entity);
        }

        public HttpEntityRequestCallback(Class<?> responseType, Object entity) {
            super(responseType);
            if(entity instanceof RequestEntity){
                this.entity = (RequestEntity<?>)entity;
            }
            else if (entity != null) {
                this.entity = new RequestEntity<>(entity);
            }
            else{
                this.entity = RequestEntity.EMPTY;
            }
        }

        @Override
        @SuppressWarnings("unchecked")
        public void wrap(HttpRequest request) {
            Object requestBody = this.entity.getBody();
            if(requestBody == null){
                HttpHeaders httpHeaders = request.getHeaders();
                HttpHeaders requestHeaders = this.entity.getHeaders();
                if(!requestHeaders.isEmpty()){
                    requestHeaders.forEach((key, values) -> httpHeaders.put(key, new LinkedList<>(values)));
                }
                if(httpHeaders.getContentLength() < 0){
                    httpHeaders.setContentLength(0);
                }
            }
            else{
                Class<?> requestBodyClass = requestBody.getClass();
                HttpHeaders httpHeaders = request.getHeaders();
                HttpHeaders requestHeaders = this.entity.getHeaders();
                ContentType contentType = requestHeaders.getContentType();

                List<HttpMessageConverter<?>> messageConverters = getMessageConverters();
                for (HttpMessageConverter<?> converter : messageConverters){
                    if(converter.supportWrite(requestBodyClass, contentType)){
                        if (!requestHeaders.isEmpty()) {
                            requestHeaders.forEach((key, values) -> httpHeaders.put(key, new LinkedList<>(values)));
                        }
                        ((HttpMessageConverter<Object>)converter).write(requestBody, contentType, request);
                        return;
                    }
                }
                throw new HttpException("No HttpMessageConverter for " + requestBodyClass.getName() + " and  content type \"" + contentType + "\"");
            }
        }
    }

    private class HttpMessageConverterExtractor<T> implements HttpResponseExtractor<T> {

        private final Class<T> responseClass;

        public HttpMessageConverterExtractor(Class<T> responseClass) {
            this.responseClass = responseClass;
        }

        @Override
        @SuppressWarnings({"unchecked"})
        public T extractData(HttpResponse response) {
            ContentType contentType = getContentType(response);

            List<HttpMessageConverter<?>> messageConverters = getMessageConverters();
            for (HttpMessageConverter<?> converter : messageConverters){
                if(converter.supportRead(responseClass, contentType)){
                    return ((HttpMessageConverter<T>)converter).read(responseClass, response);
                }
            }
            try(InputStream inputStream = response.getBody()){
                byte[] buffer = IOUtil.readBytes(inputStream);
                log.error("response：status={},headers={},body={}", response.getStatus(), response.getHeaders(), StringUtil.str(buffer));
            }
            catch (IOException e){
                log.error("", e);
            }
            throw new HttpException("No HttpMessageConverter for " + responseClass.getName() + " and  content type \"" + contentType + "\"");
        }

        protected ContentType getContentType(HttpResponse response) {
            ContentType contentType = response.getHeaders().getContentType();
            if (contentType == null) {
                contentType = ContentType.APPLICATION_OCTET_STREAM;
            }
            return contentType;
        }
    }

    private class ResponseEntityResponseExtractor<T> implements HttpResponseExtractor<ResponseEntity<T>> {

        private final HttpMessageConverterExtractor<T> delegate;

        public ResponseEntityResponseExtractor(Class<T> responseClass) {
            this.delegate = new HttpMessageConverterExtractor<>(responseClass);
        }

        @Override
        public ResponseEntity<T> extractData(HttpResponse response) throws IOException {
            T body = this.delegate.extractData(response);
            return new ResponseEntity<>(response.getStatus(), response.getHeaders(), body);
        }
    }


}

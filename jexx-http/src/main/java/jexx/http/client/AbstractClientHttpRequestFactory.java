package jexx.http.client;

public abstract class AbstractClientHttpRequestFactory implements ClientHttpRequestFactory {

    /** 服务端证书工厂类 */
    protected SSLContextFactory sslContextFactory;

    public void setSslContextFactory(SSLContextFactory sslContextFactory) {
        this.sslContextFactory = sslContextFactory;
    }
}

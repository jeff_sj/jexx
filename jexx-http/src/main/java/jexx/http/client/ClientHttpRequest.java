package jexx.http.client;

import jexx.http.HttpRequest;

import java.io.IOException;

public interface ClientHttpRequest extends HttpRequest {

    ClientHttpResponse execute() throws IOException;

}

package jexx.http.client;

import jexx.http.HttpHeaders;
import jexx.http.RequestBody;
import jexx.util.Assert;

import java.io.IOException;

public abstract class AbstractClientHttpRequest implements ClientHttpRequest {

    private final HttpHeaders headers = new HttpHeaders();

    private boolean executed = false;

    protected RequestBody requestBody;

    @Override
    public final HttpHeaders getHeaders() {
        return headers;
    }

    @Override
    public RequestBody getBody() {
        if(this.requestBody != null){
            return this.requestBody;
        }
        this.requestBody = new RequestBody();
        return this.requestBody;
    }

    @Override
    public final ClientHttpResponse execute() throws IOException {
        assertNotExecuted();

        if(headers.getContentLength() < 0 && !this.requestBody.isStreaming()){
            headers.setContentLength(this.requestBody.size());
        }

        ClientHttpResponse result = executeInternal(headers);
        this.executed = true;
        return result;
    }

    protected abstract ClientHttpResponse executeInternal(HttpHeaders headers) throws IOException;

    protected void assertNotExecuted() {
        Assert.isFalse(this.executed, "ClientHttpRequest already executed");
    }

}

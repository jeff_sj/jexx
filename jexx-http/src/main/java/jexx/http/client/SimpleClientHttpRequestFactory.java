package jexx.http.client;

import jexx.http.HttpMethod;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import java.io.IOException;
import java.net.*;

public class SimpleClientHttpRequestFactory extends AbstractClientHttpRequestFactory implements ClientHttpRequestFactory {

    private static final int DEFAULT_CHUNK_SIZE = 4096;

    /** 代理 */
    private Proxy proxy;
    private int connectTimeout = -1;
    private int readTimeout = -1;
    private int chunkSize = DEFAULT_CHUNK_SIZE;

    @Override
    public ClientHttpRequest createRequest(URI uri, HttpMethod httpMethod) throws IOException {
        HttpURLConnection connection = openConnection(uri.toURL(), this.proxy);
        prepareConnection(connection, httpMethod.name());
        return new SimpleClientHttpRequest(connection);
    }

    protected HttpURLConnection openConnection(URL url, Proxy proxy) throws IOException {
        URLConnection urlConnection = (proxy != null ? url.openConnection(proxy) : url.openConnection());
        if (!(urlConnection instanceof HttpURLConnection)) {
            throw new IllegalStateException("HttpURLConnection required for [" + url + "] but got: " + urlConnection);
        }
        if(urlConnection instanceof HttpsURLConnection){
            if (sslContextFactory != null) {
                SSLContext sslContext = sslContextFactory.getSSLContext();
                ((HttpsURLConnection) urlConnection).setSSLSocketFactory(sslContext.getSocketFactory());
                ((HttpsURLConnection) urlConnection).setHostnameVerifier(sslContextFactory.getHostnameVerifier());
            }
        }
        return (HttpURLConnection) urlConnection;
    }

    protected void prepareConnection(HttpURLConnection connection, String httpMethod) throws IOException {
        if (this.connectTimeout >= 0) {
            connection.setConnectTimeout(this.connectTimeout);
        }
        if (this.readTimeout >= 0) {
            connection.setReadTimeout(this.readTimeout);
        }

        connection.setDoInput(true);

        connection.setInstanceFollowRedirects("GET".equals(httpMethod));
        connection.setDoOutput("POST".equals(httpMethod) || "PUT".equals(httpMethod) ||
                "PATCH".equals(httpMethod) || "DELETE".equals(httpMethod));
        connection.setRequestMethod(httpMethod);
    }

    public void setProxy(Proxy proxy) {
        this.proxy = proxy;
    }

    public void setConnectTimeout(int connectTimeout) {
        this.connectTimeout = connectTimeout;
    }

    public void setReadTimeout(int readTimeout) {
        this.readTimeout = readTimeout;
    }

    public void setChunkSize(int chunkSize) {
        this.chunkSize = chunkSize;
    }

}

package jexx.http.client;

import jexx.http.HttpHeaders;
import jexx.http.RequestBody;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;

class SimpleClientHttpRequest extends AbstractClientHttpRequest {

    private static final int DEFAULT_CHUNK_SIZE = 4096;

    private final HttpURLConnection connection;

    public SimpleClientHttpRequest(HttpURLConnection connection) {
        this.connection = connection;
    }

    @Override
    public URI getURI() {
        try {
            return this.connection.getURL().toURI();
        } catch (URISyntaxException e) {
            throw new IllegalStateException("Could not get HttpURLConnection URI: " + e.getMessage(), e);
        }
    }

    @Override
    public String getMethodValue() {
        return this.connection.getRequestMethod();
    }

    @Override
    protected ClientHttpResponse executeInternal(HttpHeaders headers) throws IOException {
        RequestBody httpBody = getBody();

        if(httpBody.isStreaming()){
            long contentLength = headers.getContentLength();
            if(contentLength >= 0){
                this.connection.setFixedLengthStreamingMode(contentLength);
            }
            else{
                this.connection.setChunkedStreamingMode(DEFAULT_CHUNK_SIZE);
            }
        }
        else{
            if(this.connection.getDoOutput() && httpBody.size() >= 0){
                this.connection.setFixedLengthStreamingMode(httpBody.size());
            }
        }

        addHeaders(this.connection, headers);
        this.connection.connect();
        if (this.connection.getDoOutput()) {
            OutputStream outputStream = this.connection.getOutputStream();
            httpBody.writeTo(outputStream);
            outputStream.flush();
        }
        else{
            this.connection.getResponseCode();
        }
        return new SimpleClientHttpResponse(this.connection);
    }

    private void addHeaders(HttpURLConnection connection, HttpHeaders headers) {
        String method = connection.getRequestMethod();
        headers.forEach((headerName, headerValues) -> {
            for (String headerValue : headerValues) {
                String actualHeaderValue = headerValue != null ? headerValue : "";
                connection.addRequestProperty(headerName, actualHeaderValue);
            }
        });
    }


}

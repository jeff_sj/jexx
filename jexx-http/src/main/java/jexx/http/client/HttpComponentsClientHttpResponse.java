package jexx.http.client;

import jexx.http.HttpHeaders;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;

import java.io.ByteArrayInputStream;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;

class HttpComponentsClientHttpResponse implements ClientHttpResponse {

    private final HttpResponse httpResponse;

    private HttpHeaders headers;

    public HttpComponentsClientHttpResponse(HttpResponse httpResponse) {
        this.httpResponse = httpResponse;
    }

    @Override
    public int getStatus() {
        return this.httpResponse.getStatusLine().getStatusCode();
    }

    @Override
    public HttpHeaders getHeaders() {
        if (this.headers == null) {
            this.headers = new HttpHeaders();
            for (Header header : this.httpResponse.getAllHeaders()) {
                this.headers.add(header.getName(), header.getValue());
            }
        }
        return this.headers;
    }

    @Override
    public InputStream getBody() throws IOException {
        HttpEntity entity = this.httpResponse.getEntity();
        return (entity != null ? entity.getContent() : new ByteArrayInputStream(new byte[0]));
    }

    @Override
    public void close() {
        try {
            try {
                // 尝试获取链接
                EntityUtils.consume(this.httpResponse.getEntity());
            }
            finally {
                if (this.httpResponse instanceof Closeable) {
                    ((Closeable) this.httpResponse).close();
                }
            }
        }
        catch (IOException ex) {
            // ignore
        }
    }


}

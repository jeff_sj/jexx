package jexx.http.client;

import jexx.http.HttpMethod;
import jexx.util.Assert;
import jexx.util.ClassUtil;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.*;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.protocol.HttpContext;

import javax.net.ssl.SSLContext;
import java.net.URI;

public class HttpComponentsClientHttpRequestFactory extends AbstractClientHttpRequestFactory implements ClientHttpRequestFactory {

    private HttpClient httpClient;

    private RequestConfig requestConfig;

    public HttpComponentsClientHttpRequestFactory() {
        ClassUtil.loadClass("org.apache.http.client.HttpClient");
    }

    @Override
    public ClientHttpRequest createRequest(URI uri, HttpMethod httpMethod){
        HttpClient client = getHttpClient();

        HttpUriRequest httpRequest = createHttpUriRequest(httpMethod, uri);
        HttpContext context = HttpClientContext.create();
        if(context.getAttribute(HttpClientContext.REQUEST_CONFIG) == null) {
            RequestConfig config = null;
            if (httpRequest instanceof Configurable) {
                config = ((Configurable) httpRequest).getConfig();
            }
            if (config == null) {
                config = this.requestConfig;
            }
            if (config != null) {
                context.setAttribute(HttpClientContext.REQUEST_CONFIG, config);
            }
        }
        return new HttpComponentsClientHttpRequest(client, httpRequest, context);
    }

    protected HttpClient createHttpClients(){
        if(sslContextFactory != null){
            SSLContext sslContext = sslContextFactory.getSSLContext();
            HttpClientBuilder httpClientBuilder = HttpClients.custom();
            httpClientBuilder.setSSLHostnameVerifier(sslContextFactory.getHostnameVerifier());
            httpClientBuilder.setSSLSocketFactory(new SSLConnectionSocketFactory(sslContext, sslContextFactory.getHostnameVerifier()));
            return httpClientBuilder.build();
        }
        return HttpClients.createSystem();

    }

    protected HttpUriRequest createHttpUriRequest(HttpMethod httpMethod, URI uri) {
        switch (httpMethod) {
            case GET:
                return new HttpGet(uri);
            case HEAD:
                return new HttpHead(uri);
            case POST:
                return new HttpPost(uri);
            case PUT:
                return new HttpPut(uri);
            case PATCH:
                return new HttpPatch(uri);
            case DELETE:
                return new HttpDelete(uri);
            case OPTIONS:
                return new HttpOptions(uri);
            case TRACE:
                return new HttpTrace(uri);
            default:
                throw new IllegalArgumentException("Invalid HTTP method: " + httpMethod);
        }
    }

    private RequestConfig.Builder requestConfigBuilder() {
        return (this.requestConfig != null ? RequestConfig.copy(this.requestConfig) : RequestConfig.custom());
    }


    public HttpClient getHttpClient() {
        if(this.httpClient == null){
            synchronized (this){
                if(this.httpClient == null){
                    this.httpClient = createHttpClients();
                }
            }
        }
        return this.httpClient;
    }

    public void setReadTimeout(int timeout) {
        Assert.isTrue(timeout >= 0, "Timeout must be a non-negative value");
        this.requestConfig = requestConfigBuilder().setSocketTimeout(timeout).build();
    }

}

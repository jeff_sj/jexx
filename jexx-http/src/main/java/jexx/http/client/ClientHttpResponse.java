package jexx.http.client;

import jexx.http.HttpResponse;

import java.io.Closeable;

public interface ClientHttpResponse extends HttpResponse, Closeable {

    @Override
    void close();

}

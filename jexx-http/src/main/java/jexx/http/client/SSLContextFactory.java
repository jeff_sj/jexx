package jexx.http.client;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;

public interface SSLContextFactory {

    SSLContext getSSLContext();

    TrustManager[] getTrustManagers();

    HostnameVerifier getHostnameVerifier();

}

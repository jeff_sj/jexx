package jexx.http.client;

import jexx.http.HttpMethod;

import java.io.IOException;
import java.net.URI;

@FunctionalInterface
public interface ClientHttpRequestFactory {

    /**
     * 构建 {@link ClientHttpRequest} 的 工厂类
     * @param uri uri
     * @param httpMethod http method
     * @return {@link ClientHttpRequest}
     * @throws IOException I/O异常
     */
    ClientHttpRequest createRequest(URI uri, HttpMethod httpMethod) throws IOException;

}

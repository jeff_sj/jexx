package jexx.http.client;

import jexx.http.HttpHeaders;
import jexx.io.IOUtil;
import jexx.util.StringUtil;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;

class SimpleClientHttpResponse implements ClientHttpResponse {

    private final HttpURLConnection connection;

    private HttpHeaders headers;
    private InputStream responseStream;

    public SimpleClientHttpResponse(HttpURLConnection connection) {
        this.connection = connection;
    }

    @Override
    public int getStatus() throws IOException {
        return this.connection.getResponseCode();
    }

    @Override
    public HttpHeaders getHeaders() {
        if (this.headers == null) {
            this.headers = new HttpHeaders();
            String name = this.connection.getHeaderFieldKey(0);
            if (StringUtil.hasLength(name)) {
                this.headers.add(name, this.connection.getHeaderField(0));
            }
            int i = 1;
            while (true) {
                name = this.connection.getHeaderFieldKey(i);
                if (!StringUtil.hasLength(name)) {
                    break;
                }
                this.headers.add(name, this.connection.getHeaderField(i));
                i++;
            }
        }
        return this.headers;
    }

    @Override
    public InputStream getBody() throws IOException {
        InputStream errorStream = this.connection.getErrorStream();
        this.responseStream = (errorStream != null ? errorStream : this.connection.getInputStream());
        return this.responseStream;
    }

    @Override
    public void close() {
        IOUtil.closeQuietly(this.responseStream);
    }

}

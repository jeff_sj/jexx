package jexx.http.client.ssl;

import jexx.http.client.SSLContextFactory;
import jexx.http.exception.HttpException;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.SecureRandom;

public class DefaultSSLContextFactory implements SSLContextFactory {

    private byte[] cert;
    private char[] password;

    public DefaultSSLContextFactory() {
    }

    public DefaultSSLContextFactory(byte[] cert, char[] password) {
        this.cert = cert;
        this.password = password;
    }

    @Override
    public SSLContext getSSLContext() {
        try {
            KeyStore ks = KeyStore.getInstance("PKCS12");
            ks.load(new ByteArrayInputStream(cert), password);

            // 实例化密钥库 & 初始化密钥工厂
            KeyManagerFactory kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
            kmf.init(ks, password);

            // 创建 SSLContext
            SSLContext sslContext = SSLContext.getInstance("TLS");
            sslContext.init(kmf.getKeyManagers(), null, new SecureRandom());
            return sslContext;
        }
        catch (Exception e){
            throw new HttpException(e);
        }
    }

    @Override
    public TrustManager[] getTrustManagers() {
        return new TrustManager[0];
    }

    @Override
    public HostnameVerifier getHostnameVerifier() {
        return NoopHostnameVerifier.INSTANCE;
    }
}

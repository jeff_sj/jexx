package jexx.http.client.ssl;

import jexx.http.client.SSLContextFactory;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import java.security.SecureRandom;

public class NoopSSLContextFactory implements SSLContextFactory {

    public static final NoopSSLContextFactory INSTANCE = new NoopSSLContextFactory();

    @Override
    public SSLContext getSSLContext(){
        try {
            SSLContext ctx = SSLContext.getInstance("TLS");
            ctx.init(new KeyManager[0], getTrustManagers(),  new SecureRandom());
            ctx.getClientSessionContext().setSessionTimeout(15);
            ctx.getClientSessionContext().setSessionCacheSize(1000);
            return ctx;
        }
        catch (Exception e){
            throw new RuntimeException("failed to create SSLContext", e);
        }
    }

    @Override
    public TrustManager[] getTrustManagers() {
        return new TrustManager[]{NoopTrustManager.INSTANCE};
    }

    @Override
    public HostnameVerifier getHostnameVerifier() {
        return NoopHostnameVerifier.INSTANCE;
    }

}

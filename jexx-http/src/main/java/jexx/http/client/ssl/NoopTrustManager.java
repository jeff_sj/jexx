package jexx.http.client.ssl;

import javax.net.ssl.X509TrustManager;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

public class NoopTrustManager implements X509TrustManager {

    public static final NoopTrustManager INSTANCE = new NoopTrustManager();

    @Override
    public void checkClientTrusted(X509Certificate[] x509Certificates, String s){

    }

    @Override
    public void checkServerTrusted(X509Certificate[] x509Certificates, String s){

    }

    @Override
    public X509Certificate[] getAcceptedIssuers() {
        return null;
    }

}

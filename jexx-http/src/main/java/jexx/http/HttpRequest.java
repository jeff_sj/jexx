package jexx.http;

import jexx.util.EnumUtil;

import java.net.URI;

public interface HttpRequest extends HttpOutputMessage {

    URI getURI();

    String getMethodValue();

    default HttpMethod getMethod(){
        return EnumUtil.getByName(HttpMethod.values(), getMethodValue());
    }

}

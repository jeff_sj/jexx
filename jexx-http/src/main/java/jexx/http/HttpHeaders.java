package jexx.http;

import jexx.collect.LinkedMultiValueMap;
import jexx.collect.MultiValueMap;
import jexx.http.util.ContentTypeUtil;
import jexx.util.Assert;
import jexx.util.StringUtil;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class HttpHeaders implements MultiValueMap<String, String>, Serializable {

    private static final long serialVersionUID = 7745853887619235474L;

    public static final String ACCEPT = "Accept";
    public static final String CONTENT_DISPOSITION = "Content-Disposition";
    public static final String CONTENT_LENGTH = "Content-Length";
    public static final String CONTENT_TYPE = "Content-Type";
    public static final String COOKIE = "Cookie";

    final MultiValueMap<String, String> headers;

    public HttpHeaders(){
        this.headers = new LinkedMultiValueMap<>(8);
    }

    public HttpHeaders(MultiValueMap<String, String> headers) {
        Assert.notNull(headers, "headers must not be null");
        this.headers = headers;
    }

    @Override
    public String getFirst(String key) {
        return this.headers.getFirst(key);
    }

    @Override
    public void add(String key, String value) {
        this.headers.add(key, value);
    }

    @Override
    public void addAll(String key, List<String> values) {
        this.headers.addAll(key, values);
    }

    @Override
    public void addAll(MultiValueMap<String, String> values) {
        this.headers.addAll(values);
    }

    @Override
    public void set(String key, String value) {
        this.headers.set(key, value);
    }

    @Override
    public void setAll(Map<String, String> values) {
        this.headers.setAll(values);
    }

    @Override
    public Map<String, String> toSingleValueMap() {
        return this.headers.toSingleValueMap();
    }

    @Override
    public int size() {
        return this.headers.size();
    }

    @Override
    public boolean isEmpty() {
        return this.headers.isEmpty();
    }

    @Override
    public boolean containsKey(Object key) {
        return this.headers.containsKey(key);
    }

    @Override
    public boolean containsValue(Object value) {
        return this.headers.containsValue(value);
    }

    @Override
    public List<String> get(Object key) {
        return this.headers.get(key);
    }

    @Override
    public List<String> put(String key, List<String> value) {
        return this.headers.put(key, value);
    }

    @Override
    public List<String> remove(Object key) {
        return this.headers.remove(key);
    }

    @Override
    public void putAll(Map<? extends String, ? extends List<String>> m) {
        this.headers.putAll(m);
    }

    @Override
    public void clear() {
        this.headers.clear();
    }

    @Override
    public Set<String> keySet() {
        return this.headers.keySet();
    }

    @Override
    public Collection<List<String>> values() {
        return this.headers.values();
    }

    @Override
    public Set<Entry<String, List<String>>> entrySet() {
        return this.headers.entrySet();
    }

    public void setAccept(List<ContentType> contentTypes) {
        set(ACCEPT, ContentTypeUtil.toString(contentTypes));
    }

    public List<ContentType> getAccept() {
        return ContentType.parseContentTypes(get(ACCEPT));
    }

    public void setContentDispositionFormData(String name, String filename) {
        Assert.notNull(name, "name must not be null");
        ContentDisposition contentDisposition = new ContentDisposition("form-data");
        contentDisposition.setName(name);
        if (filename != null) {
            contentDisposition.setFilename(filename);
        }
        setContentDisposition(contentDisposition);
    }

    public void setContentDisposition(ContentDisposition contentDisposition){
        set(CONTENT_DISPOSITION, contentDisposition.toString());
    }

    public ContentDisposition getContentDisposition() {
        String contentDisposition = getFirst(CONTENT_DISPOSITION);
        if (contentDisposition != null) {
            return ContentDisposition.parse(contentDisposition);
        }
        return ContentDisposition.empty();
    }

    /**
     * 设置内容长度
     * @param contentLength 内容长度
     */
    public void setContentLength(long contentLength) {
        set(CONTENT_LENGTH, Long.toString(contentLength));
    }

    /**
     * 获取内容长度
     */
    public long getContentLength() {
        String value = getFirst(CONTENT_LENGTH);
        return (value != null ? Long.parseLong(value) : -1);
    }

    public void setContentType(ContentType contentType) {
        if (contentType != null) {
            set(CONTENT_TYPE, contentType.toString());
        }
        else {
            remove(CONTENT_TYPE);
        }
    }

    public ContentType getContentType() {
        String value = getFirst(CONTENT_TYPE);
        return (StringUtil.hasLength(value) ? ContentType.parseContentType(value) : null);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("{");
        this.forEach((k, v)->{
            int length = v.size();
            sb.append(k).append("=").append(length > 1 ? "[" : "");
            for (int i = 0; i < length; i++) {
                sb.append(i == 0 ? "" : ", ").append(v.get(i));
            }
            sb.append(length > 1 ? "]" : "").append(",");
        });
        sb.append("}");
        return sb.toString();
    }
}

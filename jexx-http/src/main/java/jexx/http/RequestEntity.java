package jexx.http;

import jexx.collect.MultiValueMap;

public class RequestEntity<T> {

    public static final RequestEntity<?> EMPTY = new RequestEntity<>();

    private final HttpHeaders headers;

    private final T body;

    protected RequestEntity() {
        this(null, null);
    }

    public RequestEntity(T body) {
        this(body, null);
    }

    public RequestEntity(T body, MultiValueMap<String, String> headers) {
        this.body = body;
        HttpHeaders tempHeaders = new HttpHeaders();
        if (headers != null) {
            tempHeaders.putAll(headers);
        }
        this.headers = new HttpHeaders(tempHeaders);
    }

    public HttpHeaders getHeaders() {
        return headers;
    }

    public T getBody() {
        return body;
    }

    public static RequestEntity<?> unwrap(Object object){
        RequestEntity<?> entity;
        if(object instanceof RequestEntity){
            entity = (RequestEntity<?>)object;
        }
        else{
            entity = new RequestEntity<>(object);
        }
        return entity;
    }

}

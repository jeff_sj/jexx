package jexx.http;

import jexx.io.ByteArrayResource;
import jexx.io.IOUtil;
import jexx.io.Resource;
import jexx.util.CollectionUtil;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

public class RequestBody extends OutputStream {

    /**
     * 缓存的资源集合; 资源有两种类型,一种是已缓存在JVM中,另一种未缓存在JVM中
     */
    private final List<Resource> resources;
    /**
     * 临时缓存
     */
    private ByteArrayOutputStream buffer;
    /**
     * 非缓存资源的标记
     */
    private boolean streaming;
    /**
     * 缓存资源内容的字节长度
     */
    private int count;

    public RequestBody() {
        this.resources = new ArrayList<>(4);
        this.buffer = new ByteArrayOutputStream(1024);
    }

    @Override
    public void write(int b) throws IOException {
        buffer.write(b);
        count += 1;
    }

    public void writeResource(Resource resource){
        handleBuffer();
        resources.add(resource);
        if(resource.getSize() < 0){
            if(!streaming){
                streaming = true;
            }
        }
        else{
            count += resource.getSize();
        }
    }

    private void handleBuffer(){
        if(this.buffer.size() > 0){
            Resource r1 = new ByteArrayResource(buffer.toByteArray());
            this.resources.add(r1);
            this.buffer = new ByteArrayOutputStream();
        }
    }

    public void writeTo(OutputStream outputStream) throws IOException{
        handleBuffer();

        if(CollectionUtil.isEmpty(this.resources)){
            return;
        }

        for (Resource resource : this.resources){
            byte[] buffer = new byte[1024 * 4];
            int readSize;
            try(InputStream inputStream = resource.getInputStream()){
                while (IOUtil.EOF != (readSize = inputStream.read(buffer))) {
                    outputStream.write(buffer, 0, readSize);
                    outputStream.flush();
                }
            }
        }
    }

    /**
     * 资源集合中是否含有流资源
     */
    public boolean isStreaming() {
        return streaming;
    }

    /**
     * 非流部分的字节长度
     */
    public int size(){
        return streaming ? -1 : this.count;
    }
}

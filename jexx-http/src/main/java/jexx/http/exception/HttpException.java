package jexx.http.exception;

public class HttpException extends RuntimeException {

    private static final long serialVersionUID = -4835506413161524918L;

    public HttpException() {
    }

    public HttpException(String message) {
        super(message);
    }

    public HttpException(String message, Throwable cause) {
        super(message, cause);
    }

    public HttpException(Throwable cause) {
        super(cause);
    }

}

package jexx.http;

/**
 * 完整http返回内容
 * @author jeff
 * @param <T> 泛型
 */
public class ResponseEntity<T> {

    private final int status;
    private final HttpHeaders headers;
    private final T body;
    private final HttpStatus httpStatus;

    public ResponseEntity(int status) {
        this(status, null);
    }

    public ResponseEntity(int status, HttpHeaders headers) {
        this(status, headers, null);
    }

    public ResponseEntity(int status, HttpHeaders headers, T body) {
        this.status = status;
        this.headers = headers;
        this.body = body;
        this.httpStatus = HttpStatus.resolve(status);
    }

    public int getStatus() {
        return status;
    }

    public HttpHeaders getHeaders() {
        return headers;
    }

    public T getBody() {
        return body;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    @Override
    public String toString() {
        return "ResponseEntity{" +
                "status=" + status +
                ", headers=" + headers +
                ", body=" + body +
                '}';
    }
}

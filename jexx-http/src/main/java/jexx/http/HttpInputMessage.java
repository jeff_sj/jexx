package jexx.http;

import java.io.IOException;
import java.io.InputStream;

public interface HttpInputMessage {

    HttpHeaders getHeaders();

    InputStream getBody() throws IOException;

}

package jexx.http;

import jexx.http.util.ContentTypeUtil;
import jexx.util.Assert;
import jexx.util.CollectionUtil;
import jexx.util.ObjectUtil;
import jexx.util.StringUtil;

import java.io.Serializable;
import java.nio.charset.Charset;
import java.util.*;

public class ContentType implements Comparable<ContentType>, Serializable {

    private static final long serialVersionUID = 7387796374851149210L;

    public static final String WILDCARD_TYPE = "*";
    public static final String PARAM_CHARSET = "charset";

    public static final ContentType ALL = new ContentType("*", "*");
    public static final ContentType APPLICATION_ATOM_XML = new ContentType("application", "atom+xml");
    public static final ContentType APPLICATION_FORM_URLENCODED = new ContentType("application", "x-www-form-urlencoded");
    public static final ContentType APPLICATION_JSON = new ContentType("application", "json");
    public static final ContentType APPLICATION_OCTET_STREAM = new ContentType("application", "octet-stream");
    public static final ContentType APPLICATION_PDF = new ContentType("application", "pdf");
    public static final ContentType APPLICATION_XML = new ContentType("application", "xml");
    public static final ContentType IMAGE_GIF = new ContentType("image", "gif");
    public static final ContentType IMAGE_JPEG = new ContentType("image", "jpeg");
    public static final ContentType IMAGE_PNG = new ContentType("image", "png");
    public static final ContentType MULTIPART_FORM_DATA = new ContentType("multipart", "form-data");
    public static final ContentType MULTIPART_MIXED = new ContentType("multipart", "mixed");
    public static final ContentType TEXT_HTML = new ContentType("text", "html");
    public static final ContentType TEXT_MARKDOWN = new ContentType("text", "markdown");
    public static final ContentType TEXT_PLAIN = new ContentType("text", "plain");
    public static final ContentType TEXT_XML = new ContentType("text", "xml");


    private final String type;
    private final String subtype;
    private final Map<String, String> parameters;

    private volatile String toStringValue;

    public ContentType(String type) {
        this(type, WILDCARD_TYPE, Collections.emptyMap());
    }

    public ContentType(String type, String subtype) {
        this(type, subtype, Collections.emptyMap());
    }

    public ContentType(String type, String subtype, Map<String, String> parameters) {
        this.type = type;
        this.subtype = subtype;
        if(CollectionUtil.isNotEmpty(parameters)){
            Map<String, String> map = new LinkedHashMap<>(4);
            parameters.forEach((k,v)-> map.put(k.toLowerCase(), v));
            this.parameters = Collections.unmodifiableMap(map);
        }
        else{
            this.parameters = Collections.emptyMap();
        }
    }

    public ContentType(ContentType contentType, Map<String, String> parameters) {
        this(contentType.getType(), contentType.getSubtype(), parameters);
    }

    public String getType() {
        return type;
    }

    public String getSubtype() {
        return subtype;
    }

    public Map<String, String> getParameters() {
        return parameters;
    }

    public String getParameter(String name) {
        return this.parameters.get(name);
    }

    public Charset getCharset() {
        String charset = getParameter(PARAM_CHARSET);
        return charset != null ? Charset.forName(unquote(charset)) : null;
    }

    /**
     * 是否包含 ContentType
     */
    public boolean include(ContentType other){
        if(other == null){
            return false;
        }
        if(isWildcardType()){
            return true;
        }
        else if(getType().equals(other.getType())){
            if(getSubtype().equals(other.getSubtype())){
                return true;
            }
            return isWildcardSubtype();
        }
        return false;
    }

    /**
     * 头和尾是否含有引号 " 或者 '
     */
    private boolean isQuotedString(String s) {
        if (s.length() < 2) {
            return false;
        }
        else {
            return ((s.startsWith("\"") && s.endsWith("\"")) || (s.startsWith("'") && s.endsWith("'")));
        }
    }

    /**
     * 是否为通配类型
     */
    public boolean isWildcardType() {
        return WILDCARD_TYPE.equals(getType());
    }

    /**
     * 子类型是否为通配符
     */
    public boolean isWildcardSubtype() {
        return WILDCARD_TYPE.equals(getSubtype()) || getSubtype().startsWith("*+");
    }

    public static ContentType parseContentType(String contentType) {
        Assert.hasText(contentType, "contentType '{}' must not be empty", contentType);

        int index = contentType.indexOf(';');
        String fullType = StringUtil.trim(index >= 0 ? contentType.substring(0, index) : contentType);
        Assert.hasText(fullType, "contentType '{}' must not be empty", contentType);

        if(WILDCARD_TYPE.equals(fullType)){
            fullType = "*/*";
        }
        int subIndex = fullType.indexOf('/');
        if(subIndex == -1){
            throw new IllegalArgumentException("contentType '"+contentType+"' does not contain '/'");
        }
        if(subIndex == fullType.length()-1){
            throw new IllegalArgumentException("contentType '"+contentType+"' does not has subType '/'");
        }

        String type = fullType.substring(0, subIndex);
        String subtype = fullType.substring(subIndex + 1);
        if (WILDCARD_TYPE.equals(type) && !WILDCARD_TYPE.equals(subtype)) {
            throw new IllegalArgumentException(contentType + ", wildcard type is legal only in '*/*' (all mime types)");
        }

        Map<String, String> parameters = null;
        while(index < contentType.length()){
            int nextIndex = index + 1;
            boolean close = true;
            for (int i = nextIndex; i < contentType.length(); i++) {
                char c = contentType.charAt(i);
                if(close && c == ';'){
                    break;
                }
                else if(c == '"'){
                    close = !close;
                }
                nextIndex++;
            }

            String parameter = contentType.substring(index+1, nextIndex).trim();
            if(parameter.length() > 0){
                if (parameters == null) {
                    parameters = new LinkedHashMap<>(4);
                }
                int eqIndex = parameter.indexOf('=');
                if (eqIndex >= 0) {
                    String attribute = parameter.substring(0, eqIndex).trim();
                    String value = parameter.substring(eqIndex + 1).trim();
                    parameters.put(attribute, value);
                }
            }
            index = nextIndex;
        }

        return new ContentType(type, subtype, parameters);
    }

    public static List<ContentType> parseContentTypes(String contentTypes) {
        List<String> ls = ContentTypeUtil.tokenize(contentTypes);
        List<ContentType> contentTypeList = new ArrayList<>();
        for (String s : ls){
            ContentType contentType = parseContentType(s);
        }
        return contentTypeList;
    }

    public static List<ContentType> parseContentTypes(List<String> contentTypes) {
        List<ContentType> list = new ArrayList<>();
        for (String s : contentTypes){
            list.addAll(parseContentTypes(s));
        }
        return list;
    }

    /**
     * 去除引号字符串
     */
    protected String unquote(String s) {
        return (isQuotedString(s) ? s.substring(1, s.length() - 1) : s);
    }

    @Override
    public int compareTo(ContentType other) {
        int comp = getType().compareToIgnoreCase(other.getType());
        if (comp != 0) {
            return comp;
        }
        comp = getSubtype().compareToIgnoreCase(other.getSubtype());
        if (comp != 0) {
            return comp;
        }
        comp = getParameters().size() - other.getParameters().size();
        if (comp != 0) {
            return comp;
        }

        TreeSet<String> thisAttributes = new TreeSet<>(String.CASE_INSENSITIVE_ORDER);
        thisAttributes.addAll(getParameters().keySet());
        TreeSet<String> otherAttributes = new TreeSet<>(String.CASE_INSENSITIVE_ORDER);
        otherAttributes.addAll(other.getParameters().keySet());
        Iterator<String> thisAttributesIterator = thisAttributes.iterator();
        Iterator<String> otherAttributesIterator = otherAttributes.iterator();

        while (thisAttributesIterator.hasNext()) {
            String thisAttribute = thisAttributesIterator.next();
            String otherAttribute = otherAttributesIterator.next();
            comp = thisAttribute.compareToIgnoreCase(otherAttribute);
            if (comp != 0) {
                return comp;
            }
            if (PARAM_CHARSET.equals(thisAttribute)) {
                Charset thisCharset = getCharset();
                Charset otherCharset = other.getCharset();
                if (thisCharset != otherCharset) {
                    if (thisCharset == null) {
                        return -1;
                    }
                    if (otherCharset == null) {
                        return 1;
                    }
                    comp = thisCharset.compareTo(otherCharset);
                    if (comp != 0) {
                        return comp;
                    }
                }
            }
            else {
                String thisValue = getParameters().get(thisAttribute);
                String otherValue = other.getParameters().get(otherAttribute);
                if (otherValue == null) {
                    otherValue = "";
                }
                comp = thisValue.compareTo(otherValue);
                if (comp != 0) {
                    return comp;
                }
            }
        }

        return 0;
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (!(other instanceof ContentType)) {
            return false;
        }
        ContentType otherType = (ContentType) other;
        return (this.type.equalsIgnoreCase(otherType.type) &&
                this.subtype.equalsIgnoreCase(otherType.subtype) &&
                parametersAreEqual(otherType));
    }

    private boolean parametersAreEqual(ContentType other) {
        if (this.parameters.size() != other.parameters.size()) {
            return false;
        }

        for (Map.Entry<String, String> entry : this.parameters.entrySet()) {
            String key = entry.getKey();
            if (!other.parameters.containsKey(key)) {
                return false;
            }
            if (PARAM_CHARSET.equals(key)) {
                if (!ObjectUtil.nullSafeEquals(getCharset(), other.getCharset())) {
                    return false;
                }
            }
            else if (!ObjectUtil.nullSafeEquals(entry.getValue(), other.parameters.get(key))) {
                return false;
            }
        }
        return true;
    }

    @Override
    public int hashCode() {
        int result = this.type.hashCode();
        result = 31 * result + this.subtype.hashCode();
        result = 31 * result + this.parameters.hashCode();
        return result;
    }

    @Override
    public String toString() {
        String value = this.toStringValue;
        if (value == null) {
            StringBuilder builder = new StringBuilder();
            builder.append(this.type).append('/').append(this.subtype);

            this.parameters.forEach((k,v)-> builder.append(";").append(k).append("=").append(v));
            value = builder.toString();
            this.toStringValue = value;
        }
        return value;
    }

}

package jexx.http;

import java.net.URI;

/**
 * http接口
 * @author jeff
 */
public interface Http {

    /**
     * GET获取对象
     * @param url 请求URL
     * @param responseType 返回对象类型
     * @param <T> 返回对象类型
     * @return 返回对象
     */
    default <T> T getForObject(String url, Class<T> responseType){
        return exchangeForObject(url, HttpMethod.GET, RequestEntity.EMPTY, responseType);
    }

    /**
     * GET获取对象
     * @param url 请求URL
     * @param headers 请求header
     * @param responseType 返回对象类型
     * @param <T> 返回对象类型
     * @return 返回对象
     */
    default <T> T getForObject(String url, HttpHeaders headers, Class<T> responseType){
        return exchangeForObject(url, HttpMethod.GET,new RequestEntity<>(null, headers), responseType);
    }

    /**
     * GET获取对象
     * @param url 请求URL
     * @param responseType 返回对象类型
     * @param <T> 返回对象类型
     * @return 返回对象
     */
    default <T> ResponseEntity<T> getForEntity(String url, Class<T> responseType){
        return exchangeForEntity(url, HttpMethod.GET, RequestEntity.EMPTY, responseType);
    }

    /**
     * GET获取对象
     * @param url 请求URL
     * @param headers 请求header
     * @param responseType 返回对象类型
     * @param <T> 返回对象类型
     * @return 返回对象
     */
    default <T> ResponseEntity<T> getForEntity(String url, HttpHeaders headers, Class<T> responseType){
        return exchangeForEntity(url, HttpMethod.GET, new RequestEntity<>(null, headers), responseType);
    }

    /**
     * POST获取对象
     * @param url 请求URL
     * @param request 请求内容
     * @param responseType 返回对象类型
     * @param <T> 返回对象类型
     * @return 返回对象
     */
    default <T> T postForObject(String url, Object request, Class<T> responseType){
        return exchangeForObject(url, HttpMethod.POST, RequestEntity.unwrap(request), responseType);
    }

    /**
     * POST获取对象
     * @param url 请求URL
     * @param headers 请求Header
     * @param request 请求内容
     * @param responseType 返回对象类型
     * @param <T> 返回对象类型
     * @return 返回对象
     */
    default <T> T postForObject(String url, HttpHeaders headers, Object request, Class<T> responseType){
        return exchangeForObject(url, HttpMethod.POST, new RequestEntity<>(request, headers), responseType);
    }

    /**
     * POST获取完整HTTP返回对象
     * @param url 请求URL
     * @param request 请求内容
     * @param responseType 返回对象类型
     * @param <T> 返回对象类型
     * @return 返回对象
     */
    default <T> ResponseEntity<T> postForEntity(String url, Object request, Class<T> responseType){
        return exchangeForEntity(url, HttpMethod.POST, RequestEntity.unwrap(request), responseType);
    }

    /**
     * POST获取完整HTTP返回对象
     * @param url 请求URL
     * @param headers 请求header
     * @param request 请求内容
     * @param responseType 返回对象类型
     * @param <T> 返回对象类型
     * @return 返回对象
     */
    default <T> ResponseEntity<T> postForEntity(String url, HttpHeaders headers, Object request, Class<T> responseType){
        return exchangeForEntity(url, HttpMethod.POST, new RequestEntity<>(request, headers), responseType);
    }

    /**
     * PUT获取完整返回对象
     * @param url 请求URL
     * @param request 请求内容
     * @param responseType 返回对象类型
     * @param <T> 返回对象类型
     * @return 返回对象
     */
    default <T> T putForObject(String url, Object request, Class<T> responseType){
        return exchangeForObject(url, HttpMethod.PUT, RequestEntity.unwrap(request), responseType);
    }

    /**
     * PUT获取返回对象
     * @param url 请求URL
     * @param headers 请求header
     * @param request 请求内容
     * @param responseType 返回对象类型
     * @param <T> 返回对象类型
     * @return 返回对象
     */
    default <T> T putForObject(String url, HttpHeaders headers, Object request, Class<T> responseType){
        return exchangeForObject(url, HttpMethod.PUT, new RequestEntity<>(request, headers), responseType);
    }

    /**
     * PUT获取完整返回对象
     * @param url 请求URL
     * @param request 请求内容
     * @param responseType 返回对象类型
     * @param <T> 返回对象类型
     * @return 返回对象
     */
    default <T> ResponseEntity<T> putForEntity(String url, Object request, Class<T> responseType){
        return exchangeForEntity(url, HttpMethod.PUT, RequestEntity.unwrap(request), responseType);
    }

    /**
     * PUT获取完整返回对象
     * @param url 请求URL
     * @param headers 请求header
     * @param request 请求内容
     * @param responseType 返回对象类型
     * @param <T> 返回对象类型
     * @return 返回对象
     */
    default <T> ResponseEntity<T> putForEntity(String url, HttpHeaders headers, Object request, Class<T> responseType){
        return exchangeForEntity(url, HttpMethod.PUT, new RequestEntity<>(request, headers), responseType);
    }

    /**
     * DELETE获取对象
     * @param url 请求URL
     * @param responseType 返回对象类型
     * @param <T> 返回对象类型
     * @return 返回对象
     */
    default <T> T deleteForObject(String url, Class<T> responseType){
        return exchangeForObject(url, HttpMethod.DELETE, RequestEntity.EMPTY, responseType);
    }

    /**
     * DELETE获取对象
     * @param url 请求URL
     * @param headers 请求header
     * @param responseType 返回对象类型
     * @param <T> 返回对象类型
     * @return 返回对象
     */
    default <T> T deleteForObject(String url, HttpHeaders headers, Class<T> responseType){
        return exchangeForObject(url, HttpMethod.DELETE, new RequestEntity<>(null, headers), responseType);
    }

    /**
     * DELETE获取完整返回对象
     * @param url 请求URL
     * @param responseType 返回对象类型
     * @param <T> 返回对象类型
     * @return 返回对象
     */
    default <T> ResponseEntity<T> deleteForEntity(String url, Class<T> responseType){
        return exchangeForEntity(url, HttpMethod.DELETE, RequestEntity.EMPTY, responseType);
    }

    /**
     * DELETE获取完整返回对象
     * @param url 请求URL
     * @param headers 请求header
     * @param responseType 返回对象类型
     * @param <T> 返回对象类型
     * @return 返回对象
     */
    default <T> ResponseEntity<T> deleteForEntity(String url, HttpHeaders headers, Class<T> responseType){
        return exchangeForEntity(url, HttpMethod.DELETE, new RequestEntity<>(null, headers), responseType);
    }

    /**
     * 交互获取返回实际对象
     * @param url 请求url
     * @param method 请求方法
     * @param httpEntity 请求对象
     * @param responseType 返回对象类型
     * @param <T> 返回对象类型
     * @return 返回实际对象
     */
    <T> T exchangeForObject(String url, HttpMethod method, RequestEntity<?> httpEntity, Class<T> responseType);

    /**
     * 交互获取返回包装对象
     * @param url 请求url
     * @param method 请求方法
     * @param httpEntity 请求对象
     * @param responseType 返回对象类型
     * @param <T> 返回对象类型
     * @return 返回包装对象
     */
    <T> ResponseEntity<T> exchangeForEntity(String url, HttpMethod method, RequestEntity<?> httpEntity, Class<T> responseType);

    /**
     * 交互获取返回实际对象
     * @param url 请求url
     * @param method 请求方法
     * @param httpEntity 请求对象
     * @param responseType 返回对象类型
     * @param <T> 返回对象类型
     * @return 返回实际对象
     */
    <T> T exchangeForObject(URI url, HttpMethod method, RequestEntity<?> httpEntity, Class<T> responseType);

    /**
     * 交互获取返回包装对象
     * @param url 请求url
     * @param method 请求方法
     * @param httpEntity 请求对象
     * @param responseType 返回对象类型
     * @param <T> 返回对象类型
     * @return 返回包装对象
     */
    <T> ResponseEntity<T> exchangeForEntity(URI url, HttpMethod method, RequestEntity<?> httpEntity, Class<T> responseType);

    /**
     * 请求返回对象
     * @param url 请求url
     * @param method 请求方法
     * @param requestCallback 请求对象构建回调
     * @param responseExtractor 内容抽取回调
     * @param <T> 返回对象类型
     * @return 返回对象
     */
    <T> T execute(String url, HttpMethod method, HttpRequestCallback requestCallback, HttpResponseExtractor<T> responseExtractor);

    /**
     * 请求返回对象
     * @param url 请求url
     * @param method 请求方法
     * @param requestCallback 请求对象构建回调
     * @param responseExtractor 内容抽取回调
     * @param <T> 返回对象类型
     * @return 返回对象
     */
    <T> T execute(URI url, HttpMethod method, HttpRequestCallback requestCallback, HttpResponseExtractor<T> responseExtractor);

}

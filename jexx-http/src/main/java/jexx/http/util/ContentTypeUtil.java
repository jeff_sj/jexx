package jexx.http.util;

import jexx.http.ContentType;
import jexx.util.StringUtil;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class ContentTypeUtil {

    /**
     * 分隔字符串为多个ContentType字符串
     */
    public static List<String> tokenize(String contentTypes) {
        if (!StringUtil.hasLength(contentTypes)) {
            return Collections.emptyList();
        }
        List<String> tokens = new ArrayList<>();
        boolean inQuotes = false;
        int startIndex = 0;
        int i = 0;
        while (i < contentTypes.length()) {
            switch (contentTypes.charAt(i)) {
                case '"':
                    inQuotes = !inQuotes;
                    break;
                case ',':
                    if (!inQuotes) {
                        tokens.add(contentTypes.substring(startIndex, i));
                        startIndex = i + 1;
                    }
                    break;
                case '\\':
                    i++;
                    break;
            }
            i++;
        }
        tokens.add(contentTypes.substring(startIndex));
        return tokens;
    }

    /**
     * 多个ContentType转换为字符串
     */
    public static String toString(Collection<? extends ContentType> contentTypes){
        return StringUtil.join(contentTypes, ", ", ContentType::toString);
    }

}

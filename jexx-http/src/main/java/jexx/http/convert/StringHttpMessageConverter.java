package jexx.http.convert;

import jexx.http.ContentType;
import jexx.http.HttpInputMessage;
import jexx.http.HttpOutputMessage;
import jexx.io.IOUtil;
import jexx.util.StringUtil;

import java.io.IOException;
import java.nio.charset.Charset;

public class StringHttpMessageConverter extends AbstractHttpMessageConverter<String> {

    public StringHttpMessageConverter() {
        super(DEFAULT_CHARSET, ContentType.TEXT_PLAIN, ContentType.ALL);
    }

    @Override
    protected boolean supports(Class<?> clazz) {
        return String.class == clazz;
    }

    @Override
    protected String readInternal(Class<? extends String> clazz, HttpInputMessage inputMessage) throws IOException {
        Charset charset = getCharset(inputMessage.getHeaders().getContentType());
        byte[] response = IOUtil.readBytes(inputMessage.getBody());
        return StringUtil.str(response, charset);
    }

    @Override
    protected void writeInternal(String s, ContentType contentType, HttpOutputMessage httpOutputMessage) throws IOException {
        Charset charset = getCharset(contentType);
        byte[] buffer = StringUtil.getBytes(s, charset);
        writeBody(httpOutputMessage.getBody(), buffer);
    }

}

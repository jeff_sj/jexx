package jexx.http.convert.json;

import jexx.collect.StringKeyMap;
import jexx.http.ContentType;
import jexx.http.HttpInputMessage;
import jexx.http.HttpOutputMessage;
import jexx.http.convert.AbstractHttpMessageConverter;
import jexx.io.IOUtil;
import jexx.json.JsonUtil;
import jexx.util.ClassUtil;
import jexx.util.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.charset.Charset;

/**
 * jexx json message 转换
 * @author jeff
 * @since 2021-03-15
 */
public class JexxJsonHttpMessageConverter extends AbstractHttpMessageConverter<Object> {

    private static final Logger logger = LoggerFactory.getLogger(JexxJsonHttpMessageConverter.class);

    public JexxJsonHttpMessageConverter() {
        super(null, ContentType.APPLICATION_JSON);
    }

    @Override
    protected boolean supports(Class<?> clazz) {
        return true;
    }

    @Override
    protected Object readInternal(Class<?> clazz, HttpInputMessage inputMessage) throws IOException {
        if(logger.isDebugEnabled()){
            byte[] bodyBytes = IOUtil.readBytes(inputMessage.getBody());
            Charset charset = getCharset(inputMessage.getHeaders().getContentType());
            String body = StringUtil.str(bodyBytes, charset);
            logger.debug("read json= {}", body);
            if(ClassUtil.isAssignableFrom(StringKeyMap.class, clazz)){
                return JsonUtil.parseMap(body);
            }
            return JsonUtil.parseObject(body, clazz);
        }
        if(ClassUtil.isAssignableFrom(StringKeyMap.class, clazz)){
            return JsonUtil.parseMap(inputMessage.getBody());
        }
        return JsonUtil.parseObject(inputMessage.getBody(), clazz);
    }

    @Override
    protected void writeInternal(Object object, ContentType contentType, HttpOutputMessage httpOutputMessage) throws IOException {
        String json = JsonUtil.toJSONString(object);
        Charset charset = getCharset(contentType);
        byte[] buffer = StringUtil.getBytes(json, charset);
        writeBody(httpOutputMessage.getBody(), buffer);
    }

}

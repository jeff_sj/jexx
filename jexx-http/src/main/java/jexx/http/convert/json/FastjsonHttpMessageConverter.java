package jexx.http.convert.json;

import com.alibaba.fastjson.JSON;
import jexx.http.ContentType;
import jexx.http.HttpInputMessage;
import jexx.http.HttpOutputMessage;
import jexx.http.convert.AbstractHttpMessageConverter;
import jexx.io.IOUtil;
import jexx.util.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.charset.Charset;

public class FastjsonHttpMessageConverter extends AbstractHttpMessageConverter<Object> {

    private static final Logger logger = LoggerFactory.getLogger(FastjsonHttpMessageConverter.class);

    public FastjsonHttpMessageConverter() {
        super(null, ContentType.APPLICATION_JSON);
    }

    @Override
    protected boolean supports(Class<?> clazz) {
        return true;
    }

    @Override
    protected Object readInternal(Class<?> clazz, HttpInputMessage inputMessage) throws IOException {
        if(logger.isDebugEnabled()){
            byte[] bodyBytes = IOUtil.readBytes(inputMessage.getBody());
            Charset charset = getCharset(inputMessage.getHeaders().getContentType());
            String body = StringUtil.str(bodyBytes, charset);
            logger.debug("read json= {}", body);
            return JSON.parseObject(bodyBytes, clazz);
        }
        return JSON.parseObject(inputMessage.getBody(), clazz);
    }

    @Override
    protected void writeInternal(Object object, ContentType contentType, HttpOutputMessage httpOutputMessage) throws IOException {
        String json = JSON.toJSONString(object);
        Charset charset = getCharset(contentType);
        byte[] buffer = StringUtil.getBytes(json, charset);
        writeBody(httpOutputMessage.getBody(), buffer);
    }

}

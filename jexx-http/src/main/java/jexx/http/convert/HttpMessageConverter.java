package jexx.http.convert;

import jexx.http.*;

import java.util.List;

public interface HttpMessageConverter<T> {

    boolean supportRead(Class<?> clazz, ContentType contentType);

    boolean supportWrite(Class<?> clazz, ContentType contentType);

    List<ContentType> getSupportContentTypes();

    T read(Class<? extends T> clazz, HttpInputMessage inputMessage);

    void write(T t, ContentType contentType, HttpOutputMessage httpOutputMessage);

}

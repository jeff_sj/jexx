package jexx.http.convert;

import jexx.http.ContentType;
import jexx.http.HttpInputMessage;
import jexx.http.HttpOutputMessage;
import jexx.io.ByteArrayResource;
import jexx.io.IOUtil;
import jexx.io.Resource;

import java.io.IOException;

/**
 * 实现 Http 消息 对 资源的 输入输出 转换
 * @author jeff
 * @since 2021-02-19
 */
public class ResourceHttpMessageConverter extends AbstractHttpMessageConverter<Resource> {

    public ResourceHttpMessageConverter() {
        super(null, ContentType.ALL);
    }

    @Override
    protected boolean supports(Class<?> clazz) {
        return Resource.class.isAssignableFrom(clazz);
    }

    @Override
    protected Resource readInternal(Class<? extends Resource> clazz, HttpInputMessage inputMessage) throws IOException {
        byte[] buffer = IOUtil.copyToByteArray(inputMessage.getBody());
        return new ByteArrayResource(buffer);
    }

    @Override
    protected void writeInternal(Resource resource, ContentType contentType, HttpOutputMessage httpOutputMessage) throws IOException {
        httpOutputMessage.getBody().writeResource(resource);
    }
}

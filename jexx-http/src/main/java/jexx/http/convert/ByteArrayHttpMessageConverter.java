package jexx.http.convert;

import jexx.http.ContentType;
import jexx.http.HttpInputMessage;
import jexx.http.HttpOutputMessage;
import jexx.io.IOUtil;

import java.io.IOException;
import java.io.InputStream;

public class ByteArrayHttpMessageConverter extends AbstractHttpMessageConverter<byte[]> {

    public ByteArrayHttpMessageConverter() {
        super(null, ContentType.APPLICATION_OCTET_STREAM, ContentType.ALL);
    }

    @Override
    protected boolean supports(Class<?> clazz) {
        return byte[].class == clazz;
    }

    @Override
    protected byte[] readInternal(Class<? extends byte[]> clazz, HttpInputMessage inputMessage) throws IOException {
        long contentLength = inputMessage.getHeaders().getContentLength();
        InputStream inputStream = inputMessage.getBody();
        return contentLength > 0 ? IOUtil.readBytes(inputStream, (int)contentLength) : IOUtil.readBytes(inputStream);
    }

    @Override
    protected void writeInternal(byte[] bytes, ContentType contentType, HttpOutputMessage httpOutputMessage) throws IOException {
        writeBody(httpOutputMessage.getBody(), bytes);
    }
}

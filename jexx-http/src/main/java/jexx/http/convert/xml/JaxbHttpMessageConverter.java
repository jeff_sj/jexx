package jexx.http.convert.xml;

import jexx.http.*;
import jexx.http.convert.AbstractHttpMessageConverter;
import jexx.util.StringUtil;
import jexx.xml.XmlUtil;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;

public class JaxbHttpMessageConverter extends AbstractHttpMessageConverter<Object> {

    public JaxbHttpMessageConverter() {
        super(null, ContentType.APPLICATION_XML);
    }

    @Override
    protected boolean supports(Class<?> clazz) {
        return clazz.isAnnotationPresent(XmlRootElement.class);
    }

    @Override
    protected Object readInternal(Class<?> clazz, HttpInputMessage inputMessage) throws IOException {
        return XmlUtil.toBean(inputMessage.getBody(), clazz);
    }

    @Override
    protected void writeInternal(Object object, ContentType contentType, HttpOutputMessage httpOutputMessage) throws IOException {
        String xml = XmlUtil.toXML(object);
        Charset charset = getCharset(contentType);
        byte[] buffer = StringUtil.getBytes(xml, charset);

        writeBody(httpOutputMessage.getBody(), buffer);
    }

}

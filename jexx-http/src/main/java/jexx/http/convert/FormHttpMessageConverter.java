package jexx.http.convert;

import jexx.collect.MultiValueMap;
import jexx.http.*;
import jexx.http.exception.HttpException;
import jexx.io.FileNameUtil;
import jexx.io.Resource;
import jexx.net.MimeTypes;
import jexx.random.RandomUtil;
import jexx.util.StringUtil;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class FormHttpMessageConverter extends AbstractHttpMessageConverter<MultiValueMap<String, ?>> {

    private List<HttpMessageConverter<?>> partConverters = new ArrayList<>();

    public FormHttpMessageConverter() {
        super(null, ContentType.APPLICATION_FORM_URLENCODED, ContentType.MULTIPART_FORM_DATA, ContentType.MULTIPART_MIXED);

        this.partConverters.add(new ByteArrayHttpMessageConverter());
        this.partConverters.add(new StringHttpMessageConverter());
        this.partConverters.add(new ResourceHttpMessageConverter());
    }

    @Override
    protected boolean supports(Class<?> clazz) {
        return MultiValueMap.class.isAssignableFrom(clazz);
    }

    @Override
    protected MultiValueMap<String, ?> readInternal(Class<? extends MultiValueMap<String, ?>> clazz, HttpInputMessage inputMessage){
        throw new UnsupportedOperationException("暂不支持读操作");
    }

    @Override
    @SuppressWarnings("unchecked")
    protected void writeInternal(MultiValueMap<String, ?> map, ContentType contentType, HttpOutputMessage httpOutputMessage) throws IOException {
        if(isMultipart(map, contentType)){
            writeMultipart((MultiValueMap<String, Object>)map, contentType, httpOutputMessage);
        }
        else{
            writeForm(map, contentType, httpOutputMessage);
        }
    }

    private boolean isMultipart(MultiValueMap<String, ?> map, ContentType contentType) {
        if (contentType != null) {
            return "multipart".equals(contentType.getType());
        }
        for (List<?> values : map.values()) {
            for (Object value : values) {
                if (value != null && !(value instanceof String)) {
                    return true;
                }
            }
        }
        return false;
    }

    protected void writeForm(MultiValueMap<String, ?> map, ContentType contentType, HttpOutputMessage httpOutputMessage) throws IOException {
        Charset charset = getCharset(contentType);

        StringBuilder builder = new StringBuilder();
        map.forEach((name, values) -> {
            values.forEach((value) -> {
                try {
                    if (builder.length() != 0) {
                        builder.append('&');
                    }
                    builder.append(URLEncoder.encode(name, charset.name()));
                    if (value != null) {
                        builder.append('=');
                        builder.append(URLEncoder.encode(String.valueOf(value), charset.name()));
                    }

                } catch (UnsupportedEncodingException var5) {
                    throw new IllegalStateException(var5);
                }
            });
        });

        byte[] buffer = builder.toString().getBytes(charset);
        RequestBody httpBody = httpOutputMessage.getBody();
        httpBody.write(buffer);
    }

    protected void writeMultipart(MultiValueMap<String, Object> parts, ContentType contentType, HttpOutputMessage httpOutputMessage) throws IOException {
        if(contentType == null){
            contentType = ContentType.MULTIPART_FORM_DATA;
        }

        byte[] boundary = generateBoundary();

        Map<String, String> parameters = new LinkedHashMap<>(2);
        parameters.put("boundary", new String(boundary, StandardCharsets.US_ASCII));
        contentType = new ContentType(contentType, parameters);
        httpOutputMessage.getHeaders().setContentType(contentType);

        RequestBody httpBody = httpOutputMessage.getBody();
        writeParts(httpBody, parts, boundary);
        writeEnd(httpBody, boundary);
    }

    private void writeParts(RequestBody requestBody, MultiValueMap<String, Object> parts, byte[] boundary) throws IOException {
        for (Map.Entry<String, List<Object>> entry : parts.entrySet()) {
            String name = entry.getKey();
            for (Object part : entry.getValue()) {
                if (part != null) {
                    writeBoundary(requestBody, boundary);
                    RequestEntity<?> partEntity = (part instanceof RequestEntity ? (RequestEntity<?>) part : new RequestEntity<>(part));
                    writePart(name, partEntity, requestBody);
                    writeNewLine(requestBody);
                }
            }
        }
    }

    @SuppressWarnings("unchecked")
    private void writePart(String name, RequestEntity<?> partEntity, RequestBody requestBody) throws IOException {
        Object partBody = partEntity.getBody();
        if (partBody == null) {
            throw new IllegalStateException("Empty body for part '" + name + "': " + partEntity);
        }
        Class<?> partType = partBody.getClass();
        HttpHeaders partHeaders = partEntity.getHeaders();
        ContentType partContentType = partHeaders.getContentType();
        for (HttpMessageConverter<?> messageConverter : this.partConverters) {
            if (messageConverter.supportWrite(partType, partContentType)) {
                Charset charset = getCharset(partContentType);
                MultipartHttpOutputMessage multipartMessage = new MultipartHttpOutputMessage(requestBody, charset);

                String fileName = null;
                if(partBody instanceof Resource){
                    fileName = ((Resource) partBody).getFilename();
                    if(partContentType == null){
                        try {
                            partContentType = getContentType((Resource) partBody);
                        }
                        catch (Exception e){
                            //ignore
                        }
                        if(partContentType == null){
                            partContentType = ContentType.APPLICATION_OCTET_STREAM;
                        }
                        partHeaders.setContentType(partContentType);
                    }
                }
                multipartMessage.getHeaders().setContentDispositionFormData(name, fileName);
                if (!partHeaders.isEmpty()) {
                    multipartMessage.getHeaders().putAll(partHeaders);
                }
                multipartMessage.writeHeaders();
                ((HttpMessageConverter<Object>) messageConverter).write(partBody, partContentType, multipartMessage);
                return;
            }
        }
        throw new HttpException("Could not write request: no suitable HttpMessageConverter " +
                "found for request type [" + partType.getName() + "]");
    }

    protected byte[] generateBoundary(){
        String uuid = RandomUtil.randomUUID();
        return uuid.getBytes(StandardCharsets.UTF_8);
    }

    protected ContentType getContentType(Resource resource){
        String filename = resource.getFilename();
        if(StringUtil.isEmpty(filename)){
            return null;
        }
        String ext = FileNameUtil.getExtension(filename);
        if(StringUtil.isEmpty(ext)){
            return null;
        }
        String mt = MimeTypes.lookupMimeType(ext);
        if(StringUtil.isEmpty(mt)){
            return null;
        }
        return ContentType.parseContentType(mt);
    }

    private void writeBoundary(RequestBody requestBody, byte[] boundary) throws IOException {
        requestBody.write("--".getBytes());
        requestBody.write(boundary);
        writeNewLine(requestBody);
    }

    private static void writeEnd(RequestBody requestBody, byte[] boundary) throws IOException {
        requestBody.write("--".getBytes());
        requestBody.write(boundary);
        requestBody.write("--".getBytes());
        writeNewLine(requestBody);
    }

    private static void writeNewLine(RequestBody requestBody) throws IOException {
        requestBody.write("\r\n".getBytes());
    }


    private static class MultipartHttpOutputMessage implements HttpOutputMessage {

        private final Charset charset;
        private final HttpHeaders headers = new HttpHeaders();

        private boolean headersWritten = false;
        private RequestBody requestBody;

        public MultipartHttpOutputMessage(RequestBody requestBody, Charset charset) {
            this.requestBody = requestBody;
            this.charset = charset;
        }

        @Override
        public HttpHeaders getHeaders() {
            return this.headers;
        }

        @Override
        public RequestBody getBody() {
            try {
                writeHeaders();
            } catch (IOException e) {
                //ignore
            }
            return this.requestBody;
        }

        private void writeHeaders() throws IOException {
            if (!this.headersWritten) {
                for (Map.Entry<String, List<String>> entry : this.headers.entrySet()) {
                    byte[] headerName = entry.getKey().getBytes(this.charset);
                    for (String headerValueString : entry.getValue()) {
                        byte[] headerValue = headerValueString.getBytes(this.charset);
                        this.requestBody.write(headerName);
                        this.requestBody.write(": ".getBytes(charset));
                        this.requestBody.write(headerValue);
                        writeNewLine(this.requestBody);
                    }
                }
                writeNewLine(this.requestBody);
                this.headersWritten = true;
            }
        }

    }

}

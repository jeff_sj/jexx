package jexx.http.convert;

import jexx.exception.IORuntimeException;
import jexx.http.ContentType;
import jexx.http.HttpInputMessage;
import jexx.http.HttpOutputMessage;
import jexx.http.RequestBody;
import jexx.util.Assert;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public abstract class AbstractHttpMessageConverter<T> implements HttpMessageConverter<T> {

    public static final Charset DEFAULT_CHARSET = StandardCharsets.UTF_8;

    private Charset defaultCharset = DEFAULT_CHARSET;
    private List<ContentType> supportedContentTypes = Collections.emptyList();

    public AbstractHttpMessageConverter(Charset defaultCharset, ContentType... supportedContentTypes) {
        if(defaultCharset != null){
            this.defaultCharset = defaultCharset;
        }
        setSupportedContentTypes(Arrays.asList(supportedContentTypes));
    }

    public void setSupportedContentTypes(List<ContentType> supportedContentTypes) {
        Assert.notEmpty(supportedContentTypes, "MediaType List must not be empty");
        this.supportedContentTypes = new ArrayList<>(supportedContentTypes);
    }

    @Override
    public List<ContentType> getSupportContentTypes() {
        return Collections.unmodifiableList(this.supportedContentTypes);
    }

    protected abstract boolean supports(Class<?> clazz);

    protected boolean supportRead(ContentType contentType){
        if(contentType == null){
            return true;
        }
        for (ContentType ct : getSupportContentTypes()){
            if(ct.include(contentType)){
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean supportRead(Class<?> clazz, ContentType contentType) {
        return supports(clazz) && supportRead(contentType);
    }

    @Override
    public final T read(Class<? extends T> clazz, HttpInputMessage inputMessage) {
        try {
            return readInternal(clazz, inputMessage);
        } catch (IOException e) {
            throw new IORuntimeException(e);
        }
    }

    protected abstract T readInternal(Class<? extends T> clazz, HttpInputMessage inputMessage) throws IOException;

    protected boolean supportWrite(ContentType contentType) {
        if(contentType == null){
            return true;
        }
        for (ContentType ct : getSupportContentTypes()){
            if(ct.include(contentType)){
                return true;
            }
        }
        return false;
    }

    @Override
    public final boolean supportWrite(Class<?> clazz, ContentType contentType) {
        return supports(clazz) && supportWrite(contentType);
    }

    @Override
    public final void write(T t, ContentType contentType, HttpOutputMessage httpOutputMessage) {
        try {
             writeInternal(t, contentType, httpOutputMessage);
        } catch (IOException e) {
            throw new IORuntimeException(e);
        }
    }

    protected abstract void writeInternal(T t, ContentType contentType, HttpOutputMessage httpOutputMessage) throws IOException;

    protected void writeBody(RequestBody body, byte[] buffer) throws IOException {
        body.write(buffer);
    }

    protected Charset getCharset(ContentType contentType){
        if(contentType == null){
            return defaultCharset;
        }
        Charset charset = contentType.getCharset();
        if(charset == null){
            charset = defaultCharset;
        }
        return charset;
    }

}

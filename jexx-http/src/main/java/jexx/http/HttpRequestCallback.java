package jexx.http;

@FunctionalInterface
public interface HttpRequestCallback {

    void wrap(HttpRequest request);

}

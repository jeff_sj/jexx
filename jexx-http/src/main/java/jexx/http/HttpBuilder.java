package jexx.http;

import jexx.http.client.*;
import jexx.http.client.ssl.DefaultSSLContextFactory;
import jexx.http.client.ssl.NoopSSLContextFactory;
import jexx.util.ClassUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Http构建对象
 * @author jeff
 */
public class HttpBuilder {

    private static final Logger log = LoggerFactory.getLogger(HttpBuilder.class);

    private static final boolean HTTP_COMPONENTS_PRESENT;

    static {
        ClassLoader classLoader = HttpBuilder.class.getClassLoader();
        HTTP_COMPONENTS_PRESENT = ClassUtil.exist("org.apache.http.client.HttpClient", classLoader);
    }

    private SSLContextFactory sslContextFactory;
    private ClientHttpRequestFactory requestFactory;

    /**
     * 全局设置是否忽视ssl
     */
    private static boolean ignoreSsl;

    private HttpBuilder() {
    }

    public HttpBuilder requestFactory(ClientHttpRequestFactory requestFactory){
        this.requestFactory = requestFactory;
        return this;
    }

    /**
     * 设置忽视ssl
     */
    public static void setIgnoreSsl(boolean ignoreSsl) {
        HttpBuilder.ignoreSsl = ignoreSsl;
    }

    public HttpBuilder sslIgnore(){
        this.sslContextFactory = new NoopSSLContextFactory();
        return this;
    }

    public HttpBuilder sslLoad(byte[] cert, String password){
        this.sslContextFactory = new DefaultSSLContextFactory(cert, password.toCharArray());
        return this;
    }

    public Http build(){
        if(requestFactory == null){
            if(HTTP_COMPONENTS_PRESENT){
                requestFactory = new HttpComponentsClientHttpRequestFactory();
            }
            else{
                requestFactory = new SimpleClientHttpRequestFactory();
            }
        }

        if(requestFactory instanceof AbstractClientHttpRequestFactory){
            SSLContextFactory sslContextFactory;
            if(this.sslContextFactory instanceof NoopSSLContextFactory){
                sslContextFactory = this.sslContextFactory;
            }
            else if(HttpBuilder.ignoreSsl){
                if(this.sslContextFactory != null){
                    log.debug("'ignoreSsl' is true, so ignore the sslContextFactory");
                }
                sslContextFactory = new NoopSSLContextFactory();
            }
            else{
                sslContextFactory = this.sslContextFactory;
            }
            ((AbstractClientHttpRequestFactory) requestFactory).setSslContextFactory(sslContextFactory);
        }

        return new DefaultHttp(requestFactory);
    }

    public static HttpBuilder create(){
        return new HttpBuilder();
    }


}

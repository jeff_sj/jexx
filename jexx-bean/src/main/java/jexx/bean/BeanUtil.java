package jexx.bean;

import jexx.util.Assert;
import jexx.util.CollectionUtil;
import jexx.util.ReflectUtil;

import java.beans.BeanInfo;
import java.beans.PropertyDescriptor;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * bean工具类
 */
public class BeanUtil {

    /**
     * 判断是否为Bean对象<br>
     * 判定方法是是否存在只有一个参数的setXXX方法
     *
     * @param clazz 待测试类
     * @return 是否为Bean对象
     */
    public static boolean isBean(Class<?> clazz) {
        return CachedIntrospectionResults.forClass(clazz).isBean();
    }

    /**
     * 根据bean描述
     */
    public static BeanInfo getBeanInfo(Class<?> cla){
        return CachedIntrospectionResults.forClass(cla).getBeanInfo();
    }

    //--------------------------------------------------------------------------toBean

    /**
     * 根据类类型, 数据 生成bean对象; 默认字段值与字段类型是匹配的
     * @param cla   类类型
     * @param map   数据
     * @param <T>   bean类型
     * @return  返回bean
     */
    public static <T> T toBean(final Class<T> cla, final Map<String,Object> map){
        return toBean(cla, map, new IBeanConsumer() {
            @Override
            public String transferKey(String fieldName) {
                return fieldName;
            }

            @Override
            public Object transferValue(Object value) {
                return value;
            }
        });
    }

    /**
     * 根据类类型, 数据 生成bean对象
     * @param clazz   类类型
     * @param map   数据
     * @param consumer  字段名和字段值转换类
     * @param <T>   bean类型
     * @return  返回bean
     */
    public static <T> T toBean(final Class<T> clazz, final Map<String,Object> map, final IBeanConsumer consumer){
        Assert.isTrue(isBean(clazz), "{} is not a bean!", clazz);
        T obj = ReflectUtil.newInstance(clazz);

        DyBean bean = DyBeanFactory.createDyBean(obj,false, false);
        map.forEach((k,v)->{
            String key = consumer != null ? consumer.transferKey(k) : k;
            Object value = consumer != null ? consumer.transferValue(v) : v;

            if(bean.isWritableProperty(key)){
                bean.setPropertyValue(key, value);
            }
        });
        return obj;
    }

    /**
     * 根据类类型, 数据 生成bean对象集合; 默认字段值与字段类型是匹配的
     * @param clazz   类类型
     * @param rows   数据
     * @param <T>   bean类型
     * @return  返回bean
     */
    public static <T> List<T> toBeans(final Class<T> clazz, final List<Map<String,Object>> rows){
        return toBeans(clazz, rows, null);
    }


    /**
     * 根据类类型, 数据 生成bean对象集合
     * @param clazz   类类型
     * @param rows   数据集合
     * @param consumer  字段名和字段值转换类
     * @param <T>   bean类型
     * @return  返回bean
     */
    public static <T> List<T> toBeans(final Class<T> clazz, final List<Map<String,Object>> rows, final IBeanConsumer consumer){
        Assert.isTrue(isBean(clazz), "Class \"{}\" is not bean!", clazz);
        if(CollectionUtil.isEmpty(rows)){
            return null;
        }
        List<T> list = new ArrayList<>();
        for(Map<String,Object> data : rows){
            list.add(toBean(clazz, data, consumer));
        }
        return list;
    }

    //--------------------------------------------------------------------------toMap


    public static Map<String, Object> toMap(Object obj){
        Map<String, Object> targetMap = new HashMap<>();
        return toMap(obj, targetMap);
    }

    /**
     * 对象转map
     * @param obj bean对象; 支持对象是map
     * @param targetMap 目标map
     * @return 返回抽取对象属性后的map
     */
    @SuppressWarnings("unchecked")
    public static Map<String, Object> toMap(Object obj, final Map<String, Object> targetMap){
        if(obj instanceof Map){
            targetMap.putAll((Map<String,Object>)obj);
            return targetMap;
        }
        Assert.isTrue(isBean(obj.getClass()), "obj is not a map or a bean!");

        DyBean bean = DyBeanFactory.createDyBean(obj,false, false);

        PropertyDescriptor[] propertyDescriptors = bean.getPropertyDescriptors();
        for(PropertyDescriptor propertyDescriptor : propertyDescriptors){
            String name = propertyDescriptor.getName();
            //skip class property
            if("class".equals(name)){
                continue;
            }
            targetMap.put(name, bean.getPropertyValue(name));
        }
        return targetMap;
    }

    //-------------------------------------------------------------------------- value

    /**
     * 获取对象的字段值<br/>
     * 支持对象为MAP类型
     * @param bean 对象
     * @param fieldName 字段名称
     * @return 字段对应的值
     */
    public static Object getFieldValue(Object bean, String fieldName) {
        if (bean instanceof Map) {
            return ((Map<?, ?>) bean).get(fieldName);
        } else {
            return ReflectUtil.getFieldValue(bean, fieldName);
        }
    }

    public interface IBeanConsumer{
        /**
         * 转换成类中的实际字段名称
         * @param fieldName 字段名
         * @return  类中的字段名
         */
        String transferKey(String fieldName);

        /**
         * 转换字段
         * @param value 字段值, 可能类型与实际类型不匹配
         * @return 转换后的值
         */
        Object transferValue(Object value);
    }

}

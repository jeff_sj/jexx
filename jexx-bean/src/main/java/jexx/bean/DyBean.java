package jexx.bean;

import java.beans.PropertyDescriptor;

public interface DyBean {

    /**
     * 属性是否可读
     * @param propertyName 属性名
     */
    boolean isReadableProperty(String propertyName);

    /**
     * 获取属性对应值
     * @param propertyName 属性名
     */
    Object getPropertyValue(String propertyName);

    /**
     * 属性是否可写
     * @param propertyName 属性名
     */
    boolean isWritableProperty(String propertyName);

    /**
     * 设置属性对应值
     * @param propertyName 属性名
     * @param value 值
     */
    void setPropertyValue(String propertyName, Object value);

    /**
     * 获取属性描述集合
     */
    PropertyDescriptor[] getPropertyDescriptors();

    /**
     * 根据属性名称获取属性描述
     * @param propertyName 属性名
     */
    PropertyDescriptor getPropertyDescriptor(String propertyName);

}

package jexx.bean;

public interface PropertyAccessor {

    /**
     * 内嵌属性分隔符，如 foo.bar
     */
    String NESTED_PROPERTY_SEPARATOR = ".";

    /**
     * 属性分隔符，如 foo.bar
     */
    char NESTED_PROPERTY_SEPARATOR_CHAR = '.';

    /**
     * 索引属性或者map属性的key开始标识符, 如 person.addresses[0]
     */
    String PROPERTY_KEY_PREFIX = "[";

    /**
     * 索引属性或者map属性的key开始标识符, 如 person.addresses[0]
     */
    char PROPERTY_KEY_PREFIX_CHAR = '[';

    /**
     * 索引属性或者map属性的key结束标识符, 如 person.addresses[0]
     */
    String PROPERTY_KEY_SUFFIX = "]";

    /**
     * 索引属性或者map属性的key结束标识符, 如 person.addresses[0]
     */
    char PROPERTY_KEY_SUFFIX_CHAR = ']';


    /**
     * 是否为可读属性
     */
    boolean isReadableProperty(String propertyName);

    /**
     * 是否为可写属性
     */
    boolean isWritableProperty(String propertyName);


}

package jexx.bean;

/**
 * DyBean工厂类
 */
public class DyBeanFactory {

    /**
     * 根据对象创建动态bean
     * @param obj 包装对象
     * @param allowCreateHoldValueIfNull 如果持有对象为null，是否允许创建
     * @param allowCollectionAutoGrow 集合对象是否允许自动增长
     * @return 动态bean
     */
    public static DyBean createDyBean(Object obj, boolean allowCreateHoldValueIfNull, boolean allowCollectionAutoGrow){
        DyBeanImpl bean = new DyBeanImpl();
        bean.setAllowCreateHoldValueIfNull(allowCreateHoldValueIfNull);
        bean.setAllowCollectionAutoGrow(allowCollectionAutoGrow);
        bean.setWrappedInstance(obj);
        return bean;
    }

    /**
     * 根据对象创建动态bean
     * @param obj 包装对象
     * @return 动态bean
     */
    public static DyBean createDyBean(Object obj){
        return createDyBean(obj,true, true);
    }

}

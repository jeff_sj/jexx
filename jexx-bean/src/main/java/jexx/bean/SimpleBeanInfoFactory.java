package jexx.bean;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.lang.reflect.Method;

/**
 * 默认的BeanInfo实现工厂类
 */
public class SimpleBeanInfoFactory implements BeanInfoFactory {

    @Override
    public BeanInfo getBeanInfo(Class<?> beanClass) throws IntrospectionException {
        return supports(beanClass) ? new SimpleBeanInfo(Introspector.getBeanInfo(beanClass)):null;
    }

    private boolean supports(Class<?> beanClass) {
        for (Method method : beanClass.getMethods()) {
            if(SimpleBeanInfo.isWriteMethod(method)) {
                return true;
            }
        }
        return false;
    }

}

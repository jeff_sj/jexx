package jexx.bean;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/***
 * 类视图缓存;
 * TODO Spring的该类实现了弱引用MAP来进行缓存，参考下
 */
public class CachedIntrospectionResults {

    private static List<BeanInfoFactory> beanInfoFactories = new ArrayList<>();
    static {
        beanInfoFactories.add(new SimpleBeanInfoFactory());
    }

    static final ConcurrentMap<Class<?>, CachedIntrospectionResults> classCache =
            new ConcurrentHashMap<>(64);

    private final BeanInfo beanInfo;
    private final Map<String, PropertyDescriptor> propertyDescriptorCache;

    public static CachedIntrospectionResults forClass(Class<?> beanClass){
        CachedIntrospectionResults results = classCache.get(beanClass);
        if (results != null) {
            return results;
        }

        results = new CachedIntrospectionResults(beanClass);
        classCache.put(beanClass, results);
        return results;
    }

    private static BeanInfo getBeanInfo(Class<?> beanClass) throws IntrospectionException {
        for (BeanInfoFactory beanInfoFactory : beanInfoFactories) {
            BeanInfo beanInfo = beanInfoFactory.getBeanInfo(beanClass);
            if (beanInfo != null) {
                return beanInfo;
            }
        }
        return Introspector.getBeanInfo(beanClass);
    }

    private CachedIntrospectionResults(Class<?> beanClass){
        try {
            beanInfo = getBeanInfo(beanClass);

            this.propertyDescriptorCache = new LinkedHashMap<>();

            PropertyDescriptor[] pds = this.beanInfo.getPropertyDescriptors();
            for (PropertyDescriptor pd : pds) {
                if (Class.class == beanClass &&
                        ("classLoader".equals(pd.getName()) ||  "protectionDomain".equals(pd.getName()))) {
                    // Ignore Class.getClassLoader() and getProtectionDomain() methods - nobody needs to bind to those
                    continue;
                }
                this.propertyDescriptorCache.put(pd.getName(), pd);
            }
        }
        catch (IntrospectionException e){
            throw new BeanException(e);
        }
    }

    BeanInfo getBeanInfo(){
        return this.beanInfo;
    }

    Class<?> getBeanClass(){
        return this.beanInfo.getBeanDescriptor().getBeanClass();
    }

    PropertyDescriptor[] getPropertyDescriptors() {
        PropertyDescriptor[] pds = new PropertyDescriptor[this.propertyDescriptorCache.size()];
        int i = 0;
        for (PropertyDescriptor pd : this.propertyDescriptorCache.values()) {
            pds[i] = pd;
            i++;
        }
        return pds;
    }

    PropertyDescriptor getPropertyDescriptor(String name) {
        return this.propertyDescriptorCache.get(name);
    }

    boolean isBean(){
        for(Map.Entry<String, PropertyDescriptor> entry : propertyDescriptorCache.entrySet()){
            if(entry.getValue().getWriteMethod() != null){
                return true;
            }
        }
        return false;
    }

}

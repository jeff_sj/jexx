package jexx.bean;

import jexx.util.StringUtil;

public class BeanException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public BeanException(Throwable e) {
        super(e);
    }

    public BeanException(String message) {
        super(message);
    }

    public BeanException(String message, Object... params) {
        super(StringUtil.format(message, params));
    }

    public BeanException(Throwable throwable, String message, Object... params) {
        super(StringUtil.format(message, params), throwable);
    }

}

package jexx.bean;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class TestBean {

    private String a;
    private Integer b;
    private TestBean c;
    private Map<String, Integer> d;
    private List<String> e;
    private Set<String> f;

    private Map<String, TestBean> g;
    private List<TestBean> h;
    private Set<TestBean> i;

    private Map<String, List<Integer>> j;

    private Date k;
    private long l;

    public String getA() {
        return a;
    }

    public void setA(String a) {
        this.a = a;
    }

    public Integer getB() {
        return b;
    }

    public void setB(Integer b) {
        this.b = b;
    }

    public TestBean getC() {
        return c;
    }

    public void setC(TestBean c) {
        this.c = c;
    }

    public Map<String, Integer> getD() {
        return d;
    }

    public void setD(Map<String, Integer> d) {
        this.d = d;
    }

    public List<String> getE() {
        return e;
    }

    public void setE(List<String> e) {
        this.e = e;
    }

    public Set<String> getF() {
        return f;
    }

    public void setF(Set<String> f) {
        this.f = f;
    }

    public Map<String, TestBean> getG() {
        return g;
    }

    public void setG(Map<String, TestBean> g) {
        this.g = g;
    }

    public List<TestBean> getH() {
        return h;
    }

    public void setH(List<TestBean> h) {
        this.h = h;
    }

    public Set<TestBean> getI() {
        return i;
    }

    public void setI(Set<TestBean> i) {
        this.i = i;
    }

    public Map<String, List<Integer>> getJ() {
        return j;
    }

    public void setJ(Map<String, List<Integer>> j) {
        this.j = j;
    }

    public Date getK() {
        return k;
    }

    public void setK(Date k) {
        this.k = k;
    }

    public long getL() {
        return l;
    }

    public void setL(long l) {
        this.l = l;
    }
}

package jexx.bean;

import jexx.time.DateUtil;
import jexx.time.TimeInterval;
import jexx.util.Console;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

public class BeanUtilTest {

    @Test
    public void testToBean(){
        Map<String, Object> map = new HashMap<>();
        map.put("a", "a1");
        map.put("b", "1");
        map.put("k", "2017-10-10");
        map.put("l", "2");

        TestBean bean1 = BeanUtil.toBean(TestBean.class, map);
        Assert.assertEquals("a1", bean1.getA());
        Assert.assertEquals(1, bean1.getB().intValue());
        Assert.assertEquals("2017-10-10", DateUtil.formatDate(bean1.getK()));
        Assert.assertEquals(2, bean1.getL());
    }

    @Test
    public void testToMap(){
        TestBean testBean = new TestBean();
        testBean.setA("a1");
        testBean.setB(1);
        testBean.setK(DateUtil.parseDate("2017-10-10"));
        testBean.setL(2);

        Map<String, Object> map = BeanUtil.toMap(testBean);
        Assert.assertEquals(testBean.getA(), map.get("a"));
        Assert.assertEquals(testBean.getB(), map.get("b"));
        Assert.assertEquals(testBean.getK(), map.get("k"));
        Assert.assertEquals(testBean.getL(), map.get("l"));

        Map<Integer, String> map1 = new HashMap<>();
        map1.put(1, "1");
        Map<String, Object> map2 = new HashMap<>();
        BeanUtil.toMap(map1,map2);
        Assert.assertEquals("1", map2.get(1));
    }

    @Ignore
    @Test
    public void test(){
        TimeInterval timeInterval = new TimeInterval();
        timeInterval.start();
        for(int i =0; i < 100*10000; i++){
            testToBean();
        }
        Console.log("耗时 {}s", timeInterval.getTaskTimeSeconds());

    }

}

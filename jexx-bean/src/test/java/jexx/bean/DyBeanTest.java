package jexx.bean;

import jexx.util.Console;
import jexx.util.StringUtil;
import org.junit.Assert;
import org.junit.Test;

import java.util.*;

public class DyBeanTest {

    @Test
    public void testMap1(){
        Map<String, Object> map3D = new HashMap<>();
        map3D.put("field1", 111);

        DyBeanImpl dyBean = new DyBeanImpl();
        dyBean.setWrappedInstance(map3D);

        Assert.assertEquals(111, dyBean.getPropertyValue("$[field1]"));
        dyBean.setPropertyValue("$[field1]", 222);
        Assert.assertEquals(222, dyBean.getPropertyValue("$[field1]"));

        try{
            Map<Integer, String> map2D = new HashMap<>();
            map2D.put(1, "1");
            dyBean.setWrappedInstance(map2D);
            dyBean.getPropertyValue("$[1]");
            Assert.fail();
        }
        catch (IllegalArgumentException e){
            //skip
        }
    }

    @Test
    public void testMap2(){
        Map<String, Integer> map3D = new HashMap<>();
        map3D.put("field1", 111);

        TestBean bean3 = new TestBean();
        bean3.setD(map3D);

        Map<String, TestBean> map2G = new HashMap<>();
        map2G.put("xx", bean3);

        TestBean bean2 = new TestBean();
        bean2.setG(map2G);

        DyBeanImpl dyBean = new DyBeanImpl();
        dyBean.setWrappedInstance(bean2);
        Assert.assertEquals(111, dyBean.getPropertyValue("g[xx].d[field1]"));
        Assert.assertNull(dyBean.getPropertyValue("g[xx].d[field2]"));
    }

    @Test
    public void testWriteOneLevel(){
        TestBean bean = new TestBean();
        DyBeanImpl dyBean = new DyBeanImpl();
        dyBean.setWrappedInstance(bean, null, null);

        //common
        dyBean.setPropertyValue("a", "aaa");
        Assert.assertEquals("aaa", dyBean.getPropertyValue("a"));
        dyBean.setPropertyValue("b", "10");
        Assert.assertEquals(10, dyBean.getPropertyValue("b"));

        //bean
        dyBean.setPropertyValue("c", new TestBean());
        dyBean.setPropertyValue("c.a", "yyy");
        Assert.assertEquals("yyy", dyBean.getPropertyValue("c.a"));

        //map
        dyBean.setPropertyValue("d", new HashMap<String, Integer>());
        dyBean.setPropertyValue("d[d1]", "11");
        Assert.assertEquals(11, dyBean.getPropertyValue("d[d1]"));
        dyBean.setPropertyValue("d[d2]", 22);
        Assert.assertEquals(22, dyBean.getPropertyValue("d[d2]"));
        dyBean.setPropertyValue("d[\"d2\"]", 33);
        Assert.assertEquals(33, dyBean.getPropertyValue("d[\"d2\"]"));
        dyBean.setPropertyValue("d['d2']", 44);
        Assert.assertEquals(44, dyBean.getPropertyValue("d['d2']"));

        //list
        dyBean.setPropertyValue("e", new ArrayList<String>());
        dyBean.setPropertyValue("e[0]", "e0");
        Assert.assertEquals("e0", dyBean.getPropertyValue("e[0]"));
        dyBean.setPropertyValue("e[3]", "e3");
        Assert.assertEquals("e3", dyBean.getPropertyValue("e[3]"));

        //set
        Set<String> set = new LinkedHashSet<>();
        set.add("f0");
        set.add("f1");
        bean.setF(set);
        Assert.assertEquals("f0", dyBean.getPropertyValue("f[0]"));
        Assert.assertEquals("f1", dyBean.getPropertyValue("f[1]"));
    }

    @Test
    public void testWriteTwoLevel(){
        TestBean bean = new TestBean();
        DyBeanImpl dyBean = new DyBeanImpl();
        dyBean.setWrappedInstance(bean, null, null);

        //bean
        TestBean bean1 = new TestBean();
        dyBean.setPropertyValue("c", bean1);
        dyBean.setPropertyValue("c.a", "2_a");
        Assert.assertEquals("2_a", dyBean.getPropertyValue("c.a"));
        dyBean.setPropertyValue("c.b", "10");
        Assert.assertEquals(10, dyBean.getPropertyValue("c.b"));

        dyBean.setPropertyValue("c.d", new HashMap<String, Integer>());
        dyBean.setPropertyValue("c.d[d1]", "1");
        Assert.assertEquals(1, dyBean.getPropertyValue("c.d[d1]"));

        dyBean.setPropertyValue("c.e", new ArrayList<String>());
        dyBean.setPropertyValue("c.e[0]", "2_e0");
        Assert.assertEquals("2_e0", dyBean.getPropertyValue("c.e[0]"));
        dyBean.setPropertyValue("c.e[1]", "2_e1");
        Assert.assertEquals("2_e1", dyBean.getPropertyValue("c.e[1]"));

        Set<String> set = new LinkedHashSet<>();
        set.add("f0");
        set.add("f1");
        bean1.setF(set);
        Assert.assertEquals("f0", dyBean.getPropertyValue("c.f[0]"));
        Assert.assertEquals("f1", dyBean.getPropertyValue("c.f[1]"));

        dyBean.setPropertyValue("j[j1][0]", "11");
        Assert.assertEquals(11, dyBean.getPropertyValue("j[j1][0]"));
        dyBean.setPropertyValue("j[j1][1]", 22);
        Assert.assertEquals(22, dyBean.getPropertyValue("j[j1][1]"));
    }


    private static int ccc = 0;

    @Test
    public void testWriteThirdLevel() {
        TestBean bean = new TestBean();
        DyBeanImpl dyBean = new DyBeanImpl();
        dyBean.setWrappedInstance(bean, null, null);

        digui(dyBean, bean, 0, "");

        Map<String, Object> xx = dyBean.getMapIndexPropertyValues("h[].d[]");
        for(int i = 0; i < bean.getH().size(); i++){
            Map<String, Integer> b = bean.getH().get(i).getD();
            for(int j = 0; j < b.size(); j++){
                String key = StringUtil.format("h[{}].d[{}]", i, j);
                Assert.assertEquals(b.get(Integer.toString(j)), dyBean.getPropertyValue(key));
                Assert.assertEquals(b.get(Integer.toString(j)), xx.get(key));
            }
        }

        xx = dyBean.getMapIndexPropertyValues("h[].h[].a");
        for(int i = 0; i < bean.getH().size(); i++){
            List<TestBean> h = bean.getH().get(i).getH();
            for(int j = 0; j < h.size(); j++){
                String key = StringUtil.format("h[{}].h[{}].a", i, j);
                Assert.assertEquals(h.get(j).getA(), dyBean.getPropertyValue(key));
            }
        }

        xx = dyBean.getMapIndexPropertyValues("f[]");
        Iterator<String> it = bean.getF().iterator();
        for (int j = 0; it.hasNext(); j++) {
            String key = StringUtil.format("f[{}]", j);
            String elem = it.next();
            Assert.assertEquals(elem, xx.get(key));
        }

        xx = dyBean.getMapIndexPropertyValues("a");
        Assert.assertEquals("0_a", xx.get("a"));

        xx = dyBean.getMapIndexPropertyValues("c.a");
        Assert.assertEquals("1_a", xx.get("c.a"));



        Console.log("递归数量：{}", ccc);
    }

    private void digui(DyBeanImpl dyBean, TestBean bean, int level, String nestedPath){
        if(level >= 3){
            return;
        }
        ccc++;
        String newNestedPath = nestedPath.isEmpty() ? "" : nestedPath.concat(".");

        dyBean.setPropertyValue(newNestedPath.concat("a"), level+"_a");
        dyBean.setPropertyValue(newNestedPath.concat("b"), level);
        digui(dyBean, bean, level+1, newNestedPath.concat("c"));
        //Map<String, Integer> d
        for(int i = 0; i < 5; i++){
            dyBean.setPropertyValue(newNestedPath.concat("d[d").concat(Integer.toString(i)).concat("]"), i);
        }
        //List<String> e
        for(int i = 0; i < 5; i++){
            dyBean.setPropertyValue(newNestedPath.concat("e[").concat(Integer.toString(i)).concat("]"), level+"_e"+i);
        }
        //Set<String> f    set无序,不能设置值
        for(int i = 0; i < 5; i++){
            dyBean.setPropertyValue(newNestedPath.concat("f[").concat(Integer.toString(i)).concat("]"), level+"_f"+i);
        }
        //Map<String, TestBean> g
        for(int i = 0; i < 5; i++){
            digui(dyBean, bean, level+1, newNestedPath.concat("g[").concat(Integer.toString(i)).concat("]"));
        }
        //List<TestBean> h
        for(int i = 0; i < 5; i++){
            digui(dyBean, bean, level+1, newNestedPath.concat("h[").concat(Integer.toString(i)).concat("]"));
        }
        for(int i = 0; i < 5; i++){
            digui(dyBean, bean, level+1, newNestedPath.concat("i[").concat(Integer.toString(i)).concat("]"));
        }
        //Map<String, List<Integer>> j
        for(int i = 0; i < 5; i++){
            String jj = newNestedPath.concat("j[j").concat(Integer.toString(i)).concat("]");
            for(int j = 0; j < 5; j++){
                dyBean.setPropertyValue(jj.concat("[").concat(Integer.toString(j)).concat("]"), StringUtil.format("{}_j_{}_{}", level, i, j));
            }
        }
        //Date k
        dyBean.setPropertyValue(newNestedPath.concat("k"), new Date());
        //long l
        dyBean.setPropertyValue(newNestedPath.concat("l"), level);
    }

}

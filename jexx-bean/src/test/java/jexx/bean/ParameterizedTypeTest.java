package jexx.bean;

import org.junit.Assert;
import org.junit.Test;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

/**
 * ParameterizedType
 * @author jeff
 * @since 2020/4/21
 */
public class ParameterizedTypeTest {

    /**
     * java由于类型擦除的原因，无法动态获取map对象的参数类型；
     * 但是是可以通过field描述来获取map的参数类型的
     * @throws Exception
     */
    @Test
    public void testGetParameterizedType() throws Exception {
        Map<String, Object> map = new HashMap<>();

        Type type = map.getClass().getGenericSuperclass();
        Type[]  types = ((ParameterizedType) type).getActualTypeArguments();
        Assert.assertEquals("K", types[0].getTypeName());

        class A {
            public Map<String, Object> a;
        }
        Field field = A.class.getDeclaredField("a");
        type = field.getGenericType();
        types = ((ParameterizedType) type).getActualTypeArguments();
        Assert.assertEquals("java.lang.String", types[0].getTypeName());
    }

}

package jexx.util;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * @author jeff
 * @since 2020/1/22
 */
public class TreeUtilTest {

    @Test
    public void testBuildTree(){
        List<TestBean> list = new ArrayList<>();
        list.add(new TestBean(1, 0, "test1"));
        list.add(new TestBean(11, 1, "test11"));
        list.add(new TestBean(12, 1, "test12"));
        list.add(new TestBean(121, 12, "test121"));

        List<TestBean> tree = TreeUtil.buildTree(list, new TreeUtil.TreeMapper<TestBean, Integer>() {
            @Override
            public Integer getId(TestBean testBean) {
                return testBean.getId();
            }

            @Override
            public Integer getParentId(TestBean testBean) {
                return testBean.getParentId();
            }

            @Override
            public void addChild(TestBean parent, TestBean child) {
                if(parent.getChildren() == null){
                    parent.setChildren(new ArrayList<>());
                }
                parent.getChildren().add(child);
            }

            @Override
            public boolean isRoot(TestBean testBean) {
                return testBean.parentId == null || testBean.parentId == 0;
            }
        });
        Assert.assertEquals(1, tree.size());
        Assert.assertEquals(2, tree.get(0).getChildren().size());
        Assert.assertEquals(1, tree.get(0).getChildren().get(1).getChildren().size());
    }

    private static class TestBean{
        private Integer id;
        private Integer parentId;
        private String name;
        private List<TestBean> children;

        public TestBean(Integer id, Integer parentId, String name) {
            this.id = id;
            this.parentId = parentId;
            this.name = name;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getParentId() {
            return parentId;
        }

        public List<TestBean> getChildren() {
            return children;
        }

        public void setChildren(List<TestBean> children) {
            this.children = children;
        }
    }

}

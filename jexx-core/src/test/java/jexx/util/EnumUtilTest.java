package jexx.util;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author jeff
 * @since 2020/9/19
 */
public class EnumUtilTest {

    @Test
    public void testGetByName(){
        Assert.assertEquals(TestEnum.A, EnumUtil.getByName(TestEnum.class, "A"));
        Assert.assertEquals(TestEnum.A, EnumUtil.getByName(TestEnum.values(), "A"));
        Assert.assertEquals(TestEnum.A, EnumUtil.getByLambda(TestEnum.class, s -> s.getType() == 1));
    }

    private enum TestEnum{
        A(1),
        B(2),
        C(3)
        ;

        private final int type;

        TestEnum(int type) {
            this.type = type;
        }

        public int getType() {
            return type;
        }
    }

}

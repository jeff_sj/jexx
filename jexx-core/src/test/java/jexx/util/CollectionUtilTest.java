package jexx.util;

import static org.junit.Assert.*;

import org.junit.Assert;
import org.junit.Test;

import java.util.*;

public class CollectionUtilTest {

    @Test
    public void testCheckDisjoint(){
        Assert.assertTrue(CollectionUtil.checkDisjoint(CollectionUtil.list("1", "2"), CollectionUtil.list("3", "4")));
    }

    @Test
    public void testCheckJoint(){
        Assert.assertTrue(CollectionUtil.checkJoint(CollectionUtil.list("1", "2"), CollectionUtil.list("2", "3")));
    }

    @Test
    public void testFindAny(){
        List<Integer> list1 = new ArrayList<>();
        list1.add(1);
        list1.add(2);
        list1.add(3);

        Optional<Integer> optional = CollectionUtil.findAny(list1);
        assertTrue(optional.isPresent());
        assertTrue(list1.contains(optional.get()));
    }

    @Test
    public void testToList(){
        byte[] bytes = new byte[]{0x11, 0x12, 0x13};
        List<Byte> byteList = CollectionUtil.toList(bytes);
        Assert.assertEquals(bytes[2], (byte)byteList.get(2));

        char[] chars = new char[]{0x11, 0x12, 0x13};
        List<Character> characterList = CollectionUtil.toList(chars);
        Assert.assertEquals(chars[2], (char)characterList.get(2));

        short[] shorts = new short[]{0x11, 0x12, 0x13};
        List<Short> shortList = CollectionUtil.toList(shorts);
        Assert.assertEquals(shorts[2], (short)shortList.get(2));

        int[] ints = new int[]{0x11, 0x12, 0x13};
        List<Integer> integerList = CollectionUtil.toList(ints);
        Assert.assertEquals(ints[2], (int)integerList.get(2));

        long[] longs = new long[]{0x11, 0x12, 0x13};
        List<Long> longList = CollectionUtil.toList(longs);
        Assert.assertEquals(longs[2], (long)longList.get(2));

        float[] floats = new float[]{0x11, 0x12, 0x13};
        List<Float> floatList = CollectionUtil.toList(floats);
        Assert.assertEquals(floats[2], floatList.get(2), 0);

        double[] doubles = new double[]{0x11, 0x12, 0x13};
        List<Double> doubleList = CollectionUtil.toList(doubles);
        Assert.assertEquals(doubles[2], doubleList.get(2), 0);

        boolean[] booleans = new boolean[]{true, false, true};
        List<Boolean> booleanList = CollectionUtil.toList(booleans);
        Assert.assertEquals(booleans[2], booleanList.get(2));

        String[] strings = new String[]{"1", "2", "3"};
        List<String> stringList = CollectionUtil.toList(strings);
        Assert.assertEquals(strings[2], stringList.get(2));
    }

    @Test
    public void testFind(){
        List<Integer> left = CollectionUtil.list(1,2,3,4,5,6);
        List<Integer> right = CollectionUtil.list(4,5,6,7,8,9,10);
        Assert.assertTrue(CollectionUtil.list(4,5,6).containsAll(CollectionUtil.findSame(left, right)));
        Assert.assertTrue(CollectionUtil.list(1,2,3,7,8,9,10).containsAll(CollectionUtil.findDiff(left, right)));
        Assert.assertTrue(CollectionUtil.list(1,2,3,4,5,6,7,8,9,10).containsAll(CollectionUtil.findUnion(left, right)));
    }

    @Test
    public void testFindLeftDiff(){
        List<Integer> left = CollectionUtil.list(1,2,3,4,5,6);
        List<Integer> right = CollectionUtil.list(4,5,6,7,8,9,10);
        Assert.assertTrue(CollectionUtil.list(1,2,3).containsAll(CollectionUtil.findLeftDiff(left, right)));
        Assert.assertTrue(CollectionUtil.list(7,8,9,10).containsAll(CollectionUtil.findLeftDiff(right, left)));

        List<Bean1> left1 = CollectionUtil.list(new Bean1("1"), new Bean1("2"), new Bean1("3"));
        List<Bean2> right1 = CollectionUtil.list(new Bean2("2"), new Bean2("3"), new Bean2("4"));
        Set<Bean1> leftDiff1 = CollectionUtil.findLeftDiff(left1, Bean1::getId, right1, Bean2::getId);
        Assert.assertEquals(1, leftDiff1.size());
        Assert.assertTrue(leftDiff1.stream().findAny().isPresent());
        Assert.assertEquals("1", leftDiff1.stream().findAny().get().getId());
        Set<Bean2> rightDiff1 = CollectionUtil.findLeftDiff(right1, Bean2::getId, left1, Bean1::getId);
        Assert.assertEquals(1, rightDiff1.size());
        Assert.assertTrue(rightDiff1.stream().findAny().isPresent());
        Assert.assertEquals("4", rightDiff1.stream().findAny().get().getId());
        Set<Bean1> leftSame1 = CollectionUtil.findLeftSame(left1, Bean1::getId, right1, Bean2::getId);
        Assert.assertEquals(2, leftSame1.size());
        Assert.assertEquals(2, leftSame1.stream().filter(s->s.getId().equals("2") || s.getId().equals("3")).count());

        List<Bean1> left2 = CollectionUtil.list(new Bean1("1"), new Bean1("2"), new Bean1("3"));
        List<Bean1> right2 = CollectionUtil.list(new Bean1("2"), new Bean1("3"), new Bean1("4"));
        Set<Bean1> leftDiff2 = CollectionUtil.findLeftDiff(left2, right2, Bean1::getId);
        Assert.assertEquals(1, leftDiff2.size());
        Assert.assertTrue(leftDiff2.stream().findAny().isPresent());
        Assert.assertEquals("1", leftDiff2.stream().findAny().get().getId());
        Set<Bean1> leftSame2 = CollectionUtil.findLeftSame(left2, right2, Bean1::getId);
        Assert.assertEquals(2, leftSame2.size());
        Assert.assertEquals(2, leftSame2.stream().filter(s->s.getId().equals("2") || s.getId().equals("3")).count());

        Set<Bean1> leftSame3 = CollectionUtil.findLeftSame(left2, Bean1::getId, right2, Bean1::getId, (l, r)-> new Bean1(l.getId()+r.getId()));
        Assert.assertEquals(2, leftSame3.size());
        Assert.assertEquals(2, leftSame3.stream().filter(s->s.getId().equals("22") || s.getId().equals("33")).count());
    }

    @Test
    public void testDikaer(){
        List<List<Integer>> listList = CollectionUtil.list(
                CollectionUtil.list(1,2,3)
        );
        List<List<Integer>> dikaers =  CollectionUtil.dikaer(listList);
        String path = StringUtil.join(dikaers, ",", s->StringUtil.join(s, "-"));
        Assert.assertEquals("1,2,3", path);

        listList = CollectionUtil.list(
                CollectionUtil.list(1,2,3),
                CollectionUtil.list(4,5)
        );
        dikaers =  CollectionUtil.dikaer(listList);
        path = StringUtil.join(dikaers, ",", s->StringUtil.join(s, "-"));
        Assert.assertEquals("1-4,1-5,2-4,2-5,3-4,3-5", path);
    }

    private static class Bean1{
        private final String id;

        public Bean1(String id) {
            this.id = id;
        }

        public String getId() {
            return id;
        }
    }

    private static class Bean2{
        private final String id;

        public Bean2(String id) {
            this.id = id;
        }

        public String getId() {
            return id;
        }
    }

}

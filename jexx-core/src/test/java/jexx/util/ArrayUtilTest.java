package jexx.util;

import jexx.convert.Convert;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import static org.junit.Assert.*;

public class ArrayUtilTest {

    @Test
    public void testAppend(){
        Integer[] expectArray1 = {1, 2, 3, 4};
        Integer[] actualArray1 = ArrayUtil.append(new Integer[]{1,2}, 3, 4);
        assertArrayEquals(expectArray1, actualArray1);

        int[] expectArray2 = {1, 2, 3, 4};
        int[] actualArray2 = ArrayUtil.append(new int[]{1,2}, 3, 4);
        assertArrayEquals(expectArray2, actualArray2);
    }

    @Test
    public void testContain(){
        assertTrue(ArrayUtil.containNull(new String[]{"1", null}));
        assertFalse(ArrayUtil.containNull(new String[]{}));
    }

    @Test
    public void testHasSame(){
        assertTrue(ArrayUtil.hasSame(new int[]{1,2}, new int[]{1,3}));
        assertFalse(ArrayUtil.hasSame(new int[]{1,2}, new int[]{3}));
    }

    @Test
    public void testJoin1(){
        Integer[] expectArray = {1, 2, 3, 4};
        Integer[] t1 = new Integer[]{1,2};
        Integer[] t2 = new Integer[]{3,4};
        Integer[] actualArray = ArrayUtil.join(t1, t2);
        assertArrayEquals(expectArray, actualArray);
    }

    @Test
    public void testJoin2(){
        String[] expectArray = {"a", "b", "c", "d"};
        String[] t1 = new String[]{"a","b"};
        String[] t2 = new String[]{"c","d"};
        String[] actualArray = ArrayUtil.join(t1, t2);
        assertArrayEquals(expectArray, actualArray);
    }


    @Test
    public void testWrap(){
        boolean[] expectBoolArr = {true, false};
        Boolean[] boolArr = ArrayUtil.wrap(expectBoolArr);
        boolean[] actualBoolArr = ArrayUtil.unWrap(boolArr);
        assertArrayEquals(expectBoolArr, actualBoolArr);

        byte[] expectByteArr = {1, 2};
        Byte[] byteArr = ArrayUtil.wrap(expectByteArr);
        byte[] actualByteArr = ArrayUtil.unWrap(byteArr);
        assertArrayEquals(expectByteArr, actualByteArr);

        char[] charCharArr = {1, 2};
        Character[] characterArr = ArrayUtil.wrap(charCharArr);
        char[] actualCharArr = ArrayUtil.unWrap(characterArr);
        assertArrayEquals(charCharArr, actualCharArr);

        short[] charShortArr = {1, 2};
        Short[] shortArr = ArrayUtil.wrap(charShortArr);
        short[] actualShortArr = ArrayUtil.unWrap(shortArr);
        assertArrayEquals(charShortArr, actualShortArr);


        int[] expectIntArr = {1, 2, 3, 4, 5};
        Integer[] integerArr = ArrayUtil.wrap(expectIntArr);
        int[] actualIntArr = ArrayUtil.unWrap(integerArr);
        assertArrayEquals(expectIntArr, actualIntArr);

        long[] expectLongArr = {1L, 2L, 3L, 4L, 5L};
        Long[] longArr = ArrayUtil.wrap(expectLongArr);
        long[] actualLongArr = ArrayUtil.unWrap(longArr);
        assertArrayEquals(expectLongArr, actualLongArr);

        float[] expectFloatArr = {0.1f, 0.2f, 0.3f, 0.4f, 0.5f};
        Float[] floatArr = ArrayUtil.wrap(expectFloatArr);
        float[] actualFloatArr = ArrayUtil.unWrap(floatArr);
        assertArrayEquals(expectFloatArr, actualFloatArr, 0);

        double[] expectDoubleArr = {0.1d, 0.2d, 0.3d, 0.4d, 0.5d};
        Double[] doubleArr = ArrayUtil.wrap(expectDoubleArr);
        double[] actualDoubleArr = ArrayUtil.unWrap(doubleArr);
        assertArrayEquals(expectDoubleArr, actualDoubleArr, 0);

        Double[] expectWrapDoubleArr = {0.1d, 0.2d, 0.3d, 0.4d, 0.5d};
        Object[] actualObjArr = ArrayUtil.wrap(wrap(expectWrapDoubleArr));
        for(int i = 0; i < expectWrapDoubleArr.length; i++){
            Double a = expectWrapDoubleArr[i];
            Object b = actualObjArr[i];
            assertEquals(a , (double)b, 0);
        }
    }

    @Test
    public void testToArray(){
        List<Integer> list1 = new ArrayList<>();

        Integer[] actual = ArrayUtil.toArray(list1, Integer.class);
        assertEquals(0, actual.length);

        list1.add(1);
        list1.add(2);
        list1.add(3);
        actual = ArrayUtil.toArray(list1, Integer.class);
        Integer[] expect = ArrayUtil.toArray(1, 2, 3);
        assertArrayEquals(expect, actual);

        String[] array1 = {"1", "2", "3"};
        Integer[] target1 = ArrayUtil.toArray(array1, Integer.class, Integer::parseInt);
        Console.log(Convert.toStr(target1));
    }

    @Test
    public void testToStringArray(){
        Vector<String> list = new Vector<>();
        list.add("a");
        list.add("b");
        list.add("c");
        String[] strArray = ArrayUtil.toStringArray(list.elements());
        Assert.assertEquals(3, strArray.length);
        Assert.assertEquals("c", strArray[2]);

    }

    @Test
    public void testSubArray(){
        int[] array = {1,2,3,4,5,6};
        Assert.assertArrayEquals(new int[]{4,5,6}, ArrayUtil.subarray(array, 3));

        String[] array1 = {"1","2","3","4","5","6"};
        Assert.assertArrayEquals(new String[]{"4","5","6"}, ArrayUtil.subarray(array1, 3));
    }

    @Test
    public void testIndexOf(){
        int[] array = {1,2,3,4,5,6};
        Assert.assertEquals(2, ArrayUtil.indexOf(array, 3));
        Assert.assertEquals(2, ArrayUtil.lastIndexOf(array, 3));
        String[] array1 = {"1","2","3","4","5","6"};
        Assert.assertEquals(2, ArrayUtil.indexOf(array1, "3"));
        Assert.assertEquals(2, ArrayUtil.lastIndexOf(array1, "3"));
        String[] array2 = {"1","2","3","4","5","6",null};
        Assert.assertEquals(6, ArrayUtil.indexOf(array2,  null));
        Assert.assertEquals(6, ArrayUtil.lastIndexOf(array2,  null));
    }

    private Object wrap(Object obj){
        return obj;
    }

}

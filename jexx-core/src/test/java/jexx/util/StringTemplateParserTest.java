package jexx.util;

import jexx.template.MapTemplateParser;
import jexx.template.StringTemplateParser;
import org.junit.Test;

import java.util.HashMap;

import static org.junit.Assert.*;

public class StringTemplateParserTest {

    @Test
    public void testMap() {
        MapTemplateParser stp = new MapTemplateParser();

        HashMap<String, Object> map = new HashMap<>();
        map.put("key1", "value1");

        assertEquals("---value1---", stp.of(map).parse("---${key1}---"));
    }


    @Test
    public void testMissing() {
        MapTemplateParser stp = new MapTemplateParser();

        HashMap<String, Object> map = new HashMap<>();
        map.put("key1", "value1");


        assertEquals("------", stp.of(map).parse("---${key2}---"));

        stp.setReplaceMissingKey(false);
        assertEquals("---${key2}---", stp.of(map).parse("---${key2}---"));

        stp.setReplaceMissingKey(true);
        stp.setMissingKeyReplacement("");

        assertEquals("------", stp.of(map).parse("---${key2}---"));

        stp.setMissingKeyReplacement("<>");
        assertEquals("---<>---", stp.of(map).parse("---${key2}---"));
    }

    @Test
    public void testInner() {
        MapTemplateParser stp = new MapTemplateParser();

        HashMap<String, Object> map = new HashMap<>();
        map.put("key0", "1");
        map.put("key1", "2");
        map.put("key2", "value");

        assertEquals("---value---", stp.of(map).parse("---${key${key1}}---"));

        assertEquals("---value---", stp.of(map).parse("---${key${key${key0}}}---"));
    }

    @Test
    public void testInner2() {
        MapTemplateParser stp = new MapTemplateParser();

        HashMap<String, Object> map = new HashMap<>();
        map.put("foo", "foo");
        map.put("boo.foo", "*${foo}*");
        map.put("zoo", "${boo.${foo}}");

        assertEquals("-*${foo}*-", stp.of(map).parse("-${boo.${foo}}-"));
        assertEquals("-${boo.${foo}}-", stp.of(map).parse("-${zoo}-"));

        stp.setParseValues(true);
        assertEquals("-*foo*-", stp.of(map).parse("-${boo.${foo}}-"));
        assertEquals("-*foo*-", stp.of(map).parse("-${zoo}-"));

    }

    @Test
    public void testResolver() {
        StringTemplateParser stp = new StringTemplateParser();
        assertEquals("xxxSMALLxxx", stp.parse("xxx${small}xxx", String::toUpperCase));
    }

    @Test
    public void testReplaceMissingKey() {
        MapTemplateParser stp = new MapTemplateParser();

        HashMap<String, Object> map = new HashMap<>();
        map.put("key0", "1");
        map.put("key1", "2");

        assertEquals(".1.", stp.of(map).parse(".${key0}."));

        assertEquals("..", stp.of(map).parse(".${key2}."));

        stp.setMissingKeyReplacement("x");
        assertEquals(".x.", stp.of(map).parse(".${key2}."));

        stp.setReplaceMissingKey(false);
        assertEquals(".${key2}.", stp.of(map).parse(".${key2}."));

        stp.setMissingKeyReplacement(null);
        assertEquals(".${key2}.", stp.of(map).parse(".${key2}."));
    }

    @Test
    public void testResolveEscapes() {
        MapTemplateParser stp = new MapTemplateParser();

        HashMap<String, Object> map = new HashMap<>();
        map.put("fooProp", "abean_value");

        stp.setResolveEscapes(false);

        assertEquals("...abean_value...", stp.of(map).parse("...${fooProp}..."));

        assertEquals("...\\${fooProp}...", stp.of(map).parse("...\\${fooProp}..."));
        assertEquals("...\\\\abean_value...", stp.of(map).parse("...\\\\${fooProp}..."));
        assertEquals("...\\\\\\${fooProp}...", stp.of(map).parse("...\\\\\\${fooProp}..."));
        assertEquals("...\\\\\\\\abean_value...", stp.of(map).parse("...\\\\\\\\${fooProp}..."));
        assertEquals("...\\\\\\\\\\${fooProp}...", stp.of(map).parse("...\\\\\\\\\\${fooProp}..."));
    }

    @Test
    public void testCustomMacrosEnds() {
        MapTemplateParser stp = new MapTemplateParser();

        HashMap<String, Object> map = new HashMap<>();
        map.put("foo", "bar");
        map.put("bar", "zap");

        assertEquals("...bar...<%=foo%>...", stp.of(map).parse("...${foo}...<%=foo%>..."));

        stp.setMacroStart("<%=");
        stp.setMacroEnd("%>");
        stp.setMacroPrefix(null);

        assertEquals("...${foo}...bar...", stp.of(map).parse("...${foo}...<%=foo%>..."));

        assertEquals("z<%=foo%>z", stp.of(map).parse("z\\<%=foo%>z"));
        assertEquals("xzapx", stp.of(map).parse("x<%=<%=foo%>%>x"));
    }

    @Test
    public void testNonScript() {
        HashMap<String, Object> map = new HashMap<>();
        map.put("foo", "bar");
        map.put("bar", "zap");
        map.put("inner.man", "jo");

        MapTemplateParser stp = new MapTemplateParser();

        assertEquals("...bar...", stp.of(map).parse("...$foo..."));
        assertEquals("xx bar xx", stp.of(map).parse("xx $foo xx"));
        assertEquals("bar", stp.of(map).parse("$foo"));

        assertEquals("jo", stp.of(map).parse("$inner.man"));
        assertEquals("jo.", stp.of(map).parse("$inner.man."));
        assertEquals("jo x", stp.of(map).parse("$inner.man x"));
        assertEquals("jo bar", stp.of(map).parse("$inner.man ${foo}"));

        stp.setStrictFormat();
        assertEquals("$inner.man bar", stp.of(map).parse("$inner.man ${foo}"));
    }

    @Test
    public void test601_IndexOutOfBounds() {
        StringTemplateParser stp = new StringTemplateParser();
        stp.setReplaceMissingKey(false);

        assertEquals("$foo", stp.parse("$foo", null));
        assertEquals("$foo bar", stp.parse("$foo bar", null));
        assertEquals("foo $bar", stp.parse("foo $bar", null));
        assertEquals("$foo", stp.parse("$foo", (s) -> {throw new RuntimeException();}));
    }

    @Test
    public void test601_DuplicatedChar() {
        StringTemplateParser stp = new StringTemplateParser();
        stp.setReplaceMissingKey(false);

        assertEquals("bar$foo baz", stp.parse("bar$foo baz", null));
        assertEquals("bar$foo baz", stp.parse("bar$foo baz", (s) -> {throw new RuntimeException();}));
    }


}

package jexx.util;

import org.junit.Assert;
import org.junit.Test;

import java.io.Serializable;

public class ObjectUtilTest {

    @Test
    public void testNotNullOrDefault(){
        Assert.assertEquals("123", ObjectUtil.notNullOrDefault("123", null));
        Assert.assertEquals("123", ObjectUtil.notNullOrDefault("123", "234"));
        Assert.assertEquals("123", ObjectUtil.notNullOrDefault(null, "123"));
    }

    @Test
    public void testSerialize(){
        ObjectUtilTestBean bean1 = new ObjectUtilTestBean();
        bean1.setName("aaa");
        bean1.setAge(111);

        byte[] bytes1 = ObjectUtil.serialize(bean1);
        Object obj1 = ObjectUtil.deserialize(bytes1);
        Assert.assertTrue(obj1 instanceof ObjectUtilTestBean);
        ObjectUtilTestBean bean2 = (ObjectUtilTestBean)obj1;
        Assert.assertEquals(bean1.getName(), bean2.getName());
        Assert.assertEquals(bean1.getAge(), bean2.getAge());
    }


    public static class ObjectUtilTestBean implements Serializable {
        private static final long serialVersionUID = -7809533204951422243L;
        private String name;
        private Integer age;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Integer getAge() {
            return age;
        }

        public void setAge(Integer age) {
            this.age = age;
        }
    }

}

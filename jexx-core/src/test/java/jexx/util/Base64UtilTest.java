package jexx.util;

import org.junit.Assert;
import org.junit.Test;

public class Base64UtilTest {

    @Test
    public void test1(){
        String str = "Hello,世界";
        String encode = Base64Util.encodeAsStr(str);
        Assert.assertEquals("SGVsbG8s5LiW55WM", encode);
        Assert.assertArrayEquals(StringUtil.getBytes(str), Base64Util.decode(StringUtil.getBytes(encode)));
    }


    @Test
    public void test2(){
        String str = "dbf7060d047e7725b19f80dbf8a3bd0a";
        byte[] bb = Base64Util.decode(str);
        Console.log(Base64Util.encodeAsStr(bb));
    }

}

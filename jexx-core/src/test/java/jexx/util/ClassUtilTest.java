package jexx.util;

import org.junit.Assert;
import org.junit.Test;

import java.io.Serializable;
import java.lang.reflect.*;
import java.util.*;

/**
 * @author jeff
 * @since 2019/6/21
 */
public class ClassUtilTest {

    @Test
    public void testForName() throws ClassNotFoundException {
        Assert.assertEquals(byte.class, ClassUtil.forName("byte"));
        Assert.assertEquals(char.class, ClassUtil.forName("char"));
        Assert.assertEquals(short.class, ClassUtil.forName("short"));
        Assert.assertEquals(int.class, ClassUtil.forName("int"));
        Assert.assertEquals(long.class, ClassUtil.forName("long"));
        try {
            ClassUtil.forName("a.b.c");
            Assert.fail();
        }
        catch (ClassNotFoundException e){
            //skip
        }
    }

    @Test
    public void testParameterizedType(){
        class A<K,V>{}
        Type type = A.class.getGenericSuperclass();
        Assert.assertEquals(Object.class, type);

        class B extends A<String, Integer>{}
        type = B.class.getGenericSuperclass();
        Assert.assertTrue(type instanceof ParameterizedType);
        Type[] types = ((ParameterizedType)type).getActualTypeArguments();
        Assert.assertEquals(2, types.length);
        Assert.assertEquals(String.class, types[0]);
        Assert.assertEquals(Integer.class, types[1]);

        types = MyInterface1.class.getGenericInterfaces();
        Assert.assertEquals(0, types.length);

        types = MyInterface2.class.getGenericInterfaces();
        Assert.assertEquals(0, types.length);

        class C implements MyInterface1{}
        types = C.class.getGenericInterfaces();
        Assert.assertEquals(1, types.length);
        Assert.assertEquals(MyInterface1.class, types[0]);

        class D implements MyInterface2<String, Integer>{}
        types = D.class.getGenericInterfaces();
        Assert.assertEquals(1, types.length);
        Assert.assertTrue(types[0] instanceof ParameterizedType);
        types = ((ParameterizedType)type).getActualTypeArguments();
        Assert.assertEquals(2, types.length);
        Assert.assertEquals(String.class, types[0]);
        Assert.assertEquals(Integer.class, types[1]);

        class E implements MyInterface3<String, Integer>{}
        types = E.class.getGenericInterfaces();
        Assert.assertEquals(1, types.length);
        Assert.assertTrue(types[0] instanceof ParameterizedType);
        types = ((ParameterizedType)type).getActualTypeArguments();
        Assert.assertEquals(2, types.length);
        Assert.assertEquals(String.class, types[0]);
        Assert.assertEquals(Integer.class, types[1]);

        class F {
            public List<String> list = new ArrayList<>();
        }
        type = new F().list.getClass().getGenericSuperclass();
        Assert.assertTrue(type instanceof ParameterizedType);
        //getGenericSuperclass 获取的是超类的参数化类型，为 AbstractList<E>
        types = ((ParameterizedType)type).getActualTypeArguments();
        Assert.assertEquals(1, types.length);

        Field[] fields = F.class.getFields();
        Assert.assertEquals(1, fields.length);
        type = fields[0].getGenericType();
        Assert.assertTrue(type instanceof ParameterizedType);
        types = ((ParameterizedType)type).getActualTypeArguments();
        Assert.assertEquals(1, types.length);
        Assert.assertEquals(String.class, types[0]);
    }

    @Test
    public void testTypeVariable(){
        class A<T extends CharSequence>{}
        TypeVariable<Class<A>>[] typeVariables = A.class.getTypeParameters();
        Assert.assertEquals(1, typeVariables.length);
        TypeVariable<Class<A>> typeVariable = typeVariables[0];
        Type[] types = typeVariable.getBounds();
        Assert.assertEquals(1, types.length);
        Type type = types[0];
        Assert.assertEquals(CharSequence.class, type);

        class B<T extends Map<String, Date> & Cloneable & Serializable>{}
        TypeVariable<Class<B>>[] typeVariables1 = B.class.getTypeParameters();
        Assert.assertEquals(1, typeVariables1.length);
        TypeVariable<Class<B>> typeVariable1 = typeVariables1[0];
        Assert.assertEquals("T", typeVariable1.getName());
        types = typeVariable1.getBounds();
        Assert.assertEquals(3, types.length);
        type = types[0];
        Assert.assertTrue(type instanceof ParameterizedType);
        Type[] actualTypeArguments = ((ParameterizedType)type).getActualTypeArguments();
        Assert.assertEquals(2, actualTypeArguments.length);
        Assert.assertEquals(String.class, actualTypeArguments[0]);
        Assert.assertEquals(Date.class, actualTypeArguments[1]);
        type = types[1];
        Assert.assertEquals(Cloneable.class,  type);
        type = types[2];
        Assert.assertEquals(Serializable.class,  type);
    }

    @Test
    public void testGenericArrayType(){
        class A{
            public String[] p1;
            public List<String>[] p3;
            public String[][] p4;
        }
        Field[] fields = A.class.getFields();
        Assert.assertEquals(3, fields.length);
        Field field = fields[0];
        Type type = field.getGenericType();
        Assert.assertEquals(String[].class, type);

        field = fields[1];
        type = field.getGenericType();
        Assert.assertTrue(type instanceof GenericArrayType);
        type = ((GenericArrayType)type).getGenericComponentType();
        Assert.assertTrue(type instanceof ParameterizedType);
        Type[] actualTypeArguments = ((ParameterizedType)type).getActualTypeArguments();
        Assert.assertEquals(1, actualTypeArguments.length);
        Assert.assertEquals(String.class, actualTypeArguments[0]);

        field = fields[2];
        type = field.getGenericType();
        Assert.assertEquals(String[][].class, type);
    }

    @Test
    public void testWildcardType() throws NoSuchFieldException {
        class A{
            public List<? extends Number> f1;
            public List<? super String> f2;
        }
        Field field = A.class.getDeclaredField("f1");
        Type type = field.getGenericType();
        Assert.assertTrue(type instanceof ParameterizedType);
        Type[] types = ((ParameterizedType)type).getActualTypeArguments();
        Assert.assertEquals(1, types.length);
        Assert.assertTrue(types[0] instanceof WildcardType);
        WildcardType wildcardType = (WildcardType)types[0];
        Assert.assertEquals(0, wildcardType.getLowerBounds().length);
        Assert.assertEquals(1, wildcardType.getUpperBounds().length);
        Assert.assertEquals(Number.class, wildcardType.getUpperBounds()[0]);


        field = A.class.getDeclaredField("f2");
        type = field.getGenericType();
        Assert.assertTrue(type instanceof ParameterizedType);
        types = ((ParameterizedType)type).getActualTypeArguments();
        Assert.assertEquals(1, types.length);
        Assert.assertTrue(types[0] instanceof WildcardType);
        wildcardType = (WildcardType)types[0];
        //上限默认Object
        Assert.assertEquals(1, wildcardType.getUpperBounds().length);
        Assert.assertEquals(1, wildcardType.getLowerBounds().length);
        Assert.assertEquals(String.class, wildcardType.getLowerBounds()[0]);
    }

    /**
     * 泛型擦除验证
     */
    @Test
    public void testClear(){
        List<Integer> a = new ArrayList<>();
        List<String> b = new ArrayList<>();
        Assert.assertEquals(a.getClass(), b.getClass());

        List<Integer> list = new ArrayList<>();
        list.add(12);
        try {
            Method method = list.getClass().getDeclaredMethod("add",Object.class);
            method.invoke(list,"test");
            method.invoke(list,12.3f);
        } catch (Exception e) {
            e.printStackTrace();
        }
        for (Object o: list){
            System.out.println(o);
        }
    }

    @Test
    public void testCreateBean() throws Exception {
        Constructor<?> ctor = TestBean.class.getDeclaredConstructor();
        ctor.setAccessible(true);
        Object testBean = ctor.newInstance();

        Field[] fields = TestBean.class.getDeclaredFields();
        for(Field field : fields){
            Type type = field.getGenericType();
            Class<?> clazzType = field.getType();
            if(type == String.class){
                field.setAccessible(true);
                field.set(testBean, "test");
            }
            else if(type instanceof ParameterizedType){
                ParameterizedType parameterizedType = (ParameterizedType)type;
                Type[] types = parameterizedType.getActualTypeArguments();
                if(clazzType == List.class && types[0] == Integer.class){
                    List<Integer> list = new ArrayList<>();
                    list.add(111);
                    field.setAccessible(true);
                    field.set(testBean, list);
                }
            }
        }

        TestBean bean = (TestBean)testBean;
        Console.log(bean.getA());
        Console.log(StringUtil.join(bean.getB(), ","));
    }


    private interface MyInterface1{}
    private interface MyInterface2<K,V>{}
    private interface MyInterface3<A,B> extends MyInterface2<Date,Long>{}

    private static class TestBean {
        private String a;
        private List<Integer> b;

        public String getA() {
            return a;
        }

        public void setA(String a) {
            this.a = a;
        }

        public List<Integer> getB() {
            return b;
        }

        public void setB(List<Integer> b) {
            this.b = b;
        }
    }




}

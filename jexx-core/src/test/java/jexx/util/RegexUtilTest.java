package jexx.util;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author jeff
 * @since 2019/10/14
 */
public class RegexUtilTest {

    @Test
    public void testBase(){
        Assert.assertTrue(RegexUtil.isWord("aAbB12_"));
        Assert.assertTrue(RegexUtil.isLetter("aAbB"));
        Assert.assertTrue(RegexUtil.isChinese("你好一龥"));
        Assert.assertEquals(0x4E00, '一');
        Assert.assertEquals(0x9FA5, '龥');
        //email
        Assert.assertTrue(RegexUtil.isEmail("asdf@163.com"));
        Assert.assertTrue(RegexUtil.isEmail("asdfAbc@163.com"));
        //ip
        Assert.assertTrue(RegexUtil.isIp("192.168.1.112"));
        Assert.assertTrue(RegexUtil.isIp("255.255.255.255"));
        //中国大陆手机号码
        Assert.assertTrue(RegexUtil.isMobilePhoneInChinese("13862427714"));
        Assert.assertTrue(RegexUtil.isMobilePhoneInChinese("19977406486"));
        Assert.assertTrue(RegexUtil.isMobilePhoneInChinese("14716034972"));
        Assert.assertTrue(RegexUtil.isMobilePhoneInChinese("17216034972"));
        //中国大陆18位身份证号码
        Assert.assertTrue(RegexUtil.isIdCard18InChinese("450311197509084501"));
    }

}

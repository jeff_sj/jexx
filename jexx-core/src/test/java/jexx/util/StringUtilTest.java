package jexx.util;

import org.junit.Assert;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.*;

public class StringUtilTest {

    @Test
    public void testCamelCaseToUnderline(){
        Assert.assertEquals("hello_world123", StringUtil.camelCaseToUnderline("helloWorld123"));
        Assert.assertEquals("hello_world123", StringUtil.camelCaseToUnderline("HelloWorld123"));
        Assert.assertEquals("Hello_world123", StringUtil.camelCaseToUnderline("HelloWorld123", false));

        Assert.assertEquals("helloWorld123", StringUtil.underlineToCamelCase("hello_world123"));
        Assert.assertEquals("HelloWorld123", StringUtil.underlineToCamelCase("hello_world123", true));

        try {
            StringUtil.camelCaseToUnderline("1helloWorld123");
        }
        catch (Exception e){
            //skip
        }

        try {
            StringUtil.underlineToCamelCase("1hello_world123");
        }
        catch (Exception e){
            //skip
        }
    }

    @Test
    public void testFillAfter(){
        Assert.assertEquals("abc", StringUtil.fillAfter("abc", 'd', 3));
        Assert.assertEquals("abcdd", StringUtil.fillAfter("abc", 'd', 5));
        Assert.assertEquals("abc", StringUtil.fillAfter("abc", 'd', 0));
    }

    @Test
    public void testFormat(){
        ResourceBundle resourceBundle = ResourceBundle.getBundle("i18n/message");
        String str = StringUtil.format(resourceBundle, "{}已经{}岁了", "name", "age");
        Assert.assertEquals("小明已经18岁了", str);
    }

    @Test
    public void testHideMiddlePart(){
        Assert.assertEquals("1", StringUtil.hideMiddlePart("1", '*'));
        Assert.assertEquals("1*", StringUtil.hideMiddlePart("12", '*'));
        Assert.assertEquals("1*3", StringUtil.hideMiddlePart("123", '*'));
        Assert.assertEquals("1**4", StringUtil.hideMiddlePart("1234", '*'));
        Assert.assertEquals("12**5", StringUtil.hideMiddlePart("12345", '*'));
        Assert.assertEquals("12***6", StringUtil.hideMiddlePart("123456", '*'));
        Assert.assertEquals("12***67", StringUtil.hideMiddlePart("1234567", '*'));
        Assert.assertEquals("12****78", StringUtil.hideMiddlePart("12345678", '*'));
        Assert.assertEquals("123****89", StringUtil.hideMiddlePart("123456789", '*'));

        Assert.assertEquals("********", StringUtil.hideMiddlePart("12345678",'*', 9));
        Assert.assertEquals("12345678", StringUtil.hideMiddlePart("12345678", '*', 0));
        Assert.assertEquals("1386****744", StringUtil.hideMiddlePart("13862427744",'*', 4, false));
        Assert.assertEquals("138****7744", StringUtil.hideMiddlePart("13862427744",'*', 4, true));
    }

    @Test
    public void testIsAllBlank(){
        Assert.assertTrue(StringUtil.isAllBlank("", "  ", null));
    }

    @Test
    public void testIsAllEmpty(){
        Assert.assertFalse(StringUtil.isAllEmpty("", "  ", null));
    }

    @Test
    public void testIsAlpha(){
        Assert.assertFalse(StringUtil.isAlpha(null));
        Assert.assertTrue(StringUtil.isAlpha(""));
        Assert.assertFalse(StringUtil.isAlpha("  "));
        Assert.assertTrue(StringUtil.isAlpha("abc"));
        Assert.assertFalse(StringUtil.isAlpha("ab2c"));
        Assert.assertFalse(StringUtil.isAlpha("ab-c"));
    }

    @Test
    public void testIsWhitespace(){
        Assert.assertFalse(StringUtil.isWhitespace(null));
        Assert.assertTrue(StringUtil.isWhitespace(""));
        Assert.assertTrue(StringUtil.isWhitespace("  "));
        Assert.assertFalse(StringUtil.isWhitespace("  abc"));
    }

    @Test
    public void testJoin(){
        List<Integer> list1 = new ArrayList<>();
        list1.add(1);
        list1.add(2);
        list1.add(3);

        String expect = "\"1\",\"2\",\"3\"";
        String actual = StringUtil.joinDirectly("\"1\"", ",", "\"2\"", ",", "\"3\"");
        assertEquals(expect, actual);
        actual = StringUtil.join(list1, ",", "\"", "\"");
        assertEquals(expect, actual);

        actual = StringUtil.join(list1, ",");
        assertEquals("1,2,3", actual);

        List<List<Integer>> list2 = new ArrayList<>();
        List<Integer> list21 = new ArrayList<>();
        list21.add(1);
        list21.add(2);
        list21.add(3);
        list2.add(list21);

        List<Integer> list22 = new ArrayList<>();
        list22.add(4);
        list22.add(5);
        list22.add(6);
        list2.add(list22);
        assertEquals("[1, 2, 3],[4, 5, 6]", StringUtil.join(list2, ","));

        List<Test11> list = new ArrayList<>();
        list.add(new Test11(1,"A"));
        list.add(new Test11(2,"B"));
        list.add(new Test11(3,"C"));
        assertEquals("1,2,3", StringUtil.join(list, ",", Test11::getId));

        String[] array1 = {"1", "2", "3"};
        assertEquals("1,2,3", StringUtil.join(array1, ","));

        Map<Integer, Test11> map = new TreeMap<>();
        map.put(1, new Test11(1,"A"));
        map.put(2, new Test11(2,"B"));
        map.put(3, new Test11(3,"C"));
        map.put(4, null);
        assertEquals("1=A,2=B,3=C", StringUtil.join(map, ",", (k,v)->v!=null?k.toString().concat("=").concat(v.getName()):null));

        assertEquals("1,null,3", StringUtil.join(CollectionUtil.list("1", null, "3"), ",", null));
        assertEquals("1,3", StringUtil.join(CollectionUtil.list("1", null, "3"), ","));
    }

    @Test
    public void testJoinByPath(){
        String expectPath = ",a,b,c,";

        assertEquals(expectPath, StringUtil.joinByPath(CollectionUtil.list("a", "b", "c"), ","));
        assertEquals(",a,,c,", StringUtil.joinByPath(CollectionUtil.list("a", null, "c"), ","));
        assertEquals(",a,b,,", StringUtil.joinByPath(CollectionUtil.list("a", "b", null), ","));
        assertEquals(StringPool.EMPTY, StringUtil.joinByPath(CollectionUtil.list(), ","));
        assertEquals(expectPath, StringUtil.joinByPath(new String[]{"a", "b", "c"}, ","));
        assertEquals(",a,,c,", StringUtil.joinByPath(new String[]{"a", null, "c"}, ","));
        assertEquals(",a,b,,", StringUtil.joinByPath(new String[]{"a", "b", null}, ","));
        assertEquals(StringPool.EMPTY, StringUtil.joinByPath(new String[]{}, ","));

        try {
            StringUtil.splitPathToList("a,b,c", ",", true);
            fail();
        }
        catch (Exception e){
            //skip
        }

        try {
            StringUtil.splitPath(",a,b,c", ",", true);
            fail();
        }
        catch (Exception e){
            //skip
        }

        List<String> list = StringUtil.splitPathToList(expectPath, ",");
        assertEquals(3, list.size());
        assertEquals("a", list.get(0));
        assertEquals("b", list.get(1));
        assertEquals("c", list.get(2));

        String[] array = StringUtil.splitPath(expectPath, ",");
        assertEquals(3, array.length);
        assertEquals("a", array[0]);
        assertEquals("b", array[1]);
        assertEquals("c", array[2]);

    }

    @Test
    public void testIndexOf(){
        assertEquals(4, StringUtil.indexOf("abcdbcf", "bc", 2, 6));
    }

    @Test
    public void testIsSubstringAt(){
        assertTrue("", StringUtil.isSubstringAt("abcdef", "bc", 1));
    }

    @Test
    public void testIsNumeric(){
        String str = "18791221120";
        Assert.assertTrue(StringUtil.isNumeric(str));
        str = "test@163.com";
        Assert.assertFalse(StringUtil.isNumeric(str));
        str = "sc00001";
        Assert.assertFalse(StringUtil.isNumeric(str));
    }

    @Test
    public void testRemoveStart(){
        Assert.assertNull(StringUtil.removeStart(null, "*"));
        Assert.assertEquals("", StringUtil.removeStart("", "*"));
        Assert.assertEquals("*", StringUtil.removeStart("*", null));
        Assert.assertEquals("domain.com", StringUtil.removeStart("www.domain.com", "www."));
        Assert.assertEquals("www.domain.com", StringUtil.removeStart("www.domain.com", "domain"));
        Assert.assertEquals("abc", StringUtil.removeStart("abc", ""));
    }

    @Test
    public void testRemoveSuffix(){
        Assert.assertNull(StringUtil.removeSuffix(null, "*"));
        Assert.assertEquals("", StringUtil.removeSuffix("", "*"));
        Assert.assertEquals("*", StringUtil.removeSuffix("*", null));
        Assert.assertEquals("www.domain", StringUtil.removeSuffix("www.domain.com", ".com"));
        Assert.assertEquals("www.domain.com", StringUtil.removeSuffix("www.domain.com", "domain"));
        Assert.assertEquals("abc", StringUtil.removeSuffix("abc", ""));
    }

    @Test
    public void testRemoveWhitespace(){
        Assert.assertEquals("", StringUtil.removeWhitespace(""));
        Assert.assertEquals("", StringUtil.removeWhitespace(" "));
        Assert.assertEquals("abc", StringUtil.removeWhitespace(" a b c "));
        Assert.assertEquals("abc", StringUtil.removeWhitespace("a\tb\t\tc\t"));
        Assert.assertEquals("", StringUtil.removeWhitespace("\r\n\t\f"));
        Assert.assertEquals("你好世界", StringUtil.removeWhitespace(" 你好 \r\n\t\f 世界 "));
    }

    @Test
    public void testSplit(){
        String[] array = StringUtil.split("boo:and:foo", ':', 2);
        Assert.assertArrayEquals(new String[]{"boo", "and:foo"}, array);

        array = StringUtil.split("boo:and:foo", ':');
        Assert.assertArrayEquals(new String[]{"boo", "and", "foo"}, array);

        array = StringUtil.split("boo:and:foo", 'o');
        Assert.assertArrayEquals(new String[]{"b", "", ":and:f", "", ""}, array);

        array = StringUtil.split("boo:and:foo", 'o', 2);
        Assert.assertArrayEquals(new String[]{"b", "o:and:foo"}, array);

        array = StringUtil.split("boo:and:foo", ":", 2);
        Assert.assertArrayEquals(new String[]{"boo", "and:foo"}, array);

        array = StringUtil.split("boo:and:foo", ":");
        Assert.assertArrayEquals(new String[]{"boo", "and", "foo"}, array);

        array = StringUtil.split("boo:and:foo", "o");
        Assert.assertArrayEquals(new String[]{"b", "", ":and:f", "", ""}, array);

        array = StringUtil.split("boo:and:foo", "o", 2);
        Assert.assertArrayEquals(new String[]{"b", "o:and:foo"}, array);

        array = StringUtil.split("boo:and:foo", "oo");
        Assert.assertArrayEquals(new String[]{"b", ":and:f", ""}, array);

        List<Long> arr1 = StringUtil.splitToList("1,2,3,4", ",", Long::parseLong);
        Assert.assertEquals(Long.valueOf(1), arr1.get(0));
        Assert.assertEquals(Long.valueOf(2), arr1.get(1));
        Assert.assertEquals(Long.valueOf(3), arr1.get(2));
        Assert.assertEquals(Long.valueOf(4), arr1.get(3));
    }

    @Test
    public void testSubstitute1(){
        assertEquals("...11...", StringUtil.substitute("...{}...", 11));
        assertEquals("...\\{}...", StringUtil.substitute("...\\{}...", 11));
        assertEquals("...{11...", StringUtil.substitute("...{{}...", 11));
        assertEquals("...11...22...", StringUtil.substitute("...{}...{}...", 11, 22));
        assertEquals("...11...{}...", StringUtil.substitute("...{}...{}...", 11));
    }

    @Test
    public void testSubstitute2(){
        Map<String,Object> map = new HashMap<>();
        map.put("fooProp", "XXX");

        assertEquals("...XXX...", StringUtil.substitute("...${fooProp}...", map));
        assertEquals("...${fooProp11}...", StringUtil.substitute("...${fooProp11}...", map));
        assertEquals("...\\${fooProp}...", StringUtil.substitute("...\\${fooProp}...", map));
    }


    @Test
    public void testStartWith(){
        assertTrue(StringUtil.startWith("abcDefg123456", "bcd", 1, true));
        assertTrue(StringUtil.startWith("abcDefg123456", "bcD", 1));
        assertTrue(StringUtil.startWith("abcDefg123456", "abcD"));
        assertTrue(StringUtil.startWithChar("abcDefg123456", 'a'));
    }

    @Test
    public void testEndWith(){
        assertTrue(StringUtil.endWith("gfeDcba", "dcba", true));
        assertTrue(StringUtil.endWith("gfeDcba", "Dcba"));
        assertTrue(StringUtil.endWithChar("gfeDcba", 'a'));
        assertTrue(StringUtil.endWith("gfeDcba", "Dcba", "ddxx"));
        assertTrue(StringUtil.endWithIgnoreCase("gfeDcba", "dcba", "ddxx"));
    }

    @Test
    public void testReplace(){
        assertEquals("Abcdef", StringUtil.replace("abcdef", "a", "A"));
        assertEquals("abcdEF", StringUtil.replace("abcdef", "ef", "EF"));
    }


    private static class Test11{
        private Integer id;
        private String name;
        public Test11(Integer id, String name) {
            this.id = id;
            this.name = name;
        }
        public Integer getId() {
            return id;
        }
        public void setId(Integer id) {
            this.id = id;
        }
        public String getName() {
            return name;
        }
        public void setName(String name) {
            this.name = name;
        }
    }



}

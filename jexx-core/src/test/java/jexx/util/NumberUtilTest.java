package jexx.util;

import jexx.exception.UtilException;
import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

public class NumberUtilTest {

    @Test
    public void testCompare(){
        Assert.assertTrue(NumberUtil.isGreater(new BigDecimal("100.00"), BigDecimal.ZERO));
        Assert.assertTrue(NumberUtil.isGreaterOrEqual(new BigDecimal("100.00"), BigDecimal.ZERO));
    }

    /**
     * decimal测试
     */
    @Test
    public void testDecimal(){
        Assert.assertEquals("0.1", Float.toString(0.1f));
        Assert.assertEquals("0.10000000149011612", Double.toString(0.1f));
        Assert.assertEquals("0.10000000149011612", BigDecimal.valueOf(0.1f).toString());
        Assert.assertEquals("0.1",  new BigDecimal("0.1").toString());

        Assert.assertEquals("0.1", Double.toString(0.1d));
        Assert.assertEquals("0.1", BigDecimal.valueOf(0.1d).toString());
        Assert.assertEquals("0.1",  new BigDecimal("0.1").toString());

        Assert.assertEquals(new BigDecimal(0.1f), new BigDecimal(0.1f));
        Assert.assertEquals(new BigDecimal(0.1d), new BigDecimal(0.1d));
        Assert.assertEquals(new BigDecimal("0.1"), new BigDecimal("0.1"));
        Assert.assertEquals(new BigDecimal("0.1"), BigDecimal.valueOf(0.1d));
        Assert.assertEquals(new BigDecimal("1"), new BigDecimal(1));
        Assert.assertEquals(new BigDecimal("1"), new BigDecimal("1"));
        Assert.assertEquals(new BigDecimal("0.01"), new BigDecimal("0.01"));


        Assert.assertNotEquals(new BigDecimal("0.1"), BigDecimal.valueOf(0.1f));
        Assert.assertNotEquals(new BigDecimal("0.1"), new BigDecimal(0.1d));
        Assert.assertNotEquals(new BigDecimal("1.1"), new BigDecimal(1.1d));
        Assert.assertNotEquals(new BigDecimal("18.8"), new BigDecimal(18.8));
    }

    @Test
    public void testRound(){
        Assert.assertEquals(new BigDecimal("1.90"), NumberUtil.round(new BigDecimal("1.9"), 2, RoundingMode.UP));
        Assert.assertEquals(new BigDecimal("2"), NumberUtil.round(new BigDecimal("1.6"), 0, RoundingMode.UP));
        Assert.assertEquals(new BigDecimal("2"), NumberUtil.round(new BigDecimal("1.5"), 0, RoundingMode.UP));
        Assert.assertEquals(new BigDecimal("2"), NumberUtil.round(new BigDecimal("1.1"), 0, RoundingMode.UP));
        Assert.assertEquals(new BigDecimal("1"), NumberUtil.round(new BigDecimal("1.0"), 0, RoundingMode.UP));
        Assert.assertEquals(new BigDecimal("-1"), NumberUtil.round(new BigDecimal("-1.0"), 0, RoundingMode.UP));
        Assert.assertEquals(new BigDecimal("-2"), NumberUtil.round(new BigDecimal("-1.1"), 0, RoundingMode.UP));
        Assert.assertEquals(new BigDecimal("-2"), NumberUtil.round(new BigDecimal("-1.5"), 0, RoundingMode.UP));
        Assert.assertEquals(new BigDecimal("-2"), NumberUtil.round(new BigDecimal("-1.6"), 0, RoundingMode.UP));

        Assert.assertEquals(new BigDecimal("1"), NumberUtil.round(new BigDecimal("1.6"), 0, RoundingMode.DOWN));
        Assert.assertEquals(new BigDecimal("1"), NumberUtil.round(new BigDecimal("1.5"), 0, RoundingMode.DOWN));
        Assert.assertEquals(new BigDecimal("1"), NumberUtil.round(new BigDecimal("1.1"), 0, RoundingMode.DOWN));
        Assert.assertEquals(new BigDecimal("1"), NumberUtil.round(new BigDecimal("1.0"), 0, RoundingMode.DOWN));
        Assert.assertEquals(new BigDecimal("-1"), NumberUtil.round(new BigDecimal("-1.0"), 0, RoundingMode.DOWN));
        Assert.assertEquals(new BigDecimal("-1"), NumberUtil.round(new BigDecimal("-1.1"), 0, RoundingMode.DOWN));
        Assert.assertEquals(new BigDecimal("-1"), NumberUtil.round(new BigDecimal("-1.5"), 0, RoundingMode.DOWN));
        Assert.assertEquals(new BigDecimal("-1"), NumberUtil.round(new BigDecimal("-1.6"), 0, RoundingMode.DOWN));

        Assert.assertEquals(new BigDecimal("2"), NumberUtil.round(new BigDecimal("1.6"), 0, RoundingMode.CEILING));
        Assert.assertEquals(new BigDecimal("2"), NumberUtil.round(new BigDecimal("1.5"), 0, RoundingMode.CEILING));
        Assert.assertEquals(new BigDecimal("2"), NumberUtil.round(new BigDecimal("1.1"), 0, RoundingMode.CEILING));
        Assert.assertEquals(new BigDecimal("1"), NumberUtil.round(new BigDecimal("1.0"), 0, RoundingMode.CEILING));
        Assert.assertEquals(new BigDecimal("-1"), NumberUtil.round(new BigDecimal("-1.0"), 0, RoundingMode.CEILING));
        Assert.assertEquals(new BigDecimal("-1"), NumberUtil.round(new BigDecimal("-1.1"), 0, RoundingMode.CEILING));
        Assert.assertEquals(new BigDecimal("-1"), NumberUtil.round(new BigDecimal("-1.5"), 0, RoundingMode.CEILING));
        Assert.assertEquals(new BigDecimal("-1"), NumberUtil.round(new BigDecimal("-1.6"), 0, RoundingMode.CEILING));

        Assert.assertEquals(new BigDecimal("1"), NumberUtil.round(new BigDecimal("1.6"), 0, RoundingMode.FLOOR));
        Assert.assertEquals(new BigDecimal("1"), NumberUtil.round(new BigDecimal("1.5"), 0, RoundingMode.FLOOR));
        Assert.assertEquals(new BigDecimal("1"), NumberUtil.round(new BigDecimal("1.1"), 0, RoundingMode.FLOOR));
        Assert.assertEquals(new BigDecimal("1"), NumberUtil.round(new BigDecimal("1.0"), 0, RoundingMode.FLOOR));
        Assert.assertEquals(new BigDecimal("-1"), NumberUtil.round(new BigDecimal("-1.0"), 0, RoundingMode.FLOOR));
        Assert.assertEquals(new BigDecimal("-2"), NumberUtil.round(new BigDecimal("-1.1"), 0, RoundingMode.FLOOR));
        Assert.assertEquals(new BigDecimal("-2"), NumberUtil.round(new BigDecimal("-1.5"), 0, RoundingMode.FLOOR));
        Assert.assertEquals(new BigDecimal("-2"), NumberUtil.round(new BigDecimal("-1.6"), 0, RoundingMode.FLOOR));

        Assert.assertEquals(new BigDecimal("2"), NumberUtil.round(new BigDecimal("1.6"), 0, RoundingMode.HALF_UP));
        Assert.assertEquals(new BigDecimal("2"), NumberUtil.round(new BigDecimal("1.5"), 0, RoundingMode.HALF_UP));
        Assert.assertEquals(new BigDecimal("1"), NumberUtil.round(new BigDecimal("1.1"), 0, RoundingMode.HALF_UP));
        Assert.assertEquals(new BigDecimal("1"), NumberUtil.round(new BigDecimal("1.0"), 0, RoundingMode.HALF_UP));
        Assert.assertEquals(new BigDecimal("-1"), NumberUtil.round(new BigDecimal("-1.0"), 0, RoundingMode.HALF_UP));
        Assert.assertEquals(new BigDecimal("-1"), NumberUtil.round(new BigDecimal("-1.1"), 0, RoundingMode.HALF_UP));
        Assert.assertEquals(new BigDecimal("-2"), NumberUtil.round(new BigDecimal("-1.5"), 0, RoundingMode.HALF_UP));
        Assert.assertEquals(new BigDecimal("-2"), NumberUtil.round(new BigDecimal("-1.6"), 0, RoundingMode.HALF_UP));

        Assert.assertEquals(new BigDecimal("2"), NumberUtil.round(new BigDecimal("1.6"), 0, RoundingMode.HALF_DOWN));
        Assert.assertEquals(new BigDecimal("1"), NumberUtil.round(new BigDecimal("1.5"), 0, RoundingMode.HALF_DOWN));
        Assert.assertEquals(new BigDecimal("1"), NumberUtil.round(new BigDecimal("1.1"), 0, RoundingMode.HALF_DOWN));
        Assert.assertEquals(new BigDecimal("1"), NumberUtil.round(new BigDecimal("1.0"), 0, RoundingMode.HALF_DOWN));
        Assert.assertEquals(new BigDecimal("-1"), NumberUtil.round(new BigDecimal("-1.0"), 0, RoundingMode.HALF_DOWN));
        Assert.assertEquals(new BigDecimal("-1"), NumberUtil.round(new BigDecimal("-1.1"), 0, RoundingMode.HALF_DOWN));
        Assert.assertEquals(new BigDecimal("-1"), NumberUtil.round(new BigDecimal("-1.5"), 0, RoundingMode.HALF_DOWN));
        Assert.assertEquals(new BigDecimal("-2"), NumberUtil.round(new BigDecimal("-1.6"), 0, RoundingMode.HALF_DOWN));
    }

    @Test
    public void testParseInt(){
        String[] str = new String[]{"1","2","3"};
        int[] numbers = NumberUtil.parseInt(str);
        Assert.assertEquals(1, numbers[0]);
        Assert.assertEquals(3, numbers[2]);
    }

    @Test
    public void testParseLong(){
        String[] str = new String[]{"1","2","3"};
        long[] numbers = NumberUtil.parseLong(str);
        Assert.assertEquals(1, numbers[0]);
        Assert.assertEquals(3, numbers[2]);
    }

    @Test
    public void testSum(){
        List<TestBean> beans = TestBean.createData();
        Assert.assertEquals(BigDecimal.valueOf(55), NumberUtil.sum(beans, TestBean::getA));
    }

    @Test
    public void testDecodeAndEncode(){
        int x = Integer.MAX_VALUE;
        String ss = NumberUtil.encodeInt(x, 6);
        Assert.assertEquals(x, NumberUtil.decodeInt(ss));
        x = 0;
        ss = NumberUtil.encodeInt(x, 6);
        Assert.assertEquals(x, NumberUtil.decodeInt(ss));
        x = 1;
        ss = NumberUtil.encodeInt(x, 6);
        Assert.assertEquals(x, NumberUtil.decodeInt(ss));
        x = 9;
        ss = NumberUtil.encodeInt(x, 6);
        Assert.assertEquals(x, NumberUtil.decodeInt(ss));
        x = 99;
        ss = NumberUtil.encodeInt(x, 6);
        Assert.assertEquals(x, NumberUtil.decodeInt(ss));
        x = 999;
        ss = NumberUtil.encodeInt(x, 6);
        Assert.assertEquals(x, NumberUtil.decodeInt(ss));
        x = 9999;
        ss = NumberUtil.encodeInt(x, 6);
        Assert.assertEquals(x, NumberUtil.decodeInt(ss));
        x = 99999;
        ss = NumberUtil.encodeInt(x, 6);
        Assert.assertEquals(x, NumberUtil.decodeInt(ss));
        x = 999999;
        ss = NumberUtil.encodeInt(x, 6);
        Assert.assertEquals(x, NumberUtil.decodeInt(ss));


        try {
            NumberUtil.encodeInt(1, "aabcde", 6);
            Assert.fail();
        }
        catch (UtilException e){
            //skip
        }

        try {
            NumberUtil.decodeInt("abcde", "aabcde");
            Assert.fail();
        }
        catch (UtilException e){
            //skip
        }

    }

    private static class TestBean{
        private BigDecimal a;

        public static List<TestBean> createData(){
            List<TestBean> beans = new ArrayList<>();
            for(int i = 1; i <= 10; i++){
                TestBean bean = new TestBean();
                bean.a = BigDecimal.valueOf(i);
                beans.add(bean);
            }
            return beans;
        }

        public BigDecimal getA() {
            return a;
        }

        public void setA(BigDecimal a) {
            this.a = a;
        }
    }

}

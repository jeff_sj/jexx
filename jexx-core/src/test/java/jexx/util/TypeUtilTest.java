package jexx.util;

import org.junit.Assert;
import org.junit.Test;

import java.lang.reflect.ParameterizedType;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class TypeUtilTest {


    @Test
    public void testType() throws NoSuchFieldException {
        class TypeInnerBean{
            private String a1;
            private Integer a2;
            private int a3;
        }

        class TypeBean{
            private String a1;
            private Integer a2;
            private int a3;
            private List<TypeInnerBean> b1;
            private List<String> b2;
            private TypeInnerBean[] c1;
            private String[] c2;
            private List<TypeInnerBean>[] c3;
            private int[] c4;
            private Map<String, TypeInnerBean> d1;
            private Map<String, List<TypeInnerBean>> d2;
            private Set<TypeInnerBean> e1;
            private Set<String> e2;
            private Set<List<TypeInnerBean>> e3;
        }

        //测试原始类
        Assert.assertEquals(String.class, TypeUtil.getClass(TypeBean.class.getDeclaredField("a1")));
        Assert.assertEquals(Integer.class, TypeUtil.getClass(TypeBean.class.getDeclaredField("a2")));
        Assert.assertEquals(int.class, TypeUtil.getClass(TypeBean.class.getDeclaredField("a3")));
        Assert.assertEquals(List.class, TypeUtil.getClass(TypeBean.class.getDeclaredField("b1")));
        Assert.assertEquals(List.class, TypeUtil.getClass(TypeBean.class.getDeclaredField("b2")));
        Assert.assertTrue(TypeUtil.isArray(TypeBean.class.getDeclaredField("c1")));
        Assert.assertTrue(TypeUtil.isArray(TypeBean.class.getDeclaredField("c2")));
        Assert.assertTrue(TypeUtil.isArray(TypeBean.class.getDeclaredField("c3")));
        Assert.assertTrue(TypeUtil.isArray(TypeBean.class.getDeclaredField("c4")));
        Assert.assertEquals(Map.class, TypeUtil.getClass(TypeBean.class.getDeclaredField("d1")));
        Assert.assertEquals(Map.class, TypeUtil.getClass(TypeBean.class.getDeclaredField("d2")));
        Assert.assertEquals(Set.class, TypeUtil.getClass(TypeBean.class.getDeclaredField("e1")));
        Assert.assertEquals(Set.class, TypeUtil.getClass(TypeBean.class.getDeclaredField("e2")));
        Assert.assertEquals(Set.class, TypeUtil.getClass(TypeBean.class.getDeclaredField("e3")));

        //测试List泛型参数
        Assert.assertEquals(TypeInnerBean.class, TypeUtil.getClassArgument(TypeBean.class.getDeclaredField("b1")));
        Assert.assertEquals(String.class, TypeUtil.getClassArgument(TypeBean.class.getDeclaredField("b2")));
        Assert.assertNull(TypeUtil.getComponentClass(TypeBean.class.getDeclaredField("b1")));

        //测试数组类型
        Assert.assertEquals(TypeInnerBean.class, TypeUtil.getComponentClass(TypeBean.class.getDeclaredField("c1")));
        Assert.assertEquals(String.class, TypeUtil.getComponentClass(TypeBean.class.getDeclaredField("c2")));
        Assert.assertEquals(List.class, TypeUtil.getComponentClass(TypeBean.class.getDeclaredField("c3")));
        Assert.assertEquals(int.class, TypeUtil.getComponentClass(TypeBean.class.getDeclaredField("c4")));

        //测试map泛型类型
        Assert.assertEquals(String.class, TypeUtil.getClassArgument(TypeBean.class.getDeclaredField("d1")));
        Assert.assertEquals(TypeInnerBean.class, TypeUtil.getClassArgument(TypeBean.class.getDeclaredField("d1"), 1));
        Assert.assertEquals(String.class, TypeUtil.getClassArgument(TypeBean.class.getDeclaredField("d2")));
        Assert.assertTrue(TypeUtil.getTypeArgument(TypeBean.class.getDeclaredField("d2"), 1) instanceof ParameterizedType);
        Assert.assertEquals(List.class, TypeUtil.getClassArgument(TypeBean.class.getDeclaredField("d2"), 1));

        //测试set泛型类型
        Assert.assertEquals(TypeInnerBean.class, TypeUtil.getClassArgument(TypeBean.class.getDeclaredField("e1")));
        Assert.assertEquals(String.class, TypeUtil.getClassArgument(TypeBean.class.getDeclaredField("e2")));
        Assert.assertTrue(TypeUtil.getTypeArgument(TypeBean.class.getDeclaredField("e3")) instanceof ParameterizedType);
        Assert.assertEquals(List.class, TypeUtil.getClassArgument(TypeBean.class.getDeclaredField("e3")));


        abstract class A<T>{
        }
        class B extends A<Boolean>{
        }

        //测试获取上级类的泛型参数类型
        Assert.assertNull(TypeUtil.getSuperClassArgument(B.class.getSuperclass()));
        Assert.assertEquals(Boolean.class, TypeUtil.getSuperClassArgument(B.class));

    }

}

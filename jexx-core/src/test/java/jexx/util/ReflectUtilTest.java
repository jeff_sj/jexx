package jexx.util;

import org.junit.Assert;
import org.junit.Test;

import java.lang.reflect.Method;

/**
 * @author jeff
 * @since 2019/5/14
 */
public class ReflectUtilTest {


    @Test
    public void testNewInstance(){
        ReflectUtilBean bean = ReflectUtil.newInstance(ReflectUtilBean.class);
        Assert.assertNotNull(bean);
        Assert.assertNull(bean.getId());
        Assert.assertNull(bean.getName());

        bean = ReflectUtil.newInstance(ReflectUtilBean.class, 11);
        Assert.assertNotNull(bean);
        Assert.assertEquals(Integer.valueOf(11), bean.getId());
        Assert.assertNull(bean.getName());

        bean = ReflectUtil.newInstance(ReflectUtilBean.class, "sun");
        Assert.assertNotNull(bean);
        Assert.assertEquals("sun", bean.getName());
        Assert.assertNull(bean.getId());

        bean = ReflectUtil.newInstance(ReflectUtilBean.class, 11,"sun");
        Assert.assertNotNull(bean);
        Assert.assertEquals("sun", bean.getName());
        Assert.assertEquals(Integer.valueOf(11), bean.getId());

        bean = ReflectUtil.newInstance(ReflectUtilBean.class, 11,"sun", 18);
        Assert.assertNotNull(bean);
        Assert.assertEquals(18, bean.getAge());
    }

    @Test
    public void testGetMethodByName(){
        Object obj = new ReflectUtilBean();

        Method method1 = ReflectUtil.getMethodByName(obj, "checkGetMethodByName", "a", 1);
        Assert.assertNotNull(method1);

        try {
            ReflectUtil.getMethodByName(obj, "checkGetMethodByName", null, 1);
            Assert.fail();
        }
        catch (Exception e){
            //skip
        }

        Method method3 = ReflectUtil.getMethodByNameSmartly(obj, "checkGetMethodByName", null, 1);
        Assert.assertNotNull(method3);
        Assert.assertEquals("checkFindMethod1", ReflectUtil.invoke(obj, method3, null, 1));

        try {
            ReflectUtil.getMethodByNameSmartly(obj, "checkGetMethodByName", null, null);
            Assert.fail();
        }
        catch (Exception e){
            Console.log(e);
        }

        Method method4 = ReflectUtil.getMethodByNameSmartly(obj, "checkGetMethodByName", 1, "1");
        Assert.assertNull(method4);
    }

    private static class ReflectUtilBean{
        private Integer id;
        private String name;

        private int age;

        public ReflectUtilBean() {
        }

        public ReflectUtilBean(Integer id, String name, int age) {
            this.id = id;
            this.name = name;
            this.age = age;
        }

        public ReflectUtilBean(Integer id, String name) {
            this.id = id;
            this.name = name;
        }

        public String checkGetMethodByName(String a, Integer b){
            return "checkFindMethod1";
        }

        public String checkGetMethodByName(String a, String b){
            return "checkFindMethod2";
        }

        public String checkGetMethodByName(String a){
            return "checkFindMethod3";
        }

        public ReflectUtilBean(Integer id) {
            this.id = id;
        }

        public ReflectUtilBean(String name) {
            this.name = name;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getAge() {
            return age;
        }

        public void setAge(int age) {
            this.age = age;
        }
    }

}

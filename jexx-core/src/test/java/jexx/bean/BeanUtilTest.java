package jexx.bean;

import jexx.time.DatePattern;
import jexx.time.DateUtil;
import jexx.util.Console;
import org.junit.Assert;
import org.junit.Test;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class BeanUtilTest {


    @Test
    public void tesTtoBean(){
        Map<String,Object> map = new HashMap<>();
        map.put("isA", "true");
        map.put("b", "false");
        map.put("cA", "true");
        map.put("ca", "true");
        map.put("da", "测试da");
        map.put("dA", "测试dA");
        map.put("e", "12");
        map.put("f", "2018-01-01");
//        TestBean testBean = BeanUtil.toBean(TestBean.class, map);
//        Assert.assertTrue(testBean.isA());
//        Assert.assertFalse(testBean.isB());
//        Assert.assertTrue(testBean.iscA());
//        Assert.assertTrue(testBean.isCa());
//        Assert.assertEquals("测试da", testBean.getDa());
//        Assert.assertEquals("测试dA", testBean.getdA());
//        Assert.assertEquals(12, testBean.getE().intValue());
//        Assert.assertEquals("2018-01-01", DateUtil.format(testBean.getF(), DatePattern.NORM_DATE_PATTERN));
    }



    public static class TestBean{
        private boolean isA;
        private boolean b;
        private boolean cA;
        private boolean ca;
        private String da;
        private String dA;

        private Integer e;
        private Date f;

        public boolean isA() {
            return isA;
        }

        public void setA(boolean a) {
            isA = a;
        }

        public boolean isB() {
            return b;
        }

        public void setB(boolean b) {
            this.b = b;
        }

        public boolean iscA() {
            return cA;
        }

        public void setcA(boolean cA) {
            this.cA = cA;
        }

        public boolean isCa() {
            return ca;
        }

        public void setCa(boolean ca) {
            this.ca = ca;
        }

        public String getDa() {
            return da;
        }

        public void setDa(String da) {
            this.da = da;
        }

        public String getdA() {
            return dA;
        }

        public void setdA(String dA) {
            this.dA = dA;
        }

        public Integer getE() {
            return e;
        }

        public void setE(Integer e) {
            this.e = e;
        }

        public Date getF() {
            return f;
        }

        public void setF(Date f) {
            this.f = f;
        }

        @Override
        public String toString() {
            return "TestBean{" +
                    "isA=" + isA +
                    ", b=" + b +
                    ", cA=" + cA +
                    ", ca=" + ca +
                    ", da='" + da + '\'' +
                    ", dA='" + dA + '\'' +
                    ", e=" + e +
                    ", f=" + f +
                    '}';
        }
    }

}

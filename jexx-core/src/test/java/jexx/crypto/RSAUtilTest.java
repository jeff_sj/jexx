package jexx.crypto;

import jexx.util.Base64Util;
import jexx.util.Console;
import jexx.util.HexUtil;
import jexx.util.StringUtil;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.security.KeyPair;

public class RSAUtilTest {

    private String privateKey;
    private String publicKey;

    @Before
    public void setUp(){
//        privateKey = RSAUtil.readPemFile(ResourceUtil.getResource("crypto/pkcs8_rsa_private_key.pem"));
//        publicKey = RSAUtil.readPemFile(ResourceUtil.getResource("crypto/rsa_public_key.pem"));

//        privateKey = RSAUtil.readPemFile(FileUtil.file("C:\\Users\\kxys4\\Desktop\\pkcs8_rsa_private.key"));
//        publicKey = RSAUtil.readPemFile(FileUtil.file("C:\\Users\\kxys4\\Desktop\\rsa_public.key"));

//        createKey();

        privateKey = "MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDXYHQaA2zTZ+6xCN6YvIWJSMPKhOzQWKTenvvjaekKAbrU7DFqoJG4uNHS8GxTdY6ftr3kHFyX4cWzeW9IjYkP9XHIwqaTbJNBSXIYRBkYA0jdGRMD6OsOMxBNdFbmRWMjynmA1/6+qjoob3DStdiydsREHdypf31Qe2xgGUlzx5+GNhWj33RdkYt5gOCkr30+fbEZNhyAyL7son6XzvKxWVLmAM4VlyToOp1ToqbfVhxnibPzhkqSvM4l29FyH2MO90zQQ388I1YRiJSNLg6ZJt+h9SY+0E6DhKVeWWuvkTv19J7bxeBV1fNjs4FbN9NbQGdaLJYddLrpkMMSTZdzAgMBAAECggEANNglpHxSp0xI9IcIltEqspR39ajHshcN3NGFXDcQRrRF8xhAx7i+CGTQlsEG0zN02xsrXFzZGyr6Jx8Ufui7SH8ge/HGjUWGm+45YFrVDV9A8X7Fe6AajFFbHqX87GL6AD4dsc9NYdrCiklNlWWo1aDuZwgsrCnrxWODI9EQhKdyvfZPaS8AS5+hHWpd5KbG7CKv9Hrk7HJl5bDgpR79AoXDcTFN7bkiBUPy4zK7Dn/hwBe4mJMRS1RYr6XKck1lWMj4cNdV9nksa6n/bONVSA8OCv1kd3ra4WObeSH94304UFbrbPaQnUgkkY0ua0LeSsLh1aZjoxRH71v0RIDWgQKBgQD5PwvCN8rWPx6uapgcxL0LaquIHR6iy1GNMKa4z+q6W/wD/ESuJ8MLr8SSq5RDWQxCv9xIxegHMQoakXly3gvPqjGRY+HM8C/Nk659ILtjPVRcYYLYdLqvcoK0ZsfdBOazTeidatzgPdkPE9PVYu8YFF6UND7tWODw1/NfVdCSjwKBgQDdNnbJHSQBSwbpVQfqm9xamdhV9QqW4rC5oSEd0hYczIhDFboZ1MC+Twll/UzTEzuU4RhD8hTO/0ir38644J/O5rWqM1OUBOV6JmDrgEXNkLyCU+DdH224FuPu+yzUHqT/JJ7LockyuRcZZT4s+1IBHBHEfi/+tvNkHsx0y7DO3QKBgQDAXdVgd5kYVC0E5oYyJfHV3uU2r2rWsF9nvjXfC7GZVacjGE+VrmCOI4FuxfgvJooc9mE41W3oJwQqTz7R1eh9xVhuYw2CE5qBasrQDjQyOsJibmwDTcxL3vBb9buprL+1gZrX7AM/GtztExt6ZNot1oLMlao/m0o4+CMA4IWSpwKBgBYSWBnK8Yu7e64OUOLkuEDF9uLktzDE35M184LxMkmfUr02zsHDC5QxeKOXeXHaCmjZaL1myxK4P/z+MLcbmnZfaKLZyi4LqwXcDHGS8+QF2/k4+0e02hKzHfz7TMbBY/8dHUb+FoAcHaToCCMe1FjN/yvW110XzgMgW5APvs11AoGAGbSXHsJXEhFz2R7BO8MfHZvjBvs+AndLUwdaaYlXC299qwXj/S42X1RwdIycy5Wtfva2m9hYzsomp4oGe55MbMNo3kE8a7NWcwCijMUkSXse2w8sXwwFXv9kE84IokLQZz6yVuBkGT6D0Q5PO68Jm3mHVENxGpNXQfDIDnEcOTo=";
        publicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA12B0GgNs02fusQjemLyFiUjDyoTs0Fik3p7742npCgG61OwxaqCRuLjR0vBsU3WOn7a95Bxcl+HFs3lvSI2JD/VxyMKmk2yTQUlyGEQZGANI3RkTA+jrDjMQTXRW5kVjI8p5gNf+vqo6KG9w0rXYsnbERB3cqX99UHtsYBlJc8efhjYVo990XZGLeYDgpK99Pn2xGTYcgMi+7KJ+l87ysVlS5gDOFZck6DqdU6Km31YcZ4mz84ZKkrzOJdvRch9jDvdM0EN/PCNWEYiUjS4OmSbfofUmPtBOg4SlXllrr5E79fSe28XgVdXzY7OBWzfTW0BnWiyWHXS66ZDDEk2XcwIDAQAB";
    }

    public void createKey(){
        KeyPair keyPair = KeyUtil.createKeyPair();
        privateKey = Base64Util.encodeAsStr(keyPair.getPrivate().getEncoded());
        Console.log("base64 private={}", privateKey);
        publicKey = Base64Util.encodeAsStr(keyPair.getPublic().getEncoded());
        Console.log("base64 public={}", publicKey);
    }

    @Test
    public void testRsa(){
        String data = "hello";
        byte[] data1 = RSAUtil.encrypt(data.getBytes(), privateKey);
        byte[] data2 = RSAUtil.decrypt(data1, publicKey);
        String data3 = StringUtil.str(data2);
        Assert.assertEquals(data, data3);
    }

    @Test
    public void testZhSign(){
        String data = "{\"val\":{\"a\":\"123\",\"b\":\"567\"},\"txcode\":\"1003\"}1549955280";
        Console.log(data);

        byte[] d1 = data.getBytes();

        byte[] d2 = SignUtil.sign(SignAlgorithm.SHA256withRSA, d1, Base64Util.decode(privateKey));
        Console.log("SHA256withRSA签名==>{}", HexUtil.encodeHexStr(d2));
        Assert.assertTrue(SignUtil.checkSign(SignAlgorithm.SHA256withRSA, d1, Base64Util.decode(publicKey), d2));
    }

}

package jexx.crypto;

import jexx.io.FileUtil;
import jexx.util.Base64Util;
import jexx.util.Console;
import jexx.util.ResourceUtil;
import jexx.util.StringUtil;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.io.IOException;
import java.net.URL;
import java.security.Security;
import java.util.Base64;

public class DigestUtilTest {


    protected String classDir;
    protected String fileDir;

    @Before
    public void setUp(){
        URL data = ResourceUtil.getResource("");
        classDir = data.getFile();
        fileDir = classDir.concat("/file");
    }

    @Test
    public void testMd5() throws IOException {
        String md5 = DigestUtil.md5Hex(ResourceUtil.getStream("file/data/a.png"));
        Assert.assertEquals("529a2cfd3346c7fe17b845b6ec90fcfd", md5);
    }

    /**
     * TODO 解码后换行符不统一
     */
    @Ignore
    @Test
    public void test2(){
        String xml = "2v7htxv/EezELYL2UZV5lUlDTV20YyBWJapCf6AG7fZMqIAl+Pcbyl9ThdkKGBVJgWq3+f+B6jQA/ch47ytvVAgxrqKEoC34r1zBsr5wwvYWI2UBWO7Wdit7vV2JrAoS1YbiGiVP+U3EQAs4LqxnGYnzCeygKYFwilO0TmHb1qJAPnxo8pYZdU0m+Z/wWYRsenjEMWVjpiWUoXqnTKU5Rl7h73EmFCh9NeXNIpHSMap/H7v5wBDRQIxfVAno74fkV4naqVcqxnlnbARhXZpg/t3+chau0cDFb2IgZb5ZuxciyoLcXOYwVbmdOtWxuh3X1wnnXvFjiNePgyguDf026zcI06e4Pf4fVmF0edHP+CBIElrE0RsWZkCryWxQMhCuk2IGr9ivSZJCyfMXL5ITvJ3aWdyI9y1JvRSPTze45t1GYyvrCpMLZyUxQVtk3JqzTt+WmElGsqQuEmNfBCMmDzoU2MctcyU3jp/CtIC0ZVZQiloaghMbj8rntaAoVA/oqp5Scj6eLH8pXcoI01/9tAYoxjUd+T6ZjohHGTz/TfwF4K68tTXB1patXv3DGj9DbBVLeM2+CzXitTSYebIWjafAYjLh1BkDz3JRWiFrseW7gs3z+uXs+5Or7AxGBQ4LK3067srSGwO6IPm0uiYFYnJFFAx7U2rOdYvzyChVylziHxNj/+QaHpOQh/xi9x+aTEgr7w2RYkr+K+8CzP1C6yCsoeRo/g5JaORdDDqZVaG6t8WcMRljt/wpPZDhjaIxISWNUjJKzfnjI29RSBPT33EKtcfFBcJWqGa8tm+JHgcNcszJe2fBNbm3cgX9ZpWJIlqOa5ybFSPXWqrQeJGwT/DN8klgi0PgoJvDQDjUgBQas2gInHjKMSJQy2nVVwtgJvVIe31+jVTnbIVmylU/SKoETRsXzFPoCim7lQpjyyaI/BQRkQfagORPqkeapMRVTkASj4j/D5YtEPXzgbH/URTe168BqhWI/tINhU7MPjW705z7IE62t5X6Rhf5lYQSCCH/yS5iUuVhdHwyBChq2zpK7w1awL2WfOQVht6dD48=";
        String key = "zZZ8CwUrwBIL8UdZZn19P6Xdk6kfk8Rk";
        String info = ResourceUtil.getStreamAsString("crypto/refund_notice_info");

        String key1 = DigestUtil.md5Hex(key).toLowerCase();
        byte[] infoXMLBytes = AESUtil.decrypt(xml, key1, "AES/ECB/PKCS7Padding", new BouncyCastleProvider());
        String infoXML = StringUtil.str(infoXMLBytes);
        Assert.assertEquals(info, infoXML);

    }

    public static String getRefundDecrypt(String reqInfoSecret, String key) {
        String result = "";
        try {
            Security.addProvider(new BouncyCastleProvider());
            byte[] bt = Base64Util.decode(reqInfoSecret);

            String md5key = DigestUtil.md5Hex(key).toLowerCase();
            SecretKey secretKey = new SecretKeySpec(md5key.getBytes(), "AES");
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS7Padding");
            cipher.init(Cipher.DECRYPT_MODE, secretKey);
            byte[] resultbt = cipher.doFinal(bt);
            result = new String(resultbt, "UTF-8");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }


}

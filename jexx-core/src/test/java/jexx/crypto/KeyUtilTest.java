package jexx.crypto;

import jexx.util.Base64Util;
import jexx.util.Console;
import jexx.util.HexUtil;
import jexx.util.ResourceUtil;
import org.junit.Assert;
import org.junit.Test;

import java.security.KeyPair;

public class KeyUtilTest {


    @Test
    public void testGetKeyPair(){
        KeyPair keyPair = KeyUtil.getKeyPair(ResourceUtil.getStream("crypto/gsyh/test.pfx"), "Aa123456");
        String privateKeyWithBase64 = "MIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQCo/KsTX1WFayoWgonoOPvpOFt5H6QZrRmzqqhG0YgX/wx9WC2rvpg89yfR3347qLcelYA2ZgMKV4JVU81GbiYTcLpoL2pRnBj+SRq3Vko9uArw915n4Bs+PS6Cs2TTctJOW8GpeceQc7jbIn4JeAm58DFWGMJYaWtwlHVA1aCSaCCV+pAYB2YOr5fLRmbkHfl1AvuebVpFelw2fNg3Wqo5wlbiKZJ+zZudVrLyYQLtAkZZ67EtUu3FCD0PQwj7J54RsvS+HDVUwrz4E8BcMAZYmmYQKJi9dCf33yGQb4B8LoghU9/Scm//YoVRkcVZ5ZaRPMV7qTt8yoo9lpPaWoVfAgMBAAECggEABGAuidzhGQhSWazdp066byqPa85+8E9EKBvWKadJT93B1AkRfa38wS9hL5UK7r6Kp1cVr9LqZz0m2dDGsvktMTmtCf+YUC6rkwIPmAm6+GgomF6/ag8qhW0OJEJC7Rq/CVpk5F+L5Fiqw7nb66DZF6B5lpIjjiiEqLCfbKNbeg7/Y6wFYYBZ7CedE5CDGua6YejWbTLbHRyVJEArqBjH3XNgM9BcGs/L1MPER8gSOcaFxDfnPt+Gtbco0QEfgfvv1fwm4NQcawu7TYwwveg2ryv2MrfOYaqDPWiZrhaZ+hgHZX/KBTgLxuLeN0s/Poa+a5HG7DX4vFZg7LgGk4OV4QKBgQDgQdrVAyFvxKSBWBlGVIITig7ZPJwK251YEp7cAvVjFB6nI3kRysBulQGrHpAPWjH2CdJNhYQtQ28hvhoO231bhl3ooeDu0gIwQEreYeq3ytNnc6V7bJSBKx0ZTOt/2jg8jYzeX6bHoLqC8ei5lpdfjXJC9ni21uuPLoDhtn+87wKBgQDA6Azdswi+2WzOdykanYNEGwsDc5NpqsbNZ7RdA01MPb1iU/NX8l6KU6tQAQT+woWPG4Azt66IM3mmFBK5ZBX/RmTBvfL6m3g836/MFsn4NPo6syPCbifGMH8wvV0Ser0ywekr1ll2JTv7aPGGl7XcJzZL6ayt73iC0C+ZKuuekQKBgFtUNoy9o6ZjDSPn/xMXY4oVlYp/FoWNfvzZSH0Nb2YUUY978NiAG/JtGKrZ3Y2DDAnJYi5UC8prRh0V0ILwfJMDm8cb9gEM4ftIk7URZ4fGQGZHQMONKsWYC1e8bcQ52OFAPHhrt/5gxNSDNGYwoCE396qxueOsj25WYAKMUmVdAoGAQqVpHIcoNFeF4ecFSm601SjKq0yY117rBDgZ9jCX2glSjtAWOIJB62RwwVk40pN99S05I6UIJYIzOw+gUHClsOYvFUh809w2tEFLySTublRPR0xwxFGuaCiFKN+jcij5iLm5xy1tBwZiLL/bAfcUG5Voit2WjRk492jZqP9NHCECgYBpPGElnjzKxKZbqj/XXcjn/LbsQ6LY7tXCekh9qxnbwklbd+khrmJP4PzLyidV7u4fXvb7spDs5483CXMRxWtwNjY1knhgLi8konigeB7Q9d1mCn5LFXdgBtm/+68umYUXNbTXRDVNTFxofxu09z8W+CGD5iO5fZf7rBKFyxAPAQ==";
        Assert.assertEquals(privateKeyWithBase64, Base64Util.encodeAsStr(keyPair.getPrivate().getEncoded()));
        String publicKeyWithBase64 = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAqPyrE19VhWsqFoKJ6Dj76ThbeR+kGa0Zs6qoRtGIF/8MfVgtq76YPPcn0d9+O6i3HpWANmYDCleCVVPNRm4mE3C6aC9qUZwY/kkat1ZKPbgK8PdeZ+AbPj0ugrNk03LSTlvBqXnHkHO42yJ+CXgJufAxVhjCWGlrcJR1QNWgkmgglfqQGAdmDq+Xy0Zm5B35dQL7nm1aRXpcNnzYN1qqOcJW4imSfs2bnVay8mEC7QJGWeuxLVLtxQg9D0MI+yeeEbL0vhw1VMK8+BPAXDAGWJpmECiYvXQn998hkG+AfC6IIVPf0nJv/2KFUZHFWeWWkTzFe6k7fMqKPZaT2lqFXwIDAQAB";
        Assert.assertEquals(publicKeyWithBase64, Base64Util.encodeAsStr(keyPair.getPublic().getEncoded()));

        String data = "{\"val\":{\"a\":\"123\",\"b\":\"567\"},\"txcode\":\"1003\"}1549955280";
        Console.log(data);

        byte[] d1 = data.getBytes();
        String d2 = SignUtil.signAsBase64(SignAlgorithm.SHA256withRSA, d1, Base64Util.decode(privateKeyWithBase64));
        Console.log("SHA256withRSA签名==>{}", d2);
        Assert.assertTrue(SignUtil.checkSignAsBase64(SignAlgorithm.SHA256withRSA, d1, Base64Util.decode(publicKeyWithBase64), d2));
    }

}

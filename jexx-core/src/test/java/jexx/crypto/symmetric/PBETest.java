package jexx.crypto.symmetric;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author jeff
 * @since 2020/5/8
 */
public class PBETest {

    @Test
    public void testPBE(){
        String data = "我是测试DES加密的";
        String password = "password12345678";
        String salt = "12345678";

        PBE pbe = new PBE(password, salt);
        String c1 = pbe.encryptAsBase64(data);
        String c2 = pbe.decryptAsBase64(c1);
        Assert.assertEquals(data, c2);
    }

}

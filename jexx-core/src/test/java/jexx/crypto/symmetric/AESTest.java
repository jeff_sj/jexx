package jexx.crypto.symmetric;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author jeff
 * @since 2020/5/8
 */
public class AESTest {

    @Test
    public void testDES(){
        String data = "我是测试DES加密的";
        String password = "password12345678";
        String isv = "1234567812345678";

        AES aes = new AES(AES.AES_ECB_PKCS5Padding, password);
        String c1 = aes.encryptAsBase64(data);
        String c2 = aes.decryptAsBase64(c1);
        Assert.assertEquals(data, c2);

        aes = new AES(password).withIv(isv);
        c1 = aes.encryptAsBase64(data);
        c2 = aes.decryptAsBase64(c1);
        Assert.assertEquals(data, c2);

    }

}

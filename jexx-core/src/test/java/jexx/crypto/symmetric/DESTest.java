package jexx.crypto.symmetric;

import jexx.util.Console;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author jeff
 * @since 2020/5/8
 */
public class DESTest {

    @Test
    public void testDES(){
        String data = "{\"cust_merch_id\":\"test\",\"opr_usr\":\"JF006201\",\"t\":\"1589765395865\",\"txcode\":\"5104\",\"merch_id\":\"JF0062\"}";
        String password = "1234qwer";
        String isv = "12345678";

        DES des = new DES(DES.DES_ECB_PKCS5Padding, password);
        String c1 = des.encryptAsBase64(data);
        Console.log("c1={}", c1);
        String c2 = des.decryptAsBase64(c1);
        Console.log("c2={}", c2);
        Assert.assertEquals(data, c2);

        des = new DES(password).withIv(isv);
        c1 = des.encryptAsBase64(data);
        c2 = des.decryptAsBase64(c1);
        Assert.assertEquals(data, c2);

    }

}

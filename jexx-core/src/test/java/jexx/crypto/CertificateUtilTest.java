package jexx.crypto;

import jexx.time.DateUtil;
import jexx.util.ResourceUtil;
import org.junit.Assert;
import org.junit.Test;

import java.io.InputStream;
import java.security.Principal;
import java.security.PublicKey;
import java.security.cert.CertificateExpiredException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

/**
 * 证书工具类测试
 * @author jeff
 * @since 2020/6/17
 */
public class CertificateUtilTest {

    @Test
    public void testCertificate() throws Exception {
        try(InputStream inputStream = ResourceUtil.getStream("crypto/test.crt")){
            CertificateFactory factory = CertificateFactory.getInstance("X.509");
            X509Certificate certificate = (X509Certificate) factory.generateCertificate(inputStream);
            Assert.assertEquals("2019-04-25 14:48:54", DateUtil.formatDateTime(certificate.getNotBefore()));
            Assert.assertEquals("2020-04-25 14:48:54", DateUtil.formatDateTime(certificate.getNotAfter()));
            Principal subjectDN = certificate.getSubjectDN();
            Assert.assertEquals("O=icbc.com.cn, OU=1102, CN=0002.e.1102", subjectDN.getName());
            Principal issuerDN = certificate.getIssuerDN();
            Assert.assertEquals("O=icbc.com.cn, CN=ICBC Corporate Sub CA", issuerDN.getName());
            //签名算法名称
            String algorithm = certificate.getSigAlgName();
            Assert.assertEquals("SHA256withRSA", algorithm);

            PublicKey publicKey = certificate.getPublicKey();
            Assert.assertNotNull(publicKey);

            try {
                certificate.checkValidity();
                Assert.fail();
            }
            catch (CertificateExpiredException e){
                //skip
            }
        }
    }

}

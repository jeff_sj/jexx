package jexx.crypto.asymmetric;

import jexx.util.Base64Util;
import jexx.util.Console;
import jexx.util.StringUtil;
import org.bouncycastle.asn1.gm.GMNamedCurves;
import org.bouncycastle.asn1.x9.X9ECParameters;
import org.bouncycastle.crypto.engines.SM2Engine;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.jce.spec.ECParameterSpec;
import org.junit.Assert;
import org.junit.Test;

import javax.crypto.Cipher;
import java.security.KeyPair;
import java.security.Provider;
import java.security.PublicKey;
import java.security.Security;

public class SM2Test {

    @Test
    public void testSM2(){
        Provider provider = new BouncyCastleProvider();
        if(Security.getProvider(provider.getName()) == null){
            Security.addProvider(provider);
        }

//        String priKey = "MIGIAgEAMBQGCCqBHM9VAYItBggqgRzPVQGCLQRtMGsCAQEEIMyursFvovtoQqQgXB6QfNKbPFxMxVsz0i2A+V3cb+jgoUQDQgAEQLHucrpdPLb8eVC7VBrLVBzwmR2guZjUQkaQ3D7yutheXleIFenMbBhJz2MgGra25UgBoJp4SEFWYnu9H0kRXA==";
//        String pubKey = "";
//        SM2 sm2 = new SM2(SM2Engine.Mode.C1C3C2, null, priKey, pubKey);

        KeyPair keyPair = SM2.generateSm2KeyPair();
        Console.log("private={}", keyPair.getPrivate());
        SM2 sm2 = new SM2(SM2Engine.Mode.C1C2C3, null, keyPair.getPrivate().getEncoded(), keyPair.getPublic().getEncoded());

        String str = "你好,我是国密SM2算法！";
        String encodeStr1 = sm2.encryptAsBase64(str);
        String decodeStr1 = sm2.decryptAsBase64(encodeStr1);
        Assert.assertEquals(str, decodeStr1);

        String signature = sm2.signAsHex(encodeStr1);

        boolean success = sm2.verifyAsHex(encodeStr1, signature);
        Assert.assertTrue(success);
    }

    @Test
    public void testGenerateKeyPair(){
        KeyPair keyPair = SM2.generateSm2KeyPair();

        Console.log(keyPair.getPublic().getAlgorithm());
        Console.log(keyPair.getPublic().getFormat());
        Console.log(Base64Util.encodeAsStr(keyPair.getPrivate().getEncoded()));
        Console.log(Base64Util.encodeAsStr(keyPair.getPublic().getEncoded()));
        Console.log(keyPair.getPrivate().getAlgorithm());
        Console.log(keyPair.getPrivate().getFormat());
    }

    @Test
    public void testCipher() throws Exception {
        Provider provider = new BouncyCastleProvider();
        if(Security.getProvider(provider.getName()) == null){
            Security.addProvider(provider);
        }


        KeyPair keyPair = SM2.generateSm2KeyPair();
        SM2 sm2 = new SM2(keyPair.getPrivate().getEncoded(), keyPair.getPublic().getEncoded());

        String str = "你好,我是国密SM2算法！";
        String encodeStr1 = sm2.encryptAsBase64(str);
        String decodeStr1 = sm2.decryptAsBase64(encodeStr1);
        Assert.assertEquals(str, decodeStr1);

        PublicKey publicKey = keyPair.getPublic();
        Console.log("publicKey class={}", publicKey.getClass());


//        KeyFactory keyFactory = KeyFactory.getInstance("EC", BouncyCastleProvider.PROVIDER_NAME);
//        final X509EncodedKeySpec keySpec = new X509EncodedKeySpec(x509Bytes);
//        return (BCECPublicKey) keyFactory.generatePublic(keySpec);

        BouncyCastleProvider p = new BouncyCastleProvider();
        Cipher cipher = Cipher.getInstance("SM2", p);
        cipher.init(Cipher.ENCRYPT_MODE, keyPair.getPublic());
        byte[] encodeByte21 = cipher.doFinal(StringUtil.getBytes(str));
        cipher.init(Cipher.DECRYPT_MODE, keyPair.getPrivate());
        byte[] decodeByte21 = cipher.doFinal(encodeByte21);
        String  decodeStr2 = StringUtil.str(decodeByte21);
        Assert.assertEquals(str, decodeStr2);

        X9ECParameters parameters = GMNamedCurves.getByName("sm2p256v1");
        ECParameterSpec ecParameterSpec = new ECParameterSpec(parameters.getCurve(), parameters.getG(), parameters.getN(), parameters.getH());
//        ECPoint ecPoint = parameters.getCurve().decodePoint(keyPair.getPrivate().getEncoded());

//        String priKey = ((BCECPrivateKey )keyPair.getPrivate()).getD().toString(16);
//        BigInteger bigInteger = new BigInteger(priKey, 16);
//        KeyFactory keyFactory = KeyFactory.getInstance("EC", provider);
//        BCECPrivateKey privateKey = (BCECPrivateKey) keyFactory.generatePrivate(new ECPrivateKeySpec(bigInteger,
//                ecParameterSpec));
//        cipher.init(Cipher.DECRYPT_MODE, privateKey);
//        byte[] decodeByte31 = cipher.doFinal(Base64Util.decode(encodeStr1));
//        String  decodeStr3 = StringUtil.str(decodeByte31);
//        Assert.assertEquals(str, decodeStr3);

//        cipher.init(Cipher.DECRYPT_MODE, keyPair.getPrivate());
//        byte[] decodeByte31 = cipher.doFinal(Base64Util.decode(encodeStr1));
//        String  decodeStr3 = StringUtil.str(decodeByte31);
//        Assert.assertEquals(str, decodeStr3);

    }

}

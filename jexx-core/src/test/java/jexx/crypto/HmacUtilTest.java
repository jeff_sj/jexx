package jexx.crypto;

import org.junit.Assert;
import org.junit.Test;

public class HmacUtilTest {

    @Test
    public void testHmacSha256(){
        String data = "abcdefg";
        String key = "123456";

        String hex = HmacUtil.hmacSha256Hex(data, key);
        Assert.assertEquals("079198f11d96de4df14f9f653746cbfff138f19aa55eb48af77314d6547f332d", hex);
    }

}

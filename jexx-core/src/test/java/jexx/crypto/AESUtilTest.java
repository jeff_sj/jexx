package jexx.crypto;

import jexx.util.Console;
import jexx.util.StringUtil;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.Assert;
import org.junit.Test;

public class AESUtilTest {

    @Test
    public void test(){
        String data = "hello";
        String key = "1234567890123456";

        // 加密
        byte[] en = AESUtil.encrypt(StringUtil.getBytes(data), StringUtil.getBytes(key), "AES/ECB/PKCS7Padding", new BouncyCastleProvider());
        String enStr = StringUtil.str(en);
        Console.log(enStr);

        // 解密
        byte[] de = AESUtil.decrypt(en, StringUtil.getBytes(key), "AES/ECB/PKCS7Padding", new BouncyCastleProvider());
        String deStr = StringUtil.str(de);
        Assert.assertEquals(data, deStr);
    }

}

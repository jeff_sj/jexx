package jexx.crypto;

import jexx.util.Base64Util;
import jexx.util.Console;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.security.KeyPair;

/**
 * @author jeff
 * @since 2020/5/12
 */
public class SignUtilTest {

    private String privateKey;
    private String publicKey;

    @Before
    public void setUp(){
        KeyPair keyPair = KeyUtil.createKeyPair();
        privateKey = Base64Util.encodeAsStr(keyPair.getPrivate().getEncoded());
        Console.log("private={}", privateKey);
        publicKey = Base64Util.encodeAsStr(keyPair.getPublic().getEncoded());
        Console.log("public={}", publicKey);
    }

    @Test
    public void testSign(){
        checkSign(SignAlgorithm.SHA256withRSA);
        checkSign(SignAlgorithm.SHA1withRSA);
    }

    private void checkSign(SignAlgorithm algorithm){
        String data = "我来测试SignUtil吧！";

        String sign = SignUtil.signAsBase64(algorithm, data.getBytes(), Base64Util.decode(privateKey));
        Assert.assertTrue(SignUtil.checkSignAsBase64(algorithm, data.getBytes(), Base64Util.decode(publicKey), sign));

        sign = SignUtil.signAsHex(algorithm, data.getBytes(), Base64Util.decode(privateKey));
        Assert.assertTrue(SignUtil.checkSignAsHex(algorithm, data.getBytes(), Base64Util.decode(publicKey), sign));

        sign = SignUtil.signAsBase64WithPkcs8Key(algorithm, data, privateKey);
        Assert.assertTrue(SignUtil.checkSign(algorithm, data, publicKey, sign));
    }

}

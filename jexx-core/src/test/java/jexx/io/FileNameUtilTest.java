package jexx.io;

import jexx.util.Console;
import static org.junit.Assert.*;

import org.junit.Test;

public class FileNameUtilTest {


    @Test
    public void testNormalize(){
        Console.log(FileNameUtil.normalize("D:\\test\\HelloWorld.pdf"));
        Console.log(FileNameUtil.normalize("../a\\\\test.txt"));
    }

    @Test
    public void testIndexOfLastSeparator(){
        assertEquals(7, FileNameUtil.indexOfLastSeparator("D:\\test\\HelloWorld.pdf"));
    }

    @Test
    public void testIndexOfExtension(){
        assertEquals(-1, FileNameUtil.indexOfExtension("D:\\test\\HelloWorld\\..\\a"));
        assertEquals(23, FileNameUtil.indexOfExtension("D:\\test\\HelloWorld\\..\\a.txt"));

        assertEquals("D:\\test\\HelloWorld\\..\\a", FileNameUtil.removeExtension("D:\\test\\HelloWorld\\..\\a"));
        assertEquals("D:\\test\\HelloWorld\\..\\a", FileNameUtil.removeExtension("D:\\test\\HelloWorld\\..\\a.txt"));
    }

    @Test
    public void testGetName(){
        assertEquals("c.txt", FileNameUtil.getName("a\\b\\c.txt"));
        assertEquals("c", FileNameUtil.getName("a\\b\\c"));
        assertEquals("c.txt", FileNameUtil.getName("a/b/c.txt"));
        assertEquals("c", FileNameUtil.getName("a/b/c"));
    }

    @Test
    public void testGetBaseName(){
        assertEquals("c", FileNameUtil.getBaseName("a\\b\\c.txt"));
        assertEquals("c", FileNameUtil.getBaseName("a\\b\\c"));
        assertEquals("c", FileNameUtil.getBaseName("a/b/c.txt"));
        assertEquals("c", FileNameUtil.getBaseName("a/b/c"));
    }

    @Test
    public void testReplace(){
        assertEquals("pdf", FileNameUtil.getExtension("test.pdf"));

        assertEquals("D:/test/test.pdf", FileNameUtil.replaceExtension("D:/test/test.txt", "pdf"));
        assertEquals("test.pdf", FileNameUtil.replaceExtension("test.txt", "pdf"));
        assertEquals("test.pdf", FileNameUtil.replaceExtension("test", "pdf"));

        assertEquals("b", FileNameUtil.replaceBaseName("a", "b"));
        assertEquals("a/b/d", FileNameUtil.replaceBaseName("a/b/c", "d"));
        assertEquals("b.txt", FileNameUtil.replaceBaseName("a.txt", "b"));
        assertEquals("a/b/d.txt", FileNameUtil.replaceBaseName("a/b/c.txt", "d"));
        assertEquals("D:/a/b/d.txt", FileNameUtil.replaceBaseName("D:/a/b/c.txt", "d"));

        assertEquals("b", FileNameUtil.replaceName("a", "b"));
        assertEquals("b", FileNameUtil.replaceName("a.txt", "b"));
        assertEquals("a/b/d", FileNameUtil.replaceName("a/b/c", "d"));
        assertEquals("a/b/d", FileNameUtil.replaceName("a/b/c.txt", "d"));
        assertEquals("D:/a/b/d", FileNameUtil.replaceName("D:/a/b/c", "d"));
        assertEquals("D:/a/b/d", FileNameUtil.replaceName("D:/a/b/c.txt", "d"));

    }

    @Test
    public void testRelativePath(){
        assertEquals("test.pdf", FileNameUtil.relativePath("D:/test/test.pdf", "D:/test"));
    }

    @Test
    public void testGetParentName(){
        assertEquals(FileNameUtil.normalize("D:/test"), FileNameUtil.getParentName("D:/test/test.pdf"));
        assertEquals(FileNameUtil.normalize("/opt/files/temp/20190628/110931"), FileNameUtil.getParentName("/opt/files/temp/20190628/110931/测试报告.xlsx"));
    }

    @Test
    public void testRemoveExtension(){
        assertEquals("a\\b\\c", FileNameUtil.removeExtension("a\\b\\c.txt"));
        assertEquals("a\\b\\c", FileNameUtil.removeExtension("a\\b\\c"));
        assertEquals("a/b/c", FileNameUtil.removeExtension("a/b/c.txt"));
        assertEquals("a/b/c", FileNameUtil.removeExtension("a/b/c"));
    }

}

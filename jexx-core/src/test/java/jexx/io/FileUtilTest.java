package jexx.io;

import jexx.system.OsInfo;
import jexx.util.CollectionUtil;
import jexx.util.Console;
import jexx.util.ResourceUtil;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import static org.junit.Assert.*;

public class FileUtilTest {

    protected String fileDir;
    protected String testDir;

    @Before
    public void setUp() {
        Path path = ResourceUtil.getResourceAsPath("file");
        fileDir = path.toString();
        path = path.resolve(FileUtilTest.class.getSimpleName());
        testDir = path.toString();
    }

    @Test
    public void testIsSameFile() throws IOException {
        FileUtil.createFileIfNotExist(Paths.get(testDir, "testIsSameFile.txt"));

        File file1 = FileUtil.file(testDir, "testIsSameFile.txt");
        File file2 = FileUtil.file(testDir, "../FileUtilTest/testIsSameFile.txt");
        Assert.assertTrue(FileUtil.isSameFile(file1, file2));
    }

    @Test
    public void diff() throws IOException {
        File file1 = new File(".\\out\\test.txt");

        OsInfo osInfo = new OsInfo();
        if(osInfo.isWindows()){
            //有 Test.txt的话, file1.getCanonicalPath()="F:\repository\chiway\jexx\jexx-core\out\Test.txt"
            Console.log(file1.getPath());
            Console.log(file1.getAbsolutePath());
            Console.log(file1.getCanonicalPath());
        }
        else if(osInfo.isLinux()){

        }
    }

    @Test
    public void testCreateFile() throws IOException {
        FileUtil.deleteDir(testDir);
        try {
            FileUtil.createFile(Paths.get(testDir, "testCreateFile.txt"), false, true);
            Assert.fail();
        }
        catch (Exception e){
        }
        FileUtil.createFile(Paths.get(testDir, "testCreateFile.txt"), true, true);
    }

    @Test
    public void testCopyFile() throws IOException {
        try {
            FileUtil.copyFile(fileDir+"/data/a.png", testDir+"/testCopyFile/jexx.png");
            FileUtil.copyFile(fileDir+"/data/lines.txt", testDir+"/testCopyFile/jexx.txt");
        }
        catch (Exception e){
            e.printStackTrace();
        }

        String temp = testDir + "/testCopyFile/tmp";
        FileUtil.mkdirs(temp);
        FileUtil.copyFileToDir(fileDir +"/data/a.png", temp);
        FileUtil.copyDirToDir(fileDir+"/data", temp);
    }

    @Test
    public void testMergeFiles() throws IOException {
        File file1 = FileUtil.file(testDir, "testMergeFiles1.txt");
        FileUtil.createFileIfNotExist(file1);
        FileUtil.write(file1, "abc".getBytes());
        File file2 = FileUtil.file(testDir, "testMergeFiles2.txt");
        FileUtil.createFileIfNotExist(file2);
        FileUtil.write(file2, "def".getBytes());
        File[] files = {file1, file2};

        File file3 = FileUtil.file(testDir, "testMergeFiles3.txt");
        FileUtil.createFileIfNotExist(file3);

        File output = FileUtil.mergeFiles(files, file3, "\r\n=========\r\n");
        Assert.assertEquals("abc\r\n=========\r\ndef", FileUtil.readStr(output));
    }

    @Test
    @Ignore
    public void testProbeContentType(){
        //mac 环境下获取不到文件类型
        Assert.assertEquals("image/png", FileUtil.probeContentType(FileUtil.file("file/data/a.png")));
        Assert.assertEquals("text/plain", FileUtil.probeContentType(FileUtil.file("file/data/lines.txt")));
        Assert.assertNull(FileUtil.probeContentType(FileUtil.file("file/setting/test.properties")));
    }

    @Test
    public void testSplitFile() throws IOException {
        File file1 = FileUtil.file(testDir, "testSplitFile.txt");
        FileUtil.createFileIfNotExist(file1);
        FileUtil.write(file1, "abc".getBytes());

        List<File> splitFiles = FileUtil.splitFile(file1, 2, FileUtil.file(testDir));
        Assert.assertEquals(2, splitFiles.size());
        Assert.assertEquals("ab", FileUtil.readStr(splitFiles.get(0)));
        Assert.assertEquals("c", FileUtil.readStr(splitFiles.get(1)));
    }

    @Test
    public void testSize() throws IOException {
        long size = FileUtil.size(Paths.get(fileDir, "size/a.txt"));
        Assert.assertEquals(1, size);
        size = FileUtil.size(Paths.get(fileDir, "size/1/b.txt"));
        Assert.assertEquals(2, size);
        size = FileUtil.size(Paths.get(fileDir, "size"));
        Assert.assertEquals(3, size);
    }

    @Test
    public void testWrite(){
        File file = FileUtil.file(testDir, "testWrite.txt");
    }

    @Test
    public void testRead() throws IOException {
        File file = FileUtil.file(testDir, "a.txt");
        FileUtil.createFileIfNotExist(file);
        FileUtil.write(file, "abc".getBytes());
        assertEquals("abc", FileUtil.readStr(file));

        FileUtil.write(file, "abc\r\ndef\r\nghi".getBytes());
        List<String> lines = FileUtil.readLines(file, line -> !line.equals("def"));
        assertEquals(2, lines.size());
        assertEquals("abc", lines.get(0));
        assertEquals("ghi", lines.get(1));

        FileUtil.writeLines(file, CollectionUtil.list("abc", "def", "ghi"));
        lines = FileUtil.readLines(file);
        assertEquals(3, lines.size());
        assertEquals("abc", lines.get(0));
        assertEquals("def", lines.get(1));
        assertEquals("ghi", lines.get(2));
    }

    @Test
    public void testMove(){
        String testFileName = testDir + "/testMove.txt";
        File file = FileUtil.write(new File(testFileName), "testMove".getBytes());
        assertTrue(FileUtil.exists(file));

        File destFile = FileUtil.move(file, FileUtil.file(testDir + "/testMove11.txt"), true);
        assertTrue(FileUtil.exists(destFile));
        assertFalse(FileUtil.exists(file));

        FileUtil.deleteFile(destFile);

        //move to dir
        testFileName = testDir + "/testMove";
        File dir = FileUtil.mkdir(testFileName);
        assertTrue(FileUtil.exists(dir));

        file = FileUtil.touch(testDir + "/testMove.txt");
        assertTrue(FileUtil.exists(file));
        FileUtil.moveToDir(file, dir, true);
        assertFalse(FileUtil.exists(file));
        assertTrue(FileUtil.exists(testDir + "/testMove/testMove.txt"));

        FileUtil.deleteDir(dir);
    }

    @Test
    public void testRename(){
        String testFileName = testDir +"/testRename.txt";
        File file = FileUtil.write(new File(testFileName), "testRename".getBytes());

        FileUtil.rename(file, "testRename111.txt", true);

        Path path = file.toPath().resolveSibling("testRename111.txt");
        Console.log(path.toString());
    }

    @Test
    public void testDelete(){
        FileUtil.mkdirs(testDir.concat("/testDelete"));
        FileUtil.touch(testDir.concat("/testDelete/a.txt"));
        FileUtil.touch(testDir.concat("/testDelete/b.txt"));
        FileUtil.mkdirs(testDir.concat("/testDelete/dir"));
        FileUtil.touch(testDir.concat("/testDelete/dir/a.txt"));

        File file = FileUtil.file(testDir.concat("/testDelete/b.txt"));
        assertTrue(FileUtil.exists(file));
        FileUtil.deleteFile(file);
        assertFalse(FileUtil.exists(file));

        file = FileUtil.file(testDir.concat("/testDelete"));
        assertTrue(FileUtil.exists(file));
        FileUtil.deleteDir(file);
        assertFalse(FileUtil.exists(file));
    }

}

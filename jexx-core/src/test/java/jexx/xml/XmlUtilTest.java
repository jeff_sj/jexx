package jexx.xml;

import jexx.util.Console;
import org.junit.Assert;
import org.junit.Test;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.LinkedHashMap;
import java.util.Map;

public class XmlUtilTest {

    @Test
    public void test1(){
        XmlBean xmlBean = new XmlBean();
        xmlBean.setId(1);
        xmlBean.setName("王五");
        xmlBean.setParentName("父亲");
        String xml = XmlUtil.toXML(xmlBean);
        Console.log(xml);
        Assert.assertTrue(xml.contains("aid"));
        Assert.assertTrue(xml.contains("bname"));
        Assert.assertTrue(xml.contains("cname"));


        XmlBean newXmlBean = XmlUtil.toBean(xml, XmlBean.class);
        Assert.assertNotNull(newXmlBean);
        Assert.assertEquals(xmlBean.id, newXmlBean.getId());
        Assert.assertEquals(xmlBean.name, newXmlBean.getName());
        Assert.assertEquals(xmlBean.getParentName(), newXmlBean.getParentName());
    }

    @Test
    public void test2(){
        String xml = "<xml><cname>父亲</cname><aid>1</aid><bname>王五</bname><dd>多个字段</dd></xml>";

        XmlBean newXmlBean = XmlUtil.toBean(xml, XmlBean.class);
        Assert.assertNotNull(newXmlBean);
        Assert.assertEquals(Integer.valueOf(1), newXmlBean.getId());
        Assert.assertEquals("王五", newXmlBean.getName());
        Assert.assertEquals("父亲", newXmlBean.getParentName());
    }

    @Test
    public void testMap(){
        Map<String, Object> map1 = new LinkedHashMap<>();
        map1.put("a", "1");
        map1.put("b", 2);

        String expectXml = "<xml><a>1</a><b>2</b></xml>";
        Assert.assertEquals(expectXml, XmlUtil.mapToXML(map1, "xml"));

        Map<String, String> map2 = XmlUtil.xmlToMap(expectXml);
        Assert.assertEquals("1", map2.get("a"));
        Assert.assertEquals("2", map2.get("b"));

    }

    @XmlRootElement(name = "xml")
    @XmlAccessorType(XmlAccessType.FIELD)
    private static class XmlBean extends Parent{
        @XmlElement(name = "aid")
        private Integer id;
        @XmlElement(name = "bname")
        private String name;

        public Integer getId() {
            return id;
        }
        public void setId(Integer id) {
            this.id = id;
        }
        public String getName() {
            return name;
        }
        public void setName(String name) {
            this.name = name;
        }
    }

    @XmlAccessorType(XmlAccessType.FIELD)
    private static class Parent{
        @XmlElement(name = "cname")
        private String parentName;

        public String getParentName() {
            return parentName;
        }

        public void setParentName(String parentName) {
            this.parentName = parentName;
        }
    }

}

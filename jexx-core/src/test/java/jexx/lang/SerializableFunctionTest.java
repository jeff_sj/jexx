package jexx.lang;

import jexx.util.Console;
import org.junit.Assert;
import org.junit.Test;

import java.io.Serializable;

public class SerializableFunctionTest {

    @Test
    public void testLambda(){
        Assert.assertEquals(ObjectUtilTestBean.class, checkClassName(ObjectUtilTestBean::getName));
        Assert.assertEquals(ObjectUtilTestBean.class, checkClassName(ObjectUtilTestBean::getAge));
        Assert.assertEquals(ObjectUtilTestBean.class, checkClassName(ObjectUtilTestBean::isSuccess));

        Assert.assertEquals("getName", checkMethodName(ObjectUtilTestBean::getName));
        Assert.assertEquals("getAge", checkMethodName(ObjectUtilTestBean::getAge));
        Assert.assertEquals("isSuccess", checkMethodName(ObjectUtilTestBean::isSuccess));
    }

    private <T> Class<T> checkClassName(SerializedFunction<T,?> lambda){
        Console.log("class={}", lambda.getClass());
        Console.log("method={}", lambda.getImplMethod());
        return lambda.getImplClass();
    }

    private <T> String checkMethodName(SerializedFunction<T,?> lambda){
        return lambda.getImplMethodName();
    }

    public static class ObjectUtilTestBean implements Serializable {
        private static final long serialVersionUID = -7809533204951422243L;
        private String name;
        private Integer age;
        private boolean success;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Integer getAge() {
            return age;
        }

        public void setAge(Integer age) {
            this.age = age;
        }

        public boolean isSuccess() {
            return success;
        }

        public void setSuccess(boolean success) {
            this.success = success;
        }
    }


}

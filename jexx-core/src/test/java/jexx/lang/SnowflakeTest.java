package jexx.lang;

import jexx.time.DateUtil;
import jexx.util.Console;
import org.junit.Test;

import java.util.Date;

/**
 * @author jeff
 * @since 2020/6/13
 */
public class SnowflakeTest {

    @Test
    public void test1(){
        Snowflake snowflake = new Snowflake(1, 1);
        Console.log(snowflake.nextId());
        Console.log(DateUtil.formatDateTime(new Date(1288834974657L)));
        Console.log(DateUtil.parseDateTime("2018-04-26 15:00:00").getTime());
    }

}

package jexx.lang;

import jexx.convert.Convert;
import jexx.util.CollectionUtil;
import org.junit.Assert;
import org.junit.Test;

public class RangeTest {

    @Test
    public void test1(){
        Ranger<Integer> range = new Ranger<>(0,10, (Integer current, int index) -> current+2, true, false);
        Assert.assertEquals("", "[0, 2, 4, 6, 8]", Convert.toStr(CollectionUtil.list(range.iterator())));

        range = new Ranger<>(0,10, (Integer current, int index) -> current+2, false, true);
        Assert.assertEquals("", "[2, 4, 6, 8, 10]", Convert.toStr(CollectionUtil.list(range.iterator())));

        range = new Ranger<>(0,10, (Integer current, int index) -> current+2, true, true);
        Assert.assertEquals("", "[0, 2, 4, 6, 8, 10]", Convert.toStr(CollectionUtil.list(range.iterator())));
    }

}

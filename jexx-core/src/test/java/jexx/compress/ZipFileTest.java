package jexx.compress;

import jexx.io.FileUtil;
import jexx.util.ResourceUtil;
import jexx.util.StringUtil;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.io.File;
import java.net.URL;
import java.util.List;

public class ZipFileTest {

    protected String fileDir;

    @Before
    public void setUp(){
        URL data = ResourceUtil.getResource("compress");
        fileDir = data.getFile();
    }

    @Test
    public void test(){
        ZipFile zipFile1 = new ZipFile(ResourceUtil.getResource("compress/c.zip"));
        zipFile1.append(ResourceUtil.getResource("compress/a.txt"), "a.txt");
        zipFile1.append(ResourceUtil.getResource("compress/b.txt"), "b.txt");
        zipFile1.append(StringUtil.getBytes("这是测试添加的文件"), "d.txt");
        zipFile1.copy("a.txt", "dir/a.txt");
        zipFile1.delete("a.txt");
        zipFile1.move("c.txt", "dir/c.txt");
        zipFile1.appendDir("dir1/");
        String testZip = fileDir.concat("/test1.zip");
        zipFile1.zip(new File(testZip));

        ZipFile zipFile2 = new ZipFile(new File(testZip));
        ZipFileEntry dirEntry = zipFile2.getEntryByName("dir1/");
        Assert.assertTrue(dirEntry.isDir());
        String testDir = fileDir.concat("/unzip");
        zipFile2.unzip(testDir);

        Assert.assertEquals(ResourceUtil.getStreamAsString("compress/a.txt"), FileUtil.readStr(FileUtil.file(testDir.concat("/dir/a.txt"))));
        Assert.assertEquals(ResourceUtil.getStreamAsString("compress/b.txt"), FileUtil.readStr(FileUtil.file(testDir.concat("/b.txt"))));
        Assert.assertEquals("这是zip中的", FileUtil.readStr(FileUtil.file(testDir.concat("/dir/c.txt"))));
        Assert.assertEquals("这是测试添加的文件", FileUtil.readStr(FileUtil.file(testDir.concat("/d.txt"))));

    }

    @Ignore
    @Test
    public void testAppendDirAndChildren(){
        ZipFile zipFile = new ZipFile();
        zipFile.appendDirAndChildren("D:/test/20190125", s -> !s.getName().startsWith("4"));
        zipFile.zip("D:/test/20190125/a.zip");
    }

}

package jexx.time;

import jexx.util.Console;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import java.util.concurrent.TimeUnit;

public class TimeIntervalTest {

    @Test
    @Ignore
    public void testTimeInterval() throws InterruptedException {
        TimeInterval timeInterval = new TimeInterval("test");
        timeInterval.start("task1");
        TimeUnit.SECONDS.sleep(1);
        timeInterval.stop();
        Assert.assertEquals( 1d, timeInterval.getLastTaskTimeSeconds(), 0.1);

        timeInterval.start("task2");
        TimeUnit.SECONDS.sleep(2);
        Assert.assertEquals( 2d, timeInterval.getTaskTimeSeconds(), 0.1);

        timeInterval.restart("task3");
        TimeUnit.SECONDS.sleep(3);
        Assert.assertEquals( 3d, timeInterval.getTaskTimeSeconds(), 0.1);
        timeInterval.stop();
        Assert.assertEquals( 3d, timeInterval.getLastTaskTimeSeconds(), 0.1);

        Assert.assertEquals( 6d, timeInterval.getTotalTimeSeconds(), 0.1);

        Console.log("task prettyPrint  ===>\n{}", timeInterval.prettyPrint(TimeUnit.SECONDS));

        Console.log("task toString  ===>\n{}", timeInterval.toString());
    }

}

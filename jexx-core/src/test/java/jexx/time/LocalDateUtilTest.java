package jexx.time;

import jexx.util.Console;
import org.junit.Assert;
import org.junit.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

/**
 * @author jeff
 * @since 2020/5/18
 */
public class LocalDateUtilTest {

    @Test
    public void testFormat(){
        LocalDateTime localDateTime = LocalDateTime.of(2021, 6, 2, 16, 30, 0);
        Assert.assertEquals("2021-06-02 16:30:00", LocalDateUtil.format(localDateTime, "yyyy-MM-dd HH:mm:ss"));
        Assert.assertEquals("2021/06/02 16:30:00", LocalDateUtil.format(localDateTime, "yyyy/MM/dd HH:mm:ss"));
    }

    @Test
    public void testIsBetween(){
        LocalDateTime start = LocalDateTime.of(2021, 11, 26, 14, 0, 0);
        LocalDateTime end = LocalDateTime.of(2021, 11, 26, 15, 0, 0);

        LocalDateTime t1 = LocalDateTime.of(2021, 11, 26, 13, 0, 0);
        Assert.assertFalse(LocalDateUtil.isBetween(t1, start, end));
        t1 = LocalDateTime.of(2021, 11, 26, 14, 0, 0);
        Assert.assertTrue(LocalDateUtil.isBetween(t1, start, end));
        t1 = LocalDateTime.of(2021, 11, 26, 14, 30, 0);
        Assert.assertTrue(LocalDateUtil.isBetween(t1, start, end));
        t1 = LocalDateTime.of(2021, 11, 26, 15, 0, 0);
        Assert.assertTrue(LocalDateUtil.isBetween(t1, start, end));
        t1 = LocalDateTime.of(2021, 11, 26, 15, 30, 0);
        Assert.assertFalse(LocalDateUtil.isBetween(t1, start, end));


    }

    @Test
    public void testToMilliSecond(){
        LocalDateTime now = LocalDateTime.now();
        String dateTimeStr = LocalDateUtil.formatDateTimeWithMilli(now);
        Assert.assertEquals(now, LocalDateUtil.parseDateTimeWithMill(dateTimeStr));

        long millSeconds = LocalDateUtil.toMilliSecond(now);
        Assert.assertEquals(now, LocalDateUtil.toLocalDateTime(millSeconds));
    }

    @Test
    public void testParse(){
//        LocalDateTime localDateTime = LocalDateTime.of(2021, 6, 2, 16, 30, 0);
//        Assert.assertEquals(localDateTime, LocalDateUtil.parse("2021-06-02 16:30:00", "yyyy-MM-dd HH:mm:ss"));
//        Assert.assertEquals(localDateTime, LocalDateUtil.parse("2021-06-02", "yyyy-MM-dd"));

        Console.log(LocalDate.now().format(DateTimeFormatter.ISO_LOCAL_DATE));

    }

}

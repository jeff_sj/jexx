package jexx.time;

import jexx.util.Console;
import org.junit.Assert;
import org.junit.Test;

import java.text.ParseException;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class DateUtilTest {

    @Test
    public void testIsSameDay() {
        DateTime day1  = DateUtil.parse("2020-01-01T00:00:00.000+08:00", DatePattern.ISO_3_DATETIME_MS_PATTERN);

        DateTime day2  = DateUtil.parse("2020-01-01T00:00:00.000+07:00", DatePattern.ISO_3_DATETIME_MS_PATTERN);
        Assert.assertTrue(DateUtil.isSameDay(day1, day2));
        try {
            day2  = DateUtil.parse("2020-01-01T23:00:00.000+07:00", DatePattern.ISO_3_DATETIME_MS_PATTERN);
            Assert.assertTrue(DateUtil.isSameDay(day1, day2));
            Assert.fail();
        }
        catch (Error e){
            //skip
        }

        day2  = DateUtil.parse("2020-01-01T00:00:00.000+07:01", DatePattern.ISO_3_DATETIME_MS_PATTERN);
        Assert.assertTrue(DateUtil.isSameHour(day1, day2));
        try {
            day2  = DateUtil.parse("2020-01-01T00:00:00.000+07:00", DatePattern.ISO_3_DATETIME_MS_PATTERN);
            Assert.assertTrue(DateUtil.isSameDay(day1, day2));
            Assert.fail();
        }
        catch (Error e){
            //skip
        }
    }

    @Test
    public void testParse(){
        String xx = "2019-04-02-13.14.58";
        DateTime dateTime = DateUtil.parse(xx, "yyyy-MM-dd-HH.mm.ss");
        Assert.assertEquals("2019-04-02 13:14:58",DateUtil.format(dateTime, DatePattern.NORM_DATETIME_PATTERN));

        xx = "2012-12-12T12:12:12.012+0701";
        dateTime = DateUtil.parse(xx);
        Assert.assertEquals("2012-12-12 13:11:12.012",DateUtil.format(dateTime, DatePattern.NORM_DATETIME_MS_PATTERN));

        xx = "2012-12-12T12:12:12.012+07:01";
        dateTime = DateUtil.parse(xx);
        Assert.assertEquals("2012-12-12 13:11:12.012",DateUtil.format(dateTime, DatePattern.NORM_DATETIME_MS_PATTERN));

        xx = "2012-12-12 12:12:12";
        dateTime = DateUtil.parse(xx);
        Assert.assertEquals("2012-12-12 12:12:12",DateUtil.format(dateTime, DatePattern.NORM_DATETIME_PATTERN));

        xx = "2012/12/12 12:12:12";
        dateTime = DateUtil.parse(xx);
        Assert.assertEquals("2012-12-12 12:12:12",DateUtil.format(dateTime, DatePattern.NORM_DATETIME_PATTERN));

        xx = "2012年12月12日 12时12分12秒";
        dateTime = DateUtil.parse(xx);
        Assert.assertEquals("2012-12-12 12:12:12",DateUtil.format(dateTime, DatePattern.NORM_DATETIME_PATTERN));

        xx = "12:12:12";
        dateTime = DateUtil.parse(xx);
        Assert.assertEquals("12:12:12",DateUtil.format(dateTime, DatePattern.NORM_TIME_PATTERN));

        xx = "12时12分12秒";
        dateTime = DateUtil.parse(xx);
        Assert.assertEquals("12:12:12",DateUtil.format(dateTime, DatePattern.NORM_TIME_PATTERN));

        xx = "20121212121212";
        dateTime = DateUtil.parse(xx);
        Assert.assertEquals("2012-12-12 12:12:12",DateUtil.format(dateTime, DatePattern.NORM_DATETIME_PATTERN));

        xx = "20121212121212012";
        dateTime = DateUtil.parse(xx);
        Assert.assertEquals("2012-12-12 12:12:12.012",DateUtil.format(dateTime, DatePattern.NORM_DATETIME_MS_PATTERN));

        xx = "20121212";
        dateTime = DateUtil.parse(xx);
        Assert.assertEquals("2012-12-12",DateUtil.format(dateTime, DatePattern.NORM_DATE_PATTERN));

        xx = "2019-03-29T16:00:00.100Z";
        dateTime = DateUtil.parse(xx);
        Assert.assertEquals("2019-03-30T00:00:00.100Z", DateUtil.format(dateTime, DatePattern.ISO_Z_DATETIME_MS_PATTERN));

        //时区
        dateTime = DateUtil.parse("2019-03-29T16:44:03.100Z", DatePattern.ISO_1_DATETIME_MS_PATTERN);
        Assert.assertEquals("2019-03-30 00:44:03", dateTime.toString());
        dateTime = DateUtil.parse("2019-03-29T16:44:03Z");
        Assert.assertEquals("2019-03-30 00:44:03", dateTime.toString());
    }

    @Test
    public void testCompare() {
        Date time1 = DateUtil.parseDateTime("2019-03-29 16:44:03");
        Date time2 = DateUtil.parseDateTime("2019-03-29 16:44:13");
        Assert.assertTrue(DateUtil.before(time1, time2, 9000));
        Assert.assertFalse(DateUtil.before(time1, time2, 10000));
        Assert.assertTrue(DateUtil.after(time2, time1, 9000));
        Assert.assertFalse(DateUtil.after(time2, time1, 10000));
    }

    @Test
    public void testBeginOfDay() {
        Date now  = DateUtil.parseDateTime("2019-08-02 18:18:18");
        Assert.assertEquals("2019-08-02 00:00:00", DateUtil.formatDateTime(DateUtil.beginOfDay(now, TimeZone.getTimeZone("Asia/Shanghai"))));
        Assert.assertEquals("2019-08-01 23:15:00", DateUtil.formatDateTime(DateUtil.beginOfDay(now, TimeZone.getTimeZone("Australia/Eucla"))));

        TimeZone timeZone = TimeZone.getDefault();
        Console.log(TimeZone.getDefault());
        Console.log(timeZone.getDisplayName());
        Console.log(timeZone.getDisplayName(Locale.ENGLISH));
        Console.log(timeZone.getRawOffset());
        Console.log(timeZone.toZoneId().getId());
    }

    @Test
    public void testFormat() {
        Date now  = DateUtil.parseDateTime("2019-08-02 18:18:18");
        Assert.assertEquals("20190802181818", DateUtil.formatPureDateTime(now));
        Assert.assertEquals("20190802", DateUtil.formatPureDate(now));
        Assert.assertEquals("2019-08-02T18:18:18+08:00", DateUtil.format(now, DatePattern.ISO_3_DATETIME_PATTERN));
    }

    @Test
    public void testTimeZone() {
        DateTime day  = DateUtil.parse("2020-01-01T00:00:00.000+08:00", DatePattern.ISO_3_DATETIME_MS_PATTERN);
        Assert.assertEquals(2020, DateUtil.year(day, TimeZone.getTimeZone("GMT+08:00")));
        Assert.assertEquals(2020, DateUtil.year(day, TimeZone.getTimeZone("GMT+08:01")));
        Assert.assertEquals(2019, DateUtil.year(day, TimeZone.getTimeZone("GMT+07:59")));
        Assert.assertEquals(0, DateUtil.month(day, TimeZone.getTimeZone("GMT+08:00")));
        Assert.assertEquals(0, DateUtil.month(day, TimeZone.getTimeZone("GMT+08:01")));
        Assert.assertEquals(11, DateUtil.month(day, TimeZone.getTimeZone("GMT+07:59")));
        Assert.assertEquals(0, DateUtil.hour(day, TimeZone.getTimeZone("GMT+08:00")));
        Assert.assertEquals(0, DateUtil.hour(day, TimeZone.getTimeZone("GMT+08:01")));
        Assert.assertEquals(23, DateUtil.hour(day, TimeZone.getTimeZone("GMT+07:59")));

        Assert.assertEquals(2020, DateUtil.year(day, TimeZone.getTimeZone("Asia/Shanghai")));
        Assert.assertEquals(0, DateUtil.month(day, TimeZone.getTimeZone("Asia/Shanghai")));
    }

    @Test
    public void testBetween() {
        Assert.assertEquals(0, DateUtil.betweenMonth(DateUtil.parseDate("2022-04-01"), DateUtil.parseDate("2022-04-02"), true));
    }


}

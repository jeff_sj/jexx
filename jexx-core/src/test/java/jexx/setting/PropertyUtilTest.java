package jexx.setting;

import org.junit.Assert;
import org.junit.Test;

import java.util.Properties;

public class PropertyUtilTest {

    @Test
    public void test1(){
        Properties properties = PropertyUtil.loadFromClasspath("file/setting/test.properties");
        PropertyUtil.resolveAllVariables(properties);

        Assert.assertEquals("你好", properties.getProperty("a"));
        Assert.assertEquals("hello", properties.getProperty("b"));
        Assert.assertEquals("你好,世界", properties.getProperty("c"));
        Assert.assertEquals("word", properties.getProperty("1"));
        Assert.assertEquals("1.22", properties.getProperty("2"));
    }

}

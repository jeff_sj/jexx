package jexx.collect;

import jexx.util.ArrayUtil;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @author jeff
 * @since 2019/10/23
 */
public class RangeTest {

    @Test
    public void testRange(){
        assertEquals("[1,2]", Range.closed(1,2).toString());
        assertEquals("(1,2)", Range.open(1,2).toString());
        assertEquals("[1,2)", Range.closedOpen(1,2).toString());
        assertEquals("(1,2]", Range.openClosed(1,2).toString());
        assertEquals("(1,+∞)", Range.greaterThan(1).toString());
        assertEquals("[1,+∞)", Range.atLeast(1).toString());
        assertEquals("(-∞,1)", Range.lessThan(1).toString());
        assertEquals("(-∞,1]", Range.atMost(1).toString());
        assertEquals("[1,1]", Range.singleton(1).toString());

        assertEquals(Range.closed(1,2), Range.closed(1,2));
        assertEquals(Range.atMost(1), Range.atMost(1));

        assertTrue(Range.closed(1,3).contains(1));
        assertTrue(Range.closed(1,3).contains(2));
        assertTrue(Range.closed(1,3).contains(3));
        assertFalse(Range.open(1,3).contains(1));
        assertTrue(Range.open(1,3).contains(2));
        assertFalse(Range.open(1,3).contains(3));
        assertTrue(Range.closedOpen(1,3).contains(1));
        assertTrue(Range.closedOpen(1,3).contains(2));
        assertFalse(Range.closedOpen(1,3).contains(3));
        assertFalse(Range.openClosed(1,3).contains(1));
        assertTrue(Range.openClosed(1,3).contains(2));
        assertTrue(Range.openClosed(1,3).contains(3));
        assertFalse(Range.greaterThan(1).contains(1));
        assertTrue(Range.greaterThan(1).contains(2));
        assertFalse(Range.atLeast(1).contains(0));
        assertTrue(Range.atLeast(1).contains(1));
        assertTrue(Range.atLeast(1).contains(2));
        assertTrue(Range.lessThan(1).contains(0));
        assertFalse(Range.lessThan(1).contains(1));
        assertFalse(Range.lessThan(1).contains(2));
        assertTrue(Range.atMost(1).contains(1));
        assertFalse(Range.atMost(1).contains(2));
        assertTrue(Range.singleton(1).contains(1));
        assertFalse(Range.singleton(1).contains(2));

        DiscreteDomain<Integer> domain = DiscreteDomain.integers();
        assertEquals(Range.closed(1,4).canonical(domain), Range.open(0,5).canonical(domain));
        assertEquals(Range.closed(1,4).canonical(domain), Range.closedOpen(1,5).canonical(domain));
        assertEquals(Range.closed(1,4).canonical(domain), Range.openClosed(0,4).canonical(domain));
        assertEquals(Range.greaterThan(1).canonical(domain), Range.atLeast(2).canonical(domain));
    }

    @Test
    public void testRangeIterator(){
        assertArrayEquals(ArrayUtil.toArray(1,2,3,4), ArrayUtil.toArray(Range.closed(1, 4).iterator(DiscreteDomain.integers()), Integer.class));
        assertArrayEquals(ArrayUtil.toArray(2,3), ArrayUtil.toArray(Range.open(1, 4).iterator(DiscreteDomain.integers()), Integer.class));
        assertArrayEquals(ArrayUtil.toArray(1,2,3), ArrayUtil.toArray(Range.closedOpen(1, 4).iterator(DiscreteDomain.integers()), Integer.class));
        assertArrayEquals(ArrayUtil.toArray(2,3,4), ArrayUtil.toArray(Range.openClosed(1, 4).iterator(DiscreteDomain.integers()), Integer.class));
        assertArrayEquals(ArrayUtil.toArray(1), ArrayUtil.toArray(Range.singleton(1).iterator(DiscreteDomain.integers()), Integer.class));
    }

}

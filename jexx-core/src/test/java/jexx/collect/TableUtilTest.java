package jexx.collect;

import jexx.random.RandomUtil;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author jeff
 * @since 2019/10/28
 */
public class TableUtilTest {

    @Test
    public void testCreateTable(){
        Table<Integer, Integer, String> table = TableUtil.createArrayTable(10, 10);
        for(int i = 0; i < 10; i++){
            for(int j = 0; j < 10; j++){
                table.put(i+1, j+1, Integer.toString(i+1).concat("_").concat(Integer.toString(j+1)));
            }
        }

        Assert.assertTrue(table.containsKey(1, 3));
        Assert.assertTrue(table.containsRowKey(1));
        Assert.assertFalse(table.containsRowKey(0));
        Assert.assertEquals("1_2", table.get(1,2));
        Assert.assertEquals("10_10", table.get(10,10));
    }

}

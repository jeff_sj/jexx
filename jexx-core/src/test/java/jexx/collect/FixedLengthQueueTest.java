package jexx.collect;

import org.junit.Assert;
import org.junit.Test;

public class FixedLengthQueueTest {

    @Test
    public void testFixedLength(){
        FixedLengthQueue<String> queue = new FixedLengthQueue<>(2);
        queue.offer("a");
        Assert.assertEquals(1, queue.size());
        queue.offer("b");
        Assert.assertEquals(2, queue.size());
        queue.offer("c");
        Assert.assertEquals(2, queue.size());
    }

}

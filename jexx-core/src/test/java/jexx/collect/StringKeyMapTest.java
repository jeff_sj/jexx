package jexx.collect;

import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;

public class StringKeyMapTest {

    @Test
    public void testGetValue(){
        StringKeyMap map = new LinkedStringKeyMap();
        map.put("x", null);
        map.put("a", (byte)1);
        map.put("b", '2');
        map.put("c", (short)3);
        map.put("d", 4);
        map.put("e", 5L);
        map.put("f", 6d);
        map.put("g", "7");
        map.put("amount1", 13.91);
        map.put("amount2", 13.912);
        map.put("amount3", "13.92");
        map.put("amount4", "13.921");

        Bean1 bean1 = new Bean1();
        bean1.setName("test");
        map.put("h", bean1);

        map.put("i", true);
        map.put("j", false);

        Assert.assertEquals((byte)1, (byte)map.getByte("a"));
        Assert.assertEquals('2', (char)map.getCharacter("b"));
        Assert.assertEquals((short)3, (short)map.getShort("c"));
        Assert.assertEquals(4, (int)map.getInteger("d"));
        Assert.assertEquals(5L, (long)map.getLong("e"));
        Assert.assertEquals( 6d, map.getDouble("f"), 0.1);
        Assert.assertEquals( "7", map.getString("g"));
        Assert.assertEquals(new BigDecimal("13.91"), map.getBigDecimal("amount1"));
        Assert.assertEquals(new BigDecimal("13.912"), map.getBigDecimal("amount2"));
        Assert.assertEquals(new BigDecimal("13.92"), map.getBigDecimal("amount3"));
        Assert.assertEquals(new BigDecimal("13.921"), map.getBigDecimal("amount4"));

        Bean1 bean2 = map.getObject("h", Bean1.class);
        Assert.assertNotNull(bean2);
        Assert.assertEquals("test", bean2.getName());
        Assert.assertEquals(bean1, bean2);

        try {
            map.getInteger("g");
        }
        catch (ClassCastException e){
            //skip
        }

        Assert.assertNull(map.getInteger("g", false));

        Assert.assertTrue(map.getBoolean("i"));
        Assert.assertFalse(map.getBoolean("j"));
        Assert.assertNull(map.getBoolean("x"));
    }

    private static class Bean1 {

        private String name;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

}

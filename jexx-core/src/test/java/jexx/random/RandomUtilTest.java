package jexx.random;

import jexx.util.Console;
import org.junit.Assert;
import org.junit.Test;

public class RandomUtilTest {

    @Test
    public void test(){
        //uuid
        RandomUtil.randomUUID();
        RandomUtil.simpleUUID();

        //
        String str = RandomUtil.random(10, new char[]{'a'});
        Console.log(str);
        Assert.assertEquals(10, str.length());
        str = RandomUtil.random(10, "a");
        Console.log(str);
        Assert.assertEquals(10, str.length());
        str = RandomUtil.random(10, 'a', 'b');
        Console.log(str);
        Assert.assertEquals(10, str.length());
        str = RandomUtil.randomAscii(10);
        Console.log(str);
        Assert.assertEquals(10, str.length());
        str = RandomUtil.randomNumeric(10);
        Assert.assertEquals(10, str.length());
        str = RandomUtil.randomRanges(10, 'a', 'b');
        Console.log(str);
        Assert.assertEquals(10, str.length());
        str = RandomUtil.randomAlpha(10);
        Assert.assertEquals(10, str.length());
        str = RandomUtil.randomAlphaNumeric(10);
        Console.log(str);
        Assert.assertEquals(10, str.length());
    }

}

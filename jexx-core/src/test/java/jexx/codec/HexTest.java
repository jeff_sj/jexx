package jexx.codec;

import jexx.util.Console;
import jexx.util.StringUtil;
import org.junit.Assert;
import org.junit.Test;

public class HexTest {

    @Test
    public void test(){
        String str = "abcdeg你好";
        String hex = Hex.encodeHexString(StringUtil.getBytes(str));
        Console.log(hex);
        Assert.assertEquals("616263646567e4bda0e5a5bd", hex);
    }

}

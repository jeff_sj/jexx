package jexx.lang;

import jexx.util.ClassUtil;
import jexx.util.ReflectUtil;

import java.io.Serializable;
import java.lang.invoke.SerializedLambda;
import java.lang.reflect.Method;
import java.util.function.Function;

/**
 * 序列化lambda行数function
 */
@FunctionalInterface
public interface SerializedFunction<T,R> extends Function<T,R>, Serializable {

    default SerializedLambda getSerializableLambda(){
        try {
            Method method = ReflectUtil.getMethodByName(this.getClass(), "writeReplace");
            if(method == null){
                throw new UnsupportedOperationException("class[".concat(this.getClass().toString()).concat("]")
                    .concat("no method \"writeReplace\""));
            }
            method.setAccessible(true);
            return (SerializedLambda)method.invoke(this);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 获取lambda函数所对应的类名称
     */
    default String getImplClassName(){
        String className = getSerializableLambda().getImplClass();
        if(className != null && className.contains("/")){
            className = className.replace('/', '.');
        }
        return className;
    }

    /**
     * 获取lambda函数所对应的类
     */
    default Class<T> getImplClass(){
        return ClassUtil.loadClass(getImplClassName());
    }

    /**
     * 获取lambda函数所对应的方法名称
     */
    default String getImplMethodName(){
        return getSerializableLambda().getImplMethodName();
    }

    /**
     * 获取lambda函数所对应的方法
     */
    default Method getImplMethod(){
        Class<T> clazz = getImplClass();
        String methodName = getImplMethodName();
        return ReflectUtil.getMethodByName(clazz, methodName);
    }

}

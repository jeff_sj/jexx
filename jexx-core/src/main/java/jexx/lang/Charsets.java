package jexx.lang;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

/**
 * 字符编码集合
 * @author Jeff
 */
public final class Charsets {

	private Charsets() {
	    throw new AssertionError("No jexx.lang.Charsets instances for you!");
	}

	public static final Charset US_ASCII = StandardCharsets.US_ASCII;
	
	public static final Charset ISO_8859_1 = StandardCharsets.ISO_8859_1;
	
	public static final Charset UTF_8 = StandardCharsets.UTF_8;
	
	public static final Charset UTF_16BE = StandardCharsets.UTF_16BE;
	
	public static final Charset UTF_16LE = StandardCharsets.UTF_16LE;
	
	public static final Charset UTF_16 = StandardCharsets.UTF_16;

    public static Charset toCharset(final String charset) {
        return charset == null ? Charset.defaultCharset() : Charset.forName(charset);
    }

    public static String name(Charset charset){
        return charset != null ? charset.name() : defaultCharsetName();
    }

    public static Charset defaultCharset(){
        return Charset.defaultCharset();
    }

    public static String defaultCharsetName(){
        return defaultCharset().name();
    }
	
}

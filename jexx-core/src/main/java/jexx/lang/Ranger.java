package jexx.lang;

import jexx.util.Assert;

import java.util.Iterator;
import java.util.Objects;

/**
 * 范围迭代器
 * @author jeff
 */
public class Ranger<T extends Comparable<T>> implements Iterable<T>, Iterator<T> {

    private T start;
    private T end;
    private T current;
    private T next;
    private int index = -1;
    /** 是否包含第一个元素 */
    private boolean includeStart = true;
    /** 是否包含最后一个元素 */
    private boolean includeEnd = true;
    /** 步进器 */
    private RangerSteper<T> steper;
    /** range生成的值是否是递增的 */
    private boolean ascending = true;

    public Ranger(T start, T end, RangerSteper<T> steper) {
        this(start,end, steper,true,true);
    }

    public Ranger(T start, T end, RangerSteper<T> steper, boolean includeStart, boolean includeEnd) {
        Objects.requireNonNull(start);
        Objects.requireNonNull(end);
        Assert.isTrue(start.compareTo(end) != 0, "start element is not equal to the end element");
        this.start = start;
        this.end = end;
        this.steper = steper;
        this.includeStart = includeStart;
        this.includeEnd = includeEnd;
        this.ascending = start.compareTo(end) < 0;
    }

    @Override
    public Iterator<T> iterator() {
        return this;
    }

    @Override
    public boolean hasNext() {
        if(current == null){
            if(includeStart){
                this.next = this.start;
                this.index = -1;
            }
            else{
                this.current = this.start;
                this.index = -1;
                this.next = getNextStep();
            }
        }
        else{
            if(crossEnd(this.current)){
                return false;
            }
        }
        return this.next != null;
    }

    @Override
    public T next() {
        Assert.isTrue(next != null, "no more element!");
        current = next;
        index++;
        if(!crossEnd(current)){
            next = getNextStep();
            if(next != null){
                if(includeEnd){
                    if(crossEnd(next)){
                        next = end;
                    }
                }
                else{
                    if(crossEnd(next)){
                        next = null;
                    }
                }
            }
        }
        return current;
    }

    private T getNextStep(){
        return steper.step(this.current, this.index);
    }

    private boolean crossEnd(T value){
        if(ascending){
            return value.compareTo(this.end) >= 0;
        }
        else{
            return value.compareTo(this.end) <= 0;
        }
    }

    /**
     * 范围步进器
     * @param <T> 泛型
     */
    public interface RangerSteper<T>{
        /**
         * 获取下一个值;请确保值是渐变的,否则抛出异常
         * @param current 当前值
         * @param index 索引; 若包含start,则index从0开始;若不包含,则从-1开始
         * @return 下一个值
         */
        T step(T current, int index);

    }

}

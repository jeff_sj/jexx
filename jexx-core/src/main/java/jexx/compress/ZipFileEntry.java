package jexx.compress;

import java.io.File;
import java.net.URL;

/**
 * zip文件文件清单
 * @author jeff
 */
public class ZipFileEntry implements Cloneable {

    /** 当前的文件条目名称 */
    private String name;
    /** 最开始的文件条目名称 */
    private String originName;
    /** 是否为目录 */
    private boolean dir;

    /** 是否追加的文件 */
    private boolean append;
    /** 追加的模式 */
    private int appendMode;
    /** 追加的文件 */
    private File appendFile;
    /** 追加的URL */
    private URL appendUrl;
    /** 追加的字节数组 */
    private byte[] appendBytes;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOriginName() {
        return originName;
    }

    public void setOriginName(String originName) {
        this.originName = originName;
    }

    public boolean isDir() {
        return dir;
    }

    public void setDir(boolean dir) {
        this.dir = dir;
    }

    public boolean isAppend() {
        return append;
    }

    public void setAppend(boolean append) {
        this.append = append;
    }

    public File getAppendFile() {
        return appendFile;
    }

    public void setAppendFile(File appendFile) {
        this.appendFile = appendFile;
    }

    public int getAppendMode() {
        return appendMode;
    }

    public void setAppendMode(int appendMode) {
        this.appendMode = appendMode;
    }

    public URL getAppendUrl() {
        return appendUrl;
    }

    public void setAppendUrl(URL appendUrl) {
        this.appendUrl = appendUrl;
    }

    public byte[] getAppendBytes() {
        return appendBytes;
    }

    public void setAppendBytes(byte[] appendBytes) {
        this.appendBytes = appendBytes;
    }

    @Override
    protected ZipFileEntry clone() {
        ZipFileEntry entry = new ZipFileEntry();
        entry.setName(name);
        entry.setOriginName(originName);
        entry.setDir(dir);
        entry.setAppend(append);
        entry.setAppendMode(appendMode);
        entry.setAppendFile(appendFile);
        entry.setAppendUrl(appendUrl);
        entry.setAppendBytes(appendBytes);
        return entry;
    }
}

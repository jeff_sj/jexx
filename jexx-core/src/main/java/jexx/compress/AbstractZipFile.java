package jexx.compress;

import jexx.exception.IORuntimeException;
import jexx.exception.UtilException;
import jexx.io.FileNameUtil;
import jexx.io.FileUtil;
import jexx.io.IOUtil;
import jexx.util.Assert;
import jexx.util.StringUtil;

import java.io.*;
import java.net.URL;
import java.util.*;
import java.util.stream.Collectors;

public abstract class AbstractZipFile<I extends InputStream, O extends OutputStream> {

    /** 追加文件 */
    public static final int ZIP_FILE = 1;
    /** 追加URL */
    public static final int ZIP_URL = 2;
    /** 追加字节数组 */
    public static final int ZIP_BYTES = 3;

    private int baseZipMode = 0;
    private File baseZipFile;
    private URL baseZipURL;
    private byte[] baseZipBytes;

    /**
     * 基础压缩对象的条目信息
     */
    private final List<ZipFileEntry> baseZipEntries = new ArrayList<>();

    /** 压缩文件中文件条目, key为当前文件条目, value为文件条目具体信息 */
    private final Map<String, ZipFileEntry> zipFileEntries = new HashMap<>();

    public AbstractZipFile() {}

    /**
     * 基于已有zip文件构建的ZipFile
     * @param zipFile 已有的压缩文件
     */
    public AbstractZipFile(File zipFile) {
        Objects.requireNonNull(zipFile);
        if(!zipFile.exists()){
            throw new IORuntimeException("file[{}] not exist!", zipFile.getName());
        }
        this.baseZipMode = ZIP_FILE;
        this.baseZipFile = zipFile;
        scanEntry(FileUtil.newBufferedInputStream(zipFile));
    }

    public AbstractZipFile(URL zipURL) {
        Objects.requireNonNull(zipURL);
        this.baseZipMode = ZIP_URL;
        this.baseZipURL = zipURL;
        try {
            scanEntry(zipURL.openStream());
        } catch (IOException e) {
            throw new IORuntimeException(e);
        }
    }

    public AbstractZipFile(InputStream inputStream) {
        Objects.requireNonNull(inputStream);
        try {
            this.baseZipMode = ZIP_BYTES;
            this.baseZipBytes = IOUtil.readBytes(inputStream);
        }
        finally {
            IOUtil.closeQuietly(inputStream);
        }
    }

    /**
     * 基础zip压缩文件流
     */
    protected InputStream getBaseZipInputStream() throws IOException {
        if(baseZipMode == ZIP_FILE){
            return FileUtil.newBufferedInputStream(baseZipFile);
        }
        else if(baseZipMode == ZIP_URL){
            return baseZipURL.openStream();
        }
        else if(baseZipMode == ZIP_BYTES){
            return new ByteArrayInputStream(baseZipBytes);
        }
        else{
            return null;
        }
    }

    /**
     * 获取追加条目的流
     * @param appendEntryName 条目名称
     */
    protected InputStream getInputStreamOfAppendEntry(String appendEntryName) throws IOException {
        ZipFileEntry entry = zipFileEntries.get(appendEntryName);
        if(entry == null || entry.isDir() || !entry.isAppend()){
            return null;
        }

        InputStream inputStream;
        if(entry.getAppendMode() == ZIP_FILE){
            inputStream = FileUtil.newBufferedInputStream(entry.getAppendFile());
        }
        else if(entry.getAppendMode() == ZIP_URL){
            inputStream = entry.getAppendUrl().openStream();
        }
        else if(entry.getAppendMode() == ZIP_BYTES){
            inputStream = new ByteArrayInputStream(entry.getAppendBytes());
        }
        else{
            throw new UtilException("no support mode={}", entry.getAppendMode());
        }
        return inputStream;
    }

    /**
     * 打开压缩文件,读取结构
     */
    private void scanEntry(InputStream inputStream){
        try {
            List<ZipFileEntry> entries = doScanEntry(inputStream);
            for (ZipFileEntry entry : entries){
                zipFileEntries.put(entry.getName(), entry);
                baseZipEntries.add(entry.clone());
            }
        }
        finally {
            IOUtil.closeQuietly(inputStream);
        }
    }

    /**
     * 获取当前对象的条目信息
     */
    protected List<ZipFileEntry> getZipFileEntry(){
        return new ArrayList<>(zipFileEntries.values());
    }

    /**
     * 获取基础压缩对象的条目结构, 非当前对象的条目结构
     */
    public List<ZipFileEntry> getBaseZipEntries(){
        return baseZipEntries;
    }

    /**
     * 根据文件名称获取压缩包中的条目
     * @param name 文件名称
     * @return 压缩文件条目
     */
    public ZipFileEntry getEntryByName(String name){
        for(ZipFileEntry entry : this.baseZipEntries){
            if(entry.getName().equals(name)){
                return entry;
            }
        }
        return null;
    }

    /**
     * zip读取条目，子类可覆盖该实现
     * @param inputStream 输入流
     * @return 压缩包文件清单
     */
    protected abstract List<ZipFileEntry> doScanEntry(InputStream inputStream);

    /**
     * 移动条目到另一个位置
     * @param srcName 移动前的条目名称
     * @param toName 移动后的条目名称
     */
    public void move(String srcName, String toName){
        checkEntryNotExist(toName);
        ZipFileEntry entry = checkEntryExist(srcName);
        zipFileEntries.remove(srcName);
        entry.setName(toName);
        zipFileEntries.put(toName, entry);
    }

    /**
     * 拷贝文件
     * @param srcName 源条目
     * @param toName 目标条目
     */
    public void copy(String srcName, String toName){
        checkEntryNotExist(toName);
        ZipFileEntry entry = checkEntryExist(srcName);

        ZipFileEntry zipFileEntry = entry.clone();
        zipFileEntry.setOriginName(entry.getOriginName());
        zipFileEntry.setName(toName);
        if(entry.isAppend()){
            zipFileEntry.setAppend(true);
            zipFileEntry.setAppendFile(entry.getAppendFile());
        }
        zipFileEntries.put(toName, zipFileEntry);
    }

    /**
     * 删除条目
     * @param srcName 源条目
     */
    public void delete(String srcName){
        checkEntryExist(srcName);
        zipFileEntries.remove(srcName);
    }

    /**
     * 对zip追加文件
     * @param file put文件
     * @param toName 追加到zip中的条目名称
     */
    public void append(File file, String toName){
        if(!file.isFile()){
            throw new IllegalArgumentException(StringUtil.format("File[{}] is not a file!", file.getPath()));
        }
        checkEntryNotExist(toName);

        ZipFileEntry zipFileEntry = new ZipFileEntry();
        zipFileEntry.setOriginName(toName);
        zipFileEntry.setName(toName);
        zipFileEntry.setAppend(true);
        zipFileEntry.setAppendMode(ZIP_FILE);
        zipFileEntry.setAppendFile(file);
        zipFileEntries.put(toName, zipFileEntry);
    }

    /**
     * 对zip追加URL
     * @param url URL
     * @param toName 追加到zip中的条目名称
     */
    public void append(URL url, String toName){
        Objects.requireNonNull(url);
        checkEntryNotExist(toName);

        ZipFileEntry zipFileEntry = new ZipFileEntry();
        zipFileEntry.setOriginName(toName);
        zipFileEntry.setName(toName);
        zipFileEntry.setAppend(true);
        zipFileEntry.setAppendMode(ZIP_URL);
        zipFileEntry.setAppendUrl(url);
        zipFileEntries.put(toName, zipFileEntry);
    }

    /**
     * 对zip追加流
     * @param inputStream 流
     * @param toName 追加到zip中的条目名称
     */
    public void append(InputStream inputStream, String toName){
        Objects.requireNonNull(inputStream);
        checkEntryNotExist(toName);

        try {
            byte[] bytes = IOUtil.readBytes(inputStream);
            append(bytes, toName);
        }
        finally {
            IOUtil.closeQuietly(inputStream);
        }
    }

    /**
     * 对zip追加 字节
     * @param bytes 字节
     * @param toName 追加到zip中的条目名称
     */
    public void append(byte[] bytes, String toName){
        Assert.notEmpty(bytes, "bytes not empty!");
        checkEntryNotExist(toName);

        ZipFileEntry zipFileEntry = new ZipFileEntry();
        zipFileEntry.setOriginName(toName);
        zipFileEntry.setName(toName);
        zipFileEntry.setAppend(true);
        zipFileEntry.setAppendMode(ZIP_BYTES);
        zipFileEntry.setAppendBytes(bytes);
        zipFileEntries.put(toName, zipFileEntry);
    }

    /**
     * 添加目录
     * @param dirName 目录
     */
    public void appendDir(String dirName){
        if(!StringUtil.endWith(dirName, "/")){
            dirName = dirName.concat("/");
        }

        ZipFileEntry zipFileEntry = new ZipFileEntry();
        zipFileEntry.setOriginName(dirName);
        zipFileEntry.setName(dirName);
        zipFileEntry.setDir(true);
        zipFileEntry.setAppend(true);
        zipFileEntries.put(dirName, zipFileEntry);
    }


    /**
     * 添加目录以及子文件
     * @param dir 目录
     * @param filter 过滤
     */
    public void appendDirAndChildren(File dir, FileFilter filter){
        if(!dir.isDirectory()){
            throw new IllegalArgumentException("File[{}] is not a dir!");
        }
        String parentName = FileNameUtil.getParentName(dir.getPath());
        _appendDirAndChildren(dir, parentName, filter);
    }

    /**
     * 添加目录以及子文件
     * @see AbstractZipFile#appendDirAndChildren(File, FileFilter)
     * @param dir 目录
     */
    public void appendDirAndChildren(File dir){
        appendDirAndChildren(dir, null);
    }

    /**
     * 添加目录以及子文件
     * @see AbstractZipFile#appendDirAndChildren(File, FileFilter)
     * @param dir 目录
     * @param filter 过滤
     */
    public void appendDirAndChildren(String dir, FileFilter filter){
        appendDirAndChildren(FileUtil.file(dir), filter);
    }

    /**
     * 添加目录以及子文件
     * @see AbstractZipFile#appendDirAndChildren(String, FileFilter)
     * @param dir 目录
     */
    public void appendDirAndChildren(String dir){
        appendDirAndChildren(dir, null);
    }


    private void _appendDirAndChildren(File file, String rootDir, FileFilter filter){
        if(filter != null && !filter.accept(file)){
            return;
        }
        String subPath = FileNameUtil.relativePath(file.getPath(), rootDir);
        if(file.isDirectory()){
            appendDir(subPath);
            File[] children = file.listFiles();
            if(children != null){
                for(File child : children){
                    _appendDirAndChildren(child, rootDir, filter);
                }
            }
        }
        else if(file.isFile()){
            append(file, subPath);
        }
    }


    /**
     * 获取压缩输入流,用于把普通流包装成压缩流; 普通流原本就是压缩流
     */
    protected abstract I getZipInputStream(InputStream inputStream) throws IOException;

    /**
     * 获取压缩输出流
     */
    protected abstract O getZipOutputStream(File compressedFile) throws IOException;

    //---------------------------------------------------压缩

    /**
     * 输出压缩文件
     * @param outputFile 压缩文件
     * @return 输出后的压缩文件
     */
    public final File zip(File outputFile){
        InputStream inputStream = null;
        I zipInputStream = null;
        O zipOutputStream = null;
        try {
            inputStream = getBaseZipInputStream();
            zipOutputStream = getZipOutputStream(outputFile);
            if(inputStream != null){
                zipInputStream = getZipInputStream(inputStream);

                List<ZipFileEntry> zipFileEntries = getZipFileEntry().stream()
                        .filter(s -> !s.isAppend())
                        .collect(Collectors.toList());
                doZipBase(zipInputStream, zipOutputStream, zipFileEntries);
                IOUtil.closeQuietly(inputStream, zipInputStream);
            }

            List<ZipFileEntry> zipFileEntries = getZipFileEntry().stream()
                    .filter(ZipFileEntry::isAppend)
                    .collect(Collectors.toList());
            for(ZipFileEntry zipFileEntry : zipFileEntries){
                if(!zipFileEntry.isAppend()){
                    continue;
                }
                inputStream = getInputStreamOfAppendEntry(zipFileEntry.getName());
                doZipAppend(inputStream, zipOutputStream, zipFileEntry);
                IOUtil.closeQuietly(inputStream);
            }
        }
        catch (IOException e){
            throw new IORuntimeException(e);
        }
        finally {
            IOUtil.closeQuietly(inputStream, zipInputStream, zipOutputStream);
        }
        return outputFile;
    }

    /**
     * 输出压缩文件
     * @see AbstractZipFile#zip(File)
     * @param outputFile 压缩文件
     * @return 输出后的压缩文件
     */
    public final File zip(String outputFile){
        return zip(FileUtil.file(outputFile));
    }

    /**
     * 压缩过程对基础压缩对象的处理
     * @param inputStream 基础压缩对象的流
     * @param outputStream 压缩输出流
     * @param baseZipFileEntries 操作后和基础压缩相关的条目集合
     */
    protected abstract void doZipBase(I inputStream, O outputStream, List<ZipFileEntry> baseZipFileEntries) throws IOException;

    /**
     * 压缩过程对添加对象的处理
     * @param inputStream 添加对象的流; 表示目录时, 流为空
     * @param outputStream 压缩输出流
     * @param entry 操作的当前条目信息
     */
    protected abstract void doZipAppend(InputStream inputStream, O outputStream, ZipFileEntry entry) throws IOException;

    //---------------------------------------------------解压

    /**
     * 解压文件到目录
     * @param outputDir 输出目录
     */
    public final void unzip(File outputDir){
        InputStream inputStream = null;
        I zipInputStream = null;
        String dir = outputDir.getPath();
        try {
            inputStream = getBaseZipInputStream();
            if(inputStream != null){
                zipInputStream = getZipInputStream(inputStream);

                List<ZipFileEntry> zipFileEntries = getZipFileEntry().stream()
                        .filter(s -> !s.isAppend())
                        .collect(Collectors.toList());
                doUnzipBase(zipInputStream, zipFileEntries, dir);
                IOUtil.closeQuietly(inputStream, zipInputStream);
            }

            List<ZipFileEntry> zipFileEntries = getZipFileEntry().stream()
                    .filter(ZipFileEntry::isAppend)
                    .collect(Collectors.toList());
            for(ZipFileEntry zipFileEntry : zipFileEntries){
                if(!zipFileEntry.isAppend()){
                    continue;
                }
                inputStream = getInputStreamOfAppendEntry(zipFileEntry.getName());
                if(inputStream != null){
                    doUnzipAppend(inputStream, zipFileEntry, dir);
                    IOUtil.closeQuietly(inputStream);
                }
            }
        }
        catch (IOException e){
            throw new IORuntimeException(e);
        }
        finally {
            IOUtil.closeQuietly(inputStream, zipInputStream);
        }
    }

    /**
     * 解压文件到目录
     * @see AbstractZipFile#unzip(File)
     * @param outputDir 输出目录
     */
    public final void unzip(String outputDir){
        unzip(FileUtil.file(outputDir));
    }


    /**
     * 解压基础压缩对象
     * @param inputStream 基础压缩对象的流
     * @param baseZipFileEntries 压缩
     * @param dir 解压到的目录
     */
    protected abstract void doUnzipBase(I inputStream, List<ZipFileEntry> baseZipFileEntries, String dir) throws IOException;


    /**
     * 解压追加压缩对象
     * @param inputStream 追加压缩流
     * @param entry 压缩文件中追加的条目
     * @param dir 解压目录
     */
    protected abstract void doUnzipAppend(InputStream inputStream, ZipFileEntry entry, String dir) throws IOException;

    //---------------------------------------------------private

    private ZipFileEntry checkEntryExist(String name){
        ZipFileEntry entry = zipFileEntries.get(name);
        if(entry == null){
            throw new IllegalArgumentException(StringUtil.format("{} not exist in zip!", name));
        }
        return entry;
    }

    private void checkEntryNotExist(String name){
        ZipFileEntry entry = zipFileEntries.get(name);
        if(entry != null){
            throw new IllegalArgumentException(StringUtil.format("{} exist in zip!", name));
        }
    }

}

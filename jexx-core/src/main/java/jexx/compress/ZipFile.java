package jexx.compress;

import jexx.exception.IORuntimeException;
import jexx.io.FileUtil;
import jexx.io.IOUtil;
import jexx.lang.Charsets;
import jexx.util.CollectionUtil;
import jexx.util.StringUtil;

import java.io.*;
import java.net.URL;
import java.util.*;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

/**
 * 压缩文件
 * <pre>
 *     实现压缩文件中文件的移动,拷贝,删除等功能
 *     实现往压缩文件中添加新文件
 * </pre>
 */
public class ZipFile extends AbstractZipFile<ZipInputStream, ZipOutputStream> {

    public ZipFile() {}

    public ZipFile(File zipFile) {
       super(zipFile);
    }

    public ZipFile(URL zipURL) {
        super(zipURL);
    }

    public ZipFile(InputStream inputStream) {
        super(inputStream);
    }

    @Override
    protected List<ZipFileEntry> doScanEntry(InputStream inputStream){
        ZipInputStream zipInputStream = null;
        try {
            zipInputStream = new ZipInputStream(inputStream, Charsets.UTF_8);
            ZipEntry zipEntry;
            List<ZipFileEntry> zipFileEntries = new ArrayList<>();
            while ((zipEntry = zipInputStream.getNextEntry()) != null){
                ZipFileEntry zipFileEntry = new ZipFileEntry();
                zipFileEntry.setOriginName(zipEntry.getName());
                zipFileEntry.setName(zipEntry.getName());
                zipFileEntry.setDir(zipEntry.isDirectory());
                zipFileEntries.add(zipFileEntry);

                zipInputStream.closeEntry();
            }
            return zipFileEntries;
        }
        catch (IOException e){
            throw new IORuntimeException(e);
        }
        finally {
            IOUtil.closeQuietly(zipInputStream);
        }
    }

    @Override
    protected ZipInputStream getZipInputStream(InputStream inputStream){
        return new ZipInputStream(inputStream, Charsets.UTF_8);
    }

    /**
     * 压缩文件输出流2
     * @param compressedFile 压缩文件
     */
    @Override
    protected ZipOutputStream getZipOutputStream(File compressedFile) throws IOException {
        return new ZipOutputStream(new FileOutputStream(compressedFile));
    }

    @Override
    protected void doZipBase(ZipInputStream inputStream, ZipOutputStream outputStream, List<ZipFileEntry> baseZipFileEntries) throws IOException {
        Map<String, List<ZipFileEntry>> zipFileEntryMapWithOld = baseZipFileEntries.stream()
                .filter(s -> !s.isAppend())
                .collect(Collectors.groupingBy(ZipFileEntry::getOriginName));

        ZipEntry zipEntry;
        while ((zipEntry = inputStream.getNextEntry()) != null){
            List<ZipFileEntry> zipFileEntries = zipFileEntryMapWithOld.get(zipEntry.getName());
            if(CollectionUtil.isNotEmpty(zipFileEntries)){
                if(zipFileEntries.size() == 1){
                    ZipFileEntry zipFileEntry = zipFileEntries.get(0);
                    createEntry(zipFileEntry, outputStream);
                    if(!zipFileEntry.isDir()){
                        IOUtil.copy(inputStream, outputStream);
                    }
                    outputStream.closeEntry();
                }
                else{
                    byte[] bytes = IOUtil.readBytes(inputStream);
                    for(ZipFileEntry zipFileEntry : zipFileEntries){
                        createEntry(zipFileEntry, outputStream);
                        if(!zipFileEntry.isDir()){
                            outputStream.write(bytes);
                        }
                        outputStream.closeEntry();
                    }
                }
            }
            inputStream.closeEntry();
        }
    }

    @Override
    protected void doZipAppend(InputStream inputStream, ZipOutputStream outputStream, ZipFileEntry zipFileEntry) throws IOException{
        createEntry(zipFileEntry, outputStream);
        if(!zipFileEntry.isDir()){
            IOUtil.copy(inputStream, outputStream);
        }
        outputStream.closeEntry();
    }

    private void createEntry(ZipFileEntry zipFileEntry, ZipOutputStream outputStream) throws IOException {
        String name = zipFileEntry.getName();
        if(zipFileEntry.isDir()){
            if(!StringUtil.endWith(zipFileEntry.getName(), "/")){
                name = name.concat("/");
            }
        }
        outputStream.putNextEntry(new ZipEntry(name));
    }

    @Override
    protected void doUnzipBase(ZipInputStream inputStream, List<ZipFileEntry> baseZipFileEntries, String dir) throws IOException {
        Map<String, List<ZipFileEntry>> zipFileEntryMapWithOld = baseZipFileEntries.stream()
                .filter(s -> !s.isAppend())
                .collect(Collectors.groupingBy(ZipFileEntry::getOriginName));

        ZipEntry zipEntry;
        try {
            while ((zipEntry = inputStream.getNextEntry()) != null){
                List<ZipFileEntry> zipFileEntries = zipFileEntryMapWithOld.get(zipEntry.getName());
                if(CollectionUtil.isNotEmpty(zipFileEntries)){
                    if(zipFileEntries.size() == 1){
                        ZipFileEntry zipFileEntry = zipFileEntries.get(0);
                        File file = FileUtil.file(dir, zipFileEntry.getName());
                        if(zipEntry.isDirectory()){
                            FileUtil.mkdirs(file);
                        }
                        else{
                            FileUtil.mkdirs(file.getParent());
                            FileUtil.write(file, inputStream);
                        }
                    }
                    else{
                        byte[] bytes = IOUtil.readBytes(inputStream);
                        for(ZipFileEntry entry : zipFileEntries){
                            File file = FileUtil.file(dir, entry.getName());
                            if(zipEntry.isDirectory()){
                                FileUtil.mkdirs(file);
                            }
                            else {
                                FileUtil.mkdirs(file.getParent());
                                FileUtil.write(file, bytes);
                            }
                        }
                    }
                }
                inputStream.closeEntry();
            }
        }
        finally {
            IOUtil.closeQuietly(inputStream);
        }
    }

    @Override
    protected void doUnzipAppend(InputStream inputStream, ZipFileEntry zipFileEntry, String dir) throws IOException {
        File file = FileUtil.file(dir, zipFileEntry.getName());
        if(zipFileEntry.isDir()){
            FileUtil.mkdirs(file);
        }
        else{
            FileUtil.mkdirs(file.getParent());
            FileUtil.write(file, inputStream);
        }
    }

}

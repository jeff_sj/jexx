package jexx.compress;

import jexx.io.FileUtil;
import jexx.util.Assert;

import java.io.File;
import java.io.FileFilter;

public class ZipUtil {

    /**
     * zip压缩文件
     * @param srcFile 源文件或者源目录
     * @param compressedFile 压缩后的文件
     * @param fileFilter 源文件过滤
     * @return 压缩后的文件
     */
    public static File zip(String srcFile, String compressedFile, FileFilter fileFilter){
        ZipFile zipFile = new ZipFile();
        zipFile.appendDirAndChildren(srcFile, fileFilter);
        return zipFile.zip(compressedFile);
    }

    /**
     * zip压缩文件
     * @param srcFile 源文件或者源目录
     * @param compressedFile 压缩后的文件
     * @return 压缩后的文件
     */
    public static File compressZip(String srcFile, String compressedFile){
        return zip(srcFile, compressedFile, null);
    }

    /**
     * zip压缩文件
     * @param srcFile 源文件或者源目录
     * @param compressedFile 压缩后的文件
     * @param fileFilter 源文件过滤
     * @return 压缩后的文件
     */
    public static File zip(File srcFile, File compressedFile, FileFilter fileFilter){
        ZipFile zipFile = new ZipFile();
        zipFile.appendDirAndChildren(srcFile, fileFilter);
        return zipFile.zip(compressedFile);
    }

    /**
     * zip压缩文件
     * @param srcFile 源文件或者源目录
     * @param compressedFile 压缩后的文件
     * @return 压缩后的文件
     */
    public static File zip(File srcFile, File compressedFile){
        return zip(srcFile, compressedFile, null);
    }

    /**
     * zip压缩多个文件
     * @param srcFiles 多个源文件或者源目录
     * @param compressedFile 压缩后的文件
     * @return 压缩后的文件
     */
    public static File zip(String[] srcFiles, String compressedFile){
        ZipFile zipFile = new ZipFile();
        for(String srcFile : srcFiles){
            zipFile.appendDirAndChildren(srcFile);
        }
        return zipFile.zip(compressedFile);
    }

    /**
     * zip压缩多个文件
     * @param srcFiles 多个源文件或者源目录
     * @param compressedFile 压缩后的文件
     * @return 压缩后的文件
     */
    public static File zip(File[] srcFiles, File compressedFile){
        ZipFile zipFile = new ZipFile();
        for(File srcFile : srcFiles){
            zipFile.appendDirAndChildren(srcFile);
        }
        return zipFile.zip(compressedFile);
    }

    /**
     * 解压文件到目录
     * @param zipFile zip压缩文件
     * @param outputDir 目录
     */
    public static void unzip(File zipFile, File outputDir){
        Assert.isTrue(zipFile != null && zipFile.isFile(), "It's not a file!");
        Assert.isTrue(outputDir != null && outputDir.isDirectory(), "It's not a dir!");

        ZipFile zf = new ZipFile(zipFile);
        zf.unzip(outputDir);
    }

    /**
     * @see ZipUtil#unzip(File, File)
     */
    public static void unzip(String zipFile, String outputDir){
        unzip(FileUtil.file(zipFile), FileUtil.file(outputDir));
    }

}

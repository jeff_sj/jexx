package jexx.net;

import jexx.io.IOUtil;
import jexx.util.ResourceUtil;
import jexx.util.StringUtil;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;

/**
 * 媒体类型工具集合。 MimeType 简单理解为 服务器中文件扩展名对应的 ContentType
 * <b>参考 jodd, 以及 spring 的 ConfigurableMimeFileTypeMap </b>
 * @author Jeff
 */
public class MimeTypes {

	public static final String MIME_APPLICATION_ATOM_XML 		= "application/atom+xml";
	public static final String MIME_APPLICATION_JAVASCRIPT		= "application/javascript";
	public static final String MIME_APPLICATION_JSON 			= "application/json";
	public static final String MIME_APPLICATION_OCTET_STREAM	= "application/octet-stream";
	public static final String MIME_APPLICATION_XML 			= "application/xml";
	public static final String MIME_TEXT_CSS					= "text/css";
	public static final String MIME_TEXT_PLAIN 					= "text/plain";
	public static final String MIME_TEXT_HTML					= "text/html";

	/**
	 * extension -> mime-type map
	 */
	private static final HashMap<String, String> MIME_TYPE_MAP;

	static {
		final Properties mimes = new Properties();

		final InputStream is = ResourceUtil.getStream(MimeTypes.class.getSimpleName() + ".properties");
		if (is == null) {
			throw new IllegalStateException("Mime types file missing");
		}

		try {
			mimes.load(is);
		}
		catch (IOException e) {
			throw new IllegalStateException("Can't load properties", e);
		} finally {
			IOUtil.closeQuietly(is);
		}

		MIME_TYPE_MAP = new HashMap<>(mimes.size() * 2);

		final Enumeration<?> keys = mimes.propertyNames();
		while (keys.hasMoreElements()) {
			String mimeType = (String) keys.nextElement();
			final String extensions = mimes.getProperty(mimeType);

			if (mimeType.startsWith("/")) {
				mimeType = "application" + mimeType;
			} else if (mimeType.startsWith("a/")) {
				mimeType = "audio" + mimeType.substring(1);
			} else if (mimeType.startsWith("i/")) {
				mimeType = "image" + mimeType.substring(1);
			} else if (mimeType.startsWith("t/")) {
				mimeType = "text" + mimeType.substring(1);
			} else if (mimeType.startsWith("v/")) {
				mimeType = "video" + mimeType.substring(1);
			}

			final String[] allExtensions = StringUtil.split(extensions, ' ');

			for (final String extension : allExtensions) {
				if (MIME_TYPE_MAP.put(extension, mimeType) != null) {
					throw new IllegalArgumentException("Duplicated extension: " + extension);
				}
			}
		}
	}

	/**
	 * Registers MIME type for provided extension. Existing extension type will be overridden.
	 */
	public static void registerMimeType(final String ext, final String mimeType) {
		MIME_TYPE_MAP.put(ext, mimeType);
	}

	/**
	 * Returns the corresponding MIME type to the given extension.
	 * If no MIME type was found it returns <code>application/octet-stream</code> type.
	 */
	public static String getMimeType(final String ext) {
		String mimeType = lookupMimeType(ext);
		if (mimeType == null) {
			mimeType = MIME_APPLICATION_OCTET_STREAM;
		}
		return mimeType;
	}

	/**
	 * Simply returns MIME type or <code>null</code> if no type is found.
	 */
	public static String lookupMimeType(final String ext) {
		return MIME_TYPE_MAP.get(ext.toLowerCase());
	}

	/**
	 * Returns {@code true} if given value is one of the registered MIME extensions.
	 */
	public static boolean isRegisteredExtension(final String extension) {
		return MIME_TYPE_MAP.containsKey(extension);
	}
}
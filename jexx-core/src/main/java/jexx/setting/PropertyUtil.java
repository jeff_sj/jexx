package jexx.setting;


import jexx.exception.IORuntimeException;
import jexx.io.IOUtil;
import jexx.lang.Charsets;
import jexx.template.StringTemplateParser;
import jexx.util.ResourceUtil;

import java.io.*;
import java.nio.charset.Charset;
import java.util.Map;
import java.util.Properties;

/**
 * 键值工具栏
 */
public class PropertyUtil {

    private static final StringTemplateParser stp;

    static {
        stp = new StringTemplateParser();
        stp.setParseValues(true);
    }

    public static Properties createFromFile(final String fileName) {
        return createFromFile(new File(fileName));
    }

    public static Properties createFromFile(final File file) {
        Properties prop = new Properties();
        loadFromFile(prop, file);
        return prop;
    }

    public static void loadFromFile(final Properties p, final String fileName){
        loadFromFile(p, new File(fileName));
    }

    public static void loadFromFile(final Properties p, final File file){
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(file);
            p.load(fis);
        }catch (IOException e){
            throw new IORuntimeException(e);
        }finally {
            IOUtil.closeQuietly(fis);
        }
    }

    public static Properties createFromString(final String data) throws IOException {
        Properties p = new Properties();
        loadFromString(p, data);
        return p;
    }

    public static void loadFromString(final Properties p, final String data) {
        try (ByteArrayInputStream is = new ByteArrayInputStream(data.getBytes())) {
            p.load(is);
        } catch (IOException e) {
            throw new IORuntimeException(e);
        }
    }

    /**
     * 以UTF8编码加载classpath中的资源文件
     * @param resourceName 资源文件名称
     * @return {@link Properties}
     */
    public static Properties loadFromClasspath(String resourceName) {
        return loadFromClasspath(ResourceUtil.getStream(resourceName), Charsets.UTF_8);
    }

    /**
     * 以UTF8编码加载输入流
     * @param inputStream 输入流, 会自动关闭
     * @return {@link Properties}
     */
    public static Properties loadFromClasspath(InputStream inputStream) {
        return loadFromClasspath(inputStream, Charsets.UTF_8);
    }

    /**
     * 指定编码加载classpath中的资源文件
     * @param resourceName 资源文件名称
     * @param charset 编码
     * @return {@link Properties}
     */
    public static Properties loadFromClasspath(String resourceName, Charset charset) {
        return loadFromClasspath(ResourceUtil.getStream(resourceName), charset);
    }

    public static Properties loadFromClasspath(InputStream inputStream, Charset charset) {
        Properties p = new Properties();
        return loadFromClasspath(p, inputStream, charset);
    }

    public static Properties loadFromClasspath(final Properties p, InputStream inputStream, Charset charset) {
        try {
            p.load(new InputStreamReader(inputStream, charset));
        } catch (IOException e) {
            throw new IORuntimeException(e);
        }
        finally {
            IOUtil.closeQuietly(inputStream);
        }
        return p;
    }

    // ---------------------------------------------------------------- variables

    /**
     * 解析所有变量
     * @param prop {@link Properties}
     */
    public static void resolveAllVariables(final Properties prop) {
        for (Object o : prop.keySet()) {
            String key = (String) o;
            String value = resolveProperty(prop, key);
            prop.setProperty(key, value);
        }
    }

    /**
     * 返回解析的变量
     */
    public static String resolveProperty(final Map map, final String key) {
        Object value = map.get(key);
        if (value == null) {
            return null;
        }
        String valueStr = value.toString();

        valueStr = stp.parse(valueStr, macroName -> {
            Object obj = map.get(macroName);
            return obj != null ? obj.toString() : null;
        });
        return valueStr;
    }

}

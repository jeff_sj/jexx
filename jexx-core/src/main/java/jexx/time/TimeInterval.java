package jexx.time;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * 计时器<br>
 * 计算某个过程花费的时间，以及各个任务的耗时时间
 */
public class TimeInterval {

	static final long T_MC = 1_000L;
	static final long T_MS = 1000 * T_MC;
	static final long T_SECOND = 1000 * T_MS;
	static final long T_MINUTE = 60 * T_SECOND;
	static final long T_HOUR = 60 * T_MINUTE;
	static final long T_DAY = 24 * T_HOUR;

	/** 唯一标识 */
	private final String id;

	private boolean keepTaskList = true;

	/** 开始计时时间,纳秒 */
	private long startTimeNanos;
	private String currentTaskName;
	private TaskInfo lastTaskInfo;
	private List<TaskInfo> taskInfoList = new ArrayList<>();
	/**
	 * 已停止任务的总耗时时间,纳秒
	 */
	private long totalTimeNanos;

	public TimeInterval() {
		this("");
	}

	public TimeInterval(String id) {
		this.id = id;
	}

	public void start() {
		start("");
	}

	/**
	 * 开始计时
	 */
	public void start(String taskName) {
		if(taskName == null){
			throw new IllegalArgumentException("taskName must be not null");
		}
		if (this.currentTaskName != null) {
			throw new IllegalStateException("task is running, so don't start");
		}
		this.currentTaskName = taskName;
		this.startTimeNanos = nanoTime();
	}

	/**
	 * 停止计时
	 */
	public void stop(){
		if (this.currentTaskName == null) {
			throw new IllegalStateException("task is not running, so don't stop");
		}

		long lastTime = nanoTime() - this.startTimeNanos;

		this.lastTaskInfo = new TaskInfo(this.currentTaskName, lastTime);
		if(this.keepTaskList){
			taskInfoList.add(this.lastTaskInfo);
		}

		this.totalTimeNanos += lastTime;
		this.currentTaskName = null;
	}

	/**
	 * 重新开始计算时间（重置开始时间）
	 * @return {@link TimeInterval}
	 */
	public TimeInterval restart(){
		return restart("");
	}

	/**
	 * 重新开始计时, 如果有任务则先停止再计时
	 * @param taskName 任务名称
	 * @return {@link TimeInterval}
	 */
	public TimeInterval restart(String taskName){
		if(isRunning()){
			stop();
		}
		start(taskName);
		return this;
	}

	/**
	 * 定时器运行中
	 * @return 是否在运行
	 */
	public boolean isRunning(){
		return this.currentTaskName != null;
	}

	/**
	 * 获取定时器标识
	 */
	public String getId() {
		return id;
	}

	public void setKeepTaskList(boolean keepTaskList) {
		this.keepTaskList = keepTaskList;
	}

	/**
	 * 获取任务列表
	 */
	public TaskInfo[] getTaskInfos() {
		if (!this.keepTaskList) {
			throw new UnsupportedOperationException("Task info is not being kept!");
		}
		return this.taskInfoList.toArray(new TaskInfo[0]);
	}

	/**
	 * 获取当前运行任务的耗时时间,单位纳秒
	 * @return 耗时时间
	 */
	public long getTaskTimeNanos(){
		if (!isRunning()) {
			throw new IllegalStateException("no running task");
		}
		return nanoTime() - this.startTimeNanos;
	}

	/**
	 * 获取当前运行任务的耗时时间,单位毫秒
	 * @return 耗时时间
	 */
	public long getTaskTimeMillis(){
		if (!isRunning()) {
			throw new IllegalStateException("no running task");
		}
		return nanosToMillis(getTaskTimeNanos());
	}

	/**
	 * 获取当前运行任务的耗时时间,单位秒
	 * @return 耗时时间
	 */
	public double getTaskTimeSeconds(){
		if (!isRunning()) {
			throw new IllegalStateException("no running task");
		}
		return nanosToSeconds(getTaskTimeNanos());
	}

	/**
	 * 获取当前运行任务的耗时时间,单位分
	 * @return 耗时时间
	 */
	public double getTaskTimeMinutes(){
		if (!isRunning()) {
			throw new IllegalStateException("no running task");
		}
		return nanosToMinutes(getTaskTimeNanos());
	}

	/**
	 * 获取当前运行任务的耗时时间,单位小时
	 * @return 耗时时间
	 */
	public double getTaskTimeHours(){
		if (!isRunning()) {
			throw new IllegalStateException("no running task");
		}
		return nanosToHours(getTaskTimeNanos());
	}

	/**
	 * 获取当前运行任务的耗时时间,单位天
	 * @return 耗时时间
	 */
	public double getTaskTimeDays(){
		if (!isRunning()) {
			throw new IllegalStateException("no running task");
		}
		return nanosToDays(getTaskTimeNanos());
	}

	/**
	 * 获取上一个任务
	 * @return 上一个任务
	 */
	public TaskInfo getLastTaskInfo(){
		if (this.lastTaskInfo == null) {
			throw new IllegalStateException("no last task");
		}
		return this.lastTaskInfo;
	}

	/**
	 * 获取上一个任务的耗时时间,单位纳秒
	 * @return 耗时时间
	 */
	public long getLastTaskTimeNanos(){
		if (this.lastTaskInfo == null) {
			throw new IllegalStateException("no last task");
		}
		return this.lastTaskInfo.getTimeNanos();
	}

	/**
	 * 获取上一个任务的耗时时间,单位毫秒
	 * @return 耗时时间
	 */
	public long getLastTaskTimeMillis(){
		if (this.lastTaskInfo == null) {
			throw new IllegalStateException("no last task");
		}
		return this.lastTaskInfo.getTimeMillis();
	}

	/**
	 * 获取上一个任务的耗时时间,单位秒
	 * @return 耗时时间
	 */
	public double getLastTaskTimeSeconds(){
		if (this.lastTaskInfo == null) {
			throw new IllegalStateException("no last task");
		}
		return this.lastTaskInfo.getTimeSeconds();
	}

	/**
	 * 获取上一个任务的耗时时间,单位分钟
	 * @return 耗时时间
	 */
	public double getLastTaskTimeMinutes(){
		if (this.lastTaskInfo == null) {
			throw new IllegalStateException("no last task");
		}
		return this.lastTaskInfo.getTimeMinutes();
	}

	/**
	 * 获取上一个任务的耗时时间,单位小时
	 * @return 耗时时间
	 */
	public double getLastTaskTimeHours(){
		if (this.lastTaskInfo == null) {
			throw new IllegalStateException("no last task");
		}
		return this.lastTaskInfo.getTimeHours();
	}

	/**
	 * 获取上一个任务的耗时时间,单位天
	 * @return 耗时时间
	 */
	public double getLastTaskTimeDays(){
		if (this.lastTaskInfo == null) {
			throw new IllegalStateException("no last task");
		}
		return this.lastTaskInfo.getTimeDays();
	}

	/**
	 * 获取总耗时,单位纳秒
	 * @return 总耗时时间
	 */
	public long getTotalTimeNanos() {
		return this.totalTimeNanos;
	}

	/**
	 * 获取总耗时,单位毫秒
	 * @return 总耗时时间
	 */
	public long getTotalTimeMillis() {
		return nanosToMillis(this.totalTimeNanos);
	}

	/**
	 * 获取总耗时,单位秒
	 * @return 总耗时时间
	 */
	public double getTotalTimeSeconds() {
		return nanosToSeconds(this.totalTimeNanos);
	}

	/**
	 * 获取总耗时,单位分钟
	 * @return 总耗时时间
	 */
	public double getTotalTimeMinutes() {
		return nanosToMinutes(this.totalTimeNanos);
	}

	/**
	 * 获取总耗时,单位小时
	 * @return 总耗时时间
	 */
	public double getTotalTimeHours() {
		return nanosToHours(this.totalTimeNanos);
	}

	/**
	 * 获取总耗时,单位小时
	 * @return 总耗时时间
	 */
	public double getTotalTimeDays() {
		return nanosToDays(this.totalTimeNanos);
	}

	/**
	 * 纳秒为时间单位,概要统计
	 * @return 统计信息
	 */
	public String shortSummary() {
		return shortSummary(TimeUnit.NANOSECONDS);
	}

	/**
	 * 概要统计
	 * @param unit 时间单位
	 * @return 统计信息
	 */
	public String shortSummary(TimeUnit unit) {
		NumberFormat nf = NumberFormat.getInstance();
		nf.setGroupingUsed(true);
		return "TimeInterval '" + getId() + "': running time = " + str(unit, getTotalTimeNanos(), nf);
	}

	/**
	 * 纳秒为时间单位,统计详细信息
	 * @return 统计信息
	 */
	public String prettyPrint() {
		return prettyPrint(TimeUnit.NANOSECONDS);
	}

	/**
	 * 详细统计，格式化信息
	 * @param unit 时间单位
	 * @return 统计信息
	 */
	public String prettyPrint(TimeUnit unit) {
		StringBuilder sb = new StringBuilder(shortSummary(unit));
		sb.append('\n');
		if (!this.keepTaskList) {
			sb.append("No task info kept");
		}
		else {
			sb.append("---------------------------------------------\n");
			sb.append("\"time\"         \"percent\"     \"Task name\"\n");
			sb.append("---------------------------------------------\n");

			NumberFormat nf = NumberFormat.getInstance();
			nf.setGroupingUsed(true);

			NumberFormat pf = NumberFormat.getPercentInstance();
			pf.setMaximumFractionDigits(2);
			pf.setGroupingUsed(false);

			for (TaskInfo task : this.taskInfoList) {
				sb.append(str(unit, task.getTimeNanos(), nf)).append("  ");
				sb.append(pf.format((double) task.getTimeNanos() / getTotalTimeNanos())).append("  ");
				sb.append(task.getTaskName()).append("\n");
			}
		}
		return sb.toString();
	}

	private String str(TimeUnit unit, long timeNanos, NumberFormat nf){
		String str;
		switch (unit){
			case MICROSECONDS:
				str = nf.format(nanosToMicros(timeNanos)) + "mis";
				break;
			case MILLISECONDS:
				str = nf.format(nanosToMillis(timeNanos)) + "ms";
				break;
			case SECONDS:
				str = nf.format(nanosToSeconds(timeNanos)) + "s";
				break;
			case MINUTES:
				str = nf.format(nanosToMinutes(timeNanos)) + "m";
				break;
			case HOURS:
				str = nf.format(nanosToHours(timeNanos)) + "h";
				break;
			case DAYS:
				str = nf.format(nanosToDays(timeNanos)) + "d";
				break;
			case NANOSECONDS:
			default:
				str = nf.format(timeNanos) + "ns";
				break;
		}
		return str;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder(shortSummary(TimeUnit.NANOSECONDS));
		if (this.keepTaskList) {
			NumberFormat pf = NumberFormat.getPercentInstance();
			pf.setMaximumFractionDigits(2);
			pf.setGroupingUsed(false);

			for (TaskInfo task : this.taskInfoList) {
				sb.append("; [").append(task.getTaskName()).append("] took ").append(task.getTimeSeconds()).append(" s");
				sb.append(" = ").append(pf.format((double)task.getTimeNanos() / getTotalTimeNanos()));
			}
		}
		else {
			sb.append("; no task info kept");
		}
		return sb.toString();
	}

	/**
	 * 获取纳秒
	 * @return 纳秒
	 */
	private long nanoTime(){
		return System.nanoTime();
	}

	private static long nanosToMicros(long duration) {
		return TimeUnit.NANOSECONDS.toMicros(duration);
	}

	private static long nanosToMillis(long duration) {
		return TimeUnit.NANOSECONDS.toMillis(duration);
	}

	private static double nanosToSeconds(long duration) {
		return (double)duration/T_SECOND;
	}

	private static double nanosToMinutes(long duration) {
		return (double)duration/T_MINUTE;
	}

	private static double nanosToHours(long duration) {
		return (double)duration/T_HOUR;
	}

	private static double nanosToDays(long duration) {
		return (double)duration/T_DAY;
	}

	public static final class TaskInfo{
		private final String taskName;
		private final long timeNanos;
		TaskInfo(String taskName, long timeNanos) {
			this.taskName = taskName;
			this.timeNanos = timeNanos;
		}

		public String getTaskName() {
			return this.taskName;
		}

		public long getTimeNanos() {
			return this.timeNanos;
		}

		public long getTimeMillis() {
			return nanosToMillis(this.timeNanos);
		}

		public double getTimeSeconds() {
			return nanosToSeconds(this.timeNanos);
		}

		public double getTimeMinutes() {
			return nanosToMinutes(this.timeNanos);
		}

		public double getTimeHours() {
			return nanosToHours(this.timeNanos);
		}

		public double getTimeDays() {
			return nanosToDays(this.timeNanos);
		}

	}
	
}

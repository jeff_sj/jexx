package jexx.time;

import jexx.util.Assert;
import jexx.util.StringUtil;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;
import java.util.Calendar;
import java.util.Date;

/**
 * 基于 JDK 8 time包的时间工具类
 * 时区问题：https://blog.csdn.net/u011165335/article/details/76636296
 */
@Deprecated
public class TimeUtil {

    public static final String NOR_DATE_FORMAT = "yyyy-MM-dd";
    public static final String NOR_TIME_FORMAT = "HH:mm:ss";
    public static final String NOR_DATETIME_MINUTE_FORMAT = "yyyy-MM-dd HH:mm";
    public static final String NOR_DATETIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
    public static final String NOR_DATETIME_MS_FORMAT = "yyyy-MM-dd HH:mm:ss.SSS";

    /**
     * Converts local date to Date.
     * @param localDate LocalDate
     * @return Date
     */
    public static Date toDate(final LocalDate localDate) {
        return Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
    }

    /**
     * converts Date to local date
     * @param date  date
     * @return  LocalDate
     */
    public static LocalDate toLocalDate(final Date date){
        return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
    }

    /**
     * Converts local date time to Date.
     */
    public static Date toDate(final LocalDateTime localDateTime) {
        return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
    }

    /**
     * converts Date to local datetime
     * @param date  date
     * @return  LocalDateTime
     */
    public static LocalDateTime toLocalDateTime(final Date date){
        return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
    }

    public static String format(LocalDateTime localDateTime){
        return format(localDateTime, NOR_DATETIME_FORMAT);
    }

    public static String format(LocalDateTime localDateTime, String pattern){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
        return localDateTime.format(formatter);
    }

    public static String format(LocalDate localDate){
        return format(localDate,NOR_DATE_FORMAT);
    }

    public static String format(LocalDate localDate, String pattern){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
        return localDate.format(formatter);
    }

    public static String format(LocalTime localTime){
        return format(localTime, NOR_TIME_FORMAT);
    }

    public static String format(LocalTime localTime, String pattern){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
        return localTime.format(formatter);
    }

    /**
     * 格式化Date类型的时间
     * @param date  Date类型的时间
     * @param pattern   格式化模式
     * @return  格式化后的时间
     */
    public static String format(Date date, String pattern){
        LocalDateTime localDateTime = toLocalDateTime(date);
        return format(localDateTime, pattern);
    }

    /**
     * 解析字符串时间为LocalDateTime
     * @param time  datetime字符串时间
     * @return  LocalDateTime
     */
    public static LocalDateTime parseDateTime(String time){
        return parseDateTime(time, NOR_DATETIME_FORMAT);
    }

    /**
     * 按指定格式解析字符串时间为LocalDateTime
     * @param time  datetime字符串时间
     * @return  LocalDateTime
     */
    public static LocalDateTime parseDateTime(String time, String pattern){
        return LocalDateTime.parse(time, DateTimeFormatter.ofPattern(pattern));
    }

    /**
     * 解析字符串时间为LocalDate
     * @param time date字符串
     * @return LocalDate
     */
    public static LocalDate parseDate(String time){
        return parseDate(time, NOR_DATE_FORMAT);
    }

    /**
     * 按指定格式解析字符串时间为LocalDate
     * @param time date字符串
     * @param pattern 格式
     * @return LocalDate
     */
    public static LocalDate parseDate(String time, String pattern){
        return LocalDate.parse(time, DateTimeFormatter.ofPattern(pattern));
    }

    /**
     * 计算指定日期的年龄
     * @param birthday 出生日期
     * @param dateToCompare 指定日期
     * @return 年龄
     */
    public static int age(LocalDate birthday, LocalDate dateToCompare){
        if (birthday.compareTo(dateToCompare) < 0) {
            throw new IllegalArgumentException(StringUtil.substitute("Birthday is after date {}!", format(dateToCompare)));
        }

        int year = dateToCompare.getYear();
        int month = dateToCompare.getMonthValue();
        int dayOfMonth = dateToCompare.getDayOfMonth();

        int age = year - birthday.getYear();

        int monthBirth = birthday.getMonthValue();
        if (month == monthBirth) {
            int dayOfMonthBirth = birthday.getDayOfMonth();
            if (dayOfMonth < dayOfMonthBirth) {
                // 如果生日在当月，但是未达到生日当天的日期，年龄减一
                age--;
            }
        } else if (month < monthBirth) {
            // 如果当前月份未达到生日的月份，年龄计算减一
            age--;
        }

        return age;
    }

    //------------------------------------------------- time location

    public static LocalDateTime beginOfDay(LocalDateTime time){
        Assert.notNull(time, "time is not illegal");
        return LocalDateTime.of(time.toLocalDate(), LocalTime.MIN);
    }

    public static LocalDateTime endOfDay(LocalDateTime time){
        Assert.notNull(time, "time is not illegal");
        return LocalDateTime.of(time.toLocalDate(), LocalTime.MAX);
    }

    public static LocalDateTime beginOfMonth(LocalDateTime time){
        Assert.notNull(time, "time is not illegal");
        return time.with(TemporalAdjusters.firstDayOfMonth()).withHour(0).withMinute(0).withSecond(0);
    }

    public static LocalDateTime endOfMonth(LocalDateTime time){
        Assert.notNull(time, "time is not illegal");
        return time.with(TemporalAdjusters.lastDayOfMonth()).withHour(23).withMinute(59).withSecond(59);
    }

}

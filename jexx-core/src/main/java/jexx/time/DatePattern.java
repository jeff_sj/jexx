package jexx.time;

import jexx.time.format.FastDateFormat;
import jexx.util.StringUtil;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 日期格式化类，提供常用的日期格式化对象;
 * <pre>
 *     时间与日期模式 请参考 java.text.SimpleDateFormat
 * </pre>
 * @see java.text.SimpleDateFormat
 */
public enum DatePattern {

    /** 标准日期格式 */
	NORM_DATE_PATTERN("yyyy-MM-dd", "\\d{4}-\\d{1,2}-\\d{1,2}"),
    /** 标准时间格式：HH:mm:ss */
	NORM_TIME_PATTERN("HH:mm:ss", "\\d{1,2}:\\d{1,2}:\\d{1,2}"),
	NORM_DATETIME_MINUTE_PATTERN("yyyy-MM-dd HH:mm", "\\d{4}-\\d{1,2}-\\d{1,2}\\s\\d{1,2}:\\d{1,2}"), //标准日期时间格式，精确到分：yyyy-MM-dd HH:mm
	NORM_DATETIME_PATTERN("yyyy-MM-dd HH:mm:ss", "\\d{4}-\\d{1,2}-\\d{1,2}\\s\\d{1,2}:\\d{1,2}:\\d{1,2}"), //标准日期时间格式，精确到秒：yyyy-MM-dd HH:mm:ss
	NORM_DATETIME_MS_PATTERN("yyyy-MM-dd HH:mm:ss.SSS", "\\d{4}-\\d{1,2}-\\d{1,2}\\s\\d{1,2}:\\d{1,2}:\\d{1,2}\\.\\d{1,3}"), //标准日期时间格式，精确到毫秒：yyyy-MM-dd HH:mm:ss.SSS

	PURE_DATE_PATTERN("yyyyMMdd", "\\d{8}"),
	PURE_TIME_PATTERN("HHmmss", "\\d{6}"),
	PURE_DATETIME_PATTERN("yyyyMMddHHmmss", "\\d{14}"),
	PURE_DATETIME_MS_PATTERN("yyyyMMddHHmmssSSS", "\\d{17}"),

	HTTP_DATETIME_PATTERN("EEE, dd MMM yyyy HH:mm:ss z", null), //TTP头中日期时间格式：EEE, dd MMM yyyy HH:mm:ss z
	JDK_DATETIME_PATTERN("EEE MMM dd HH:mm:ss zzz yyyy", null), //JDK中日期时间格式：EEE MMM dd HH:mm:ss zzz yyyy

    //时区
    ISO_1_DATETIME_MS_PATTERN("yyyy-MM-dd'T'HH:mm:ss.SSSX", "\\d{4}-\\d{1,2}-\\d{1,2}T\\d{1,2}:\\d{1,2}:\\d{1,2}\\.\\d{1,3}(?:Z|(?:[+-]\\d{2}))"),
    ISO_1_DATETIME_PATTERN("yyyy-MM-dd'T'HH:mm:ssX", "\\d{4}-\\d{1,2}-\\d{1,2}T\\d{1,2}:\\d{1,2}:\\d{1,2}(?:Z|(?:[+-]\\d{2}))"),
    ISO_2_DATETIME_MS_PATTERN("yyyy-MM-dd'T'HH:mm:ss.SSSXX", "\\d{4}-\\d{1,2}-\\d{1,2}T\\d{1,2}:\\d{1,2}:\\d{1,2}\\.\\d{1,3}(?:Z|(?:[+-]\\d{4}))"),
    ISO_2_DATETIME_PATTERN("yyyy-MM-dd'T'HH:mm:ssXX", "\\d{4}-\\d{1,2}-\\d{1,2}T\\d{1,2}:\\d{1,2}:\\d{1,2}(?:Z|(?:[+-]\\d{4}))"),
    //2001-07-04T12:08:56.235+08:00
    ISO_3_DATETIME_MS_PATTERN("yyyy-MM-dd'T'HH:mm:ss.SSSXXX", "\\d{4}-\\d{1,2}-\\d{1,2}T\\d{1,2}:\\d{1,2}:\\d{1,2}\\.\\d{1,3}(?:Z|(?:[+-]\\d{2}:\\d{2}))"),
    ISO_3_DATETIME_PATTERN("yyyy-MM-dd'T'HH:mm:ssXXX", "\\d{4}-\\d{1,2}-\\d{1,2}T\\d{1,2}:\\d{1,2}:\\d{1,2}(?:Z|(?:[+-]\\d{2}:\\d{2}))"),
    ISO_Z_DATETIME_MS_PATTERN("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", "\\d{4}-\\d{1,2}-\\d{1,2}T\\d{1,2}:\\d{1,2}:\\d{1,2}\\.\\d{1,3}Z"),
    ISO_Z_DATETIME_PATTERN("yyyy-MM-dd'T'HH:mm:ss'Z'", "\\d{4}-\\d{1,2}-\\d{1,2}T\\d{1,2}:\\d{1,2}:\\d{1,2}Z"),
	;

	private String format;
	private String pattern;
	private static Map<String, Pattern> patternMap = new HashMap<>();

	DatePattern(String format, String pattern) {
		this.format = format;
		this.pattern = pattern;
	}

	public static FastDateFormat getDateFormat(DatePattern datePattern){
		if(datePattern == null){
			datePattern = NORM_DATE_PATTERN;
		}
		return FastDateFormat.getInstance(datePattern.format);
	}

    /**
     * 根据日期内容找到对应的日期格式化类
     * @param dateStr 日期内容
     * @return 日期格式化类
     */
    public static FastDateFormat getDateFormat(String dateStr){
        if(StringUtil.isEmpty(dateStr)){
            return null;
        }

        Matcher matcher;
        FastDateFormat fastDateFormat = null;
        for(DatePattern datePattern : DatePattern.values()){
            if(StringUtil.isEmpty(datePattern.getPattern())){
                continue;
            }

            Pattern pattern = patternMap.get(datePattern.getPattern());
            if(pattern == null){
                pattern = Pattern.compile(datePattern.getPattern());
                patternMap.put(datePattern.getPattern(), pattern);
            }
            matcher = pattern.matcher(dateStr);
            if(matcher.matches()){
                fastDateFormat = getDateFormat(datePattern);
                break;
            }
        }
        return fastDateFormat;
    }

	public String getFormat() {
		return format;
	}

    public String getPattern() {
        return pattern;
    }
}

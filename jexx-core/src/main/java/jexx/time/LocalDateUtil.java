package jexx.time;

import jexx.util.Assert;
import jexx.util.StringUtil;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;
import java.util.Date;

/**
 * 针对 LocalDateTime,LocalDate,LocalTime的时间工具类
 * @author jeff
 * @since 2020/5/25
 */
public class LocalDateUtil {

    /**
     * 计算指定日期的年龄
     * @param birthday 出生日期
     * @param dateToCompare 指定日期
     * @return 年龄
     */
    public static int age(LocalDate birthday, LocalDate dateToCompare){
        if (birthday.compareTo(dateToCompare) < 0) {
            throw new IllegalArgumentException(StringUtil.substitute("Birthday is after date {}!", format(dateToCompare)));
        }

        int year = dateToCompare.getYear();
        int month = dateToCompare.getMonthValue();
        int dayOfMonth = dateToCompare.getDayOfMonth();

        int age = year - birthday.getYear();

        int monthBirth = birthday.getMonthValue();
        if (month == monthBirth) {
            int dayOfMonthBirth = birthday.getDayOfMonth();
            if (dayOfMonth < dayOfMonthBirth) {
                // 如果生日在当月，但是未达到生日当天的日期，年龄减一
                age--;
            }
        } else if (month < monthBirth) {
            // 如果当前月份未达到生日的月份，年龄计算减一
            age--;
        }

        return age;
    }

    public static String format(LocalDate localDate){
        return format(localDate, DatePattern.NORM_DATE_PATTERN);
    }

    public static String format(LocalDate localDate, DatePattern pattern){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern.getFormat());
        return localDate.format(formatter);
    }

    public static String format(LocalDate localDate, String pattern){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
        return localDate.format(formatter);
    }

    public static String formatDateTime(LocalDateTime localDateTime) {
        return format(localDateTime, DatePattern.NORM_DATETIME_PATTERN);
    }

    public static String formatDateTimeWithMilli(LocalDateTime localDateTime) {
        return format(localDateTime, DatePattern.NORM_DATETIME_MS_PATTERN);
    }

    public static String format(LocalDateTime localDateTime, String pattern) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
        return localDateTime.format(formatter);
    }

    public static String format(LocalDateTime localDateTime, DatePattern pattern) {
        return format(localDateTime, pattern.getFormat());
    }

    public static boolean isBetween(LocalDateTime time ,LocalDateTime start, LocalDateTime end){
        if(time == null || start == null || end == null){
            return false;
        }
        return (time.isEqual(start) || time.isAfter(start)) && (time.isEqual(end) || time.isBefore(end));
    }

    public static boolean isEqual(LocalTime left, LocalTime right){
        return left != null && left.equals(right);
    }

    public static boolean isEqual(LocalDate left, LocalDate right){
        return left != null && right != null && left.isEqual(right);
    }

    public static boolean isEqual(LocalDateTime left, LocalDateTime right){
        return left != null && right != null && left.isEqual(right);
    }

    public static boolean isToday(LocalDateTime time){
        return time != null &&LocalDate.now().isEqual(time.toLocalDate());
    }

    public static LocalDateTime parse(String dateStr, String pattern) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
        return LocalDateTime.parse(dateStr, formatter);
    }

    public static LocalDateTime parse(String dateStr, DatePattern pattern) {
        return parse(dateStr, pattern.getFormat());
    }

    public static LocalDate parseDate(String time){
        return parseDate(time, DatePattern.NORM_DATE_PATTERN.getFormat());
    }

    public static LocalDate parseDate(String time, String pattern){
        return LocalDate.parse(time, DateTimeFormatter.ofPattern(pattern));
    }

    public static LocalDateTime parseDateTime(String dateTime){
        return parse(dateTime, DatePattern.NORM_DATETIME_PATTERN);
    }

    public static LocalDateTime parseDateTimeWithMill(String dateTime){
        return parse(dateTime, DatePattern.NORM_DATETIME_MS_PATTERN);
    }

    public static DateTime toDate(LocalDate localDate){
        Date date = Date.from(localDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
        return new DateTime(date);
    }

    public static DateTime toDate(LocalDateTime localDateTime){
        Date date = Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
        return new DateTime(date);
    }

    public static LocalDate toLocalDate(Date date){
        return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
    }

    public static LocalDateTime toLocalDateTime(Date date){
        return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
    }

    public static LocalTime toLocalTime(Date date){
        return date.toInstant().atZone(ZoneId.systemDefault()).toLocalTime();
    }

    public static long toMilliSecond(LocalDateTime dateTime){
        Assert.notNull(dateTime, "dateTime is not null");
        return dateTime.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
    }

    public static LocalDateTime toLocalDateTime(long milliseconds){
        Assert.isTrue(milliseconds > 0 , "milliseconds is not illegal");
        Instant instant = Instant.ofEpochMilli(milliseconds);
        return LocalDateTime.ofInstant(instant, ZoneId.systemDefault());
    }

    //------------------------------------------------- time location

    public static LocalDateTime beginOfDay(LocalDateTime time){
        Assert.notNull(time, "time is not illegal");
        return LocalDateTime.of(time.toLocalDate(), LocalTime.MIN);
    }

    public static LocalDateTime endOfDay(LocalDateTime time){
        Assert.notNull(time, "time is not illegal");
        return LocalDateTime.of(time.toLocalDate(), LocalTime.MAX);
    }

    public static LocalDateTime beginOfMonth(LocalDateTime time){
        Assert.notNull(time, "time is not illegal");
        return LocalDateTime.of(time.toLocalDate().with(TemporalAdjusters.firstDayOfMonth()), LocalTime.MIN);
    }

    public static LocalDateTime endOfMonth(LocalDateTime time){
        Assert.notNull(time, "time is not illegal");
        return LocalDateTime.of(time.toLocalDate().with(TemporalAdjusters.lastDayOfMonth()), LocalTime.MIN);
    }

    public static LocalDateTime beginOfYear(LocalDateTime time){
        Assert.notNull(time, "time is not illegal");
        return LocalDateTime.of(time.toLocalDate().with(TemporalAdjusters.firstDayOfYear()), LocalTime.MIN);
    }

    public static LocalDateTime endOfYear(LocalDateTime time){
        Assert.notNull(time, "time is not illegal");
        return LocalDateTime.of(time.toLocalDate().with(TemporalAdjusters.lastDayOfYear()), LocalTime.MIN);
    }

}

package jexx.time;

import jexx.lang.Ranger;

import java.util.Date;

/**
 * 日期范围
 * @author jeff
 */
public class DateRanger extends Ranger<Date> {

	/**
	 * 构造，包含开始和结束日期时间
	 * 
	 * @param start 起始日期时间
	 * @param end 结束日期时间
	 * @param unit 步进单位
	 */
	public DateRanger(Date start, Date end, final DateField unit) {
		this(start, end, unit, 1);
	}

	/**
	 * 构造，包含开始和结束日期时间
	 * 
	 * @param start 起始日期时间
	 * @param end 结束日期时间
	 * @param unit 步进单位
	 * @param step 步进数
	 */
	public DateRanger(Date start, Date end, final DateField unit, final int step) {
		this(start, end, unit, step, true, true);
	}

	/**
	 * 构造
	 * 
	 * @param start 起始日期时间
	 * @param end 结束日期时间
	 * @param unit 步进单位
	 * @param step 步进数
	 * @param isIncludeStart 是否包含开始的时间
	 * @param isIncludeEnd 是否包含结束的时间
	 */
	public DateRanger(Date start, Date end, final DateField unit, final int step, boolean isIncludeStart, boolean isIncludeEnd) {
		super(start, end, (Date current, int index) -> DateUtil.offset(current,unit, step), isIncludeStart, isIncludeEnd);
	}

}

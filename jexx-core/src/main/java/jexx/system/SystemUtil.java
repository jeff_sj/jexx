package jexx.system;

import java.security.AccessController;
import java.security.PrivilegedAction;
import java.util.Objects;

/**
 * 参考 jodd
 */
public class SystemUtil {

    public static String get(final String name, final String defaultValue) {
        Objects.requireNonNull(name);

        String value = null;
        try {
            if (System.getSecurityManager() == null) {
                value = System.getProperty(name);
            } else {
                value = AccessController.doPrivileged((PrivilegedAction<String>) () -> System.getProperty(name));
            }
        } catch (Exception ignore) {
        }

        if (value == null) {
            return defaultValue;
        }

        return value;
    }

    public static String get(final String name) {
        return get(name, null);
    }

    public static boolean getBoolean(final String name, final boolean defaultValue) {
        String value = get(name);
        if (value == null) {
            return defaultValue;
        }

        value = value.trim().toLowerCase();

        switch (value) {
            case "true" :
            case "yes"  :
            case "1"    :
            case "on"   :
                return true;
            case "false":
            case "no"   :
            case "0"    :
            case "off"  :
                return false;
            default:
                return defaultValue;
        }
    }

    public static long getInt(final String name, final int defaultValue) {
        String value = get(name);
        if (value == null) {
            return defaultValue;
        }

        value = value.trim().toLowerCase();
        try {
            return Integer.parseInt(value);
        }
        catch (NumberFormatException nfex) {
            return defaultValue;
        }
    }

    public static long getLong(final String name, final long defaultValue) {
        String value = get(name);
        if (value == null) {
            return defaultValue;
        }

        value = value.trim().toLowerCase();
        try {
            return Long.parseLong(value);
        }
        catch (NumberFormatException nfex) {
            return defaultValue;
        }
    }

}

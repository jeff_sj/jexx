package jexx.crypto;

import jexx.util.Base64Util;
import jexx.util.StringUtil;

import javax.crypto.Cipher;
import java.security.AlgorithmParameters;
import java.security.Key;
import java.security.spec.AlgorithmParameterSpec;

/**
 * 提供加密和解密的加密密码的抽象功能
 * @author jeff
 * @since 2020/5/8
 */
public abstract class BaseCipher {

    protected String transformation;
    protected Cipher cipher;

    public BaseCipher(String transformation) {
        try {
            this.transformation = transformation;
            this.cipher = Cipher.getInstance(transformation);
        } catch (Exception e) {
            throw new CryptoException(e);
        }
    }

    protected byte[] encrypt(Key key, byte[] input) {
        try {
            this.cipher.init(Cipher.ENCRYPT_MODE, key);
            return cipher.doFinal(input);
        }
        catch (Exception e){
            throw new CryptoException(e);
        }
    }

    protected byte[] encrypt(Key key, byte[] input, AlgorithmParameters params) {
        try {
            this.cipher.init(Cipher.ENCRYPT_MODE, key, params);
            return cipher.doFinal(input);
        }
        catch (Exception e){
            throw new CryptoException(e);
        }
    }

    protected byte[] encrypt(Key key, byte[] input, AlgorithmParameterSpec params) {
        try {
            this.cipher.init(Cipher.ENCRYPT_MODE, key, params);
            return cipher.doFinal(input);
        }
        catch (Exception e){
            throw new CryptoException(e);
        }
    }

    protected byte[] decrypt(Key key, byte[] input){
        try {
            this.cipher.init(Cipher.DECRYPT_MODE, key);
            return cipher.doFinal(input);
        }
        catch (Exception e){
            throw new CryptoException(e);
        }
    }

    protected byte[] decrypt(Key key, byte[] input,  AlgorithmParameters params){
        try {
            this.cipher.init(Cipher.DECRYPT_MODE, key, params);
            return cipher.doFinal(input);
        }
        catch (Exception e){
            throw new CryptoException(e);
        }
    }

    protected byte[] decrypt(Key key, byte[] input, AlgorithmParameterSpec params) {
        try {
            this.cipher.init(Cipher.DECRYPT_MODE, key, params);
            return cipher.doFinal(input);
        }
        catch (Exception e){
            throw new CryptoException(e);
        }
    }

    protected abstract byte[] encrypt(byte[] data);

    public byte[] encryptAsBase64(byte[] data){
        byte[] buffer = encrypt(data);
        return Base64Util.encode(buffer);
    }

    public String encryptAsBase64(String data){
        byte[] buffer = encryptAsBase64(StringUtil.getBytes(data));
        return StringUtil.str(buffer);
    }

    protected abstract byte[] decrypt(byte[] data);

    public byte[] decryptAsBase64(byte[] data){
        byte[] buffer = Base64Util.decode(data);
        return decrypt(buffer);
    }

    public String decryptAsBase64(String data){
        byte[] buffer = decryptAsBase64(StringUtil.getBytes(data));
        return StringUtil.str(buffer);
    }





}

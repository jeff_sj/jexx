package jexx.crypto.asymmetric;

import jexx.crypto.CryptoException;
import jexx.util.Base64Util;
import jexx.util.HexUtil;
import jexx.util.ObjectUtil;
import jexx.util.StringUtil;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.Digest;
import org.bouncycastle.crypto.InvalidCipherTextException;
import org.bouncycastle.crypto.digests.SM3Digest;
import org.bouncycastle.crypto.engines.SM2Engine;
import org.bouncycastle.crypto.params.*;
import org.bouncycastle.crypto.signers.SM2Signer;
import org.bouncycastle.jcajce.provider.asymmetric.ec.BCECPrivateKey;
import org.bouncycastle.jcajce.provider.asymmetric.ec.BCECPublicKey;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.jce.spec.ECParameterSpec;
import org.bouncycastle.math.ec.ECCurve;
import org.bouncycastle.math.ec.ECPoint;

import java.math.BigInteger;
import java.security.*;
import java.security.spec.ECGenParameterSpec;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

/**
 * 椭圆曲线（ECC）公钥加密算法，非对称加密。对标RSA，速度更快。
 * 基于BC库。
 *
 * https://blog.csdn.net/pridas/article/details/86118774
 * https://gitee.com/dromara/hutool/blob/v5-master/hutool-crypto/src/main/java/cn/hutool/crypto/asymmetric/SM2.java
 *
 *
 * SM2密码算法 介绍比较详细：https://github.com/Trisia/alg-sm2-demo
 *
 * openssl生成：
 * openssl ecparam -genkey -name SM2 -out sm2_priv.key
 * gmssl ecparam -genkey -name sm2p256v1 -out sm2_priv.key
 *
 * openssl ec -in sm2_priv.key -pubout -out sm2_pub.key
 * gmssl ec -in sm2_priv.key -pubout -out sm2_pub.key
 *
 * openssl pkcs8 -topk8 -inform PEM -in priv.key -outform pem -nocrypt -out pri_key_pkcs8.pem
 *
 *
 * 
 *
 *
 */
public class SM2 {

    private final SM2Engine.Mode mode;
    private final Digest digest;
    private final ECPrivateKeyParameters privateKeyParams;
    private final ECPublicKeyParameters publicKeyParams;
    private final SM2Engine sm2Engine;
    private final SM2Signer sm2Signer;

    public SM2(SM2Engine.Mode mode, Digest digest, ECPrivateKeyParameters privateKeyParams, ECPublicKeyParameters publicKeyParams) {
        initProvider();

        this.privateKeyParams = privateKeyParams;
        this.publicKeyParams = publicKeyParams;
        this.mode = mode;
        this.digest = ObjectUtil.notNullOrDefault(digest, new SM3Digest());
        this.sm2Engine = new SM2Engine(this.digest, mode);
        this.sm2Signer = new SM2Signer();
    }

    /**
     * 构造
     * @param mode SM2Engine模式
     * @param digest 摘要算法
     * @param privateKeyBytes x509私钥
     * @param publicKeyBytes pcks8公钥
     */
    public SM2(SM2Engine.Mode mode, Digest digest, byte[] privateKeyBytes, byte[] publicKeyBytes) {
        initProvider();

        BCECPublicKey publicKey = convertX509ToECPublicKey(publicKeyBytes);
        this.publicKeyParams = convertPublicKeyToParameters(publicKey);

        BCECPrivateKey privateKey = convertPKCS8ToECPrivateKey(privateKeyBytes);
        this.privateKeyParams = convertPrivateKeyToParameters(privateKey);

        this.mode = mode;
        this.digest = ObjectUtil.notNullOrDefault(digest, new SM3Digest());
        this.sm2Engine = new SM2Engine(this.digest, mode);
        this.sm2Signer = new SM2Signer();
    }

    public SM2(byte[] privateKeyBytes, byte[] publicKeyBytes) {
        this(SM2Engine.Mode.C1C3C2, null, privateKeyBytes, publicKeyBytes);
    }

    public SM2(SM2Engine.Mode mode, Digest digest, String privateKey, String publicKey) {
        this(mode, digest, Base64Util.decode(privateKey), Base64Util.decode(publicKey));
    }

    public SM2(String privateKey, String publicKey) {
        this(SM2Engine.Mode.C1C3C2, null, privateKey, publicKey);
    }

    /**
     * 加载BC驱动
     */
    private void initProvider(){
        Provider provider = new BouncyCastleProvider();
        if(Security.getProvider(provider.getName()) == null){
            Security.addProvider(provider);
        }
    }

    // --------------------------------------------------------------------------------- encrypt

    /**
     * TODO GMCipherSpi 实现了  CipherSpi， 那么能通过Cipher.getInstance(transformation)找到实现嘛？
     * @param pubKeyParams
     * @param srcData
     * @return
     */
    protected byte[] encrypt(final SM2Engine engine, final ECPublicKeyParameters pubKeyParams, final byte[] srcData){
        try {
            final ParametersWithRandom pwr = new ParametersWithRandom(pubKeyParams, new SecureRandom());
            engine.init(true, pwr);
            return engine.processBlock(srcData, 0, srcData.length);
        } catch (InvalidCipherTextException e) {
            throw new CryptoException(e);
        }
    }

    public byte[] encrypt(final byte[] srcData){
        return encrypt(getSm2Engine(), publicKeyParams, srcData);
    }

    public String encrypt(final String srcData){
        byte[] encodeBytes = encrypt(StringUtil.getBytes(srcData));
        return StringUtil.str(encodeBytes);
    }

    public byte[] encryptAsBase64(final byte[] srcData){
        byte[] encodedData = encrypt(srcData);
        return Base64Util.encode(encodedData);
    }

    public String encryptAsBase64(final String srcData){
        byte[] srcDataBytes = StringUtil.getBytes(srcData);
        byte[] encodedData = encryptAsBase64(srcDataBytes);
        return StringUtil.str(encodedData);
    }

    // --------------------------------------------------------------------------------- decrypt

    protected byte[] decrypt(final SM2Engine engine, final ECPrivateKeyParameters privateKeyParameters, final byte[] srcData){
        try {
            engine.init(false, privateKeyParameters);
            return engine.processBlock(srcData, 0, srcData.length);
        } catch (InvalidCipherTextException e) {
            throw new CryptoException(e);
        }
    }

    /**
     * 私钥解密
     * @param srcData sm2加密后的数据
     * @return 原始数据
     */
    public byte[] decrypt(final byte[] srcData){
        return decrypt(getSm2Engine(), privateKeyParams, srcData);
    }

    /**
     * 私钥解密
     * @param srcData sm2加密后的数据
     * @return 原始数据
     */
    public String decrypt(final String srcData){
        byte[] decodedBytes = decrypt(StringUtil.getBytes(srcData));
        return StringUtil.str(decodedBytes);
    }

    /**
     * 私钥解密数据，数据为base64格式
     * @param srcData sm2加密后并用base64加密的数据
     * @return 原始数据
     */
    public byte[] decryptAsBase64(final byte[] srcData){
        byte[] decodedData = Base64Util.decode(srcData);
        return decrypt(decodedData);
    }

    /**
     * 私钥解密数据，数据为base64格式
     * @param srcData sm2加密后并用base64加密的数据
     * @return 原始数据
     */
    public String decryptAsBase64(final String srcData){
        byte[] srcDataBytes = StringUtil.getBytes(srcData);
        byte[] decodedData = decryptAsBase64(srcDataBytes);
        return StringUtil.str(decodedData);
    }

    // --------------------------------------------------------------------------------- sign

    /**
     * 签名
     * @param signer sm2签名类
     * @param priKeyParams 私钥参数化对象
     * @param withId 标准参数，若为null，则默认为 "1234567812345678"
     * @param srcData 源数据
     * @return 签名后的数据
     */
    private byte[] sign(final SM2Signer signer, final ECPrivateKeyParameters priKeyParams, final byte[] withId, final byte[] srcData){
        try {
            CipherParameters param = new ParametersWithRandom(priKeyParams, new SecureRandom());;
            if(withId != null) {
                param = new ParametersWithID(param, withId);
            }
            signer.init(true, param);
            signer.update(srcData, 0, srcData.length);
            return signer.generateSignature();
        } catch (org.bouncycastle.crypto.CryptoException e) {
            throw new CryptoException(e);
        }
    }

    /**
     * 数据签名
     * @param withId 国密标准字符。为null，默认为 "1234567812345678" 。用处？
     * @param srcData 源数据
     * @return 数据签名
     */
    public byte[] sign(final byte[] withId, final byte[] srcData){
        return sign(getSm2Signer(), this.privateKeyParams, withId, srcData);
    }

    /**
     * 数据签名
     * @param srcData 源数据
     * @return 数据签名
     */
    public byte[] sign(final byte[] srcData){
        return sign(null, srcData);
    }

    /**
     * 数据签名
     * @param srcData 源数据
     * @return 数据签名
     */
    public byte[] sign(final String srcData){
        return sign(StringUtil.getBytes(srcData));
    }

    /**
     * 16进制的数据签名
     * @param srcData 源数据
     * @return 16进制数据签名
     */
    public String signAsHex(final byte[] srcData){
        byte[] s = sign(null, srcData);
        return HexUtil.encodeHexStr(s);
    }

    /**
     * 数据签名，且转化为16进制
     * @param srcData 源数据
     * @return 16进制数据签名
     */
    public String signAsHex(final String srcData){
        return signAsHex(StringUtil.getBytes(srcData));
    }


    // --------------------------------------------------------------------------------- verify

    private boolean verify(final ECPublicKeyParameters pubKeyParams, final byte[] withId, final byte[] srcData, final byte[] sign) {
        final SM2Signer signer = new SM2Signer();
        CipherParameters param = pubKeyParams;
        if (withId != null) {
            param = new ParametersWithID(pubKeyParams, withId);
        }
        signer.init(false, param);
        signer.update(srcData, 0, srcData.length);
        return signer.verifySignature(sign);
    }


    /**
     * 验签
     * @param withId 国密标准字符。为null，默认为 "1234567812345678" 。用处？
     * @param srcData 源数据
     * @param sign 签名
     * @return 验签是否成功
     */
    public boolean verify(final byte[] withId, final byte[] srcData, final byte[] sign){
        return verify(this.publicKeyParams, withId, srcData, sign);
    }

    /**
     * 验签
     * @param srcData 源数据
     * @param sign 签名
     * @return 验签是否成功
     */
    public boolean verify(final byte[] srcData, final byte[] sign){
        return verify(this.publicKeyParams, null, srcData, sign);
    }

    /**
     * 16进制签名来验签
     * @param withId 国密标准字符。为null，默认为 "1234567812345678" 。用处？
     * @param srcData 源数据
     * @param hexSign 16进制签名
     * @return 验签是否成功
     */
    public boolean verifyAsHex(final String withId, final String srcData, final String hexSign){
        byte[] withIdBytes = StringUtil.isEmpty(withId) ? null : StringUtil.getBytes(withId);
        return verify(withIdBytes, StringUtil.getBytes(srcData), HexUtil.decodeHex(hexSign));
    }

    /**
     * 16进制签名来验签
     * @param srcData 源数据
     * @param hexSign 16进制
     * @return 验签是否成功
     */
    public boolean verifyAsHex(final String srcData, final String hexSign){
        return verifyAsHex(null, srcData, hexSign);
    }

    public SM2Engine getSm2Engine(){
        return this.sm2Engine;
    }

    public SM2Signer getSm2Signer() {
        return sm2Signer;
    }

    public SM2Engine.Mode getMode() {
        return mode;
    }

    public static KeyPair generateSm2KeyPair(){
        try {
            KeyPairGenerator generator = KeyPairGenerator.getInstance("EC", new BouncyCastleProvider());
            generator.initialize(new ECGenParameterSpec("sm2p256v1"), new SecureRandom());
            return generator.generateKeyPair();
        }
        catch (NoSuchAlgorithmException | InvalidAlgorithmParameterException e) {
            throw new CryptoException(e);
        }
    }

    /**
     * 根据BC库的ECC算法把基于x509标准的公钥字节流转化为公钥对象
     * @param x509Bytes 基于x509标准的公钥字节流转
     * @return 公钥对象
     */
    private BCECPublicKey convertX509ToECPublicKey(final byte[] x509Bytes){
        try{
            KeyFactory keyFactory = KeyFactory.getInstance("EC", BouncyCastleProvider.PROVIDER_NAME);
            final X509EncodedKeySpec keySpec = new X509EncodedKeySpec(x509Bytes);
            return (BCECPublicKey) keyFactory.generatePublic(keySpec);
        } catch (NoSuchProviderException | InvalidKeySpecException | NoSuchAlgorithmException e) {
            throw new CryptoException(e);
        }
    }

    private ECPublicKeyParameters convertPublicKeyToParameters(final BCECPublicKey publicKey){
        final ECParameterSpec parameterSpec = publicKey.getParameters();

        final ECCurve curve = parameterSpec.getCurve();
        final ECPoint g = parameterSpec.getG();
        final BigInteger n = parameterSpec.getN();
        final BigInteger h = parameterSpec.getH();
        final ECDomainParameters domainParams = new ECDomainParameters(curve, g, n, h);

        final ECPoint q = publicKey.getQ();
        return new ECPublicKeyParameters(q, domainParams);
    }

    /**
     * 根据BC库的ECC算法把将PKCS8标准的私钥字节流转换为私钥对象
     * @param pkcs8Key PKCS8标准的私钥字节流
     * @return 私钥对象
     */
    private BCECPrivateKey convertPKCS8ToECPrivateKey(final byte[] pkcs8Key){
        try {
            KeyFactory keyFactory = KeyFactory.getInstance("EC", BouncyCastleProvider.PROVIDER_NAME);
            final PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(pkcs8Key);
            return (BCECPrivateKey) keyFactory.generatePrivate(keySpec);
        } catch (NoSuchAlgorithmException | NoSuchProviderException | InvalidKeySpecException e) {
            throw new CryptoException(e);
        }
    }

    private static ECPrivateKeyParameters convertPrivateKeyToParameters(final BCECPrivateKey privateKey){
        final ECParameterSpec parameterSpec = privateKey.getParameters();
        final ECCurve curve = parameterSpec.getCurve();
        final ECPoint g = parameterSpec.getG();
        final BigInteger n = parameterSpec.getN();
        final BigInteger h = parameterSpec.getH();
        final ECDomainParameters domainParams = new ECDomainParameters(curve, g, n, h);
        return new ECPrivateKeyParameters(privateKey.getD(), domainParams);
    }

}

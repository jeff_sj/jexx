/**
 *
 * https://blog.csdn.net/hqy1719239337/article/details/88637324
 * 对称加密算法：
 * <pre>
 *     DES
 *     3DES
 *     AES
 *     PBE
 *     RC4
 * </pre>
 *
 * 非对称密钥算法:
 * <pre>
 *     RSA
 *     ECC
 *     DSA
 * </pre>
 *
 * HASH算法:
 * <pre>
 *     MD5
 *     SHA-1
 *     SHA-224,SHA-256,SHA-384,SHA-512
 * </pre>
 *
 * @author jeff
 * @since 2020/5/9
 */
package jexx.crypto;
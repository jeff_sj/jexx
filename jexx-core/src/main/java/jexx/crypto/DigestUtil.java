package jexx.crypto;

import jexx.codec.Hex;
import jexx.exception.IORuntimeException;
import jexx.exception.UtilException;
import jexx.io.FileUtil;
import jexx.io.IOUtil;
import jexx.util.StringUtil;

import java.io.*;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * 摘要加密工具
 * copy from spring.
 */
public class DigestUtil {

    private static final int STREAM_BUFFER_LENGTH = 1024;

    private static byte[] digest(final MessageDigest digest, final InputStream data) {
        return updateDigest(digest, data).digest();
    }

    public static MessageDigest getDigest(final String algorithm) {
        try {
            return MessageDigest.getInstance(algorithm);
        } catch (final NoSuchAlgorithmException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public static MessageDigest getMd2Digest() {
        return getDigest(MessageDigestAlgorithms.MD2);
    }

    /**
     * Returns an MD5 MessageDigest.
     *
     * @return An MD5 digest instance.
     * @throws IllegalArgumentException
     *             when a {@link NoSuchAlgorithmException} is caught, which should never happen because MD5 is a
     *             built-in algorithm
     * @see MessageDigestAlgorithms#MD5
     */
    public static MessageDigest getMd5Digest() {
        return getDigest(MessageDigestAlgorithms.MD5);
    }

    /**
     * Returns an SHA-1 digest.
     *
     * @return An SHA-1 digest instance.
     * @throws IllegalArgumentException
     *             when a {@link NoSuchAlgorithmException} is caught, which should never happen because SHA-1 is a
     *             built-in algorithm
     * @see MessageDigestAlgorithms#SHA_1
     * @since 1.7
     */
    public static MessageDigest getSha1Digest() {
        return getDigest(MessageDigestAlgorithms.SHA_1);
    }

    /**
     * Returns an SHA-256 digest.
     * <p>
     * Throws a <code>RuntimeException</code> on JRE versions prior to 1.4.0.
     * </p>
     *
     * @return An SHA-256 digest instance.
     * @throws IllegalArgumentException
     *             when a {@link NoSuchAlgorithmException} is caught, which should never happen because SHA-256 is a
     *             built-in algorithm
     * @see MessageDigestAlgorithms#SHA_256
     */
    public static MessageDigest getSha256Digest() {
        return getDigest(MessageDigestAlgorithms.SHA_256);
    }

    /**
     * Returns an SHA-384 digest.
     * <p>
     * Throws a <code>RuntimeException</code> on JRE versions prior to 1.4.0.
     * </p>
     *
     * @return An SHA-384 digest instance.
     * @throws IllegalArgumentException
     *             when a {@link NoSuchAlgorithmException} is caught, which should never happen because SHA-384 is a
     *             built-in algorithm
     * @see MessageDigestAlgorithms#SHA_384
     */
    public static MessageDigest getSha384Digest() {
        return getDigest(MessageDigestAlgorithms.SHA_384);
    }

    /**
     * Returns an SHA-512 digest.
     * <p>
     * Throws a <code>RuntimeException</code> on JRE versions prior to 1.4.0.
     * </p>
     *
     * @return An SHA-512 digest instance.
     * @throws IllegalArgumentException
     *             when a {@link NoSuchAlgorithmException} is caught, which should never happen because SHA-512 is a
     *             built-in algorithm
     * @see MessageDigestAlgorithms#SHA_512
     */
    public static MessageDigest getSha512Digest() {
        return getDigest(MessageDigestAlgorithms.SHA_512);
    }

    /**
     * Returns an SHA-1 digest.
     *
     * @return An SHA-1 digest instance.
     * @throws IllegalArgumentException
     *             when a {@link NoSuchAlgorithmException} is caught
     * @deprecated Use {@link #getSha1Digest()}
     */
    @Deprecated
    public static MessageDigest getShaDigest() {
        return getSha1Digest();
    }

    /**
     * Calculates the MD2 digest and returns the value as a 16 element <code>byte[]</code>.
     *
     * @param data
     *            Data to digest
     * @return MD2 digest
     * @since 1.7
     */
    public static byte[] md2(final byte[] data) {
        return getMd2Digest().digest(data);
    }

    /**
     * Calculates the MD2 digest and returns the value as a 16 element <code>byte[]</code>.
     *
     * @param data
     *            Data to digest
     * @return MD2 digest
     * @throws IOException
     *             On error reading from the stream
     * @since 1.7
     */
    public static byte[] md2(final InputStream data) throws IOException {
        return digest(getMd2Digest(), data);
    }

    public static byte[] md2(final String data) {
        return md2(StringUtil.getBytes(data));
    }

    public static String md2Hex(final byte[] data) {
        return Hex.encodeHexString(md2(data));
    }

    public static String md2Hex(final InputStream data) throws IOException {
        return Hex.encodeHexString(md2(data));
    }

    public static String md2Hex(final String data) {
        return Hex.encodeHexString(md2(data));
    }

    public static byte[] md5(final byte[] data) {
        return getMd5Digest().digest(data);
    }

    public static byte[] md5(final InputStream data) {
        return digest(getMd5Digest(), data);
    }

    public static byte[] md5(final String data) {
        return md5(StringUtil.getBytes(data));
    }

    public static byte[] md5(final File file) throws IOException {
        try(FileInputStream inputStream = new FileInputStream(file)){
            return md5(inputStream);
        }
    }

    public static String md5Hex(final byte[] data) {
        return Hex.encodeHexString(md5(data));
    }

    public static String md5Hex(final InputStream data) {
        return Hex.encodeHexString(md5(data));
    }

    public static String md5Hex(final String data) {
        return Hex.encodeHexString(md5(data));
    }

    public static String md5Hex(final File file) throws IOException {
        return Hex.encodeHexString(md5(file));
    }


    public static byte[] sha1(final byte[] data) {
        return getSha1Digest().digest(data);
    }

    public static byte[] sha1(final InputStream data) throws IOException {
        return digest(getSha1Digest(), data);
    }

    public static byte[] sha1(final String data) {
        return sha1(StringUtil.getBytes(data));
    }

    public static String sha1Hex(final byte[] data) {
        return Hex.encodeHexString(sha1(data));
    }

    public static String sha1Hex(final InputStream data) throws IOException {
        return Hex.encodeHexString(sha1(data));
    }

    public static String sha1Hex(final String data) {
        return Hex.encodeHexString(sha1(data));
    }

    public static byte[] sha256(final byte[] data) {
        return getSha256Digest().digest(data);
    }

    public static byte[] sha256(final InputStream data) throws IOException {
        return digest(getSha256Digest(), data);
    }

    public static byte[] sha256(final String data) {
        return sha256(StringUtil.getBytes(data));
    }

    public static String sha256Hex(final byte[] data) {
        return Hex.encodeHexString(sha256(data));
    }

    public static String sha256Hex(final InputStream data) throws IOException {
        return Hex.encodeHexString(sha256(data));
    }

    public static String sha256Hex(final String data) {
        return Hex.encodeHexString(sha256(data));
    }

    public static byte[] sha384(final byte[] data) {
        return getSha384Digest().digest(data);
    }

    public static byte[] sha384(final InputStream data) throws IOException {
        return digest(getSha384Digest(), data);
    }

    public static byte[] sha384(final String data) {
        return sha384(StringUtil.getBytes(data));
    }

    public static String sha384Hex(final byte[] data) {
        return Hex.encodeHexString(sha384(data));
    }

    public static String sha384Hex(final InputStream data) throws IOException {
        return Hex.encodeHexString(sha384(data));
    }

    public static String sha384Hex(final String data) {
        return Hex.encodeHexString(sha384(data));
    }

    public static byte[] sha512(final byte[] data) {
        return getSha512Digest().digest(data);
    }

    public static byte[] sha512(final InputStream data) throws IOException {
        return digest(getSha512Digest(), data);
    }

    public static byte[] sha512(final String data) {
        return sha512(StringUtil.getBytes(data));
    }

    public static String sha512Hex(final byte[] data) {
        return Hex.encodeHexString(sha512(data));
    }

    public static String sha512Hex(final InputStream data) throws IOException {
        return Hex.encodeHexString(sha512(data));
    }

    public static String sha512Hex(final String data) {
        return Hex.encodeHexString(sha512(data));
    }

    public static MessageDigest updateDigest(final MessageDigest messageDigest, final byte[] valueToDigest) {
        messageDigest.update(valueToDigest);
        return messageDigest;
    }

    public static MessageDigest updateDigest(final MessageDigest digest, final InputStream data){
        try {
            final byte[] buffer = new byte[STREAM_BUFFER_LENGTH];
            int read = data.read(buffer, 0, STREAM_BUFFER_LENGTH);

            while (read > -1) {
                digest.update(buffer, 0, read);
                read = data.read(buffer, 0, STREAM_BUFFER_LENGTH);
            }
        }
        catch (IOException e){
            throw new IORuntimeException(e);
        }
        return digest;
    }

    public static MessageDigest updateDigest(final MessageDigest messageDigest, final String valueToDigest) {
        messageDigest.update(StringUtil.getBytes(valueToDigest));
        return messageDigest;
    }

}

package jexx.crypto;

import jexx.util.Base64Util;
import jexx.util.HexUtil;
import jexx.util.StringUtil;

import java.security.GeneralSecurityException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;

/**
 * 签名工具类
 */
public class SignUtil {

    private static final String DEFAULT_CHARSET = "UTF-8";

    /**
     * 指定算法使用指定私钥签名
     * @param algorithm 算法
     * @param data 待签名数据
     * @param key 私钥
     * @return 签名后数据
     */
    public static byte[] sign(String algorithm, byte[] data, PrivateKey key){
        try {
            Signature signature = Signature.getInstance(algorithm);
            signature.initSign(key);
            signature.update(data);
            return signature.sign();
        }
        catch (GeneralSecurityException e){
            throw new CryptoException(e);
        }
    }

    public static byte[] sign(String algorithm, byte[] data, byte[] privateKey){
        return sign(algorithm, data, KeyUtil.createPrivateKey(privateKey));
    }

    public static byte[] sign(SignAlgorithm algorithm, byte[] data, PrivateKey key){
        return sign(algorithm.getValue(), data, key);
    }

    public static byte[] sign(SignAlgorithm algorithm, byte[] data, byte[] privateKey){
        return sign(algorithm, data, KeyUtil.createPrivateKey(privateKey));
    }

    public static String signAsHex(SignAlgorithm algorithm, byte[] data, byte[] privateKey){
        return HexUtil.encodeHexStr(sign(algorithm, data, privateKey));
    }

    public static String signAsBase64(SignAlgorithm algorithm, byte[] data, byte[] privateKey){
        return Base64Util.encodeAsStr(sign(algorithm, data, privateKey));
    }

    /**
     * 指定算法, 以及pkcs8标准私钥, 签名数据
     * @param algorithm 指定算法
     * @param data 待签名数据
     * @param pkcs8PrivateKey pkcs8标准私钥字符串
     * @return 签名结果
     */
    public static byte[] signWithPkcs8Key(SignAlgorithm algorithm, String data, String pkcs8PrivateKey){
        byte[] dataBytes = StringUtil.getBytes(data, DEFAULT_CHARSET);
        byte[] decodedKey = Base64Util.decode(pkcs8PrivateKey);
        return sign(algorithm, dataBytes, decodedKey);
    }

    /**
     * 指定算法, 以及pkcs8标准私钥, 签名数据, 并对结果base64加密
     * @param algorithm 指定算法
     * @param data 待签名数据
     * @param pkcs8PrivateKey pkcs8标准私钥字符串
     * @return base64加密后的签名
     */
    public static String signAsBase64WithPkcs8Key(SignAlgorithm algorithm, String data, String pkcs8PrivateKey){
        return Base64Util.encodeAsStr(signWithPkcs8Key(algorithm, data, pkcs8PrivateKey));
    }

    /**
     * 验证签名
     * @param data 加密数据
     * @param publicKey 公钥
     * @param sign 签名
     * @param algorithm 签名算法
     * @return 是否匹配
     */
    public static boolean checkSign(String algorithm, byte[] data, PublicKey publicKey, byte[] sign){
        try {
            Signature signature = Signature.getInstance(algorithm);
            signature.initVerify(publicKey);
            signature.update(data);
            return signature.verify(sign);
        }
        catch (GeneralSecurityException e){
            throw new CryptoException(e);
        }
    }

    public static boolean checkSign(SignAlgorithm algorithm, byte[] data, PublicKey publicKey, byte[] sign){
        return checkSign(algorithm.getValue(), data, publicKey, sign);
    }

    public static boolean checkSign(SignAlgorithm algorithm, byte[] data, byte[] publicKey, byte[] sign){
        return checkSign(algorithm, data, KeyUtil.createPublicKey(publicKey), sign);
    }

    /**
     * 验证16进制签名是否合法
     * @param algorithm 肃反
     * @param data 待验证数据
     * @param publicKey 公钥
     * @param sign 16进制加密的签名
     * @return 是否合法
     */
    public static boolean checkSignAsHex(SignAlgorithm algorithm, byte[] data, byte[] publicKey, String sign){
        byte[] unSign = HexUtil.decodeHex(sign);
        return checkSign(algorithm, data, publicKey, unSign);
    }

    /**
     * 验证base64签名是否合法
     * @param algorithm 肃反
     * @param data 待验证数据
     * @param publicKey 公钥
     * @param sign base64加密的签名
     * @return 是否合法
     */
    public static boolean checkSignAsBase64(SignAlgorithm algorithm, byte[] data, byte[] publicKey, String sign){
        byte[] unSign = Base64Util.decode(sign);
        return checkSign(algorithm, data, publicKey, unSign);
    }

    /**
     * 验证签名
     * @param algorithm 算法
     * @param data 数据
     * @param publicKey x509标准公钥
     * @param sign base64加密的签名
     * @return 是否通过
     */
    public static boolean checkSign(SignAlgorithm algorithm, String data, String publicKey, String sign){
        try {
            byte[] dataBytes = StringUtil.getBytes(data, DEFAULT_CHARSET);
            PublicKey pk = KeyUtil.createPublicKeyX509(algorithm.getEncryption(), publicKey);
            byte[] decodedSign = Base64Util.decode(sign);
            return checkSign(algorithm, dataBytes, pk, decodedSign);
        }
        catch (Exception e){
            throw new CryptoException(e, "check failed, algorithm={}, data={}, publicKey={}, sign={}", algorithm, data, publicKey, sign);
        }
    }

}

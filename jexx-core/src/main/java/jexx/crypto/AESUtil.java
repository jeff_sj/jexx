package jexx.crypto;

import jexx.crypto.symmetric.AES;
import jexx.util.StringUtil;

import java.security.Provider;
import java.security.Security;

public class AESUtil {

    /**
     * 加密
     * @param data 数据
     * @param key key
     */
    public static byte[] encrypt(byte[] data, byte[] key, String transformation, Provider provider) {
        if(provider != null){
            Security.addProvider(provider);
        }
        return new AES(transformation, key).encryptAsBase64(data);
    }

    public static byte[] encrypt(String data, String key, String transformation, Provider provider) {
        return encrypt(StringUtil.getBytes(data), StringUtil.getBytes(key), transformation, provider);
    }

    public static byte[] encrypt(byte[] data, byte[] key) {
        return encrypt(data, key, "AES/ECB/PKCS5Padding", null);
    }

    public static byte[] encrypt(String data, String key) {
        return encrypt(StringUtil.getBytes(data), StringUtil.getBytes(key));
    }

    /**
     * 解密
     * @param data 数据
     * @param key key
     */
    public static byte[] decrypt(byte[] data, byte[] key, String transformation, Provider provider) {
        if(provider != null){
            Security.addProvider(provider);
        }
        return new AES(transformation, key).decryptAsBase64(data);
    }

    public static byte[] decrypt(String data, String key, String transformation, Provider provider) {
        return decrypt(StringUtil.getBytes(data), StringUtil.getBytes(key), transformation, provider);
    }

    public static byte[] decrypt(byte[] data, byte[] key) {
        return decrypt(data, key, "AES/ECB/PKCS5Padding", null);
    }

    public static byte[] decrypt(String data, String key) {
        return decrypt(StringUtil.getBytes(data), StringUtil.getBytes(key));
    }

}

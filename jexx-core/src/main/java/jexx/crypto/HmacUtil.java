package jexx.crypto;

import jexx.codec.Hex;
import jexx.exception.IORuntimeException;
import jexx.util.StringUtil;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

public class HmacUtil {

    private static final int STREAM_BUFFER_LENGTH = 1024;

    public static byte[] hmac(final byte[] data, final byte[] key, final HmacAlgorithms algorithms) {
        Mac mac = getInitializedMac(algorithms, key);
        return mac.doFinal(data);
    }

    public static byte[] hmac(final InputStream data, final byte[] key, final HmacAlgorithms algorithms){
        Mac mac = getInitializedMac(algorithms, key);
        return updateHmac(mac, data).doFinal();
    }

    public static byte[] hmac(final String data, final String key, final HmacAlgorithms algorithms){
        return hmac(StringUtil.getBytes(data), StringUtil.getBytes(key), algorithms);
    }

    public static String hmacHex(final byte[] data, final byte[] key, final HmacAlgorithms algorithms) {
        return Hex.encodeHexString(hmac(data, key, algorithms));
    }

    public static String hmacHex(final InputStream data, final byte[] key, final HmacAlgorithms algorithms){
        return Hex.encodeHexString(hmac(data, key, algorithms));
    }

    public static String hmacHex(final String data, final String key, final HmacAlgorithms algorithms){
        return Hex.encodeHexString(hmac(data, key, algorithms));
    }

    //---------------------------------------------------HmacSHA256

    public static byte[] hmacSha256(final byte[] data, final byte[] key) {
        return hmac(data, key, HmacAlgorithms.HMAC_SHA_256);
    }

    public static byte[] hmacSha256(final InputStream data, final byte[] key){
        return hmac(data, key, HmacAlgorithms.HMAC_SHA_256);
    }

    public static byte[] hmacSha256(final String data, final String key){
        return hmac(data, key, HmacAlgorithms.HMAC_SHA_256);
    }

    public static String hmacSha256Hex(final byte[] data, final byte[] key) {
        return hmacHex(data, key, HmacAlgorithms.HMAC_SHA_256);
    }

    public static String hmacSha256Hex(final InputStream data, final byte[] key){
        return hmacHex(data, key, HmacAlgorithms.HMAC_SHA_256);
    }

    public static String hmacSha256Hex(final String data, final String key){
        return hmacHex(data, key, HmacAlgorithms.HMAC_SHA_256);
    }


    private static Mac getInitializedMac(final HmacAlgorithms algorithm, final byte[] key) {
        if (key == null) {
            throw new IllegalArgumentException("Null key");
        }

        try {
            final SecretKeySpec keySpec = new SecretKeySpec(key, algorithm.toString());
            final Mac mac = Mac.getInstance(algorithm.toString());
            mac.init(keySpec);
            return mac;
        } catch (NoSuchAlgorithmException | InvalidKeyException e) {
            throw new IllegalArgumentException(e);
        }
    }

    private static Mac updateHmac(final Mac mac, final InputStream inputStream) {
        try {
            mac.reset();
            final byte[] buffer = new byte[STREAM_BUFFER_LENGTH];
            int read = inputStream.read(buffer, 0, STREAM_BUFFER_LENGTH);

            while (read > -1) {
                mac.update(buffer, 0, read);
                read = inputStream.read(buffer, 0, STREAM_BUFFER_LENGTH);
            }
            return mac;
        }
        catch (IOException e){
            throw new IORuntimeException(e);
        }
    }


}

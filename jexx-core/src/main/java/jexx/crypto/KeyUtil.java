package jexx.crypto;

import jexx.exception.UtilException;
import jexx.io.FileUtil;
import jexx.io.IOUtil;
import jexx.util.Base64Util;
import jexx.util.StringUtil;

import java.io.File;
import java.io.InputStream;
import java.security.*;
import java.security.cert.Certificate;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Enumeration;

/**
 * 密钥工具类
 */
public class KeyUtil {

    public static final String DEFAULT_KEY_ALGORITHM = "RSA";

    /**
     * 从证书中获取密钥对
     * @param certificate 证书
     * @param password 密码, 为null不需要密码
     * @return 密钥对
     */
    public static KeyPair getKeyPair(File certificate, String password){
        return getKeyPair(FileUtil.newBufferedInputStream(certificate), password);
    }

    /**
     * 从证书中获取密钥对
     * @param certificateInputStream 证书流
     * @param password 密码, 为null不需要密码
     * @return 密钥对
     */
    public static KeyPair getKeyPair(InputStream certificateInputStream, String password){
        try {
            char[] cPassword = null;
            if(StringUtil.isNotBlank(password)){
                cPassword = password.toCharArray();
            }

            KeyStore keyStore = KeyStore.getInstance("PKCS12");
            keyStore.load(certificateInputStream, cPassword);

            Enumeration<String> aliases =  keyStore.aliases();
            String keyAlias = null;
            if(aliases.hasMoreElements()){
                keyAlias = aliases.nextElement();
            }

            PrivateKey privateKey = (PrivateKey) keyStore.getKey(keyAlias, cPassword);

            Certificate cert = keyStore.getCertificate(keyAlias);
            PublicKey publicKey = cert.getPublicKey();
            return new KeyPair(publicKey, privateKey);
        }
        catch (Exception e){
            throw new UtilException(e);
        }
        finally {
            IOUtil.closeQuietly(certificateInputStream);
        }
    }

    /**
     * RSA算法创建密钥对
     * @return 密钥对
     */
    public static KeyPair createKeyPair(){
        return createKeyPair(DEFAULT_KEY_ALGORITHM, 1024);
    }

    /**
     * 根据算法创建密钥对
     * @param algorithm 算法
     * @param keySize 长度
     * @return 密钥对
     */
    public static KeyPair createKeyPair(String algorithm, int keySize){
        KeyPairGenerator myKeyGen;
        try {
            myKeyGen = KeyPairGenerator.getInstance(algorithm);
            myKeyGen.initialize(keySize);
            return myKeyGen.generateKeyPair();
        } catch (NoSuchAlgorithmException e) {
            throw new CryptoException(e);
        }
    }

    /**
     * RSA构建私钥
     * @param key 私钥key
     * @return 私钥
     */
    public static PrivateKey createPrivateKey(byte[] key){
       return createPrivateKey("RSA", key);
    }

    /**
     * 指定算法构建私钥
     * @param algorithm 算法
     * @param key 私钥key
     * @return 私钥
     */
    public static PrivateKey createPrivateKey(String algorithm, byte[] key){
        return createPrivateKeyPKCS8(algorithm, key);
    }

    /**
     * 生成PKCS8标准的私钥
     * @param algorithm 算法
     * @param privateKey 未加密私钥
     * @return PKCS8标准的私钥
     */
    public static PrivateKey createPrivateKeyPKCS8(String algorithm, byte[] privateKey){
        try {
            PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(privateKey);
            KeyFactory keyFactory = KeyFactory.getInstance(algorithm);
            return keyFactory.generatePrivate(keySpec);
        }
        catch (GeneralSecurityException e){
            throw new CryptoException(e);
        }
    }

    /**
     * 将私钥字符串进行Base64解码之后, 生成PKCS8标准的私钥
     * @param algorithm 算法
     * @param privateKey 加密私钥字符串
     * @return PKCS8标准的私钥
     */
    public static PrivateKey createPrivateKeyPKCS8(String algorithm, String privateKey){
        byte[] key = Base64Util.decode(privateKey);
        return createPrivateKeyPKCS8(algorithm, key);
    }

    /**
     * RSA构建公钥
     * @param key 公钥key
     * @return 公钥
     */
    public static PublicKey createPublicKey(byte[] key){
        return createPublicKey(DEFAULT_KEY_ALGORITHM, key);
    }

    /**
     * 指定算法构建公钥
     * @param algorithm 算法
     * @param key 公钥key
     * @return 公钥
     */
    public static PublicKey createPublicKey(String algorithm, byte[] key){
        return createPublicKeyX509(algorithm, key);
    }

    /**
     * 指定算法,生成X509标准公钥
     * @param algorithm 算法
     * @param publicKey 未加密公钥key
     * @return X509标准公钥
     */
    public static PublicKey createPublicKeyX509(String algorithm, byte[] publicKey){
        try {
            X509EncodedKeySpec keySpec = new X509EncodedKeySpec(publicKey);
            KeyFactory keyFactory = KeyFactory.getInstance(algorithm);
            return keyFactory.generatePublic(keySpec);
        }
        catch (GeneralSecurityException e){
            throw new CryptoException(e);
        }
    }

    /**
     * 公钥字符串base64解码后, 生成X509标准公钥
     * @param algorithm 算法
     * @param publicKey base64加密公钥key字符串
     * @return X509标准公钥
     */
    public static PublicKey createPublicKeyX509(String algorithm, String publicKey){
        byte[] decodedKey = Base64Util.decode(publicKey);
        return createPublicKeyX509(algorithm, decodedKey);
    }

    public static PublicKey createPublicKeyByX509(KeyFactory keyFactory, final byte[] x509Bytes){
        X509EncodedKeySpec keySpec = new X509EncodedKeySpec(x509Bytes);
        try {
            return keyFactory.generatePublic(keySpec);
        } catch (InvalidKeySpecException e) {
            throw new CryptoException(e);
        }
    }



}

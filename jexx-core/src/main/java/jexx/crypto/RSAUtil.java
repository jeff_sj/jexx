package jexx.crypto;

import jexx.exception.IORuntimeException;
import jexx.exception.UtilException;
import jexx.io.FileUtil;
import jexx.io.IOUtil;
import jexx.util.Base64Util;
import jexx.util.HexUtil;

import javax.crypto.Cipher;
import java.io.*;
import java.net.URL;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

/**
 * rsa工具类
 * @author jeff
 */
public class RSAUtil {

    /**
     * pem文件中密钥开始结束行的标记
     */
    public static final String PEM_PREFIX = "--";

    /**
     * 读取pem密钥文件
     */
    public static String readPemFile(File pemFile){
        try(BufferedReader reader = FileUtil.newBufferedReader(pemFile)){
            StringBuilder sb = new StringBuilder();
            String line;
            while((line = reader.readLine()) != null){
                if(!line.startsWith(PEM_PREFIX)){
                    sb.append(line);
                }
            }
            return sb.toString();
        }
        catch (IOException e){
            throw new IORuntimeException(e);
        }
    }

    /**
     * 读取pem密钥文件
     */
    public static String readPemFile(URL url){
        try (InputStream inputStream = url.openStream()){
            return readPemFile(inputStream);
        }
        catch (IOException e){
            throw new IORuntimeException(e);
        }
    }

    /**
     * 从流中读取pem文件密钥
     * @param inputStream 密钥流
     * @return 密钥
     */
    public static String readPemFile(InputStream inputStream){
        try(BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream))){
            StringBuilder sb = new StringBuilder();
            String line;
            while((line = reader.readLine()) != null){
                if(!line.startsWith(PEM_PREFIX)){
                    sb.append(line);
                }
            }
            return sb.toString();
        }
        catch (IOException e){
            throw new IORuntimeException(e);
        }
        finally {
            IOUtil.closeQuietly(inputStream);
        }
    }

    /**
     * key加密
     * @param content 数据
     * @param privateKey 私钥, 确保无换行等字符存在
     * @return 加密后内容
     */
    public static byte[] encrypt(byte[] content, String privateKey){
        try {
            //java默认"RSA"="RSA/ECB/PKCS1Padding"
            Cipher cipher = Cipher.getInstance("RSA");
            cipher.init(Cipher.ENCRYPT_MODE, getPrivateKey(privateKey));
            return cipher.doFinal(content);
        }
        catch (Exception e){
            throw new UtilException(e);
        }
    }

    /**
     * key加密后内容转换16进制字符串
     * @param content 数据
     * @param privateKey 私钥, 确保无换行等字符存在
     * @return 加密后内容
     */
    public static String encryptAsHexStr(byte[] content, String privateKey){
        return HexUtil.encodeHexStr(encrypt(content, privateKey));
    }

    /**
     * 私钥或者公钥解密
     * @param content 内容
     * @param publicKey 公钥, 确保无换行等字符存在
     * @return 解密后内容
     */
    public static byte[] decrypt(byte[] content, String publicKey){
        try {
            Cipher cipher = Cipher.getInstance("RSA");
            cipher.init(Cipher.DECRYPT_MODE, getPublicKey(publicKey));
            return cipher.doFinal(content);
        }
        catch (Exception e){
            throw new UtilException(e);
        }
    }

    /**
     * 私钥或者公钥解密后转化为16进制字符串
     * @param content 内容
     * @param publicKey 公钥, 确保无换行等字符存在
     * @return 解密后内容
     */
    public static String decryptAsHexStr(byte[] content, String publicKey){
        return HexUtil.encodeHexStr(decrypt(content, publicKey));
    }

    /**
     * 获取私钥
     */
    protected static PrivateKey getPrivateKey(String privateKey){
        try {
            byte[] keyBytes = Base64Util.decode(privateKey);
            if(keyBytes == null){
                return null;
            }

            PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(keyBytes);
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            return keyFactory.generatePrivate(keySpec);
        }
        catch (Exception e){
            throw new UtilException(e);
        }
    }

    /**
     * 获取公钥
     */
    protected static PublicKey getPublicKey(String publicKey){
        try {
            byte[] keyBytes = Base64Util.decode(publicKey);
            if(keyBytes == null){
                return null;
            }

            X509EncodedKeySpec keySpec = new X509EncodedKeySpec(keyBytes);
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            return keyFactory.generatePublic(keySpec);
        }
        catch (Exception e){
            throw new UtilException(e);
        }
    }

}

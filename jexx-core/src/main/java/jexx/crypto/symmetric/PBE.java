package jexx.crypto.symmetric;

import jexx.crypto.BaseCipher;
import jexx.crypto.CryptoException;
import jexx.util.StringUtil;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;
import java.security.Key;
import java.security.spec.AlgorithmParameterSpec;

/**
 * @author jeff
 * @since 2020/5/8
 */
public class PBE extends BaseCipher {

    /** 需要8个字节的salt */
    public static final String PBE_With_MD5_And_DES = "PBEWithMD5AndDES";
    /** 需要8个字节的salt */
    public static final String PBE_With_MD5_And_Triple_DES = "PBEWithMD5AndTripleDES";
    public static final String PBE_With_SHA1_And_DESede = "PBEWithSHA1AndDESede";
    public static final String PBE_With_SHA1_And_RC2_40 = "PBEWithSHA1AndRC2_40";

    private char[] password;
    private Key key;
    private AlgorithmParameterSpec spec;

    /**
     * 使用默认 PBEWithMD5AndDES 构造
     * @param password 密码
     * @param salt 盐
     */
    public PBE(String password, String salt) {
        this(PBE.PBE_With_MD5_And_DES, password, salt);
    }

    /**
     * PBE构造, 常用算法可使用枚举
     * @param transformation 算法, 如 PBEWithMD5AndDES
     * @param password 密码
     * @param salt 盐
     */
    public PBE(String transformation, char[] password, byte[] salt) {
        super(transformation);
        this.password = password;
        this.key = generateKey(password);
        this.spec = new PBEParameterSpec(salt, 100);
    }

    /**
     * 使用默认 PBEWithMD5AndDES 构造
     * @param transformation 算法
     * @param password 密码
     * @param salt 盐
     */
    public PBE(String transformation, String password, String salt) {
        this(transformation, password.toCharArray(), StringUtil.getBytes(salt));
    }

    @Override
    public byte[] encrypt(byte[] data){
        return encrypt(key, data, spec);
    }

    @Override
    public byte[] decrypt(byte[] data){
        return decrypt(key, data, spec);
    }

    private Key generateKey(char[] password){
        try {
            PBEKeySpec keySpec = new PBEKeySpec(password);
            SecretKeyFactory factory = SecretKeyFactory.getInstance(this.transformation);
            Key key = factory.generateSecret(keySpec);
            return key;
        }
        catch (Exception e){
            throw new CryptoException(e);
        }
    }

}

package jexx.crypto.symmetric;

import jexx.crypto.BaseCipher;
import jexx.crypto.CryptoException;
import jexx.util.Assert;
import jexx.util.StringUtil;

import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.security.Key;
import java.security.spec.AlgorithmParameterSpec;

/**
 * @author jeff
 * @since 2020/5/8
 */
public class AES extends BaseCipher {

    public static final String AES_CBC_NoPadding = "AES/CBC/NoPadding";
    public static final String AES_CBC_PKCS5Padding = "AES/CBC/PKCS5Padding";
    public static final String AES_ECB_NoPadding = "AES/ECB/NoPadding";
    public static final String AES_ECB_PKCS5Padding = "AES/ECB/PKCS5Padding";

    public static final String ALGORITHM = "AES";

    private Key key;
    private AlgorithmParameterSpec spec;
    private boolean needIvs;

    /**
     * 使用默认 AES/CBC/PKCS5Padding 构造
     */
    public AES(byte[] password) {
        this(AES_CBC_PKCS5Padding, password);
    }

    /**
     * 使用默认 AES/CBC/PKCS5Padding 构造
     */
    public AES(String password) {
        this(AES_CBC_PKCS5Padding, password);
    }

    /**
     * DES构造
     * @param transformation 转换的名称，例如 DES/CBC/PKCS5Padding
     * @param password 密码
     */
    public AES(String transformation, byte[] password) {
        super(transformation);
        Assert.isTrue(transformation.startsWith("AES/"), "AES no support {}", transformation);
        this.needIvs = checkCBC(transformation);
        this.key = generateKey(password);
    }

    /**
     * DES构造
     * @param transformation 转换的名称，例如 DES/CBC/PKCS5Padding
     * @param password 密码
     */
    public AES(String transformation, String password) {
        this(transformation, StringUtil.getBytes(password));
    }

    /**
     * 设置偏移向量
     * @param ivs 偏移向量
     * @return {@link DES}
     */
    public AES withIv(byte[] ivs){
        Assert.isTrue(needIvs, "transformation[{}] cannot use IV", transformation);
        Assert.isTrue(ivs.length == 16, "IV length must be  16 bytes long", transformation);
        this.spec = new IvParameterSpec(ivs);
        return this;
    }

    /**
     * 设置便宜向量
     * @param ivs 偏移向量
     * @return {@link DES}
     */
    public AES withIv(String ivs){
        return withIv(StringUtil.getBytes(ivs));
    }

    @Override
    public byte[] encrypt(byte[] data){
        return needIvs ? encrypt(key, data, spec) : encrypt(key, data);
    }

    @Override
    public byte[] decrypt(byte[] data){
        return needIvs ? decrypt(key, data, spec) : decrypt(key, data);
    }

    /**
     * 检测是否为ECB模式, ECB不需要便宜向量
     * @param transformation 转换的名称
     * @return bool
     */
    private boolean checkCBC(String transformation){
        return transformation.contains("/CBC/");
    }

    private Key generateKey(byte[] password){
        try {
            Key key = new SecretKeySpec(password , ALGORITHM);
            return key;
        }
        catch (Exception e){
            throw new CryptoException(e);
        }
    }

}

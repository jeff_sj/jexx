package jexx.crypto.symmetric;

import jexx.crypto.BaseCipher;
import jexx.crypto.CryptoException;
import jexx.util.Assert;
import jexx.util.StringUtil;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.IvParameterSpec;
import java.security.Key;
import java.security.spec.AlgorithmParameterSpec;

/**
 * DES加解密
 * @author jeff
 * @since 2020/5/8
 */
public class DES extends BaseCipher {

    public static final String DES_CBC_NoPadding = "DES/CBC/NoPadding";
    public static final String DES_CBC_PKCS5Padding = "DES/CBC/PKCS5Padding";
    public static final String DES_ECB_NoPadding = "DES/ECB/NoPadding";
    public static final String DES_ECB_PKCS5Padding = "DES/ECB/PKCS5Padding";

    public static final String ALGORITHM = "DES";

    private Key key;
    private AlgorithmParameterSpec spec;
    private boolean needIvs;

    /**
     * 使用默认 DES/CBC/PKCS5Padding 构造
     */
    public DES(byte[] password) {
        this(DES_CBC_PKCS5Padding, password);
    }

    /**
     * 使用默认 DES/CBC/PKCS5Padding 构造
     */
    public DES(String password) {
        this(DES_CBC_PKCS5Padding, password);
    }

    /**
     * DES构造, 常用算法可使用枚举
     * @param transformation 转换的名称，例如 DES/CBC/PKCS5Padding
     * @param password 密码
     */
    public DES(String transformation, byte[] password) {
        super(transformation);
        Assert.isTrue(transformation.startsWith("DES/"), "DES no support {}", transformation);
        this.needIvs = checkCBC(transformation);
        this.key = generateKey(password);
    }

    /**
     * DES构造, 常用算法可使用枚举
     * @param transformation 转换的名称，例如 DES/CBC/PKCS5Padding
     * @param password 密码
     */
    public DES(String transformation, String password) {
        this(transformation, StringUtil.getBytes(password));
    }

    /**
     * 设置便宜向量
     * @param ivs 偏移向量
     * @return {@link DES}
     */
    public DES withIv(byte[] ivs){
        Assert.isTrue(needIvs, "transformation[{}] cannot use IV", transformation);
        Assert.isTrue(ivs.length == 8, "IV length must be  8 bytes long", transformation);
        this.spec = new IvParameterSpec(ivs);
        return this;
    }

    /**
     * 设置便宜向量
     * @param ivs 偏移向量
     * @return {@link DES}
     */
    public DES withIv(String ivs){
        return withIv(StringUtil.getBytes(ivs));
    }

    @Override
    public byte[] encrypt(byte[] data){
        return needIvs ? encrypt(key, data, spec) : encrypt(key, data);
    }

    @Override
    public byte[] decrypt(byte[] data){
        return needIvs ? decrypt(key, data, spec) : decrypt(key, data);
    }

    /**
     * 检测是否为ECB模式, ECB不需要偏移向量
     * @param transformation 转换的名称
     * @return bool
     */
    private boolean checkCBC(String transformation){
        return transformation.contains("/CBC/");
    }

    private Key generateKey(byte[] password){
        try {
            DESKeySpec keySpec = new DESKeySpec(password);
            SecretKeyFactory keyFactory = SecretKeyFactory.getInstance(ALGORITHM);
            return keyFactory.generateSecret(keySpec);
        }
        catch (Exception e){
            throw new CryptoException(e);
        }
    }

}

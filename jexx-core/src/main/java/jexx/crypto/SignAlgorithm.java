package jexx.crypto;

/**
 * 签名算法
 */
public enum SignAlgorithm {

    // The RSA signature algorithm
    NONEwithRSA("NONEwithRSA", "", "RSA"),

    // The MD2/MD5 with RSA Encryption signature algorithm
    MD2withRSA("MD2withRSA", "MD2", "RSA"),
    MD5withRSA("MD5withRSA", "MD5", "RSA"),

    // The signature algorithm with SHA-* and the RSA
    SHA1withRSA("SHA1withRSA", "SHA1", "RSA"),
    SHA256withRSA("SHA256withRSA", "SHA256", "RSA"),
    SHA384withRSA("SHA384withRSA", "SHA384", "RSA"),
    SHA512withRSA("SHA512withRSA", "SHA512", "RSA"),

    // The Digital Signature Algorithm
    NONEwithDSA("NONEwithDSA", "", "DSA"),
    // The DSA with SHA-1 signature algorithm
    SHA1withDSA("SHA1withDSA", "SHA1", "DSA"),

    // The ECDSA signature algorithms
    NONEwithECDSA("NONEwithECDSA", "", "ECDSA"),
    SHA1withECDSA("SHA1withECDSA", "SHA1", "ECDSA"),
    SHA256withECDSA("SHA256withECDSA", "SHA256", "ECDSA"),
    SHA384withECDSA("SHA384withECDSA", "SHA384", "ECDSA"),
    SHA512withECDSA("SHA512withECDSA", "SHA512", "ECDSA");

    private String value;
    private String sign;
    private String encryption;

    /**
     * 构造
     *
     * @param value 算法字符表示，区分大小写
     */
    SignAlgorithm(String value, String sign, String encryption) {
        this.value = value;
        this.sign = sign;
        this.encryption = encryption;
    }

    /**
     * 获取算法字符串表示，区分大小写
     *
     * @return 算法字符串表示
     */
    public String getValue() {
        return this.value;
    }

    /**
     * 签名算法,可能为空
     */
    public String getSign() {
        return sign;
    }

    /**
     * 加密算法
     */
    public String getEncryption() {
        return encryption;
    }
}

package jexx.template;

import java.util.Map;

public class MapTemplateParser extends StringTemplateParser {

	public ContextTemplateParser of(final Map<String,Object> map) {
		return template -> parseWithMap(template, map);
	}

	public String parseWithMap(final String template, final Map<String,Object> map) {
		return super.parse(template, macroName -> {
			Object value = map.get(macroName);
			if (value == null) {
				return null;
			}
			return value.toString();
		});
	}
}
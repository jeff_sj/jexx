package jexx.exception;

import jexx.util.StringUtil;

/**
 * 依赖异常
 */
public class DependencyException extends RuntimeException {

    public DependencyException(Throwable e) {
        super(e);
    }

    public DependencyException(String message) {
        super(message);
    }

    public DependencyException(String messageTemplate, Object... params) {
        super(StringUtil.substitute(messageTemplate, params));
    }

    public DependencyException(String message, Throwable throwable) {
        super(message, throwable);
    }

    public DependencyException(Throwable throwable, String messageTemplate, Object... params) {
        super(StringUtil.substitute(messageTemplate, params), throwable);
    }

}

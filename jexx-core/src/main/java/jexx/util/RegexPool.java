package jexx.util;

import java.util.regex.Pattern;

/**
 * 正则常量池
 * <p>
 *     {@link Pattern} 是线程安全的;
 *     由{@link Pattern}生成的 {@link java.util.regex.Matcher} 非线程安全
 * </p>
 * @author jeff
 * @since 2019/9/23
 */
public final class RegexPool {

    /**
     * 英文字母，数字和下划线
     */
    public static final Pattern WORD = Pattern.compile("[A-Za-z0-9_]+");

    /**
     * 字母
     */
    public static final Pattern LETTER = Pattern.compile("[a-zA-Z]+");

    /**
     * RFC 5322 Official Standard; http://emailregex.com/
     */
    public static final Pattern EMAIL = Pattern.compile("(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])",
            Pattern.CASE_INSENSITIVE);

    /**
     * IP
     */
    public static final Pattern IP = Pattern.compile("((?:(?:25[0-5]|2[0-4]\\d|[01]?\\d?\\d)\\.){3}(?:25[0-5]|2[0-4]\\d|[01]?\\d?\\d))");

    /**
     * 禁止输入含有~的字符
     */
    public static final Pattern NO_TILDE = Pattern.compile("[^~\\x22]+");

    /**
     * 基本汉字
     */
    public static final Pattern CHINESE = Pattern.compile("[\u4E00-\u9FA5]+");

    /**
     * 是否为中国大陆移动号码, 匹配所有支持短信功能的号码（手机卡 + 上网卡）
     * https://github.com/VincentSit/ChinaMobilePhoneNumberRegex/blob/master/README-CN.md
     */
    public static final Pattern CHINESE_MOBILE_PHONE = Pattern.compile("^(?:\\+?86)?1(?:3\\d{3}|5[^4\\D]\\d{2}|8\\d{3}|7(?:[0-35-9]\\d{2}|4(?:0\\d|1[0-2]|9\\d))|9[0-35-9]\\d{2}|6[2567]\\d{2}|4[579]\\d{2})\\d{6}$");

    /**
     * 中国大陆18位身份证号码
     */
    public final static Pattern CHINESE_ID_CARD = Pattern.compile("[1-9]\\d{5}[1-2]\\d{3}((0\\d)|(1[0-2]))(([0|1|2]\\d)|3[0-1])\\d{3}(\\d|X|x)");


}

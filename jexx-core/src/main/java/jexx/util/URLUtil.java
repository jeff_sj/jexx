package jexx.util;

import jexx.exception.UtilException;
import jexx.lang.Charsets;

import java.io.UnsupportedEncodingException;
import java.net.*;
import java.nio.charset.Charset;

/**
 * 统一定位符工具类
 */
public class URLUtil {

    /** 针对ClassPath路径的伪协议前缀（兼容Spring）: "classpath:" */
    public static final String CLASSPATH_URL_PREFIX = "classpath:";
    /** URL 前缀表示文件: "file:" */
    public static final String FILE_URL_PREFIX = "file:";
    /** URL 前缀表示jar: "jar:" */
    public static final String JAR_URL_PREFIX = "jar:";
    /** URL 前缀表示war: "war:" */
    public static final String WAR_URL_PREFIX = "war:";
    /** URL 协议表示文件: "file" */
    public static final String URL_PROTOCOL_FILE = "file";
    /** URL 协议表示Jar文件: "jar" */
    public static final String URL_PROTOCOL_JAR = "jar";
    /** URL 协议表示zip文件: "zip" */
    public static final String URL_PROTOCOL_ZIP = "zip";
    /** URL 协议表示WebSphere文件: "wsjar" */
    public static final String URL_PROTOCOL_WSJAR = "wsjar";
    /** URL 协议表示JBoss zip文件: "vfszip" */
    public static final String URL_PROTOCOL_VFSZIP = "vfszip";
    /** URL 协议表示JBoss文件: "vfsfile" */
    public static final String URL_PROTOCOL_VFSFILE = "vfsfile";
    /** URL 协议表示JBoss VFS资源: "vfs" */
    public static final String URL_PROTOCOL_VFS = "vfs";
    /** Jar路径以及内部文件路径的分界符: "!/" */
    public static final String JAR_URL_SEPARATOR = "!/";
    /** WAR路径及内部文件路径分界符 */
    public static final String WAR_URL_SEPARATOR = "*/";

    /**
     * 编码URL，默认使用UTF-8编码<br>
     * 将需要转换的内容（ASCII码形式之外的内容），用十六进制表示法转换出来，并在之前加上%开头。
     * @param url URL
     * @return 编码后的URL
     */
    public static String encode(String url){
        return encode(url, Charsets.name(Charsets.UTF_8));
    }

    /**
     * 编码URL<br>
     * 将需要转换的内容（ASCII码形式之外的内容），用十六进制表示法转换出来，并在之前加上%开头。
     * @param url URL
     * @param charset 编码
     * @return 编码后的URL
     */
    public static String encode(String url, String charset){
        try {
            return URLEncoder.encode(url, charset);
        } catch (UnsupportedEncodingException e) {
            throw new UtilException(e);
        }
    }

    public static String encode(String url, Charset charset){
        return encode(url, ObjectUtil.notNullOrDefault(charset, Charsets.defaultCharset()).name());
    }

    /**
     * 解码URL<br>
     * 将%开头的16进制表示的内容解码。
     * @param url URL
     * @return 解码后的URL
     */
    public static String decode(String url) throws UtilException {
        return decode(url, Charsets.name(Charsets.UTF_8));
    }

    /**
     * 解码URL<br>
     * 将%开头的16进制表示的内容解码。
     * @param url URL
     * @param charset 编码
     * @return 解码后的URL
     */
    public static String decode(String url, String charset){
        try {
            return URLDecoder.decode(url, charset);
        } catch (UnsupportedEncodingException e) {
            throw new UtilException(e);
        }
    }

    public static String decode(String url, Charset charset){
        return decode(url, ObjectUtil.notNullOrDefault(charset, Charsets.defaultCharset()).name());
    }

    /**
     * 从URL对象中获取不被编码的路径Path<br>
     * 对于本地路径，URL对象的getPath方法对于包含中文或空格时会被编码，导致本读路径读取错误。<br>
     * 此方法将URL转为URI后获取路径用于解决路径被编码的问题
     * @param url {@link URL}
     * @return 路径
     */
    public static String getDecodedPath(URL url) {
        String path = null;
        try {
            // URL对象的getPath方法对于包含中文或空格的问题
            path = toURI(url).getPath();
        } catch (UtilException e) {
            // ignore
        }
        return (null != path) ? path : url.getPath();
    }

    /**
     * 转URL为URI
     * @param url URL
     * @return URI
     */
    public static URI toURI(URL url){
        if (null == url) {
            return null;
        }
        try {
            return url.toURI();
        } catch (URISyntaxException e) {
            throw new UtilException(e);
        }
    }

    /**
     * 转字符串为URI
     * @param location 字符串路径
     * @return URI
     */
    public static URI toURI(String location){
        try {
            return new URI(location.replace(" ", "%20"));
        } catch (URISyntaxException e) {
            throw new UtilException(e);
        }
    }

}

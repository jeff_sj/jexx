package jexx.util;

import java.awt.*;
import java.util.Arrays;
import java.util.Locale;

/**
 * 字体工具类
 *
 * @author jeff
 * @since 2019/6/26
 */
public class FontUtil {

    private static final Locale DEFAULT_LOCALE = Locale.SIMPLIFIED_CHINESE;

    public static String[] getAvailableFontFamilyNames(Locale locale){
        return GraphicsEnvironment.getLocalGraphicsEnvironment().getAvailableFontFamilyNames(locale);
    }

    public static String[] getAvailableFontFamilyNames(){
        return getAvailableFontFamilyNames(DEFAULT_LOCALE);
    }

    public static Font[] getAllFonts(){
        return GraphicsEnvironment.getLocalGraphicsEnvironment().getAllFonts();
    }

    public static Font getFontByFamilyName(String familyName){
        Font[] fonts = getAllFonts();
        return Arrays.stream(fonts).filter(s -> s.getFontName().equals(familyName)).findFirst().orElse(null);
    }

    public static boolean hasFontFamilyName(String familyName, Locale locale){
        Font[] fonts = getAllFonts();
        return Arrays.stream(fonts).anyMatch(s -> s.getFontName(locale).equals(familyName));
    }

    public static boolean hasFontFamilyName(String familyName){
        Font[] fonts = getAllFonts();
        return Arrays.stream(fonts).anyMatch(s -> s.getFontName(DEFAULT_LOCALE).equals(familyName));
    }

}

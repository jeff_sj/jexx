package jexx.util;

import jexx.base.BasicType;
import jexx.exception.UtilException;
import jexx.io.FileUtil;
import jexx.lang.Charsets;

import java.io.IOException;
import java.lang.reflect.*;
import java.net.URI;
import java.net.URL;
import java.util.*;

public class ClassUtil {

    /**
     * 指定类是否与给定的类名相同
     *
     * @param clazz 类
     * @param className 类名，可以是全类名（包含包名），也可以是简单类名（不包含包名）
     * @param ignoreCase 是否忽略大小写
     * @return 指定类是否与给定的类名相同
     * @since 3.0.7
     */
    public static boolean equals(Class<?> clazz, String className, boolean ignoreCase) {
        if (null == clazz || StringUtil.isBlank(className)) {
            return false;
        }
        if (ignoreCase) {
            return className.equalsIgnoreCase(clazz.getName()) || className.equalsIgnoreCase(clazz.getSimpleName());
        } else {
            return className.equals(clazz.getName()) || className.equals(clazz.getSimpleName());
        }
    }

    /**
     * 判断类是否存在
     */
    public static boolean exist(String className){
        return exist(className, null);
    }

    public static boolean exist(String className, ClassLoader classLoader){
        try {
            forName(className, classLoader);
            return true;
        } catch (ClassNotFoundException e) {
            //skip
        }
        return false;
    }

    public static Class<?> forName(String name) throws ClassNotFoundException{
        return forName(name, null);
    }

    /**
     * 代替  Class.forName 来通过类名称返回class。name 可以是基本类型的类; 如果 name 是数组类，内部类, 请使用 {@link ClassUtil#loadClass(String)}
     * @param name 类名称
     * @param classLoader 类加载器
     * @return clazz
     * @throws ClassNotFoundException 类找不到异常
     */
    public static Class<?> forName(String name, ClassLoader classLoader) throws ClassNotFoundException{
        Assert.notNull(name, "name must not be null");

        Class<?> clazz = resolvePrimitiveClassName(name);
        if(clazz != null){
            return clazz;
        }

        ClassLoader clToUse = classLoader;
        if (clToUse == null) {
            clToUse = ClassLoaderUtil.getDefaultClassLoader();
        }
        return Class.forName(name, false, clToUse);
    }

    /**
     * @return 获得Java ClassPath路径，不包括 jre
     */
    public static String[] getJavaClassPaths() {
        return System.getProperty("java.class.path").split(System.getProperty("path.separator"));
    }

    /**
     * 获取对象的类型
     * @param obj   对象
     * @param <T>   类型
     * @return  返回类型
     */
    @SuppressWarnings("unchecked")
	public static <T> Class<T> getClass(T obj) {
        return null == obj ? null : (Class<T>) obj.getClass();
    }

    /**
     * 获得对象数组的类数组
     * @param objects 对象数组，如果数组中存在{@code null}元素，则此元素被认为是Object类型
     * @return 类数组
     */
    public static Class<?>[] getClasses(Object... objects) {
        Class<?>[] classes = new Class<?>[objects.length];
        Object obj;
        for (int i = 0; i < objects.length; i++) {
            obj = objects[i];
            classes[i] = (null == obj) ? Object.class : obj.getClass();
        }
        return classes;
    }

    /**
     * 获取类名
     *
     * @param obj 获取类名对象
     * @param isSimple 是否简单类名，如果为true，返回不带包名的类名
     * @return 类名
     * @since 3.0.7
     */
    public static String getClassName(Object obj, boolean isSimple) {
        if (null == obj) {
            return null;
        }
        final Class<?> clazz = obj.getClass();
        return getClassName(clazz, isSimple);
    }

    /**
     * 获取类名<br>
     * 类名并不包含“.class”这个扩展名<br>
     * 例如：ClassUtil这个类<br>
     *
     * <pre>
     * isSimple为false: "com.xiaoleilu.hutool.util.ClassUtil"
     * isSimple为true: "ClassUtil"
     * </pre>
     *
     * @param clazz 类
     * @param isSimple 是否简单类名，如果为true，返回不带包名的类名
     * @return 类名
     * @since 3.0.7
     */
    public static String getClassName(Class<?> clazz, boolean isSimple) {
        if (null == clazz) {
            return null;
        }
        return isSimple ? clazz.getSimpleName() : clazz.getName();
    }

    /**
     * 获得ClassPath，不解码路径中的特殊字符（例如空格和中文）
     *
     * @return ClassPath集合
     */
    public static Set<String> getClassPathResources() {
        return getClassPathResources(false);
    }

    /**
     * 获得ClassPath
     *
     * @param isDecode 是否解码路径中的特殊字符（例如空格和中文）
     * @return ClassPath集合
     * @since 4.0.11
     */
    public static Set<String> getClassPathResources(boolean isDecode) {
        return getClassPaths(StringPool.EMPTY, isDecode);
    }

    /**
     * 获得ClassPath，不解码路径中的特殊字符（例如空格和中文）
     *
     * @param packageName 包名称
     * @return ClassPath路径字符串集合
     */
    public static Set<String> getClassPaths(String packageName) {
        return getClassPaths(packageName, false);
    }

    /**
     * 获得ClassPath
     *
     * @param packageName 包名称
     * @param isDecode 是否解码路径中的特殊字符（例如空格和中文）
     * @return ClassPath路径字符串集合
     * @since 4.0.11
     */
    public static Set<String> getClassPaths(String packageName, boolean isDecode) {
        String packagePath = packageName.replace(StringPool.DOT, StringPool.SLASH);
        Enumeration<URL> resources;
        try {
            resources = ClassLoaderUtil.getClassLoader().getResources(packagePath);
        } catch (IOException e) {
            throw new UtilException(e, "Loading classPath [{}] error!", packagePath);
        }
        final Set<String> paths = new HashSet<>();
        String path;
        while (resources.hasMoreElements()) {
            path = resources.nextElement().getPath();
            paths.add(isDecode ? URLUtil.decode(path, Charsets.defaultCharsetName()) : path);
        }
        return paths;
    }

    /**
     * 获得ClassPath，将编码后的中文路径解码为原字符<br>
     * 这个ClassPath路径会文件路径被标准化处理
     *
     * @return ClassPath
     */
    public static String getClassPath() {
        return getClassPath(false);
    }

    /**
     * 获得ClassPath，这个ClassPath路径会文件路径被标准化处理
     *
     * @param isEncoded 是否编码路径中的中文
     * @return ClassPath
     * @since 3.2.1
     */
    public static String getClassPath(boolean isEncoded) {
        final URL classPathURL = getClassPathURL();
        String url = isEncoded ? classPathURL.getPath() : URLUtil.getDecodedPath(classPathURL);
        return FileUtil.normalize(url);
    }

    /**
     * 获得ClassPath URL
     *
     * @return ClassPath URL
     */
    public static URL getClassPathURL() {
        return ClassLoaderUtil.getClassLoader().getResource(StringPool.EMPTY);
    }

    /**
     * 获取指定类型分的默认值<br>
     * 默认值规则为：
     * <pre>
     * 1、如果为原始类型，返回0
     * 2、非原始类型返回{@code null}
     * </pre>
     *
     * @param clazz 类
     * @return 默认值
     * @since 3.0.8
     */
    public static Object getDefaultValue(Class<?> clazz) {
        if(clazz.isPrimitive()) {
            if(long.class == clazz) {
                return 0L;
            }else if(int.class == clazz) {
                return 0;
            }else if(short.class == clazz) {
                return (short)0;
            }else if(char.class == clazz) {
                return (char)0;
            }else if(byte.class == clazz) {
                return (byte)0;
            }else if(double.class == clazz) {
                return 0D;
            }else if(float.class == clazz) {
                return 0f;
            }else if(boolean.class == clazz) {
                return false;
            }
        }

        return null;
    }

    /**
     * 获得默认值列表
     * @param classes 值类型
     * @return 默认值列表
     * @since 3.0.9
     */
    public static Object[] getDefaultValues(Class<?>... classes) {
        final Object[] values = new Object[classes.length];
        for(int i = 0; i < classes.length; i++) {
            values[i] = getDefaultValue(classes[i]);
        }
        return values;
    }

    /**
     * 获得给定类所在包的名称
     * @param clazz 类
     * @return 包名
     */
    public static String getPackage(Class<?> clazz) {
        if (clazz == null) {
            return StringPool.EMPTY;
        }
        final String className = clazz.getName();
        int packageEndIndex = className.lastIndexOf(StringPool.DOT);
        if (packageEndIndex == -1) {
            return StringPool.EMPTY;
        }
        return className.substring(0, packageEndIndex);
    }

    /**
     * 获得给定类所在包的路径<br>
     * 例如：<br>
     * com.xiaoleilu.hutool.util.ClassUtil =》 com/xiaoleilu/hutool/util
     *
     * @param clazz 类
     * @return 包名
     */
    public static String getPackagePath(Class<?> clazz) {
        return getPackage(clazz).replace(StringPool.DOT, StringPool.SLASH);
    }

    /**
     * 获得给定类的第一个泛型参数
     *
     * @param clazz 被检查的类，必须是已经确定泛型类型的类
     * @return {@link Class}
     */
    public static Class<?> getTypeArgument(Class<?> clazz) {
        return getTypeArgument(clazz, 0);
    }

    /**
     * 获得给定类的泛型参数
     *
     * @param clazz 被检查的类，必须是已经确定泛型类型的类
     * @param index 泛型类型的索引号，既第几个泛型类型
     * @return {@link Class}
     */
    public static Class<?> getTypeArgument(Class<?> clazz, int index) {
        final Type argumentType = TypeUtil.getTypeArgument(clazz, index);
        if (argumentType instanceof Class) {
            return (Class<?>) argumentType;
        }
        return null;
    }


    /**
     * 比较判断types1和types2两组类，如果types1中所有的类都与types2对应位置的类相同，或者是其父类或接口，则返回<code>true</code>
     *
     * @param types1 类组1
     * @param types2 类组2
     * @return 是否相同、父类或接口
     */
    public static boolean isAllAssignableFrom(Class<?>[] types1, Class<?>[] types2) {
        if (ArrayUtil.isEmpty(types1) && ArrayUtil.isEmpty(types2)) {
            return true;
        }
        if (types1.length != types2.length) {
            return false;
        }

        Class<?> type1;
        Class<?> type2;
        for (int i = 0; i < types1.length; i++) {
            type1 = types1[i];
            type2 = types2[i];
            if(!isAssignableFrom(type1, type2)){
                return false;
            }
        }
        return true;
    }

    /**
     * 判断type1是type2的本身或者父类
     * 如  ClassUtil.isAssignableFrom(StringKeyMap.class, LinkedStringKeyMap.class)=true
     * @param type1 类
     * @param type2 类2
     * @return bool
     */
    public static boolean isAssignableFrom(Class<?> type1, Class<?> type2) {
        if(type1 == null || type2 == null){
            return false;
        }
        if(isBasicType(type1) && isBasicType(type2)) {
            //原始类型和包装类型存在不一致情况
            return unWrap(type1) == unWrap(type2);
        }
        return type1.isAssignableFrom(type2);
    }

    /**
     * 是否为基本类型, 包含基本类型的包装类
     * @param clazz 类型
     */
    public static boolean isBasicType(Class<?> clazz) {
        if (null == clazz) {
            return false;
        }
        return clazz.isPrimitive() || isPrimitiveWrapper(clazz);
    }

    /**
     * 是否为原始类型的包装类
     * @param clazz 类型
     * @return  验证是否
     */
	public static boolean isPrimitiveWrapper(Class<?> clazz) {
		if (null == clazz) {
			return false;
		}
		return BasicType.isWrapperPrimitiveClass(clazz);
	}

    /**
     * 是否为抽象类
     *
     * @param clazz 类
     * @return 是否为抽象类
     */
    public static boolean isAbstract(Class<?> clazz) {
        return Modifier.isAbstract(clazz.getModifiers());
    }

    /**
     * 是否为简单属性
     * <p>{@link #isSimpleValueType(Class)}，以及判定数组中的元素类型 </p>
     * @param type 待检测类型
     * @return 是否为简单属性
     */
    public static boolean isSimpleProperty(Class<?> type) {
        Assert.notNull(type, "'type' must not be null");
        return isSimpleValueType(type) || (type.isArray() && isSimpleValueType(type.getComponentType()));
    }

    /**
     * 是否为简单值类型
     * <pre>
     * a primitive or primitive wrapper, an enum, a String or other CharSequence, a Number, a Date, a URI, a URL, a Locale, or a Class.
     * </pre>
     * @param type 待检测类型
     * @return 是否为简单类型
     */
    public static boolean isSimpleValueType(Class<?> type) {
        return (type != void.class && type != Void.class &&
                (isPrimitiveWrapper(type) ||
                        Enum.class.isAssignableFrom(type) ||
                        CharSequence.class.isAssignableFrom(type) ||
                        Number.class.isAssignableFrom(type) ||
                        Date.class.isAssignableFrom(type) ||
                        URI.class == type ||
                        URL.class == type ||
                        Locale.class == type ||
                        Class.class == type));
    }

    /**
     * 是否为静态方法
     *
     * @param method 方法
     * @return 是否为静态方法
     */
    public static boolean isStatic(Method method) {
        return Modifier.isStatic(method.getModifiers());
    }

    /**
     * 是否为标准的类<br>
     * 这个类必须：
     * <pre>
     * 1、非接口
     * 2、非抽象类
     * 3、非Enum枚举
     * 4、非数组
     * 5、非注解
     * 6、非原始类型（int, long等）
     * </pre>
     *
     * @param clazz 类
     * @return 是否为标准类
     */
    public static boolean isNormalClass(Class<?> clazz) {
        return null != clazz
                && !clazz.isInterface()
                && !isAbstract(clazz)
                && !clazz.isEnum()
                && !clazz.isArray()
                && !clazz.isAnnotation()
                && !clazz.isSynthetic()
                && !clazz.isPrimitive();
    }

    /**
     * 加载类
     *
     * @param <T> 对象类型
     * @param className 类名
     * @param isInitialized 是否初始化
     * @return 类
     */
    @SuppressWarnings("unchecked")
    public static <T> Class<T> loadClass(String className, boolean isInitialized) {
        return (Class<T>) ClassLoaderUtil.loadClass(className, isInitialized);
    }

    /**
     * 加载类并初始化
     *
     * @param <T> 对象类型
     * @param className 类名
     * @return 类
     */
    public static <T> Class<T> loadClass(String className) {
        return loadClass(className, true);
    }


    /**
     * 名字 解析为 原始类型
     * @param name 类名
     * @return 原始类型, 非原始类型则返回空
     */
    public static Class<?> resolvePrimitiveClassName(String name) {
        return BasicType.getPrimitiveTypeByClassname(name);
    }

    /**
     * 原始类转为包装类，非原始类返回原类
     * @param clazz 原始类
     * @return 包装类
     */
    public static Class<?> wrap(Class<?> clazz){
        return BasicType.wrap(clazz);
    }

    /**
     * 包装类转为原始类，非包装类返回原类
     * @param clazz 包装类
     * @return 原始类
     */
    public static Class<?> unWrap(Class<?> clazz){
        return BasicType.unWrap(clazz);
    }

    //------------------------------------

    public static Class<?> getComponentType(Type type){
        Class<?> clazz = type.getClass();
        if(clazz.isArray()){
            return clazz.getComponentType();
        }
        else{
            if(type instanceof ParameterizedType){
                Type[]  types = ((ParameterizedType) type).getActualTypeArguments();
                if(types.length > 0){
                    return resolveClass(types[0]);
                }
            }
        }
        return null;
    }

    protected static Class<?> resolveClass(Type type){
        if(type instanceof Class){
            return (Class<?>)type;
        }
        if(type instanceof GenericArrayType){
            return ((GenericArrayType) type).getGenericComponentType().getClass();
        }
        if(type instanceof ParameterizedType){
            Type newType = ((ParameterizedType) type).getRawType();
            return resolveClass(newType);
        }
        return null;
    }
	
}

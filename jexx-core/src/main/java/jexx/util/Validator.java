package jexx.util;

import java.time.LocalDate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 验证类
 */
public class Validator {

    public final static Pattern BIRTHDAY = Pattern.compile("^(\\d{2,4})([/\\-\\.年]?)(\\d{1,2})([/\\-\\.月]?)(\\d{1,2})日?$");

    /**
     * 验证是否为生日, 雁阵年月日是否符合规范<br>
     *
     * @param year 年
     * @param month 月
     * @param day 日
     * @return 是否为生日
     */
    public static boolean isBirthday(int year, int month, int day) {
        LocalDate birthdate = LocalDate.of(year, month, day);

        //验证年
        int thisYear = LocalDate.now().getYear();
        if(year < 1930 || year > thisYear){
            return false;
        }

        //验证月
        if (month < 1 || month > 12) {
            return false;
        }

        //验证日
        if (day < 1 || day > 31) {
            return false;
        }
        if ((month == 4 || month == 6 || month == 9 || month == 11) && day == 31) {
            return false;
        }
        if (month == 2) {
            if (day > 29 || (day == 29 && !birthdate.isLeapYear())) {
                return false;
            }
        }
        return true;
    }

    /**
     * 验证是否为生日<br>
     * 只支持以下几种格式：
     * <ul>
     * 	<li>yyyyMMdd</li>
     * 	<li>yyyy-MM-dd</li>
     * 	<li>yyyy/MM/dd</li>
     * 	<li>yyyyMMdd</li>
     * 	<li>yyyy年MM月dd日</li>
     * </ul>
     *
     * @param value 值
     * @return 是否为生日
     */
    public static boolean isBirthday(String value) {
        if(isMatchRegex(BIRTHDAY, value)){
            Matcher matcher = BIRTHDAY.matcher(value);
            if(matcher.find()){
                int year = Integer.parseInt(matcher.group(1));
                int month = Integer.parseInt(matcher.group(3));
                int day = Integer.parseInt(matcher.group(5));
                return isBirthday(year, month, day);
            }
        }
        return false;
    }

    /**
     * 通过正则表达式验证
     *
     * @param pattern 正则模式
     * @param value 值
     * @return 是否匹配正则
     */
    public static boolean isMatchRegex(Pattern pattern, String value) {
        if(pattern == null || value == null) {
            //提供null的字符串为不匹配
            return false;
        }
        return pattern.matcher(value).matches();
    }

    /**
     * 验证该字符串是否是数字
     *
     * @param value 字符串内容
     * @return 是否是数字
     */
    public static boolean isNumber(String value) {
        return NumberUtil.isNumber(value);
    }

}

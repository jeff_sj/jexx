package jexx.util;

import jexx.exception.IORuntimeException;
import jexx.exception.UtilException;
import jexx.io.IOUtil;
import jexx.lang.Charsets;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

/**
 * resource工具类
 * @author jeff
 */
public class ResourceUtil {

    /**
     * 获得资源的URL<br>
     * 路径用/分隔，例如:
     *
     * <pre>
     * config/a/db.config
     * spring/xml/test.xml
     * </pre>
     *
     * @param resource 资源（相对Classpath的路径）
     * @return 资源URL
     */
    public static URL getResource(String resource){
        return getResource(resource, ClassLoaderUtil.getDefaultClassLoader());
    }

    /**
     * 获得资源相对路径对应的URL
     *
     * @param resource 资源相对路径
     * @param baseClass 基准Class，获得的相对路径相对于此Class所在路径，如果为{@code null}则相对ClassPath
     * @return {@link URL}
     */
    public static URL getResource(String resource, Class<?> baseClass) {
        return getResource(resource, baseClass == null ? null : baseClass.getClassLoader());
    }

    /**
     * 通过类加载器获取资源URL
     * @param resource 资源相对路径
     * @param classLoader 类加载器
     * @return {@link URL}
     */
    public static URL getResource(String resource, ClassLoader classLoader) {
        final URL url;
        if(classLoader == null){
            url = ClassLoader.getSystemResource(resource);
        }
        else{
            url = classLoader.getResource(resource);
        }
        return url;
    }

    /**
     * 获取资源文件; <b>注意资源文件非系统文件，会抛出异常,<b/>
     * @param resource 资源相对路径
     * @return file
     */
    public static File getResourceAsFile(String resource){
        return getResourceAsFile(resource, ClassLoaderUtil.getDefaultClassLoader());
    }

    /**
     * 获取资源的path; <b>注意资源文件非系统文件，会抛出异常,<b/>
     * @param resource 资源相对路径
     * @param classLoader 类加载器
     * @return file
     */
    public static File getResourceAsFile(String resource, ClassLoader classLoader) {
        Path path = getResourceAsPath(resource, classLoader);
        return path != null ? path.toFile() : null;
    }

    /**
     * 获取资源的path, <b>注意资源文件非系统文件，会抛出异常,<b/>
     * @param resource 资源相对路径
     * @return path
     */
    public static Path getResourceAsPath(String resource){
        return getResourceAsPath(resource, ClassLoaderUtil.getDefaultClassLoader());
    }

    /**
     * 获取资源的path; <b>注意资源文件非系统文件，会抛出异常,<b/>
     * @param resource 资源相对路径
     * @param baseClass 类
     * @return path
     */
    public static Path getResourceAsPath(String resource, Class<?> baseClass) {
        return getResourceAsPath(resource, baseClass == null ? null : baseClass.getClassLoader());
    }

    /**
     * 获取资源的path; <b>注意资源文件非系统文件，会抛出异常,<b/>
     * @param resource 资源相对路径
     * @param classLoader 类加载器
     * @return path
     */
    public static Path getResourceAsPath(String resource, ClassLoader classLoader) {
        URL url = getResource(resource, classLoader);
        if(url != null){
            try {
                return Paths.get(url.toURI()).normalize().toRealPath();
            } catch (Exception e) {
                throw new UtilException(e);
            }
        }
        return null;
    }

    /**
     * 获取资源,可获取重名的资源<br>
     * 路径格式必须为目录格式,用/分隔，例如:
     * @param resource 资源名称
     * @return 资源列表
     */
    public static List<URL> getResources(String resource) {
        final Enumeration<URL> resources;
        try {
            resources = ClassLoaderUtil.getClassLoader().getResources(resource);
        } catch (IOException e) {
            throw new IORuntimeException(e);
        }
        return CollectionUtil.list(resources);
    }

    /**
     * 通过资源名称定位资源URL
     * @param resource 资源名称
     * @param baseClass 类
     * @return 资源列表
     */
    public static List<URL> getResources(String resource, Class<?> baseClass) {
        return getResources(resource, baseClass == null ? ClassLoaderUtil.getContextClassLoader() : baseClass.getClassLoader());
    }

    /**
     * 通过资源名称定位资源URL
     * @param resource 资源名称
     * @param classLoader 类加载器
     * @return 资源列表
     */
    public static List<URL> getResources(String resource, ClassLoader classLoader) {
        final Enumeration<URL> resources;
        try {
            if(classLoader == null){
                resources = ClassLoader.getSystemResources(resource);
            }
            else{
                resources = classLoader.getResources(resource);
            }
        } catch (IOException e) {
            throw new IORuntimeException(e);
        }
        return CollectionUtil.list(resources);
    }

    /**
     * 从工程或者jar中获取该资源的流
     * @param resource 资源相对路径
     * @return {@link InputStream}
     */
    public static InputStream getStream(String resource){
        URL url = getResource(resource);
        try {
            return url != null ? url.openStream() : null;
        } catch (IOException e) {
            return null;
        }
    }

    /**
     * 资源转化为byte数组
     * @param resource 资源文件路径
     * @return  bytes
     */
    public static byte[] getStreamAsBytes(String resource){
        try(InputStream inputStream = getStream(resource)){
            if(inputStream != null){
                return IOUtil.readBytes(inputStream);
            }
        }
        catch (IOException e){
            //skip
        }
        return null;
    }

    /**
     * 获取多个同名的资源
     * @param resource 资源名称
     */
    public static List<byte[]> getStreamsAsBytes(String resource){
        List<byte[]> bytes = new ArrayList<>();

        List<URL> urls = getResources(resource);
        for(URL url : urls){
            if(url == null){
                continue;
            }
            try(InputStream inputStream = url.openStream()) {
                bytes.add(IOUtil.readBytes(inputStream));
            } catch (IOException e) {
                //skip
            }
        }

        return bytes;
    }

    /**
     * 获取资源的内容
     * @param resource 资源相对路径
     * @return 字符串
     */
    public static String getStreamAsString(String resource){
        byte[] bytes = getStreamAsBytes(resource);
        if(bytes == null){
            return null;
        }
        return StringUtil.str(bytes, Charsets.UTF_8);
    }

    /**
     * 获取多个同名的资源
     * @param resource 资源
     */
    public static List<String> getStreamsAsString(String resource){
        List<byte[]> bytes = getStreamsAsBytes(resource);

        List<String> list = new ArrayList<>();
        for(byte[] b : bytes){
            list.add(StringUtil.str(b, Charsets.UTF_8));
        }
        return list;
    }

}

package jexx.util;

import jexx.exception.UtilException;
import jexx.lang.Charsets;
import jexx.template.MapTemplateParser;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.*;
import java.util.function.BiFunction;
import java.util.function.Function;

/**
 * 字符串工具类
 */
public class StringUtil {

    public static boolean equals(String v1, String v2) {
        return Objects.equals(v1, v2);
    }

    public static boolean equals(String v1, char v2){
        if(v1 == null){
            return false;
        }
        char[] chars = v1.toCharArray();
        return chars.length == 1 && chars[0] == v2;
    }

    /**
     * 忽视大小写判断字符串是否相等
     */
    public static boolean equalsIgnoreCase(String str1, String str2) {
        return str1 == null ? str2 == null : str1.equalsIgnoreCase(str2);
    }

    /**
     * 隐藏字符串中间部分字符,字符长度小于等于1,则不处理; 常用于隐藏姓名手机号等
     * @param str 待隐藏字符串
     * @param hideNum 隐藏数量
     * @param hiddenChar 隐藏需要的字符
     * @param preferBefore 隐藏字符，是否尽量靠前; 字符串无法取中间时，两边多余字符不等，优先考虑是取前还是取后。
     * @return 隐藏后的字符串
     */
    public static String hideMiddlePart(String str, char hiddenChar, int hideNum, boolean preferBefore) {
        int strLength = length(str);
        if(strLength <= 1 || hideNum <= 0){
            return str;
        }

        if(hideNum >= strLength){
            return repeat(hiddenChar, str.length());
        }
        int halfHideNum = (strLength-hideNum)/2;

        int start = halfHideNum;
        int end = strLength - 1 - halfHideNum;

        while (end - start + 1 > hideNum){
            if (preferBefore) {
                end--;
            } else {
                start++;
            }
            if(end - start + 1 <= hideNum){
                break;
            }
            if (preferBefore) {
                start++;
            } else {
                end--;
            }
        }
        return StringUtil.replace(str, start, end+1, hiddenChar);
    }

    public static String hideMiddlePart(String str, char hiddenChar, int hideNum) {
        return hideMiddlePart(str, hiddenChar,  hideNum, false);
    }

    public static String hideMiddlePart(String str, char hiddenChar) {
        return hideMiddlePart(str, hiddenChar, length(str)/2);
    }

    /**
     * 字符直接连接成一个新的字符串
     * @param args 数组
     * @return 组合字符串; args为空,则返回null
     */
    public static String joinDirectly(Object... args) {
        if (args == null || args.length == 0) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        for (Object arg : args){
            sb.append(arg != null ? arg.toString() : "");
        }
        return sb.toString();
    }

    /**
     * 用指定转换函数连接数组中元素, function 转换后的null元素会被抛弃
     * @param list 元素集合
     * @param conjunction 连接字符串
     * @param function 转换函数
     * @param <T> 元素类型
     * @param <R> 转换后的类型
     * @return 连接后的字符串
     */
    public static <T,R> String join(Iterator<T> list, String conjunction, Function<T, R> function){
        if (null == list) {
            return null;
        }

        final StringBuilder sb = new StringBuilder();
        boolean isFirst = true;
        while (list.hasNext()){
            T t = list.next();
            if(function != null){
                R r = function.apply(t);
                if(r != null){
                    sb.append(isFirst? "": conjunction).append(str(r));
                    if (isFirst) {
                        isFirst = false;
                    }
                }
            }
            else{
                sb.append(isFirst? "": conjunction).append(str(t));
                if (isFirst) {
                    isFirst = false;
                }
            }
        }
        return sb.toString();
    }

    /**
     * @see StringUtil#join(Iterator, String, Function)
     */
    public static <T,R> String join(Iterable<T> list, String conjunction, Function<T, R> function){
        return join(list.iterator(), conjunction, function);
    }

    /**
     * @see StringUtil#join(Iterator, String, Function)
     */
    public static <T,R> String join(T[] array, String conjunction, Function<T, R> function){
        List<T> list = new ArrayList<>();
        Collections.addAll(list, array);
        return join(list, conjunction, function);
    }

    /**
     * 连接数组中的元素,组合成字符串
     * @param array 数组
     * @param conjunction 连接符
     * @return 连接后的字符串
     */
    public static <T> String join(T[] array, String conjunction) {
        return join(array, conjunction, s -> s);
    }

    /**
     * 用指定转换函数连接map中元素
     * @param map map
     * @param conjunction 连接字符串
     * @param function 转换函数
     * @return 连接后的字符串
     */
    public static <K,V,R> String join(Map<K,V> map, String conjunction, BiFunction<K,V, R> function){
        if (null == map) {
            return null;
        }

        final StringBuilder sb = new StringBuilder();
        boolean isFirst = true;
        for (Map.Entry<K,V> entry : map.entrySet()){
            if(function != null){
                R r = function.apply(entry.getKey(), entry.getValue());
                if(r != null){
                    sb.append(isFirst?"":conjunction).append(str(r));
                    if (isFirst) {
                        isFirst = false;
                    }
                }
            }
            else{
                sb.append(isFirst?"":conjunction).append(entry.getKey()).append("=").append(entry.getValue());
                if (isFirst) {
                    isFirst = false;
                }
            }
        }
        return sb.toString();
    }


    /**
     * 连接迭代器中的元素,组合成字符串
     * @param iterable 迭代器
     * @param conjunction 连接符
     * @param prefix 每个元素的前缀
     * @param suffix 每个元素的后缀
     * @return 连接后的字符串
     */
    public static <T> String join(Iterable<T> iterable, String conjunction, String prefix, String suffix) {
        return join(iterable, conjunction, s -> prefix.concat(StringUtil.str(s)).concat(suffix));
    }

    public static <T> String join(Iterable<T> iterable, String conjunction) {
        return join(iterable.iterator(), conjunction);
    }

    /**
     * 连接迭代器中的元素,组合成字符串
     * @param iterator 迭代器
     * @param conjunction 连接符
     * @return 连接后的字符串
     */
    public static <T> String join(Iterator<T> iterator, String conjunction, String prefix, String suffix) {
        return join(iterator, conjunction, prefix, suffix);
    }

    public static <T> String join(Iterator<T> iterator, String conjunction) {
        return join(iterator, conjunction, s->s);
    }

    /**
     * 以路径形式来组合成为字符串
     * @param array 集合
     * @param pathSeparator 路径分割符
     * @param function 对象转换为路径字符串
     * @return 路径字符串
     */
    public static <T,R> String joinByPath(T[] array, String pathSeparator, Function<T, R> function){
        Assert.hasText(pathSeparator, "pathSeparator is illegal");
        if(CollectionUtil.isEmpty(array)){
            return StringPool.EMPTY;
        }
        return pathSeparator.concat(join(array, pathSeparator, function)).concat(pathSeparator);
    }

    /**
     * 以路径形式来组合成为字符串
     * @param collection 集合
     * @param pathSeparator 路径分割符
     * @param function 对象转换为路径字符串
     * @return 路径字符串
     */
    public static <T,R> String joinByPath(Collection<T> collection, String pathSeparator, Function<T, R> function){
        Assert.hasText(pathSeparator, "pathSeparator is illegal");
        if(CollectionUtil.isEmpty(collection)){
            return StringPool.EMPTY;
        }
        return pathSeparator.concat(join(collection, pathSeparator, function)).concat(pathSeparator);
    }

    /**
     * 以路径形式来组合成为字符串;如下
     * <pre>
     *     StringUtil.joinByPath(CollectionUtil.list("a", "b", "c"), ",")=",a,b,c,"
     *     StringUtil.joinByPath(CollectionUtil.list("a", null, "c"), ",")=",a,,c,"
     * </pre>
     * @param collection 集合
     * @param pathSeparator 路径分割符
     * @return 路径字符串
     */
    public static <T> String joinByPath(Collection<T> collection, String pathSeparator){
        return joinByPath(collection, pathSeparator, s -> s != null ? s : StringPool.EMPTY);
    }

    /**
     * 以路径形式来组合成为字符串;如下
     * <pre>
     *     StringUtil.joinByPath(["a", "b", "c"], ",")=",a,b,c,"
     *     StringUtil.joinByPath(["a", null, "c"], ",")=",a,,c,"
     * </pre>
     * @param array 数组
     * @param pathSeparator 路径分割符
     * @return 路径字符串
     */
    public static <T> String joinByPath(T[] array, String pathSeparator){
        return joinByPath(array, pathSeparator, s -> s != null ? s : StringPool.EMPTY);
    }

    /**
     * 字符串是否有长度
     * @param str 字符串
     * @return bool
     */
    public static boolean hasLength(String str) {
        return (str != null && str.length() > 0);
    }

    /**
     * 是否含有非空字符
     * @param str 字符串
     * @return bool
     */
    public static boolean hasText(String str) {
        if (!hasLength(str)) {
            return false;
        }
        int strLen = str.length();
        for (int i = 0; i < strLen; i++) {
            if (!Character.isWhitespace(str.charAt(i))) {
                return true;
            }
        }
        return false;
    }

    public static int indexOf(final String src, final String sub, int startIndex) {
        return indexOf(src, sub, startIndex, src.length());
    }

    /**
     * 获取原字符串中子串的索引
     * @param src 原字符串
     * @param sub 子串
     * @param startIndex 开始索引
     * @param endIndex 结束索引,不包含
     * @return 子串的索引
     */
    public static int indexOf(final String src, final String sub, int startIndex, int endIndex) {
        if (startIndex < 0) {
            startIndex = 0;
        }
        int srcLen = src.length();
        if (endIndex > srcLen) {
            endIndex = srcLen;
        }
        int sublen = sub.length();
        if (sublen == 0) {
            return Math.min(srcLen, startIndex);
        }

        int total = endIndex - sublen + 1;
        char c = sub.charAt(0);
        loop:
        for (int i = startIndex; i < total; i++) {
            if (src.charAt(i) != c) {
                continue;
            }
            int j = 1;
            int k = i + 1;
            while (j < sublen) {
                if (sub.charAt(j) != src.charAt(k)) {
                    continue loop;
                }
                j++; k++;
            }
            return i;
        }
        return -1;
    }

    public static boolean isAllBlank(CharSequence... css){
        if(ArrayUtil.isEmpty(css)){
            return true;
        }
        for (CharSequence cs : css) {
            if(isNotBlank(cs)){
                return false;
            }
        }
        return true;
    }

    public static boolean isAllEmpty(CharSequence... css){
        if(ArrayUtil.isEmpty(css)){
            return true;
        }
        for (CharSequence cs : css) {
            if(isNotEmpty(cs)){
                return false;
            }
        }
        return true;
    }

    /**
     * 是否全是字母
     * <pre>
     * StringUtils.isAlpha(null)   = false
     * StringUtils.isAlpha("")     = true
     * StringUtils.isAlpha("  ")   = false
     * StringUtils.isAlpha("abc")  = true
     * StringUtils.isAlpha("ab2c") = false
     * StringUtils.isAlpha("ab-c") = false
     * </pre>
     */
    public static boolean isAlpha(String str) {
        if (str == null) {
            return false;
        }
        int sz = str.length();
        for (int i = 0; i < sz; i++) {
            if (!Character.isLetter(str.charAt(i))) {
                return false;
            }
        }
        return true;
    }

    /**
     * 字符串为空字符串, 不含有空字符以外的字符
     */
    public static boolean isBlank(CharSequence str) {
        int strLen;
        if (str == null || (strLen = str.length()) == 0) {
            return true;
        }
        for (int i = 0; i < strLen; i++) {
            if ( !Character.isWhitespace(str.charAt(i)) ) {
                return false;
            }
        }
        return true;
    }



    /**
     * 字符串是否为非空白 空白的定义如下： <br>
     * 1、不为null <br>
     * 2、不为不可见字符（如空格）<br>
     * 3、不为""<br>
     *
     * @param str 被检测的字符串
     * @return 是否为非空
     */
    public static boolean isNotBlank(CharSequence str) {
        return !isBlank(str);
    }

    /**
     * 字符串为空, 为null或者长度为0
     */
    public static boolean isEmpty(CharSequence str) {
        return str == null || str.length() == 0;
    }

    /**
     * 字符串非空, 不为null且大于0
     */
    public static boolean isNotEmpty(CharSequence str) {
        return str != null && str.length() > 0;
    }

    /**
     * 字符串全为数字
     */
    public static boolean isNumeric(String str) {
        if (str == null || isBlank(str)) {
            return false;
        }
        int sz = str.length();
        for (int i = 0; i < sz; i++) {
            if (!Character.isDigit(str.charAt(i))) {
                return false;
            }
        }
        return true;
    }

    /**
     * 是否只含有空字符串
     */
    public static boolean isWhitespace(String str) {
        if (str == null) {
            return false;
        }
        int sz = str.length();
        for (int i = 0; i < sz; i++) {
            if ( !Character.isWhitespace(str.charAt(i)) ) {
                return false;
            }
        }
        return true;
    }

    /**
     * 获取指定位置的子字符串
     * @param string    原字符串
     * @param fromIndex 开始索引
     * @param toIndex   结束索引
     * @return  子串
     */
    public static String substring(final String string, int fromIndex, int toIndex) {
        if(string == null){
            return null;
        }

        int len = string.length();

        if (fromIndex < 0) {
            fromIndex = len + fromIndex;

            if (toIndex == 0) {
                toIndex = len;
            }
        }

        if (toIndex < 0) {
            toIndex = len + toIndex;
        }

        // safe net

        if (fromIndex < 0) {
            fromIndex = 0;
        }
        if (toIndex > len) {
            toIndex = len;
        }
        if (fromIndex >= toIndex) {
            return StringPool.EMPTY;
        }

        return string.substring(fromIndex, toIndex);
    }

    /**
     * 指定偏移位置的子串在原字符串中是否存在
     */
    public static boolean isSubstringAt(final String string, final String substring, final int offset) {
        int len = substring.length();

        int max = offset + len;

        if (max > string.length()) {
            return false;
        }

        int ndx = 0;
        for (int i = offset; i < max; i++, ndx++) {
            if (string.charAt(i) != substring.charAt(ndx)) {
                return false;
            }
        }

        return true;
    }

    /**
     * 字符串长度
     */
    public static int length(String str) {
        return str == null ? 0 : str.length();
    }

    /**
     * 字符串转小写
     */
    public static String lowerCase(String str) {
        return str == null ? null : str.toLowerCase();
    }

    // ---------------------------------------------------------------- bytes

    public static byte[] getBytes(final String string) {
        return getBytes(string, StringPool.UTF_8);
    }

    public static byte[] getBytes(final String string, final Charset charset) {
        return getBytes(string, Charsets.name(charset));
    }

    public static byte[] getBytes(final String string, final String charsetName) {
        if(string == null){
            return null;
        }
        try {
            return string.getBytes(charsetName);
        } catch (UnsupportedEncodingException e) {
            throw new UtilException(e);
        }
    }

    /**
     * 反转字符串
     * <pre>
     * StringUtils.reverse(null)  = null
     * StringUtils.reverse("")    = ""
     * StringUtils.reverse("bat") = "tab"
     * </pre>
     * @param str 待处理字符串
     */
    public static String reverse(String str) {
        if (str == null) {
            return null;
        }
        return new StringBuilder(str).reverse().toString();
    }


    /**
     * 替换字符串中的指定字符串
     *
     * @param str 字符串
     * @param searchStr 被查找的字符串
     * @param replacement 被替换的字符串
     * @return 替换后的字符串
     */
    public static String replace(CharSequence str, CharSequence searchStr, CharSequence replacement) {
        return replace(str, 0, searchStr, replacement);
    }

    /**
     * 替换字符串中的指定字符串
     *
     * @param str 字符串
     * @param fromIndex 开始位置（包括）
     * @param searchStr 被查找的字符串
     * @param replacement 被替换的字符串
     * @return 替换后的字符串
     */
    public static String replace(CharSequence str, int fromIndex, CharSequence searchStr, CharSequence replacement) {
        if (isEmpty(str) || isEmpty(searchStr)) {
            return str(str);
        }
        if (null == replacement) {
            replacement = StringPool.EMPTY;
        }

        final int strLength = str.length();
        final int searchStrLength = searchStr.length();
        if (fromIndex > strLength) {
            return str(str);
        } else if (fromIndex < 0) {
            fromIndex = 0;
        }

        final StringBuilder result = new StringBuilder();
        if (0 != fromIndex) {
            result.append(str.subSequence(0, fromIndex));
        }

        String string = str(str);
        String search = str(searchStr);
        int preIndex = fromIndex;
        int index;
        while ((index = string.indexOf(search, preIndex)) > -1) {
            result.append(str.subSequence(preIndex, index));
            result.append(replacement);
            preIndex = index + searchStrLength;
        }

        if (preIndex < strLength) {
            // 结尾部分
            result.append(str.subSequence(preIndex, strLength));
        }
        return result.toString();
    }

    /**
     * 替换指定字符串的指定区间内字符为固定字符
     *
     * @param str          字符串
     * @param startInclude 开始位置（包含）
     * @param endExclude   结束位置（不包含）
     * @param replacedChar 被替换的字符
     * @return 替换后的字符串
     */
    public static String replace(CharSequence str, int startInclude, int endExclude, char replacedChar) {
        if (isEmpty(str)) {
            return str(str);
        }
        final int strLength = str.length();
        if (startInclude > strLength) {
            return str(str);
        }
        if (endExclude > strLength) {
            endExclude = strLength;
        }
        if (startInclude > endExclude) {
            // 如果起始位置大于结束位置，不替换
            return str(str);
        }

        final char[] chars = new char[strLength];
        for (int i = 0; i < strLength; i++) {
            if (i >= startInclude && i < endExclude) {
                chars[i] = replacedChar;
            } else {
                chars[i] = str.charAt(i);
            }
        }
        return new String(chars);
    }

    /**
     * 移除字符串开头得到新字符串
     * <pre>
     * StringUtils.removeStartIgnoreCase(null, *)      = null
     * StringUtils.removeStartIgnoreCase("", *)        = ""
     * StringUtils.removeStartIgnoreCase(*, null)      = *
     * StringUtils.removeStartIgnoreCase("www.domain.com", "www.")   = "domain.com"
     * StringUtils.removeStartIgnoreCase("domain.com", "www.")       = "domain.com"
     * StringUtils.removeStartIgnoreCase("www.domain.com", "domain") = "www.domain.com"
     * StringUtils.removeStartIgnoreCase("abc", "")    = "abc"
     * </pre>
     */
    public static String removeStart(String str, String remove) {
        if (isEmpty(str) || isEmpty(remove)) {
            return str;
        }
        if (str.startsWith(remove)) {
            return str.substring(remove.length());
        }
        return str;
    }

    /**
     * 去掉指定后缀
     *
     * @param str 字符串
     * @param suffix 后缀
     * @return 切掉后的字符串，若后缀不是 suffix， 返回原字符串
     */
    public static String removeSuffix(CharSequence str, CharSequence suffix) {
        if (isEmpty(str) || isEmpty(suffix)) {
            return str(str);
        }

        final String str2 = str.toString();
        if (str2.endsWith(suffix.toString())) {
            return str2.substring(0, str2.length() - suffix.length());
        }
        return str2;
    }

    /**
     * 移除所有空白字符
     * @param str 待处理字符
     * @return 移除空白字符后的字符
     */
    public static String removeWhitespace(CharSequence str) {
        if (isEmpty(str)) {
            return str(str);
        }
        final String str1 = str.toString();
        return str1.replaceAll("\\s+", "");
    }

    /**
     * 将已有字符串填充为规定长度，如果已有字符串超过这个长度则返回这个字符串<br>
     * 字符填充于字符串前
     *
     * @param str 被填充的字符串
     * @param filledChar 填充的字符
     * @param len 填充长度
     * @return 填充后的字符串
     */
    public static String fillBefore(String str, char filledChar, int len) {
        return fill(str, filledChar, len, true);
    }

    /**
     * 将已有字符串填充为规定长度，如果已有字符串超过这个长度则返回这个字符串<br>
     * 字符填充于字符串后
     *
     * @param str 被填充的字符串
     * @param filledChar 填充的字符
     * @param len 填充长度
     * @return 填充后的字符串
     */
    public static String fillAfter(String str, char filledChar, int len) {
        return fill(str, filledChar, len, false);
    }

    /**
     * 将已有字符串填充为规定长度，如果已有字符串超过这个长度则返回这个字符串
     *
     * @param str 被填充的字符串
     * @param filledChar 填充的字符
     * @param len 填充长度
     * @param isPre 是否填充在前
     * @return 填充后的字符串
     */
    public static String fill(String str, char filledChar, int len, boolean isPre) {
        final int strLen = str.length();
        if (strLen > len) {
            return str;
        }

        String filledStr = repeat(filledChar, len - strLen);
        return isPre ? filledStr.concat(str) : str.concat(filledStr);
    }

    /**
     * 重复某个字符
     *
     * @param c 被重复的字符
     * @param count 重复的数目，如果小于等于0则返回""
     * @return 重复字符字符串
     */
    public static String repeat(char c, int count) {
        if (count <= 0) {
            return StringPool.EMPTY;
        }

        char[] result = new char[count];
        for (int i = 0; i < count; i++) {
            result[i] = c;
        }
        return new String(result);
    }

    /**
     * 替换字符串中的{}
     * <pre>
     *     StringUtil.format("...{}...", 11)="...11..."
     *     StringUtil.format("...\\{}...", 11)="...\\{}..."
     *     StringUtil.format("...{{}...", 11)="...{11..."
     *     StringUtil.format("...{}...{}...", 11, 22)="...11...22..."
     *     StringUtil.format("...{}...{}...", 11)="...11...{}..."
     * </pre>
     * @param template 待替换的字符串
     * @param objects  替换数组
     * @return  替换后的字符串
     */
    public static String format(String template, Object... objects){
        return substitute(template, objects);
    }

    public static String substitute(String template, Object... objects){
        return substituteWithMacro(template, "{", "}", objects);
    }

    public static String substituteWithMacro(final String template, final String macroStart, final String macroEnd, Object... objects){
        if(StringUtil.isEmpty(template)){
            return template;
        }

        StringBuilder result = new StringBuilder(template.length());

        final char escapeChar = '\\';

        final int startLen = macroStart.length();
        final int objectsLen = objects.length;

        int i = 0;
        int len = template.length();
        int replaceNum = -1;
        while (i < len) {
            int ndx = template.indexOf(macroStart, i);
            if (ndx == -1 || replaceNum >= objectsLen - 1) {
                result.append(i == 0 ? template : template.substring(i));
                break;
            }

            // check escaped
            int j = ndx - 1;
            boolean escape = false;
            int count = 0;

            while ((j >= 0) && (template.charAt(j) == escapeChar)) {
                escape = !escape;
                if (escape) {
                    count++;
                }
                j--;
            }
            result.append(template.substring(i, ndx));

            if (escape) {
                result.append(macroStart);
                i = ndx + startLen;
                continue;
            }

            int ndx1 = ndx;
            int ndx2 = template.indexOf(macroEnd, ndx);
            if(ndx2 == ndx1 + 1){
                replaceNum++;
                result.append(objects[replaceNum]);
                i = ndx2 + 1;
            }
            else{
                result.append(template.substring(ndx1,ndx1+1));
                i = ndx1 + 1;
            }
        }

        return result.toString();
    }

    /**
     * 解析字符串,使用map替换对象的变量
     * <pre>
     *     Map<String,Object> map = new HashMap<>();
     *     map.put("fooProp", "XXX");
     *     StringUtil.substitute("...${fooProp}...", map)="...XXX..."
     *     StringUtil.substitute("...${fooProp11}...")="...${fooProp11}..."
     *     StringUtil.substitute("...\\${fooProp}...", map)="...\\${fooProp}..."
     * </pre>
     * @param template 模板
     * @param map 键值对
     * @return 格式化后的字符串
     */
    public static String substitute(String template, Map<String, Object> map){
        MapTemplateParser parser = new MapTemplateParser();
        parser.setResolveEscapes(false);
        parser.setReplaceMissingKey(false);
        return parser.of(map).parse(template);
    }

    public static String format(String template, Map<String, Object> map){
        return substitute(template, map);
    }

    /**
     * i18格式化字符串
     * @param resourceBundle i18n资源对象
     * @param template 模板
     * @param args 参数
     * @return 格式化后的字符串
     */
    public static String format(ResourceBundle resourceBundle, String template, String... args){
        Object[] values = new Object[args.length];
        ArrayUtil.copy(args, values, 0);
        if(resourceBundle == null){
            return format(template, values);
        }

        String x;
        for (int i = 0; i < args.length; i++) {
            x = resourceBundle.getString(args[i]);
            //ResourceBundle读取properties文件时以iso8859-1编码,why?
            values[i] = new String(x.getBytes(Charsets.ISO_8859_1), Charsets.UTF_8);
        }
        return format(template, values);
    }

    // --------------------------split-------------------------------------- start

    /**
     * 分割字符串到集合
     * @param src 待分割字符串
     * @param delimiter 割字符
     * @param limit 限制个数; 小于等于0则不限制个数
     * @param ignoreEmpty 是否忽视空字符串
     * @param isTrim 是否两边去空
     * @return 分割后的字符串集合
     */
    public static String[] split(final String src, final char delimiter, final int limit, final boolean ignoreEmpty, final boolean isTrim) {
        List<String> list = splitToList(src, delimiter, limit, ignoreEmpty, isTrim);
        return ArrayUtil.toArray(list, String.class);
    }

    public static String[] split(final String src, final char delimiter) {
        List<String> list = splitToList(src, delimiter, -1, false, false);
        return ArrayUtil.toArray(list, String.class);
    }

    public static String[] split(final String src, final char delimiter, final int limit) {
        List<String> list = splitToList(src, delimiter, limit, false, false);
        return ArrayUtil.toArray(list, String.class);
    }

    /**
     * 路径字符串转换为集合, 不校验是否符合路径格式; 不符合返回自身
     * @see StringUtil#splitPath(String, String, boolean)
     * @param path 路径字符串
     * @param pathSeparator 路径分隔符
     */
    public static String[] splitPath(final String path, final String pathSeparator){
        return splitPath(path, pathSeparator, false);
    }

    /**
     * 路径字符串转换为集合
     * <pre>
     *     ,a,b,c,=[a,b,c]
     * </pre>
     * @param path 路径字符串
     * @param pathSeparator 路径分隔符
     * @param checkFormat 是否检查格式
     * @return 集合
     */
    public static String[] splitPath(final String path, final String pathSeparator, boolean checkFormat){
        if(isEmpty(path)){
            return new String[0];
        }
        List<String> list = splitPathToList(path, pathSeparator, checkFormat);
        return ArrayUtil.toStringArray(list);
    }

    /**
     * 路径字符串转换为集合, 不校验是否符合路径格式; 不符合返回自身
     * @see StringUtil#splitPathToList(String, String, boolean)
     * @param path 路径字符串
     * @param pathSeparator 路径分隔符
     */
    public static List<String> splitPathToList(final String path, final String pathSeparator){
        return splitPathToList(path, pathSeparator, false);
    }

    /**
     * 路径字符串转换为集合
     * <pre>
     *     ,a,b,c,=(a,b,c)
     * </pre>
     * @param path 路径字符串
     * @param pathSeparator 路径分隔符
     * @param checkFormat 是否检查路径格式
     * @return 集合
     */
    public static List<String> splitPathToList(final String path, final String pathSeparator, boolean checkFormat){
        if(isEmpty(path)){
            return Collections.emptyList();
        }
        Assert.hasText(pathSeparator, "pathSeparator is illegal");

        if(path.startsWith(pathSeparator) && path.endsWith(pathSeparator)){
            int length = pathSeparator.length();
            String string = path.substring(length, path.length()-length);
            return splitToList(string, pathSeparator);
        }
        else{
            if(checkFormat){
                throw new IllegalArgumentException(StringUtil.format("'{}' must start with and end with '{}'", path, pathSeparator));
            }
            return CollectionUtil.list(path);
        }
    }


    /**
     * 分割字符串到集合
     * @param src 待分割字符串
     * @param delimiter 割字符
     * @param limit 限制个数; 小于等于0则不限制个数
     * @param ignoreEmpty 是否忽视空字符串
     * @param isTrim 是否两边去空
     * @return 分割后的字符串集合
     */
    public static List<String> splitToList(final String src, final char delimiter, final int limit, final boolean ignoreEmpty, final boolean isTrim) {
        if (src.length() == 0) {
            return new ArrayList<>();
        }
        if(limit == 1){
            return addToList(new ArrayList<String>(1), src, isTrim, ignoreEmpty);
        }

        char[] chars = src.toCharArray();

        List<String> list = new ArrayList<>();
        int s = 0, e = 0;

        int count = 0;
        while (e < chars.length) {
            if(limit > 0 && count == limit-1){
                e = chars.length;
                addToList(list, src.substring(s, e), ignoreEmpty, isTrim);
                break;
            }
            e = CharUtil.findFirstEqual(chars, s, delimiter);
            if (e == -1) {
                e = chars.length;
            }
            addToList(list, src.substring(s, e), ignoreEmpty, isTrim);
            count++;
            s = e+1;
        }
        return list;
    }

    public static List<String> splitToList(final String src, final char delimiter, final int limit) {
        return splitToList(src, delimiter, limit, false, false);
    }

    public static List<String> splitToList(final String src, final char delimiter) {
        return splitToList(src, delimiter, -1);
    }

    public static List<String> splitToList(final String src, final String delimiter, final int limit, final boolean ignoreEmpty, final boolean isTrim) {
        return splitToList(src, delimiter, limit, s -> {
            String temp = s;
            if(temp == null || ignoreEmpty && StringUtil.isEmpty(temp)){
                return null;
            }
            if(isTrim){
                temp = temp.trim();
            }
            return temp;
        });
    }

    public static List<String> splitToList(final String src, final String delimiter, final int limit) {
        return splitToList(src, delimiter, limit, false, false);
    }

    public static List<String> splitToList(final String src, final String delimiter) {
        return splitToList(src, delimiter, -1);
    }

    public static String[] split(final String src, final String delimiter, final int limit, final boolean ignoreEmpty, final boolean isTrim) {
        List<String> list = splitToList(src, delimiter, limit, ignoreEmpty, isTrim);
        return ArrayUtil.toArray(list, String.class);
    }

    public static String[] split(final String src, final String delimiter, final int limit) {
        return split(src, delimiter, limit, false, false);
    }

    public static String[] split(final String src, final String delimiter) {
        return split(src, delimiter, -1);
    }

    public static <R> List<R> splitToList(final String src, final String delimiter, final int limit, final Function<String, R> transform) {
        if (src.length() == 0) {
            return Collections.emptyList();
        }

        List<R> list = new ArrayList<>();

        int i = 0;
        int start = 0;
        int end;
        int count = 0;
        int srcLength = src.length();
        int delimiterLen = delimiter.length();
        String temp;
        while (i < srcLength){
            if(limit > 0 && count == limit - 1){
                end = srcLength;
            }
            else{
                i = indexOf(src, delimiter, start);
                end = i == -1 ? srcLength : i;
            }

            temp = src.substring(start, end);
            R r = transform.apply(temp);
            if(r != null){
                list.add(r);
                count++;
            }
            if(i == -1 || ( limit > 0 && count >= limit )){
                break;
            }
            start = end + delimiterLen;
        }
        return list;
    }

    public static <R> List<R> splitToList(final String src, final String delimiter, final Function<String, R> transform) {
        return splitToList(src, delimiter, 0, transform);
    }


    private static List<String> addToList(List<String> list, String part, boolean ignoreEmpty, boolean isTrim){
        if(isTrim){
            part = part.trim();
        }
        if(!ignoreEmpty || StringUtil.isNotEmpty(part)){
            list.add(part);
        }
        return list;
    }

    // --------------------------split-------------------------------------- end

    /**
     * 是否以指定字符串开头
     * @param str 被监测字符串
     * @param sub 开头字符串
     * @param startIndex 检测字符串开始索引
     * @param isIgnoreCase 是否忽略大小写
     * @return 是否以指定字符串开头
     */
    public static boolean startWith(final String str, final String sub, final int startIndex, boolean isIgnoreCase) {
        Assert.notNull(str, "str is not null!");
        Assert.notNull(sub, "prefix is not null!");

        int subLength = sub.length();
        if(startIndex + subLength > str.length()){
            return false;
        }

        int i = 0;
        int j = startIndex;
        while(i < subLength){
            char srcSource = isIgnoreCase ? Character.toLowerCase(str.charAt(j)) : str.charAt(j);
            char subSource = isIgnoreCase ? Character.toLowerCase(sub.charAt(i)) : sub.charAt(i);
            if(srcSource != subSource){
                return false;
            }
            i++; j++;
        }
        return true;
    }

    public static boolean startWith(final String str, final String sub, final int startIndex) {
        return startWith(str, sub, startIndex, false);
    }

    public static boolean startWith(final String str, final String sub) {
        return startWith(str, sub, 0, false);
    }

    public static boolean startWithIgnoreCase(final String str, final String sub, final int startIndex) {
        return startWith(str, sub, startIndex, true);
    }

    public static boolean startWithIgnoreCase(final String str, final String sub) {
        return startWithIgnoreCase(str, sub, 0);
    }

    public static boolean startWithChar(final String str, final char c) {
        if (str.length() == 0) {
            return false;
        }
        return str.charAt(0) == c;
    }

    /**
     * 是否以指定字符串结尾<br>
     * @param str 被监测字符串
     * @param sub 结尾字符串
     * @param isIgnoreCase 是否忽略大小写
     * @return 是否以指定字符串结尾
     */
    public static boolean endWith(final String str, final String sub, boolean isIgnoreCase) {
        Assert.notNull(str, "str is not null!");
        Assert.notNull(str, "sub is not null!");

        int subLength = sub.length();
        int strLength = str.length();
        if(subLength > strLength){
            return false;
        }

        int i = subLength - 1;
        int j = strLength - 1;
        while(i >= 0){
            char subSource = isIgnoreCase ? Character.toLowerCase(sub.charAt(i)) : sub.charAt(i);
            char srcSource = isIgnoreCase ? Character.toLowerCase(str.charAt(j)) : str.charAt(j);
            if(srcSource != subSource){
                return false;
            }
            i--; j--;
        }
        return true;
    }

    public static boolean endWith(final String str, final String sub) {
        return endWith(str, sub, false);
    }

    public static boolean endWithIgnoreCase(final String str, final String sub) {
        return endWith(str, sub, true);
    }

    /**
     * 指定字符串是否以多个结尾字符串中任一个结束
     * @param str 被监测字符串
     * @param subs 结尾字符串
     * @return boool
     */
    public static boolean endWith(final String str, final String... subs) {
        if(ArrayUtil.isEmpty(subs)){
            return false;
        }
        for(String sub : subs){
            if(endWith(str, sub)){
                return true;
            }
        }
        return false;
    }

    /**
     * 指定字符串是否以多个忽视大小写的结尾字符串中任一个结束
     * @param str 被监测字符串
     * @param subs 结尾字符串
     * @return boool
     */
    public static boolean endWithIgnoreCase(final String str, final String... subs) {
        if(ArrayUtil.isEmpty(subs)){
            return false;
        }
        for(String sub : subs){
            if(endWith(str, sub, true)){
                return true;
            }
        }
        return false;
    }

    public static boolean endWithChar(final String str, final char c) {
        if (str.length() == 0) {
            return false;
        }
        return str.charAt(str.length()-1) == c;
    }

    /**
     * 除去字符串头尾部的空白符，如果字符串是<code>null</code>，依然返回<code>null</code>。
     *
     * @param str 要处理的字符串
     * @param mode <code>-1</code>表示trimStart，<code>0</code>表示trim全部， <code>1</code>表示trimEnd
     *
     * @return 除去指定字符后的的字符串，如果原字串为<code>null</code>，则返回<code>null</code>
     */
    public static String trim(String str, int mode) {
        if (str == null) {
            return null;
        }

        int length = str.length();
        int start = 0;
        int end = length;

        // 扫描字符串头部
        if (mode <= 0) {
            while ((start < end) && (CharUtil.isBlankChar(str.charAt(start)))) {
                start++;
            }
        }

        // 扫描字符串尾部
        if (mode >= 0) {
            while ((start < end) && (CharUtil.isBlankChar(str.charAt(end - 1)))) {
                end--;
            }
        }

        if ((start > 0) || (end < length)) {
            return str.substring(start, end);
        }

        return str;
    }

    public static String trim(String str) {
        return trim(str, 0);
    }

    public static String trimStart(String str) {
        return trim(str, -1);
    }

    public static String trimEnd(String str) {
        return trim(str, 1);
    }


    public static String toCamelCase(String name) {
        return toCamelCase(name, false);
    }

    public static String toCamelCase(CharSequence name, boolean upperFirstLetter) {
        return underlineToCamelCase(name, upperFirstLetter);
    }

    public static String underlineToCamelCase(CharSequence name) {
        return underlineToCamelCase(name, false);
    }

    /**
     * 下划线转驼峰拼写法; 如  <code>underlineToCamelCase("hello_world")="helloWorld"</code>
     * @param name 下划线拼写字段
     * @param upperFirstLetter 是否大写首字母
     * @return 驼峰拼写的字符串
     */
    public static String underlineToCamelCase(CharSequence name, boolean upperFirstLetter) {
        if (null == name) {
            return null;
        }

        final StringBuilder sb = new StringBuilder(name.length());
        boolean upperCase = false;
        for (int i = 0; i < name.length(); i++) {
            char c = name.charAt(i);
            if (equals(StringPool.UNDERSCORE, c)) {
                upperCase = true;
            } else if (upperCase) {
                sb.append(Character.toUpperCase(c));
                upperCase = false;
            } else {
                if(upperFirstLetter && i == 0){
                    sb.append(Character.toUpperCase(c));
                }
                else{
                    sb.append(Character.toLowerCase(c));
                }
            }
        }
        return sb.toString();
    }

    public static String camelCaseToUnderline(CharSequence name) {
        return camelCaseToUnderline(name, true);
    }

    /**
     * 驼峰拼写字符串转下划线字符串; 如  <code>camelCaseToUnderline("helloWorld")="hello_world"</code>
     * @param name 驼峰字符串
     * @param lowerFirstLetter 是否首字母小写
     * @return 下划线字符串
     */
    public static String camelCaseToUnderline(CharSequence name, boolean lowerFirstLetter) {
        final StringBuilder sb = new StringBuilder(name.length());
        for (int i = 0; i < name.length(); i++) {
            char c = name.charAt(i);

            if(i == 0){
                if(!CharUtil.isAlpha(c)){
                    throw new IllegalArgumentException("first letter is not alpha");
                }
                if(lowerFirstLetter){
                    sb.append(Character.toLowerCase(c));
                }
                else{
                    sb.append(c);
                }
            }
            else{
                if(!CharUtil.isAlphaOrDigit(c)){
                    throw new IllegalArgumentException("name is not illegal");
                }
                if(CharUtil.isAlphaUpper(c)){
                    sb.append(StringPool.UNDERSCORE);
                    sb.append(Character.toLowerCase(c));
                }
                else {
                    sb.append(c);
                }
            }
        }
        return sb.toString();
    }


    // ---------------------------------------------------------------- change string

    /**
     * 字符串转大写
     */
    public static String upperCase(String str) {
        return str == null ? null : str.toUpperCase();
    }

    /**
     * 首字母小写
     * @param str   字符串
     * @return  返回字符串
     */
    public static String lowerFirst(CharSequence str) {
        if (null == str) {
            return null;
        }
        if (str.length() > 0) {
            char firstChar = str.charAt(0);
            if (Character.isUpperCase(firstChar)) {
                return Character.toLowerCase(firstChar) + str.toString().substring(1);
            }
        }
        return str.toString();
    }

    /**
     * 首字母大写
     * @param str 字符串
     * @return  返回字符串
     */
    public static String upperFirst(CharSequence str) {
        if (null == str) {
            return null;
        }
        if (str.length() > 0) {
            char firstChar = str.charAt(0);
            if (Character.isLowerCase(firstChar)) {
                return Character.toUpperCase(firstChar) + str.toString().substring(1);
            }
        }
        return str.toString();
    }

    /**
     * 使用系统编码来解码字节码
     *
     * @param data 字符串
     * @return 解码后的字符串
     */
    public static String str(byte[] data) {
        return str(data, Charsets.UTF_8);
    }

    /**
     * 解码字节码
     *
     * @param data 字符串
     * @param charset 字符集，如果此字段为空，则解码的结果取决于平台
     * @return 解码后的字符串
     */
    public static String str(byte[] data, Charset charset) {
        if (data == null) {
            return null;
        }

        if (null == charset) {
            return new String(data);
        }
        return new String(data, charset);
    }

    /**
     * 使用系统编码来解码字节码
     * @param data 字符串
     * @return 解码后的字符串
     */
    public static String str(Byte[] data) {
        return str(data, Charsets.UTF_8);
    }

    /**
     * 解码字节码
     *
     * @param data 字符串
     * @param charset 字符集，如果此字段为空，则解码的结果取决于平台
     * @return 解码后的字符串
     */
    public static String str(Byte[] data, Charset charset) {
        if (data == null) {
            return null;
        }

        byte[] bytes = new byte[data.length];
        Byte dataByte;
        for (int i = 0; i < data.length; i++) {
            dataByte = data[i];
            bytes[i] = (null == dataByte) ? -1 : dataByte;
        }

        return str(bytes, charset);
    }

    /**
     * 将编码的byteBuffer数据转换为字符串
     *
     * @param data 数据
     * @param charset 字符集，如果为空使用当前系统字符集
     * @return 字符串
     */
    public static String str(ByteBuffer data, Charset charset) {
        if (null == charset) {
            charset = Charsets.UTF_8;
        }
        return charset.decode(data).toString();
    }

    /**
     * {@link CharSequence} 转为字符串，null安全
     *
     * @param cs {@link CharSequence}
     * @return 字符串
     */
    public static String str(CharSequence cs) {
        return null == cs ? null : cs.toString();
    }

    public static String str(Object obj) {
        return str(obj, Charsets.UTF_8);
    }

    public static String str(Object obj, Charset charset) {
        if (null == obj) {
            return null;
        }

        if (obj instanceof String) {
            return (String) obj;
        } else if (obj instanceof byte[]) {
            return str((byte[]) obj, charset);
        } else if (obj instanceof Byte[]) {
            return str((Byte[]) obj, charset);
        } else if (obj instanceof ByteBuffer) {
            return str((ByteBuffer) obj, charset);
        } else if (ArrayUtil.isArray(obj)) {
            return ArrayUtil.toString(obj);
        }
        return obj.toString();
    }

}

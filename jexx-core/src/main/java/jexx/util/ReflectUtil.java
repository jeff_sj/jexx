package jexx.util;

import jexx.cache.SimpleCache;
import jexx.exception.UtilException;

import java.lang.annotation.Annotation;
import java.lang.reflect.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 反射工具类
 *
 * @author jeff
 */
public class ReflectUtil {

    /** 构造方法缓存 */
    private static final SimpleCache<Class<?>, Constructor<?>[]> CACHE_CONSTRUCTORS = new SimpleCache<>();
    /** 字段缓存 */
    private static final SimpleCache<Class<?>, Field[]> CACHE_FIELDS = new SimpleCache<>();
    /** 方法缓存 */
    private static final SimpleCache<Class<?>, Method[]> CACHE_METHODS = new SimpleCache<>();


    /**
     * 获取类的指定注解
     * @param clazz 查询类
     * @param aClazz 指定注解类
     * @param <T> 注解类的泛型
     * @return 类的指定注解
     */
    public static <T extends Annotation> T getAnnotationOfClass(Class<?> clazz, Class<T> aClazz){
        return clazz.getAnnotation(aClazz);
    }

    /**
     * 根据构造函数类参数类型，获取相应的构造函数
     * @param clazz 类型
     * @param parameterTypes    参数类型
     * @param <T>   泛型
     * @return  返回对应的构造函数
     */
    @SuppressWarnings("unchecked")
	public static <T> Constructor<T> getConstructor(Class<T> clazz, Class<?>... parameterTypes) {
        if (null == clazz) {
            return null;
        }

        final Constructor<?>[] constructors = clazz.getConstructors();
        Class<?>[] pts;
        for (Constructor<?> constructor : constructors) {
            pts = constructor.getParameterTypes();
            if (ClassUtil.isAllAssignableFrom(pts, parameterTypes)) {
                return (Constructor<T>) constructor;
            }
        }
        return null;
    }

    /**
     * 获取类的构造函数
     * @param beanClass the bean's class
     * @return constructor
     */
    @SuppressWarnings("unchecked")
	public static <T> Constructor<T>[] getConstructors(Class<T> beanClass) throws SecurityException {
        Assert.notNull(beanClass);
        Constructor<?>[] constructors = CACHE_CONSTRUCTORS.get(beanClass);
        if (null != constructors) {
            return (Constructor<T>[]) constructors;
        }
        constructors = beanClass.getDeclaredConstructors();
        return (Constructor<T>[]) CACHE_CONSTRUCTORS.put(beanClass, constructors);
    }

    /**
     * find the right constructor by the parameters
     * @param beanClass the class of bean
     * @param parameters parameters
     * @return the constructor
     */
    public static <T> Constructor<T> findConstructorsByParameter(Class<T> beanClass, Object... parameters){
        Constructor<T>[] constructors = getConstructors(beanClass);
        List<Constructor<T>> list = new ArrayList<>();
        Object parameter;
        for(Constructor<T> constructor : constructors){
            Class<?>[] parameterTypes = constructor.getParameterTypes();
            if(parameters.length != parameterTypes.length){
                continue;
            }
            boolean success = true;
            for (int i = 0; i < parameters.length; i++) {
                parameter = parameters[i];
                if(parameter == null){
                    continue;
                }
                Class<?> parameterType = ClassUtil.wrap(parameterTypes[i]);
                if(!parameterType.isAssignableFrom(parameter.getClass())){
                    success = false;
                    break;
                }
            }
            if(success){
                list.add(constructor);
            }
        }
        if(list.size() > 1){
            throw new IllegalArgumentException("find more than one constructor for this parameters");
        }
        return list.size() == 0 ? null : list.get(0);
    }

    /**
     * 获取类以及父类的字段
     * @param cla   类
     * @return  返回所有字段
     */
    public static Field[] getFields(Class<?> cla){
        return getFields(cla, true);
    }

    /**
     * 返回类的字段
     * @param cla   类
     * @param withSuperClassFields  是否包含父级类的字段
     * @return  返回字段
     */
    public static Field[] getFields(Class<?> cla, boolean withSuperClassFields){
        Field[] fields = CACHE_FIELDS.get(cla);
        if(fields != null){
            return fields;
        }

        Class<?> searchType = cla;
        Field[] declaredFields;
        while (searchType != null) {
            declaredFields = searchType.getDeclaredFields();
            if (null == fields) {
                fields = declaredFields;
            } else {
                fields = ArrayUtil.append(fields, declaredFields);
            }
            searchType = withSuperClassFields ? searchType.getSuperclass() : null;
        }
        CACHE_FIELDS.put(cla, fields);
        return fields;
    }

    /**
     * 根据注解类获取字段
     * @param cla 类
     * @param aClazz 注解类
     * @param <T> 注解类型
     * @return 指定注解的字段集合
     */
    public static <T extends Annotation> List<Field> getFieldsByAnnotation(Class<?> cla, Class<T> aClazz){
        Field[] fields = getFields(cla);
        List<Field> fieldList = new ArrayList<>();
        for(Field f : fields){
            T annotation = f.getAnnotation(aClazz);
            if(annotation != null){
                fieldList.add(f);
            }
        }
        return fieldList;
    }

    /**
     * 根据字段名获取字段
     * @param beanClass 类
     * @param name  字段名称
     * @return  返回字段
     * @throws SecurityException    异常
     */
    public static Field getField(Class<?> beanClass, String name) throws SecurityException {
        final Field[] fields = getFields(beanClass);
        if (ArrayUtil.isNotEmpty(fields)) {
            for (Field field : fields) {
                if ((name.equals(field.getName()))) {
                    return field;
                }
            }
        }
        return null;
    }

    /**
     * 获取字段值
     *
     * @param obj 对象
     * @param fieldName 字段名
     * @return 字段值
     * @throws UtilException 包装IllegalAccessException异常
     */
    public static Object getFieldValue(Object obj, String fieldName) throws UtilException {
        if (null == obj || StringUtil.isBlank(fieldName)) {
            return null;
        }
        return getFieldValue(obj, getField(obj.getClass(), fieldName));
    }

    /**
     * 获取字段值
     *
     * @param obj 对象
     * @param field 字段
     * @return 字段值
     * @throws UtilException 包装IllegalAccessException异常
     */
    public static Object getFieldValue(Object obj, Field field){
        if (null == obj || null == field) {
            return null;
        }
        field.setAccessible(true);
        Object result;
        try {
            result = field.get(obj);
        } catch (IllegalAccessException e) {
            throw new UtilException(e, "IllegalAccess for {}.{}", obj.getClass(), field.getName());
        }
        return result;
    }

    /**
     * 获取字段的字符串值
     * @param obj 对象
     * @param field 变量
     * @return 字符串值
     */
    public static String getFieldStringValue(Object obj, Field field){
        Object value = getFieldValue(obj, field);
        if(value != null){
            return value.toString();
        }
        return null;
    }

    /**
     * 设置对象的字段值
     * @param obj   对象
     * @param fieldName 字段名称
     * @param value 值
     * @throws UtilException    工具异常
     */
    public static void setFieldValue(Object obj, String fieldName, Object value) throws UtilException {
        Assert.notNull(obj);
        Assert.hasText(fieldName);
        setFieldValue(obj, getField(obj.getClass(), fieldName), value);
    }


    /**
     * 设置对象的字段值
     * @param obj   对象
     * @param field 字段
     * @param value 值
     * @throws UtilException    包装IllegalAccessException异常
     */
    public static void setFieldValue(Object obj, Field field, Object value) throws UtilException {
        Assert.notNull(obj);
        Assert.notNull(field);
        field.setAccessible(true);

        try {
            field.set(obj, value);
        } catch (IllegalAccessException e) {
            throw new UtilException(e, "IllegalAccess for {}.{}", obj.getClass(), field.getName());
        }
    }

    // --------------------------------------------------------------------------------------------------------- method

    /**
     * 根据方法名称获取方法
     * @param clazz   类
     * @param methodName    方法名称
     * @param paramTypes    方法参数类型
     * @return  返回方法
     */
    public static Method getMethodByName(Class<?> clazz, String methodName, Class<?>... paramTypes){
        if (null == clazz || StringUtil.isBlank(methodName)) {
            throw new IllegalArgumentException("clazz or methodName not null");
        }
        if(ArrayUtil.containNull(paramTypes)){
            throw new IllegalArgumentException("paramTypes cannot contain null element");
        }
        Method[] methods = getMethods(clazz);
        for(Method method : methods){
            if(method.getName().equals(methodName)){
                Class<?>[] parameterTypes = method.getParameterTypes();
                if(ClassUtil.isAllAssignableFrom(parameterTypes, paramTypes)){
                   return method;
                }
            }
        }
        return null;
    }

    /**
     * 查找指定对象中的所有方法（包括非public方法），也包括父对象和Object类的方法
     *
     * @param obj 被查找的对象，如果为{@code null}返回{@code null}
     * @param methodName 方法名，如果为空字符串返回{@code null}
     * @param args 参数
     * @return 方法
     */
    public static Method getMethodByName(Object obj, String methodName, Object... args){
        if(ArrayUtil.containNull(args)){
            throw new IllegalArgumentException("args cannot contain null element");
        }
        return getMethodByName(obj.getClass(), methodName, ClassUtil.getClasses(args));
    }

    /**
     * 根据方法名和参数智能推断获取method
     * @param obj 实体对象
     * @param methodName 方法名称
     * @param args 参数
     * @return 方法
     */
    public static Method getMethodByNameSmartly(Object obj, String methodName, Object... args){
        if (null == obj || StringUtil.isBlank(methodName)) {
            throw new IllegalArgumentException("obj or methodName not null");
        }
        boolean needSmart = ArrayUtil.containNull(args);
        if(!needSmart){
            return getMethodByName(obj, methodName, args);
        }

        Class<?>[] paramTypes = ClassUtil.getClasses(args);
        Method[] methods = getMethods(obj.getClass());

        int checkSum = 0;
        boolean conflict = false;
        Method availableMethod = null;
        for(Method method : methods){
            if(method.getName().equals(methodName)){
                Class<?>[] parameterTypes = method.getParameterTypes();
                if(parameterTypes.length != paramTypes.length){
                    continue;
                }

                int s = 0;
                boolean pass = true;
                for(int i = 0; i < parameterTypes.length; i++) {
                    if(args[i] == null){
                        continue;
                    }
                    Class<?> c1 = parameterTypes[i];
                    Class<?> c2 = paramTypes[i];
                    if(!ClassUtil.isAssignableFrom(c2, c1)){
                        pass = false;
                        break;
                    }
                    s++;
                }
                if(!pass){
                    continue;
                }
                if(availableMethod == null ){
                    availableMethod = method;
                    checkSum = s;
                }
                else{
                    if(checkSum == s){
                        conflict = true;
                    }
                    else if(checkSum < s){
                        conflict = false;
                        availableMethod = method;
                        checkSum = s;
                    }
                }
            }
        }
        if(conflict){
            throw new UtilException("has same method with same arguments when has null element");
        }
        return availableMethod;
    }

    /**
     * 获取类以及父类的方法
     * @param cla   类
     * @return 返回类的方法
     */
    public static Method[] getMethods(Class<?> cla){
        return getMethods(cla, true);
    }

    /**
     * 返回类的方法
     * @param cla   类
     * @param withSuperClassMethods 是否包含父类的方法
     * @return  返回类的方法
     */
    public static Method[] getMethods(Class<?> cla, boolean withSuperClassMethods){
        Method[] methods = CACHE_METHODS.get(cla);
        if(methods != null){
            return methods;
        }

        Class<?> searchType = cla;
        Method[] declaredMethods;
        while (searchType != null) {
            declaredMethods = searchType.getDeclaredMethods();
            if (null == methods) {
                methods = declaredMethods;
            } else {
                methods = ArrayUtil.append(methods, declaredMethods);
            }
            searchType = withSuperClassMethods ? searchType.getSuperclass() : null;
        }
        CACHE_METHODS.put(cla, methods);
        return methods;
    }

    // --------------------------------------------------------------------------------------------------------- invoke

    /**
     * 执行静态方法
     *
     * @param <T> 对象类型
     * @param method 方法（对象方法或static方法都可）
     * @param args 参数对象
     * @return 结果
     * @throws UtilException 多种异常包装
     */
    public static <T> T invokeStatic(Method method, Object... args) throws UtilException{
        return invoke(null, method, args);
    }

    /**
     * 执行方法
     *
     * @param <T> 返回对象类型
     * @param obj 对象，如果执行静态方法，此值为<code>null</code>
     * @param method 方法（对象方法或static方法都可）
     * @param args 参数对象
     * @return 结果
     * @throws UtilException 一些列异常的包装
     */
    @SuppressWarnings("unchecked")
    public static <T> T invoke(Object obj, Method method, Object... args) throws UtilException{
        try {
            makeAccessible(method);
            return (T) method.invoke(ClassUtil.isStatic(method) ? null : obj, args);
        } catch (Exception e) {
            throw new UtilException(e);
        }
    }

    /**
     * 执行对象中指定方法
     *
     * @param <T> 返回对象类型
     * @param obj 方法所在对象
     * @param methodName 方法名
     * @param args 参数列表
     * @return 执行结果
     * @throws UtilException IllegalAccessException包装
     * @since 3.1.2
     */
    public static <T> T invoke(Object obj, String methodName, Object... args) throws UtilException{
        final Method method = getMethodByName(obj, methodName, args);
        if (null == method) {
            throw new UtilException(StringUtil.substitute("No such method: [{}]", methodName));
        }
        return invoke(obj, method, args);
    }

    /**
     * 确保字段可获取
     * @param field 字段
     */
    public static void makeAccessible(Field field) {
        if ((!Modifier.isPublic(field.getModifiers()) ||
                !Modifier.isPublic(field.getDeclaringClass().getModifiers()) ||
                Modifier.isFinal(field.getModifiers())) && !field.isAccessible()) {
            field.setAccessible(true);
        }
    }

    /**
     * 确保方法可调用
     * @param method 方法
     */
    public static void makeAccessible(Method method) {
        if ((!Modifier.isPublic(method.getModifiers()) ||
                !Modifier.isPublic(method.getDeclaringClass().getModifiers())) && !method.isAccessible()) {
            method.setAccessible(true);
        }
    }

    /**
     * 确保构造可用
     */
    public static void makeAccessible(Constructor<?> constructor) {
        if ((!Modifier.isPublic(constructor.getModifiers()) ||
                !Modifier.isPublic(constructor.getDeclaringClass().getModifiers())) && !constructor.isAccessible()) {
            constructor.setAccessible(true);
        }
    }

    // --------------------------------------------------------------------------------------------------------- newInstance

    /**
     * 实例化对象
     *
     * @param <T> 对象类型
     * @param clazz 类名
     * @return 对象
     * @throws UtilException 包装各类异常
     */
    @SuppressWarnings("unchecked")
    public static <T> T newInstance(String clazz) throws UtilException{
        try {
            return (T) Class.forName(clazz).newInstance();
        } catch (Exception e) {
            throw new UtilException(e, "Instance class [{}] error!", clazz);
        }
    }

    /**
     * 实例化对象
     *
     * @param <T> 对象类型
     * @param clazz 类
     * @param params 构造函数参数
     * @return 对象
     */
    public static <T> T newInstance(Class<T> clazz, Object... params){
        Assert.notNull(clazz, "Class must not be null");
        if (clazz.isInterface()) {
            throw new IllegalArgumentException("Specified class is an interface for "+clazz.getName());
        }
        Constructor<T> constructor = findConstructorsByParameter(clazz, params);
        if(constructor == null){
            throw new IllegalArgumentException(clazz.getName().concat(" can't find a right constructor for this parameter ".concat(Arrays.toString(params))));
        }
        return newInstance(constructor, params);
    }

    /**
     * 通过构造来实例对象
     * @param ctor 构造
     * @param params 参数
     * @param <T> 泛型
     * @return 对象
     */
    public static <T> T newInstance(Constructor<T> ctor, Object... params){
        Assert.notNull(ctor, "Constructor must not be null");
        try {
            makeAccessible(ctor);
            return ctor.newInstance(params);
        } catch (InstantiationException e) {
            throw new IllegalArgumentException("Is it an abstract class?", e);
        } catch (IllegalAccessException e) {
            throw new IllegalArgumentException("Is the constructor accessible?", e);
        } catch (InvocationTargetException e) {
            throw new IllegalArgumentException("Constructor threw exception", e);
        }
    }

    // --------------------------------------------------------------------------------------------------------- proxy

    /**
     * 创建动态代理类
     * @param interfaceType 接口类
     * @param handler   代理handler
     * @param <T>   接口类型
     * @return  返回代理类
     */
    public static <T> T newProxy(Class<T> interfaceType, InvocationHandler handler) {
        Assert.notNull(handler);
        Assert.isTrue(interfaceType.isInterface(), "{} is not an interface!", interfaceType);
        Object object = Proxy.newProxyInstance(
                interfaceType.getClassLoader(),
                new Class<?>[]{interfaceType},
                handler);
        return interfaceType.cast(object);
    }

}

package jexx.util;

import java.util.Collection;
import java.util.Map;

/**
 * 断言类
 *
 * @author Jeff
 */
public class Assert {

    public static void isTrue(boolean expression, String message, Object... args) {
        if (!expression) {
            throw new IllegalArgumentException(StringUtil.format(message, args));
        }
    }

    public static void isTrue(boolean expression) {
        isTrue(expression, "[Assertion failed] - this expression must be true");
    }

    public static void isFalse(boolean expression, String message, Object... args) {
        if (expression) {
            throw new IllegalArgumentException(StringUtil.format(message, args));
        }
    }

    public static void isFalse(boolean expression) {
        isFalse(expression, "[Assertion failed] - this expression must be false");
    }

    public static void isNull(Object object, String message, Object... args) {
        if (object != null) {
            throw new IllegalArgumentException(StringUtil.format(message, args));
        }
    }

    public static void isNull(Object object) {
        isNull(object, "[Assertion failed] - the object argument must be null");
    }

    public static void notNull(Object object, String message, Object... args) {
        if (object == null) {
            throw new IllegalArgumentException(StringUtil.format(message, args));
        }
    }

    public static void notNull(Object object) {
        notNull(object, "[Assertion failed] - this argument is required; it must not be null");
    }

    public static void hasLength(String text, String message, Object... args) {
        if (!StringUtil.hasLength(text)) {
            throw new IllegalArgumentException(StringUtil.format(message, args));
        }
    }

    public static void hasLength(String text) {
        hasLength(text,
                "[Assertion failed] - this String argument must have length; it must not be null or empty");
    }

    public static void hasText(String text, String message, Object... args) {
        if (!StringUtil.hasText(text)) {
            throw new IllegalArgumentException(StringUtil.format(message, args));
        }
    }

    public static void hasText(String text) {
        hasText(text,
                "[Assertion failed] - this String argument must have text; it must not be null, empty, or blank");
    }

    public static void doesNotContain(String textToSearch, String substring, String message, Object... args) {
        if (StringUtil.hasLength(textToSearch) && StringUtil.hasLength(substring) &&
                textToSearch.contains(substring)) {
            throw new IllegalArgumentException(StringUtil.format(message, args));
        }
    }

    public static void doesNotContain(String textToSearch, String substring) {
        doesNotContain(textToSearch, substring,
                "[Assertion failed] - this String argument must not contain the substring [" + substring + "]");
    }

    public static <T> void isEmpty(T[] array, String message, Object... args) {
        if (array != null && array.length > 0) {
            throw new IllegalArgumentException(StringUtil.format(message, args));
        }
    }

    public static <T> void isEmpty(T[] array) {
        notEmpty(array, "[Assertion failed] - this array must be empty!");
    }

    public static void isEmpty(Collection<?> collection, String message, Object... args) {
        if (collection != null && collection.size() > 0) {
            throw new IllegalArgumentException(StringUtil.format(message, args));
        }
    }

    public static void isEmpty(Collection<?> collection) {
        notEmpty(collection, "[Assertion failed] - this collection must be empty!");
    }

    public static void isEmpty(Map<?, ?> map, String message, Object... args) {
        if (map != null && map.size() > 0) {
            throw new IllegalArgumentException(StringUtil.format(message, args));
        }
    }

    public static void isEmpty(Map<?, ?> map) {
        notEmpty(map, "[Assertion failed] - this map must be empty!");
    }

    public static <T> void notEmpty(T[] array, String message, Object... args) {
        if (array == null || array.length == 0) {
            throw new IllegalArgumentException(StringUtil.format(message, args));
        }
    }

    public static <T> void notEmpty(T[] array) {
        notEmpty(array, "[Assertion failed] - this array must not be empty: it must contain at least 1 element");
    }

    public static void notEmpty(long[] array, String message, Object... args) {
        if (array == null || array.length == 0) {
            throw new IllegalArgumentException(StringUtil.format(message, args));
        }
    }

    public static void notEmpty(long[] array) {
        notEmpty(array, "[Assertion failed] - this array must not be empty: it must contain at least 1 element");
    }

    public static void notEmpty(int[] array, String message, Object... args) {
        if (array == null || array.length == 0) {
            throw new IllegalArgumentException(StringUtil.format(message, args));
        }
    }

    public static void notEmpty(int[] array) {
        notEmpty(array, "[Assertion failed] - this array must not be empty: it must contain at least 1 element");
    }

    public static void notEmpty(short[] array, String message, Object... args) {
        if (array == null || array.length == 0) {
            throw new IllegalArgumentException(StringUtil.format(message, args));
        }
    }

    public static void notEmpty(short[] array) {
        notEmpty(array, "[Assertion failed] - this array must not be empty: it must contain at least 1 element");
    }

    public static void notEmpty(char[] array, String message, Object... args) {
        if (array == null || array.length == 0) {
            throw new IllegalArgumentException(StringUtil.format(message, args));
        }
    }

    public static void notEmpty(char[] array) {
        notEmpty(array, "[Assertion failed] - this array must not be empty: it must contain at least 1 element");
    }

    public static void notEmpty(byte[] array, String message, Object... args) {
        if (array == null || array.length == 0) {
            throw new IllegalArgumentException(StringUtil.format(message, args));
        }
    }

    public static void notEmpty(byte[] array) {
        notEmpty(array, "[Assertion failed] - this array must not be empty: it must contain at least 1 element");
    }

    public static void notEmpty(float[] array, String message, Object... args) {
        if (array == null || array.length == 0) {
            throw new IllegalArgumentException(StringUtil.format(message, args));
        }
    }

    public static void notEmpty(float[] array) {
        notEmpty(array, "[Assertion failed] - this array must not be empty: it must contain at least 1 element");
    }

    public static void notEmpty(double[] array, String message, Object... args) {
        if (array == null || array.length == 0) {
            throw new IllegalArgumentException(StringUtil.format(message, args));
        }
    }

    public static void notEmpty(double[] array) {
        notEmpty(array, "[Assertion failed] - this array must not be empty: it must contain at least 1 element");
    }


    public static void notEmpty(Collection<?> collection, String message, Object... args) {
        if (collection == null || collection.size() == 0) {
            throw new IllegalArgumentException(StringUtil.format(message, args));
        }
    }

    public static void notEmpty(Collection<?> collection) {
        notEmpty(collection,
                "[Assertion failed] - this collection must not be empty: it must contain at least 1 element");
    }

    public static void notEmpty(Map<?, ?> map, String message, Object... args) {
        if (map == null || map.size() == 0) {
            throw new IllegalArgumentException(StringUtil.format(message, args));
        }
    }

    public static void notEmpty(Map<?, ?> map) {
        notEmpty(map, "[Assertion failed] - this map must not be empty; it must contain at least one entry");
    }

    public static void noNullElements(Object[] array, String message, Object... args) {
        if (array != null) {
            for (Object element : array) {
                if (element == null) {
                    throw new IllegalArgumentException(StringUtil.format(message, args));
                }
            }
        }
    }

    public static void noNullElements(Object[] array) {
        noNullElements(array, "[Assertion failed] - this array must not contain any null elements");
    }

    public static void isInstanceOf(Class<?> clazz, Object obj) {
        isInstanceOf(clazz, obj, "");
    }

    public static void isInstanceOf(Class<?> type, Object obj, String message, Object... args) {
        notNull(type, "Type to check against must not be null");
        if (!type.isInstance(obj)) {
            String msg = StringUtil.format(message, args);
            msg = StringUtil.format("{}, Object of class [{}] must be an instance of {}",
                    msg, obj != null ? obj.getClass().getName() : "null", type);
            throw new IllegalArgumentException(msg);
        }
    }

    public static void isAssignable(Class<?> superType, Class<?> subType) {
        isAssignable(superType, subType, "");
    }

    /**
     * 用来判断一个类superType和另一个类subType是否相同或是subType的超类或接口
     *
     * @param superType 超类
     * @param subType   子类或者同类
     * @param message
     * @param args
     */
    public static void isAssignable(Class<?> superType, Class<?> subType, String message, Object... args) {
        notNull(superType, "Type to check against must not be null");
        if (subType == null || !superType.isAssignableFrom(subType)) {
            throw new IllegalArgumentException(message + subType + " is not assignable to " + superType);
        }
    }

}

package jexx.util;

import jexx.exception.UtilException;

import java.lang.reflect.Array;
import java.util.*;
import java.util.function.Function;
import java.util.stream.IntStream;

/**
 * 数组工具集合
 */
public class ArrayUtil {

    // ---------------------------------------------------------------- append

    /**
     * 追加新元素
     * @param buffer    已有数组
     * @param newElements   新数组
     * @param <T>   数组元素类型
     * @return  返回追加后的数组
     */
	@SuppressWarnings("unchecked")
	public static <T> T[] append(T[] buffer, T... newElements){
        if(isEmpty(newElements)){
            return buffer;
        }
        T[] newBuffer = resize(buffer, buffer.length + newElements.length);
        copy(newElements, 0, newBuffer, buffer.length, newElements.length);
        return newBuffer;
    }

    /**
     * 追加字符串数据
     * @param buffer 已有数组
     * @param newElements 新数据
     * @return 追加后的字符串数组
     */
    public static String[] append(String[] buffer, String... newElements){
        if(isEmpty(newElements)){
            return buffer;
        }
        String[] newBuffer = resize(buffer, buffer.length + newElements.length);
        copy(newElements, 0, newBuffer, buffer.length, newElements.length);
        return newBuffer;
    }

    /** Append an element to <code>byte<code/> array */
    public static byte[] append(byte[] buffer, byte... newElements){
        if(isEmpty(newElements)){
            return buffer;
        }
        byte[] newBuffer = resize(buffer, buffer.length + newElements.length);
        copy(newElements, 0, newBuffer, buffer.length, newElements.length);
        return newBuffer;
    }

    /** Append an element to <code>char<code/> array */
    public static char[] append(char[] buffer, char... newElements){
        if(isEmpty(newElements)){
            return buffer;
        }
        char[] newBuffer = resize(buffer, buffer.length + newElements.length);
        copy(newElements, 0, newBuffer, buffer.length, newElements.length);
        return newBuffer;
    }

    /** Append an element to <code>short<code/> array */
    public static short[] append(short[] buffer, short... newElements){
        if(isEmpty(newElements)){
            return buffer;
        }
        short[] newBuffer = resize(buffer, buffer.length + newElements.length);
        copy(newElements, 0, newBuffer, buffer.length, newElements.length);
        return newBuffer;
    }

    /** Append an element to <code>int<code/> array */
    public static int[] append(int[] buffer, int... newElements){
        if(isEmpty(newElements)){
            return buffer;
        }
        int[] newBuffer = resize(buffer, buffer.length + newElements.length);
        copy(newElements, 0, newBuffer, buffer.length, newElements.length);
        return newBuffer;
    }

    /** Append an element to <code>long<code/> array */
    public static long[] append(long[] buffer, long... newElements){
        if(isEmpty(newElements)){
            return buffer;
        }
        long[] newBuffer = resize(buffer, buffer.length + newElements.length);
        copy(newElements, 0, newBuffer, buffer.length, newElements.length);
        return newBuffer;
    }

    /** Append an element to <code>float<code/> array */
    public static float[] append(float[] buffer, float... newElements){
        if(isEmpty(newElements)){
            return buffer;
        }
        float[] newBuffer = resize(buffer, buffer.length + newElements.length);
        copy(newElements, 0, newBuffer, buffer.length, newElements.length);
        return newBuffer;
    }

    /** Append an element to <code>double<code/> array */
    public static double[] append(double[] buffer, double... newElements){
        if(isEmpty(newElements)){
            return buffer;
        }
        double[] newBuffer = resize(buffer, buffer.length + newElements.length);
        copy(newElements, 0, newBuffer, buffer.length, newElements.length);
        return newBuffer;
    }

    /** Append an element to <code>boolean<code/> array */
    public static boolean[] append(boolean[] buffer, boolean... newElements){
        if(isEmpty(newElements)){
            return buffer;
        }
        boolean[] newBuffer = resize(buffer, buffer.length + newElements.length);
        copy(newElements, 0, newBuffer, buffer.length, newElements.length);
        return newBuffer;
    }

    // ---------------------------------------------------------------- join

    /**
     * 连接数组
     */
    @SuppressWarnings({"unchecked"})
    public static <T> T[] join(T[]... arrays) {
        Class<T> componentType = (Class<T>) arrays.getClass().getComponentType().getComponentType();
        return join(componentType, arrays);
    }

    /**
     * 使用提供的数组类型来连接数组
     */
    @SuppressWarnings({"unchecked"})
    public static <T> T[] join(Class<T> componentType, T[][] arrays) {
        if (arrays.length == 1) {
            return arrays[0];
        }
        int length = 0;
        for (T[] array : arrays) {
            length += array.length;
        }
        T[] result = (T[]) Array.newInstance(componentType, length);

        length = 0;
        for (T[] array : arrays) {
            System.arraycopy(array, 0, result, length, array.length);
            length += array.length;
        }
        return result;
    }


    /**
     * 连接字符串数组
     */
    public static String[] join(String[]... arrays) {
        if (arrays.length == 0) {
            return new String[0];
        }
        if (arrays.length == 1) {
            return arrays[0];
        }
        int length = 0;
        for (String[] array : arrays) {
            length += array.length;
        }
        String[] result = new String[length];
        length = 0;
        for (String[] array : arrays) {
            System.arraycopy(array, 0, result, length, array.length);
            length += array.length;
        }
        return result;
    }

    /**
     * 连接byte数组
     */
    public static byte[] join(byte[]... arrays) {
        if (arrays.length == 0) {
            return new byte[0];
        }
        if (arrays.length == 1) {
            return arrays[0];
        }
        int length = 0;
        for (byte[] array : arrays) {
            length += array.length;
        }
        byte[] result = new byte[length];
        length = 0;
        for (byte[] array : arrays) {
            System.arraycopy(array, 0, result, length, array.length);
            length += array.length;
        }
        return result;
    }

    /**
     * 连接char数组
     */
    public static char[] join(char[]... arrays) {
        if (arrays.length == 0) {
            return new char[0];
        }
        if (arrays.length == 1) {
            return arrays[0];
        }
        int length = 0;
        for (char[] array : arrays) {
            length += array.length;
        }
        char[] result = new char[length];
        length = 0;
        for (char[] array : arrays) {
            System.arraycopy(array, 0, result, length, array.length);
            length += array.length;
        }
        return result;
    }

    /**
     * 连接short数组
     */
    public static short[] join(short[]... arrays) {
        if (arrays.length == 0) {
            return new short[0];
        }
        if (arrays.length == 1) {
            return arrays[0];
        }
        int length = 0;
        for (short[] array : arrays) {
            length += array.length;
        }
        short[] result = new short[length];
        length = 0;
        for (short[] array : arrays) {
            System.arraycopy(array, 0, result, length, array.length);
            length += array.length;
        }
        return result;
    }

    /**
     * 连接int数组
     */
    public static int[] join(int[]... arrays) {
        if (arrays.length == 0) {
            return new int[0];
        }
        if (arrays.length == 1) {
            return arrays[0];
        }
        int length = 0;
        for (int[] array : arrays) {
            length += array.length;
        }
        int[] result = new int[length];
        length = 0;
        for (int[] array : arrays) {
            System.arraycopy(array, 0, result, length, array.length);
            length += array.length;
        }
        return result;
    }

    /**
     * 连接long数组
     */
    public static long[] join(long[]... arrays) {
        if (arrays.length == 0) {
            return new long[0];
        }
        if (arrays.length == 1) {
            return arrays[0];
        }
        int length = 0;
        for (long[] array : arrays) {
            length += array.length;
        }
        long[] result = new long[length];
        length = 0;
        for (long[] array : arrays) {
            System.arraycopy(array, 0, result, length, array.length);
            length += array.length;
        }
        return result;
    }

    /**
     * 连接float数组
     */
    public static float[] join(float[]... arrays) {
        if (arrays.length == 0) {
            return new float[0];
        }
        if (arrays.length == 1) {
            return arrays[0];
        }
        int length = 0;
        for (float[] array : arrays) {
            length += array.length;
        }
        float[] result = new float[length];
        length = 0;
        for (float[] array : arrays) {
            System.arraycopy(array, 0, result, length, array.length);
            length += array.length;
        }
        return result;
    }

    /**
     * 连接double数组
     */
    public static double[] join(double[]... arrays) {
        if (arrays.length == 0) {
            return new double[0];
        }
        if (arrays.length == 1) {
            return arrays[0];
        }
        int length = 0;
        for (double[] array : arrays) {
            length += array.length;
        }
        double[] result = new double[length];
        length = 0;
        for (double[] array : arrays) {
            System.arraycopy(array, 0, result, length, array.length);
            length += array.length;
        }
        return result;
    }

    /**
     * 连接bool数组
     */
    public static boolean[] join(boolean[]... arrays) {
        if (arrays.length == 0) {
            return new boolean[0];
        }
        if (arrays.length == 1) {
            return arrays[0];
        }
        int length = 0;
        for (boolean[] array : arrays) {
            length += array.length;
        }
        boolean[] result = new boolean[length];
        length = 0;
        for (boolean[] array : arrays) {
            System.arraycopy(array, 0, result, length, array.length);
            length += array.length;
        }
        return result;
    }

    // ---------------------------------------------------------------- clone

    /**
     * 克隆数组
     *
     * @param <T> 数组元素类型
     * @param array 被克隆的数组
     * @return 新数组
     */
    public static <T> T[] clone(T[] array) {
        if (array == null) {
            return null;
        }
        return array.clone();
    }

    /**
     * 克隆数组，如果非数组返回<code>null</code>
     *
     * @param <T> 数组元素类型
     * @param obj 数组对象
     * @return 克隆后的数组对象
     */
    @SuppressWarnings("unchecked")
    public static <T> T clone(final T obj) {
        if (null == obj) {
            return null;
        }
        if (isArray(obj)) {
            final Object result;
            final Class<?> componentType = obj.getClass().getComponentType();
            if (componentType.isPrimitive()) {// 原始类型
                int length = Array.getLength(obj);
                result = Array.newInstance(componentType, length);
                while (length-- > 0) {
                    Array.set(result, length, Array.get(obj, length));
                }
            } else {
                result = ((Object[]) obj).clone();
            }
            return (T) result;
        }
        return null;
    }

    //-------------------------------------------------------------------------contain

    /**
     * 判断数组中是否包含元素
     * @param array 数组
     * @param objectToFind 查询元素
     */
    public static boolean contain(final Object[] array, final Object objectToFind) {
        return indexOf(array, objectToFind) != -1;
    }

    public static boolean contain(final double[] array, final double objectToFind) {
        return indexOf(array, objectToFind) != -1;
    }

    public static boolean contain(final long[] array, final long objectToFind) {
        return indexOf(array, objectToFind) != -1;
    }

    public static boolean contain(final int[] array, final int objectToFind) {
        return indexOf(array, objectToFind) != -1;
    }

    public static boolean contain(final short[] array, final short objectToFind) {
        return indexOf(array, objectToFind) != -1;
    }

    public static boolean contain(final char[] array, final char objectToFind) {
        return indexOf(array, objectToFind) != -1;
    }

    public static boolean contain(final byte[] array, final byte objectToFind) {
        return indexOf(array, objectToFind) != -1;
    }

    public static boolean contain(final boolean[] array, final boolean objectToFind) {
        return indexOf(array, objectToFind) != -1;
    }

    /**
     * 是否包含null
     * @param array 数组
     * @return bool
     */
    public static boolean containNull(final Object[] array) {
        return indexOf(array, null) != -1;
    }


    /**
     * 包装 {@link System#arraycopy(Object, int, Object, int, int)}<br>
     * 数组复制
     *
     * @param src 源数组
     * @param srcPos 源数组开始位置
     * @param dest 目标数组
     * @param destPos 目标数组开始位置
     * @param length 拷贝数组长度
     * @return 目标数组
     */
    public static Object copy(Object src, int srcPos, Object dest, int destPos, int length) {
        System.arraycopy(src, srcPos, dest, destPos, length);
        return dest;
    }

    /**
     * 包装 {@link System#arraycopy(Object, int, Object, int, int)}<br>
     * 数组复制，缘数组和目标数组都是从位置0开始复制
     *
     * @param src 源数组
     * @param dest 目标数组
     * @param length 拷贝数组长度
     * @return 目标数组
     */
    public static Object copy(Object src, Object dest, int length) {
        System.arraycopy(src, 0, dest, 0, length);
        return dest;
    }

    /**
     * 判断对象是否为数组
     * @param obj 对象
     */
    public static boolean isArray(Object obj) {
        if (null == obj) {
            return false;
        }
        return obj.getClass().isArray();
    }

    /**
     * 获取数组对象的元素类型
     *
     * @param array 数组对象
     * @return 元素类型
     */
    public static Class<?> getComponentType(Object array) {
        return getComponentType(array.getClass());
    }

    /**
     * 获取数组对象的元素类型
     *
     * @param arrayClass 数组类
     * @return 元素类型
     */
    public static Class<?> getComponentType(Class<?> arrayClass) {
        return null == arrayClass ? null : arrayClass.getComponentType();
    }

    public static boolean hasSame(int[] a, int[] b){
        if(isEmpty(a) || isEmpty(b)){
            return false;
        }
        for (int i : a){
            for (int j : b){
                if(i == j){
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean hasSame(long[] a, long[] b){
        if(isEmpty(a) || isEmpty(b)){
            return false;
        }
        for (long i : a){
            for (long j : b){
                if(i == j){
                    return true;
                }
            }
        }
        return false;
    }

    public static <T> boolean hasSame(T[] a, T[] b){
        if(isEmpty(a) || isEmpty(b)){
            return false;
        }
        for (T i : a){
            for (T j : b){
                if(i != null && i.equals(j)){
                    return true;
                }
            }
        }
        return false;
    }

    //------------------------------------------------------------------------------------------indexOf

    public static int indexOf(final Object[] array, final Object objectToFind, int startIndex) {
        if (array == null) {
            return -1;
        }
        startIndex = Math.max(startIndex, 0);
        for (int i = startIndex; i < array.length; i++) {
            if(ObjectUtil.nullSafeEquals(array[i], objectToFind)){
                return i;
            }
        }
        return -1;
    }

    public static int indexOf(final Object[] array, final Object objectToFind) {
        return indexOf(array, objectToFind, 0);
    }

    public static int indexOf(final double[] array, final double objectToFind, int startIndex) {
        if (array == null) {
            return -1;
        }
        startIndex = Math.max(startIndex, 0);
        for (int i = startIndex; i < array.length; i++) {
            if(objectToFind == array[i]){
                return i;
            }
        }
        return -1;
    }

    public static int indexOf(final double[] array, final double objectToFind) {
        return indexOf(array, objectToFind, 0);
    }

    public static int indexOf(final long[] array, final long objectToFind, int startIndex) {
        if (array == null) {
            return -1;
        }
        startIndex = Math.max(startIndex, 0);
        for (int i = startIndex; i < array.length; i++) {
            if(objectToFind == array[i]){
                return i;
            }
        }
        return -1;
    }

    public static int indexOf(final long[] array, final long objectToFind) {
        return indexOf(array, objectToFind, 0);
    }

    public static int indexOf(final int[] array, final int objectToFind, int startIndex) {
        if (array == null) {
            return -1;
        }
        startIndex = Math.max(startIndex, 0);
        for (int i = startIndex; i < array.length; i++) {
            if(objectToFind == array[i]){
                return i;
            }
        }
        return -1;
    }

    public static int indexOf(final int[] array, final int objectToFind) {
        return indexOf(array, objectToFind, 0);
    }

    public static int indexOf(final short[] array, final short objectToFind, int startIndex) {
        if (array == null) {
            return -1;
        }
        startIndex = Math.max(startIndex, 0);
        for (int i = startIndex; i < array.length; i++) {
            if(objectToFind == array[i]){
                return i;
            }
        }
        return -1;
    }

    public static int indexOf(final short[] array, final short objectToFind) {
        return indexOf(array, objectToFind, 0);
    }

    public static int indexOf(final byte[] array, final byte objectToFind, int startIndex) {
        if (array == null) {
            return -1;
        }
        startIndex = Math.max(startIndex, 0);
        for (int i = startIndex; i < array.length; i++) {
            if(objectToFind == array[i]){
                return i;
            }
        }
        return -1;
    }

    public static int indexOf(final byte[] array, final byte objectToFind) {
        return indexOf(array, objectToFind, 0);
    }

    public static int indexOf(final char[] array, final char objectToFind, int startIndex) {
        if (array == null) {
            return -1;
        }
        startIndex = Math.max(startIndex, 0);
        for (int i = startIndex; i < array.length; i++) {
            if(objectToFind == array[i]){
                return i;
            }
        }
        return -1;
    }

    public static int indexOf(final char[] array, final char objectToFind) {
        return indexOf(array, objectToFind, 0);
    }

    public static int indexOf(final boolean[] array, final boolean objectToFind, int startIndex) {
        if (array == null) {
            return -1;
        }
        startIndex = Math.max(startIndex, 0);
        for (int i = startIndex; i < array.length; i++) {
            if(objectToFind == array[i]){
                return i;
            }
        }
        return -1;
    }

    public static int indexOf(final boolean[] array, final boolean objectToFind) {
        return indexOf(array, objectToFind, 0);
    }

    public static int lastIndexOf(final Object[] array, final Object objectToFind, int startIndex) {
        if (array == null) {
            return -1;
        }
        startIndex = Math.min(startIndex, array.length-1);
        for (int i = startIndex; i >= 0; i--) {
            if(ObjectUtil.nullSafeEquals(array[i], objectToFind)){
                return i;
            }
        }
        return -1;
    }

    public static int lastIndexOf(final Object[] array, final Object objectToFind) {
        return lastIndexOf(array, objectToFind, array.length-1);
    }

    public static int lastIndexOf(final double[] array, final double objectToFind, int startIndex) {
        if (array == null) {
            return -1;
        }
        startIndex = Math.min(startIndex, array.length-1);
        for (int i = startIndex; i >= 0; i--) {
            if(array[i] == objectToFind){
                return i;
            }
        }
        return -1;
    }

    public static int lastIndexOf(final double[] array, final double objectToFind) {
        return lastIndexOf(array, objectToFind, array.length-1);
    }

    public static int lastIndexOf(final long[] array, final long objectToFind, int startIndex) {
        if (array == null) {
            return -1;
        }
        startIndex = Math.min(startIndex, array.length-1);
        for (int i = startIndex; i >= 0; i--) {
            if(array[i] == objectToFind){
                return i;
            }
        }
        return -1;
    }

    public static int lastIndexOf(final long[] array, final long objectToFind) {
        return lastIndexOf(array, objectToFind, array.length-1);
    }

    public static int lastIndexOf(final int[] array, final int objectToFind, int startIndex) {
        if (array == null) {
            return -1;
        }
        startIndex = Math.min(startIndex, array.length-1);
        for (int i = startIndex; i >= 0; i--) {
            if(array[i] == objectToFind){
                return i;
            }
        }
        return -1;
    }

    public static int lastIndexOf(final int[] array, final int objectToFind) {
        return lastIndexOf(array, objectToFind, array.length-1);
    }

    public static int lastIndexOf(final short[] array, final short objectToFind, int startIndex) {
        if (array == null) {
            return -1;
        }
        startIndex = Math.min(startIndex, array.length-1);
        for (int i = startIndex; i >= 0; i--) {
            if(array[i] == objectToFind){
                return i;
            }
        }
        return -1;
    }

    public static int lastIndexOf(final short[] array, final short objectToFind) {
        return lastIndexOf(array, objectToFind, array.length-1);
    }

    public static int lastIndexOf(final char[] array, final char objectToFind, int startIndex) {
        if (array == null) {
            return -1;
        }
        startIndex = Math.min(startIndex, array.length-1);
        for (int i = startIndex; i >= 0; i--) {
            if(array[i] == objectToFind){
                return i;
            }
        }
        return -1;
    }

    public static int lastIndexOf(final char[] array, final char objectToFind) {
        return lastIndexOf(array, objectToFind, array.length-1);
    }

    public static int lastIndexOf(final byte[] array, final byte objectToFind, int startIndex) {
        if (array == null) {
            return -1;
        }
        startIndex = Math.min(startIndex, array.length-1);
        for (int i = startIndex; i >= 0; i--) {
            if(array[i] == objectToFind){
                return i;
            }
        }
        return -1;
    }

    public static int lastIndexOf(final byte[] array, final byte objectToFind) {
        return lastIndexOf(array, objectToFind, array.length-1);
    }

    public static int lastIndexOf(final boolean[] array, final boolean objectToFind, int startIndex) {
        if (array == null) {
            return -1;
        }
        startIndex = Math.min(startIndex, array.length-1);
        for (int i = startIndex; i >= 0; i--) {
            if(array[i] == objectToFind){
                return i;
            }
        }
        return -1;
    }

    public static int lastIndexOf(final boolean[] array, final boolean objectToFind) {
        return lastIndexOf(array, objectToFind, array.length-1);
    }

    //------------------------------------------------------------------------------------------min or max

    public static long min(long... numberArray){
        Assert.notEmpty(numberArray, "it must contain at least 1 element");
        long min = numberArray[0];
        for (int i = 0; i < numberArray.length; i++) {
            if (min > numberArray[i]) {
                min = numberArray[i];
            }
        }
        return min;
    }

    public static int min(int... numberArray){
        Assert.notEmpty(numberArray, "it must contain at least 1 element");
        int min = numberArray[0];
        for (int i = 0; i < numberArray.length; i++) {
            if (min > numberArray[i]) {
                min = numberArray[i];
            }
        }
        return min;
    }

    public static short min(short... numberArray){
        Assert.notEmpty(numberArray, "it must contain at least 1 element");
        short min = numberArray[0];
        for (int i = 0; i < numberArray.length; i++) {
            if (min > numberArray[i]) {
                min = numberArray[i];
            }
        }
        return min;
    }

    public static char min(char... numberArray){
        Assert.notEmpty(numberArray, "it must contain at least 1 element");
        char min = numberArray[0];
        for (int i = 0; i < numberArray.length; i++) {
            if (min > numberArray[i]) {
                min = numberArray[i];
            }
        }
        return min;
    }

    public static byte min(byte... numberArray){
        Assert.notEmpty(numberArray, "it must contain at least 1 element");
        byte min = numberArray[0];
        for (int i = 0; i < numberArray.length; i++) {
            if (min > numberArray[i]) {
                min = numberArray[i];
            }
        }
        return min;
    }

    public static float min(float... numberArray){
        Assert.notEmpty(numberArray, "it must contain at least 1 element");
        float min = numberArray[0];
        for (int i = 0; i < numberArray.length; i++) {
            if (min > numberArray[i]) {
                min = numberArray[i];
            }
        }
        return min;
    }

    public static double min(double... numberArray){
        Assert.notEmpty(numberArray, "it must contain at least 1 element");
        double min = numberArray[0];
        for (int i = 0; i < numberArray.length; i++) {
            if (min > numberArray[i]) {
                min = numberArray[i];
            }
        }
        return min;
    }

    public static long max(long... numberArray){
        Assert.notEmpty(numberArray, "it must contain at least 1 element");
        long min = numberArray[0];
        for (int i = 0; i < numberArray.length; i++) {
            if (min < numberArray[i]) {
                min = numberArray[i];
            }
        }
        return min;
    }

    public static int max(int... numberArray){
        Assert.notEmpty(numberArray, "it must contain at least 1 element");
        int min = numberArray[0];
        for (int i = 0; i < numberArray.length; i++) {
            if (min < numberArray[i]) {
                min = numberArray[i];
            }
        }
        return min;
    }

    public static short max(short... numberArray){
        Assert.notEmpty(numberArray, "it must contain at least 1 element");
        short min = numberArray[0];
        for (int i = 0; i < numberArray.length; i++) {
            if (min < numberArray[i]) {
                min = numberArray[i];
            }
        }
        return min;
    }

    public static char max(char... numberArray){
        Assert.notEmpty(numberArray, "it must contain at least 1 element");
        char min = numberArray[0];
        for (int i = 0; i < numberArray.length; i++) {
            if (min < numberArray[i]) {
                min = numberArray[i];
            }
        }
        return min;
    }

    public static byte max(byte... numberArray){
        Assert.notEmpty(numberArray, "it must contain at least 1 element");
        byte min = numberArray[0];
        for (int i = 0; i < numberArray.length; i++) {
            if (min < numberArray[i]) {
                min = numberArray[i];
            }
        }
        return min;
    }

    public static float max(float... numberArray){
        Assert.notEmpty(numberArray, "it must contain at least 1 element");
        float min = numberArray[0];
        for (int i = 0; i < numberArray.length; i++) {
            if (min < numberArray[i]) {
                min = numberArray[i];
            }
        }
        return min;
    }

    public static double max(double... numberArray){
        Assert.notEmpty(numberArray, "it must contain at least 1 element");
        double min = numberArray[0];
        for (int i = 0; i < numberArray.length; i++) {
            if (min < numberArray[i]) {
                min = numberArray[i];
            }
        }
        return min;
    }

    public static <T extends Comparable<? super T>> T max(T[] numberArray) {
        if (isEmpty(numberArray)) {
            throw new IllegalArgumentException("Number array must not empty !");
        }
        T max = numberArray[0];
        for (T t : numberArray) {
            if(max.compareTo(t) < 0){
                max = t;
            }
        }
        return max;
    }


    /**
     * 根据元素类型新建数组
     * @param componentType 元素类型
     * @param length    新建数组长度
     * @param <T>  元素类型
     * @return  返回新建后的数组
     */
    @SuppressWarnings("unchecked")
	public static <T> T[] newArray(Class<T> componentType, int length) {
        return (T[]) Array.newInstance(componentType, length);
    }

    /**
     * 获取数组长度<br>
     * 如果参数为{@code null}，返回0
     *
     * <pre>
     * ArrayUtil.length(null)            = 0
     * ArrayUtil.length([])              = 0
     * ArrayUtil.length([null])          = 1
     * ArrayUtil.length([true, false])   = 2
     * ArrayUtil.length([1, 2, 3])       = 3
     * ArrayUtil.length(["a", "b", "c"]) = 3
     * </pre>
     *
     * @param array 数组对象
     * @return 数组长度
     * @throws IllegalArgumentException 如果参数不为数组，抛出此异常
     * @since 3.0.8
     * @see Array#getLength(Object)
     */
    public static int length(Object array) throws IllegalArgumentException {
        if (null == array) {
            return 0;
        }
        return Array.getLength(array);
    }


    //------------------------------------------------------------------toArray

    /**
     * 转换为数组
     * @param items 元素
     * @param <T>   泛型
     * @return  数组
     */
	public static <T> T[] toArray(final T... items){
        return items;
    }

    /**
     * 集合转数组
     * @param collection  集合
     * @param componentType 集合泛型
     * @param <T> 泛型
     * @return 数组
     */
    public static <T> T[] toArray(Collection<T> collection, Class<T> componentType){
        if(CollectionUtil.isEmpty(collection)){
            return newArray(componentType, 0);
        }
        T[] array = newArray(componentType, collection.size());
        return collection.toArray(array);
    }

    /**
     * 迭代器转数组
     * @param iterator  迭代器转数组
     * @return 数组; 若无集合元素,则返回<code>null</code>
     */
    public static <T> T[] toArray(Iterator<T> iterator, Class<T> componentType){
        List<T> list = new ArrayList<>();
        while (iterator.hasNext()){
            list.add(iterator.next());
        }
        return toArray(list, componentType);
    }

    /**
     * 迭代器转数组
     * @param iterable 迭代器
     * @return 数组; 若无集合元素,则返回<code>null</code>
     */
    public static <T> T[] toArray(Iterable<T> iterable, Class<T> componentType){
        return toArray(iterable.iterator(), componentType);
    }

    public static <T,R> R[] toArray(T[] source, Class<R> clazz, Function<T,R> function){
        R[] target = newArray(clazz, source.length);
        for(int i = 0; i < source.length; i++){
            target[i] = function.apply(source[i]);
        }
        return target;
    }

    public static <T> int[] toIntArray(Collection<Integer> collection){
        if(CollectionUtil.isEmpty(collection)){
            return new int[0];
        }
        return collection.stream().flatMapToInt(IntStream::of).toArray();
    }

    /**
     * 集合转数组
     * @param collection 集合
     * @return 字符串数组
     */
    public static String[] toStringArray(Collection<String> collection){
        return collection.toArray(new String[0]);
    }

    /**
     * 集合转数组
     * @param enumeration 集合
     * @return 字符串数组
     */
    public static String[] toStringArray(Enumeration<String> enumeration) {
        return toStringArray(Collections.list(enumeration));
    }

    //-------------------------------------------------------------toString

    /**
     * 数组或集合转String
     *
     * @param obj 集合或数组对象
     * @return 数组字符串，与集合转字符串格式相同
     */
    @SuppressWarnings("ConstantConditions")
    public static String toString(Object obj) {
        if (null == obj) {
            return null;
        }
        if (ArrayUtil.isArray(obj)) {
            try {
                return Arrays.deepToString((Object[]) obj);
            } catch (Exception e) {
                final String className = obj.getClass().getComponentType().getName();
                switch (className) {
                    case "long":
                        return Arrays.toString((long[]) obj);
                    case "int":
                        return Arrays.toString((int[]) obj);
                    case "short":
                        return Arrays.toString((short[]) obj);
                    case "char":
                        return Arrays.toString((char[]) obj);
                    case "byte":
                        return Arrays.toString((byte[]) obj);
                    case "boolean":
                        return Arrays.toString((boolean[]) obj);
                    case "float":
                        return Arrays.toString((float[]) obj);
                    case "double":
                        return Arrays.toString((double[]) obj);
                    default:
                        throw new UtilException(e);
                }
            }
        }
        return obj.toString();
    }

    //---------------------------------------------------- resize

    /**
     * 调整数组大小
     * @param buffer    数组
     * @param length    调整之后的数组长度
     * @param <T>   数组中元素类型
     * @return  返回调整大小后的数组
     */
    public static <T> T[] resize(T[] buffer, int length){
        Assert.isTrue(length > 0);
        @SuppressWarnings("unchecked") // OK, because array is of type T
        T[] newBuffer = newArray((Class<T>)buffer.getClass().getComponentType(), length);
        if(isEmpty(buffer)){
            return newBuffer;
        }
        System.arraycopy(buffer, 0, newBuffer, 0, Math.min(buffer.length, length));
        return newBuffer;
    }

    /** Resize a <code>byte</code> array */
    public static byte[] resize(byte[] buffer, int length){
        Assert.isTrue(length > 0);
        byte[] newBuffer = new byte[length];
        if(isEmpty(buffer)){
            return newBuffer;
        }
        System.arraycopy(buffer, 0, newBuffer, 0, Math.min(buffer.length, length));
        return newBuffer;
    }

    /** Resize a <code>char</code> array */
    public static char[] resize(char[] buffer, int length){
        Assert.isTrue(length > 0);
        char[] newBuffer = new char[length];
        if(isEmpty(buffer)){
            return newBuffer;
        }
        System.arraycopy(buffer, 0, newBuffer, 0, Math.min(buffer.length, length));
        return newBuffer;
    }

    /** Resize a <code>short</code> array */
    public static short[] resize(short[] buffer, int length){
        Assert.isTrue(length > 0);
        short[] newBuffer = new short[length];
        if(isEmpty(buffer)){
            return newBuffer;
        }
        System.arraycopy(buffer, 0, newBuffer, 0, Math.min(buffer.length, length));
        return newBuffer;
    }

    /** Resize a <code>int</code> array */
    public static int[] resize(int[] buffer, int length){
        Assert.isTrue(length > 0);
        int[] newBuffer = new int[length];
        if(isEmpty(buffer)){
            return newBuffer;
        }
        System.arraycopy(buffer, 0, newBuffer, 0, Math.min(buffer.length, length));
        return newBuffer;
    }

    /** Resize a <code>long</code> array */
    public static long[] resize(long[] buffer, int length){
        Assert.isTrue(length > 0);
        long[] newBuffer = new long[length];
        if(isEmpty(buffer)){
            return newBuffer;
        }
        System.arraycopy(buffer, 0, newBuffer, 0, Math.min(buffer.length, length));
        return newBuffer;
    }

    /** Resize a <code>float</code> array */
    public static float[] resize(float[] buffer, int length){
        Assert.isTrue(length > 0);
        float[] newBuffer = new float[length];
        if(isEmpty(buffer)){
            return newBuffer;
        }
        System.arraycopy(buffer, 0, newBuffer, 0, Math.min(buffer.length, length));
        return newBuffer;
    }

    /** Resize a <code>double</code> array */
    public static double[] resize(double[] buffer, int length){
        Assert.isTrue(length > 0);
        double[] newBuffer = new double[length];
        if(isEmpty(buffer)){
            return newBuffer;
        }
        System.arraycopy(buffer, 0, newBuffer, 0, Math.min(buffer.length, length));
        return newBuffer;
    }

    /** Resize a <code>boolean</code> array */
    public static boolean[] resize(boolean[] buffer, int length){
        Assert.isTrue(length > 0);
        boolean[] newBuffer = new boolean[length];
        if(isEmpty(buffer)){
            return newBuffer;
        }
        System.arraycopy(buffer, 0, newBuffer, 0, Math.min(buffer.length, length));
        return newBuffer;
    }

    // ------------------------------------------------------------------- subarray

    /**
     * 截取数组
     * @param array 数组
     * @param startIndex 开始所有
     * @param endIndex 结束索引
     * @param <T> 数组泛型
     * @return 截取后的数组
     */
    public static <T> T[] subarray(final T[] array, int startIndex, int endIndex) {
        if (array == null) {
            return null;
        }
        startIndex = Math.max(startIndex, 0);
        endIndex = Math.min(endIndex, array.length);

        final int newSize = endIndex - startIndex;
        final Class<?> type = array.getClass().getComponentType();
        if (newSize <= 0) {
            @SuppressWarnings("unchecked") // OK, because array is of type T
            final T[] emptyArray = (T[])newArray(type, 0);
            return emptyArray;
        }
        @SuppressWarnings("unchecked") // OK, because array is of type T
        final T[] subArray = (T[]) newArray(type, newSize);
        System.arraycopy(array, startIndex, subArray, 0, newSize);
        return subArray;
    }

    /**
     * @see ArrayUtil#subarray(Object[], int, int)
     */
    public static <T> T[] subarray(final T[] array, int startIndex) {
        return subarray(array, startIndex, array.length);
    }

    public static double[] subarray(final double[] array, int startIndex, int endIndex) {
        if (array == null) {
            return null;
        }
        startIndex = Math.max(startIndex, 0);
        endIndex = Math.min(endIndex, array.length);

        final int newSize = endIndex - startIndex;
        if (newSize <= 0) {
            return new double[0];
        }
        final double[] subArray = new double[newSize];;
        System.arraycopy(array, startIndex, subArray, 0, newSize);
        return subArray;
    }

    public static double[] subarray(final double[] array, int startIndex) {
        return subarray(array, startIndex, array.length);
    }

    public static long[] subarray(final long[] array, int startIndex, int endIndex) {
        if (array == null) {
            return null;
        }
        startIndex = Math.max(startIndex, 0);
        endIndex = Math.min(endIndex, array.length);

        final int newSize = endIndex - startIndex;
        if (newSize <= 0) {
            return new long[0];
        }
        final long[] subArray = new long[newSize];;
        System.arraycopy(array, startIndex, subArray, 0, newSize);
        return subArray;
    }

    public static long[] subarray(final long[] array, int startIndex) {
        return subarray(array, startIndex, array.length);
    }

    public static int[] subarray(final int[] array, int startIndex, int endIndex) {
        if (array == null) {
            return null;
        }
        startIndex = Math.max(startIndex, 0);
        endIndex = Math.min(endIndex, array.length);

        final int newSize = endIndex - startIndex;
        if (newSize <= 0) {
            return new int[0];
        }
        final int[] subArray = new int[newSize];;
        System.arraycopy(array, startIndex, subArray, 0, newSize);
        return subArray;
    }

    public static int[] subarray(final int[] array, int startIndex) {
        return subarray(array, startIndex, array.length);
    }

    public static short[] subarray(final short[] array, int startIndex, int endIndex) {
        if (array == null) {
            return null;
        }
        startIndex = Math.max(startIndex, 0);
        endIndex = Math.min(endIndex, array.length);

        final int newSize = endIndex - startIndex;
        if (newSize <= 0) {
            return new short[0];
        }
        final short[] subArray = new short[newSize];;
        System.arraycopy(array, startIndex, subArray, 0, newSize);
        return subArray;
    }

    public static short[] subarray(final short[] array, int startIndex) {
        return subarray(array, startIndex, array.length);
    }

    public static byte[] subarray(final byte[] array, int startIndex, int endIndex) {
        if (array == null) {
            return null;
        }
        startIndex = Math.max(startIndex, 0);
        endIndex = Math.min(endIndex, array.length);

        final int newSize = endIndex - startIndex;
        if (newSize <= 0) {
            return new byte[0];
        }
        final byte[] subArray = new byte[newSize];;
        System.arraycopy(array, startIndex, subArray, 0, newSize);
        return subArray;
    }

    public static byte[] subarray(final byte[] array, int startIndex) {
        return subarray(array, startIndex, array.length);
    }

    public static char[] subarray(final char[] array, int startIndex, int endIndex) {
        if (array == null) {
            return null;
        }
        startIndex = Math.max(startIndex, 0);
        endIndex = Math.min(endIndex, array.length);

        final int newSize = endIndex - startIndex;
        if (newSize <= 0) {
            return new char[0];
        }
        final char[] subArray = new char[newSize];;
        System.arraycopy(array, startIndex, subArray, 0, newSize);
        return subArray;
    }

    public static char[] subarray(final char[] array, int startIndex) {
        return subarray(array, startIndex, array.length);
    }

    public static boolean[] subarray(final boolean[] array, int startIndex, int endIndex) {
        if (array == null) {
            return null;
        }
        startIndex = Math.max(startIndex, 0);
        endIndex = Math.min(endIndex, array.length);

        final int newSize = endIndex - startIndex;
        if (newSize <= 0) {
            return new boolean[0];
        }
        final boolean[] subArray = new boolean[newSize];;
        System.arraycopy(array, startIndex, subArray, 0, newSize);
        return subArray;
    }

    public static boolean[] subarray(final boolean[] array, int startIndex) {
        return subarray(array, startIndex, array.length);
    }

    // ------------------------------------------------------------------- Wrap and unwrap

    /**
     * 将原始类型数组包装为包装类型
     * @param values 原始类型数组
     * @return 包装类型数组
     */
    public static Boolean[] wrap(boolean... values) {
        if (null == values) {
            return null;
        }
        final int length = values.length;
        if (0 == length) {
            return new Boolean[0];
        }

        final Boolean[] array = new Boolean[length];
        for (int i = 0; i < length; i++) {
            array[i] = values[i];
        }
        return array;
    }

    /**
     * 包装类数组转为原始类型数组
     * @param values 包装类型数组
     * @return 原始类型数组
     */
    public static boolean[] unWrap(Boolean... values) {
        if (null == values) {
            return null;
        }
        final int length = values.length;
        if (0 == length) {
            return new boolean[0];
        }

        final boolean[] array = new boolean[length];
        for (int i = 0; i < length; i++) {
            array[i] = values[i];
        }
        return array;
    }

    /**
     * 将原始类型数组包装为包装类型
     * @param values 原始类型数组
     * @return 包装类型数组
     */
    public static Byte[] wrap(byte... values) {
        if (null == values) {
            return null;
        }
        final int length = values.length;
        if (0 == length) {
            return new Byte[0];
        }

        final Byte[] array = new Byte[length];
        for (int i = 0; i < length; i++) {
            array[i] = values[i];
        }
        return array;
    }

    /**
     * 包装类数组转为原始类型数组
     * @param values 包装类型数组
     * @return 原始类型数组
     */
    public static byte[] unWrap(Byte... values) {
        if (null == values) {
            return null;
        }
        final int length = values.length;
        if (0 == length) {
            return new byte[0];
        }

        final byte[] array = new byte[length];
        for (int i = 0; i < length; i++) {
            array[i] = values[i];
        }
        return array;
    }

    /**
     * 将原始类型数组包装为包装类型
     * @param values 原始类型数组
     * @return 包装类型数组
     */
    public static Character[] wrap(char... values) {
        if (null == values) {
            return null;
        }
        final int length = values.length;
        if (0 == length) {
            return new Character[0];
        }

        final Character[] array = new Character[length];
        for (int i = 0; i < length; i++) {
            array[i] = values[i];
        }
        return array;
    }

    /**
     * 包装类数组转为原始类型数组
     * @param values 包装类型数组
     * @return 原始类型数组
     */
    public static char[] unWrap(Character... values) {
        if (null == values) {
            return null;
        }
        final int length = values.length;
        if (0 == length) {
            return new char[0];
        }

        char[] array = new char[length];
        for (int i = 0; i < length; i++) {
            array[i] = values[i];
        }
        return array;
    }

    /**
     * 将原始类型数组包装为包装类型
     * @param values 原始类型数组
     * @return 包装类型数组
     */
    public static Short[] wrap(short... values) {
        if (null == values) {
            return null;
        }
        final int length = values.length;
        if (0 == length) {
            return new Short[0];
        }

        final Short[] array = new Short[length];
        for (int i = 0; i < length; i++) {
            array[i] = values[i];
        }
        return array;
    }

    /**
     * 包装类数组转为原始类型数组
     * @param values 包装类型数组
     * @return 原始类型数组
     */
    public static short[] unWrap(Short... values) {
        if (null == values) {
            return null;
        }
        final int length = values.length;
        if (0 == length) {
            return new short[0];
        }

        final short[] array = new short[length];
        for (int i = 0; i < length; i++) {
            array[i] = values[i];
        }
        return array;
    }

    /**
     * 将原始类型数组包装为包装类型
     * @param values 原始类型数组
     * @return 包装类型数组
     */
    public static Integer[] wrap(int... values) {
        if (null == values) {
            return null;
        }
        final int length = values.length;
        if (0 == length) {
            return new Integer[0];
        }

        final Integer[] array = new Integer[length];
        for (int i = 0; i < length; i++) {
            array[i] = values[i];
        }
        return array;
    }

    /**
     * 包装类数组转为原始类型数组
     *
     * @param values 包装类型数组
     * @return 原始类型数组
     */
    public static int[] unWrap(Integer... values) {
        if (null == values) {
            return null;
        }
        final int length = values.length;
        if (0 == length) {
            return new int[0];
        }

        final int[] array = new int[length];
        for (int i = 0; i < length; i++) {
            array[i] = values[i];
        }
        return array;
    }

    /**
     * 将原始类型数组包装为包装类型
     *
     * @param values 原始类型数组
     * @return 包装类型数组
     */
    public static Long[] wrap(long... values) {
        if (null == values) {
            return null;
        }
        final int length = values.length;
        if (0 == length) {
            return new Long[0];
        }

        final Long[] array = new Long[length];
        for (int i = 0; i < length; i++) {
            array[i] = values[i];
        }
        return array;
    }

    /**
     * 包装类数组转为原始类型数组
     *
     * @param values 包装类型数组
     * @return 原始类型数组
     */
    public static long[] unWrap(Long... values) {
        if (null == values) {
            return null;
        }
        final int length = values.length;
        if (0 == length) {
            return new long[0];
        }

        final long[] array = new long[length];
        for (int i = 0; i < length; i++) {
            array[i] = values[i];
        }
        return array;
    }

    /**
     * 将原始类型数组包装为包装类型
     * @param values 原始类型数组
     * @return 包装类型数组
     */
    public static Float[] wrap(float... values) {
        if (null == values) {
            return null;
        }
        final int length = values.length;
        if (0 == length) {
            return new Float[0];
        }

        final Float[] array = new Float[length];
        for (int i = 0; i < length; i++) {
            array[i] = values[i];
        }
        return array;
    }

    /**
     * 包装类数组转为原始类型数组
     * @param values 包装类型数组
     * @return 原始类型数组
     */
    public static float[] unWrap(Float... values) {
        if (null == values) {
            return null;
        }
        final int length = values.length;
        if (0 == length) {
            return new float[0];
        }

        final float[] array = new float[length];
        for (int i = 0; i < length; i++) {
            array[i] = values[i];
        }
        return array;
    }

    /**
     * 将原始类型数组包装为包装类型
     * @param values 原始类型数组
     * @return 包装类型数组
     */
    public static Double[] wrap(double... values) {
        if (null == values) {
            return null;
        }
        final int length = values.length;
        if (0 == length) {
            return new Double[0];
        }

        final Double[] array = new Double[length];
        for (int i = 0; i < length; i++) {
            array[i] = values[i];
        }
        return array;
    }

    /**
     * 包装类数组转为原始类型数组
     * @param values 包装类型数组
     * @return 原始类型数组
     */
    public static double[] unWrap(Double... values) {
        if (null == values) {
            return null;
        }
        final int length = values.length;
        if (0 == length) {
            return new double[0];
        }

        final double[] array = new double[length];
        for (int i = 0; i < length; i++) {
            array[i] = values[i];
        }
        return array;
    }

    /**
     * 包装数组对象
     *
     * @param obj 对象，可以是对象数组或者基本类型数组
     * @return 包装类型数组或对象数组
     * @throws UtilException 对象为非数组
     */
    public static Object[] wrap(Object obj) throws ClassCastException {
        if (null == obj) {
            return null;
        }
        Assert.isTrue(isArray(obj), "Class[{}] is not array!", obj.getClass());

        Object[] array;
        String className = obj.getClass().getComponentType().getName();
        switch (className) {
            case "boolean":
                array = wrap((boolean[]) obj);
                break;
            case "byte":
                array = wrap((byte[]) obj);
                break;
            case "char":
                array = wrap((char[]) obj);
                break;

            case "int":
                array = wrap((int[]) obj);
                break;
            case "short":
                array = wrap((short[]) obj);
                break;
            case "long":
                array = wrap((long[]) obj);
                break;
            case "float":
                array = wrap((float[]) obj);
                break;
            case "double":
                array = wrap((double[]) obj);
                break;
            default:
                array = (Object[]) obj;
        }
        return array;
    }

    //----------------------------------------------------check empty

	public static <T> boolean isEmpty(T[] arrays){
		return arrays == null || arrays.length == 0;
	}

	public static <T> boolean isNotEmpty(T[] arrays){
		return arrays != null && arrays.length > 0;
	}
	
	public static boolean isEmpty(final long... array) {
		return array == null || array.length == 0;
	}

    public static boolean isNotEmpty(final long... array) {
        return array != null && array.length > 0;
    }
	
	public static boolean isEmpty(final int... array) {
		return array == null || array.length == 0;
	}

    public static boolean isNotEmpty(final int... array) {
        return array != null && array.length > 0;
    }
	
	public static boolean isEmpty(final short... array) {
		return array == null || array.length == 0;
	}

    public static boolean isNotEmpty(final short... array) {
        return array != null && array.length > 0;
    }
	
	public static boolean isEmpty(final char... array) {
		return array == null || array.length == 0;
	}

    public static boolean isNotEmpty(final char... array) {
        return array != null && array.length > 0;
    }
	
	public static boolean isEmpty(final byte... array) {
		return array == null || array.length == 0;
	}

    public static boolean isNotEmpty(final byte... array) {
        return array != null && array.length > 0;
    }
	
	public static boolean isEmpty(final double... array) {
		return array == null || array.length == 0;
	}

    public static boolean isNotEmpty(final double... array) {
        return array != null && array.length > 0;
    }
	
	public static boolean isEmpty(final float... array) {
		return array == null || array.length == 0;
	}

    public static boolean isNotEmpty(final float... array) {
        return array != null && array.length > 0;
    }
	
	public static boolean isEmpty(final boolean... array) {
		return array == null || array.length == 0;
	}

    public static boolean isNotEmpty(final boolean... array) {
        return array != null && array.length > 0;
    }



}

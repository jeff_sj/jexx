package jexx.util;

/**
 * 布尔工具类
 */
public class BooleanUtil {

    public static Boolean negate(final Boolean bool) {
        if (bool == null) {
            return null;
        }
        return bool ? Boolean.FALSE : Boolean.TRUE;
    }

    public static boolean isTrue(final Boolean bool) {
        return Boolean.TRUE.equals(bool);
    }

    public static boolean isNotTrue(final Boolean bool) {
        return !isTrue(bool);
    }

    /**
     * 是否为false, null值也默认为false
     * @param bool bool
     * @return 是否为false
     */
    public static boolean isFalse(final Boolean bool) {
        return bool == null || Boolean.FALSE.equals(bool);
    }

    public static boolean isNotFalse(final Boolean bool) {
        return !isFalse(bool);
    }

    /**
     * 转换为bool
     * <pre>
     *     toBoolean(null)=false
     *     toBoolean(Boolean.TRUE)=true
     *     toBoolean(Boolean.FALSE)=false
     * </pre>
     * @param bool
     */
    public static boolean toBoolean(final Boolean bool) {
        return bool != null && bool;
    }

    /**
     * <p>转换为bool,如果未null,则使用默认值</p>
     * <pre>
     *     toBooleanDefaultIfNull(null, true)=true
     * </pre>
     */
    public static boolean toBooleanDefaultIfNull(final Boolean bool, final boolean valueIfNull) {
        if (bool == null) {
            return valueIfNull;
        }
        return bool;
    }


}

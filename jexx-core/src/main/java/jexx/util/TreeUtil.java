package jexx.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 构建树结构的工具类
 * @author jeff
 * @since 2020/1/22
 */
public class TreeUtil {

    /**
     * 集合对象构建树结构
     * @param list 集合
     * @param mapper 树结构转换类
     * @param <T> 集合对象类型
     * @param <R> 结合对象树结构关联字段类型
     * @return 具有树结构的集合
     */
    public static <T,R> List<T> buildTree(List<T> list, TreeMapper<T,R> mapper){
        Map<R,T> map = new HashMap<>(list.size());
        for(T t : list){
            R id = mapper.getId(t);
            if(id == null){
                continue;
            }
            map.put(id, t);
        }
        for(T t : list){
            R parentId = mapper.getParentId(t);
            if(parentId == null){
                continue;
            }
            T parent = map.get(parentId);
            if(parent == null){
                continue;
            }
            mapper.addChild(parent, t);
        }
        return list.stream().filter(mapper::isRoot).collect(Collectors.toList());
    }

    /**
     * 树转换类
     */
    public interface TreeMapper<T, R>{
        /**
         * id
         */
        R getId(T t);

        /**
         * parentId
         */
        R getParentId(T t);

        /**
         * 添加孩子
         */
        void addChild(T parent, T child);

        /**
         * 是否第一层
         */
        boolean isRoot(T t);
    }


}

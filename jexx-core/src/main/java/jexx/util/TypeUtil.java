package jexx.util;

import java.lang.reflect.*;

/**
 * 针对 {@link Type} 的工具类封装；
 * type 为类型描述类型，常见具体对象为 class(普通class),ParameterizedType(集合泛型),GenericArrayType(数组泛型)
 * 最主要功能包括：
 * <pre>
 * 1. 获取方法的参数和返回值类型（包括Type和Class）
 * 2. 获取泛型参数类型（包括对象的泛型参数或集合元素的泛型类型）
 * </pre>
 */
public class TypeUtil {

	/**
	 * 获得Type对应的原始类
	 * @param type {@link Type}
	 * @return 原始类，如果无法获取原始类，返回null
	 */
	public static Class<?> getClass(Type type) {
		if (null != type) {
			if (type instanceof Class) {
				return (Class<?>) type;
			} else if (type instanceof ParameterizedType) {
				return (Class<?>) ((ParameterizedType) type).getRawType();
			}
		}
		return null;
	}

	/**
	 * 获取字段对应的Type类型
	 * @param field 字段
	 * @return 字段type，可能为原始类，或者类定义ParameterizedType
	 */
	public static Type getType(Field field) {
		if (null == field) {
			return null;
		}
		return field.getGenericType();
	}

	/**
	 * 获得Field对应的原始类
	 * @param field {@link Field}
	 * @return 原始类，如果无法获取原始类，返回{@code null}
	 */
	public static Class<?> getClass(Field field) {
		Type type = getType(field);
		return getClass(type);
	}

	/**
	 * 判断type的原始类型是否为数组
	 */
	public static boolean isArray(Type type){
		if(type instanceof Class){
			return ((Class<?>) type).isArray();
		}
		return type instanceof GenericArrayType;
	}

	/**
	 * 判断字符原始类型是否为数组
	 */
	public static boolean isArray(Field field){
		return isArray(getType(field));
	}

	// ----------------------------------------------------------------------------------- Param Type
	/**
	 * 获取方法的第一个参数类型<br>
	 * 优先获取方法的GenericParameterTypes，如果获取不到，则获取ParameterTypes
	 * 
	 * @param method 方法
	 * @return {@link Type}，可能为{@code null}
	 */
	public static Type getFirstParamType(Method method) {
		return getParamType(method, 0);
	}

	/**
	 * 获取方法的第一个参数类
	 * 
	 * @param method 方法
	 * @return 第一个参数类型，可能为{@code null}
	 */
	public static Class<?> getFirstParamClass(Method method) {
		return getParamClass(method, 0);
	}

	/**
	 * 获取方法的参数类型<br>
	 * 优先获取方法的GenericParameterTypes，如果获取不到，则获取ParameterTypes
	 * 
	 * @param method 方法
	 * @param index 第几个参数的索引，从0开始计数
	 * @return {@link Type}，可能为{@code null}
	 */
	public static Type getParamType(Method method, int index) {
		Type[] types = getParamTypes(method);
		if (null != types && types.length > index) {
			return types[index];
		}
		return null;
	}

	/**
	 * 获取方法的参数类
	 * 
	 * @param method 方法
	 * @param index 第几个参数的索引，从0开始计数
	 * @return 参数类，可能为{@code null}
	 */
	public static Class<?> getParamClass(Method method, int index) {
		Class<?>[] classes = getParamClasses(method);
		if (null != classes && classes.length > index) {
			return classes[index];
		}
		return null;
	}

	/**
	 * 获取方法的参数类型列表<br>
	 * 优先获取方法的GenericParameterTypes，如果获取不到，则获取ParameterTypes
	 * 
	 * @param method 方法
	 * @return {@link Type}列表，可能为{@code null}
	 * @see Method#getGenericParameterTypes()
	 * @see Method#getParameterTypes()
	 */
	public static Type[] getParamTypes(Method method) {
		return null == method ? null : method.getGenericParameterTypes();
	}

	/**
	 * 解析方法的参数类型列表<br>
	 * 依赖jre\lib\rt.jar
	 *
	 * @param method t方法
	 * @return 参数类型类列表
	 *
	 * @see Method#getGenericParameterTypes
	 * @see Method#getParameterTypes
	 */
	public static Class<?>[] getParamClasses(Method method) {
		return null == method ? null : method.getParameterTypes();
	}

	// ----------------------------------------------------------------------------------- Return Type
	/**
	 * 获取方法的返回值类型<br>
	 * 获取方法的GenericReturnType
	 * 
	 * @param method 方法
	 * @return {@link Type}，可能为{@code null}
	 * @see Method#getGenericReturnType()
	 * @see Method#getReturnType()
	 */
	public static Type getReturnType(Method method) {
		return null == method ? null : method.getGenericReturnType();
	}

	/**
	 * 解析方法的返回类型类列表
	 *
	 * @param method 方法
	 * @return 返回值类型的类
	 * @see Method#getGenericReturnType
	 * @see Method#getReturnType
	 */
	public static Class<?> getReturnClass(Method method) {
		return null == method ? null : method.getReturnType();
	}

	// ----------------------------------------------------------------------------------- Type Argument
	/**
	 * 获得给定类的第一个泛型参数
	 * @param clazz 被检查的类，必须是已经确定泛型类型的类
	 * @return 指定泛型类型
	 */
	public static Type getTypeArgument(Class<?> clazz) {
		return getTypeArgument(clazz, 0);
	}

	/**
	 * 获得给定类的泛型参数
	 * @param clazz 被检查的类，必须是已经确定泛型类型的类
	 * @param index 泛型类型的索引号，既第几个泛型类型
	 * @return 指定泛型类型
	 */
	public static Type getTypeArgument(Class<?> clazz, int index) {
		return getTypeArgument((Type)clazz, index);
	}

	/**
	 * 获得给定类的第一个泛型参数
	 * @param type 被检查的类型，必须是已经确定泛型类型的类型
	 * @return 指定泛型类型
	 */
	public static Type getTypeArgument(Type type) {
		return getTypeArgument(type, 0);
	}

	/**
	 * 获得指定类型中所有泛型参数的描述类型,非泛型抛出异常
	 * @param type 指定类型
	 * @return 所有泛型参数类型
	 */
	public static Type[] getTypeArguments(Type type) {
		if(!(type instanceof ParameterizedType)){
			return null;
		}
		ParameterizedType parameterizedType = (ParameterizedType) type;
		return parameterizedType.getActualTypeArguments();
	}

	/**
	 * 获得给定类的泛型参数
	 * @param type 被检查的类型，必须是已经确定泛型类型的类
	 * @param index 泛型类型的索引号，既第几个泛型类型
	 * @return 指定type对象对应索引的泛型类型
	 */
	public static Type getTypeArgument(Type type, int index) {
		Type[] typeArguments = getTypeArguments(type);
		return typeArguments != null && typeArguments.length > index ? typeArguments[index] : null;
	}

	public static Type getTypeArgument(Field field, int index) {
		Type type = getType(field);
		if(type != null){
			return getTypeArgument(type, index);
		}
		return null;
	}

	/**
	 * 获取字段对应的第一个泛型
	 * @param field 指定字段
	 * @return 指定类型的第一个泛型
	 */
	public static Type getTypeArgument(Field field) {
		return getTypeArgument(field, 0);
	}

	/**
	 * 获取指定描述类型的泛型原始类
	 * @param type 描述类型
	 * @param index 指定泛型的索引
	 * @return 泛型类型的原始类
	 */
	public static Class<?> getClassArgument(Type type, int index) {
		Type argumentType = getTypeArgument(type, index);
		if(argumentType != null){
			return getClass(argumentType);
		}
		return null;
	}

	/**
	 * 获取指定描述类型的第一个泛型原始类
	 * @param type 描述类型
	 * @return 泛型类型的原始类
	 */
	public static Class<?> getClassArgument(Type type) {
		return getClassArgument(type, 0);
	}

	/**
	 * 获取指定泛型类型的字段的，指定索引的泛型原始类
	 * @param field 指定字段
	 * @param index 索引
	 * @return 泛型原始类
	 */
	public static Class<?> getClassArgument(Field field, int index) {
		Type type = getType(field);
		return getClassArgument(type, index);
	}

	/**
	 * 获取指定泛型类型的字段的第一个泛型原始类
	 * @param field 指定字段
	 * @return 泛型原始类
	 */
	public static Class<?> getClassArgument(Field field) {
		return getClassArgument(field, 0);
	}

	/**
	 * 获取指定类的直接父类的第一个泛型原始类
	 * @param clazz 指定类型
	 * @return 直接父类泛型类型的原始类
	 */
	public static Class<?> getSuperClassArgument(Class<?> clazz) {
		if(clazz == null){
			return null;
		}
		Type type = clazz.getGenericSuperclass();
		return getClassArgument(type);
	}

	// ----------------------------------------------------------------------------------- Type Component

	/**
	 * 获取指定类型的数组类型，非数组抛出异常
	 * @param type 指定类型
	 * @return 数组类型, 可能为 class,ParameterizedType,GenericArrayType
	 */
	public static Type getComponentType(Type type){
		if(type instanceof Class){
			Class<?> clazz = ((Class<?>) type);
			if(clazz.isArray()){
				return clazz.getComponentType();
			}
		}
		else if(type instanceof GenericArrayType){
			return ((GenericArrayType) type).getGenericComponentType();
		}
		return null;
	}

	/**
	 * 获取指定类型的数组对象原始类，非数组抛出异常
	 * @param type 指定类型
	 * @return 数组对象原始类
	 */
	public static Class<?> getComponentClass(Type type){
		Type componentType = getComponentType(type);
		return getClass(componentType);
	}

	/**
	 * 获取指定字段的数组对象原始类，非数组抛出异常
	 * @param field 指定字段
	 * @return 数组对象类型, 可能为 class,ParameterizedType,GenericArrayType
	 */
	public static Type getComponentType(Field field){
		Type type = getType(field);
		return getComponentType(type);
	}

	/**
	 * 获取指定字段的数组对象原始类，非数组抛出异常
	 * @param field 指定字段
	 * @return 数组对象原始类
	 */
	public static Class<?> getComponentClass(Field field){
		Type componentType = getComponentType(field);
		return getClass(componentType);
	}

}

package jexx.util;

import java.util.Collection;
import java.util.Optional;

/**
 * {@link Iterable} 工具类
 * @author jeff
 * @since 2019/10/23
 */
public class IterableUtil {

    /**
     * 找到任意一个对象
     * @param list 集合
     * @return 可选对象
     */
    public static <T> Optional<T> findAny(Iterable<T> list){
        return IteratorUtil.findAny(list != null ? list.iterator() : null);
    }

    public static boolean isEmpty(Iterable<?> iterable) {
        if (iterable instanceof Collection) {
            return ((Collection<?>) iterable).isEmpty();
        }
        return null == iterable || !iterable.iterator().hasNext();
    }

    public static boolean isNotEmpty(Iterable<?> iterator) {
        return !isEmpty(iterator);
    }

}

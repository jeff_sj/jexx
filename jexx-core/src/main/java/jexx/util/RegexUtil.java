package jexx.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 正则工具类
 * @author jeff
 * @since 2019/9/23
 */
public class RegexUtil {

    /**
     * 英文字母，数字和下划线
     * @param input 输入
     * @return bool
     */
    public static boolean isWord(CharSequence input){
        return matches(RegexPool.WORD, input);
    }

    /**
     * 字母
     * @param input 输入
     * @return bool
     */
    public static boolean isLetter(CharSequence input){
        return matches(RegexPool.LETTER, input);
    }

    /**
     * 是否为email
     * @param input 输入
     * @return bool
     */
    public static boolean isEmail(CharSequence input){
        return matches(RegexPool.EMAIL, input);
    }

    /**
     * 是否为email
     * @param input 输入
     * @return bool
     */
    public static boolean isIp(CharSequence input){
        return matches(RegexPool.IP, input);
    }

    /**
     * 基本汉字匹配
     * @param input 输入
     * @return bool
     */
    public static boolean isChinese(CharSequence input){
        return matches(RegexPool.CHINESE, input);
    }

    /**
     * 是否为中国大陆手机号码
     * @param input 输入
     * @return bool
     */
    public static boolean isMobilePhoneInChinese(CharSequence input){
        return matches(RegexPool.CHINESE_MOBILE_PHONE, input);
    }

    /**
     * 是否为中国大陆18位身份证号码
     * @param input 输入
     * @return bool
     */
    public static boolean isIdCard18InChinese(CharSequence input){
        return matches(RegexPool.CHINESE_ID_CARD, input);
    }

    public static boolean matches(Pattern pattern, CharSequence input){
        Matcher matcher = pattern.matcher(input);
        return matcher.matches();
    }

    public static boolean matches(String regex, CharSequence input){
        return Pattern.matches(regex, input);
    }

}

package jexx.util;

import java.util.*;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * 集合工具类
 * @author jeff
 */
public class CollectionUtil {

    public static <T> boolean isEmpty(T[] array){
        return array == null || array.length == 0;
    }

    public static boolean isEmpty(Collection<?> collection) {
        return (collection == null || collection.isEmpty());
    }

    public static boolean isNotEmpty(Collection<?> collection) {
        return !isEmpty(collection);
    }

    public static boolean isEmpty(Iterable<?> iterable) {
        return IterableUtil.isEmpty(iterable);
    }

    public static boolean isNotEmpty(Iterable<?> iterable) {
        return !isEmpty(iterable);
    }

    public static boolean isEmpty(Iterator<?> iterator) {
        return IteratorUtil.isEmpty(iterator);
    }

    public static boolean isNotEmpty(Iterator<?> iterator) {
        return !isEmpty(iterator);
    }

    public static boolean isEmpty(Map<?, ?> map) {
        return (map == null || map.isEmpty());
    }

    public static boolean isNotEmpty(Map<?, ?> map) {
        return !isEmpty(map);
    }

    public static boolean isEmpty(Enumeration<?> enumeration) {
        return null == enumeration || !enumeration.hasMoreElements();
    }

    public static boolean isNotEmpty(Enumeration<?> enumeration) {
        return !isEmpty(enumeration);
    }


    // ---------------------------------------------------------------- compare

    /**
     * 在指定长度内比较元素是否相等
     *
     * @param first 第一个集合
     * @param second 第二集合
     * @param length 指定长度
     * @return 指定长度内元素是否相等
     */
    public static <T> boolean equals(List<T> first, List<T> second, int length){
        if(first == null && second == null){
            return true;
        }
        else if(first == null || second == null){
            return false;
        }
        int firstLength = first.size();
        int secondLength = second.size();
        if(length > firstLength || length > secondLength){
            throw new IllegalArgumentException("length is too long");
        }
        T a;
        T b;
        for(int i = 0; i < length; i++){
            a = first.get(i);
            b = second.get(i);
            if(a == null && b == null){
                //skip
            }
            else if(a == null || b == null){
                return false;
            }
            else{
                if(!a.equals(b)){
                    return false;
                }
            }
        }
        return true;
    }


    // ---------------------------------------------------------------- list

    /**
     * 新建list
     * @param enumration {@link Enumeration}
     * @param <T> 泛型
     * @return ArrayList
     */
    public static <T> List<T> list(Enumeration<T> enumration){
        final List<T> list = new ArrayList<>();
        if (null != enumration) {
            while (enumration.hasMoreElements()) {
                list.add(enumration.nextElement());
            }
        }
        return list;
    }

    /**
     * 新建list
     * @param iter {@link Iterator}
     * @param <T> 泛型
     * @return ArrayList
     */
    public static <T> List<T> list(Iterator<T> iter) {
        final List<T> list = new ArrayList<>();
        if (null != iter) {
            while (iter.hasNext()) {
                list.add(iter.next());
            }
        }
        return list;
    }

    @SafeVarargs
    public static <T> List<T> list(T... values) {
        if (ArrayUtil.isEmpty(values)) {
            return new ArrayList<>();
        }
        List<T> arrayList = new ArrayList<>(values.length);
        Collections.addAll(arrayList, values);
        return arrayList;
    }

    // ---------------------------------------------------------------- parse


    // ---------------------------------------------------------------- toList

    public static List<Byte> toList(byte[] values){
        if (ArrayUtil.isEmpty(values)) {
            return new ArrayList<>();
        }
        List<Byte> list = new ArrayList<>(values.length);
        for(byte v : values){
            list.add(v);
        }
        return list;
    }

    public static List<Character> toList(char[] values){
        if (ArrayUtil.isEmpty(values)) {
            return new ArrayList<>();
        }
        List<Character> list = new ArrayList<>(values.length);
        for(char v : values){
            list.add(v);
        }
        return list;
    }

    public static List<Short> toList(short[] values){
        if (ArrayUtil.isEmpty(values)) {
            return new ArrayList<>();
        }
        List<Short> list = new ArrayList<>(values.length);
        for(short v : values){
            list.add(v);
        }
        return list;
    }

    public static List<Integer> toList(int[] values){
        if (ArrayUtil.isEmpty(values)) {
            return new ArrayList<>();
        }
        List<Integer> list = new ArrayList<>(values.length);
        for(int v : values){
            list.add(v);
        }
        return list;
    }

    public static List<Long> toList(long[] values){
        if (ArrayUtil.isEmpty(values)) {
            return new ArrayList<>();
        }
        List<Long> list = new ArrayList<>(values.length);
        for(long v : values){
            list.add(v);
        }
        return list;
    }

    public static List<Float> toList(float[] values){
        if (ArrayUtil.isEmpty(values)) {
            return new ArrayList<>();
        }
        List<Float> list = new ArrayList<>(values.length);
        for(float v : values){
            list.add(v);
        }
        return list;
    }

    public static List<Double> toList(double[] values){
        if (ArrayUtil.isEmpty(values)) {
            return new ArrayList<>();
        }
        List<Double> list = new ArrayList<>(values.length);
        for(double v : values){
            list.add(v);
        }
        return list;
    }

    public static List<Boolean> toList(boolean[] values){
        if (ArrayUtil.isEmpty(values)) {
            return new ArrayList<>();
        }
        List<Boolean> list = new ArrayList<>(values.length);
        for(boolean v : values){
            list.add(v);
        }
        return list;
    }

    public static <T> List<T> toList(T[] values){
        if (ArrayUtil.isEmpty(values)) {
            return new ArrayList<>();
        }
        List<T> list = new ArrayList<>(values.length);
        for(T v : values){
            list.add(v);
        }
        return list;
    }



    /**
     * 如果集合为null则返回空的{@link ArrayList}，否则返回自身
     * @param list 检查对象
     * @param <T> 泛型
     * @return 集合
     */
    public static <T> List<T> wrapEmptyIfNull(List<T> list){
        return list == null ? new ArrayList<>() : list;
    }

    /**
     * 如果map为null，则返回空的{@link HashMap}，否则返回自身
     * @param map 检查对象
     * @param <K> 泛型
     * @param <V> 泛型
     * @return map
     */
    public static <K,V> Map<K,V> wrapEmptyIfNull(Map<K,V> map){
        return map == null ? new HashMap<>() : map;
    }

    // ----------------------------------------------------------------create

    /**
     * 创建集合
     * @param collectionType 集合类型
     * @param capacity 容量
     * @param <T> 泛型
     * @return 集合
     */
    public static <T> Collection<T> createCollection(Class<?> collectionType, int capacity) {
        return createCollection(collectionType, null, capacity);
    }

    /**
     * 创建新的集合对象
     *
     * @param <T> 集合类型
     * @param collectionType 集合类型
     * @return 集合类型对应的实例
     */
    @SuppressWarnings({ "unchecked"})
    public static <T> Collection<T> createCollection(Class<?> collectionType, Class<?> elementType, int capacity) {
        Assert.notNull(collectionType, "Collection type must not be null");

        if(collectionType.isInterface()){
            if (Set.class == collectionType || Collection.class == collectionType) {
                return new LinkedHashSet<>(capacity);
            }
            else if (List.class == collectionType) {
                return new ArrayList<>(capacity);
            }
            else if (SortedSet.class == collectionType || NavigableSet.class == collectionType) {
                return new TreeSet<>();
            }
            else {
                throw new IllegalArgumentException("Unsupported Collection interface: " + collectionType.getName());
            }
        }
        else if (EnumSet.class == collectionType) {
            Assert.notNull(elementType, "Cannot create EnumSet for unknown element type");
            if (!Enum.class.isAssignableFrom(collectionType)) {
                throw new IllegalArgumentException("Supplied type is not an enum: " + collectionType.getName());
            }
            // Cast is necessary for compilation in Eclipse 4.4.1.
            return (Collection<T>) EnumSet.noneOf(collectionType.asSubclass(Enum.class));
        }
        else {
            if (!Collection.class.isAssignableFrom(collectionType)) {
                throw new IllegalArgumentException("Unsupported Collection type: " + collectionType.getName());
            }
            try {
                return (Collection<T>) ReflectUtil.newInstance(collectionType);
            }
            catch (Throwable ex) {
                throw new IllegalArgumentException("Could not instantiate Collection type: " + collectionType.getName(), ex);
            }
        }
    }

    // ---------------------------------------------------------------- check

    /**
     * 判断两个集合是否不相交
     * @param left 左集合
     * @param right 右集合
     * @param <T> 泛型
     * @return 是否不相交
     */
    public static <T> boolean checkDisjoint(final Collection<T> left, final Collection<T> right){
        return Collections.disjoint(left, right);
    }

    /**
     * 判断两个集合是否相交
     * @param left 左集合
     * @param right 右集合
     * @param <T> 泛型
     * @return 是否相交
     */
    public static <T> boolean checkJoint(final Collection<T> left, final Collection<T> right){
        return !checkDisjoint(left, right);
    }


    // ---------------------------------------------------------------- find

    /**
     * 找到任意一个对象
     * @param list 集合
     * @return 可选对象
     */
    public static <T> Optional<T> findAny(Iterator<T> list){
        return IteratorUtil.findAny(list);
    }

    /**
     * 找到任意一个对象
     * @param list 集合
     * @return 可选对象
     */
    public static <T> Optional<T> findAny(Iterable<T> list){
        return findAny(list != null ? list.iterator() : null);
    }

    /**
     * 找到两个集合中交集,重复元素合并
     * @param left 左集合
     * @param right 右集合
     * @param <T> 泛型
     * @return 两个集合中相同元素
     */
    public static <T> Set<T> findSame(final Collection<T> left, final Collection<T> right){
        if(isEmpty(left) || isEmpty(right)){
            return new HashSet<>();
        }
        Set<T> set = new HashSet<>(left);
        set.retainAll(right);
        return set;
    }

    /**
     * 找到左边差集
     * @param left 左集合
     * @param right 右集合
     * @param <T> 泛型
     * @return 左边差集
     */
    public static <T> Set<T> findLeftDiff(final Collection<T> left, final Collection<T> right){
        if(isEmpty(left)){
            return new HashSet<>();
        }
        Set<T> set = new HashSet<>(left);
        if(right != null){
            set.removeAll(right);
        }
        return set;
    }

    /**
     * 通过转换获取同一类型元素后并比较，找到左边的差集
     * @param left 左集合
     * @param leftTransform 左集合转换
     * @param right 右集合
     * @param rightTransform 右集合转换
     * @param <M> 左集合泛型
     * @param <N> 右集合泛型
     * @param <T> 同一元素类型
     * @return 返回左边差集
     */
    public static <M,N,T> Set<M> findLeftDiff(final Collection<M> left, Function<M,T> leftTransform, final Collection<N> right, Function<N,T> rightTransform){
        if(isEmpty(left)){
            return new HashSet<>();
        }
        Set<M> set = new HashSet<>(left);
        if(CollectionUtil.isNotEmpty(right)){
            List<T> rightIds = right.stream().map(rightTransform).collect(Collectors.toList());
            set = set.stream().filter(s -> !rightIds.contains(leftTransform.apply(s))).collect(Collectors.toSet());
        }
        return set;
    }

    /**
     * 通过转换获取同一类型元素后并比较，找到左边的差集
     * @param left 左集合
     * @param right 右集合
     * @param transform 统一集合转换
     * @param <M> 集合泛型
     * @param <T> 同一元素类型
     * @return 返回左边差集
     */
    public static <M,T> Set<M> findLeftDiff(final Collection<M> left, final Collection<M> right, Function<M,T> transform){
        return findLeftDiff(left, transform, right, transform);
    }

    /**
     * 通过转换获取同一类型元素后并比较，找到左边相同
     * @param left 左集合
     * @param leftExtract 左集合对比元素抽取
     * @param right 右集合
     * @param rightExtract 右集合对比元素抽取
     * @param leftTransform 左集合元素转换, 常用于左边元素根据右边对应元素来更新； 建议 返回对象 重新创建，尽量不要使用左对象元素
     * @param <L> 左集合泛型
     * @param <R> 右集合泛型
     * @param <T> 同一元素类型
     * @return 返回左边差集
     */
    public static <L,R,T> Set<L> findLeftSame(final Collection<L> left, Function<L,T> leftExtract, final Collection<R> right, Function<R,T> rightExtract,
                                               BiFunction<L, R, L> leftTransform){
        if(isEmpty(left) || isEmpty(right)){
            return new HashSet<>();
        }
        Set<L> set = new HashSet<>(left);
        if(CollectionUtil.isNotEmpty(right)){
            Map<T, R> rightMap = right.stream().collect(Collectors.toMap(rightExtract, s->s));
            set = set.stream().filter(s -> rightMap.containsKey(leftExtract.apply(s))).map(s -> {
                T leftId = leftExtract.apply(s);
                R r = rightMap.get(leftId);
                return leftTransform != null ? leftTransform.apply(s, r) : s;
            }).collect(Collectors.toSet());
        }
        return set;
    }

    public static <L,R,T> Set<L> findLeftSame(final Collection<L> left, Function<L,T> leftExtract, final Collection<R> right, Function<R,T> rightExtract){
        return findLeftSame(left, leftExtract, right, rightExtract, null);
    }

    /**
     * 通过转换获取同一类型元素后并比较，找到左边相同
     * @param left 左集合
     * @param right 右集合
     * @param transform 统一集合转换
     * @param <M> 集合泛型
     * @param <T> 同一元素类型
     * @return 返回左边差集
     */
    public static <M,T> Set<M> findLeftSame(final Collection<M> left, final Collection<M> right, Function<M,T> transform){
        return findLeftSame(left, transform, right, transform);
    }

    /**
     * 找到差集
     * @param left 左集合
     * @param right 右集合
     * @param <T> 泛型
     * @return 差集
     */
    public static <T> Set<T> findDiff(final Collection<T> left, final Collection<T> right){
        Set<T> leftDiff = findLeftDiff(left, right);
        Set<T> rightDiff = findLeftDiff(right, left);
        leftDiff.addAll(rightDiff);
        return leftDiff;
    }

    /**
     * 找到并集
     * @param left 左集合
     * @param right 右集合
     * @param <T> 泛型
     * @return 并集
     */
    public static <T> Set<T> findUnion(final Collection<T> left, final Collection<T> right){
        Set<T> set = new HashSet<>(left);
        set.addAll(right);
        return set;
    }

    // ---------------------------------------------------------------- 笛卡尔乘积

    /**
     * 对二维集合计算笛卡尔乘积
     * @param listList 二维集合
     * @param <T> 泛型
     * @return 笛卡尔乘积
     */
    public static <T> List<List<T>> dikaer(List<List<T>> listList){
        if(isEmpty(listList)){
            return listList;
        }
        int num = listList.stream().mapToInt(List::size).reduce(1, (a, b) -> a * b);
        if(num <= 0){
            throw new IllegalArgumentException("listList cannot contain one element with zero size!");
        }

        List<List<T>> dikaerList = new ArrayList<>();
        for (int i = 0; i < num; i++) {
            List<T> el = new ArrayList<>();
            int j = 1;
            for (List<T> list : listList){
                j = j * list.size();

                T t = list.get(i / (num / j ) % list.size());
                el.add(t);
            }
            dikaerList.add(el);
        }
        return dikaerList;
    }

}

package jexx.util;

import java.util.function.Function;

/**
 * 枚举工具类
 * @author jeff
 * @since 2020/9/19
 */
public class EnumUtil {


    public static <T extends Enum<T>> T getByName(Class<T> clazz, String name){
        T[] arr = clazz.getEnumConstants();
        return getByName(arr, name);
    }

    public static <T extends Enum<T>> T getByName(T[] arr, String name){
        if(StringUtil.isEmpty(name) || ArrayUtil.isEmpty(arr)){
            return null;
        }
        for(T t : arr){
            if(name.equals(t.name())){
                return t;
            }
        }
        return null;
    }

    public static <T extends Enum<T>> T getByLambda(Class<T> clazz, Function<T, Boolean> function){
        T[] arr = clazz.getEnumConstants();
        return getByLambda(arr, function);
    }

    public static <T extends Enum<T>> T getByLambda(T[] arr, Function<T, Boolean> function){
        if(function == null){
            throw new IllegalArgumentException("function is not null");
        }
        if(ArrayUtil.isEmpty(arr)){
            return null;
        }
        for(T t : arr){
            Boolean success = function.apply(t);
            if(success != null && success){
                return t;
            }
        }
        return null;
    }

}

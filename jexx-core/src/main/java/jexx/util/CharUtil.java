package jexx.util;

/**
 * 字符工具
 */
public class CharUtil {

    /**
     * 汉字unicode范围
     */
    public static final char[] CHINESE_UNICODE_RANGE = new char[]{(char)0x4E00, (char)0x9FA5};


    // ---------------------------------------------------------------- find

    /**
     * 字符是否匹配在给定的字符集中能匹配
     */
    public static boolean equalsOne(final char c, final char[] match) {
        for (char aMatch : match) {
            if (c == aMatch) {
                return true;
            }
        }
        return false;
    }

    /**
     * 找到第一个匹配的字符索引
     */
    public static int findFirstEqual(final char[] source, final int index, final char[] match) {
        for (int i = index; i < source.length; i++) {
            if (equalsOne(source[i], match)) {
                return i;
            }
        }
        return -1;
    }

    /**
     * 找到第一个匹配的字符索引
     */
    public static int findFirstEqual(final char[] source, final int index, final char match) {
        for (int i = index; i < source.length; i++) {
            if (source[i] == match) {
                return i;
            }
        }
        return -1;
    }


    /**
     * 找到第一个不匹配的字符索引
     */
    public static int findFirstDiff(final char[] source, final int index, final char[] match) {
        for (int i = index; i < source.length; i++) {
            if (!equalsOne(source[i], match)) {
                return i;
            }
        }
        return -1;
    }

    /**
     * 找到第一个不匹配的字符索引
     */
    public static int findFirstDiff(final char[] source, final int index, final char match) {
        for (int i = index; i < source.length; i++) {
            if (source[i] != match) {
                return i;
            }
        }
        return -1;
    }

    /**
     * 是否为ASCII字符，ASCII字符位于0~127之间
     *
     * <pre>
     *   CharUtil.isAscii('a')  = true
     *   CharUtil.isAscii('A')  = true
     *   CharUtil.isAscii('3')  = true
     *   CharUtil.isAscii('-')  = true
     *   CharUtil.isAscii('\n') = true
     *   CharUtil.isAscii('&copy;') = false
     * </pre>
     *
     * @param ch 被检查的字符处
     * @return true表示为ASCII字符，ASCII字符位于0~127之间
     */
    public static boolean isAscii(char ch) {
        return ch < 128;
    }

    /**
     * 是否为可见ASCII字符，可见字符位于32~126之间
     *
     * <pre>
     *   CharUtil.isAsciiPrintable('a')  = true
     *   CharUtil.isAsciiPrintable('A')  = true
     *   CharUtil.isAsciiPrintable('3')  = true
     *   CharUtil.isAsciiPrintable('-')  = true
     *   CharUtil.isAsciiPrintable('\n') = false
     *   CharUtil.isAsciiPrintable('&copy;') = false
     * </pre>
     *
     * @param ch 被检查的字符处
     * @return true表示为ASCII可见字符，可见字符位于32~126之间
     */
    public static boolean isAsciiPrintable(char ch) {
        return ch >= 32 && ch < 127;
    }

    /**
     * 是否为数字
     * @param c 被检查的字符处
     */
    public static boolean isDigit(final char c) {
        return c >= '0' && c <= '9';
    }

    /**
     * 是否为字母
     * @param ch 被检查的字符
     * @return true表示为字母
     */
    public static boolean isAlpha(char ch) {
        return isAlphaLower(ch) || isAlphaUpper(ch);
    }

    /**
     * <p>
     * 判断是否为大写字母，大写字母包括A~Z
     * </p>
     *
     * <pre>
     *   CharUtil.isLetterUpper('a')  = false
     *   CharUtil.isLetterUpper('A')  = true
     *   CharUtil.isLetterUpper('3')  = false
     *   CharUtil.isLetterUpper('-')  = false
     *   CharUtil.isLetterUpper('\n') = false
     *   CharUtil.isLetterUpper('&copy;') = false
     * </pre>
     *
     * @param ch 被检查的字符
     * @return true表示为大写字母，大写字母包括A~Z
     */
    public static boolean isAlphaUpper(final char ch) {
        return ch >= 'A' && ch <= 'Z';
    }

    /**
     * <p>
     * 检查字符是否为小写字母，小写字母指a~z
     * </p>
     *
     * <pre>
     *   CharUtil.isLetterLower('a')  = true
     *   CharUtil.isLetterLower('A')  = false
     *   CharUtil.isLetterLower('3')  = false
     *   CharUtil.isLetterLower('-')  = false
     *   CharUtil.isLetterLower('\n') = false
     *   CharUtil.isLetterLower('&copy;') = false
     * </pre>
     *
     * @param ch  被检查的字符
     * @return true表示为小写字母，小写字母指a~z
     */
    public static boolean isAlphaLower(final char ch) {
        return ch >= 'a' && ch <= 'z';
    }

    /**
     * 是否为字母字符或数字字符
     * @param ch 被检查的字符
     * @return 为true为字母或数字字符
     */
    public static boolean isAlphaOrDigit(final char ch) {
        return isAlpha(ch) || isDigit(ch);
    }

    /**
     * 是否空白符<br>
     * 空白符包括空格、制表符、全角空格和不间断空格<br>
     * @param c 字符
     * @return 是否空白符
     * @see Character#isWhitespace(int)
     * @see Character#isSpaceChar(int)
     */
    public static boolean isBlankChar(char c) {
        return isBlankChar((int) c);
    }

    /**
     * 是否空白符<br>
     * 空白符包括空格、制表符、全角空格和不间断空格<br>
     * @see Character#isWhitespace(int)
     * @see Character#isSpaceChar(int)
     * @param c 字符
     * @return 是否空白符
     */
    public static boolean isBlankChar(int c) {
        return Character.isWhitespace(c) || Character.isSpaceChar(c) || c == '\ufeff' || c == '\u202a';
    }

    /**
     * 给定对象对应的类是否为字符类，字符类包括：
     *
     * <pre>
     * Character.class
     * char.class
     * </pre>
     *
     * @param value 被检查的对象
     * @return true表示为字符类
     */
    public static boolean isChar(Object value) {
        return value instanceof Character || value.getClass() == char.class;
    }

    /**
     * 字符是否为常用中文字符;
     * <pre>
     *     char表示的Unicode最大表示65535,不能表示所有中文字符;
     *     网上常用的中文字符表示范围为 [0x4e00,0x9fa5]（或十进制[19968,40869]）
     * </pre>
     * @param c 被检查字符
     */
    public static boolean isChineseCharacter(final char c){
        return c >= CHINESE_UNICODE_RANGE[0] && c <= CHINESE_UNICODE_RANGE[1];
    }

    /**
     * 字符是否为属性名的字符。
     * 属性名称 有 字母,数字,下划线,点  组成
     * @param c 被检查字符
     * @return 为true为属性字符
     */
    public static boolean isPropertyNameChar(final char c) {
        return isDigit(c) || isAlpha(c) || (c == '_') || (c == '.');
    }

    /**
     * 比较两个字符是否相同
     *
     * @param c1 字符1
     * @param c2 字符2
     * @param ignoreCase 是否忽略大小写
     * @return 是否相同
     * @since 4.0.3
     */
    public static boolean equals(char c1, char c2, boolean ignoreCase) {
        if (ignoreCase) {
            return Character.toLowerCase(c1) == Character.toLowerCase(c2);
        }
        return c1 == c2;
    }

    /**
     * 字符转为字符串<br>
     *
     * @param c 字符
     * @return 字符串
     */
    public static String toString(char c) {
        return String.valueOf(c);
    }

}

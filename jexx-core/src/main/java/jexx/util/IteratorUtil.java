package jexx.util;

import java.util.Iterator;
import java.util.Optional;

/**
 * 迭代器工具类
 * @author jeff
 * @since 2019/10/23
 */
public class IteratorUtil {

    /**
     * 找到任意一个对象
     * @param list 集合
     * @return 可选对象
     */
    public static <T> Optional<T> findAny(Iterator<T> list){
        if(list == null || !list.hasNext()){
            return Optional.empty();
        }
        return Optional.ofNullable(list.next());
    }

    public static boolean isEmpty(Iterator<?> iterator) {
        return null == iterator || !iterator.hasNext();
    }

    public static boolean isNotEmpty(Iterator<?> iterator) {
        return !isEmpty(iterator);
    }

}

package jexx.util;

import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * MAP工具集
 *
 * @author jeff
 */
public class MapUtil {

    public static boolean isEmpty(final Map<?, ?> map) {
        return (map == null || map.isEmpty());
    }

    public static boolean isNotEmpty(final Map<?, ?> map) {
        return !isEmpty(map);
    }

    /**
     * map中的value转成list
     * @param map MAP对象
     * @param <K>  key
     * @param <V>   value
     * @return  list集合
     */
    public static <K,V> List<V> convertValueToList(final Map<K, V> map){
        if(map == null){
            return null;
        }
        return new ArrayList<>(map.values());
    }

    /**
     * map中的value过滤转成list
     * @param map MAP对象
     * @param predicate 判定
     * @param <K>   KEY
     * @param <V>   VALUE
     * @return  list集合
     */
    public static <K,V> List<V> convertValueToList(final Map<K, V> map, Predicate<? super V> predicate){
        if(map == null){
            return null;
        }
        return map.values().stream().filter(predicate).collect(Collectors.toList());
    }

    /**
     * 将键列表和值列表转换为Map<br>
     * 以键为准，值与键位置需对应。如果键元素数多于值元素，多余部分值用null代替。<br>
     * 如果值多于键，忽略多余的值。
     *
     * @param <K> 键类型
     * @param <V> 值类型
     * @param keys 键列表
     * @param values 值列表
     * @return 标题内容Map
     * @since 3.1.0
     */
    public static <K, V> Map<K, V> toMap(Iterable<K> keys, Iterable<V> values) {
        return toMap(null == keys ? null : keys.iterator(), null == values ? null : values.iterator());
    }

    /**
     * 将键列表和值列表转换为Map<br>
     * 以键为准，值与键位置需对应。如果键元素数多于值元素，多余部分值用null代替。<br>
     * 如果值多于键，忽略多余的值。
     *
     * @param <K> 键类型
     * @param <V> 值类型
     * @param keys 键列表
     * @param values 值列表
     * @return 标题内容Map
     */
    public static <K, V> Map<K, V> toMap(Iterator<K> keys, Iterator<V> values) {
        final Map<K, V> resultMap = new HashMap<>();
        if (CollectionUtil.isNotEmpty(keys)) {
            while (keys.hasNext()) {
                resultMap.put(keys.next(), (null != values && values.hasNext()) ? values.next() : null);
            }
        }
        return resultMap;
    }

    /**
     * 根据键值创建map
     * @param k 键
     * @param v 值
     * @param <K> 键类型
     * @param <V> 值类型
     */
    public static <K, V> Map<K, V> toMap(K k, V v) {
        final Map<K, V> resultMap = new HashMap<>();
        resultMap.put(k, v);
        return resultMap;
    }

    /**
     * 创建单一键值的map, 不支持新增
     * @param k 键
     * @param v 值
     * @return 只有唯一一个键值的map
     */
    public static <K, V> Map<K, V> toSingletonMap(K k, V v) {
        return Collections.singletonMap(k, v);
    }

    //------------------------------------------------------------------------create map

    /**
     * 创建map
     * @param mapType map类型
     * @param capacity 容量
     * @param <K> map的key类型
     * @param <V> map的value类型
     * @return map
     */
    public static <K, V> Map<K, V> createMap(Class<?> mapType, int capacity) {
        return createMap(mapType, null, capacity);
    }

    /**
     * 创建map
     * @param mapType map类型
     * @param keyType key类型
     * @param capacity 容量
     * @param <K> map的key类型
     * @param <V> map的value类型
     * @return map
     */
    @SuppressWarnings({"unchecked", "rawtypes"})
    public static <K, V> Map<K, V> createMap(Class<?> mapType, Class<?> keyType, int capacity) {
        Assert.notNull(mapType, "Map type must not be null");
        if (mapType.isInterface()) {
            if (Map.class == mapType) {
                return new LinkedHashMap<>(capacity);
            }
            else if (SortedMap.class == mapType || NavigableMap.class == mapType) {
                return new TreeMap<>();
            }
            else {
                throw new IllegalArgumentException("Unsupported Map interface: " + mapType.getName());
            }
        }
        else if (EnumMap.class == mapType) {
            Assert.notNull(keyType, "Cannot create EnumMap for unknown key type");
            if (!Enum.class.isAssignableFrom(mapType)) {
                throw new IllegalArgumentException("Supplied type is not an enum: " + mapType.getName());
            }
            return new EnumMap(keyType.asSubclass(Enum.class));
        }
        else {
            if (!Map.class.isAssignableFrom(mapType)) {
                throw new IllegalArgumentException("Unsupported Map type: " + mapType.getName());
            }
            try {
                return (Map<K, V>) ReflectUtil.newInstance(mapType);
            }
            catch (Throwable ex) {
                throw new IllegalArgumentException("Could not instantiate Map type: " + mapType.getName(), ex);
            }
        }
    }

    /**
     * 代替 {@link Map#containsKey(Object)}。抛出 {@link ClassCastException} 或者 {@link NullPointerException} 时返回false
     * @param map map
     * @param key key
     * @return 是否包含key
     */
    public static boolean safeContainsKey(Map<?, ?> map, Object key) {
        Objects.requireNonNull(map);
        try {
            return map.containsKey(key);
        } catch (ClassCastException | NullPointerException e) {
            return false;
        }
    }

    /**
     * 代替 {@link Map#get(Object)}。抛出 {@link ClassCastException} 或者 {@link NullPointerException} 时返回false
     * @param map map
     * @param key key
     * @param <V> value类型
     * @return value
     */
    public static <V> V safeGet(Map<?, V> map, Object key){
        Objects.requireNonNull(map);
        try {
            return map.get(key);
        } catch (ClassCastException | NullPointerException e) {
            return null;
        }
    }

    /**
     * 代替 {@link Map#remove(Object)} 。抛出 {@link ClassCastException} 或者 {@link NullPointerException} 时返回false
     * @param map map
     * @param key key
     * @param <V> value类型
     * @return value
     */
    public static <V> V safeRemove(Map<?, V> map, Object key){
        Objects.requireNonNull(map);
        try {
            return map.remove(key);
        } catch (ClassCastException | NullPointerException e) {
            return null;
        }
    }


}

package jexx.util;

import java.util.Base64;

public abstract class Base64Util {

    private static final byte[] EMPTY_BUFFER = new byte[0];

    //------------------encode

    public static byte[] encode(byte[] src) {
        return src.length == 0 ? src : Base64.getEncoder().encode(src);
    }

    public static String encodeAsStr(byte[] src){
        return StringUtil.str(encode(src));
    }

    public static String encodeAsStr(String src){
        byte[] srcBytes = StringUtil.getBytes(src);
        return encodeAsStr(srcBytes);
    }

    public static byte[] encodeUrlSafe(byte[] src) {
        return src.length == 0 ? src : Base64.getUrlEncoder().encode(src);
    }

    public static String encodeUrlSafeAsStr(byte[] src) {
        return StringUtil.str(encodeUrlSafe(src));
    }

    //------------------decode

    public static byte[] decode(byte[] src) {
        return src.length == 0 ? src : Base64.getDecoder().decode(src);
    }

    public static String decodeAsStr(byte[] src) {
        return StringUtil.str(decode(src));
    }

    public static byte[] decodeUrlSafe(byte[] src) {
        return src.length == 0 ? src : Base64.getUrlDecoder().decode(src);
    }

    public static String decodeUrlSafeAsStr(byte[] src) {
        return StringUtil.str(decodeUrlSafe(src));
    }

    public static byte[] decode(String src) {
        return src.isEmpty() ? EMPTY_BUFFER : decode(StringUtil.getBytes(src));
    }

    public static String decodeAsStr(String src) {
        return StringUtil.str(decode(src));
    }

}

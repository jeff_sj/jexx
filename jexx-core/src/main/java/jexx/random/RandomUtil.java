package jexx.random;

import jexx.util.CharUtil;
import jexx.util.StringPool;

import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

/**
 * 随机字符工具
 */
public abstract class RandomUtil {

    private static final char[] ALPHA_RANGE = new char[] {'A', 'Z', 'a', 'z'};
    private static final char[] ALPHA_NUMERIC_RANGE = new char[] {'0', '9', 'A', 'Z', 'a', 'z'};

    /**
     * 带横杠的uuid,超过32位
     */
    public static String randomUUID(){
        return UUID.randomUUID().toString();
    }

    /**
     * 去掉横杠的uuid, 32位
     */
    public static String simpleUUID(){
        return randomUUID().replace("-", "");
    }

    /**
     * 指定字符中随机组成字符串
     * @param count 字符串长度
     * @param chars 待随机字符串
     */
    public static String random(int count, final char[] chars) {
        if (count == 0) {
            return StringPool.EMPTY;
        }
        char[] result = new char[count];
        while (count-- > 0) {
            result[count] = chars[ThreadLocalRandom.current().nextInt(chars.length)];
        }
        return new String(result);
    }

    /**
     * @see RandomUtil#random(int, char[])
     */
    public static String random(final int count, final String chars) {
        return random(count, chars.toCharArray());
    }

    /**
     * 指定字符间随机,组成字符串
     * @param count 数量
     * @param start 开始字符
     * @param end 结束字符
     */
    public static String random(int count, final char start, final char end) {
        if (count == 0) {
            return StringPool.EMPTY;
        }
        char[] result = new char[count];
        int len = end - start + 1;
        while (count-- > 0) {
            result[count] = (char) (ThreadLocalRandom.current().nextInt(len) + start);
        }
        return new String(result);
    }

    /**
     * 随机指定的ascii
     * @param count 数量
     */
    public static String randomAscii(final int count) {
        return random(count, (char) 32, (char) 126);
    }

    /**
     * 随机指定的汉字
     * @param count 数量
     */
    public static String randomChinese(final int count){
        return random(count, CharUtil.CHINESE_UNICODE_RANGE[0], CharUtil.CHINESE_UNICODE_RANGE[1]);
    }

    /**
     * 随机指定的数字
     * @param count 数量
     */
    public static String randomNumeric(final int count) {
        return random(count, '0', '9');
    }

    /**
     * 指定字符间随机字符
     * @param count 数量
     * @param ranges 字符区间
     */
    public static String randomRanges(int count, final char... ranges) {
        if (count == 0) {
            return StringPool.EMPTY;
        }
        int i = 0;
        int len = 0;
        int[] lens = new int[ranges.length];
        while (i < ranges.length) {
            int gap = ranges[i + 1] - ranges[i] + 1;
            len += gap;
            lens[i] = len;
            i += 2;
        }

        char[] result = new char[count];
        while (count-- > 0) {
            char c = 0;
            int r = ThreadLocalRandom.current().nextInt(len);
            for (i = 0; i < ranges.length; i += 2) {
                if (r < lens[i]) {
                    r += ranges[i];
                    if (i != 0) {
                        r -= lens[i - 2];
                    }
                    c = (char) r;
                    break;
                }
            }
            result[count] = c;
        }
        return new String(result);
    }

    /**
     * 随机字母
     * @param count 数量
     */
    public static String randomAlpha(final int count) {
        return randomRanges(count, ALPHA_RANGE);
    }

    /**
     * 随机字母数字
     * @param count 数量
     */
    public static String randomAlphaNumeric(final int count) {
        return randomRanges(count, ALPHA_NUMERIC_RANGE);
    }

}

package jexx.io;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

public class ByteArrayResource implements Resource {

    private final byte[] buffer;

    public ByteArrayResource(byte[] buffer) {
        this.buffer = buffer;
    }

    @Override
    public boolean isBuffered() {
        return true;
    }

    @Override
    public InputStream getInputStream() throws IOException {
        return new ByteArrayInputStream(buffer);
    }

    @Override
    public boolean isFile() {
        return false;
    }

    @Override
    public File getFile() {
        return null;
    }

    @Override
    public String getFilename() {
        return null;
    }

    @Override
    public long getSize() {
        return buffer.length;
    }
}

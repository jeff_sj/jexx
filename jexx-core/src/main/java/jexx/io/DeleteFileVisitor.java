package jexx.io;

import jexx.util.Console;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;

/**
 * 删除文件
 *
 * @author jeff
 * @since 2019/6/13
 */
public class DeleteFileVisitor implements FileVisitor<Path> {

    @Override
    public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs){
        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
        boolean success = Files.deleteIfExists(file);
        if(!success){
            Console.log("Not delete file: {}", file);
        }
        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult visitFileFailed(Path file, IOException exc){
        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
        if(exc == null){
            boolean success = Files.deleteIfExists(dir);
            if(!success){
                Console.log("Not delete file: {}", dir);
            }
        }
        else{
            throw exc;
        }
        return FileVisitResult.CONTINUE;
    }

}

package jexx.io;

import jexx.util.Assert;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.InputStream;

public class InputStreamResource implements Resource{

    private final InputStream inputStream;
    private final boolean buffered;
    private final long size;
    private boolean read = false;

    public InputStreamResource(InputStream inputStream) {
        this(inputStream, -1);
    }

    public InputStreamResource(InputStream inputStream, long size) {
        this(inputStream, size, checkBuffered(inputStream));
    }

    public InputStreamResource(InputStream inputStream, long size, boolean buffered) {
        this.inputStream = inputStream;
        this.size = size;
        this.buffered = buffered;
    }

    @Override
    public boolean isBuffered() {
        return buffered;
    }

    @Override
    public InputStream getInputStream() {
        Assert.isFalse(read, "inputStream has already been read");
        read = true;
        return this.inputStream;
    }

    @Override
    public boolean isFile() {
        return false;
    }

    @Override
    public File getFile() {
        return null;
    }

    @Override
    public String getFilename() {
        return null;
    }

    public static boolean checkBuffered(InputStream inputStream){
        return BufferedInputStream.class.isAssignableFrom(inputStream.getClass());
    }

    @Override
    public long getSize() {
        return size;
    }
}

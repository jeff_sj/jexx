package jexx.io;

import jexx.util.StringPool;
import jexx.util.StringUtil;

import java.io.File;
import java.nio.file.Paths;

/**
 * 参考 jodd\jodd-core\src\main\java\jodd\io\FileUtil.java
 *
 * @author jeff
 */
public class FileNameUtil {

    private static final char EXTENSION_SEPARATOR = '.';

    private static final char UNIX_SEPARATOR = '/';

    private static final char WINDOWS_SEPARATOR = '\\';

    private static final char SYSTEM_SEPARATOR = File.separatorChar;

    private static final char OTHER_SEPARATOR;
    static {
        if (SYSTEM_SEPARATOR == WINDOWS_SEPARATOR) {
            OTHER_SEPARATOR = UNIX_SEPARATOR;
        } else {
            OTHER_SEPARATOR = WINDOWS_SEPARATOR;
        }
    }

    private static boolean isSeparator(final char ch) {
        return (ch == UNIX_SEPARATOR) || (ch == WINDOWS_SEPARATOR);
    }

    /**
     * 标准化文件名
     * @param filename 标准化文件名
     * @return 修复后的路径
     */
    public static String normalize(final String filename) {
        if(StringUtil.isEmpty(filename)){
            return filename;
        }
        return Paths.get(filename).normalize().toString();
    }

    /**
     * 找到文件名最后一个分隔符
     * @param filename 文件名
     * @return 分隔符所有
     */
    public static int indexOfLastSeparator(final String filename){
        if(filename == null){
            return -1;
        }
        int lastUnixPos = filename.lastIndexOf(UNIX_SEPARATOR);
        int lastWindowsPos = filename.lastIndexOf(WINDOWS_SEPARATOR);
        return Math.max(lastUnixPos, lastWindowsPos);
    }

    /**
     * 文件名的扩展分隔符索引. 为逗号
     * @param filename 文件名
     * @return 扩展分隔符索引
     */
    public static int indexOfExtension(final String filename) {
        if (filename == null) {
            return -1;
        }
        int extensionPos = filename.lastIndexOf(EXTENSION_SEPARATOR);
        int lastSeparator = indexOfLastSeparator(filename);
        return (lastSeparator > extensionPos ? -1 : extensionPos);
    }

    /**
     * 移除文件格式扩展后的名称
     * @param filename 文件名
     * @return 移除文件格式扩展后的名称
     */
    public static String removeExtension(final String filename) {
        if (filename == null) {
            return null;
        }
        int index = indexOfExtension(filename);
        if (index == -1) {
            return filename;
        } else {
            return filename.substring(0, index);
        }
    }

    /**
     * 替换扩展格式
     * <p>
     *  <pre>
     *         replaceExtension("test.txt", "pdf")="test.pdf"
     *         replaceExtension("test", "pdf")="test.pdf"
     *  </pre>
     * </p>
     * @param filename 文件名称
     * @param extension 扩展格式
     */
    public static String replaceExtension(final String filename, String extension){
        if (filename == null) {
            return null;
        }
        int index = indexOfExtension(filename);
        if (index == -1) {
            return filename.concat(Character.toString(EXTENSION_SEPARATOR)).concat(extension);
        } else {
            return filename.substring(0, index).concat(Character.toString(EXTENSION_SEPARATOR)).concat(extension);
        }
    }

    /**
     * 替换文件名,格式不变
     * <p>
     *     <pre>
     *         replaceBaseName("a", "b")="b"
     *         replaceBaseName("a/b/c", "d")="a/b/d"
     *         replaceBaseName("a.txt", "b")="b.txt"
     *         replaceBaseName("a/b/c.txt", "d")="a/b/d.txt"
     *     </pre>
     * </p>
     * @param filename 文件名称
     * @param name 待替换的文件名, 为空返回filename
     * @return 替换后的文件名
     */
    public static String replaceBaseName(final String filename, final String name){
        if (filename == null) {
            return null;
        }
        if(StringUtil.isEmpty(name)){
            return filename;
        }
        int indexOfLastSeparator = indexOfLastSeparator(filename);
        int indexOfExtension = indexOfExtension(filename);

        if(indexOfLastSeparator == -1 && indexOfExtension == -1){
            return name;
        }
        else if(indexOfLastSeparator == -1){
            return name.concat(filename.substring(indexOfExtension));
        }
        else if(indexOfExtension == -1){
            return filename.substring(0, indexOfLastSeparator+1).concat(name);
        }
        else{
            return filename.substring(0, indexOfLastSeparator+1).concat(name).concat(filename.substring(indexOfExtension));
        }
    }

    /**
     * 替换文件名(包含格式)
     * <p>
     *     <pre>
     *         replaceName("a", "b")="b"
     *         replaceName("a.txt", "b")="b"
     *         replaceName("a/b/c", "d")="a/b/d"
     *         replaceName("a/b/c.txt", "d")="a/b/d"
     *         replaceName("D:/a/b/c", "b")="D:/a/b/d"
     *         replaceName("D:/a/b/c.txt", "b")="D:/a/b/d"
     *     </pre>
     * </p>
     * @param filename 文件名称
     * @param name 待替换的文件名, 为空返回filename
     * @return 替换后的文件名
     */
    public static String replaceName(final String filename, final String name){
        if (filename == null) {
            return null;
        }
        if(StringUtil.isEmpty(name)){
            return filename;
        }
        int indexOfLastSeparator = indexOfLastSeparator(filename);
        if(indexOfLastSeparator == -1){
            return name;
        }
        else{
            return filename.substring(0, indexOfLastSeparator+1).concat(name);
        }
    }

    /**
     * 获取文件名称
     * <p>
     * <pre>
     *      a/b/c.txt --> c.txt
     * </pre>
     * </p>
     * @param filename 文件名
     * @return 去掉路径的文件名称
     */
    public static String getName(final String filename) {
        if (filename == null) {
            return null;
        }
        int index = indexOfLastSeparator(filename);
        return filename.substring(index + 1);
    }

    /**
     * 获取文件名的基本名称
     * <p>
     *     <pre>
     *         c.txt --> c
     *     </pre>
     * </p>
     * @param filename 文件名
     * @return 基本名称
     */
    public static String getBaseName(final String filename) {
        return removeExtension(getName(filename));
    }

    /**
     * 获取文件名的扩展格式
     * <p>
     *     <pre>
     *         c.txt --> txt
     *     </pre>
     * </p>
     * @param filename 文件名
     * @return 扩展格式
     */
    public static String getExtension(final String filename) {
        if (filename == null) {
            return null;
        }
        int index = indexOfExtension(filename);
        if (index == -1) {
            return StringPool.EMPTY;
        } else {
            return filename.substring(index + 1);
        }
    }

    /**
     * 获取相对路径
     * <p>
     *     <pre>
     *         relativePath("D:\\test\\test.pdf", "D:\\test")="test.pdf"
     *     </pre>
     * </p>
     * @param targetPath 目标地址
     * @param basePath 基本地址
     * @return 决定路径
     */
    public static String relativePath(final String targetPath, final String basePath) {
        return new File(basePath).toPath().relativize(new File(targetPath).toPath()).toString();
    }

    /**
     * 获取文件的父目录名
     * @param filename 文件名
     * @return 父目录名
     */
    public static String getParentName(final String filename){
        return Paths.get(filename).normalize().getParent().toString();
    }

}

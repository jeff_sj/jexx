package jexx.io;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

/**
 * 万物皆资源
 * @author jeff
 * @since 2021-02-19
 */
public interface Resource {

    /**
     * 资源内容是否已缓存在jvm中
     */
    boolean isBuffered();

    /**
     * 获取资源的输出流
     * @return {@link InputStream}
     * @throws IOException i/o异常
     */
    InputStream getInputStream() throws IOException;

    /**
     * 是否文件资源
     */
    boolean isFile();

    /**
     * 获取文件
     */
    File getFile();

    /**
     * 获取文件名称
     */
    String getFilename();

    /**
     * 资源大小, -1 表示未知
     */
    long getSize();


}

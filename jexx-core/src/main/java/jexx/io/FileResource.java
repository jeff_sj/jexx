package jexx.io;

import jexx.util.Assert;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;

/**
 * 文件资源
 * @author jeff
 * @since 2021-02-19
 */
public class FileResource implements Resource{

    private final File file;
    private final Path filePath;

    public FileResource(File file) {
        Assert.notNull(file, "file must not be null");
        this.file = file;
        this.filePath = file.toPath();
    }

    public FileResource(Path filePath) {
        Assert.notNull(filePath, "filePath must not be null");
        this.file = null;
        this.filePath = filePath;
    }

    @Override
    public boolean isBuffered() {
        return false;
    }

    @Override
    public InputStream getInputStream() throws IOException {
        try {
            return Files.newInputStream(this.filePath);
        }
        catch (NoSuchFileException ex) {
            throw new FileNotFoundException(ex.getMessage());
        }
    }

    @Override
    public boolean isFile() {
        return this.file != null;
    }

    @Override
    public File getFile() {
        return this.file != null ? this.file : this.filePath.toFile();
    }

    @Override
    public String getFilename() {
        return this.file != null ? this.file.getName() : this.filePath.getFileName().toString();
    }

    @Override
    public long getSize() {
        return getFile().length();
    }
}

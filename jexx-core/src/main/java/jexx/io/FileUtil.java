package jexx.io;

import jexx.exception.IORuntimeException;
import jexx.exception.UtilException;
import jexx.lang.Charsets;
import jexx.util.*;

import java.io.*;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.file.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * 文件处理工具类
 * @author jeff
 */
public class FileUtil {

    private static final byte[] EMPTY_BYTE = new byte[0];

    /**
     * 比较两个文件是否同一个文件
     * @param file1 文件1
     * @param file2 文件2
     * @return 是否相同
     */
    public static boolean isSameFile(File file1, File file2){
        if(file1 == null || file2 == null){
            return false;
        }
        return isSameFile(file1.toPath(), file2.toPath());
    }

    /**
     * 比较两个文件是否同一个文件
     * @param file1 文件1
     * @param file2 文件2
     * @return 是否相同
     */
    public static boolean isSameFile(Path file1, Path file2){
        if(file1 == null || file2 == null){
            return false;
        }
        try {
            return Files.isSameFile(file1, file2);
        } catch (IOException e) {
            throw new IORuntimeException(e);
        }
    }

    /**
     * 创建File对象
     * @param filename 文件名
     * @return File
     */
    public static File file(String filename) {
        return new File(filename);
    }

    /**
     * 创建File对象
     *
     * @param parent 父目录
     * @param path 文件路径
     * @return File
     */
    public static File file(String parent, String path) {
        if (StringUtil.isBlank(path)) {
            throw new NullPointerException("File path is blank!");
        }
        return new File(parent, path);
    }

    /**
     * 创建File对象
     *
     * @param parent 父文件对象
     * @param path 文件路径
     * @return File
     */
    public static File file(File parent, String path) {
        if (StringUtil.isBlank(path)) {
            throw new NullPointerException("File path is blank!");
        }
        return new File(parent, path);
    }

    /**
     * 文件是否存在
     * @param file file
     */
    public static boolean exists(File file){
        return file != null && file.exists();
    }

    /**
     * 判断文件是否存在
     * @param file 文件路径
     */
    public static boolean exists(String file){
        return exists(file(file));
    }

    public static boolean exists(Path file){
        return file != null && Files.exists(file);
    }


    // ---------------------------------------------------------------- merge

    /**
     * 合并多个文件
     * @param files 待合并的文件
     * @param outputFile 输出文件
     * @param conjunction 文件分割内容
     * @param prefix 前缀
     * @param suffix 后缀
     */
    public static File mergeFiles(File[] files, File outputFile, byte[] conjunction, byte[] prefix, byte[] suffix){
        FileChannel out = null;
        try {
            checkExists(files);
            checkExists(outputFile);

            out = FileChannel.open(toPath(outputFile), StandardOpenOption.WRITE);

            if(prefix != null && prefix.length > 0){
                out.write(ByteBuffer.wrap(prefix));
            }

            for (int i = 0; i < files.length; i++) {
                File f = files[i];
                try(FileChannel in = FileChannel.open(f.toPath(), StandardOpenOption.READ)) {
                    for(long p = 0, le = in.size(); p < le;){
                        p += in.transferTo(p, le-p, out);
                    }
                }
                if(conjunction != null && conjunction.length > 0 && i < files.length - 1){
                    out.write(ByteBuffer.wrap(conjunction));
                }
            }

            if(suffix != null && suffix.length > 0){
                out.write(ByteBuffer.wrap(suffix));
            }

            return outputFile;
        } catch (IOException e) {
            throw new IORuntimeException(e);
        }
        finally {
            IOUtil.closeQuietly(out);
        }
    }

    public static File mergeFiles(File[] files, File outputFile, byte[] conjunction){
        return mergeFiles(files, outputFile, conjunction, EMPTY_BYTE, EMPTY_BYTE);
    }

    /**
     * 合并多个文件
     * @param files 待合并的文件
     * @param outputFile 输出文件
     * @param conjunction 连接字符
     * @param prefix 前缀
     * @param suffix 后缀
     */
    public static File mergeFiles(File[] files, File outputFile, String conjunction, String prefix, String suffix){
        return mergeFiles(files, outputFile, StringUtil.getBytes(conjunction), StringUtil.getBytes(prefix), StringUtil.getBytes(suffix));
    }

    /**
     * 合并多个文件
     * @param files 待合并的文件
     * @param outputFile 输出文件
     * @param conjunction 连接字符
     */
    public static File mergeFiles(File[] files, File outputFile, String conjunction){
        return mergeFiles(files, outputFile, conjunction, StringPool.EMPTY, StringPool.EMPTY);
    }

    /**
     * 合并多个文件
     * @param files 待合并的文件
     * @param outputFile 输出文件
     */
    public static File mergeFiles(File[] files, File outputFile){
        return mergeFiles(files, outputFile, EMPTY_BYTE);
    }

    /**
     * 分割文件成多个文件; 多个分割文件会以0,1,2,3....结尾
     * @param file 待分割文件
     * @param splitFileSize 分割后文件的限制大小,byte长度
     * @param outputDir 输出目录
     * @return 分割后的多个文件
     */
    public static List<File> splitFile(File file, long splitFileSize, File outputDir){
        if(splitFileSize < 1){
            throw new IllegalArgumentException("splitFileSize must be greater than zero");
        }

        FileChannel out = null;
        try {
            checkExists(file);
            checkIsDirectory(outputDir);

            out = FileChannel.open(toPath(file), StandardOpenOption.READ);
            long remainingSize = out.size();
            String fileName = file.getName();
            int i = 0;
            List<File> fileList = new ArrayList<>();
            while(remainingSize > 0){
                long outLength = remainingSize > splitFileSize ? splitFileSize : remainingSize;
                File splitFile = file(outputDir, fileName.concat(Integer.toString(i)));
                try(FileChannel in = FileChannel.open(splitFile.toPath(), StandardOpenOption.TRUNCATE_EXISTING, StandardOpenOption.CREATE, StandardOpenOption.WRITE)) {
                    in.transferFrom(out, 0, outLength);
                }
                fileList.add(splitFile);
                remainingSize = remainingSize - outLength;
                i++;
            }
            return fileList;
        } catch (IOException e) {
            throw new IORuntimeException(e);
        }
        finally {
            IOUtil.closeQuietly(out);
        }
    }

    // ---------------------------------------------------------------- mkdirs

    /**
     * 创建所有目录
     * @param dirs 多级目录
     */
    public static File mkdirs(final String dirs){
        return mkdirs(file(dirs));
    }

    /**
     * 创建所有目录
     * @param dirs 要创建的目录
     */
    public static File mkdirs(final File dirs) {
        try {
            if (dirs.exists()) {
                checkIsDirectory(dirs);
                return dirs;
            }
            return checkCreateDirectory(dirs);
        }
        catch (IOException e){
            throw new IORuntimeException(e);
        }
    }

    /**
     * @see #mkdir(File)
     */
    public static File mkdir(final String dir) {
        return mkdir(file(dir));
    }

    /**
     * 创建单个目录
     */
    public static File mkdir(final File dir){
        try {
            if (dir.exists()) {
                checkIsDirectory(dir);
                return dir;
            }
            return checkCreateDirectory(dir);
        }
        catch (Exception e){
            throw new IORuntimeException(e);
        }
    }

    // ---------------------------------------------------------------- new

    /**
     * 根据文件创建BufferedReader
     * @param file 文件
     * @param charset 编码
     * @return {@link BufferedReader}
     */
    public static BufferedReader newBufferedReader(File file, Charset charset){
        Assert.notNull(file, "file must be not null");
        try {
            return Files.newBufferedReader(file.toPath(), charset);
        } catch (IOException e) {
            throw new IORuntimeException(e);
        }
    }

    /**
     * 根据文件创建BufferedReader
     * @param file 文件
     * @return {@link BufferedReader}
     */
    public static BufferedReader newBufferedReader(File file){
        return newBufferedReader(file, Charsets.UTF_8);
    }

    public static BufferedWriter newBufferedWriter(File file, Charset charset){
        Assert.notNull(file, "file must be not null");
        try {
            return Files.newBufferedWriter(file.toPath(), charset);
        } catch (IOException e) {
            throw new IORuntimeException(e);
        }
    }

    public static BufferedWriter newBufferedWriter(File file){
        return newBufferedWriter(file, Charsets.UTF_8);
    }

    /**
     * 获得输入流
     * @param file 文件
     * @return 输入流
     * @throws IORuntimeException 文件未找到
     */
    public static BufferedInputStream newBufferedInputStream(File file){
        return new BufferedInputStream(IOUtil.toStream(file));
    }

    /**
     * 获得输入流
     * @param path 文件路径
     * @return 输入流
     * @throws IORuntimeException 文件未找到
     */
    public static BufferedInputStream newBufferedInputStream(String path){
        return newBufferedInputStream(file(path));
    }

    public static BufferedOutputStream newBufferedOutputStream(File file){
        try {
            return new BufferedOutputStream(new FileOutputStream(file));
        } catch (Exception e) {
            throw new IORuntimeException(e);
        }
    }

    // ---------------------------------------------------------------- create file

    /**
     * @see #touch(File)
     */
    public static File touch(final String file){
        return touch(file(file));
    }

    /**
     * Implements the Unix "touch" utility. It creates a new {@link File}
     * with size 0 or, if the {@link File} exists already, it is opened and
     * closed without modifying it, but updating the {@link File} date and time.
     */
    public static File touch(final File file) {
        try {
            if (!file.exists()) {
                IOUtil.closeQuietly(new FileOutputStream(file, false));
            }
            file.setLastModified(System.currentTimeMillis());
            return file;
        }
        catch (IOException e){
            throw new IORuntimeException(e);
        }
    }

    //---------------------------------------------create file

    /**
     * 直接创建文件
     * @param file file
     * @return path
     * @throws IOException I/O异常
     */
    public static File createFile(final File file) throws IOException{
        return createFile(file, false, false);
    }

    /**
     * 直接创建文件
     * @param path path
     * @return path
     * @throws IOException I/O异常
     */
    public static Path createFile(final Path path) throws IOException{
        return createFile(path, false, false);
    }

    /**
     * 如果文件或者父目录不存在则创建
     * @param file file
     * @return path
     * @throws IOException I/O异常
     */
    public static File createFileIfNotExist(final File file) throws IOException{
       return createFile(file, true, true);
    }

    /**
     * 如果文件或者父目录不存在则创建
     * @param path path
     * @return path
     * @throws IOException I/O异常
     */
    public static Path createFileIfNotExist(final Path path) throws IOException {
        return createFile(path, true, true);
    }

    /**
     * 创建文件
     * @param path path
     * @param createParentDirIfNotExist 是否父目录不存则创建
     * @param createFileIfNotExist true：不存在才创建文件 false:不管存不存在都创建文件
     * @return path
     * @throws IOException I/O异常
     */
    public static Path createFile(final Path path, final boolean createParentDirIfNotExist, final boolean createFileIfNotExist) throws IOException {
        Objects.requireNonNull(path, "path must be not null");
        Path parent = path.getParent();
        if(parent != null){
            if(Files.exists(parent)){
                checkIsDirectory(parent);
            }
            else{
                if(createParentDirIfNotExist){
                    Files.createDirectories(parent);
                }
                else{
                    throw new IOException("parent["+parent+"] not exist");
                }
            }
        }

        if(Files.exists(path)){
            checkIsFile(path);
        }

        if(Files.notExists(path)){
            Files.createFile(path);
        }
        else{
            if(!createFileIfNotExist){
                Files.createFile(path);
            }
        }
        return path;
    }

    public static File createFile(final File file, final boolean createParentDirIfNotExist, final boolean createFileIfNotExist) throws IOException {
        Objects.requireNonNull(file, "file must be not null");
        Path path = createFile(file.toPath(), createParentDirIfNotExist, createFileIfNotExist);
        return path != null ? path.toFile() : null;
    }

    // ---------------------------------------------------------------- copy file to file

    /**
     * 拷贝源文件(文件或者文件夹)为目标文件, 默认覆盖
     * @param srcFile 源文件
     * @param destFile 目标文件
     * @throws IOException cannot copy file
     */
    public static void copyFile(final String srcFile, final String destFile) throws IOException {
        copyFile(file(srcFile), file(destFile));
    }

    public static void copyFile(final File srcFile, final File destFile) throws IOException {
        copyFile(srcFile, destFile, true);
    }

    public static void copyFile(final File srcFile, final File destFile, final boolean isOverride) throws IOException {
        checkExists(srcFile);
        doCopyFile(srcFile, destFile, isOverride);
    }

    private static void doCopyFile(final File srcFile, final File destFile, final boolean isOverride) throws IOException {
        doCopyFile(srcFile.toPath(), destFile.toPath(), isOverride);
    }

    private static void doCopyFile(final Path srcPath, final Path destPath, final boolean isOverride) throws IOException {
        if(isOverride){
            Files.copy(srcPath, destPath, StandardCopyOption.REPLACE_EXISTING);
        }
        else{
            Files.copy(srcPath, destPath);
        }
    }

    /**
     * 拷贝文件到目标目录
     * @param srcFile 源文件
     * @param destDir 目标目录
     * @param isOverride 同名文件是否覆盖
     */
    public static void copyFileToDir(File srcFile, File destDir, final boolean isOverride) throws IOException {
        checkIsDirectory(destDir);

        Path srcPath = srcFile.toPath();
        Path destDirPath = destDir.toPath();
        Path destPath = destDirPath.resolve(srcFile.getName());
        if(isOverride){
            Files.copy(srcPath, destPath, StandardCopyOption.REPLACE_EXISTING);
        }
        else{
            Files.copy(srcPath, destPath);
        }
    }

    /**
     * 拷贝源文件到目标目录, 同名可覆盖
     * @param srcFile 源文件
     * @param destDir 目标目录
     */
    public static void copyFileToDir(String srcFile, String destDir) throws IOException {
        copyFileToDir(new File(srcFile), new File(destDir), true);
    }


    /**
     * 拷贝目录到目标目录,默认覆盖
     * @param srcDir 源目录
     * @param destDir 目标目录
     */
    public static void copyDirToDir(String srcDir, String destDir) throws IOException {
        copyDirToDir(new File(srcDir), new File(destDir), true);
    }

    /**
     * 递归拷贝源目录到目标目录
     * @param srcDir 源目录
     * @param destDir 目标目录
     * @param isOverride 重名是否覆盖
     */
    public static void copyDirToDir(File srcDir, File destDir, final boolean isOverride) throws IOException {
        checkIsDirectory(srcDir);
        checkIsDirectory(destDir);

        doCopyDirToDir(srcDir, destDir, isOverride);
    }

    private static void doCopyDirToDir(File srcDir, File destDir, final boolean isOverride) throws IOException {
        Path srcDirPath = srcDir.toPath();
        Path destDirPath = destDir.toPath();
        if(destDirPath.startsWith(srcDirPath)){
            throw new IllegalArgumentException(StringUtil.format("Source file[{}] is not in the target file[{}]!", srcDir.getPath(), destDir.getPath()));
        }

        Path destSrcDirPath = destDirPath.resolve(srcDir.getName());

        if(isOverride){
            if(!Files.exists(destSrcDirPath)){
                Files.copy(srcDirPath, destSrcDirPath, StandardCopyOption.REPLACE_EXISTING);
            }
        }
        else{
            Files.copy(srcDirPath, destSrcDirPath);
        }

        File[] children = srcDirPath.toFile().listFiles();
        if(ArrayUtil.isNotEmpty(children)){
            for(File file : children){
                if(file.isDirectory()){
                    doCopyDirToDir(file, destSrcDirPath.toFile(), isOverride);
                }
                else{
                    copyFileToDir(file, destSrcDirPath.toFile(), isOverride);
                }
            }
        }
    }


    // ---------------------------------------------------------------- move file

    private static File doMove(Path srcPath, Path destPath, boolean isOverride){
        final CopyOption[] options = isOverride ? new CopyOption[] { StandardCopyOption.REPLACE_EXISTING } : new CopyOption[] {};
        try {
            return Files.move(srcPath, destPath, options).toFile();
        } catch (IOException e) {
            throw new IORuntimeException(e);
        }
    }

    /**
     * move src file to dest file
     *
     * @param srcFile source file
     * @param destFile target file
     * @param isOverride override if target file exists; it will throw exception if exist!
     * @return the target file
     */
    public static File move(File srcFile, File destFile, boolean isOverride){
        final Path srcPath = srcFile.toPath();
        final Path destPath = destFile.toPath();
        return doMove(srcPath, destPath, isOverride);
    }

    /**
     *  move src file to dest file; it will throw exception if target file exist!
     *
     * @param srcFile source file
     * @param destFile target file
     * @return the target file
     */
    public static File move(File srcFile, File destFile){
        return move(srcFile, destFile, false);
    }

    /**
     * 移动源文件到目标目录
     *
     * @param srcFile 源文件
     * @param destFile 目标目录
     * @param isOverride 是否覆盖; 为true的情况下，如果目标文件存在，则报错
     * @return 目标文件
     */
    public static File moveToDir(File srcFile, File destFile, boolean isOverride){
        try {
            checkIsDirectory(destFile);
            return move(srcFile, FileUtil.file(destFile, srcFile.getName()), isOverride);
        } catch (IOException e) {
            throw new IORuntimeException(e);
        }
    }

    // ---------------------------------------------------------------- rename file

    /**
     * rename a new name for file
     * @param file file
     * @param newName new name
     * @param isOverride override if file exist
     * @return the new file
     */
    public static File rename(File file, String newName, boolean isOverride) {
        final Path path = file.toPath();
        final CopyOption[] options = isOverride ? new CopyOption[] { StandardCopyOption.REPLACE_EXISTING } : new CopyOption[] {};
        try {
            return Files.move(path, path.resolveSibling(newName), options).toFile();
        } catch (IOException e) {
            throw new IORuntimeException(e);
        }
    }

    /**
     * rename a new name for file; it will throw a exception if a file with new name exists
     * @param file file
     * @param newName new name
     * @return the new file
     */
    public static File rename(File file, String newName) {
        return rename(file, newName, false);
    }

    // ---------------------------------------------------------------- delete file

    /**
     * 删除文件,非目录
     * @param file 文件,非目录
     * @return 是否删除成功
     */
    public static boolean deleteFile(File file){
        if(file == null){
            return true;
        }
        try {
            checkIsFile(file);
            return Files.deleteIfExists(file.toPath());
        } catch (IOException e) {
            throw new IORuntimeException(e);
        }
    }

    public static boolean deleteDir(Path dir, boolean forceDelete){
        if(dir == null){
            return true;
        }
        try {
            if(!forceDelete && dir.getNameCount() <= 2){
                throw new UnsupportedOperationException("dir depth is too low");
            }
            if(!Files.exists(dir)){
                return false;
            }

            checkIsDirectory(dir);
            Files.walkFileTree(dir, new DeleteFileVisitor());
            return exists(dir);
        } catch (IOException e) {
            throw new IORuntimeException(e);
        }
    }

    public static boolean deleteDir(Path dir){
        return deleteDir(dir, false);
    }

    /**
     * 删除目录
     *
     * @param dir 目录
     * @param forceDelete 如果目录层次太少，不允许删除, 防止误删除
     * @return 是否删除成功
     */
    public static boolean deleteDir(File dir, boolean forceDelete){
        if(dir == null){
            return true;
        }
        return deleteDir(dir.toPath(), forceDelete);
    }

    public static boolean deleteDir(File dir){
        return deleteDir(dir, false);
    }

    public static boolean deleteDir(String dir, boolean forceDelete){
        return deleteDir(Paths.get(dir), forceDelete);
    }

    public static boolean deleteDir(String dir){
        return deleteDir(dir, false);
    }

    // ---------------------------------------------------------------- get path

    /**
     * 获取绝对路径，相对于ClassPath的目录<br>
     * 如果给定就是绝对路径，则返回原路径，原路径把所有\替换为/<br>
     * 兼容Spring风格的路径表示，例如：classpath:config/example.setting也会被识别后转换
     *
     * @param path 相对路径
     * @return 绝对路径
     */
    public static String getAbsolutePath(String path) {
        return getAbsolutePath(path, null);
    }

    /**
     * 获取绝对路径<br>
     * 此方法不会判定给定路径是否有效（文件或目录存在）
     *
     * @param path 相对路径
     * @param baseClass 相对路径所相对的类
     * @return 绝对路径
     */
    public static String getAbsolutePath(String path, Class<?> baseClass) {
        String normalPath;
        if (path == null) {
            normalPath = StringPool.EMPTY;
        } else {
            normalPath = normalize(path);
            if (isAbsolutePath(normalPath)) {
                // 给定的路径已经是绝对路径了
                return normalPath;
            }
        }

        // 相对于ClassPath路径
        final URL url = ResourceUtil.getResource(normalPath, baseClass);
        if (null != url) {
            // 对于jar中文件包含file:前缀，需要去掉此类前缀，在此做标准化，since 3.0.8 解决中文或空格路径被编码的问题
            return FileUtil.normalize(URLUtil.getDecodedPath(url));
        }

        // 如果资源不存在，则返回一个拼接的资源绝对路径
        final String classPath = ClassUtil.getClassPath();
        if (null == classPath) {
            throw new NullPointerException("ClassPath is null !");
        }

        // 资源不存在的情况下使用标准化路径有问题，使用原始路径拼接后标准化路径
        return normalize(classPath.concat(path));
    }

    /**
     * 检测文件类型
     * @param file file
     * @return 文件类型
     */
    public static String probeContentType(File file){
        return probeContentType(toPath(file));
    }

    public static String probeContentType(Path path){
        try {
            return Files.probeContentType(path);
        } catch (IOException e) {
            throw new IORuntimeException(e);
        }
    }

    // ---------------------------------------------------------------- path

    /**
     * 获取文件对应的path
     */
    public static Path toPath(File file){
        if(file == null){
            throw new IllegalArgumentException("file is null");
        }
        return file.toPath();
    }


    //---------------------------------------------------------------------------write

    /**
     * 写文件
     * @param file 文件对象
     * @param bytes 内容
     */
    public static File write(File file, byte[] bytes){
        try {
            Objects.requireNonNull(file);
            Files.write(toPath(file), bytes);
            return file;
        } catch (IOException e) {
            throw new IORuntimeException(e);
        }
    }

    public static File write(File file, InputStream inputStream){
        Objects.requireNonNull(file);
        try(BufferedOutputStream bufferedOutputStream = newBufferedOutputStream(file)){
            IOUtil.copy(inputStream, bufferedOutputStream);
            return file;
        }
        catch (IOException e){
            throw new IORuntimeException(e);
        }
    }

    public static void writeLines(File file, List<String> lines, Charset cs){
        Objects.requireNonNull(file, "file must be not null");
        writeLines(file.toPath(), lines, cs);
    }

    public static void writeLines(File file, List<String> lines){
        writeLines(file, lines, Charsets.UTF_8);
    }

    public static void writeLines(Path path, List<String> lines){
        writeLines(path, lines, Charsets.UTF_8);
    }

    public static void writeLines(Path path, List<String> lines, Charset cs){
        Objects.requireNonNull(path, "path must be not null");
        try {
            Files.write(path, lines, cs, StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING);
        } catch (IOException e) {
            throw new IORuntimeException(e);
        }
    }

    //---------------------------------------------------------------------------read

    /**
     * 读取文件
     * @param file 文件对象
     */
    public static byte[] read(File file) {
        try {
            checkExists(file);
            return Files.readAllBytes(file.toPath());
        } catch (IOException e) {
            throw new IORuntimeException(e);
        }
    }

    /**
     * 读取内容
     * @param file 文件对象
     * @param charset 编码
     * @return 内容
     */
    public static String readStr(File file, Charset charset) {
        byte[] bytes = read(file);
        return StringUtil.str(bytes, charset);
    }

    /**
     * UTF-8编码读取文件
     * @param file 文件
     * @return 内容
     */
    public static String readStr(File file) {
        return readStr(file, Charsets.UTF_8);
    }

    /**
     * 指定编码读取文件所有行
     * @param file 文件对象
     * @param charset 编码
     * @return 所有行
     */
    public static List<String> readAllLines(File file, Charset charset){
        return readLines(file, charset, null);
    }

    /**
     * 默认编码读取文件所有行
     * @param file 文件对象
     * @return 所有行
     */
    public static List<String> readLines(File file){
        return readAllLines(file, Charsets.UTF_8);
    }

    /**
     * 指定读取文件行
     * @param file 文件对象
     * @param charset 指定编码
     * @param predicate 判定这行是否需要
     * @return 过滤后的行
     */
    public static List<String> readLines(File file, Charset charset, Predicate<String> predicate){
        List<String> lines;
        try {
            checkExists(file);
            lines = Files.readAllLines(file.toPath(), charset);
        } catch (IOException e) {
            throw new IORuntimeException(e);
        }

        if(predicate != null){
            return lines.stream().filter(predicate).collect(Collectors.toList());
        }
        return lines;
    }

    /**
     * 默认编码读取文件行
     * @param file 文件对象
     * @param predicate 判定这行是否需要
     * @return 过滤后的行
     */
    public static List<String> readLines(File file, Predicate<String> predicate){
        return readLines(file, Charsets.UTF_8, predicate);
    }

    public static String normalize(final String filename) {
        return FileNameUtil.normalize(filename);
    }

    /**
     * 给定路径已经是绝对路径<br>
     * 此方法并没有针对路径做标准化，建议先执行{@link #normalize(String)}方法标准化路径后判断
     *
     * @param path 需要检查的Path
     * @return 是否已经是绝对路径
     */
    public static boolean isAbsolutePath(String path) {
        if (StringUtil.isEmpty(path)) {
            return false;
        }

        if (StringPool.SLASH.equals(String.valueOf(path.charAt(0))) || path.matches("^[a-zA-Z]:[/\\\\].*")) {
            // 给定的路径已经是绝对路径了
            return true;
        }
        return false;
    }

    public static long size(File file) throws IOException {
        Objects.requireNonNull(file, "file must be not null");
        return size(file.toPath());
    }

    /**
     * 计算实际文件大小或者目录下所有实际文件大小总和
     * @param path 实际文件或者目录
     * @return 文件大小总和
     * @throws IOException IO异常
     */
    public static long size(Path path) throws IOException {
        Objects.requireNonNull(path, "path must be not null");
        Assert.isTrue(Files.exists(path), "path[{}] not exist", path);
        if(Files.isRegularFile(path)){
            return Files.size(path);
        }
        else if (Files.isDirectory(path)){
            try (Stream<Path> walk = Files.walk(path)) {
                return walk.filter(Files::isRegularFile).mapToLong(s -> {
                    try {
                        return Files.size(s);
                    } catch (IOException e) {
                        throw new UtilException(e);
                    }
                }).sum();
            }
        }
        else{
            throw new UnsupportedOperationException("only support to regular file or dir, path=" + path);
        }
    }

    public static long size(String file) throws IOException {
        Assert.hasText(file, "file must be not empty");
        return size(file(file));
    }


    private static void checkDeleteSuccessful(final File dir) throws IOException {
        if (!dir.delete()) {
            throw new IOException("Unable delete: " + dir);
        }
    }

    private static File checkCreateDirectory(final File dir) throws IOException {
        if (!dir.mkdirs()) {
            throw new IOException("Not create dir: " + dir);
        }
        return dir;
    }

    private static void checkExists(final File file) throws FileNotFoundException {
        if (!file.exists()) {
            throw new FileNotFoundException("Not found: " + file);
        }
    }

    private static void checkExists(final Path path) throws FileNotFoundException {
        if (!Files.exists(path)) {
            throw new FileNotFoundException("Not found: " + path);
        }
    }

    private static void checkExists(final File[] files) throws FileNotFoundException {
        for(File f : files){
            checkExists(f);
        }
    }

    private static void checkIsDirectory(final File dir) throws IOException {
        if (dir == null || !dir.isDirectory()) {
            throw new IOException("Not directory: " + dir);
        }
    }

    private static void checkIsDirectory(final Path dir) throws IOException {
        if (dir == null || !Files.isDirectory(dir)) {
            throw new IOException("Not directory: " + dir);
        }
    }

    private static void checkIsFile(final File file) throws IOException{
        if (file == null || !file.isFile()) {
            throw new IOException("Not file: " + file);
        }
    }

    private static void checkIsFile(final Path path) throws IOException{
        if (path == null || (path.toFile() != null && !path.toFile().isFile())) {
            throw new IOException("Not file: " + path);
        }
    }



}

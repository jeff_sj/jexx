package jexx.io;

import jexx.util.StringUtil;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.URL;
import java.net.UnknownHostException;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;
import java.nio.charset.Charset;
import java.nio.file.StandardOpenOption;

/**
 * Network utilities.
 */
public class NetUtil {

	public static final String LOCAL_HOST = "localhost";
	public static final String LOCAL_IP = "127.0.0.1";
	public static final String DEFAULT_MASK = "255.255.255.0";
	public static final int INT_VALUE_127_0_0_1 = 0x7f000001;

	/**
	 * Resolves IP address from a hostname.
	 */
	public static String resolveIpAddress(final String hostname) {
		try {
			InetAddress netAddress;

			if (hostname == null || hostname.equalsIgnoreCase(LOCAL_HOST)) {
				netAddress = InetAddress.getLocalHost();
			} else {
				netAddress = Inet4Address.getByName(hostname);
			}
			return netAddress.getHostAddress();
		} catch (UnknownHostException ignore) {
			return null;
		}
	}

	/**
	 * Returns IP address as integer.
	 */
	public static int getIpAsInt(final String ipAddress) {
		int ipIntValue = 0;
		String[] tokens = StringUtil.split(ipAddress, '.');
		for (String token : tokens) {
			if (ipIntValue > 0) {
				ipIntValue <<= 8;
			}
			ipIntValue += Integer.parseInt(token);
		}
		return ipIntValue;
	}

	public static int getMaskAsInt(String mask) {
		if (!validateAgaintIPAdressV4Format(mask)) {
			mask = DEFAULT_MASK;
		}
		return getIpAsInt(mask);
	}

	public static boolean isSocketAccessAllowed(final int localIp, final int socketIp, final int mask) {
		boolean _retVal = false;

		if (socketIp == INT_VALUE_127_0_0_1 || (localIp & mask) == (socketIp & mask)) {
			_retVal = true;
		}
		return _retVal;
	}

	/**
	 * Checks given string against IP address v4 format.
	 *
	 * @param input an ip address - may be null
	 * @return <tt>true</tt> if param has a valid ip v4 format <tt>false</tt> otherwise
	 * @see <a href="https://en.wikipedia.org/wiki/IP_address#IPv4_addresses">ip address v4</a>
	 */
	public static boolean validateAgaintIPAdressV4Format(final String input) {
		if (input == null) {
			return false;
		}

		int hitDots = 0;
		char[] data = input.toCharArray();
		for (int i = 0; i < data.length; i++) {
			char c = data[i];
			int b = 0;
			do {
				if (c < '0' || c > '9') {
					return false;
				}
				b = (b * 10 + c) - 48;
				if (++i >= data.length) {
					break;
				}
				c = data[i];
			} while (c != '.');

			if (b > 255) {
				return false;
			}
			hitDots++;
		}

		return hitDots == 4;
	}

	/**
	 * Resolves host name from IP address bytes.
	 */
	public static String resolveHostName(final byte[] ip) {
		try {
			InetAddress address = InetAddress.getByAddress(ip);
			return address.getHostName();
		} catch (UnknownHostException ignore) {
			return null;
		}
	}

	// ---------------------------------------------------------------- download

	/**
	 * Downloads resource as byte array.
	 */
	public static byte[] downloadBytes(final String url) throws IOException {
		try (InputStream inputStream = new URL(url).openStream()) {
			return IOUtil.readBytes(inputStream);
		}
	}

	/**
	 * Downloads resource as String.
	 */
	public static String downloadString(final String url, final Charset charset) throws IOException {
		try (InputStream inputStream = new URL(url).openStream()) {
			return new String(IOUtil.readBytes(inputStream), charset);
		}
	}

	/**
	 * Downloads resource as String.
	 */
	public static String downloadString(final String url) throws IOException {
		try (InputStream inputStream = new URL(url).openStream()) {
			return new String(IOUtil.readBytes(inputStream));
		}
	}

	/**
	 * Downloads resource to a file, potentially very efficiently.
	 */
	public static void downloadFile(final String url, final File file) throws IOException {
		try (
			InputStream inputStream = new URL(url).openStream();
			ReadableByteChannel rbc = Channels.newChannel(inputStream);
			FileChannel fileChannel = FileChannel.open(
				file.toPath(),
				StandardOpenOption.CREATE,
				StandardOpenOption.TRUNCATE_EXISTING,
				StandardOpenOption.WRITE)
		) {
			fileChannel.transferFrom(rbc, 0, Long.MAX_VALUE);
		}
	}
}
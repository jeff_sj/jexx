package jexx.xml;

import jexx.exception.UtilException;
import jexx.io.IOUtil;
import jexx.util.Assert;
import jexx.util.MapUtil;
import jexx.util.StringUtil;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * xml工具类
 */
public class XmlUtil {

    /**
     * map 转 xml
     * @param obj map对象
     * @param rootName xml根名称
     */
    public static <K,V> String mapToXML(Map<K, V> obj, String rootName){
        Assert.notNull(rootName, "rootName must be not null");
        StringBuilder sb = new StringBuilder();
        sb.append("<").append(rootName).append(">");
        if(MapUtil.isNotEmpty(obj)){
            obj.forEach((k,v)->{
                sb.append("<").append(k.toString()).append(">");
                sb.append(v != null ? v.toString() : "");
                sb.append("</").append(k.toString()).append(">");
            });
        }
        sb.append("</").append(rootName).append(">");
        return sb.toString();
    }

    /**
     * xml 转 map
     * @param xml xml
     * @return map
     */
    public static Map<String, String> xmlToMap(String xml){
        Map<String, String> data = new LinkedHashMap<>(8);
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();

        InputStream stream = null;
        try {
            DocumentBuilder documentBuilder= documentBuilderFactory.newDocumentBuilder();
            stream = new ByteArrayInputStream(StringUtil.getBytes(xml));
            org.w3c.dom.Document doc = documentBuilder.parse(stream);
            doc.getDocumentElement().normalize();
            NodeList nodeList = doc.getDocumentElement().getChildNodes();
            for (int idx=0; idx<nodeList.getLength(); ++idx) {
                Node node = nodeList.item(idx);
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    org.w3c.dom.Element element = (org.w3c.dom.Element) node;
                    data.put(element.getNodeName(), element.getTextContent());
                }
            }
        }
        catch (Exception e){
            throw new UtilException(e);
        }
        finally {
            IOUtil.closeQuietly(stream);
        }
        return data;
    }

    /**
     * xml 转换为 对象
     * @param xml xml
     * @param clazz 对象
     */
    public static <T> T toBean(String xml, Class<T> clazz){
        try {
            JAXBContext context = JAXBContext.newInstance(clazz);
            Unmarshaller unmarshaller = context.createUnmarshaller();
            Object obj = unmarshaller.unmarshal(new StringReader(xml));
            return clazz.cast(obj);
        } catch (JAXBException e) {
            throw new UtilException(e);
        }
    }

    public static <T> T toBean(InputStream inputStream, Class<T> clazz){
        try {
            JAXBContext context = JAXBContext.newInstance(clazz);
            Unmarshaller unmarshaller = context.createUnmarshaller();
            Object obj = unmarshaller.unmarshal(inputStream);

            return clazz.cast(obj);
        } catch (JAXBException e) {
            throw new UtilException(e);
        }
        finally {
            IOUtil.closeQuietly(inputStream);
        }
    }

    /**
     * 对象转xml
     * @param obj 待转换对象
     * @param fragment 是否省略xm头声明信息
     * @param format 是否格式化
     * @return xml
     */
    public static String toXML(Object obj, boolean fragment, boolean format){
        try {
            JAXBContext context = JAXBContext.newInstance(obj.getClass());

            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, format);
            marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
            marshaller.setProperty(Marshaller.JAXB_FRAGMENT, fragment);
            StringWriter writer = new StringWriter();
            marshaller.marshal(obj, writer);
            return writer.toString();
        }
        catch (JAXBException e){
            throw new UtilException(e);
        }
    }

    /**
     * 对象转xml
     * @param obj 待转换对象
     * @param fragment 是否省略xm头声明信息
     * @return xml
     */
    public static String toXML(Object obj, boolean fragment){
        return toXML(obj, fragment, false);
    }

    /**
     * 对象转xml
     * @param obj 待转换对象
     * @return xml
     */
    public static String toXML(Object obj){
        return toXML(obj, false);
    }

    /**
     * 对象转格式化的xml
     * @param obj 对象
     * @param fragment 是否省略xm头声明信息
     * @return 格式化后的xml
     */
    public static String toPrettyXML(Object obj, boolean fragment){
        return toXML(obj, fragment, true);
    }

    /**
     * 对象转格式化的xml
     * @param obj 对象
     * @return 格式化后的xml
     */
    public static String toPrettyXML(Object obj){
        return toPrettyXML(obj, true);
    }


    public static String toString(Object obj){
        return toXML(obj, false);
    }

}

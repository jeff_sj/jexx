package jexx.collect;

import jexx.util.Assert;
import jexx.util.CollectionUtil;

import java.io.Serializable;
import java.util.*;

/**
 * @author jeff
 * @since 2019/10/28
 */
public class ArrayTable<R, C, V> extends AbstractTable<R, C, V> implements Serializable {

    private List<R> rowKeys;
    private List<C> columnKeys;
    private final Map<R, Integer> rowKeyToIndex;
    private final Map<C, Integer> columnKeyToIndex;
    private final V[][] array;
    private final Map<V, List<Key<R,C>>> valueMap;

    @SuppressWarnings("unchecked")
    public ArrayTable(List<? extends R> rowKeys, List<? extends C> columnKeys) {
        Assert.notEmpty(rowKeys);
        Assert.notEmpty(columnKeys);
        this.rowKeys = Collections.unmodifiableList(rowKeys);
        this.columnKeys = Collections.unmodifiableList(columnKeys);
        this.rowKeyToIndex = index(rowKeys);
        this.columnKeyToIndex = index(columnKeys);

        this.array = (V[][])new Object[rowKeys.size()][columnKeys.size()];
        eraseAll();
        this.valueMap = new HashMap<>(this.rowKeys.size() * this.columnKeys.size());
    }

    private static <E> Map<E, Integer> index(List<? extends E> list){
        Map<E, Integer> map = new HashMap<>(list.size());
        for (int i = 0; i < list.size(); i++) {
            map.put(list.get(i), i);
        }
        return map;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public boolean containsKey(Object rowKey, Object columnKey){
        return containsRowKey(rowKey) && containsColumnKey(columnKey);
    }

    @Override
    public boolean containsRowKey(Object rowKey){
        return rowKeyToIndex.containsKey(rowKey);
    }

    @Override
    public boolean containsColumnKey(Object columnKey){
        return columnKeyToIndex.containsKey(columnKey);
    }

    @Override
    public boolean containsValue(Object value) {
        List<Key<R,C>> list = valueMap.get(value);
        return CollectionUtil.isNotEmpty(list);
    }

    public V at(int rowIndex, int columnIndex){
        Assert.isTrue(rowIndex > -1, "rowKey not exist");
        Assert.isTrue(columnIndex > -1, "columnKey not exist");
        return array[rowIndex][columnIndex];
    }

    @Override
    public V get(Object rowKey, Object columnKey) {
        Integer rowIndex = rowKeyToIndex.get(rowKey);
        Integer columnIndex = columnKeyToIndex.get(columnKey);
        return (rowIndex == null || columnIndex == null)? null : at(rowIndex, columnIndex);
    }

    @Override
    public V put(R rowKey, C columnKey, V value) {
        Integer rowIndex = rowKeyToIndex.get(rowKey);
        Assert.isTrue(rowIndex != null && rowIndex > -1, "rowKey={} not exist", rowKey);
        Integer columnIndex = columnKeyToIndex.get(columnKey);
        Assert.isTrue(columnIndex != null && columnIndex > -1, "columnKey={} not exist", columnKey);

        array[rowIndex][columnIndex] = value;
        return value;
    }

    @Override
    public void putAll(Table<? extends R, ? extends C, ? extends V> table) {
        super.putAll(table);
    }

    public V set(int rowIndex, int columnIndex, V value){
        Assert.isTrue(rowIndex < rowKeys.size(), "rowIndex is out of row key");
        Assert.isTrue(columnIndex < columnKeys.size(), "columnIndex is out of column key");
        V oldValue = array[rowIndex][columnIndex];
        array[rowIndex][columnIndex] = value;
        return oldValue;
    }

    @Override
    Iterator<Cell<R, C, V>> cellIterator() {
        return new ListIterator<Cell<R, C, V>>() {
            private final int size = ArrayTable.this.size();
            private int position;

            @Override
            public boolean hasNext() {
                return position < size;
            }

            @Override
            public Cell<R, C, V> next() {
                if (!hasNext()) {
                    throw new NoSuchElementException();
                }
                return get(position++);
            }

            @Override
            public boolean hasPrevious() {
                return position > 0;
            }

            @Override
            public Cell<R, C, V> previous() {
                if (!hasPrevious()) {
                    throw new NoSuchElementException();
                }
                return get(position--);
            }

            @Override
            public int nextIndex() {
                return position+1;
            }

            @Override
            public int previousIndex() {
                return position-1;
            }

            protected Cell<R, C, V> get(final int index) {
                final int rowIndex = index / columnKeys.size();
                final int columnIndex = index % columnKeys.size();
                return new SimpleCell<>(rowKeys.get(rowIndex), columnKeys.get(columnIndex), at(rowIndex, columnIndex));
            }

            @Override
            public void remove() {
                throw new UnsupportedOperationException();
            }

            @Override
            public void set(Cell<R, C, V> rcvCell) {
                throw new UnsupportedOperationException();
            }

            @Override
            public void add(Cell<R, C, V> rcvCell) {
                throw new UnsupportedOperationException();
            }
        };
    }

    public Iterator<Cell<R, C, V>> toColumnIterator(R rowKey) {
        int rowIndex = rowKeyToIndex.get(rowKey);
        if(rowIndex >= rowKeys.size()){
            throw new ArrayIndexOutOfBoundsException();
        }
        return new ListIterator<Cell<R, C, V>>() {
            private final int size = ArrayTable.this.array[rowIndex].length;
            private int position;

            @Override
            public boolean hasNext() {
                return position < size;
            }

            @Override
            public Cell<R, C, V> next() {
                if (!hasNext()) {
                    throw new NoSuchElementException();
                }
                return get(position++);
            }

            @Override
            public boolean hasPrevious() {
                return position > 0;
            }

            @Override
            public Cell<R, C, V> previous() {
                if (!hasPrevious()) {
                    throw new NoSuchElementException();
                }
                return get(position--);
            }

            @Override
            public int nextIndex() {
                return position+1;
            }

            @Override
            public int previousIndex() {
                return position-1;
            }

            protected Cell<R, C, V> get(final int index) {
                return new SimpleCell<>(rowKeys.get(rowIndex), columnKeys.get(index), at(rowIndex, index));
            }

            @Override
            public void remove() {
                throw new UnsupportedOperationException();
            }

            @Override
            public void set(Cell<R, C, V> rcvCell) {
                throw new UnsupportedOperationException();
            }

            @Override
            public void add(Cell<R, C, V> rcvCell) {
                throw new UnsupportedOperationException();
            }
        };
    }

    public Iterator<Cell<R, C, V>> toRowIterator(C columnKey) {
        int columnIndex = columnKeyToIndex.get(columnKey);
        if(columnIndex >= columnKeys.size()){
            throw new ArrayIndexOutOfBoundsException();
        }
        return new ListIterator<Cell<R, C, V>>() {
            private final int size = ArrayTable.this.array.length;
            private int position;

            @Override
            public boolean hasNext() {
                return position < size;
            }

            @Override
            public Cell<R, C, V> next() {
                if (!hasNext()) {
                    throw new NoSuchElementException();
                }
                return get(position++);
            }

            @Override
            public boolean hasPrevious() {
                return position > 0;
            }

            @Override
            public Cell<R, C, V> previous() {
                if (!hasPrevious()) {
                    throw new NoSuchElementException();
                }
                return get(position--);
            }

            @Override
            public int nextIndex() {
                return position+1;
            }

            @Override
            public int previousIndex() {
                return position-1;
            }

            protected Cell<R, C, V> get(final int index) {
                return new SimpleCell<>(rowKeys.get(index), columnKeys.get(columnIndex), at(index, columnIndex));
            }

            @Override
            public void remove() {
                throw new UnsupportedOperationException();
            }

            @Override
            public void set(Cell<R, C, V> rcvCell) {
                throw new UnsupportedOperationException();
            }

            @Override
            public void add(Cell<R, C, V> rcvCell) {
                throw new UnsupportedOperationException();
            }
        };
    }

    @Override
    public int size() {
        return rowKeys.size() * columnKeys.size();
    }

    @Override
    public V remove(Object rowKey, Object columnKey) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void clear() {
        eraseAll();
    }

    public void eraseAll() {
        for (V[] row : array) {
            Arrays.fill(row, null);
        }
    }

}

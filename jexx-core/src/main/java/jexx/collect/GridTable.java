package jexx.collect;

import java.io.Serializable;
import java.util.Iterator;

/**
 * 网格table,用于表达类似excel的格子类表格; <b>非线程安全</b>
 * @author jeff
 * @since 2019/11/5
 */
public class GridTable<V> extends AbstractTable<Integer, Integer, V> implements Table<Integer, Integer, V>, Cloneable, Serializable {

    private static final long serialVersionUID = 1L;

    transient final SimpleCell<Integer, Integer, V>[][] table;

    @SuppressWarnings("unchecked")
    public GridTable(int rowNum, int columnNum) {
        this.table = new SimpleCell[rowNum][columnNum];
    }

    @Override
    Iterator<Cell<Integer, Integer, V>> cellIterator() {
        return null;
    }

    @Override
    public int size() {
        return 0;
    }

}

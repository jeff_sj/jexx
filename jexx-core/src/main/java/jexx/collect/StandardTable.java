package jexx.collect;

import java.io.Serializable;
import java.util.Iterator;
import java.util.Map;

/**
 * @author jeff
 * @since 2019/10/28
 */
class StandardTable <R, C, V> extends AbstractTable<R, C, V> implements Serializable {

    private final Map<Key<R, C>, V> dataMap;

    StandardTable(Map<Key<R, C>, V> dataMap) {
        this.dataMap = dataMap;
    }

    @Override
    Iterator<Table.Cell<R, C, V>> cellIterator(){
        return new CellIterator();
    }

    @Override
    public V put(R rowKey, C columnKey, V value) {
        Key<R,C> key = createKey(rowKey, columnKey);
        return dataMap.put(key, value);
    }

    @Override
    public int size() {
        return dataMap != null ? dataMap.size() : 0;
    }

    private class CellIterator implements Iterator<Cell<R, C, V>> {
        final Iterator<Map.Entry<Key<R,C>, V>> cellIterator = dataMap.entrySet().iterator();

        @Override
        public boolean hasNext() {
            return cellIterator.hasNext();
        }

        @Override
        public Cell<R, C, V> next() {
            Map.Entry<Key<R,C>, V> entry = cellIterator.next();
            if (entry != null) {
                Key<R,C> key = entry.getKey();
                return new SimpleCell<>(key, entry.getValue());
            }
            throw new IllegalStateException();
        }

        @Override
        public void remove() {
            cellIterator.remove();
        }
    }

}

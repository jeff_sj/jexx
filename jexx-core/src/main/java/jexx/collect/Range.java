package jexx.collect;

import jexx.util.Assert;
import jexx.util.IterableUtil;
import jexx.util.StringPool;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.SortedSet;

/**
 * 范围
 * @author jeff
 * @since 2019/10/23
 */
public class Range<C extends Comparable> implements Serializable {

    final Cut<C> lowerBound;
    final Cut<C> upperBound;

    private Range(Cut<C> lowerBound, Cut<C> upperBound) {
        Objects.requireNonNull(lowerBound, "lowerBound must be not null");
        Objects.requireNonNull(upperBound, "upperBound must be not null");
        if(lowerBound.compareTo(upperBound) > 0 || lowerBound == Cut.aboveAll() || upperBound == Cut.belowAll()){
            throw new IllegalArgumentException("Invalid range: " + toString(lowerBound, upperBound));
        }
        this.lowerBound = lowerBound;
        this.upperBound = upperBound;
    }

    static <C extends Comparable<?>> Range<C> create(Cut<C> lowerBound, Cut<C> upperBound) {
        return new Range<>(lowerBound, upperBound);
    }

    public static <C extends Comparable<?>> Range<C> open(C lower, C upper) {
        return create(Cut.aboveValue(lower), Cut.belowValue(upper));
    }

    public static <C extends Comparable<?>> Range<C> closed(C lower, C upper) {
        return create(Cut.belowValue(lower), Cut.aboveValue(upper));
    }

    public static <C extends Comparable<?>> Range<C> closedOpen(C lower, C upper) {
        return create(Cut.belowValue(lower), Cut.belowValue(upper));
    }

    public static <C extends Comparable<?>> Range<C> openClosed(C lower, C upper) {
        return create(Cut.aboveValue(lower), Cut.aboveValue(upper));
    }

    public static <C extends Comparable<?>> Range<C> range(C lower, BoundType lowerType, C upper, BoundType upperType) {
        Objects.requireNonNull(lowerType);
        Objects.requireNonNull(upperType);

        Cut<C> lowerBound = (lowerType == BoundType.OPEN) ? Cut.aboveValue(lower) : Cut.belowValue(lower);
        Cut<C> upperBound = (upperType == BoundType.OPEN) ? Cut.belowValue(upper) : Cut.aboveValue(upper);
        return create(lowerBound, upperBound);
    }

    public static <C extends Comparable<?>> Range<C> lessThan(C endpoint) {
        return create(Cut.belowAll(), Cut.belowValue(endpoint));
    }

    public static <C extends Comparable<?>> Range<C> atMost(C endpoint) {
        return create(Cut.belowAll(), Cut.aboveValue(endpoint));
    }

    public static <C extends Comparable<?>> Range<C> greaterThan(C endpoint) {
        return create(Cut.aboveValue(endpoint), Cut.aboveAll());
    }

    public static <C extends Comparable<?>> Range<C> atLeast(C endpoint) {
        return create(Cut.belowValue(endpoint), Cut.aboveAll());
    }

    public static <C extends Comparable<?>> Range<C> singleton(C value) {
        return closed(value, value);
    }

    /**
     * 是否有下边界
     * @return bool
     */
    public boolean hasLowerBound() {
        return lowerBound != Cut.belowAll();
    }

    /**
     * 下边界值
     */
    public C lowerEndpoint() {
        return lowerBound.endpoint();
    }

    /**
     * 下边界类型
     */
    public BoundType lowerBoundType() {
        return lowerBound.typeAsLowerBound();
    }

    /**
     * 是否有上边界
     */
    public boolean hasUpperBound() {
        return upperBound != Cut.aboveAll();
    }

    /**
     * 上边界值
     */
    public C upperEndpoint() {
        return upperBound.endpoint();
    }

    /**
     * 上边界类型
     */
    public BoundType upperBoundType() {
        return upperBound.typeAsUpperBound();
    }

    /**
     * 判断range是否未空，如 {@code [1,1) 或者 (1,1]}。 当泛型为Integer时 类似{@code [1,2) 或者 (1,2] }
     * 的表达是不为空的
     * @return bool
     */
    public boolean isEmpty() {
        return lowerBound.equals(upperBound);
    }

    /**
     * 根据离散域标准化range, 可用于判定两个range范围是否一致
     * @param domain 离散域
     * @return 标准化的range
     */
    public Range<C> canonical(DiscreteDomain<C> domain) {
        Objects.requireNonNull(domain, "domain must be not null");
        Cut<C> lower = lowerBound.canonical(domain);
        Cut<C> upper = upperBound.canonical(domain);
        return (lower == lowerBound && upper == upperBound) ? this : create(lower, upper);
    }

    /**
     * 判断是否包含指定元素
     * @param value 指定元素
     * @return bool
     */
    public boolean contains(C value) {
        Objects.requireNonNull(value, "value must be not null");
        return lowerBound.isLessThan(value) && !upperBound.isLessThan(value);
    }

    public boolean containsAll(Iterable<? extends C> values) {
        if (IterableUtil.isEmpty(values)) {
            return true;
        }

        for (C value : values) {
            if (!contains(value)) {
                return false;
            }
        }
        return true;
    }

    private <T> SortedSet<T> cast(Iterable<T> iterable) {
        return (SortedSet<T>) iterable;
    }

    /**
     * 通过离散域 获取当前range的值迭代器
     * @param domain 离散域
     * @return range指定离散域的迭代器
     */
    public Iterator<C> iterator(DiscreteDomain<C> domain) {
        return new RangeIterator<>(this, domain);
    }

    @Override
    public boolean equals(Object object) {
        if (object instanceof Range) {
            Range<?> other = (Range<?>) object;
            return lowerBound.equals(other.lowerBound)
                    && upperBound.equals(other.upperBound);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return lowerBound.hashCode() * 31 + upperBound.hashCode();
    }

    @Override
    public String toString() {
        return toString(lowerBound, upperBound);
    }

    private String toString(Cut<?> lowerBound, Cut<?> upperBound) {
        StringBuilder sb = new StringBuilder(16);
        lowerBound.describeAsLowerBound(sb);
        sb.append(StringPool.COMMA);
        upperBound.describeAsUpperBound(sb);
        return sb.toString();
    }


    private static class RangeIterator<C extends Comparable> implements Iterator<C> {

        private Range<C> range;
        private DiscreteDomain<C> domain;

        private C current;
        private C next;

        public RangeIterator(Range<C> range, DiscreteDomain<C> domain) {
            this.range = range.canonical(domain);
            this.domain = domain;
        }

        @Override
        public boolean hasNext() {
            if(next == null){
                C n;
                if(current == null){
                    n = this.range.lowerBound.endpoint;
                }
                else{
                    n = this.domain.next(current);
                    if(this.range.upperBound.isLessThan(n)){
                        return false;
                    }
                }
                this.next = n;
            }
            return next != null;
        }

        @Override
        public C next() {
            Assert.notNull(this.next, "please use hasNext");
            current = this.next;
            this.next = null;
            return current;
        }
    }

}

package jexx.collect;

import jexx.util.Assert;

import java.io.Serializable;
import java.util.*;

/**
 * 固定长度的队列, 非线程安全
 */
public class FixedLengthQueue<E> extends AbstractQueue<E> implements Serializable {

    private static final long serialVersionUID = 6533390596633207042L;

    private final Queue<E> delegate;
    private final int maxSize;

    public FixedLengthQueue(int maxSize) {
        Assert.isTrue(maxSize > 0, "maxSize must be greater than zero");
        this.delegate = new ArrayDeque<>();
        this.maxSize = maxSize;
    }

    protected Queue<E> delegate(){
        return this.delegate;
    }

    @Override
    public Iterator<E> iterator() {
        return delegate().iterator();
    }

    @Override
    public int size() {
        return delegate().size();
    }

    @Override
    public boolean add(E e) {
        if(maxSize == 0) {
            return true;
        }
        if(size() == maxSize) {
            delegate.remove();
        }
        delegate.add(e);
        return true;
    }

    @Override
    public boolean offer(E e) {
        return add(e);
    }

    @Override
    public E poll() {
        return delegate().poll();
    }

    @Override
    public E peek() {
        return delegate().peek();
    }

}

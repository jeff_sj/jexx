package jexx.collect;

import java.util.List;
import java.util.Map;

/**
 * 一个key对应多个值的map
 * @author jeff
 * @since 1.0.10
 * @param <K> key类型
 * @param <V> value元素类型
 */
public interface MultiValueMap<K, V> extends Map<K, List<V>> {

    V getFirst(K key);

    void add(K key, V value);

    void addAll(K key, List<V> values);

    void addAll(MultiValueMap<K, V> values);

    default void addIfAbsent(K key, V value) {
        if (!containsKey(key)) {
            add(key, value);
        }
    }

    void set(K key, V value);

    void setAll(Map<K, V> values);

    Map<K, V> toSingleValueMap();

}

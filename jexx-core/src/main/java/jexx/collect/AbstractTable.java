package jexx.collect;

import java.util.*;

/**
 * table抽象接口
 * @author jeff
 * @since 2019/10/25
 */
abstract class AbstractTable<R, C, V> implements Table<R, C, V> {

    private transient Collection<V> values;

    protected AbstractTable() {
    }

    @Override
    public boolean isEmpty() {
        return size() == 0;
    }

    @Override
    public boolean containsKey(Object rowKey, Object columnKey) {
        Iterator<Cell<R,C,V>> i = cellSet().iterator();
        while(i.hasNext()) {
            Cell<R,C,V> e = i.next();
            return eq(rowKey, e.getKey().getRowKey()) && eq(columnKey, e.getKey().getColumnKey());
        }
        return false;
    }

    private boolean eq(Object a, Object b){
        if(a == null && b == null){
            return true;
        }
        else if(a == null || b == null){
            return false;
        }
        return a.equals(b);
    }

    @Override
    public boolean containsRowKey(Object rowKey) {
        Iterator<Cell<R,C,V>> i = cellSet().iterator();
        while(i.hasNext()) {
            Cell<R,C,V> e = i.next();
            return eq(rowKey, e.getKey().getRowKey());
        }
        return false;
    }

    @Override
    public boolean containsColumnKey(Object columnKey) {
        Iterator<Cell<R,C,V>> i = cellSet().iterator();
        while(i.hasNext()) {
            Cell<R,C,V> e = i.next();
            return eq(columnKey, e.getKey().getColumnKey());
        }
        return false;
    }

    @Override
    public boolean containsValue(Object value) {
        Iterator<Cell<R, C,V>> i = cellSet().iterator();
        if (value==null) {
            while (i.hasNext()) {
                Cell<R, C,V> e = i.next();
                if(e.getValue()==null){
                    return true;
                }
            }
        } else {
            while (i.hasNext()) {
                Cell<R, C,V> e = i.next();
                if (value.equals(e.getValue())){
                    return true;
                }
            }
        }
        return false;
    }

    protected Key<R,C> createKey(R rowKey, C columnKey){
        return new SimpleKey<>(rowKey, columnKey);
    }

    @Override
    public V get(Object rowKey, Object columnKey) {
        Iterator<Cell<R, C,V>> i = cellSet().iterator();
        while(i.hasNext()) {
            Cell<R, C,V> e = i.next();
            if (eq(rowKey, e.getKey().getRowKey()) && eq(columnKey, e.getKey().getColumnKey())){
                return e.getValue();
            }
        }
        return null;
    }

    @Override
    public V put(R rowKey, C columnKey, V value) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void putAll(Table<? extends R, ? extends C, ? extends V> table) {
        for (Table.Cell<? extends R, ? extends C, ? extends V> cell : table.cellSet()) {
            Table.Key<? extends R, ? extends C> key = cell.getKey();
            put(key.getRowKey(), key.getColumnKey(), cell.getValue());
        }
    }

    @Override
    public V remove(Object rowKey, Object columnKey) {
        Iterator<Cell<R, C,V>> i = cellSet().iterator();
        V oldValue = null;
        for(Iterator<Cell<R, C,V>> iterator = cellSet().iterator(); i.hasNext();){
            Cell<R, C,V> cell = iterator.next();
            if (eq(rowKey, cell.getKey().getRowKey()) && eq(columnKey, cell.getKey().getColumnKey())){
                i.remove();
                oldValue = cell.getValue();
            }
        }
        return oldValue;
    }

    @Override
    public Set<Cell<R, C,V>> cellSet(){
        return createCellSet();
    }

    Set<Cell<R, C, V>> createCellSet() {
        return new CellSet();
    }

    abstract Iterator<Table.Cell<R, C, V>> cellIterator();

    @Override
    public Collection<V> values() {
        Collection<V> result = values;
        if (result == null) {
            result = new AbstractCollection<V>() {
                @Override
                public Iterator<V> iterator() {
                    return new Iterator<V>() {
                        private Iterator<Cell<R,C,V>> i = cellSet().iterator();
                        @Override
                        public boolean hasNext() {
                            return i.hasNext();
                        }
                        @Override
                        public V next() {
                            return i.next().getValue();
                        }
                        @Override
                        public void remove() {
                            i.remove();
                        }
                    };
                }
                @Override
                public int size() {
                    return AbstractTable.this.size();
                }
                @Override
                public boolean isEmpty() {
                    return AbstractTable.this.isEmpty();
                }
                @Override
                public void clear() {
                    AbstractTable.this.clear();
                }
                @Override
                public boolean contains(Object v) {
                    return AbstractTable.this.containsValue(v);
                }
            };
            values = result;
        }
        return result;
    }

    @Override
    public void clear() {
        cellSet().clear();
    }

    @Override
    public int hashCode() {
        int h = 0;
        for(Cell<R,C,V> cell : cellSet()){
            h += cell.hashCode();
        }
        return h;
    }

    @Override
    public String toString() {
        Iterator<Cell<R,C,V>> i = cellSet().iterator();
        if(!i.hasNext()){
            return "{}";
        }

        StringBuilder sb = new StringBuilder();
        sb.append('{');
        for (;;) {
            Cell<R,C,V> e = i.next();
            Key<R,C> key = e.getKey();
            R rowKey = key.getRowKey();
            C columnKey = key.getColumnKey();
            V value = e.getValue();
            sb.append("(");
            sb.append(rowKey == this ? "null" : rowKey);
            sb.append(",");
            sb.append(columnKey == this ? "null" : columnKey);
            sb.append(")=");
            sb.append(value == this ? "null" : value);
            if (!i.hasNext()){
                return sb.append('}').toString();
            }
            sb.append(',').append(' ');
        }
    }


    public static class SimpleKey<R,C> implements Key<R,C>, java.io.Serializable{

        private static final long serialVersionUID = 0;

        private final R rowKey;
        private final C columnKey;

        public SimpleKey(R rowKey, C columnKey) {
            this.rowKey = rowKey;
            this.columnKey = columnKey;
        }

        @Override
        public R getRowKey() {
            return rowKey;
        }

        @Override
        public C getColumnKey() {
            return columnKey;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o){
                return true;
            }
            if (o == null || getClass() != o.getClass()){
                return false;
            }
            Key<?, ?> simpleKey = (Key<?, ?>) o;
            return Objects.equals(rowKey, simpleKey.getRowKey()) && Objects.equals(columnKey, simpleKey.getColumnKey());
        }

        @Override
        public int hashCode() {
            return Objects.hash(rowKey, columnKey);
        }
    }

    public static class SimpleCell<R,C,V> implements Cell<R,C,V>, java.io.Serializable{

        private static final long serialVersionUID = 0;

        private final Key<R, C> key;
        private final V value;

        public SimpleCell(R rowKey, C columnKey, V value) {
            this.key = new SimpleKey<>(rowKey, columnKey);
            this.value = value;
        }

        public SimpleCell(Key<R, C> key, V value) {
            this.key = key;
            this.value = value;
        }

        public SimpleCell(Cell<R, C, V> cell) {
            this.key = cell.getKey();
            this.value = cell.getValue();
        }

        @Override
        public Key<R, C> getKey() {
            return key;
        }

        @Override
        public V getValue() {
            return value;
        }

        @Override
        public boolean equals(Object o) {
            if(this == o){
                return true;
            }
            if(o == null || getClass() != o.getClass()){
                return false;
            }
            Cell<?, ?, ?> that = (Cell<?, ?, ?>) o;
            return Objects.equals(key, that.getKey()) && Objects.equals(value, that.getValue());
        }

        @Override
        public int hashCode() {
            return Objects.hash(key, value);
        }
    }

    class CellSet extends AbstractSet<Cell<R, C, V>> {
        @Override
        public boolean contains(Object o) {
            if (o instanceof Cell) {
                Cell<?, ?, ?> cell = (Cell<?, ?, ?>) o;
                V value = AbstractTable.this.get(cell.getKey().getRowKey(), cell.getKey().getColumnKey());
                return value != null && value.equals(cell.getValue());
            }
            return false;
        }

        @Override
        public boolean remove(Object o) {
            if (o instanceof Cell) {
                Cell<?, ?, ?> cell = (Cell<?, ?, ?>) o;
                AbstractTable.this.remove(cell.getKey().getRowKey(), cell.getKey().getColumnKey());
                return true;
            }
            return false;
        }

        @Override
        public void clear() {
            AbstractTable.this.clear();
        }

        @Override
        public Iterator<Table.Cell<R, C, V>> iterator() {
            return cellIterator();
        }

        @Override
        public int size() {
            return AbstractTable.this.size();
        }
    }

}

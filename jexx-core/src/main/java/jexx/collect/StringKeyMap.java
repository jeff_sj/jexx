package jexx.collect;

import jexx.util.*;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * key为字符串类型的map; 常用于 json 反序列化时使用
 * @author jeff
 * @since 2021-02-27
 */
public interface StringKeyMap extends Map<String, Object> {

    default Boolean getBoolean(String key){
        return getBoolean(key, true);
    }

    default Boolean getBoolean(String key, boolean exceptionIfTypeNotMatch){
        return getObject(key, Boolean.class, exceptionIfTypeNotMatch);
    }

    default Byte getByte(String key){
        return getByte(key, true);
    }

    default Byte getByte(String key, boolean exceptionIfTypeNotMatch){
        return getObject(key, Byte.class, exceptionIfTypeNotMatch);
    }

    default Character getCharacter(String key){
        return getCharacter(key, true);
    }

    default Character getCharacter(String key, boolean exceptionIfTypeNotMatch){
        return getObject(key, Character.class, exceptionIfTypeNotMatch);
    }

    default Short getShort(String key){
        return getShort(key, true);
    }

    default Short getShort(String key, boolean exceptionIfTypeNotMatch){
        return getObject(key, Short.class, exceptionIfTypeNotMatch);
    }

    default Integer getInteger(String key){
        return getInteger(key, true);
    }

    default Integer getInteger(String key, boolean exceptionIfTypeNotMatch){
        return getObject(key, Integer.class, exceptionIfTypeNotMatch);
    }

    default Long getLong(String key){
        return getLong(key, true);
    }

    default Long getLong(String key, boolean exceptionIfTypeNotMatch){
        return getObject(key, Long.class, exceptionIfTypeNotMatch);
    }

    default Double getDouble(String key){
        return getDouble(key, true);
    }

    default Double getDouble(String key, boolean exceptionIfTypeNotMatch){
        return getObject(key, Double.class, exceptionIfTypeNotMatch);
    }

    default BigDecimal getBigDecimal(String key){
        return getBigDecimal(key, true);
    }

    default BigDecimal getBigDecimal(String key, boolean exceptionIfTypeNotMatch){
        return getObject(key, BigDecimal.class, exceptionIfTypeNotMatch);
    }

    default String getString(String key){
        return getString(key, true);
    }

    default String getString(String key, boolean exceptionIfTypeNotMatch){
        return getObject(key, String.class, exceptionIfTypeNotMatch);
    }

    default StringKeyMap getStringKeyMap(String key){
        return getStringKeyMap(key, true);
    }

    default StringKeyMap getStringKeyMap(String key, boolean exceptionIfTypeNotMatch){
        return getObject(key, LinkedStringKeyMap.class, exceptionIfTypeNotMatch);
    }

    default <T> T getObject(String key, Class<T> clazz){
        return getObject(key, clazz, true);
    }

    /**
     * 获取指定类型的值
     * @param key key
     * @param clazz 值类型
     * @param exceptionIfTypeNotMatch 类型不匹配是否抛出异常
     * @param <T> 值泛型
     * @return 值
     */
    @SuppressWarnings("unchecked")
    default <T> T getObject(String key, Class<T> clazz, boolean exceptionIfTypeNotMatch){
        Object value = get(key);
        if(value == null){
            return null;
        }

        if(clazz.isAssignableFrom(value.getClass())){
            return  (T) value;
        }

        if(clazz.equals(LinkedStringKeyMap.class) && value instanceof Map){
            return (T)new LinkedStringKeyMap((Map<?,?>) value);
        }

        if(clazz.equals(BigDecimal.class)){
            String str = value.toString();
            if(NumberUtil.isNumber(str)){
                return (T)new BigDecimal(str);
            }
        }

        if(exceptionIfTypeNotMatch){
            throw new ClassCastException(value.getClass() +" cannot cast "+clazz+", key="+key);
        }
        return null;
    }

    @SuppressWarnings("unchecked")
    default <T> T[] getArray(String key, Class<T> clazz){
         Object value = get(key);
         if(value == null){
             return ArrayUtil.newArray(clazz, 0);
         }

         if(!ArrayUtil.isArray(value) && !(value instanceof Set) && !(value instanceof List)){
             throw new ClassCastException(value.getClass() +" is not list");
         }
         if(value.getClass().isArray()){
             Class<?> componentType = value.getClass().getComponentType();
             if(!ClassUtil.isAssignableFrom(clazz, componentType)){
                 throw new ClassCastException(StringUtil.format("class[{}] cannot be cast to {} for key[{}]", componentType, clazz, key));
             }

            return (T[])value;
         }
         else if(value instanceof Set){
            Set<T> set = (Set<T>) value;
            int length = set.size();
            if(length == 0){
                return ArrayUtil.newArray(clazz, 0);
            }

            T[] array = ArrayUtil.newArray(clazz, length);
            int i = 0;
            for (T t : set){
                if(ClassUtil.isAssignableFrom(clazz, t.getClass())){
                    array[i++] = t;
                }
                else{
                    throw new ClassCastException(StringUtil.format("class[{}] cannot be cast to {} for key[{}] at set [{}]",
                            t.getClass(), clazz, key, i));
                }
            }
            return array;
         }
         else if(value instanceof List){
            List<T> list = (List<T>) value;
            int length = list.size();
            if(length == 0){
                return ArrayUtil.newArray(clazz, 0);
            }
            T[] array = ArrayUtil.newArray(clazz,length);
            for (int i = 0; i < length; i++) {
                T t = list.get(i);
                if(ClassUtil.isAssignableFrom(clazz, t.getClass())){
                    array[i] = t;
                }
                else{
                    throw new ClassCastException(StringUtil.format("class[{}] cannot be cast to {} for key[{}] at list [{}]",
                            t.getClass(), clazz, key, i));
                }
            }
            return array;
         }
         else {
            throw new ClassCastException(value.getClass() +" is not list");
         }
     }

}

package jexx.collect;

import java.util.Collection;
import java.util.Set;

/**
 * 行列值,比map类型多一个key
 * @author jeff
 * @since 2019/10/25
 */
public interface Table<R, C, V> {

    /**
     * 返回table的键值数量
     * @return table元素数量
     */
    int size();

    /**
     * table是否为空
     * @return bool
     */
    boolean isEmpty();

    /**
     * 是否包含key
     * @param rowKey 行key
     * @param columnKey 列key
     * @return bool
     */
    boolean containsKey(Object rowKey, Object columnKey);

    /**
     * 是否包含行键
     * @param rowKey 行key
     * @return bool
     */
    boolean containsRowKey(Object rowKey);

    /**
     * 是否包含列键
     * @param columnKey 列key
     * @return bool
     */
    boolean containsColumnKey(Object columnKey);

    /**
     * 是否包含值
     * @param value 值
     * @return bool
     */
    boolean containsValue(Object value);

    /**
     * 根据key获取值
     * @param rowKey 行key
     * @param columnKey 列key
     * @return 值
     */
    V get(Object rowKey, Object columnKey);

    /**
     * 设置数据
     * @param rowKey 行key
     * @param columnKey 列key
     * @param value 值
     * @return 值
     */
    V put(R rowKey, C columnKey, V value);

    /**
     * 复制table数据到当前table
     * @param table table
     */
    void putAll(Table<? extends R, ? extends C, ? extends V> table);

    /**
     * 根据key移除数据
     * @param rowKey 行key
     * @param columnKey 列key
     * @return 移除后的数据
     */
    V remove(Object rowKey, Object columnKey);

    /**
     * 返回table元素迭代器
     * @return table元素迭代器
     */
    Set<Cell<R, C,V>> cellSet();

    /**
     * 值集合
     * @return 值集合
     */
    Collection<V> values();


    /**
     * 清除数据
     */
    void clear();

    /**
     * table中key，由行列组合
     * @param <R> 行key类型
     * @param <C> 列key类型
     */
    interface Key<R,C>{
        /**
         * 获取行key
         * @return 行key
         */
        R getRowKey();

        /**
         * 获取列key
         * @return 列key
         */
        C getColumnKey();
    }

    /**
     * table中最小单元
     * @param <R> 行key
     * @param <C> 列key
     * @param <V> 值
     */
    interface Cell<R, C, V> {

        /**
         * 获取key
         * @return 行key
         */
        Key<R,C> getKey();

        /**
         * 获取值
         * @return 值
         */
        V getValue();

        /**
         * 是否相同
         * @param o 比较对象
         * @return bool
         */
        @Override
        boolean equals(Object o);

        /**
         * 对象hash
         * @return hash
         */
        @Override
        int hashCode();
    }


}

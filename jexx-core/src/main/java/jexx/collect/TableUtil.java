package jexx.collect;

import jexx.util.Assert;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author jeff
 * @since 2019/10/28
 */
public class TableUtil {

    public static <R, C, V> Table<R, C, V> createTable(){
        Map<Table.Key<R, C>, V> dataMap = new HashMap<>(16);
        return new StandardTable<>(dataMap);
    }

    public static <V> Table<Integer, Integer, V> createArrayTable(int rowNum, int columnNum){
        Assert.isTrue(rowNum > 0);
        Assert.isTrue(columnNum > 0);
        return new ArrayTable<>(createList(rowNum), createList(columnNum));
    }

    private static List<Integer> createList(int num){
        List<Integer> list = new ArrayList<>();
        for(int i = 0; i < num; i++){
            list.add(i+1);
        }
        return list;
    }

}

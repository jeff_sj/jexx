package jexx.collect;

/**
 * 用于 range 的 开闭边界类型
 * @author jeff
 * @since 2019/10/23
 */
public enum BoundType {

    /**
     * 开
     */
    OPEN {
        @Override
        BoundType flip() {
            return CLOSED;
        }
    },
    /**
     * 闭
     */
    CLOSED {
        @Override
        BoundType flip() {
            return OPEN;
        }
    };

    /**
     * 根据布尔值来返回边界类型
     */
    static BoundType forBoolean(boolean inclusive) {
        return inclusive ? CLOSED : OPEN;
    }

    /**
     * 边界类型的反转值
     */
    abstract BoundType flip();

}

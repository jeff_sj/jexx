package jexx.collect;

import jexx.util.MapUtil;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

/**
 * {@link StringKeyMap} 的 实现
 * @author jeff
 * @since 2021-02-27
 */
public class LinkedStringKeyMap implements StringKeyMap{

    static final int DEFAULT_INITIAL_CAPACITY = 1 << 4;

    private final Map<String, Object> map;

    public LinkedStringKeyMap() {
        this(DEFAULT_INITIAL_CAPACITY);
    }

    public LinkedStringKeyMap(int initialCapacity) {
        if (initialCapacity < 0){
            throw new IllegalArgumentException("Illegal initial capacity: " + initialCapacity);
        }
        this.map = new LinkedHashMap<>(initialCapacity);
    }

    public LinkedStringKeyMap(Map<?, ?> m) {
        if(MapUtil.isEmpty(m)){
            this.map = new LinkedHashMap<>(DEFAULT_INITIAL_CAPACITY);
        }
        else{
            this.map = new LinkedHashMap<>(m.size());
            m.forEach((k,v)->{
                this.map.put(k.toString(), v);
            });
        }
    }

    @Override
    public int size() {
        return this.map.size();
    }

    @Override
    public boolean isEmpty() {
        return this.map.isEmpty();
    }

    @Override
    public boolean containsKey(Object key) {
        return this.map.containsKey(key);
    }

    @Override
    public boolean containsValue(Object value) {
        return this.map.containsValue(value);
    }

    @Override
    public Object get(Object key) {
        return this.map.get(key);
    }

    @Override
    public Object put(String key, Object value) {
        return this.map.put(key, value);
    }

    @Override
    public Object remove(Object key) {
        return this.map.remove(key);
    }

    @Override
    public void putAll(Map<? extends String, ?> m) {
        this.map.putAll(m);
    }

    @Override
    public void clear() {
        this.map.clear();
    }

    @Override
    public Set<String> keySet() {
        return this.map.keySet();
    }

    @Override
    public Collection<Object> values() {
        return this.map.values();
    }

    @Override
    public Set<Entry<String, Object>> entrySet() {
        return this.map.entrySet();
    }
}

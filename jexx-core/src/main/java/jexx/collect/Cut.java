package jexx.collect;

import jexx.util.StringPool;

import java.io.Serializable;
import java.util.NoSuchElementException;

/**
 * 范围边界值的边界描述
 * @author jeff
 * @since 2019/10/23
 */
abstract class Cut<C extends Comparable> implements Comparable<Cut<C>>, Serializable {

    private static final long serialVersionUID = 0;

    final C endpoint;

    Cut(C endpoint) {
        this.endpoint = endpoint;
    }

    C endpoint() {
        return endpoint;
    }

    /**
     * 是否小于指定值，主要用于判定value是否满足范围
     * <ui>
     *     <li>BelowAll: 只能为下边界，永为true 表示  c < value</li>
     *     <li>AboveAll: 只能为上边界，永为false 表示 c >= value</li>
     *     <li>BelowValue: 当为下边界时，为闭边界， 返回true表示 c <= value ; 当上边界时，为开边界, 返回false 表示 c > value</li>
     *     <li>AboveValue: 当为下边界时，为开边界， 返回true表示 c < value ; 当上边界时，为闭边界, 返回false 表示 c >= value</li>
     * </ui>
     * @param value 指定值
     * @return bool
     */
    abstract boolean isLessThan(C value);

    /**
     * 作为下边界时的边界类型
     * @return 边界类型
     */
    abstract BoundType typeAsLowerBound();

    /**
     * 作为上边界时的边界类型
     * @return 边界类型
     */
    abstract BoundType typeAsUpperBound();

    /**
     * 作为下边界时的描述
     * @param sb 字符串构造
     */
    abstract void describeAsLowerBound(StringBuilder sb);

    /**
     * 作为上边界时的描述
     * @param sb 字符串构造
     */
    abstract void describeAsUpperBound(StringBuilder sb);

    /**
     * 标准化边界,组成开闭区间;
     * <p>
     *     如下等价:
     *     <pre>
     *         (1,3)=[2,3)
     *         (1,3]=[2,4)
     *         (-∞,3)=[-2147483648,3)
     *         (-∞,3]=[-2147483648,4)
     *     </pre>
     * </p>
     * @param domain 离散域
     * @return  边界
     */
    Cut<C> canonical(DiscreteDomain<C> domain) {
        return this;
    }

    @Override
    public int compareTo(Cut<C> that) {
        if(that == belowAll()){
            return 1;
        }
        if(that == aboveAll()){
            return -1;
        }
        int result = compareOrThrow(endpoint, that.endpoint);
        if(result != 0){
            return result;
        }
        return Boolean.compare(this instanceof AboveValue, that instanceof AboveValue);
    }

    @SuppressWarnings("unchecked")
    static int compareOrThrow(Comparable left, Comparable right) {
        return left.compareTo(right);
    }

    @SuppressWarnings("unchecked")
    @Override
    public boolean equals(Object obj) {
        if(obj instanceof Cut) {
            Cut<C> that = (Cut<C>) obj;
            try {
                int compareResult = compareTo(that);
                return compareResult == 0;
            } catch (ClassCastException ignored) {
            }
        }
        return false;
    }

    @SuppressWarnings("unchecked")
    static <C extends Comparable> Cut<C> belowAll(){
        return (Cut<C>)BelowAll.INSTANCE;
    }

    @SuppressWarnings("unchecked")
    static <C extends Comparable> Cut<C> aboveAll(){
        return (Cut<C>)AboveAll.INSTANCE;
    }

    static <C extends Comparable> Cut<C> belowValue(C endpoint) {
        return new BelowValue<>(endpoint);
    }

    static <C extends Comparable> Cut<C> aboveValue(C endpoint) {
        return new AboveValue<>(endpoint);
    }


    /**
     * 负无穷
     */
    private static final class BelowAll extends Cut<Comparable<?>> {

        private static final BelowAll INSTANCE = new BelowAll();

        public BelowAll() {
            super(null);
        }

        @Override
        boolean isLessThan(Comparable<?> value) {
            return true;
        }

        @Override
        BoundType typeAsLowerBound() {
            throw new IllegalStateException();
        }

        @Override
        BoundType typeAsUpperBound() {
            throw new IllegalStateException();
        }

        @Override
        void describeAsLowerBound(StringBuilder sb) {
            sb.append("(-").append(StringPool.INFINITY);
        }

        @Override
        void describeAsUpperBound(StringBuilder sb) {
            throw new AssertionError();
        }

        @Override
        public int compareTo(Cut<Comparable<?>> that) {
            return that == this ? 0 : -1;
        }

        @Override
        Cut<Comparable<?>> canonical(DiscreteDomain<Comparable<?>> domain) {
            try {
                return Cut.belowValue(domain.minValue());
            } catch (NoSuchElementException e) {
                return this;
            }
        }
    }

    /**
     * 正无穷
     */
    private static final class AboveAll extends Cut<Comparable<?>> {

        private static final AboveAll INSTANCE = new AboveAll();

        public AboveAll() {
            super(null);
        }

        @Override
        boolean isLessThan(Comparable<?> value) {
            return false;
        }

        @Override
        BoundType typeAsLowerBound() {
            throw new IllegalStateException();
        }

        @Override
        BoundType typeAsUpperBound() {
            throw new IllegalStateException();
        }

        @Override
        void describeAsLowerBound(StringBuilder sb) {
            throw new AssertionError();
        }

        @Override
        void describeAsUpperBound(StringBuilder sb) {
            sb.append("+").append(StringPool.INFINITY).append(")");
        }

        @Override
        public int compareTo(Cut<Comparable<?>> that) {
            return that == this ? 0 : 1;
        }
    }

    /**
     * 在指定值下
     */
    private static final class BelowValue<C extends Comparable> extends Cut<C> {

        public BelowValue(C endpoint) {
            super(endpoint);
        }

        @Override
        boolean isLessThan(C value) {
            return compareOrThrow(this.endpoint, value) <= 0;
        }

        @Override
        BoundType typeAsLowerBound() {
            return BoundType.CLOSED;
        }

        @Override
        BoundType typeAsUpperBound() {
            return BoundType.OPEN;
        }

        @Override
        void describeAsLowerBound(StringBuilder sb) {
            sb.append("[").append(endpoint);
        }

        @Override
        void describeAsUpperBound(StringBuilder sb) {
            sb.append(endpoint).append(")");
        }

    }

    /**
     * 在指定值上
     */
    private static final class AboveValue<C extends Comparable> extends Cut<C> {

        public AboveValue(C endpoint) {
            super(endpoint);
        }

        @SuppressWarnings("unchecked")
        @Override
        boolean isLessThan(C value) {
            return compareOrThrow(this.endpoint, value) < 0;
        }

        @Override
        BoundType typeAsLowerBound() {
            return BoundType.OPEN;
        }

        @Override
        BoundType typeAsUpperBound() {
            return BoundType.CLOSED;
        }

        @Override
        void describeAsLowerBound(StringBuilder sb) {
            sb.append("(").append(endpoint);
        }

        @Override
        void describeAsUpperBound(StringBuilder sb) {
            sb.append(endpoint).append("]");
        }

        @Override
        Cut<C> canonical(DiscreteDomain<C> domain) {
            C next = domain.next(endpoint);
            return (next != null) ? belowValue(next) : Cut.aboveAll();
        }
    }


}

package jexx.collect;

import java.io.Serializable;
import java.util.NoSuchElementException;

/**
 * 离散域,用于表达不连续的值集合
 * @author jeff
 * @since 2019/10/24
 */
public abstract class DiscreteDomain <C extends Comparable> {

    protected DiscreteDomain() {}

    /**
     * short离散域
     */
    public static DiscreteDomain<Short> shorts() {
        return ShortDomain.INSTANCE;
    }

    /**
     * int离散域
     */
    public static DiscreteDomain<Integer> integers() {
        return IntegerDomain.INSTANCE;
    }

    /**
     * long离散域
     */
    public static DiscreteDomain<Long> longs() {
        return LongDomain.INSTANCE;
    }

    /**
     * char离散域
     */
    public static DiscreteDomain<Character> chars() {
        return CharacterDomain.INSTANCE;
    }

    /**
     * 指定值的下一个离散值
     * @param value 指定值
     * @return 离散值
     */
    public abstract C next(C value);

    /**
     * 指定值的上一个离散值
     * @param value 指定值
     * @return 离散值
     */
    public abstract C previous(C value);

    /**
     * 离散值域的最大值
     * @return 最大值
     */
    public C maxValue() {
        throw new NoSuchElementException();
    }

    /**
     * 离散值域的最小值
     * @return 最小值
     */
    public C minValue() {
        throw new NoSuchElementException();
    }

    private static final class LongDomain extends DiscreteDomain<Long> implements Serializable {

        private static final LongDomain INSTANCE = new LongDomain();

        @Override
        public Long next(Long value) {
            long v = value;
            return v == Long.MAX_VALUE ? null : (v + 1);
        }

        @Override
        public Long previous(Long value) {
            long v = value;
            return v == Long.MIN_VALUE ? null : (v - 1);
        }

        @Override
        public Long maxValue() {
            return Long.MAX_VALUE;
        }

        @Override
        public Long minValue() {
            return Long.MIN_VALUE;
        }

    }

    private static final class IntegerDomain extends DiscreteDomain<Integer> implements Serializable {

        private static final IntegerDomain INSTANCE = new IntegerDomain();

        @Override
        public Integer next(Integer value) {
            int v = value;
            return (v == Integer.MAX_VALUE) ? null : v + 1;
        }

        @Override
        public Integer previous(Integer value) {
            int v = value;
            return (v == Integer.MIN_VALUE) ? null : v - 1;
        }

        @Override
        public Integer maxValue() {
            return Integer.MAX_VALUE;
        }

        @Override
        public Integer minValue() {
            return Integer.MIN_VALUE;
        }

    }

    private static final class ShortDomain extends DiscreteDomain<Short> implements Serializable {

        private static final ShortDomain INSTANCE = new ShortDomain();

        @Override
        public Short next(Short value) {
            short v = value;
            return (v == Short.MAX_VALUE) ? null : (short)(v + 1);
        }

        @Override
        public Short previous(Short value) {
            short v = value;
            return (v == Short.MIN_VALUE) ? null : (short)(v - 1);
        }

        @Override
        public Short maxValue() {
            return Short.MAX_VALUE;
        }

        @Override
        public Short minValue() {
            return Short.MIN_VALUE;
        }

    }

    private static final class CharacterDomain extends DiscreteDomain<Character> implements Serializable {

        private static final CharacterDomain INSTANCE = new CharacterDomain();

        @Override
        public Character next(Character value) {
            char v = value;
            return (v == Character.MAX_VALUE) ? null : (char)(v + 1);
        }

        @Override
        public Character previous(Character value) {
            char v = value;
            return (v == Character.MIN_VALUE) ? null : (char)(v - 1);
        }

        @Override
        public Character maxValue() {
            return Character.MAX_VALUE;
        }

        @Override
        public Character minValue() {
            return Character.MIN_VALUE;
        }

    }



}

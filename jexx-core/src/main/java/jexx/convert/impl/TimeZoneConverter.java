package jexx.convert.impl;

import jexx.convert.AbstractConverter;

import java.util.TimeZone;

/**
 * TimeZone转换器
 *
 */
public class TimeZoneConverter extends AbstractConverter<TimeZone> {

	@Override
	protected TimeZone convertInternal(Object value) {
		return TimeZone.getTimeZone(convertToStr(value));
	}

}

package jexx.convert.impl;

import jexx.convert.AbstractConverter;
import jexx.util.ClassLoaderUtil;

/**
 * 类转换器<br>
 * 将类名转换为类
 */
public class ClassConverter extends AbstractConverter<Class<?>> {
	
	@Override
	protected Class<?> convertInternal(Object value) {
		String valueStr = convertToStr(value);
		try {
			return ClassLoaderUtil.getClassLoader().loadClass(valueStr);
		} catch (Exception e) {
			// Ignore Exception
		}
		return null;
	}

}

package jexx.convert.impl;

import jexx.convert.AbstractConverter;
import jexx.lang.Charsets;

import java.nio.charset.Charset;

/**
 * 编码对象转换器
 * @author Looly
 *
 */
public class CharsetConverter extends AbstractConverter<Charset> {

	@Override
	protected Charset convertInternal(Object value) {
		return Charsets.toCharset(convertToStr(value));
	}

}

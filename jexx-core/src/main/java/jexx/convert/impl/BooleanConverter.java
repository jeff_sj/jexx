package jexx.convert.impl;

import jexx.convert.AbstractConverter;

/**
 * 波尔转换器
 */
public class BooleanConverter extends AbstractConverter<Boolean> {

	@Override
	protected Boolean convertInternal(Object value) {
		if(boolean.class == value.getClass()){
			return (boolean)value;
		}
		String valueStr = convertToStr(value);
		return PrimitiveConverter.parseBoolean(valueStr);
	}

}

package jexx.convert.impl;

import jexx.convert.AbstractConverter;
import jexx.convert.ConverterRegistry;

/**
 * byte 类型数组转换器
 */
public class ByteArrayConverter extends AbstractConverter<byte[]> {
	
	@Override
	protected byte[] convertInternal(Object value) {
		return ConverterRegistry.getInstance().convert(byte[].class, value);
	}

}

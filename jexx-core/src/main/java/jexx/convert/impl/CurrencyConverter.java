package jexx.convert.impl;

import jexx.convert.AbstractConverter;

import java.util.Currency;

/**
 * 货币{@link Currency} 转换器
 */
public class CurrencyConverter extends AbstractConverter<Currency> {

	@Override
	protected Currency convertInternal(Object value) {
		return Currency.getInstance(value.toString());
	}

}

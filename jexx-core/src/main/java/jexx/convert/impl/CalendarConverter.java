package jexx.convert.impl;

import jexx.convert.AbstractConverter;
import jexx.time.DatePattern;
import jexx.time.DateUtil;
import jexx.util.StringUtil;

import java.util.Calendar;
import java.util.Date;

/**
 * 日期转换器
 */
public class CalendarConverter extends AbstractConverter<Calendar> {

	/** 日期格式化 */
	private String format;

	/**
	 * 获取日期格式
	 * 
	 * @return 设置日期格式
	 */
	public String getFormat() {
		return format;
	}

	/**
	 * 设置日期格式
	 * 
	 * @param format 日期格式
	 */
	public void setFormat(String format) {
		this.format = format;
	}

	@Override
	protected Calendar convertInternal(Object value) {
        Calendar calendar = Calendar.getInstance();
		// Handle Date
		if (value instanceof Date) {
			calendar.setTime((Date)value);
			return calendar;
		}

		// Handle Long
		if (value instanceof Long) {
			calendar.setTimeInMillis((long)value);
			return calendar;
		}

		final String valueStr = convertToStr(value);
		Date date = StringUtil.isBlank(format)?DateUtil.parse(valueStr):DateUtil.parse(valueStr,format);
        calendar.setTime(date);
		return calendar;
	}

}

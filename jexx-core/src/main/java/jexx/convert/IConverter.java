package jexx.convert;

public interface IConverter<T> {

    T convert(Object value, T defaultValue) throws IllegalArgumentException;

}

package jexx.base;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 基本变量类型的枚举<br>
 * 基本类型枚举包括原始类型和包装类型
 *
 * @author jeff
 */
public enum BasicType {
	/** byte */
	BYTE,
    /** short */
    SHORT,
    /** int */
    INT,
    /** INTEGER */
    INTEGER,
    /** LONG */
    LONG,
    /** DOUBLE */
    DOUBLE,
    /** FLOAT */
    FLOAT,
    /** BOOLEAN */
    BOOLEAN,
    /** CHAR */
    CHAR,
    /** CHARACTER */
    CHARACTER,
    /** STRING */
    STRING;
	
	/** 包装类型为Key，原始类型为Value，例如： Integer.class =》 int.class. */
	private static final Map<Class<?>, Class<?>> wrapperPrimitiveMap = new ConcurrentHashMap<>(8);
	/** 原始类型为Key，包装类型为Value，例如： int.class =》 Integer.class. */
	public static final Map<Class<?>, Class<?>> primitiveWrapperMap = new ConcurrentHashMap<>(8);
	/**
	 * 获取类名到 class 的映射
	 */
	private static final Map<String, Class<?>> primitiveTypeNameMap = new HashMap<>(16);
	
	static {
		wrapperPrimitiveMap.put(Boolean.class, boolean.class);
		wrapperPrimitiveMap.put(Byte.class, byte.class);
		wrapperPrimitiveMap.put(Character.class, char.class);
		wrapperPrimitiveMap.put(Double.class, double.class);
		wrapperPrimitiveMap.put(Float.class, float.class);
		wrapperPrimitiveMap.put(Integer.class, int.class);
		wrapperPrimitiveMap.put(Long.class, long.class);
		wrapperPrimitiveMap.put(Short.class, short.class);
		wrapperPrimitiveMap.put(Void.class, void.class);

		for (Map.Entry<Class<?>, Class<?>> entry : wrapperPrimitiveMap.entrySet()) {
			primitiveWrapperMap.put(entry.getValue(), entry.getKey());
		}

		Set<Class<?>> primitiveTypes = new HashSet<>(32);
		primitiveTypes.addAll(wrapperPrimitiveMap.values());
		Collections.addAll(primitiveTypes, boolean[].class, byte[].class, char[].class,
				double[].class, float[].class, int[].class, long[].class, short[].class);
		for (Class<?> primitiveType : primitiveTypes) {
			primitiveTypeNameMap.put(primitiveType.getName(), primitiveType);
		}

	}

	/**
	 * 是否为 包装基本原始类型
	 */
	public static boolean isWrapperPrimitiveClass(Class<?> clazz){
		return wrapperPrimitiveMap.containsKey(clazz);
	}

	/**
	 * 是否为 基本类型
	 */
	public static boolean isPrimitiveClass(Class<?> clazz){
		return primitiveWrapperMap.containsKey(clazz);
	}

	/**
	 * 获取名字对应的原始类型
	 * @param name 类名
	 * @return class
	 */
	public static Class<?> getPrimitiveTypeByClassname(String name){
		return primitiveTypeNameMap.get(name);
	}


	/**
	 * 原始类转为包装类，非原始类返回原类
	 * @param clazz 原始类
	 * @return 包装类
	 */
	public static Class<?> wrap(Class<?> clazz){
		if(null == clazz || !clazz.isPrimitive()){
			return clazz;
		}
		Class<?> result = primitiveWrapperMap.get(clazz);
		return (null == result) ? clazz : result;
	}
	
	/**
	 * 包装类转为原始类，非包装类返回原类
	 * @param clazz 包装类
	 * @return 原始类
	 */
	public static Class<?> unWrap(Class<?> clazz){
		if(null == clazz || clazz.isPrimitive()){
			return clazz;
		}
		Class<?> result = wrapperPrimitiveMap.get(clazz);
		return (null == result) ? clazz : result;
	}
}

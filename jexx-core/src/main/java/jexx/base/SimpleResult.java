package jexx.base;

/**
 * 简单结果, 常用于方法返回一个行为执行后的结果内容
 */
public class SimpleResult<T> {

    /**
     * 是否成功
     */
    private final boolean success;
    /**
     * 提示信息
     */
    private final String message;
    /**
     * 返回结果
     */
    private final T data;

    public SimpleResult(boolean success, String message, T data) {
        this.success = success;
        this.message = message;
        this.data = data;
    }

    public static <T> SimpleResult<T> success(String message, T data){
        return new SimpleResult<>(true, message, data);
    }

    public static <T> SimpleResult<T> success(String message){
        return new SimpleResult<>(true, message, null);
    }

    public static <T> SimpleResult<T> fail(String message, T data){
        return new SimpleResult<>(false, message, data);
    }

    public static <T> SimpleResult<T> fail(String message){
        return new SimpleResult<>(false, message, null);
    }

    public boolean isSuccess() {
        return success;
    }

    public String getMessage() {
        return message;
    }

    public T getData() {
        return data;
    }
}

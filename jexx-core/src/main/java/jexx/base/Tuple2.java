package jexx.base;

/**
 * 二维元祖
 * @param <F> 第一个元素
 * @param <S> 第二个元素
 */
public class Tuple2<F, S> {

    private F first;
    private S second;

    public Tuple2(F first, S second) {
        this.first = first;
        this.second = second;
    }

    public static <F, S> Tuple2<F, S> of(F f, S s){
        return new Tuple2<>(f, s);
    }

    public F getFirst() {
        return first;
    }

    public void setFirst(F first) {
        this.first = first;
    }

    public S getSecond() {
        return second;
    }

    public void setSecond(S second) {
        this.second = second;
    }
}

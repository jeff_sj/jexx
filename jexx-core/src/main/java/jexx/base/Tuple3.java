package jexx.base;

/**
 * 三维元祖
 * @param <F> 第一个元素
 * @param <S> 第二个元素
 * @param <T> 第三个元素
 */
public class Tuple3<F, S, T> {

    private F first;
    private S second;
    private T third;

    public Tuple3(F first, S second, T third) {
        this.first = first;
        this.second = second;
        this.third = third;
    }

    public static <F, S, T> Tuple3<F, S, T> of(F first, S second, T third){
        return new Tuple3<>(first, second, third);
    }

    public F getFirst() {
        return first;
    }

    public void setFirst(F first) {
        this.first = first;
    }

    public S getSecond() {
        return second;
    }

    public void setSecond(S second) {
        this.second = second;
    }

    public T getThird() {
        return third;
    }

    public void setThird(T third) {
        this.third = third;
    }
}

# 使用说明

## 解压缩
ZipFile是对压缩对象的一个封装。利用该类，可以对压缩中的条目进行移动,删除,复制; 也可以添加文件或者目录到该对象中。
```
//构建ZipFile对象来进行压缩
ZipFile zipFile1 = new ZipFile("D:/compress/c.zip");
//添加条目
zipFile1.append(ResourceUtil.getResource("compress/a.txt"), "a.txt");
//复制条目
zipFile1.copy("a.txt", "dir/a.txt");
//删除条目
zipFile1.delete("a.txt");
//移动条目
zipFile1.move("c.txt", "dir/c.txt");
//添加目录
zipFile1.appendDir("dir1/");
//最后压缩到某文件
zipFile1.zip(new File("D:/compress/c1.zip"));

//构建ZipFile对象来压缩目录
ZipFile zipFile = new ZipFile();
//添加目录及子文件
zipFile.appendDirAndChildren("D:/test/20190125", s -> !s.getName().startsWith("4"));
zipFile.zip("D:/test/20190125/a.zip");

//构建ZipFile对象来解压
ZipFile zipFile2 = new ZipFile(new File("D:/compress/c1.zip"));
String testDir = fileDir.concat("/unzip");
zipFile2.unzip("D:/compress/c2);
```
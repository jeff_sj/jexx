package jexx.template.excel;

import jexx.exception.IORuntimeException;
import jexx.io.FastByteArrayOutputStream;
import jexx.io.FileUtil;
import jexx.io.IOUtil;
import jexx.poi.ExcelWriter;
import jexx.template.MagicTemplate;
import org.apache.poi.ss.usermodel.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;

public class MagicExcelTemplate extends MagicTemplate {

    private static final Logger LOG = LoggerFactory.getLogger(MagicExcelTemplate.class);

    private Workbook workbook;

    public MagicExcelTemplate(Workbook workbook) {
        this.workbook = workbook;
    }

    public MagicExcelTemplate(InputStream inputStream) {
        try {
            this.workbook = WorkbookFactory.create(inputStream);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        finally {
            IOUtil.closeQuietly(inputStream);
        }
    }

    public MagicExcelTemplate(File file) {
        this(FileUtil.newBufferedInputStream(file));
    }

    @Override
    public void output(OutputStream outputStream){
        try {
            int i,j,k;
            Row row;
            Cell cell;
            for(i = 0; i < workbook.getNumberOfSheets(); i++){
                Sheet sheet = workbook.getSheetAt(i);
                if(sheet.getLastRowNum() <= 0){
                    continue;
                }
                for(j = 0; j <= sheet.getLastRowNum(); j++){
                    row = sheet.getRow(j);
                    for(k = 0; k < row.getLastCellNum(); k++){
                        cell = row.getCell(k);
                        if(cell != null){
                            replaceCell(cell);
                        }
                    }
                }
            }
            workbook.write(outputStream);
            outputStream.flush();
        }
        catch (IOException e){
            throw new IORuntimeException(e);
        }
    }

    protected void replaceCell(Cell cell){
        if(cell.getCellTypeEnum() == CellType.STRING){
            String text = cell.getStringCellValue();
            String replace = parse(text);
            cell.setCellValue(replace);
        }
    }

    /**
     * 当前Workbook转化为excel
     */
    public ExcelWriter toExcelWriter(){
        try(FastByteArrayOutputStream outputStream = new FastByteArrayOutputStream()){
            output(outputStream);
            ByteArrayInputStream inputStream = new ByteArrayInputStream(outputStream.toByteArray());
            return new ExcelWriter(inputStream, true);
        }
    }

    @Override
    public void doClose() {
        IOUtil.closeQuietly(workbook);
    }
}

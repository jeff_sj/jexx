package jexx.template;

import jexx.bean.BeanUtil;
import jexx.io.FileUtil;
import jexx.io.IOUtil;
import jexx.template.property.ITemplateProperty;
import jexx.template.property.impl.DefaultTemplateProperty;
import jexx.util.MapUtil;
import org.apache.poi.POITextExtractor;
import org.apache.poi.extractor.ExtractorFactory;
import org.apache.poi.openxml4j.opc.OPCPackage;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

public abstract class MagicTemplate implements Closeable {

    protected boolean debug;

    public static final String DEFAULT_MACRO_START = "${";
    public static final String DEFAULT_MACRO_END = "}";

    protected String macroStart = DEFAULT_MACRO_START;
    protected String macroEnd = DEFAULT_MACRO_END;
    protected char escapeChar = '\\';
    private StringTemplateParser parser;

    protected Map<String, ITemplateProperty> propertyMap = new HashMap<>();

    public void put(String name, Object value){
        ITemplateProperty property = new DefaultTemplateProperty(name, value);
        propertyMap.put(name, property);
    }

    public void put(ITemplateProperty property){
        propertyMap.put(property.getName(), property);
    }

    public void putMap(Map<String, Object> attributes){
        if(MapUtil.isNotEmpty(attributes)){
            attributes.forEach(this::put);
        }
    }

    public void putBean(Object bean){
        putMap(BeanUtil.toMap(bean));
    }

    protected String parse(String text){
        if(parser == null){
            parser = new StringTemplateParser();
            parser.setMacroStart(macroStart);
            parser.setMacroPrefix(macroStart.substring(0, 1));
            parser.setMacroEnd(macroEnd);
        }
        return parser.parse(text, s -> {
            ITemplateProperty property = propertyMap.get(s);
            if(property instanceof  DefaultTemplateProperty){
                DefaultTemplateProperty defaultTemplateProperty = (DefaultTemplateProperty)property;
                return defaultTemplateProperty.getValue() != null ? defaultTemplateProperty.getValue().toString() : null;
            }
            return null;
        });
    }

    public abstract void output(OutputStream outputStream);

    public void outputFile(File file){
        output(FileUtil.newBufferedOutputStream(file));
    }

    public String getText(){
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        output(outputStream);

        byte[] buffer = outputStream.toByteArray();
        ByteArrayInputStream inputStream = new ByteArrayInputStream(buffer);

        POITextExtractor extractor = null;
        try {
            extractor = ExtractorFactory.createExtractor(OPCPackage.open(inputStream));
            String text = extractor.getText();
            extractor.close();
            return text;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        finally {
            IOUtil.closeQuietly(extractor);
        }
    }

    public void setDebug(boolean debug) {
        this.debug = debug;
    }



    @Override
    public void close() {
        doClose();
    }

    protected abstract void doClose();

}

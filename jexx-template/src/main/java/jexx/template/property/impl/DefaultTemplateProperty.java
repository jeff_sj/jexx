package jexx.template.property.impl;

import jexx.template.property.ITemplateProperty;
import jexx.template.property.TemplatePropertyType;

public class DefaultTemplateProperty implements ITemplateProperty {

    private String name;
    private Object value;

    public DefaultTemplateProperty(String name, Object value) {
        this.name = name;
        this.value = value;
    }

    @Override
    public String getName() {
        return name;
    }

    public Object getValue() {
        return value;
    }

    @Override
    public TemplatePropertyType getType() {
        return TemplatePropertyType.DEFAULT;
    }
}

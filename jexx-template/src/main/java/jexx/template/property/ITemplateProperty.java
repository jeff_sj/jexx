package jexx.template.property;

public interface ITemplateProperty {

    String getName();

    TemplatePropertyType getType();



}

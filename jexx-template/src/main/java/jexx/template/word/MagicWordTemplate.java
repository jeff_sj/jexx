package jexx.template.word;

import jexx.exception.IORuntimeException;
import jexx.io.FileUtil;
import jexx.io.IOUtil;
import jexx.template.MagicTemplate;
import jexx.util.ResourceUtil;
import org.apache.poi.POITextExtractor;
import org.apache.poi.POIXMLDocument;
import org.apache.poi.POIXMLTextExtractor;
import org.apache.poi.extractor.ExtractorFactory;
import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.xwpf.extractor.XWPFWordExtractor;
import org.apache.poi.xwpf.usermodel.*;
import org.apache.xmlbeans.XmlException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.*;

/**
 * 07版本以上的word模板替换
 */
public class MagicWordTemplate extends MagicTemplate {

    private static final Logger LOG = LoggerFactory.getLogger(MagicWordTemplate.class);

    private XWPFDocument document;

    public MagicWordTemplate(InputStream inputStream) {
        try {
            this.document = new XWPFDocument(inputStream);
        } catch (IOException e) {
            throw new IORuntimeException(e);
        }
    }

    public MagicWordTemplate(File file) {
        this(FileUtil.newBufferedInputStream(file));
    }

    @Override
    public void output(OutputStream outputStream){
        try {
            // 替换段落中的变量
            Iterator<XWPFParagraph> itPara = document.getParagraphsIterator();
            while (itPara.hasNext()) {
                XWPFParagraph paragraph = itPara.next();
                replaceParagraph(paragraph);
            }

            // 替换表格中的变量
            Iterator<XWPFTable> itTable = document.getTablesIterator();
            while (itTable.hasNext()) {
                XWPFTable table = itTable.next();
                int rowCount = table.getNumberOfRows();
                for (int i = 0; i < rowCount; i++) {
                    XWPFTableRow row = table.getRow(i);
                    List<XWPFTableCell> cells = row.getTableCells();
                    for (XWPFTableCell cell : cells) {
                        for(XWPFParagraph paragraph : cell.getParagraphs()){
                            replaceParagraph(paragraph);
                        }
                    }
                }
            }

            document.write(outputStream);
        }
        catch (IOException e){
            throw new IORuntimeException(e);
        }
    }

    protected void replaceParagraph(XWPFParagraph paragraph){
        if(debug){
            LOG.info("=====================Before optimize XWPFParagraph");
            printParagraph(paragraph);
        }

        int macroStartPos = 0;
        int beginRunPos = 0;
        int beginCharPos = 0;

        boolean hasMacroStart = false;

        int macroEndPos = 0;

        boolean clear = false;
        boolean skipLetter = false;

        List<TextSegement> segments = new ArrayList<>();
        List<XWPFRun> waitReplaceRuns = new ArrayList<>();

        List<XWPFRun> runs = paragraph.getRuns();
        for(int i = 0; i < runs.size(); i++){
            XWPFRun run = runs.get(i);
            String runText = run.getText(0);

            for(int charPos = 0; charPos < runText.length(); charPos++){
                char c = runText.charAt(charPos);
                if(skipLetter){
                    skipLetter = false;
                    continue;
                }

                if(hasMacroStart){
                    if(c == macroEnd.charAt(macroEndPos)){
                        if(macroEndPos + 1 < macroEnd.length()){
                            macroEndPos++;
                        }
                        else{
                            TextSegement segment = new TextSegement();
                            segment.setBeginRun(beginRunPos);
                            segment.setBeginChar(beginCharPos);
                            segment.setEndRun(i);
                            segment.setEndChar(charPos);
                            segments.add(segment);

                            clear = true;
                        }
                    }
                    else{
                        if(macroEndPos > 0){
                            clear = true;
                        }
                    }
                }
                //
                else{
                    if(c == macroStart.charAt(macroStartPos)){
                        if(macroStartPos == 0){
                            beginRunPos = i;
                            beginCharPos = charPos;
                        }

                        if(macroStartPos + 1 < macroStart.length()){
                            macroStartPos++;
                        }
                        else{
                            hasMacroStart = true;
                        }
                    }
                    else{
                        if(macroStartPos > 0){
                            clear = true;
                        }
                        else if(c == escapeChar){
                            skipLetter = true;
                        }
                    }
                }

                if(clear){
                    clear = false;
                    macroStartPos = 0;
                    beginRunPos = 0;
                    beginCharPos = 0;
                    hasMacroStart = false;
                    macroEndPos = 0;
                }
            }
        }

        //优化
        for(int i = segments.size()-1; i >= 0; i--){
            TextSegement segment = segments.get(i);
            String text = paragraph.getText(segment);

            if(segment.getBeginRun() == segment.getEndRun()){
                waitReplaceRuns.add(runs.get(segment.getBeginRun()));
                continue;
            }

            if(segment.getBeginRun() < segment.getEndRun()){
                XWPFRun startRun = runs.get(segment.getBeginRun());
                startRun.setText(text, segment.getBeginChar());

                XWPFRun endRun = runs.get(segment.getEndRun());
                if(segment.getEndRun() < endRun.getTextPosition()){
                    endRun.setText(endRun.getText(segment.getEndChar()), segment.getEndChar());
                }
                else{
                    endRun.setText("", 0);
                }

                int pos = segment.getBeginRun() + 1;
                while(pos < segment.getEndRun()){
                    runs.get(pos).setText("", 0);
                    pos++;
                }

                waitReplaceRuns.add(startRun);
            }
        }

        if(debug){
            LOG.info("=====================After optimize XWPFParagraph");
            printParagraph(paragraph);
        }

        //处理
        for(XWPFRun waitReplaceRun : waitReplaceRuns){
            replaceRun(paragraph, waitReplaceRun);
        }

        if(debug){
            LOG.info("=====================Finally");
            printParagraph(paragraph);
        }
    }

    protected void printParagraph(XWPFParagraph paragraph){
        List<XWPFRun> runs = paragraph.getRuns();

        for(int i = 0; i < runs.size(); i++){
            XWPFRun run = runs.get(i);
            LOG.info("{}===>{}", i, run.getText(0));
        }
    }

    protected void replaceRun(XWPFParagraph paragraph, XWPFRun run){
        String text = run.getText(0);
        String replace = parse(text);
        run.setText(replace, 0);
    }

    @Override
    public void doClose() {
        IOUtil.closeQuietly(document);
    }
}

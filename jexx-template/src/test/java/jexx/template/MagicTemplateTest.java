package jexx.template;

import jexx.crypto.DigestUtil;
import jexx.io.FileUtil;
import jexx.poi.ExcelWriter;
import jexx.poi.SheetWriter;
import jexx.poi.style.CellStyleMapper;
import jexx.template.excel.MagicExcelTemplate;
import jexx.template.property.impl.DefaultTemplateProperty;
import jexx.template.word.MagicWordTemplate;
import jexx.time.DateUtil;
import jexx.util.CollectionUtil;
import jexx.util.ResourceUtil;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import java.io.File;
import java.util.Date;

public class MagicTemplateTest {

    @Test
    public void testWord(){
        MagicTemplate template = new MagicWordTemplate(ResourceUtil.getStream("test.docx"));
        template.setDebug(true);

        A a = new A();
        a.setName("小明");
        a.setAge(26);
        a.setBirthdate(DateUtil.parse("1988-12-12"));
        a.setFriendName("小李");
        a.setFriendAge(22);
        template.putBean(a);

        template.put(new DefaultTemplateProperty("x1", "xxx"));

        String text = template.getText();
        Assert.assertEquals("29b4a76ca46fc160927481637b530e74", DigestUtil.md5Hex(text));
//        template.outputFile(new File("C:\\Users\\kxys4\\Desktop\\test1.docx"));
        template.close();
    }

    @Test
    public void testExcel(){
        MagicTemplate template = new MagicExcelTemplate(ResourceUtil.getStream("test.xlsx"));

        A a = new A();
        a.setName("小明");
        a.setAge(26);
        a.setBirthdate(DateUtil.parse("1988-12-12"));
        a.setFriendName("小李");
        a.setFriendAge(22);
        template.putBean(a);

        template.put(new DefaultTemplateProperty("x1", "xxx"));

        String text = template.getText();
        Assert.assertEquals("dad9ca489cee4ac3d1ad5027a4778c42", DigestUtil.md5Hex(text));
//        template.outputFile(new File("C:\\Users\\kxys4\\Desktop\\test1.xlsx"));
        template.close();
    }

    @Test
    @Ignore
    public void testToExcelWriter() {
        MagicExcelTemplate template = new MagicExcelTemplate(ResourceUtil.getStream("test.xlsx"));

        A a = new A();
        a.setName("小明");
        a.setAge(26);
        a.setBirthdate(DateUtil.parse("1988-12-12"));
        a.setFriendName("小李");
        a.setFriendAge(22);
        template.putBean(a);

        template.put(new DefaultTemplateProperty("x1", "xxx"));

        ExcelWriter writer = template.toExcelWriter();
        SheetWriter sheetWriter = writer.sheet("Sheet1");
        sheetWriter.skipRows(3);
        sheetWriter.writeRows(CollectionUtil.list(a), (CellStyleMapper)null);
        writer.flush(new File("C:/Users/kxys4/Desktop/a.xlsx"));
        writer.close();
    }

    private static class A{
        private String name;
        private int age;
        private Date birthdate;
        private String friendName;
        private int friendAge;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getAge() {
            return age;
        }

        public void setAge(int age) {
            this.age = age;
        }

        public Date getBirthdate() {
            return birthdate;
        }

        public void setBirthdate(Date birthdate) {
            this.birthdate = birthdate;
        }

        public String getFriendName() {
            return friendName;
        }

        public void setFriendName(String friendName) {
            this.friendName = friendName;
        }

        public int getFriendAge() {
            return friendAge;
        }

        public void setFriendAge(int friendAge) {
            this.friendAge = friendAge;
        }
    }

}

package jexx.csv;

import java.io.IOException;

/**
 * @author jeff
 * @since 2019/10/10
 */
public interface CsvWriterBuilder {

    CsvWriterBuilder setTextDelimiter(char textDelimiter);

    CsvWriterBuilder setAlwaysDelimitText(boolean alwaysDelimitText);

    CsvWriterBuilder setLineDelimiter(char[] lineDelimiter);

    CsvWriterBuilder setFieldSeparator(char fieldSeparator);

    CsvWriter build() throws IOException;

}

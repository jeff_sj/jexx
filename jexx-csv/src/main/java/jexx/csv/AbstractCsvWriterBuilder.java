package jexx.csv;

/**
 * CsvWriter 的工厂类
 * @author jeff
 * @since 2019/10/8
 */
public abstract class AbstractCsvWriterBuilder implements CsvWriterBuilder {

    /**
     * 字段分割符, 默认 ','
     */
    protected char fieldSeparator = ',';

    /**
     * 获取文本包装符, 默认 '"'
     */
    protected char textDelimiter = '"';

    /**
     * 字段是否始终使用文本包装符, 默认false
     */
    protected boolean alwaysDelimitText;

    /**
     * 换行符, 默认系统换行符
     */
    protected char[] lineDelimiter = System.lineSeparator().toCharArray();

    @Override
    public AbstractCsvWriterBuilder setTextDelimiter(char textDelimiter) {
        this.textDelimiter = textDelimiter;
        return this;
    }

    @Override
    public AbstractCsvWriterBuilder setAlwaysDelimitText(boolean alwaysDelimitText) {
        this.alwaysDelimitText = alwaysDelimitText;
        return this;
    }

    @Override
    public AbstractCsvWriterBuilder setLineDelimiter(char[] lineDelimiter) {
        this.lineDelimiter = lineDelimiter;
        return this;
    }

    @Override
    public AbstractCsvWriterBuilder setFieldSeparator(char fieldSeparator) {
        this.fieldSeparator = fieldSeparator;
        return this;
    }
}

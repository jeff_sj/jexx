package jexx.csv;

/**
 * @author jeff
 * @since 2019/10/10
 */
public abstract class AbstractCsvReaderBuilder implements CsvReaderBuilder {

    protected char fieldSeparator = ',';

    protected char textDelimiter = '"';

    protected boolean containHeader = false;

    protected boolean skipEmptyRows = true;

    protected boolean errorIfDifferentFieldCount = false;

    @Override
    public AbstractCsvReaderBuilder setFieldSeparator(char fieldSeparator) {
        this.fieldSeparator = fieldSeparator;
        return this;
    }

    @Override
    public AbstractCsvReaderBuilder setTextDelimiter(char textDelimiter) {
        this.textDelimiter = textDelimiter;
        return this;
    }

    @Override
    public AbstractCsvReaderBuilder setContainHeader(boolean containHeader) {
        this.containHeader = containHeader;
        return this;
    }

    @Override
    public AbstractCsvReaderBuilder setSkipEmptyRows(boolean skipEmptyRows) {
        this.skipEmptyRows = skipEmptyRows;
        return this;
    }

    @Override
    public AbstractCsvReaderBuilder setErrorIfDifferentFieldCount(boolean errorIfDifferentFieldCount) {
        this.errorIfDifferentFieldCount = errorIfDifferentFieldCount;
        return this;
    }

}

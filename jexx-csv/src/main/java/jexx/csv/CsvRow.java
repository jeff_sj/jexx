package jexx.csv;

import java.util.Collections;
import java.util.List;

/**
 * csv行数据
 * @author jeff
 * @since 2019/10/10
 */
public class CsvRow {

    private final long lineNumber;
    private final List<String> fields;

    public CsvRow(long lineNumber, List<String> fields) {
        this.lineNumber = lineNumber;
        this.fields = fields;
    }

    public long getLineNumber() {
        return lineNumber;
    }

    public List<String> getFields() {
        return Collections.unmodifiableList(fields);
    }
}

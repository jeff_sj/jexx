package jexx.csv.fastcsv;

import jexx.csv.AbstractCsvReaderBuilder;
import jexx.csv.CsvReader;

import java.io.IOException;

/**
 * @author jeff
 * @since 2019/10/10
 */
public class FastcsvCsvReaderBuilder extends AbstractCsvReaderBuilder {

    @Override
    public CsvReader build() throws IOException {
        FastcsvCsvReaderImpl reader = new FastcsvCsvReaderImpl();
        reader.setFieldSeparator(fieldSeparator);
        reader.setTextDelimiter(textDelimiter);
        reader.setContainHeader(containHeader);
        reader.setSkipEmptyRows(skipEmptyRows);
        reader.setErrorIfDifferentFieldCount(errorIfDifferentFieldCount);
        return reader;
    }

}

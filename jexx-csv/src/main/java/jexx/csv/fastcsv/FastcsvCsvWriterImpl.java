package jexx.csv.fastcsv;

import de.siegmar.fastcsv.writer.CsvAppender;
import jexx.csv.CsvWriter;
import jexx.csv.CsvWriterAppender;

import java.io.File;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.Collection;
import java.util.Objects;

/**
 * fastcsv 实现的 {@link CsvWriter}
 * @author jeff
 * @since 2019/10/8
 */
public class FastcsvCsvWriterImpl implements CsvWriter {

    /**
     * 字段分割符, 默认 ','
     */
    protected char fieldSeparator = ',';

    /**
     * 获取文本包装符, 默认 '"'
     */
    protected char textDelimiter = '"';

    /**
     * 字段是否始终使用文本包装符, 默认false
     */
    protected boolean alwaysDelimitText;

    /**
     * 换行符, 默认系统换行符
     */
    protected char[] lineDelimiter = System.lineSeparator().toCharArray();

    protected de.siegmar.fastcsv.writer.CsvWriter csvWriter;

    public FastcsvCsvWriterImpl() {
        this.csvWriter = new de.siegmar.fastcsv.writer.CsvWriter();
        updateConfig();
    }

    private void updateConfig(){
        this.csvWriter.setFieldSeparator(getFieldSeparator());
        this.csvWriter.setTextDelimiter(getTextDelimiter());
        this.csvWriter.setAlwaysDelimitText(isAlwaysDelimitText());
        this.csvWriter.setLineDelimiter(getLineDelimiter());
    }

    @Override
    public char getFieldSeparator() {
        return fieldSeparator;
    }

    public void setFieldSeparator(char fieldSeparator) {
        this.fieldSeparator = fieldSeparator;
        this.csvWriter.setFieldSeparator(fieldSeparator);
    }

    @Override
    public char getTextDelimiter() {
        return textDelimiter;
    }

    public void setTextDelimiter(char textDelimiter) {
        this.textDelimiter = textDelimiter;
        this.csvWriter.setTextDelimiter(textDelimiter);
    }

    @Override
    public boolean isAlwaysDelimitText() {
        return alwaysDelimitText;
    }

    public void setAlwaysDelimitText(boolean alwaysDelimitText) {
        this.alwaysDelimitText = alwaysDelimitText;
        this.csvWriter.setAlwaysDelimitText(alwaysDelimitText);
    }

    @Override
    public char[] getLineDelimiter() {
        return lineDelimiter;
    }

    public void setLineDelimiter(char[] lineDelimiter) {
        this.lineDelimiter = lineDelimiter;
        this.csvWriter.setLineDelimiter(lineDelimiter);
    }

    @Override
    public void write(File file, Charset charset, Collection<String[]> data) throws IOException {
        write(
                Objects.requireNonNull(file, "file must not be null").toPath(),
                Objects.requireNonNull(charset, "charset must not be null"),
                data
        );
    }

    @Override
    public void write(File file, Collection<String[]> data) throws IOException {
        write(file, StandardCharsets.UTF_8, data);
    }

    @Override
    public void write(Path path, Charset charset, Collection<String[]> data) throws IOException {
        write(newWriter(
                Objects.requireNonNull(path, "path must not be null"),
                Objects.requireNonNull(charset, "charset must not be null")
        ), data);
    }

    @Override
    public void write(Path path, Collection<String[]> data) throws IOException {
        write(path, StandardCharsets.UTF_8, data);
    }

    @Override
    public void write(final Writer writer, Collection<String[]> data) throws IOException {
        CsvWriterAppender appender = append(writer);
        for(String[] line : data){
            appender.appendLine(line);
        }
        appender.flush();
    }

    @Override
    public CsvWriterAppender append(File file, Charset charset) throws IOException {
        return append(
                Objects.requireNonNull(file, "file must not be null").toPath(),
                Objects.requireNonNull(charset, "charset must not be null")
        );
    }

    @Override
    public CsvWriterAppender append(File file) throws IOException {
        return append(file, StandardCharsets.UTF_8);
    }

    @Override
    public CsvWriterAppender append(Path path, Charset charset) throws IOException {
        return append(newWriter(
                Objects.requireNonNull(path, "path must not be null"),
                Objects.requireNonNull(charset, "charset must not be null")
        ));
    }

    @Override
    public CsvWriterAppender append(Path path) throws IOException {
        return append(path, StandardCharsets.UTF_8);
    }

    @Override
    public CsvWriterAppender append(Writer writer) {
        CsvAppender csvAppender = this.csvWriter.append(writer);
        return new FastcsvCsvWriterAppenderImpl(csvAppender);
    }

    private static Writer newWriter(final Path path, final Charset charset) throws IOException {
        return new OutputStreamWriter(Files.newOutputStream(path, StandardOpenOption.CREATE,
                StandardOpenOption.TRUNCATE_EXISTING), charset);
    }

}

package jexx.csv.fastcsv;

import de.siegmar.fastcsv.reader.CsvParser;
import jexx.csv.CsvReader;
import jexx.csv.CsvReaderParser;
import jexx.csv.CsvRow;
import jexx.csv.CsvSheet;

import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @author jeff
 * @since 2019/10/10
 */
public class FastcsvCsvReaderImpl implements CsvReader {

    private char fieldSeparator = ',';

    private char textDelimiter = '"';

    private boolean containHeader = false;

    private boolean skipEmptyRows = true;

    private boolean errorIfDifferentFieldCount = false;

    private de.siegmar.fastcsv.reader.CsvReader reader;

    public FastcsvCsvReaderImpl() {
        this.reader = new de.siegmar.fastcsv.reader.CsvReader();
        updateConfig(this.reader);
    }

    @Override
    public char getFieldSeparator() {
        return fieldSeparator;
    }

    @Override
    public char getTextDelimiter() {
        return textDelimiter;
    }

    @Override
    public boolean isContainHeader() {
        return containHeader;
    }

    @Override
    public boolean isSkipEmptyRow() {
        return skipEmptyRows;
    }

    @Override
    public boolean isErrorIfDifferentFieldCount() {
        return errorIfDifferentFieldCount;
    }

    public void setFieldSeparator(char fieldSeparator) {
        this.fieldSeparator = fieldSeparator;
        this.reader.setFieldSeparator(fieldSeparator);
    }

    public void setTextDelimiter(char textDelimiter) {
        this.textDelimiter = textDelimiter;
        this.reader.setTextDelimiter(textDelimiter);
    }

    public void setContainHeader(boolean containHeader) {
        this.containHeader = containHeader;
        this.reader.setContainsHeader(containHeader);
    }

    public void setSkipEmptyRows(boolean skipEmptyRows) {
        this.skipEmptyRows = skipEmptyRows;
        this.reader.setSkipEmptyRows(skipEmptyRows);
    }

    public void setErrorIfDifferentFieldCount(boolean errorIfDifferentFieldCount) {
        this.errorIfDifferentFieldCount = errorIfDifferentFieldCount;
        this.reader.setErrorOnDifferentFieldCount(errorIfDifferentFieldCount);
    }

    @Override
    public CsvSheet read(File file, Charset charset) throws IOException {
        Objects.requireNonNull(file, "file must be not null");
        return read(file.toPath(), charset);
    }

    @Override
    public CsvSheet read(File file) throws IOException {
        return read(file, StandardCharsets.UTF_8);
    }

    @Override
    public CsvSheet read(Path path, Charset charset) throws IOException {
        Objects.requireNonNull(path, "path must be not null");
        Objects.requireNonNull(charset, "charset must be not null");
        return read(newPathReader(path, charset));
    }

    @Override
    public CsvSheet read(Path path) throws IOException {
        return read(path, StandardCharsets.UTF_8);
    }

    @Override
    public CsvSheet read(Reader reader) throws IOException {
        Objects.requireNonNull(reader, "reader must not be null");

        final CsvReaderParser csvParser = parse(reader);

        final List<CsvRow> rows = new ArrayList<>();
        CsvRow csvRow;
        while((csvRow = csvParser.nextRow()) != null) {
            rows.add(csvRow);
        }

        final List<String> header = isContainHeader() ? csvParser.getHeader() : null;
        return new CsvSheet(header, rows);
    }

    @Override
    public CsvReaderParser parse(Path path, Charset charset) throws IOException {
        return parse(newPathReader(
                Objects.requireNonNull(path, "path must be not null"),
                Objects.requireNonNull(charset, "charset must be not null")
        ));
    }

    @Override
    public CsvReaderParser parse(Path path) throws IOException {
        return parse(path, StandardCharsets.UTF_8);
    }

    @Override
    public CsvReaderParser parse(File file, Charset charset) throws IOException {
        return parse(
                Objects.requireNonNull(file, "file must be not null").toPath(),
                Objects.requireNonNull(charset, "charset must be not null")
        );
    }

    @Override
    public CsvReaderParser parse(File file) throws IOException {
        return parse(file, StandardCharsets.UTF_8);
    }

    @Override
    public CsvReaderParser parse(Reader reader) throws IOException {
        CsvParser csvParser = this.reader.parse(reader);
        return new FastcsvCsvReaderParserImpl(csvParser);
    }

    private Reader newPathReader(final Path path, final Charset charset) throws IOException {
        return new InputStreamReader(Files.newInputStream(path, StandardOpenOption.READ), charset);
    }

    private void updateConfig(de.siegmar.fastcsv.reader.CsvReader reader){
        reader.setFieldSeparator(getFieldSeparator());
        reader.setTextDelimiter(getTextDelimiter());
        reader.setContainsHeader(isContainHeader());
        reader.setSkipEmptyRows(isSkipEmptyRow());
        reader.setErrorOnDifferentFieldCount(isErrorIfDifferentFieldCount());
    }

}

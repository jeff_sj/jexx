package jexx.csv.fastcsv;

import de.siegmar.fastcsv.writer.CsvAppender;
import jexx.csv.CsvWriterAppender;

import java.io.IOException;
import java.util.Objects;

/**
 * @author jeff
 * @since 2019/10/10
 */
public class FastcsvCsvWriterAppenderImpl implements CsvWriterAppender {

    private CsvAppender csvAppender;

    public FastcsvCsvWriterAppenderImpl(CsvAppender csvAppender) {
        Objects.requireNonNull(csvAppender, "csvAppender cannot be null!");
        this.csvAppender = csvAppender;
    }

    @Override
    public void appendField(String value) throws IOException {
        csvAppender.appendField(value);
    }

    @Override
    public void appendLine(String[] values) throws IOException {
        csvAppender.appendLine(values);
    }

    @Override
    public void endLine() throws IOException {
        csvAppender.endLine();
    }

    @Override
    public void close() throws IOException {
        csvAppender.close();
    }

    @Override
    public void flush() throws IOException {
        csvAppender.flush();
    }
}

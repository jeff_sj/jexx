package jexx.csv.fastcsv;

import jexx.csv.CsvWriter;
import jexx.csv.AbstractCsvWriterBuilder;
import jexx.util.ClassUtil;

/**
 * fastcsv 实现的 {@link AbstractCsvWriterBuilder}
 * https://github.com/osiegmar/FastCSV
 * @author jeff
 * @since 2019/10/8
 */
public class FastcsvCsvWriterBuilder extends AbstractCsvWriterBuilder {

    public FastcsvCsvWriterBuilder() {
        if(!ClassUtil.exist("de.siegmar.fastcsv.writer.CsvWriter")){
            throw new IllegalArgumentException("cannot find fastcsv");
        }
    }

    @Override
    public CsvWriter build() {
        FastcsvCsvWriterImpl csvWriter = new FastcsvCsvWriterImpl();
        csvWriter.setFieldSeparator(fieldSeparator);
        csvWriter.setTextDelimiter(textDelimiter);
        csvWriter.setAlwaysDelimitText(alwaysDelimitText);
        csvWriter.setLineDelimiter(lineDelimiter);
        return csvWriter;
    }

}

package jexx.csv.fastcsv;

import de.siegmar.fastcsv.reader.CsvParser;
import jexx.csv.CsvReaderParser;
import jexx.csv.CsvRow;

import java.io.IOException;
import java.util.List;
import java.util.Objects;

/**
 * @author jeff
 * @since 2019/10/10
 */
public class FastcsvCsvReaderParserImpl implements CsvReaderParser {

    private CsvParser csvParser;

    public FastcsvCsvReaderParserImpl(CsvParser csvParser) {
        Objects.requireNonNull(csvParser, "parser must be not null");
        this.csvParser = csvParser;
    }

    @Override
    public List<String> getHeader() {
        return csvParser.getHeader();
    }

    @Override
    public CsvRow nextRow() throws IOException {
        de.siegmar.fastcsv.reader.CsvRow row = csvParser.nextRow();
        if(row == null){
            return null;
        }
        return new CsvRow(row.getOriginalLineNumber(), row.getFields());
    }

    @Override
    public void close() throws IOException {
        csvParser.close();
    }
}

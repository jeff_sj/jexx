package jexx.csv;

import java.io.Closeable;
import java.io.IOException;
import java.util.List;

/**
 * csv 具体解析类
 * @author jeff
 * @since 2019/10/10
 */
public interface CsvReaderParser extends Closeable {

    List<String> getHeader();

    CsvRow nextRow() throws IOException;

}

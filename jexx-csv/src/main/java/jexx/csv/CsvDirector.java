package jexx.csv;

import jexx.csv.fastcsv.FastcsvCsvReaderBuilder;
import jexx.csv.fastcsv.FastcsvCsvWriterBuilder;

/**
 * csv建造者导演类
 * @author jeff
 * @since 2019/10/10
 */
public class CsvDirector {

    /**
     * 构建csv读取建造者类
     * @return {@link CsvReaderBuilder}
     */
    public static CsvReaderBuilder buildReader(){
        return buildFastcsvReader();
    }

    /**
     * 构建依赖于fastcsv的csv读取建造者类
     * @return {@link CsvReaderBuilder}
     */
    public static CsvReaderBuilder buildFastcsvReader(){
        return new FastcsvCsvReaderBuilder();
    }

    /**
     * 构建csv写入建造者类
     * @return {@link CsvWriterBuilder}
     */
    public static CsvWriterBuilder buildWriter(){
        return buildFastcsvWriter();
    }

    /**
     * 构建依赖于fastcsv的csv写入建造者类
     * @return {@link CsvWriterBuilder}
     */
    public static CsvWriterBuilder buildFastcsvWriter(){
        return new FastcsvCsvWriterBuilder();
    }

}

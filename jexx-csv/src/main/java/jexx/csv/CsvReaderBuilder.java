package jexx.csv;

import java.io.IOException;

/**
 * csv读构造器
 * @author jeff
 * @since 2019/10/10
 */
public interface CsvReaderBuilder {

    CsvReaderBuilder setFieldSeparator(char fieldSeparator);

    CsvReaderBuilder setTextDelimiter(char textDelimiter);

    CsvReaderBuilder setContainHeader(boolean containHeader);

    CsvReaderBuilder setSkipEmptyRows(boolean skipEmptyRows);

    CsvReaderBuilder setErrorIfDifferentFieldCount(boolean errorIfDifferentFieldCount);

    CsvReader build() throws IOException;

}

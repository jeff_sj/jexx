package jexx.csv;

import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.nio.charset.Charset;
import java.nio.file.Path;

/**
 * csv 读取器接口
 * @author jeff
 * @since 2019/10/9
 */
public interface CsvReader {

    /**
     * 字段分割符, 默认 ','
     * @return 变量分割符
     */
    char getFieldSeparator();

    /**
     * 获取文本包装符, 默认 '"'
     * @return 文本定界符
     */
    char getTextDelimiter();

    /**
     * 是否包含header
     * @return bool
     */
    boolean isContainHeader();

    /**
     * 是否跳过空行
     * @return bool
     */
    boolean isSkipEmptyRow();

    /**
     * 如果csv数据含有不匹配的字段数,是否抛出异常, 默认false
     * @return bool
     */
    boolean isErrorIfDifferentFieldCount();

    /**
     * 读取csv文件
     * @param file csv文件
     * @param charset 读取编码
     * @return csv数据
     * @throws IOException I/O异常
     */
    CsvSheet read(final File file, final Charset charset) throws IOException;

    /**
     * 默认UTF8读取csv文件
     * @param file csv文件
     * @return csv数据
     * @throws IOException I/O异常
     */
    CsvSheet read(final File file) throws IOException;

    /**
     * 读取csv文件
     * @param path Path
     * @param charset 读取编码
     * @return csv数据
     * @throws IOException I/O异常
     */
    CsvSheet read(final Path path, final Charset charset) throws IOException;

    /**
     * 默认UTF8读取csv文件
     * @return csv数据
     * @throws IOException I/O异常
     */
    CsvSheet read(final Path path) throws IOException;

    /**
     * 读取csv文件
     * @param reader {@link Reader}
     * @return csv数据
     * @throws IOException I/O异常
     */
    CsvSheet read(final Reader reader) throws IOException;

    /**
     * 获取解析csv文件的核心类
     * @param path Path
     * @param charset 解析编码
     * @return {@link CsvReaderParser}
     * @throws IOException I/O异常
     */
    CsvReaderParser parse(final Path path, final Charset charset) throws IOException;

    /**
     * 默认UTF8,获取解析csv文件的核心类
     * @param path Path
     * @return {@link CsvReaderParser}
     * @throws IOException I/O异常
     */
    CsvReaderParser parse(final Path path) throws IOException;

    /**
     * 获取解析csv文件的核心类
     * @param file csv文件
     * @param charset 解析编码
     * @return {@link CsvReaderParser}
     * @throws IOException I/O异常
     */
    CsvReaderParser parse(final File file, final Charset charset) throws IOException;

    /**
     * 默认UTF8,获取解析csv文件的核心类
     * @param file csv文件
     * @return {@link CsvReaderParser}
     * @throws IOException I/O异常
     */
    CsvReaderParser parse(final File file) throws IOException;

    /**
     * 获取解析csv文件的核心类
     * @param reader {@link Reader}
     * @return {@link CsvReaderParser}
     * @throws IOException I/O异常
     */
    CsvReaderParser parse(final Reader reader) throws IOException;

}

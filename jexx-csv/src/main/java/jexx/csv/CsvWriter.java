package jexx.csv;

import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.util.Collection;

/**
 * csv写入器接口
 * @author jeff
 * @since 2019/10/8
 */
public interface CsvWriter {

    /**
     * 字段分割符, 默认 ','
     * @return 变量分割符
     */
    char getFieldSeparator();

    /**
     * 获取文本包装符, 默认 '"'
     * @return 文本定界符
     */
    char getTextDelimiter();

    /**
     * 字段是否始终使用文本包装符
     * @return bool
     */
    boolean isAlwaysDelimitText();

    /**
     * 换行符
     * @return 换行符
     */
    char[] getLineDelimiter();

    /**
     * 写入多行
     * @param file 待写入文件
     * @param charset 写入数据编码
     * @param data 多行数据
     * @throws IOException I/O异常
     */
    void write(final File file, final Charset charset, final Collection<String[]> data) throws IOException;

    /**
     * 默认UTF8编码写入多行
     * @param file 待写入文件
     * @param data 多行数据
     * @throws IOException I/O异常
     */
    void write(final File file, final Collection<String[]> data) throws IOException;

    /**
     * 写入多行
     * @param path Path
     * @param charset 写入数据编码
     * @param data 多行数据
     * @throws IOException I/O异常
     */
    void write(final Path path, final Charset charset, final Collection<String[]> data) throws IOException;

    /**
     * 默认UTF8编码写入多行
     * @param path Path
     * @param data 多行数据
     * @throws IOException I/O异常
     */
    void write(final Path path, final Collection<String[]> data) throws IOException;

    /**
     * 写入多行
     * @param data 多行数据
     * @throws IOException I/O异常
     */
    void write(final Writer writer, final Collection<String[]> data) throws IOException;

    /**
     * 为指定文件构造 {@link CsvWriterAppender}
     * @param file 待写入数据的文件
     * @param charset 写入数据编码
     * @return {@link CsvWriterAppender}
     * @throws IOException I/O异常
     */
    CsvWriterAppender append(final File file, final Charset charset) throws IOException;

    /**
     * 默认UTF8编码为指定文件构造 {@link CsvWriterAppender}
     * @param file 待写入数据的文件
     * @return {@link CsvWriterAppender}
     * @throws IOException I/O异常
     */
    CsvWriterAppender append(final File file) throws IOException;

    /**
     * 为指定文件构造 {@link CsvWriterAppender}
     * @param path 待写入数据的文件
     * @param charset 写入数据编码
     * @return {@link CsvWriterAppender}
     * @throws IOException I/O异常
     */
    CsvWriterAppender append(final Path path, final Charset charset) throws IOException;

    /**
     * 默认UTF8编码为指定文件构造 {@link CsvWriterAppender}
     * @param path 待写入数据的文件
     * @return {@link CsvWriterAppender}
     * @throws IOException I/O异常
     */
    CsvWriterAppender append(final Path path) throws IOException;

    /**
     * 为指定字节输入流写入数据
     * @param writer 字节输入流
     * @return {@link CsvWriterAppender}
     * @throws IOException I/O异常
     */
    CsvWriterAppender append(final Writer writer) throws IOException;

}

package jexx.csv;

import java.io.Closeable;
import java.io.Flushable;
import java.io.IOException;
import java.util.Collection;

/**
 * 写csv数据的主要类
 * @author jeff
 * @since 2019/10/10
 */
public interface CsvWriterAppender extends Closeable, Flushable {

    /**
     * 写入一个字段
     * @param value 一个字段
     * @throws IOException if an I/O error occurs
     */
    void appendField(final String value) throws IOException;

    /**
     * 写入一行数据
     * @param values 一行数据
     * @throws IOException if an I/O error occurs
     */
    void appendLine(final String[] values) throws IOException;

    /**
     * 当前行追加换行符
     * @throws IOException if an I/O error occurs
     */
    void endLine() throws IOException;


}

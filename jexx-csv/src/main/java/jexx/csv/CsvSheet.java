package jexx.csv;

import java.util.Collections;
import java.util.List;

/**
 * csv文件的全部数据
 * @author jeff
 * @since 2019/10/10
 */
public class CsvSheet {

    private List<String> headers;
    private List<CsvRow> rows;

    public CsvSheet(List<String> headers, List<CsvRow> rows) {
        this.headers = headers;
        this.rows = rows;
    }

    public List<String> getHeaders() {
        return Collections.unmodifiableList(headers);
    }

    public List<CsvRow> getRows() {
        return Collections.unmodifiableList(rows);
    }

}

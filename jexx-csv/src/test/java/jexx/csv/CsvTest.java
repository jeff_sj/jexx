package jexx.csv;

import jexx.io.FileUtil;
import jexx.util.Console;
import jexx.util.ResourceUtil;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;

/**
 * @author jeff
 * @since 2019/10/9
 */
public class CsvTest {

    protected String baseDir;

    @Before
    public void setUp(){
        URL data = ResourceUtil.getResource("");
        baseDir = data.getFile().concat("/").concat(CsvTest.class.getSimpleName());
        FileUtil.mkdirs(baseDir);
        Console.log(baseDir);
    }

    @Test
    public void testWriteAndRead() throws IOException {
        CsvWriter writer = CsvDirector.buildWriter().setAlwaysDelimitText(true).build();

        Collection<String[]> data = new ArrayList<>();
        data.add(new String[] { "header1", "header2" });
        data.add(new String[] { "value1", "value2" });
        data.add(new String[] { "value2", "中国" });
        File file1 = FileUtil.file(baseDir, "testWrite1.csv");
        writer.write(file1, data);


        File file2 = FileUtil.file(baseDir, "testWrite2.csv");
        CsvWriterAppender appender = writer.append(file2);
        for(String[] line : data){
            appender.appendLine(line);
        }
        appender.flush();
        appender.close();

        //read
        CsvReader reader = CsvDirector.buildReader().setContainHeader(true).build();

        CsvSheet sheet = reader.read(file1);
        Assert.assertArrayEquals(new String[] { "header1", "header2" }, sheet.getHeaders().toArray());
        Assert.assertArrayEquals(new String[] { "value1", "value2" }, sheet.getRows().get(0).getFields().toArray());

        sheet = reader.read(file2);
        Assert.assertArrayEquals(new String[] { "header1", "header2" }, sheet.getHeaders().toArray());
        Assert.assertArrayEquals(new String[] { "value1", "value2" }, sheet.getRows().get(0).getFields().toArray());
    }


}
